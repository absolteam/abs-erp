package com.aiotech.aios.hr.action;

/*******************************************************************************
 *
 * Modified Log
 * -----------------------------------------------------------------------------
 * Ver 		Created Date 	Created By                 Description
 * -----------------------------------------------------------------------------
 * 1.0		13 March 2012 	Mohamed Rabeek S.	 		Initial Version

 ******************************************************************************/

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.action.GLCombinationAction;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.CreditTerm;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.ShippingDetail;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.vo.CustomerVO;
import com.aiotech.aios.accounts.service.GLChartOfAccountService;
import com.aiotech.aios.accounts.service.GLCombinationService;
import com.aiotech.aios.accounts.service.bl.CustomerBL;
import com.aiotech.aios.common.to.Constants.Accounts.CustomerType;
import com.aiotech.aios.common.to.FileVO;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.AcademicQualifications;
import com.aiotech.aios.hr.domain.entity.AdvertisingPanel;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.Candidate;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Dependent;
import com.aiotech.aios.hr.domain.entity.Experiences;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.domain.entity.OpenPosition;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.PersonType;
import com.aiotech.aios.hr.domain.entity.RecruitmentSource;
import com.aiotech.aios.hr.domain.entity.Skills;
import com.aiotech.aios.hr.domain.entity.vo.PersonVO;
import com.aiotech.aios.hr.service.bl.PersonBL;
import com.aiotech.aios.hr.service.bl.SupplierBL;
import com.aiotech.aios.hr.to.HrPersonalDetailsTO;
import com.aiotech.aios.hr.to.converter.HrPersonalDetailsTOConverter;
import com.aiotech.aios.system.bl.CommonBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.domain.entity.Country;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.notify.enterprise.NotifyEnterpriseService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class HrPersonalDetailsAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(HrPersonalDetailsAction.class);
	private int recId;
	private String firstName;
	private String firstNameArabic;
	private String lastName;
	private String lastNameArabic;
	private String title;
	private String prefix;
	private String suffix;
	private String middleName;
	private String middleNameArabic;
	private String personTypes;
	private int personFlag;
	private String emirateId;
	private String identification;
	private String latestStartDate;
	private String effectiveDatesFrom;
	private String effectiveDatesTo;
	private String birthDate;
	private String townBirth;
	private String regionBirth;
	private String countryBirth;
	private String nationalityTypeId;
	private char gender;
	private String age;
	private char maritalStatus;
	private String nationality;
	private String nationalityTypeName;
	private String countryCode;
	private String countryName;

	private int disabled;
	private String imageUpload;
	private String office;
	private String location;
	private String mailStop;
	private String emailAddress;
	private String mailTo;
	private int exists;
	private String exists1;
	private String holdApplicationUntil;
	private String lastUpdatedResume;
	private String honors;
	private String preferredName;
	private String previousLastName;
	private String availabilitySchedules;
	private Integer fullTimeAvailability;
	private String correspondenceLanguage;
	private String deathDate;
	private String studentStatus;
	private String dateLastVerified;
	private int military;
	private int secondPassport;
	private int personId;
	private String pageName;
	private int sessionId;
	private int flagnew;
	private Boolean isCustomer;
	private Boolean isSupplier;
	private String viewType;

	private Byte supplierAcType;
	private Byte acClassification;
	private long paymentTermId;
	private Double creditLimit;
	private Double openingBalance;
	private String bankName;
	private String branchName;
	private String ibanNumber;
	private String accountNumber;
	private Integer supplierNumber;
	private Long currencyId;

	private Long customerId;
	private String customerNumber;
	private Integer customerAcType;
	private Long creditTermId;
	private Long saleRepresentativeId;
	private String billingAddress;
	private Long refferalSource;
	private Long shippingMethod;
	private String customerDescription;
	private String shippingDetails;

	private String identificationId;
	private String description;
	private String expirationdate;
	private String identificationDesc;
	private String identificationExpDate;

	private String applicantId;
	// work flow
	private String notificationId;
	private String notificationType;
	private String companyId;
	private String workflowId;
	private String workflowName;
	private String workflowStatus;
	private String categoryId;
	private String notifyFunctionId;
	private String functionId;
	private String wfFunctionType;
	private String workflowParam;
	private String mappingId;
	private String attribute1;
	private String attribute2;
	private String applicationId;
	private String dmsTrnId;
	private Character personGroup;
	private Character nationalityChar;
	private String redirectUrl;
	String jsonResult;
	String json;
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private int sessionPersonId;
	private Implementation implementation = null;
	private String addEditFlag;
	private String address;
	private String email;
	private String phone;
	private String mobile;
	private long personAddressId;
	private long identityId;
	private int identityType;
	private String identityNumber;
	private String issuedPlace;
	private String expireDate;
	private long dependentId;
	private int relationshipId;
	private String dependentName;
	private String dependentMobile;
	private String dependentPhone;
	private String dependentMail;
	private boolean emergency;
	private boolean dependentflag;
	private boolean benefit;
	private String dependentAddress;
	private String dependentDescription;
	private String residentialAddress;
	private String postBoxNumber;
	private String permanentAddress;
	private String alternateMobile;
	private String telephone;
	private String alternateTelephone;
	private String tradeLicenseNumber;
	private String tradeIssuedPlace;
	private String tradeExpireDate;
	private String tradeDescription;
	private Long recordId;
	private Long supplierId;
	private Long employeeCombinationId;
	private Long tenantCombinationId;
	private Long supplierCombinationId;
	private Long customerCombinationId;
	private Long ownerCombinationId;
	private File fileUpload;
	private String fileUploadContentType;
	private String fileUploadFileName;
	private String profilePic;
	private String completionYear;
	private Long candidateId;
	private Long openPositionId;
	private Long recruitmentSourceId;
	private Long advertisingPanelId;
	private String candidateStatus;
	private Long candidateAvailability;
	private String candidateDescription;
	private String expectedJoiningDate;
	private String startDate;
	private String endDate;
	private byte memberCarType;
	private int cardNumber;
	// Accounts Integration
	private Long combinationId;
	private GLChartOfAccountService accountService;
	private GLCombinationService combinationService;
	private GLCombinationAction combinationAction;
	// Grid To variabls, getter & setters
	List<HrPersonalDetailsTO> colors = new ArrayList<HrPersonalDetailsTO>();

	private PersonBL personBL;
	private ImageBL imageBL;
	private DocumentBL documentBL;
	private SupplierBL supplierBL;
	private CustomerBL customerBL;
	CommonBL commonBL;
	// result List
	private List<HrPersonalDetailsTO> personalDetailsList;
	private Identity identity = null;
	private List<Identity> identitys = null;
	private Dependent dependent = null;
	private List<Dependent> dependents = null;
	private AcademicQualifications qualification;
	private List<AcademicQualifications> qualifications = null;
	private Experiences experience;
	private List<Experiences> experiences = null;
	private Skills skill;
	private List<Skills> skills = null;
	private String id;
	private String returnMessage;
	private String employeeId;
	private String employeeSecretPin;
	private String posPersonId;
	private InputStream fileInputStream;
	private List<PersonVO> personDS;
	private Object personObject;
	private Map<String, Object> jasperRptParams = new HashMap<String, Object>();

	private NotifyEnterpriseService notifyEnterpriseService;

	public void getImplementId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		implementation = new Implementation();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
			// implementationId=implementation.getImplementationId();
			// LOGGER.info("implementation id------>"+implementationId);
		}
	}

	public String returnSuccess() {
		try {
			ServletActionContext.getRequest().setAttribute("PERSON_TYPES",
					personTypes);
			ServletActionContext.getRequest().setAttribute("rowid", id);
			ServletActionContext.getRequest().setAttribute("AddEditFlag",
					addEditFlag);

			if (title != null && !title.equalsIgnoreCase(""))
				ServletActionContext.getRequest().setAttribute("POPUP_TITLE",
						title);
			else
				ServletActionContext.getRequest().setAttribute("POPUP_TITLE",
						"Personal Information");
			return SUCCESS;

		} catch (Exception e) {
			return ERROR;
		}
	}

	// Listing the Personal Details
	public String execute() {
		try {
			getImplementId();
			personalDetailsList = new ArrayList<HrPersonalDetailsTO>();
			// Set the value to push to Personal Details DAO
			List<Person> person = personBL.getPersonService().getAllPerson(
					implementation);
			person = personBL.personTypeFilteration("1", person);
			personalDetailsList = HrPersonalDetailsTOConverter
					.convertToTOList(person);

			Country country = null;
			PersonType personType = null;
			if (personalDetailsList != null && personalDetailsList.size() > 0) {
				country = new Country();
				personType = new PersonType();
				for (int i = 0; i < personalDetailsList.size(); i++) {
					// Find the country
					if (personalDetailsList.get(i).getCountryCode() != null
							&& !personalDetailsList.get(i).getCountryCode()
									.equals("")) {
						country = personBL.getPersonService().getCountry(
								Long.parseLong(personalDetailsList.get(i)
										.getCountryCode()));
						personalDetailsList.get(i).setCountryBirth(
								country.getCountryName());
					}

				}

				iTotalRecords = personalDetailsList.size();
				iTotalDisplayRecords = personalDetailsList.size();

			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (HrPersonalDetailsTO list : personalDetailsList) {

				// if (list.getPersonTypes().contains("Supplier") ||
				// list.getPersonTypes().contains("Customer"))
				// continue;
				JSONArray array = new JSONArray();
				array.add(list.getPersonId());
				array.add(list.getFirstName());
				array.add(list.getLastName());
				array.add(list.getPersonTypes());
				array.add(list.getCountryBirth());
				array.add(list.getIdentification() + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String getPersonList() {
		try {
			getImplementId();
			personalDetailsList = new ArrayList<HrPersonalDetailsTO>();
			// Set the value to push to Personal Details DAO
			List<Person> person = personBL.getPersonList(personTypes);

			personalDetailsList = HrPersonalDetailsTOConverter
					.convertToTOList(person);

			Country country = null;
			PersonType personType = null;
			if (personalDetailsList != null && personalDetailsList.size() > 0) {
				country = new Country();
				personType = new PersonType();
				for (int i = 0; i < personalDetailsList.size(); i++) {
					// Find the country
					if (null != personalDetailsList.get(i).getCountryCode()
							&& !personalDetailsList.get(i).getCountryCode()
									.equals("")) {
						country = personBL.getPersonService().getCountry(
								Long.parseLong(personalDetailsList.get(i)
										.getCountryCode()));
						personalDetailsList.get(i).setCountryBirth(
								country.getCountryName());
					}
				}

				iTotalRecords = personalDetailsList.size();
				iTotalDisplayRecords = personalDetailsList.size();

			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (HrPersonalDetailsTO list : personalDetailsList) {

				// if (list.getPersonTypes().contains("Supplier") ||
				// list.getPersonTypes().contains("Customer"))
				// continue;
				JSONArray array = new JSONArray();
				array.add(list.getPersonId());
				array.add(list.getFirstName());
				array.add(list.getLastName());
				array.add(list.getPersonTypes());
				array.add(list.getCountryBirth());
				array.add(list.getIdentification() + "");
				array.add(list.getEmailAddress() + "");
				array.add(list.getMobile() + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String getAllEmployee() {
		try {
			getImplementId();
			personalDetailsList = new ArrayList<HrPersonalDetailsTO>();
			// Set the value to push to Personal Details DAO
			List<Person> person = personBL.getPersonService().getAllEmployee(
					implementation);

			personalDetailsList = HrPersonalDetailsTOConverter
					.convertToTOList(person);

			Country country = null;
			PersonType personType = null;
			if (personalDetailsList != null && personalDetailsList.size() > 0) {
				country = new Country();
				personType = new PersonType();
				for (int i = 0; i < personalDetailsList.size(); i++) {
					// Find the country
					country = personBL.getPersonService().getCountry(
							Long.parseLong(personalDetailsList.get(i)
									.getCountryCode()));
					personalDetailsList.get(i).setCountryBirth(
							country.getCountryName());

				}

				iTotalRecords = personalDetailsList.size();
				iTotalDisplayRecords = personalDetailsList.size();

			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (HrPersonalDetailsTO list : personalDetailsList) {

				if (list.getPersonTypes().contains("Supplier"))
					continue;
				JSONArray array = new JSONArray();
				array.add(list.getPersonId());
				array.add(list.getFirstName());
				array.add(list.getLastName());
				array.add(list.getPersonTypes());
				array.add(list.getCountryBirth());
				array.add(list.getIdentification() + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	// Listing the Personal Details
	public String addPersonRedirect() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			long documentRecId = -1;
			AIOSCommons.removeUploaderSession(session, "personDocs",
					documentRecId, "Person");
			AIOSCommons.removeUploaderSession(session, "personImages",
					documentRecId, "Person");
			getImplementId();
			// Removing Sessions
			session.removeAttribute("PERSON_IDENTITY_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("PERSON_DEPENDENT_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("PERSON_IDENTITY_TYPE_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("QUALIFICATION_LIST_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("EXPERIENCE_LIST_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("SKILL_LIST_"
					+ sessionObj.get("jSessionId"));

			session.removeAttribute("AIOS-img-" + "Person-" + session.getId());
			session.removeAttribute("AIOS-doc-" + "Person-" + session.getId());
			session.removeAttribute("ProfilePicture_"
					+ sessionObj.get("jSessionId"));

			personId = this.getPersonId();
			if (recordId != null && recordId > 0) {
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				comment.setRecordId(recordId);
				comment.setUseCase(AttendancePolicy.class.getName());
				comment = personBL.getCommentBL().getCommentInfo(comment);
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
			}
			if (null != recordId && ("").equals(recordId) && recordId > 0)
				personId = Integer.parseInt(recordId + "");

			AIOSCommons.removeUploaderSession(session, "personDocs",
					(long) personId, "Person");
			AIOSCommons.removeUploaderSession(session, "personImages",
					(long) personId, "Person");
			HrPersonalDetailsTO personalDetailsTO = new HrPersonalDetailsTO();
			List<Country> country = new ArrayList<Country>();
			List<PersonType> personType = new ArrayList<PersonType>();
			List<HrPersonalDetailsTO> identityList = null;
			List<HrPersonalDetailsTO> dependentList = null;
			identitys = new ArrayList<Identity>();
			dependents = new ArrayList<Dependent>();
			Person person = new Person();
			Combination employeeCombination = null;
			Combination tenantCombination = null;
			Combination customerCombination = null;
			Combination supplierCombination = null;
			Combination ownerCombination = null;
			if (personId != 0) {
				person = personBL.getPersonService().getPersonDetails(
						Long.parseLong(personId + ""));

				personalDetailsTO = HrPersonalDetailsTOConverter
						.convertPersonToTO(person);
				identitys = personBL.getPersonService().getAllIdentity(person);
				dependents = personBL.getPersonService()
						.getAllDependent(person);
				identityList = HrPersonalDetailsTOConverter
						.convertIdentityToTOList(identitys);
				dependentList = HrPersonalDetailsTOConverter
						.convertDependentToTOList(dependents);
				// Can be used for jasperReport views
				/*
				 * List<PersonVO>
				 * qualificationList=personBL.getAllAcadamicQualifications
				 * (person); List<PersonVO>
				 * experienceList=personBL.getAllExperiences(person);
				 * List<PersonVO> skillList=personBL.getAllSkills(person);
				 */
				qualifications = personBL.getPersonService()
						.getAllAcademicQualifications(person);
				experiences = personBL.getPersonService().getAllExperiences(
						person);
				skills = personBL.getPersonService().getAllSkills(person);
				{

					employeeCombination = personBL.findCombinationInfo(person
							.getFirstName()
							+ (person.getLastName() != null
									&& !person.getLastName().equals("") ? " "
									+ person.getLastName() : "")
							+ "-"
							+ person.getPersonNumber() + "-EMPLOYEE");

					tenantCombination = personBL.findCombinationInfo(person
							.getFirstName()
							+ (person.getLastName() != null
									&& !person.getLastName().equals("") ? " "
									+ person.getLastName() : "")
							+ "-"
							+ person.getPersonNumber() + "-TENANT");

					if (null != person.getCustomersForPersonId()
							&& person.getCustomersForPersonId().size() > 0) {
						List<Customer> customers = new ArrayList<Customer>(
								person.getCustomersForPersonId());
						customerCombination = personBL.getCombinationService()
								.getCombinationAllAccountDetail(
										customers.get(0).getCombination()
												.getCombinationId());
					} else if (null != person.getSuppliers()
							&& person.getSuppliers().size() > 0) {
						List<Supplier> suppliers = new ArrayList<Supplier>(
								person.getSuppliers());
						supplierCombination = personBL.getCombinationService()
								.getCombinationAllAccountDetail(
										suppliers.get(0).getCombination()
												.getCombinationId());
					}

					ownerCombination = personBL.findCombinationInfo(person
							.getFirstName()
							+ (person.getLastName() != null
									&& !person.getLastName().equals("") ? " "
									+ person.getLastName() : "")
							+ "-"
							+ person.getPersonNumber() + "-OWNER");
				}

			}

			byte[] encoded = null;
			byte[] fileByte = null;
			String pmg = new String();
			if (person.getProfilePic() != null) {
				String hashName = new String(person.getProfilePic());
				fileByte = personBL.getImageBL().showImageByHashedName(
						hashName, "AIOSProfilePicture");
				if (fileByte != null) {
					encoded = Base64.encodeBase64(fileByte);
					pmg = new String(encoded);
					session.setAttribute(
							"ProfilePicture_" + sessionObj.get("jSessionId"),
							person.getProfilePic());
					personalDetailsTO.setProfilePic(pmg);
				} else {
					personalDetailsTO.setProfilePic(null);
				}

			}

			country = personBL.getPersonService().getAllCountry();
			personType = personBL.getPersonService().getAllPersonType();

			List<LookupDetail> personTypeList = personBL.getLookupMasterBL()
					.getActiveLookupDetails("PERSON_TYPE", true);
			List<LookupDetail> personIdentityTypes = personBL
					.getLookupMasterBL().getActiveLookupDetails(
							"PERSON_IDENTITY_LIST", false);
			session.setAttribute(
					"PERSON_IDENTITY_TYPE_" + sessionObj.get("jSessionId"),
					personIdentityTypes);
			ServletActionContext.getRequest().setAttribute("bean",
					personalDetailsTO);
			ServletActionContext.getRequest().setAttribute("COUNTRYLIST",
					country);
			ServletActionContext.getRequest().setAttribute("PERSONTYPELIST",
					personTypeList);
			ServletActionContext.getRequest().setAttribute("IDENTITY_LIST",
					identityList);
			ServletActionContext.getRequest().setAttribute("DEPENDENT_LIST",
					dependentList);
			ServletActionContext.getRequest().setAttribute(
					"QUALIFICATION_LIST", qualifications);
			ServletActionContext.getRequest().setAttribute("EXPERIENCE_LIST",
					experiences);
			ServletActionContext.getRequest()
					.setAttribute("SKILL_LIST", skills);

			session.setAttribute(
					"PERSON_IDENTITY_" + sessionObj.get("jSessionId"),
					identitys);
			session.setAttribute(
					"PERSON_DEPENDENT_" + sessionObj.get("jSessionId"),
					dependents);

			session.setAttribute(
					"QUALIFICATION_LIST_" + sessionObj.get("jSessionId"),
					qualifications);
			session.setAttribute(
					"EXPERIENCE_LIST_" + sessionObj.get("jSessionId"),
					experiences);
			session.setAttribute("SKILL_LIST_" + sessionObj.get("jSessionId"),
					skills);

			ServletActionContext.getRequest().setAttribute("EMPLOYEE_ACCOUNT",
					employeeCombination);
			ServletActionContext.getRequest().setAttribute("TENANT_ACCOUNT",
					tenantCombination);
			ServletActionContext.getRequest().setAttribute("SUPPLIER_ACCOUNT",
					supplierCombination);
			ServletActionContext.getRequest().setAttribute("CUSTOMER_ACCOUNT",
					customerCombination);

			List<Company> companies = personBL.getCompanyService()
					.getAllCompany(implementation);
			ServletActionContext.getRequest().setAttribute("COMPANY_LIST",
					companies);

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	@SuppressWarnings("unused")
	public String viewPersonDetail() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			long documentRecId = -1;
			AIOSCommons.removeUploaderSession(session, "personDocs",
					documentRecId, "Person");
			AIOSCommons.removeUploaderSession(session, "personImages",
					documentRecId, "Person");
			// Removing Sessions
			session.removeAttribute("PERSON_IDENTITY_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("PERSON_DEPENDENT_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("QUALIFICATION_LIST_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("EXPERIENCE_LIST_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("SKILL_LIST_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("PERSON_IDENTITY_TYPE_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("AIOS-img-" + "Person-" + session.getId());
			session.removeAttribute("AIOS-doc-" + "Person-" + session.getId());
			session.removeAttribute("ProfilePicture_"
					+ sessionObj.get("jSessionId"));

			HrPersonalDetailsTO personalDetailsTO = new HrPersonalDetailsTO();
			List<Country> country = new ArrayList<Country>();
			List<PersonType> personType = new ArrayList<PersonType>();
			List<HrPersonalDetailsTO> identityList = null;
			List<HrPersonalDetailsTO> dependentList = null;
			identitys = new ArrayList<Identity>();
			dependents = new ArrayList<Dependent>();
			Person person = new Person();
			Combination employeeCombination = null;
			Combination tenantCombination = null;
			Combination customerCombination = null;
			Combination supplierCombination = null;
			Combination ownerCombination = null;
			List<Person> persons = null;

			if (personId != 0) {
				persons = personBL.getPersonList("ALL");

				if (persons != null && persons.size() > 0) {
					Collections.sort(persons, new Comparator<Person>() {
						public int compare(Person m1, Person m2) {
							return m2.getFirstName().compareTo(
									m1.getFirstName());
						}
					});
					int i = 0;
					int recordIndex = 0;
					for (Person personTemp : persons) {
						if (personTemp.getPersonId() == personId)
							recordIndex = i;

						i++;
					}

					if (viewType.equalsIgnoreCase("NEXT")
							&& (persons.size() - 1) > recordIndex)
						person = persons.get(recordIndex + 1);
					else if (viewType.equalsIgnoreCase("PREVIOUS")
							&& 0 < recordIndex)
						person = persons.get(recordIndex - 1);
					else
						person = personBL.getPersonService().getPersonDetails(
								Long.parseLong(personId + ""));

					AIOSCommons.removeUploaderSession(session, "personDocs",
							(long) person.getPersonId(), "Person");
					AIOSCommons.removeUploaderSession(session, "personImages",
							(long) person.getPersonId(), "Person");
				}

				personalDetailsTO = HrPersonalDetailsTOConverter
						.convertPersonToTO(person);
				identitys = personBL.getPersonService().getAllIdentity(person);
				dependents = personBL.getPersonService()
						.getAllDependent(person);
				identityList = HrPersonalDetailsTOConverter
						.convertIdentityToTOList(identitys);
				dependentList = HrPersonalDetailsTOConverter
						.convertDependentToTOList(dependents);

				// Can be used for jasperReport views
				/*
				 * List<PersonVO>
				 * qualificationList=personBL.getAllAcadamicQualifications
				 * (person); List<PersonVO>
				 * experienceList=personBL.getAllExperiences(person);
				 * List<PersonVO> skillList=personBL.getAllSkills(person);
				 */
				qualifications = personBL.getPersonService()
						.getAllAcademicQualifications(person);
				experiences = personBL.getPersonService().getAllExperiences(
						person);
				skills = personBL.getPersonService().getAllSkills(person);
				{

					employeeCombination = personBL.findCombinationInfo(person
							.getFirstName()
							+ (person.getLastName() != null
									&& !person.getLastName().equals("") ? " "
									+ person.getLastName() : "")
							+ "-"
							+ person.getPersonNumber() + "-EMPLOYEE");

					tenantCombination = personBL.findCombinationInfo(person
							.getFirstName()
							+ (person.getLastName() != null
									&& !person.getLastName().equals("") ? " "
									+ person.getLastName() : "")
							+ "-"
							+ person.getPersonNumber() + "-TENANT");

					customerCombination = personBL.findCombinationInfo(person
							.getFirstName()
							+ (person.getLastName() != null
									&& !person.getLastName().equals("") ? " "
									+ person.getLastName() : "")
							+ "-"
							+ person.getPersonNumber() + "-CUSTOMER");

					supplierCombination = personBL.findCombinationInfo(person
							.getFirstName()
							+ (person.getLastName() != null
									&& !person.getLastName().equals("") ? " "
									+ person.getLastName() : "")
							+ "-"
							+ person.getPersonNumber() + "-SUPPLIER");

					ownerCombination = personBL.findCombinationInfo(person
							.getFirstName()
							+ " "
							+ person.getLastName()
							+ "-"
							+ person.getPersonNumber() + "-OWNER");
				}

			}

			byte[] encoded = null;
			byte[] fileByte = null;
			String pmg = new String();
			if (person.getProfilePic() != null) {
				String hashName = new String(person.getProfilePic());
				fileByte = personBL.getImageBL().showImageByHashedName(
						hashName, "AIOSProfilePicture");
				if (fileByte != null) {
					encoded = Base64.encodeBase64(fileByte);
					pmg = new String(encoded);
					session.setAttribute(
							"ProfilePicture_" + sessionObj.get("jSessionId"),
							person.getProfilePic());
					personalDetailsTO.setProfilePic(pmg);
				} else {
					personalDetailsTO.setProfilePic(null);
				}

			}

			country = personBL.getPersonService().getAllCountry();
			personType = personBL.getPersonService().getAllPersonType();

			List<LookupDetail> personTypeList = personBL.getLookupMasterBL()
					.getActiveLookupDetails("PERSON_TYPE", true);
			List<LookupDetail> personIdentityTypes = personBL
					.getLookupMasterBL().getActiveLookupDetails(
							"PERSON_IDENTITY_LIST", false);
			session.setAttribute(
					"PERSON_IDENTITY_TYPE_" + sessionObj.get("jSessionId"),
					personIdentityTypes);
			ServletActionContext.getRequest().setAttribute("bean",
					personalDetailsTO);
			ServletActionContext.getRequest().setAttribute("COUNTRYLIST",
					country);
			ServletActionContext.getRequest().setAttribute("PERSONTYPELIST",
					personTypeList);
			ServletActionContext.getRequest().setAttribute("IDENTITY_LIST",
					identityList);
			ServletActionContext.getRequest().setAttribute("DEPENDENT_LIST",
					dependentList);
			ServletActionContext.getRequest().setAttribute(
					"QUALIFICATION_LIST", qualifications);
			ServletActionContext.getRequest().setAttribute("EXPERIENCE_LIST",
					experiences);
			ServletActionContext.getRequest()
					.setAttribute("SKILL_LIST", skills);

			session.setAttribute(
					"PERSON_IDENTITY_" + sessionObj.get("jSessionId"),
					identitys);
			session.setAttribute(
					"PERSON_DEPENDENT_" + sessionObj.get("jSessionId"),
					dependents);
			session.setAttribute(
					"QUALIFICATION_LIST_" + sessionObj.get("jSessionId"),
					qualifications);
			session.setAttribute(
					"EXPERIENCE_LIST_" + sessionObj.get("jSessionId"),
					experiences);
			session.setAttribute("SKILL_LIST_" + sessionObj.get("jSessionId"),
					skills);

			ServletActionContext.getRequest().setAttribute("EMPLOYEE_ACCOUNT",
					employeeCombination);
			ServletActionContext.getRequest().setAttribute("TENANT_ACCOUNT",
					tenantCombination);
			ServletActionContext.getRequest().setAttribute("SUPPLIER_ACCOUNT",
					supplierCombination);
			ServletActionContext.getRequest().setAttribute("CUSTOMER_ACCOUNT",
					customerCombination);

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	// Save Person Customer Local
	public String saveSpecialCustomer() {
		try {
			getImplementId();
			PersonVO personVO = null;
			String customerName = firstName;
			if (null != customerId && customerId > 0) {
				Person person = personBL.getPersonService()
						.getPersonByCustomerId(customerId);
				String personNameArray[] = firstName.split(" ");
				if (personNameArray.length > 0) {
					firstName = personNameArray[0];
					lastName = "";
					for (int i = 1; i < personNameArray.length; i++) {
						lastName += personNameArray[i];
					}
				}
				person.setFirstName(firstName);
				person.setLastName(null != lastName ? lastName : firstName);
				person.setMobile(mobile);
				person.setResidentialAddress(address);
				Person personLocal = personBL.getPersonService()
						.getPersonByCustomerId(customerId);
				personLocal.setFirstName(firstName);
				if (gender == 'M')
					person.setPrefix("MR.");
				else
					person.setPrefix("MISS.");
				personLocal
						.setLastName(null != lastName ? lastName : firstName);
				personLocal.setMobile(mobile);
				personLocal.setGender(gender);
				personLocal.setResidentialAddress(address);
				personBL.udpateCustomerPerson(personLocal);
				personVO = new PersonVO();
				personVO.setCustomerId(customerId);
				personVO.setCustomerName(customerName);
			} else {
				// Fetch Person Number
				List<Person> personList = personBL.getPersonService()
						.getAllPerson(implementation);
				List<Integer> personalDetailsList = new ArrayList<Integer>();
				if (personList != null && personList.size() > 0) {
					for (int i = 0; i < personList.size(); i++) {
						personalDetailsList.add(Integer.parseInt(personList
								.get(i).getPersonNumber().toString()));
					}
					int temp = Collections.max(personalDetailsList);
					temp += 1;
					identification = temp + "";
				} else {
					identification = "1000";
				}
				Person person = new Person();
				person.setPersonNumber(identification);
				String personNameArray[] = firstName.split(" ");
				if (personNameArray.length > 0) {
					firstName = personNameArray[0];
					lastName = "";
					for (int i = 1; i < personNameArray.length; i++) {
						lastName += personNameArray[i];
					}
				}
				person.setFirstName(firstName);
				person.setLastName(null != lastName ? lastName : firstName);
				person.setMobile(mobile);
				person.setBirthDate(Calendar.getInstance().getTime());
				person.setCountryBirth("252");
				person.setNationality("252");
				person.setPersonGroup('E');
				person.setGender(gender);
				if (gender == 'M')
					person.setPrefix("MR.");
				else
					person.setPrefix("MISS.");
				person.setStatus((byte) 1);
				person.setResidentialAddress(address);
				person.setImplementation(implementation);
				List<Customer> customerList = customerBL.getCustomerService()
						.getAllCustomers(implementation);
				if (null != customerList && customerList.size() > 0) {
					List<Long> customerNoList = new ArrayList<Long>();
					for (Customer list : customerList) {
						customerNoList.add(Long.parseLong(list
								.getCustomerNumber()));
					}
					long customerNo = Collections.max(customerNoList);
					customerNumber = String.valueOf(customerNo + 1);
				} else
					customerNumber = "1000";
				Customer customer = new Customer();
				customer.setCustomerNumber(customerNumber);
				customer.setBillingAddress(address);
				customer.setCustomerType(CustomerType.Retail.getCode());
				customer.setImplementation(implementation);
				customer.setIsApprove((byte) WorkflowConstants.Status.Published
						.getCode());
				ShippingDetail shippingDetail = new ShippingDetail();
				shippingDetail.setName(firstName);
				shippingDetail.setContactNo("0");
				person.setPersonId(null);
				customer.setCustomerId(null);
				shippingDetail.setShippingId(null);
				customer.setCombination(null);
				customer.setPosCustomer(true);
				customer = personBL.saveCustomerPerson(person, customer,
						shippingDetail);
				personVO = new PersonVO();
				personVO.setCustomerId(customer.getCustomerId());
				personVO.setCustomerName(customerName);
			}
			personObject = personVO;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Person Customer Local
	public String saveCustomerCommon() {
		try {
			getImplementId();
			PersonVO personVO = null;
			String customerName = firstName;
			if (null != customerId && customerId > 0) {
				Person person = personBL.getPersonService()
						.getPersonByCustomerId(customerId);
				String personNameArray[] = firstName.split(" ");
				if (personNameArray.length > 0) {
					firstName = personNameArray[0];
					lastName = "";
					for (int i = 1; i < personNameArray.length; i++) {
						lastName += personNameArray[i];
					}
				}
				person.setFirstName(firstName);
				person.setLastName(null != lastName ? lastName : firstName);
				person.setMobile(mobile);
				person.setResidentialAddress(address);
				Person personLocal = personBL.getPersonService()
						.getPersonByCustomerId(customerId);
				personLocal.setFirstName(firstName);
				if (gender == 'M')
					person.setPrefix("MR.");
				else
					person.setPrefix("MISS.");
				personLocal
						.setLastName(null != lastName ? lastName : firstName);
				personLocal.setMobile(mobile);
				personLocal.setGender(gender);
				personLocal.setResidentialAddress(address);
				personBL.udpateCustomerPerson(personLocal);
				personVO = new PersonVO();
				personVO.setCustomerId(customerId);
				personVO.setCustomerName(customerName);
			} else {
				// Fetch Person Number
				List<Person> personList = personBL.getPersonService()
						.getAllPersonWithoutStatus(implementation);
				List<Integer> personalDetailsList = new ArrayList<Integer>();
				if (personList != null && personList.size() > 0) {
					for (int i = 0; i < personList.size(); i++) {
						personalDetailsList.add(Integer.parseInt(personList
								.get(i).getPersonNumber().toString()));
					}
					int temp = Collections.max(personalDetailsList);
					temp += 1;
					identification = temp + "";
				} else {
					identification = "1000";
				}
				Person person = new Person();
				person.setPersonNumber(identification);
				String personNameArray[] = firstName.split(" ");
				if (personNameArray.length > 0) {
					firstName = personNameArray[0];
					lastName = "";
					for (int i = 1; i < personNameArray.length; i++) {
						lastName += personNameArray[i];
					}
				}
				person.setFirstName(firstName);
				person.setLastName(null != lastName ? lastName : firstName);
				person.setMobile(mobile);
				person.setGender(gender);
				if (gender == 'M')
					person.setPrefix("MR.");
				else
					person.setPrefix("MISS.");
				person.setStatus((byte) 1);
				person.setImplementation(implementation);
				List<Customer> customerList = customerBL.getCustomerService()
						.getAllPOSCustomers(implementation);
				if (null != customerList && customerList.size() > 0) {
					List<Long> customerNoList = new ArrayList<Long>();
					for (Customer list : customerList) {
						customerNoList.add(Long.parseLong(list
								.getCustomerNumber()));
					}
					long customerNo = Collections.max(customerNoList);
					customerNumber = String.valueOf(customerNo + 1);
				} else
					customerNumber = "1000";
				Customer customer = new Customer();
				customer.setCustomerNumber(customerNumber);
				customer.setBillingAddress(address);
				customer.setCustomerType(CustomerType.Retail.getCode());
				customer.setImplementation(implementation);
				ShippingDetail shippingDetail = new ShippingDetail();
				shippingDetail.setName(firstName);
				shippingDetail.setContactNo("0");
				person.setPersonId(null);

				Combination customerCombination = personBL
						.findCombinationInfo(person.getFirstName() + " "
								+ person.getLastName() + "-"
								+ person.getPersonNumber() + "-CUSTOMER");
				customer.setCustomerId(null);
				shippingDetail.setShippingId(null);
				customer.setCombination(customerCombination);
				customer = personBL.saveCustomerPerson(person, customer,
						shippingDetail);
				personVO = new PersonVO();
				personVO.setCustomerId(customer.getCustomerId());
				personVO.setCustomerName(customerName);
			}
			personObject = personVO;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Upload customers
	public String uploadPersonCustomer() {
		try {
			Map<Byte, List<CustomerVO>> customerMapVOs = personBL
					.addPersonCustomer(personBL.fileReader(fileUploadFileName));
			List<Person> persons = null;
			Person person = null;
			List<Company> companys = null;
			Company company = null;
			Combination combination = new Combination();
			List<ShippingDetail> shippingDetails = null;
			List<Customer> customers = null;
			Customer cst = null;
			ShippingDetail shippingDetail = null;
			List<CustomerVO> customerVOs = customerMapVOs.get((byte) 0);
			if (null != customerVOs && customerVOs.size() > 0) {
				shippingDetails = new ArrayList<ShippingDetail>();
				persons = new ArrayList<Person>();
				customers = new ArrayList<Customer>();
				combination.setCombinationId(customerVOs.get(0)
						.getCombinationId());
				List<Person> personList = personBL.getPersonService()
						.getAllPerson(personBL.getImplementation());
				List<Integer> personalDetailsList = new ArrayList<Integer>();
				int personNumber = 1000;
				if (personList != null && personList.size() > 0) {
					for (int i = 0; i < personList.size(); i++) {
						personalDetailsList.add(Integer.parseInt(personList
								.get(i).getPersonNumber().toString()));
					}
					personNumber = Collections.max(personalDetailsList);
					personNumber += 1;
				}
				List<Customer> customerList = customerBL.getCustomerService()
						.getAllCustomers(personBL.getImplementation());
				long customerNo = 1000;
				if (null != customerList && customerList.size() > 0) {
					List<Long> customerNoList = new ArrayList<Long>();
					for (Customer list : customerList) {
						customerNoList.add(Long.parseLong(list
								.getCustomerNumber()));
					}
					customerNo = Collections.max(customerNoList);
					customerNo += 1;
				}
				for (CustomerVO customer : customerVOs) {
					person = new Person();
					shippingDetail = new ShippingDetail();
					cst = new Customer();
					person.setFirstName(customer.getCustomerName());
					person.setLastName("");
					person.setGender('M');
					person.setMobile(customer.getMobileNumber());
					person.setPersonNumber(String.valueOf(personNumber));
					cst.setPersonByPersonId(person);
					cst.setCombination(combination);
					cst.setImplementation(personBL.getImplementation());
					person.setImplementation(personBL.getImplementation());
					cst.setCustomerType(CustomerType.Others.getCode());
					cst.setCustomerNumber(String.valueOf(customerNo));
					shippingDetail.setName(person.getFirstName());
					shippingDetail.setCustomer(cst);
					shippingDetails.add(shippingDetail);
					persons.add(person);
					customers.add(cst);
					personNumber += 1;
					customerNo += 1;
				}
				personBL.saveCustomerPerson(persons, customers, shippingDetails);
			}
			List<CustomerVO> customerCompanyVOs = customerMapVOs.get((byte) 1);
			if (null != customerCompanyVOs && customerCompanyVOs.size() > 0) {
				shippingDetails = new ArrayList<ShippingDetail>();
				companys = new ArrayList<Company>();
				customers = new ArrayList<Customer>();
				List<Customer> customerList = customerBL.getCustomerService()
						.getAllPOSCustomers(personBL.getImplementation());
				long customerNo = 1000;
				if (null != customerList && customerList.size() > 0) {
					List<Long> customerNoList = new ArrayList<Long>();
					for (Customer list : customerList) {
						customerNoList.add(Long.parseLong(list
								.getCustomerNumber()));
					}
					customerNo = Collections.max(customerNoList);
					customerNo += 1;
				}
				combination.setCombinationId(customerCompanyVOs.get(0)
						.getCombinationId());
				for (CustomerVO customer : customerCompanyVOs) {
					company = new Company();
					shippingDetail = new ShippingDetail();
					company.setCompanyName(customer.getCustomerName());
					company.setCompanyPhone(customer.getMobileNumber());
					cst = new Customer();
					cst.setCompany(company);
					cst.setImplementation(personBL.getImplementation());
					cst.setCombination(combination);
					cst.setCustomerType(CustomerType.Others.getCode());
					cst.setCustomerNumber(String.valueOf(customerNo));
					shippingDetail.setName(customer.getCustomerName());
					shippingDetail.setCustomer(cst);
					shippingDetails.add(shippingDetail);
					companys.add(company);
					customers.add(cst);
					customerNo += 1;
				}
				personBL.saveCustomerCompany(companys, customers,
						shippingDetails);
			}
			returnMessage = "SUCCESS";
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String customerDirverDetails() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: customerDirverDetails");
			Customer customer = personBL.getCustomerBL().getCustomerService()
					.getCustomerDetails(customerId);
			PersonVO personVO = new PersonVO();
			if (null != customer.getPersonByPersonId()) {
				personVO.setResidentialAddress(customer.getPersonByPersonId()
						.getResidentialAddress());
				personVO.setMobile(customer.getPersonByPersonId().getMobile());
				personVO.setCustomerName(customer.getPersonByPersonId()
						.getFirstName()
						+ " "
						+ customer.getPersonByPersonId().getLastName());
			} else if (null != customer.getCmpDeptLocation()) {
				personVO.setResidentialAddress(customer.getCmpDeptLocation()
						.getCompany().getCompanyAddress());
				personVO.setMobile(customer.getCmpDeptLocation().getCompany()
						.getCompanyPhone());
				personVO.setCustomerName(customer.getCmpDeptLocation()
						.getCompany().getCompanyName());
			} else if (null != customer.getCompany()) {
				personVO.setResidentialAddress(customer.getCompany()
						.getCompanyAddress());
				personVO.setMobile(customer.getCompany().getCompanyPhone());
				personVO.setCustomerName(customer.getCompany().getCompanyName());
			}
			List<Person> persons = personBL.getPersonList("1");
			ServletActionContext.getRequest().setAttribute("PERSONS_LIST",
					persons);
			ServletActionContext.getRequest().setAttribute("CUSTOMER_INFO",
					personVO);
			LOGGER.info("Module: Accounts : Method: customerDirverDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: customerDirverDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// save the Personal Details
	@SuppressWarnings("unchecked")
	public String savePerson() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		long documentRecId = -1;
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person createdPer = new Person();
			createdPer.setPersonId(user.getPersonId());
			Person person = new Person();
			PersonType personType = new PersonType();
			PersonVO personVO = new PersonVO();

			getImplementId();
			// Fetch Person Number
			List<Person> personList = personBL.getPersonService()
					.getAllPersonWithoutStatus(implementation);
			List<Integer> personalDetailsList = new ArrayList<Integer>();
			Map<String, String> secretPinSet = null;
			if (identification == null || identification.trim().equals("0")
					|| identification.trim().equals("")) {
				if (personList != null && personList.size() > 0) {
					secretPinSet = new HashMap<String, String>();
					for (int i = 0; i < personList.size(); i++) {
						personalDetailsList.add(Integer.parseInt(personList
								.get(i).getPersonNumber().toString()));
						if (null != personList.get(i).getSecretPin())
							secretPinSet.put(personList.get(i).getSecretPin(),
									personList.get(i).getSecretPin());
					}
					LOGGER.info("Person Number Max()--"
							+ Collections.max(personalDetailsList));

					int temp = Collections.max(personalDetailsList);
					temp += 1;
					identification = temp + "";
				} else {
					identification = "1000";
				}
			}
			Person personToEdit = null;
			if (recId != 0) {
				person.setPersonId(Long.parseLong(recId + ""));
				personToEdit = personBL.getPersonService().getPersonDetails(
						Long.parseLong(recId + ""));

			}
			// personType.setPersonTypeId(Integer.parseInt(personTypes));
			person.setPersonNumber(identification);
			person.setFirstName(firstName);
			person.setMiddleName(middleName);
			person.setLastName(lastName);
			person.setFirstNameArabic(AIOSCommons
					.stringToUTFBytes(firstNameArabic));
			person.setMiddleNameArabic(AIOSCommons
					.stringToUTFBytes(middleNameArabic));
			person.setLastNameArabic(AIOSCommons
					.stringToUTFBytes(lastNameArabic));
			person.setPrefix(prefix);
			person.setSuffix(suffix);
			// person.setPersonType(personType);
			person.setValidFromDate(DateFormat
					.convertStringToDate(effectiveDatesFrom));
			person.setValidToDate(DateFormat
					.convertStringToDate(effectiveDatesTo));
			if (birthDate != null && !birthDate.equals(""))
				person.setBirthDate(DateFormat.convertStringToDate(birthDate));

			if (countryBirth != null && !countryBirth.equals(""))
				person.setCountryBirth(countryBirth);

			if (regionBirth != null && !regionBirth.equals(""))
				person.setRegionBirth(regionBirth);

			if (companyId != null && !companyId.equals("")
					&& Long.parseLong(companyId) > 0) {
				Company company = new Company();
				company.setCompanyId(Long.parseLong(companyId));
				person.setCompany(company);
			}

			person.setTownBirth(townBirth);
			person.setPersonGroup(personGroup);
			person.setNationality(nationality);
			person.setGender(gender);
			person.setMaritalStatus(maritalStatus);
			person.setImplementation(implementation);

			person.setResidentialAddress(residentialAddress);
			person.setPermanentAddress(permanentAddress);

			if (mobile != null && !mobile.trim().equals(""))
				person.setMobile(mobile);
			if (alternateMobile != null && !alternateMobile.trim().equals(""))
				person.setAlternateMobile(alternateMobile);
			if (telephone != null && !telephone.trim().equals(""))
				person.setTelephone(telephone);
			if (alternateTelephone != null
					&& !alternateTelephone.trim().equals(""))
				person.setAlternateTelephone(alternateTelephone);

			person.setPostboxNumber(postBoxNumber);
			person.setEmail(email);
			person.setTradeLicenseNumber(tradeLicenseNumber);
			person.setIssuedPlace(tradeIssuedPlace);
			if (tradeExpireDate != null)
				person.setExpireDate(DateFormat
						.convertStringToDate(tradeExpireDate));
			person.setDescription(tradeDescription);
			person.setStatus(Byte.parseByte(personFlag + ""));

			List<Identity> identitys = (ArrayList<Identity>) session
					.getAttribute("PERSON_IDENTITY_"
							+ sessionObj.get("jSessionId"));
			List<Dependent> dependents = (ArrayList<Dependent>) session
					.getAttribute("PERSON_DEPENDENT_"
							+ sessionObj.get("jSessionId"));

			List<AcademicQualifications> qualifications = (ArrayList<AcademicQualifications>) session
					.getAttribute("QUALIFICATION_LIST_"
							+ sessionObj.get("jSessionId"));
			List<Experiences> experiences = (ArrayList<Experiences>) session
					.getAttribute("EXPERIENCE_LIST_"
							+ sessionObj.get("jSessionId"));
			List<Skills> skills = (ArrayList<Skills>) session
					.getAttribute("SKILL_LIST_" + sessionObj.get("jSessionId"));
			// Multiple Person Types
			personVO.setPersonTypes(personTypes);

			byte[] fl = (byte[]) session.getAttribute("ProfilePicture_"
					+ sessionObj.get("jSessionId"));

			if (fl != null) {
				String fileHashName = "";
				Map<String, byte[]> imageFileBytesMap = new HashMap<String, byte[]>();
				Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");
				imageFileBytesMap.put(
						firstName + "_" + identification + ".PNG", fl);
				Map<String, FileVO> imageFilesMap = FileUtil.copyCachedFiles(
						imageFileBytesMap, confProps, null,
						"AIOSProfilePicture");
				for (Map.Entry<String, FileVO> entry : imageFilesMap.entrySet()) {
					fileHashName = entry.getValue().getHashedName();
				}
				person.setProfilePic(fileHashName.getBytes());
			}

			// Account integration
			personVO.setPersonId(person.getPersonId());
			personVO.setPersonName(person.getFirstName() + " "
					+ person.getLastName());
			personVO.setPersonNumber(person.getPersonNumber());
			if (employeeCombinationId != null && employeeCombinationId > 0)
				personVO.setEmployeeCombinationId(employeeCombinationId);

			if (tenantCombinationId != null && tenantCombinationId > 0)
				personVO.setTenantCombinationId(tenantCombinationId);

			if (supplierCombinationId != null && supplierCombinationId > 0)
				personVO.setSupplierCombinationId(supplierCombinationId);

			if (customerCombinationId != null && customerCombinationId > 0)
				personVO.setCustomerCombinationId(customerCombinationId);

			if (person.getPersonId() != null && person.getPersonId() > 0) {
				List<Identity> tempidentitys = new ArrayList<Identity>(
						personToEdit.getIdentities());
				List<Identity> identityDeleteList = personBL
						.getIdentityToDelete(identitys, tempidentitys);

				List<Dependent> tempDependents = new ArrayList<Dependent>(
						personToEdit.getDependents());
				List<Dependent> dependentDeleteList = personBL
						.getDependentToDelete(dependents, tempDependents);

				if (identityDeleteList != null && identityDeleteList.size() > 0)
					personBL.getPersonService().deleteIdentityAll(
							identityDeleteList);

				if (dependentDeleteList != null
						&& dependentDeleteList.size() > 0)
					personBL.getPersonService().deleteDependentAll(
							dependentDeleteList);

				List<AcademicQualifications> tempqualifications = new ArrayList<AcademicQualifications>(
						personToEdit.getAcademicQualificationses());
				List<AcademicQualifications> qualificationDeleteList = personBL
						.getQualificationToDelete(qualifications,
								tempqualifications);
				if (qualificationDeleteList != null
						&& qualificationDeleteList.size() > 0)
					personBL.getPersonService().deleteQualificationAll(
							qualificationDeleteList);

				List<Experiences> tempexperiences = new ArrayList<Experiences>(
						personToEdit.getExperienceses());
				List<Experiences> experienceDeleteList = personBL
						.getExperienceToDelete(experiences, tempexperiences);
				if (experienceDeleteList != null
						&& experienceDeleteList.size() > 0)
					personBL.getPersonService().deleteExperienceAll(
							experienceDeleteList);

				List<Skills> tempskills = new ArrayList<Skills>(
						personToEdit.getSkillses());
				List<Skills> skillDeleteList = personBL.getSkillToDelete(
						skills, tempskills);
				if (skillDeleteList != null && skillDeleteList.size() > 0)
					personBL.getPersonService().deleteSkillAll(skillDeleteList);

			}

			if (isSupplier) {
				Supplier supplier = new Supplier();
				supplier.setSupplierType(supplierAcType);
				supplier.setClassification(acClassification);
				CreditTerm paymentTerm = null;
				if (paymentTermId > 0) {
					paymentTerm = new CreditTerm();
					paymentTerm.setCreditTermId(paymentTermId);
				}
				supplier.setCreditLimit(creditLimit);
				supplier.setOpeningBalance(openingBalance);
				supplier.setCreditTerm(paymentTerm);
				supplier.setBankName(bankName);
				supplier.setBranchName(branchName);
				supplier.setIbanNumber(ibanNumber);
				supplier.setAccountNumber(accountNumber);
				supplier.setImplementation(implementation);
				// supplier.setCombination(combination);
				if (null != supplierId && supplierId > 0)
					supplier.setSupplierId(supplierId);
				else {
					List<Supplier> supplierList = this.getSupplierBL()
							.getAllSuppliers(implementation);
					if (null != supplierList && supplierList.size() > 0) {
						List<Integer> supplierNo = new ArrayList<Integer>();
						for (Supplier list : supplierList) {
							supplierNo.add(Integer.parseInt(list
									.getSupplierNumber()));
						}
						supplierNumber = Collections.max(supplierNo);
						supplierNumber += 1;
					} else {
						supplierNumber = 1000;
					}
				}
				supplier.setSupplierNumber(String.valueOf(supplierNumber));
				supplier.setPerson(person);
				personVO.setSupplier(supplier);
				personVO.setCurrencyId(currencyId);
				// supplierBL.saveSupplier(supplier, (long)
				// combinationId,currencyId);
			} else if (isCustomer) {
				Customer customer = new Customer();
				customer.setCustomerType(null != customerAcType
						&& customerAcType > 0 ? customerAcType : null);
				LookupDetail lookupDetail = null;
				if (null != refferalSource && refferalSource > 0) {
					lookupDetail = new LookupDetail();
					lookupDetail.setLookupDetailId(refferalSource);
					customer.setLookupDetailByRefferalSource(lookupDetail);
				}
				if (null != shippingMethod && shippingMethod > 0) {
					lookupDetail = new LookupDetail();
					lookupDetail.setLookupDetailId(shippingMethod);
					customer.setLookupDetailByShippingMethod(lookupDetail);
				}
				customer.setBillingAddress(billingAddress);
				customer.setCreditLimit(null != creditLimit && creditLimit > 0 ? creditLimit
						: null);
				customer.setOpeningBalance(null != openingBalance
						&& openingBalance > 0 ? openingBalance : null);
				customer.setDescription(customerDescription);
				customer.setMemberType(memberCarType > 0 ? memberCarType : null);
				customer.setMemberCardNumber(customerId > 0 && cardNumber > 0 ? cardNumber
						: null);
				// customer.setCombination(combination);-----------------------------
				customer.setPersonByPersonId(person);
				customer.setImplementation(implementation);
				if (null != saleRepresentativeId && saleRepresentativeId > 0) {
					Person salePerson = new Person();
					salePerson.setPersonId(saleRepresentativeId);
					customer.setPersonBySaleRepresentative(salePerson);
				}
				if (null != creditTermId && creditTermId > 0) {
					CreditTerm creditTerm = new CreditTerm();
					creditTerm.setCreditTermId(creditTermId);
					customer.setCreditTerm(creditTerm);
				}
				String[] lineDetailArray = splitValues(shippingDetails, "#@");
				ShippingDetail shippingDetails = null;
				List<ShippingDetail> shippingDetailList = new ArrayList<ShippingDetail>();
				for (String detail : lineDetailArray) {
					String[] detailString = splitValues(detail, "__");
					if (detailString.length <= 1)
						continue;

					shippingDetails = new ShippingDetail();
					shippingDetails.setName(detailString[0]);
					shippingDetails.setContactPerson(detailString[1]);
					if (null != detailString[2]
							&& !("").equals(detailString[2])
							&& !("##").equals(detailString[2]))
						shippingDetails.setContactNo(detailString[2]);
					if (null != detailString[3]
							&& !("").equals(detailString[3])
							&& !("##").equals(detailString[3]))
						shippingDetails.setAddress(detailString[3]);
					if (Long.parseLong(detailString[4].trim()) > 0)
						shippingDetails.setShippingId(Long
								.parseLong(detailString[4].trim()));
					shippingDetailList.add(shippingDetails);
				}
				if (null != customerId && customerId > 0) {
					customer.setCustomerId(customerId);
					customer.setCustomerNumber(customerNumber);
				} else {
					List<Customer> customerList = this.getCustomerBL()
							.getCustomerService()
							.getAllCustomers(implementation);
					if (null != customerList && customerList.size() > 0) {
						List<Long> customerNoList = new ArrayList<Long>();
						for (Customer list : customerList) {
							customerNoList.add(Long.parseLong(list
									.getCustomerNumber()));
						}
						long customerNo = Collections.max(customerNoList);
						customer.setCustomerNumber(String.valueOf(++customerNo));

					} else {
						customer.setCustomerNumber("1000");
					}
				}

				personVO.setCustomer(customer);
				personVO.setShippingDetailList(shippingDetailList);
			} else if (openPositionId != null && openPositionId > 0) {
				Candidate candidate = new Candidate();
				if (candidateId != null && candidateId > 0) {
					candidate.setCandidateId(candidateId);
				} else {
					candidate.setCreatedDate(new Date());
					candidate.setPersonByCreatedBy(createdPer);
				}

				candidate.setDescription(candidateDescription);
				candidate.setExpectedJoiningDate(DateFormat
						.convertStringToDate(expectedJoiningDate));
				candidate.setImplementation(getImplementation());
				if (candidateStatus != null)
					candidate.setStatus(Byte.valueOf(candidateStatus));
				if (advertisingPanelId != null && advertisingPanelId > 0) {
					AdvertisingPanel advertisingPanel = new AdvertisingPanel();
					advertisingPanel.setAdvertisingPanelId(advertisingPanelId);
					candidate.setAdvertisingPanel(advertisingPanel);
				}
				if (openPositionId != null && openPositionId > 0) {
					OpenPosition openPosition = new OpenPosition();
					openPosition.setOpenPositionId(openPositionId);
					candidate.setOpenPosition(openPosition);
				}
				if (recruitmentSourceId != null && recruitmentSourceId > 0) {
					RecruitmentSource recruitmentSource = new RecruitmentSource();
					recruitmentSource
							.setRecruitmentSourceId(recruitmentSourceId);
					candidate.setRecruitmentSource(recruitmentSource);
				}
				if (candidateAvailability != null && candidateAvailability > 0) {
					LookupDetail availability = new LookupDetail();
					availability.setLookupDetailId(candidateAvailability);
					candidate.setLookupDetail(availability);
				}
				personVO.setCandidate(candidate);
			}

			personVO.setPersonEdit(personToEdit);

			if (person.getLastName() == null)
				person.setLastName("");

			if (person.getValidFromDate() == null
					|| person.getValidFromDate().equals(""))
				person.setValidFromDate(new Date());

			String[] personTypes = splitValues(personVO.getPersonTypes(), ",");
			boolean pr = false;
			for (String personTyp : personTypes) {
				if (personTyp.equals("1")) {
					pr = true;
					break;
				}
			}
			String secretPin = null;
			User user1 = null;
			if (pr) {
				if (null != person.getPersonId() && person.getPersonId() > 0) {
					secretPin = personToEdit.getSecretPin();
					List<User> users = personBL.getWorkflowEnterpriseService()
							.getUserActionBL().getUserRoleService()
							.getActiveUserByPersonId(person.getPersonId());
					if (null != users && users.size() > 0) {
						user1 = new User();
						BeanUtils.copyProperties(user1, users.get(0));
						user1.setMobileNumber(person.getMobile());
						user1.setEmailAddress(person.getEmail());
					}
				}
			}
			boolean sendSecretPin = false;
			if (null == secretPin || ("").equals(secretPin)) {
				secretPin = createSecretPin(secretPinSet);
				sendSecretPin = true;
			}
			person.setSecretPin(secretPin);
			// -----------------------------Person Save
			// Call------------------------------------------------------------------------------
			personBL.savePerson(person, identitys, dependents, personVO,
					qualifications, experiences, skills, user1);

			if (sendSecretPin) {
				Boolean emailEnabled = session.getAttribute("email_secretpin") != null ? ((String) session
						.getAttribute("email_secretpin"))
						.equalsIgnoreCase("true") : false;
				Boolean smsEnabled = session.getAttribute("sms_secretpin") != null ? ((String) session
						.getAttribute("sms_secretpin"))
						.equalsIgnoreCase("true") : false;
				/*
				 * Properties confProps = (Properties) ServletActionContext
				 * .getServletContext().getAttribute("confProps");
				 */
				if (emailEnabled && null != person.getEmail()
						&& !("").equals(person.getEmail()))
					notifyEnterpriseService
							.sendEmail(
									"Secret Pin",
									"Dear "
											+ WordUtils.capitalizeFully(person
													.getFirstName())
											+ ",<br/><br/>Your 4 Digit Secret Pin: <b>"
											+ person.getSecretPin()
											+ "</b><h6>This pin can only be used to make POS transactions.</h6>",
									person.getEmail(), "en", person
											.getImplementation()
											.getImplementationId(), person
											.getPersonId(), false, null);
				if (smsEnabled && null != person.getMobile()
						&& !("").equals(person.getMobile()))
					notifyEnterpriseService.sendSMS(
							"Dear "
									+ WordUtils.capitalizeFully(person
											.getFirstName())
									+ ",\n\nYour 4 Digit Secret Pin: "
									+ person.getSecretPin(),
							person.getMobile(), "en", person
									.getImplementation().getImplementationId(),
							person.getPersonId(), false, null);
			}
			if (recId > 0) {
				documentRecId = recId;
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "SUCCESS");
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "SUCCESS");
			}
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			if (recId > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			return ERROR;
		} finally {

			AIOSCommons.removeUploaderSession(session, "personDocs",
					documentRecId, "Person");
			AIOSCommons.removeUploaderSession(session, "personImages",
					documentRecId, "Person");
		}
	}

	private String createSecretPin(Map<String, String> secretPinSet) {
		java.util.Random pinGenerator = new java.util.Random();
		String secretPin = String.format("%04d", pinGenerator.nextInt(9999));
		if (null == secretPinSet || secretPinSet.size() == 0)
			return secretPin;
		else if (secretPinSet.containsKey(secretPin))
			createSecretPin(secretPinSet);
		return secretPin;
	}

	// delete the Personal Details
	public String deletePerson() {
		try {
			Person person = new Person();
			String returnMessage = "Successfully Deleted";
			String message = null;
			message = commonBL.deletePersonDependancyCheck(Long.parseLong(recId
					+ ""));
			if (recId != 0 && message == null) {

				personBL.deletePerson(Long.parseLong(recId + ""));
				ServletActionContext.getRequest().setAttribute("successMsg",
						returnMessage);
			} else {
				ServletActionContext.getRequest().setAttribute("errorMsg",
						message);
			}
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errorMsg",
					"Can't Delete, information is in use already");
			return ERROR;
		}

	}

	public String identityAdd() {
		try {

			List<HrPersonalDetailsTO> identityTypeList = HrPersonalDetailsTOConverter
					.identificationTypeList();
			ServletActionContext.getRequest().setAttribute("rowid", id);
			ServletActionContext.getRequest().setAttribute("AddEditFlag",
					addEditFlag);
			ServletActionContext.getRequest().setAttribute(
					"IDENTITY_TYPE_LIST", identityTypeList);
			return SUCCESS;

		} catch (Exception e) {
			return ERROR;
		}
	}

	public String dependentAdd() {
		try {

			List<HrPersonalDetailsTO> dependentTypeList = HrPersonalDetailsTOConverter
					.dependentTypeList();
			ServletActionContext.getRequest().setAttribute("rowid", id);
			ServletActionContext.getRequest().setAttribute("AddEditFlag",
					addEditFlag);
			ServletActionContext.getRequest().setAttribute(
					"DEPENDENT_TYPE_LIST", dependentTypeList);
			return SUCCESS;

		} catch (Exception e) {
			return ERROR;
		}
	}

	public String qualificationAdd() {
		try {

			List<LookupDetail> degrees = personBL.getLookupMasterBL()
					.getActiveLookupDetails("DEGREE", true);
			ServletActionContext.getRequest().setAttribute("rowid", id);
			ServletActionContext.getRequest().setAttribute("AddEditFlag",
					addEditFlag);
			ServletActionContext.getRequest().setAttribute("DEGREES", degrees);
			return SUCCESS;

		} catch (Exception e) {
			return ERROR;
		}
	}

	public String experienceAdd() {
		try {

			ServletActionContext.getRequest().setAttribute("rowid", id);
			ServletActionContext.getRequest().setAttribute("AddEditFlag",
					addEditFlag);

			return SUCCESS;

		} catch (Exception e) {
			return ERROR;
		}
	}

	public String skillAdd() {
		try {

			List<LookupDetail> skills = personBL.getLookupMasterBL()
					.getActiveLookupDetails("SKILLS", true);
			ServletActionContext.getRequest().setAttribute("rowid", id);
			ServletActionContext.getRequest().setAttribute("AddEditFlag",
					addEditFlag);
			ServletActionContext.getRequest().setAttribute("SKILLS", skills);
			return SUCCESS;

		} catch (Exception e) {
			return ERROR;
		}
	}

	public String savePersonIdentity() {

		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			identity = new Identity();
			List<Identity> identitys = null;
			Person person = new Person();
			if (personId != 0) {
				person.setPersonId(Long.parseLong(personId + ""));
				identity.setPerson(person);
			}
			if (identityId > 0)
				identity.setIdentityId(identityId);
			identity.setIdentityType(identityType);
			List<LookupDetail> lookupDetails = (List<LookupDetail>) session
					.getAttribute("PERSON_IDENTITY_TYPE_"
							+ sessionObj.get("jSessionId"));
			for (LookupDetail lookupDetail : lookupDetails) {
				if (lookupDetail.getDataId().equals(identityType))
					identity.setLookupDetail(lookupDetail);
			}
			identity.setIdentityNumber(identityNumber);
			identity.setIssuedPlace(issuedPlace);
			if (expireDate != null && !expireDate.equals(""))
				identity.setExpireDate(DateFormat
						.convertStringToDate(expireDate));
			identity.setDescription(description);
			identity.setIsActive(true);

			if (session.getAttribute("PERSON_IDENTITY_"
					+ sessionObj.get("jSessionId")) == null) {
				identitys = new ArrayList<Identity>();
			} else {
				identitys = (List<Identity>) session
						.getAttribute("PERSON_IDENTITY_"
								+ sessionObj.get("jSessionId"));
			}
			if (addEditFlag != null && addEditFlag.equals("A")) {
				LOGGER.info("Add Process");
				identitys.add(identity);
			} else if (addEditFlag.equals("E")) {
				LOGGER.info("Edit Process");
				identitys.set(Integer.parseInt(id) - 1, identity);
			} else if (addEditFlag.equals("D")) {
				LOGGER.info("Delete Process");
				identitys.remove(Integer.parseInt(id) - 1);
			}
			session.setAttribute(
					"PERSON_IDENTITY_" + sessionObj.get("jSessionId"),
					identitys);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg", "Error");
			return ERROR;
		}
	}

	public String savePersonDependent() {

		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			dependent = new Dependent();
			Person person = new Person();
			if (personId != 0) {
				person.setPersonId(Long.parseLong(personId + ""));
				dependent.setPerson(person);
			}
			if (dependentId > 0)
				dependent.setDependentId(dependentId);
			dependent.setName(dependentName);
			dependent.setRelationship(relationshipId);
			if (dependentMobile != null && !dependentMobile.trim().equals(""))
				dependent.setMobile(Long.parseLong(dependentMobile));
			if (dependentPhone != null && !dependentPhone.trim().equals(""))
				dependent.setTelephone(Long.parseLong(dependentPhone));
			dependent.setEmail(dependentMail);
			dependent.setAddress(dependentAddress);
			dependent.setDescription(dependentDescription);
			dependent.setEmergency(emergency);
			dependent.setDependent(dependentflag);
			dependent.setBenefit(benefit);

			if (session.getAttribute("PERSON_DEPENDENT_"
					+ sessionObj.get("jSessionId")) == null) {
				dependents = new ArrayList<Dependent>();
			} else {
				dependents = (List<Dependent>) session
						.getAttribute("PERSON_DEPENDENT_"
								+ sessionObj.get("jSessionId"));
			}
			if (addEditFlag != null && addEditFlag.equals("A")) {
				LOGGER.info("Add Process");
				dependents.add(dependent);
			} else if (addEditFlag.equals("E")) {
				LOGGER.info("Edit Process");
				dependents.set(Integer.parseInt(id) - 1, dependent);
			} else if (addEditFlag.equals("D")) {
				LOGGER.info("Delete Process");
				dependents.remove(Integer.parseInt(id) - 1);
			}
			session.setAttribute(
					"PERSON_DEPENDENT_" + sessionObj.get("jSessionId"),
					dependents);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg", "Error");
			return ERROR;
		}
	}

	public String saveQualification() {

		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			if (personId != 0) {
				person.setPersonId(Long.parseLong(personId + ""));
				qualification.setPerson(person);
			}
			qualification.setImplementation(getImplementation());
			if (completionYear != null && !completionYear.equals(""))
				qualification.setCompletionYear(DateFormat
						.convertStringToDate(completionYear));

			if (session.getAttribute("QUALIFICATION_LIST_"
					+ sessionObj.get("jSessionId")) == null) {
				qualifications = new ArrayList<AcademicQualifications>();
			} else {
				qualifications = (List<AcademicQualifications>) session
						.getAttribute("QUALIFICATION_LIST_"
								+ sessionObj.get("jSessionId"));
			}
			if (addEditFlag != null && addEditFlag.equals("A")) {
				LOGGER.info("Add Process");
				qualifications.add(qualification);
			} else if (addEditFlag.equals("E")) {
				LOGGER.info("Edit Process");
				qualifications.set(Integer.parseInt(id) - 1, qualification);
			} else if (addEditFlag.equals("D")) {
				LOGGER.info("Delete Process");
				qualifications.remove(Integer.parseInt(id) - 1);
			}
			session.setAttribute(
					"QUALIFICATION_LIST_" + sessionObj.get("jSessionId"),
					qualifications);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg", "Error");
			return ERROR;
		}
	}

	public String saveExperience() {

		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			Person person = new Person();
			if (personId != 0) {
				person.setPersonId(Long.parseLong(personId + ""));
				experience.setPerson(person);
			}

			if (startDate != null && !startDate.equals(""))
				experience.setStartDate(DateFormat
						.convertStringToDate(startDate));

			if (endDate != null && !endDate.equals(""))
				experience.setEndDate(DateFormat.convertStringToDate(endDate));

			if (session.getAttribute("EXPERIENCE_LIST_"
					+ sessionObj.get("jSessionId")) == null) {
				experiences = new ArrayList<Experiences>();
			} else {
				experiences = (List<Experiences>) session
						.getAttribute("EXPERIENCE_LIST_"
								+ sessionObj.get("jSessionId"));
			}
			if (addEditFlag != null && addEditFlag.equals("A")) {
				LOGGER.info("Add Process");
				experiences.add(experience);
			} else if (addEditFlag.equals("E")) {
				LOGGER.info("Edit Process");
				experiences.set(Integer.parseInt(id) - 1, experience);
			} else if (addEditFlag.equals("D")) {
				LOGGER.info("Delete Process");
				experiences.remove(Integer.parseInt(id) - 1);
			}
			session.setAttribute(
					"EXPERIENCE_LIST_" + sessionObj.get("jSessionId"),
					experiences);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg", "Error");
			return ERROR;
		}
	}

	public String saveSkill() {

		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			Person person = new Person();
			if (personId != 0) {
				person.setPersonId(Long.parseLong(personId + ""));
				skill.setPerson(person);
			}

			if (session.getAttribute("SKILL_LIST_"
					+ sessionObj.get("jSessionId")) == null) {
				skills = new ArrayList<Skills>();
			} else {
				skills = (List<Skills>) session.getAttribute("SKILL_LIST_"
						+ sessionObj.get("jSessionId"));
			}
			if (addEditFlag != null && addEditFlag.equals("A")) {
				LOGGER.info("Add Process");
				skills.add(skill);
			} else if (addEditFlag.equals("E")) {
				LOGGER.info("Edit Process");
				skills.set(Integer.parseInt(id) - 1, skill);
			} else if (addEditFlag.equals("D")) {
				LOGGER.info("Delete Process");
				skills.remove(Integer.parseInt(id) - 1);
			}
			session.setAttribute("SKILL_LIST_" + sessionObj.get("jSessionId"),
					skills);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg", "Error");
			return ERROR;
		}
	}

	public String saveProfilePicture() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		try {

			byte[] bFile = null;

			if (fileUpload != null) {
				FileInputStream fileInputStream = null;
				bFile = new byte[(int) fileUpload.length()];
				try {
					// convert file into array of bytes
					fileInputStream = new FileInputStream(fileUpload);
					fileInputStream.read(bFile);
					fileInputStream.close();

					System.out.println("Done");
				} catch (Exception e) {
					e.printStackTrace();
				}

			} else {
				if (session.getAttribute("ProfilePicture_"
						+ sessionObj.get("jSessionId")) != null)
					;
				bFile = (byte[]) session.getAttribute("ProfilePicture_"
						+ sessionObj.get("jSessionId"));
			}
			session.setAttribute(
					"ProfilePicture_" + sessionObj.get("jSessionId"), bFile);

			// filePath = fileUpload.getPath();

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception", e);
			return ERROR;

		}
	}

	public String moveProfilePictureIntoDrive() {
		try {
			personBL.moveProfilePicToDrive();
			returnMessage = "SUCCESS";
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			returnMessage = "FAILURE";
			LOGGER.error("Exception", e);
			return ERROR;
		}
	}

	// Get Person Combination Details
	public String getPersonCombination() {
		try {
			personObject = personBL.getPersonDetails(personId);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception", e);
			return ERROR;
		}
	}

	public String validatePersonSecretPin() {
		try {
			getImplementId();
			posPersonId = null;
			Person person = personBL.getPersonService().getPersonBySecretPin(
					implementation, employeeId, employeeSecretPin);
			if (null != person) {
				returnMessage = "SUCCESS";
				posPersonId = String.valueOf(person.getPersonId());
			} else
				returnMessage = "Invalid Employee Id or PIN Number.";
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Invalid Employee Id or PIN Number.";
			ex.printStackTrace();
			LOGGER.error("Exception", ex);
			return ERROR;
		}
	}

	// ********************************************** Report Section
	// ****************************

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public int getRecId() {
		return recId;
	}

	public void setRecId(int recId) {
		this.recId = recId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getPersonTypes() {
		return personTypes;
	}

	public void setPersonTypes(String personTypes) {
		this.personTypes = personTypes;
	}

	public String getEmirateId() {
		return emirateId;
	}

	public void setEmirateId(String emirateId) {
		this.emirateId = emirateId;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public String getEffectiveDatesFrom() {
		return effectiveDatesFrom;
	}

	public void setEffectiveDatesFrom(String effectiveDatesFrom) {
		this.effectiveDatesFrom = effectiveDatesFrom;
	}

	public String getEffectiveDatesTo() {
		return effectiveDatesTo;
	}

	public void setEffectiveDatesTo(String effectiveDatesTo) {
		this.effectiveDatesTo = effectiveDatesTo;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getTownBirth() {
		return townBirth;
	}

	public void setTownBirth(String townBirth) {
		this.townBirth = townBirth;
	}

	public String getRegionBirth() {
		return regionBirth;
	}

	public void setRegionBirth(String regionBirth) {
		this.regionBirth = regionBirth;
	}

	public String getCountryBirth() {
		return countryBirth;
	}

	public void setCountryBirth(String countryBirth) {
		this.countryBirth = countryBirth;
	}

	public Character getGender() {
		return gender;
	}

	public void setGender(Character gender) {
		this.gender = gender;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public Character getMartialStatus() {
		return maritalStatus;
	}

	public void setMartialStatus(Character martialStatus) {
		this.maritalStatus = martialStatus;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public int getDisabled() {
		return disabled;
	}

	public void setDisabled(int disabled) {
		this.disabled = disabled;
	}

	public String getImageUpload() {
		return imageUpload;
	}

	public void setImageUpload(String imageUpload) {
		this.imageUpload = imageUpload;
	}

	public String getOffice() {
		return office;
	}

	public void setOffice(String office) {
		this.office = office;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMailStop() {
		return mailStop;
	}

	public void setMailStop(String mailStop) {
		this.mailStop = mailStop;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getMailTo() {
		return mailTo;
	}

	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}

	public int getExists() {
		return exists;
	}

	public void setExists(int exists) {
		this.exists = exists;
	}

	public String getHoldApplicationuntil() {
		return holdApplicationUntil;
	}

	public void setHoldApplicationuntil(String holdApplicationUntil) {
		this.holdApplicationUntil = holdApplicationUntil;
	}

	public String getLastUpdatedResume() {
		return lastUpdatedResume;
	}

	public void setLastUpdatedResume(String lastUpdatedResume) {
		this.lastUpdatedResume = lastUpdatedResume;
	}

	public String getHonors() {
		return honors;
	}

	public void setHonors(String honors) {
		this.honors = honors;
	}

	public String getPreferredName() {
		return preferredName;
	}

	public void setPreferredName(String preferredName) {
		this.preferredName = preferredName;
	}

	public String getPreviousLastName() {
		return previousLastName;
	}

	public void setPreviousLastName(String previousLastName) {
		this.previousLastName = previousLastName;
	}

	public String getAvailabilitySchedule() {
		return availabilitySchedules;
	}

	public void setAvailabilitySchedule(String availabilitySchedules) {
		this.availabilitySchedules = availabilitySchedules;
	}

	public Integer getFullTimeAvailability() {
		return fullTimeAvailability;
	}

	public void setFullTimeAvailability(Integer fullTimeAvailability) {
		this.fullTimeAvailability = fullTimeAvailability;
	}

	public Character getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(Character maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getHoldApplicationUntil() {
		return holdApplicationUntil;
	}

	public void setHoldApplicationUntil(String holdApplicationUntil) {
		this.holdApplicationUntil = holdApplicationUntil;
	}

	public String getAvailabilitySchedules() {
		return availabilitySchedules;
	}

	public void setAvailabilitySchedules(String availabilitySchedules) {
		this.availabilitySchedules = availabilitySchedules;
	}

	public String getCorrespondenceLanguage() {
		return correspondenceLanguage;
	}

	public void setCorrespondenceLanguage(String correspondenceLanguage) {
		this.correspondenceLanguage = correspondenceLanguage;
	}

	public String getDeathDate() {
		return deathDate;
	}

	public void setDeathDate(String deathDate) {
		this.deathDate = deathDate;
	}

	public String getStudentStatus() {
		return studentStatus;
	}

	public void setStudentStatus(String studentStatus) {
		this.studentStatus = studentStatus;
	}

	public String getDateLastVerified() {
		return dateLastVerified;
	}

	public void setDateLastVerified(String dateLastVerified) {
		this.dateLastVerified = dateLastVerified;
	}

	public int getMilitary() {
		return military;
	}

	public void setMilitary(int military) {
		this.military = military;
	}

	public int getSecondPassport() {
		return secondPassport;
	}

	public void setSecondPassport(int secondPassport) {
		this.secondPassport = secondPassport;
	}

	public String getLatestStartDate() {
		return latestStartDate;
	}

	public void setLatestStartDate(String latestStartDate) {
		this.latestStartDate = latestStartDate;
	}

	public String getJsonResult() {
		return jsonResult;
	}

	public void setJsonResult(String jsonResult) {
		this.jsonResult = jsonResult;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public int getSessionId() {
		return sessionId;
	}

	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}

	public String getExists1() {
		return exists1;
	}

	public void setExists1(String exists1) {
		this.exists1 = exists1;
	}

	public int getFlagnew() {
		return flagnew;
	}

	public void setFlagnew(int flagnew) {
		this.flagnew = flagnew;
	}

	public int getPersonFlag() {
		return personFlag;
	}

	public void setPersonFlag(int personFlag) {
		this.personFlag = personFlag;
	}

	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(String workflowId) {
		this.workflowId = workflowId;
	}

	public String getWorkflowName() {
		return workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public String getWorkflowStatus() {
		return workflowStatus;
	}

	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getNotifyFunctionId() {
		return notifyFunctionId;
	}

	public void setNotifyFunctionId(String notifyFunctionId) {
		this.notifyFunctionId = notifyFunctionId;
	}

	public String getFunctionId() {
		return functionId;
	}

	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}

	public String getWfFunctionType() {
		return wfFunctionType;
	}

	public void setWfFunctionType(String wfFunctionType) {
		this.wfFunctionType = wfFunctionType;
	}

	public String getWorkflowParam() {
		return workflowParam;
	}

	public void setWorkflowParam(String workflowParam) {
		this.workflowParam = workflowParam;
	}

	public String getMappingId() {
		return mappingId;
	}

	public void setMappingId(String mappingId) {
		this.mappingId = mappingId;
	}

	public String getAttribute1() {
		return attribute1;
	}

	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}

	public String getAttribute2() {
		return attribute2;
	}

	public void setAttribute2(String attribute2) {
		this.attribute2 = attribute2;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getApplicantId() {
		return applicantId;
	}

	public void setApplicantId(String applicantId) {
		this.applicantId = applicantId;
	}

	public int getSessionPersonId() {
		return sessionPersonId;
	}

	public void setSessionPersonId(int sessionPersonId) {
		this.sessionPersonId = sessionPersonId;
	}

	public String getNationalityTypeName() {
		return nationalityTypeName;
	}

	public void setNationalityTypeName(String nationalityTypeName) {
		this.nationalityTypeName = nationalityTypeName;
	}

	public void setNationalityTypeId(String nationalityTypeId) {
		this.nationalityTypeId = nationalityTypeId;
	}

	public String getNationalityTypeId() {
		return nationalityTypeId;
	}

	public String getDmsTrnId() {
		return dmsTrnId;
	}

	public void setDmsTrnId(String dmsTrnId) {
		this.dmsTrnId = dmsTrnId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getExpirationdate() {
		return expirationdate;
	}

	public void setExpirationdate(String expirationdate) {
		this.expirationdate = expirationdate;
	}

	public String getIdentificationId() {
		return identificationId;
	}

	public void setIdentificationId(String identificationId) {
		this.identificationId = identificationId;
	}

	public String getIdentificationDesc() {
		return identificationDesc;
	}

	public void setIdentificationDesc(String identificationDesc) {
		this.identificationDesc = identificationDesc;
	}

	public String getIdentificationExpDate() {
		return identificationExpDate;
	}

	public void setIdentificationExpDate(String identificationExpDate) {
		this.identificationExpDate = identificationExpDate;
	}

	public Character getPersonGroup() {
		return personGroup;
	}

	public void setPersonGroup(Character personGroup) {
		this.personGroup = personGroup;
	}

	public char getNationalityChar() {
		return nationalityChar;
	}

	public void setNationalityChar(Character nationalityChar) {
		this.nationalityChar = nationalityChar;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public PersonBL getPersonBL() {
		return personBL;
	}

	public void setPersonBL(PersonBL personBL) {
		this.personBL = personBL;
	}

	public String getAddEditFlag() {
		return addEditFlag;
	}

	public void setAddEditFlag(String addEditFlag) {
		this.addEditFlag = addEditFlag;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public long getPersonAddressId() {
		return personAddressId;
	}

	public void setPersonAddressId(long personAddressId) {
		this.personAddressId = personAddressId;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public long getIdentityId() {
		return identityId;
	}

	public void setIdentityId(long identityId) {
		this.identityId = identityId;
	}

	public int getIdentityType() {
		return identityType;
	}

	public void setIdentityType(int identityType) {
		this.identityType = identityType;
	}

	public String getIdentityNumber() {
		return identityNumber;
	}

	public void setIdentityNumber(String identityNumber) {
		this.identityNumber = identityNumber;
	}

	public String getIssuedPlace() {
		return issuedPlace;
	}

	public void setIssuedPlace(String issuedPlace) {
		this.issuedPlace = issuedPlace;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public long getDependentId() {
		return dependentId;
	}

	public void setDependentId(long dependentId) {
		this.dependentId = dependentId;
	}

	public int getRelationshipId() {
		return relationshipId;
	}

	public void setRelationshipId(int relationshipId) {
		this.relationshipId = relationshipId;
	}

	public String getDependentName() {
		return dependentName;
	}

	public void setDependentName(String dependentName) {
		this.dependentName = dependentName;
	}

	public String getDependentMobile() {
		return dependentMobile;
	}

	public void setDependentMobile(String dependentMobile) {
		this.dependentMobile = dependentMobile;
	}

	public String getDependentPhone() {
		return dependentPhone;
	}

	public void setDependentPhone(String dependentPhone) {
		this.dependentPhone = dependentPhone;
	}

	public String getDependentMail() {
		return dependentMail;
	}

	public void setDependentMail(String dependentMail) {
		this.dependentMail = dependentMail;
	}

	public boolean isEmergency() {
		return emergency;
	}

	public void setEmergency(boolean emergency) {
		this.emergency = emergency;
	}

	public boolean isDependentflag() {
		return dependentflag;
	}

	public void setDependentflag(boolean dependentflag) {
		this.dependentflag = dependentflag;
	}

	public boolean isBenefit() {
		return benefit;
	}

	public void setBenefit(boolean benefit) {
		this.benefit = benefit;
	}

	public String getDependentAddress() {
		return dependentAddress;
	}

	public void setDependentAddress(String dependentAddress) {
		this.dependentAddress = dependentAddress;
	}

	public String getDependentDescription() {
		return dependentDescription;
	}

	public void setDependentDescription(String dependentDescription) {
		this.dependentDescription = dependentDescription;
	}

	public String getResidentialAddress() {
		return residentialAddress;
	}

	public void setResidentialAddress(String residentialAddress) {
		this.residentialAddress = residentialAddress;
	}

	public String getPostBoxNumber() {
		return postBoxNumber;
	}

	public void setPostBoxNumber(String postBoxNumber) {
		this.postBoxNumber = postBoxNumber;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getAlternateMobile() {
		return alternateMobile;
	}

	public void setAlternateMobile(String alternateMobile) {
		this.alternateMobile = alternateMobile;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getAlternateTelephone() {
		return alternateTelephone;
	}

	public void setAlternateTelephone(String alternateTelephone) {
		this.alternateTelephone = alternateTelephone;
	}

	public String getTradeLicenseNumber() {
		return tradeLicenseNumber;
	}

	public void setTradeLicenseNumber(String tradeLicenseNumber) {
		this.tradeLicenseNumber = tradeLicenseNumber;
	}

	public String getTradeIssuedPlace() {
		return tradeIssuedPlace;
	}

	public void setTradeIssuedPlace(String tradeIssuedPlace) {
		this.tradeIssuedPlace = tradeIssuedPlace;
	}

	public String getTradeExpireDate() {
		return tradeExpireDate;
	}

	public void setTradeExpireDate(String tradeExpireDate) {
		this.tradeExpireDate = tradeExpireDate;
	}

	public String getTradeDescription() {
		return tradeDescription;
	}

	public void setTradeDescription(String tradeDescription) {
		this.tradeDescription = tradeDescription;
	}

	public Long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(Long combinationId) {
		this.combinationId = combinationId;
	}

	public GLChartOfAccountService getAccountService() {
		return accountService;
	}

	public void setAccountService(GLChartOfAccountService accountService) {
		this.accountService = accountService;
	}

	public GLCombinationService getCombinationService() {
		return combinationService;
	}

	public void setCombinationService(GLCombinationService combinationService) {
		this.combinationService = combinationService;
	}

	public GLCombinationAction getCombinationAction() {
		return combinationAction;
	}

	public void setCombinationAction(GLCombinationAction combinationAction) {
		this.combinationAction = combinationAction;
	}

	public String getFirstNameArabic() {
		return firstNameArabic;
	}

	public String getLastNameArabic() {
		return lastNameArabic;
	}

	public String getMiddleNameArabic() {
		return middleNameArabic;
	}

	public void setFirstNameArabic(String firstNameArabic) {
		this.firstNameArabic = firstNameArabic;
	}

	public void setLastNameArabic(String lastNameArabic) {
		this.lastNameArabic = lastNameArabic;
	}

	public void setMiddleNameArabic(String middleNameArabic) {
		this.middleNameArabic = middleNameArabic;
	}

	public CommonBL getCommonBL() {
		return commonBL;
	}

	public void setCommonBL(CommonBL commonBL) {
		this.commonBL = commonBL;
	}

	public Boolean getIsSupplier() {
		return isSupplier;
	}

	public void setIsSupplier(Boolean isSupplier) {
		this.isSupplier = isSupplier;
	}

	public Byte getSupplierAcType() {
		return supplierAcType;
	}

	public void setSupplierAcType(Byte supplierAcType) {
		this.supplierAcType = supplierAcType;
	}

	public Byte getAcClassification() {
		return acClassification;
	}

	public void setAcClassification(Byte acClassification) {
		this.acClassification = acClassification;
	}

	public long getPaymentTermId() {
		return paymentTermId;
	}

	public void setPaymentTermId(long paymentTermId) {
		this.paymentTermId = paymentTermId;
	}

	public Double getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(Double creditLimit) {
		this.creditLimit = creditLimit;
	}

	public Double getOpeningBalance() {
		return openingBalance;
	}

	public void setOpeningBalance(Double openingBalance) {
		this.openingBalance = openingBalance;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getIbanNumber() {
		return ibanNumber;
	}

	public void setIbanNumber(String ibanNumber) {
		this.ibanNumber = ibanNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Integer getSupplierNumber() {
		return supplierNumber;
	}

	public void setSupplierNumber(Integer supplierNumber) {
		this.supplierNumber = supplierNumber;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public List<HrPersonalDetailsTO> getPersonalDetailsList() {
		return personalDetailsList;
	}

	public void setPersonalDetailsList(
			List<HrPersonalDetailsTO> personalDetailsList) {
		this.personalDetailsList = personalDetailsList;
	}

	public Identity getIdentity() {
		return identity;
	}

	public void setIdentity(Identity identity) {
		this.identity = identity;
	}

	public List<Identity> getIdentitys() {
		return identitys;
	}

	public void setIdentitys(List<Identity> identitys) {
		this.identitys = identitys;
	}

	public Dependent getDependent() {
		return dependent;
	}

	public void setDependent(Dependent dependent) {
		this.dependent = dependent;
	}

	public List<Dependent> getDependents() {
		return dependents;
	}

	public void setDependents(List<Dependent> dependents) {
		this.dependents = dependents;
	}

	public SupplierBL getSupplierBL() {
		return supplierBL;
	}

	public void setSupplierBL(SupplierBL supplierBL) {
		this.supplierBL = supplierBL;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public Boolean getIsCustomer() {
		return isCustomer;
	}

	public void setIsCustomer(Boolean isCustomer) {
		this.isCustomer = isCustomer;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public Integer getCustomerAcType() {
		return customerAcType;
	}

	public void setCustomerAcType(Integer customerAcType) {
		this.customerAcType = customerAcType;
	}

	public Long getCreditTermId() {
		return creditTermId;
	}

	public void setCreditTermId(Long creditTermId) {
		this.creditTermId = creditTermId;
	}

	public Long getSaleRepresentativeId() {
		return saleRepresentativeId;
	}

	public void setSaleRepresentativeId(Long saleRepresentativeId) {
		this.saleRepresentativeId = saleRepresentativeId;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public Long getRefferalSource() {
		return refferalSource;
	}

	public void setRefferalSource(Long refferalSource) {
		this.refferalSource = refferalSource;
	}

	public Long getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(Long shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public String getCustomerDescription() {
		return customerDescription;
	}

	public void setCustomerDescription(String customerDescription) {
		this.customerDescription = customerDescription;
	}

	public String getShippingDetails() {
		return shippingDetails;
	}

	public void setShippingDetails(String shippingDetails) {
		this.shippingDetails = shippingDetails;
	}

	public CustomerBL getCustomerBL() {
		return customerBL;
	}

	public void setCustomerBL(CustomerBL customerBL) {
		this.customerBL = customerBL;
	}

	public Long getEmployeeCombinationId() {
		return employeeCombinationId;
	}

	public void setEmployeeCombinationId(Long employeeCombinationId) {
		this.employeeCombinationId = employeeCombinationId;
	}

	public Long getTenantCombinationId() {
		return tenantCombinationId;
	}

	public void setTenantCombinationId(Long tenantCombinationId) {
		this.tenantCombinationId = tenantCombinationId;
	}

	public Long getSupplierCombinationId() {
		return supplierCombinationId;
	}

	public void setSupplierCombinationId(Long supplierCombinationId) {
		this.supplierCombinationId = supplierCombinationId;
	}

	public Long getCustomerCombinationId() {
		return customerCombinationId;
	}

	public void setCustomerCombinationId(Long customerCombinationId) {
		this.customerCombinationId = customerCombinationId;
	}

	public Long getOwnerCombinationId() {
		return ownerCombinationId;
	}

	public void setOwnerCombinationId(Long ownerCombinationId) {
		this.ownerCombinationId = ownerCombinationId;
	}

	public String getFileUploadContentType() {
		return fileUploadContentType;
	}

	public void setFileUploadContentType(String fileUploadContentType) {
		this.fileUploadContentType = fileUploadContentType;
	}

	public String getFileUploadFileName() {
		return fileUploadFileName;
	}

	public void setFileUploadFileName(String fileUploadFileName) {
		this.fileUploadFileName = fileUploadFileName;
	}

	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	public File getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(File fileUpload) {
		this.fileUpload = fileUpload;
	}

	public String getViewType() {
		return viewType;
	}

	public void setViewType(String viewType) {
		this.viewType = viewType;
	}

	public String getCompletionYear() {
		return completionYear;
	}

	public void setCompletionYear(String completionYear) {
		this.completionYear = completionYear;
	}

	public AcademicQualifications getQualification() {
		return qualification;
	}

	public void setQualification(AcademicQualifications qualification) {
		this.qualification = qualification;
	}

	public Experiences getExperience() {
		return experience;
	}

	public void setExperience(Experiences experience) {
		this.experience = experience;
	}

	public Skills getSkill() {
		return skill;
	}

	public void setSkill(Skills skill) {
		this.skill = skill;
	}

	public Long getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(Long candidateId) {
		this.candidateId = candidateId;
	}

	public Long getOpenPositionId() {
		return openPositionId;
	}

	public void setOpenPositionId(Long openPositionId) {
		this.openPositionId = openPositionId;
	}

	public Long getRecruitmentSourceId() {
		return recruitmentSourceId;
	}

	public void setRecruitmentSourceId(Long recruitmentSourceId) {
		this.recruitmentSourceId = recruitmentSourceId;
	}

	public Long getAdvertisingPanelId() {
		return advertisingPanelId;
	}

	public void setAdvertisingPanelId(Long advertisingPanelId) {
		this.advertisingPanelId = advertisingPanelId;
	}

	public String getCandidateStatus() {
		return candidateStatus;
	}

	public void setCandidateStatus(String candidateStatus) {
		this.candidateStatus = candidateStatus;
	}

	public Long getCandidateAvailability() {
		return candidateAvailability;
	}

	public void setCandidateAvailability(Long candidateAvailability) {
		this.candidateAvailability = candidateAvailability;
	}

	public String getCandidateDescription() {
		return candidateDescription;
	}

	public void setCandidateDescription(String candidateDescription) {
		this.candidateDescription = candidateDescription;
	}

	public String getExpectedJoiningDate() {
		return expectedJoiningDate;
	}

	public void setExpectedJoiningDate(String expectedJoiningDate) {
		this.expectedJoiningDate = expectedJoiningDate;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public List<PersonVO> getPersonDS() {
		return personDS;
	}

	public void setPersonDS(List<PersonVO> personDS) {
		this.personDS = personDS;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public byte getMemberCarType() {
		return memberCarType;
	}

	public void setMemberCarType(byte memberCarType) {
		this.memberCarType = memberCarType;
	}

	public int getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(int cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Object getPersonObject() {
		return personObject;
	}

	public void setPersonObject(Object personObject) {
		this.personObject = personObject;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeSecretPin() {
		return employeeSecretPin;
	}

	public void setEmployeeSecretPin(String employeeSecretPin) {
		this.employeeSecretPin = employeeSecretPin;
	}

	public String getPosPersonId() {
		return posPersonId;
	}

	public void setPosPersonId(String posPersonId) {
		this.posPersonId = posPersonId;
	}

	public NotifyEnterpriseService getNotifyEnterpriseService() {
		return notifyEnterpriseService;
	}

	public void setNotifyEnterpriseService(
			NotifyEnterpriseService notifyEnterpriseService) {
		this.notifyEnterpriseService = notifyEnterpriseService;
	}
}
