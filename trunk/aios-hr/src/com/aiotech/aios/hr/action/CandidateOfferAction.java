package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Candidate;
import com.aiotech.aios.hr.domain.entity.CandidateOffer;
import com.aiotech.aios.hr.domain.entity.InterviewProcess;
import com.aiotech.aios.hr.domain.entity.Job;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.CandidateVO;
import com.aiotech.aios.hr.domain.entity.vo.InterviewVO;
import com.aiotech.aios.hr.service.bl.CandidateOfferBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class CandidateOfferAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(CandidateOfferAction.class);
	private Implementation implementation;
	private CandidateOfferBL candidateOfferBL;
	private List<Object> aaData;
	private Long candidateOfferId;
	private Long jobId;
	private CandidateOffer candidateOffer;
	private String comments;
	private String expectedJoiningDate;
	private Boolean isActive;
	private Long recordId = null;
	private String status;

	public String returnSuccess() {

		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String getCandidateOfferList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();
			List<CandidateOffer> candidateOffers = candidateOfferBL
					.getCandidateOfferService().getCandidateOfferByStatus(
							getImplementation(),
							(byte) Constants.HR.InterviewStatus.ShortListed
									.getCode());

			if (candidateOffers != null && candidateOffers.size() > 0) {
				List<CandidateVO> candidateVOs = new ArrayList<CandidateVO>();
				Map<Integer, String> rattings = candidateOfferBL
						.getInterviewBL().rattings();
				Map<Integer, String> statusTypes = candidateOfferBL
						.getCandidateBL().statusType();
				CandidateVO candidateVO = null;
				for (CandidateOffer candidateOffer : candidateOffers) {
					Candidate list = candidateOffer.getCandidate();
					candidateVO = new CandidateVO();
					candidateVO.setCandidateOfferId(candidateOffer
							.getCandidateOfferId());
					candidateVO.setCandidateId(list.getCandidateId());

					candidateVO.setPositionReferenceNumber(list
							.getOpenPosition().getPositionReferenceNumber());
					candidateVO.setCandidateName(list.getPersonByPersonId()
							.getFirstName()
							+ " "
							+ list.getPersonByPersonId().getLastName());

					candidateVO.setStatusDisplay(statusTypes.get(Integer
							.valueOf(list.getStatus())));
					InterviewVO interViewVo = candidateOfferBL.getInterviewBL()
							.calculateScoreAndRating(
									new ArrayList<InterviewProcess>(
											list.getInterviewProcesses()),
									new InterviewVO());
					if (interViewVo != null) {
						candidateVO.setRating(rattings.get(Integer
								.valueOf(interViewVo.getAverateRateing())));
						candidateVO.setRatingPoints(Integer.valueOf(interViewVo
								.getAverateRateing()));
						candidateVO.setScore(interViewVo.getTotalScore());
					}

					candidateVOs.add(candidateVO);

				}

				Collections.sort(candidateVOs, new Comparator<CandidateVO>() {
					public int compare(CandidateVO m1, CandidateVO m2) {
						return m2.getRatingPoints().compareTo(
								m1.getRatingPoints());
					}
				});

				aaData.addAll(candidateVOs);
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String getCandidateOfferFinaliseList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();
			List<CandidateOffer> candidateOffers = candidateOfferBL
					.getCandidateOfferService().getCandidateOfferByStatuses(
							getImplementation(),
							(byte) Constants.HR.InterviewStatus.Offered
									.getCode(),
							(byte) Constants.HR.InterviewStatus.OfferAccepted
									.getCode());

			if (candidateOffers != null && candidateOffers.size() > 0) {
				List<CandidateVO> candidateVOs = new ArrayList<CandidateVO>();
				Map<Integer, String> rattings = candidateOfferBL
						.getInterviewBL().rattings();
				Map<Integer, String> statusTypes = candidateOfferBL
						.getCandidateBL().statusType();
				CandidateVO candidateVO = null;
				for (CandidateOffer candidateOffer : candidateOffers) {
					Candidate list = candidateOffer.getCandidate();
					candidateVO = new CandidateVO();
					candidateVO.setCandidateOfferId(candidateOffer
							.getCandidateOfferId());
					candidateVO.setCandidateId(list.getCandidateId());

					candidateVO.setPositionReferenceNumber(list
							.getOpenPosition().getPositionReferenceNumber());
					candidateVO.setCandidateName(list.getPersonByPersonId()
							.getFirstName()
							+ " "
							+ list.getPersonByPersonId().getLastName());

					candidateVO.setStatusDisplay(statusTypes.get(Integer
							.valueOf(list.getStatus())));
					InterviewVO interViewVo = candidateOfferBL.getInterviewBL()
							.calculateScoreAndRating(
									new ArrayList<InterviewProcess>(
											list.getInterviewProcesses()),
									new InterviewVO());
					if (interViewVo != null) {
						candidateVO.setRating(rattings.get(Integer
								.valueOf(interViewVo.getAverateRateing())));
						candidateVO.setRatingPoints(Integer.valueOf(interViewVo
								.getAverateRateing()));
						candidateVO.setScore(interViewVo.getTotalScore());
					}

					candidateVOs.add(candidateVO);

				}

				Collections.sort(candidateVOs, new Comparator<CandidateVO>() {
					public int compare(CandidateVO m1, CandidateVO m2) {
						return m2.getRatingPoints().compareTo(
								m1.getRatingPoints());
					}
				});

				aaData.addAll(candidateVOs);
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getCandidateOfferAdd() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			if (recordId != null && recordId > 0)
				candidateOfferId = recordId;

			CandidateVO candidateVO = null;
			if (candidateOfferId != null && candidateOfferId > 0) {
				candidateVO = new CandidateVO();
				candidateOffer = candidateOfferBL.getCandidateOfferService()
						.getCandiateOfferById(candidateOfferId);

				// Apply the Conversion to set the Entity value into EntityVO
				candidateVO = candidateOfferBL
						.convertEntityIntoVO(candidateOffer);

			} else {
				candidateVO = new CandidateVO();
				candidateVO.setCreatedDateDisplay(DateFormat
						.convertSystemDateToString(new Date()));
			}
			Map<Integer, String> statusTypes = candidateOfferBL
					.getCandidateBL().statusType();

			List<LookupDetail> availabilities = candidateOfferBL
					.getCandidateBL().getLookupMasterBL()
					.getActiveLookupDetails("CANDIDATE_AVAILABILITY", true);

			// Comment Information --Added by rafiq
			CommentVO comment = new CommentVO();
			if (candidateOffer != null
					&& candidateOffer.getCandidateOfferId() != 0) {
				comment.setRecordId(candidateOffer.getCandidateOfferId());
				comment.setUseCase("com.aiotech.aios.hr.domain.entity.CandidateOffer");
				comment = candidateOfferBL.getCommentBL().getCommentInfo(
						comment);

			}
			ServletActionContext.getRequest().setAttribute(
					"CANDIDATE_AVAILABILITY", availabilities);
			ServletActionContext.getRequest().setAttribute("STATUS_TYPE",
					statusTypes);

			ServletActionContext.getRequest().setAttribute("CANDIDATE_OFFER",
					candidateVO);

			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}

	public String loadOfferLetter() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			if (recordId != null && recordId > 0)
				candidateOfferId = recordId;

			CandidateVO candidateVO = null;
			if (candidateOfferId != null && candidateOfferId > 0) {
				candidateVO = new CandidateVO();
				candidateOffer = candidateOfferBL.getCandidateOfferService()
						.getCandiateOfferById(candidateOfferId);
				if (jobId != null && jobId > 0) {
					Job job = new Job();
					job.setJobId(jobId);
					candidateOffer.setJob(job);
				}
				// Apply the Conversion to set the Entity value into EntityVO
				candidateVO = candidateOfferBL
						.convertEntityIntoVO(candidateOffer);

			} else {
				candidateVO = new CandidateVO();
				candidateVO.setCreatedDateDisplay(DateFormat
						.convertSystemDateToString(new Date()));
			}
			Map<Integer, String> statusTypes = candidateOfferBL
					.getCandidateBL().statusType();

			List<LookupDetail> availabilities = candidateOfferBL
					.getCandidateBL().getLookupMasterBL()
					.getActiveLookupDetails("CANDIDATE_AVAILABILITY", true);

			// Comment Information --Added by rafiq
			Comment comment = new Comment();
			if (candidateOffer != null
					&& candidateOffer.getCandidateOfferId() != 0) {
				comment.setRecordId(candidateOffer.getCandidateOfferId());
				comment.setUseCase("com.aiotech.aios.hr.domain.entity.CandidateOffer");
				comment = candidateOfferBL.getCommentBL().getCommentInfo(
						comment);

			}
			ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
					comment);

			ServletActionContext.getRequest().setAttribute(
					"CANDIDATE_AVAILABILITY", availabilities);
			ServletActionContext.getRequest().setAttribute("STATUS_TYPE",
					statusTypes);

			ServletActionContext.getRequest().setAttribute("CANDIDATE_OFFER",
					candidateVO);

			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}

	public String saveOfferLetter() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			String returnStatus = "SUCCESS";
			if (candidateOfferId != null && candidateOfferId > 0) {
				candidateOffer = candidateOfferBL.getCandidateOfferService()
						.getCandiateOfferById(candidateOfferId);
				if (jobId != null && jobId > 0) {
					Job job = new Job();
					job.setJobId(jobId);
					candidateOffer.setJob(job);
				}
				candidateOffer.setComments(comments);
			}
			returnStatus = candidateOfferBL.saveCandidateOffer(candidateOffer);
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnStatus);
			LOGGER.info("saveOfferLetter() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"saveOfferLetter Failure");
			LOGGER.error("saveOfferLetter() Info unsuccessful Return");
			return ERROR;
		}
	}

	public String finaliseOfferLetter() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			String returnStatus = "SUCCESS";
			if (candidateOfferId != null && candidateOfferId > 0) {
				candidateOffer = candidateOfferBL.getCandidateOfferService()
						.getCandiateOfferById(candidateOfferId);
				candidateOffer.setComments(comments);
				candidateOffer.setUpdatedDate(new Date());

			}
			// Update Candidate Information with status
			if (expectedJoiningDate != null
					&& !expectedJoiningDate.equalsIgnoreCase("")) {
				candidateOffer.getCandidate().setExpectedJoiningDate(
						DateFormat.convertStringToDate(expectedJoiningDate));
			}
			candidateOffer.getCandidate().setStatus(
					(status != null) ? Byte.parseByte(status) : candidateOffer
							.getCandidate().getStatus());
			// Create Job Assignment
			List<JobAssignment> jobAssignments = null;
			JobAssignment jobAssignment = new JobAssignment();
			if (candidateOffer.getJobAssignment() != null
					&& candidateOffer.getJobAssignment().getJobAssignmentId() != null) {
				jobAssignment = candidateOfferBL
						.getJobAssignmentBL()
						.getJobAssignmentService()
						.getJobAssignmentInfo(
								candidateOffer.getJobAssignment()
										.getJobAssignmentId());

				if (!isActive) {
					candidateOffer.getCandidate().setStatus(
							(byte) Constants.HR.InterviewStatus.OfferAccepted
									.getCode());
					candidateOffer.getJobAssignment().setIsActive(false);
				}
			} else {
				List<Integer> intelist = new ArrayList<Integer>();
				jobAssignments = new ArrayList<JobAssignment>();
				jobAssignments = candidateOfferBL.getJobAssignmentBL()
						.getJobAssignmentService()
						.getAllJobAssignment(getImplementation());
				if (jobAssignments != null && jobAssignments.size() > 0) {
					for (int i = 0; i < jobAssignments.size(); i++) {
						intelist.add(Integer.parseInt(jobAssignments.get(i)
								.getJobAssignmentNumber()));
					}

					int temp = Collections.max(intelist);
					temp += 1;
					jobAssignment.setJobAssignmentNumber(temp + "");
				} else {
					jobAssignment.setJobAssignmentNumber(1000 + "");
				}

				jobAssignment.setJob(candidateOffer.getJob());
				jobAssignment.setDesignation(candidateOffer.getCandidate()
						.getOpenPosition().getDesignation());
				jobAssignment.setCmpDeptLocation(candidateOffer.getCandidate()
						.getOpenPosition().getCmpDeptLocation());
				Person candidateEmp = new Person();
				candidateEmp.setPersonId(candidateOffer.getCandidate()
						.getPersonByPersonId().getPersonId());
				jobAssignment.setPerson(candidateEmp);
				jobAssignment.setIsActive(isActive);
				jobAssignment.setIsPrimary(isActive);
				jobAssignment.setEffectiveDate(candidateOffer.getCandidate()
						.getExpectedJoiningDate());
				candidateOffer.setJobAssignment(jobAssignment);
				if (isActive)
					candidateOffer.getCandidate().setStatus(
							(byte) Constants.HR.InterviewStatus.JobAssigned
									.getCode());
			}

			returnStatus = candidateOfferBL.finaliseCandidateOffer(
					candidateOffer, jobAssignments);
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnStatus);
			LOGGER.info("saveOfferLetter() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Finalization Failured");
			LOGGER.error("saveOfferLetter() Info unsuccessful Return");
			return ERROR;
		}
	}

	public String getOfferLetterApproval() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			if (recordId != null && recordId > 0)
				candidateOfferId = recordId;

			CandidateVO candidateVO = null;
			if (candidateOfferId != null && candidateOfferId > 0) {
				candidateVO = new CandidateVO();
				candidateOffer = candidateOfferBL.getCandidateOfferService()
						.getCandiateOfferById(candidateOfferId);

				// Apply the Conversion to set the Entity value into EntityVO
				candidateVO = candidateOfferBL
						.convertEntityIntoVO(candidateOffer);

			} else {
				candidateVO = new CandidateVO();
				candidateVO.setCreatedDateDisplay(DateFormat
						.convertSystemDateToString(new Date()));
			}
			Map<Integer, String> statusTypes = candidateOfferBL
					.getCandidateBL().statusType();

			List<LookupDetail> availabilities = candidateOfferBL
					.getCandidateBL().getLookupMasterBL()
					.getActiveLookupDetails("CANDIDATE_AVAILABILITY", true);

			ServletActionContext.getRequest().setAttribute(
					"CANDIDATE_AVAILABILITY", availabilities);
			ServletActionContext.getRequest().setAttribute("STATUS_TYPE",
					statusTypes);

			ServletActionContext.getRequest().setAttribute("CANDIDATE_OFFER",
					candidateVO);

			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public CandidateOfferBL getCandidateOfferBL() {
		return candidateOfferBL;
	}

	public void setCandidateOfferBL(CandidateOfferBL candidateOfferBL) {
		this.candidateOfferBL = candidateOfferBL;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Long getCandidateOfferId() {
		return candidateOfferId;
	}

	public void setCandidateOfferId(Long candidateOfferId) {
		this.candidateOfferId = candidateOfferId;
	}

	public CandidateOffer getCandidateOffer() {
		return candidateOffer;
	}

	public void setCandidateOffer(CandidateOffer candidateOffer) {
		this.candidateOffer = candidateOffer;
	}

	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getExpectedJoiningDate() {
		return expectedJoiningDate;
	}

	public void setExpectedJoiningDate(String expectedJoiningDate) {
		this.expectedJoiningDate = expectedJoiningDate;
	}
}
