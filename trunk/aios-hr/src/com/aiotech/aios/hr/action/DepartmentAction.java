package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Department;
import com.aiotech.aios.hr.domain.entity.Location;
import com.aiotech.aios.hr.domain.entity.vo.DepartmentVO;
import com.aiotech.aios.hr.service.bl.DepartmentBL;
import com.aiotech.aios.system.domain.entity.City;
import com.aiotech.aios.system.domain.entity.Country;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.State;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class DepartmentAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(DepartmentAction.class);

	private Long companyId;
	private String companyName;
	private String companyNameArabic;

	private int companyTypeId;
	private String companyTypeName;
	private String description;
	private long tradeId;
	private String tradeName;
	private String id;

	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private long departmentId;
	private long locationId;
	private String locations;
	private String locationName;
	private Long countryId;
	private Long stateId;
	private Long cityId;
	private String address;
	private String area;
	private String zone;
	private String postalCode;
	private String contactNumber;
	private String returnMessage;

	private String departmentName;
	private Long cmpDeptLocId;
	private Long parentDepartmentId;
	private Long combinationId;
	private Long recordId;
	private boolean sharing;

	// Accounts Integration

	Implementation implementation;

	DepartmentBL departmentBL;

	// JSON Listing Redirect
	public String returnSuccess() {
		ServletActionContext.getRequest().setAttribute("rowid", id);
		List<Department> departmentList;
		try {
			departmentList = departmentBL.getDepartmentService()
					.getAllDepartments(getImplementation());
			ServletActionContext.getRequest().setAttribute("DEPARTMENT_LIST",
					departmentList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getAddDepartment() {
		List<Company> companyList = new ArrayList<Company>();
		try {
			companyList = departmentBL.getCompanyService().getOnlyCompanies(
					getImplementation());
			ServletActionContext.getRequest().setAttribute("COMPANY_LIST",
					companyList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getDepartmentList() {
		List<Department> departmentList = new ArrayList<Department>();
		try {
			departmentList = departmentBL.getDepartmentService()
					.getAllDepartmentsBasedOnCompany(companyId);
			ServletActionContext.getRequest().setAttribute("DEPARTMENT_LIST",
					departmentList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String setupAddrow() {
		List<Department> departmentList = new ArrayList<Department>();
		try {
			ServletActionContext.getRequest().setAttribute("rowId", id);
			departmentList = departmentBL.getDepartmentService()
					.getAllDepartmentsBasedOnCompany(companyId);
			ServletActionContext.getRequest().setAttribute("DEPARTMENT_LIST",
					departmentList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getAddLocation() {
		List<Country> countryList = new ArrayList<Country>();
		try {
			countryList = departmentBL.getSystemService().getAllCountries();
			ServletActionContext.getRequest().setAttribute("COUNTRIES",
					countryList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String saveLocation() {
		try {
			Location location = new Location();
			CmpDeptLoc setup = new CmpDeptLoc();

			if (locationId != 0) {
				location = departmentBL.getLocationService().getLocationById(
						locationId);
				location.setLocationName(locationName);
				Country country = new Country();
				State state = new State();
				City city = new City();
				country.setCountryId(countryId);
				location.setCountry(country);
				state.setStateId(stateId);
				location.setState(state);
				city.setCityId(cityId);
				location.setCity(city);
				location.setAddress(address);
				location.setArea(area);
				location.setZone(zone);
				location.setPostalCode(postalCode);
				location.setContactNumber(contactNumber);
				location.setDescription(description);
				returnMessage = departmentBL.saveLocation(location);
			} else {
				location.setLocationName(locationName);
				Country country = new Country();
				State state = new State();
				City city = new City();
				country.setCountryId(countryId);
				location.setCountry(country);
				state.setStateId(stateId);
				location.setState(state);
				city.setCityId(cityId);
				location.setCity(city);
				location.setAddress(address);
				location.setArea(area);
				location.setZone(zone);
				location.setPostalCode(postalCode);
				location.setContactNumber(contactNumber);
				location.setDescription(description);
				location.setImplementation(getImplementation());
				Company company = null;
				if (companyId != null && companyId > 0) {
					company = new Company();
					company.setCompanyId(companyId);
					location.setCompany(company);
				}
				returnMessage = departmentBL.saveCompanySetup(company, null,
						location, setup);
			}
			if (returnMessage == null
					|| returnMessage.trim().equalsIgnoreCase(""))
				returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"System Error Contact system administrator...");
		}
		return SUCCESS;
	}

	// Save department
	public String saveDepartment() {
		try {
			Department department = new Department();
			CmpDeptLoc setup = new CmpDeptLoc();
			Location location = null;
			Company company = null;
			if (departmentId != 0 && locationId != 0 && companyId != 0) {
				department = departmentBL.getDepartmentService()
						.getDepartmentById(departmentId);
				company = new Company();
				company.setCompanyId(companyId);
				department.setCompany(company);
				location = new Location();
				location.setLocationId(locationId);
				Combination combination = new Combination();
				if (combinationId != null) {
					combination.setCombinationId(combinationId);
					setup.setCombination(combination);
				}
				returnMessage = departmentBL.saveCompanySetup(company,
						department, location, setup);
			} else if (departmentId != 0 && (locationId == 0 || companyId == 0)) {
				department = departmentBL.getDepartmentService()
						.getDepartmentById(departmentId);
				department.setDepartmentName(departmentName);
				if (parentDepartmentId != null && parentDepartmentId > 0)
					department.setParentDepartmentId(parentDepartmentId);
				department.setDescription(description);
				returnMessage = departmentBL.saveDepartment(department);
			} else {
				department.setDepartmentId(null);
				department.setDepartmentName(departmentName);
				if (parentDepartmentId != null && parentDepartmentId > 0)
					department.setParentDepartmentId(parentDepartmentId);
				department.setDescription(description);
				department.setImplementation(getImplementation());
				department.setStatus(Byte.parseByte("1"));
				if (locationId > 0) {
					location = new Location();
					location.setLocationId(locationId);
				}
				company = new Company();
				company.setCompanyId(companyId);
				department.setCompany(company);
				Combination combination = new Combination();
				if (combinationId != null) {
					combination.setCombinationId(combinationId);
					setup.setCombination(combination);
				}
				returnMessage = departmentBL.saveCompanySetup(company,
						department, location, setup);

			}

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			ServletActionContext.getRequest().setAttribute("departmentId",
					department.getDepartmentId());
			ServletActionContext.getRequest().setAttribute("cmdDeptLocId",
					setup.getCmpDeptLocId());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"System Error Contact system administrator...");
		}
		return SUCCESS;
	}

	// edit department
	public String getEditDepartment() {
		try {
			Department department = new Department();
			List<Department> departmentList = null;
			try {
				if (recordId != null && recordId > 0) {
					departmentId = recordId;
					// Comment Information --Added by rafiq
					CommentVO comment = new CommentVO();
					if (departmentId > 0) {
						comment.setRecordId(recordId);
						comment.setUseCase(AttendancePolicy.class.getName());
						comment = departmentBL.getCommentBL().getCommentInfo(
								comment);
						ServletActionContext.getRequest().setAttribute(
								"COMMENT_IFNO", comment);
					}
				}

				if (departmentId != 0) {
					department = departmentBL.getDepartmentService()
							.getDepartmentById(departmentId);
					departmentList = departmentBL.getDepartmentService()
							.getAllDepartmentsBasedOnCompany(
									department.getCompany().getCompanyId());
				}
				ServletActionContext.getRequest().setAttribute(
						"DEPARTMENT_LIST", departmentList);
				ServletActionContext.getRequest().setAttribute("DEPARTMENT",
						department);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"System Error Contact system administrator...");
		}
		return SUCCESS;
	}

	public String getDepartmentLocationApproval() {
		try {
			CmpDeptLoc departmentLocation = null;
			List<Department> departmentList = null;
			DepartmentVO departmentVO = new DepartmentVO();
			try {

				departmentLocation = departmentBL.getCmpDeptLocBL()
						.getCmpDeptLocService()
						.getBranchAndLocationInfo(recordId);
				departmentVO.setCompanyName(departmentLocation.getCompany()
						.getCompanyName());
				departmentVO.setLocationName(departmentLocation.getLocation()
						.getLocationName());
				departmentVO.setDepartmentName(departmentLocation
						.getDepartment().getDepartmentName());
				departmentVO.setAddress(departmentLocation.getLocation()
						.getAddress());
				ServletActionContext.getRequest().setAttribute("DEPARTMENT",
						departmentVO);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"System Error Contact system administrator...");
		}
		return SUCCESS;
	}

	public String getEditLocation() {
		List<Country> countryList = new ArrayList<Country>();
		Location location = null;
		try {
			if (locationId != 0) {
				location = departmentBL.getLocationService().getLocationById(
						locationId);
				countryList = departmentBL.getSystemService().getAllCountries();
			}
			ServletActionContext.getRequest().setAttribute("COUNTRIES",
					countryList);
			ServletActionContext.getRequest()
					.setAttribute("LOCATION", location);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	// Listing JSON
	public String execute() {

		try {

			List<Department> departments = departmentBL.getDepartmentService()
					.getAllDepartments(getImplementation());
			if (departments != null && departments.size() > 0) {

				iTotalRecords = departments.size();
				iTotalDisplayRecords = departments.size();
			}

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (Department list : departments) {

				JSONArray array = new JSONArray();
				array.add(list.getDepartmentId());
				array.add(list.getDepartmentName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// JSON Listing Redirect
	public String getLocationList() {
		ServletActionContext.getRequest().setAttribute("rowId", id);
		ServletActionContext.getRequest().setAttribute("companyId", companyId);
		return SUCCESS;
	}

	public String getCompanySetupListJson() {
		try {

			JSONObject jsonResponse = new JSONObject();
			List<CmpDeptLoc> cmpDeptLocations = departmentBL
					.getCmpDeptLocationService().getAllLocation(
							getImplementation());
			jsonResponse.put("iTotalRecords", cmpDeptLocations.size());
			jsonResponse.put("iTotalDisplayRecords", cmpDeptLocations.size());
			JSONArray data = new JSONArray();
			for (CmpDeptLoc list : cmpDeptLocations) {
				JSONArray array = new JSONArray();
				array.add(list.getCmpDeptLocId());
				array.add(list.getDepartment().getDepartmentId());
				array.add(list.getLocation().getLocationId());
				array.add(list.getCompany().getCompanyName());
				array.add(list.getDepartment().getDepartmentName());
				array.add(list.getLocation().getLocationName());
				if(list.getDataSharing()!=null && list.getDataSharing()){
					array.add(true);
				}else{
					array.add(false);
				}
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Listing JSON
	public String getLocationListJson() {

		try {

			// Call Implementation
			List<Location> locationList = null;
			if (companyId == null || companyId == 0)
				locationList = departmentBL.getLocationService()
						.getAllLocation(getImplementation());
			else
				locationList = departmentBL.getLocationService()
						.getAllLocationBasedOnCompany(companyId);
			if (locationList != null && locationList.size() > 0) {

				iTotalRecords = locationList.size();
				iTotalDisplayRecords = locationList.size();
			}

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (Location location : locationList) {
				JSONArray array = new JSONArray();
				array.add(location.getLocationId() + "");
				array.add(location.getLocationName() + "");
				String addressStr = "";

				if (location.getArea() != null)
					addressStr += " Area :" + location.getArea();
				if (location.getZone() != null)
					addressStr += "| Zone :" + location.getZone();
				if (location.getPostalCode() != null)
					addressStr += "| Postal Code :" + location.getPostalCode();
				if (location.getAddress() != null)
					addressStr += "| Address :" + location.getAddress();

				array.add(addressStr);
				if (location.getState().getCities() != null) {
					List<City> cities = new ArrayList<City>(location.getState()
							.getCities());
					array.add(cities.get(0).getCityName() + "");
				} else {
					array.add("-NA-");
				}
				array.add(location.getState().getStateName() + "");
				array.add(location.getCountry().getCountryName() + "");
				array.add(location.getDescription() + "");
				array.add((departmentBL.getLocationBL().getLocationFullAddress(
						location.getLocationId()).getFullAddress())
						+ "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String deleteCompanySetup() {
		try {

			CmpDeptLoc companySetup = null;

			if (cmpDeptLocId != null && cmpDeptLocId > 0) {
				companySetup = new CmpDeptLoc();
				companySetup.setCmpDeptLocId(cmpDeptLocId);
			}
			returnMessage = departmentBL.deleteCompanySetup(companySetup);
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Can't Delete Information is in use already");
			return ERROR;
		}
	}

	public String deleteDepartment() {
		try {
			if (departmentId > 0) {
				returnMessage = departmentBL.deleteDepartment(departmentId);
				returnMessage = "SUCCESS";

			} else {
				returnMessage = "No information is selected to delete.";
			}
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String deleteLocation() {
		try {
			if (locationId > 0) {
				returnMessage = departmentBL.deleteLocation(locationId);
				returnMessage = "SUCCESS";

			} else {
				returnMessage = "Delete Failure";
			}
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Can't Delete Information is in use already");
			return ERROR;
		}
	}

	public String updateLocationSharing() {
		try {
			if (cmpDeptLocId > 0) {
				CmpDeptLoc setup = departmentBL.getCmpDeptLocationService()
						.getCompanySetup(cmpDeptLocId);
				setup.setDataSharing(sharing);
				departmentBL.getCmpDeptLocationService().saveCmpDeptLocation(
						setup);
				returnMessage = "SUCCESS";

			} else {
				returnMessage = "Update Failure";
			}
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			returnMessage = "Update failured";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return ERROR;
		}
	}

	// JSON Listing Redirect
	public String getAllDepartment() {
		ServletActionContext.getRequest().setAttribute("rowId", id);
		return SUCCESS;
	}

	// Listing JSON
	public String getDepartmentListJson() {

		try {

			// Call Implementation
			List<Department> departmentList = null;
			departmentList = departmentBL.getDepartmentService()
					.getAllDepartmentsBasedOnCompany(companyId);
			if (departmentList != null && departmentList.size() > 0) {

				iTotalRecords = departmentList.size();
				iTotalDisplayRecords = departmentList.size();
			}

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (Department department : departmentList) {
				JSONArray array = new JSONArray();
				array.add(department.getDepartmentId() + "");
				array.add(department.getDepartmentName() + "");
				array.add(department.getParentDepartmentId() + "");
				array.add(department.getDescription() + "");
				array.add(department.getParentDepartmentId() + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getCompanyTypeId() {
		return companyTypeId;
	}

	public void setCompanyTypeId(int companyTypeId) {
		this.companyTypeId = companyTypeId;
	}

	public String getCompanyTypeName() {
		return companyTypeName;
	}

	public void setCompanyTypeName(String companyTypeName) {
		this.companyTypeName = companyTypeName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getTradeId() {
		return tradeId;
	}

	public void setTradeId(long tradeId) {
		this.tradeId = tradeId;
	}

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public String getCompanyNameArabic() {
		return companyNameArabic;
	}

	public void setCompanyNameArabic(String companyNameArabic) {
		this.companyNameArabic = companyNameArabic;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public long getLocationId() {
		return locationId;
	}

	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}

	public String getLocations() {
		return locations;
	}

	public void setLocations(String locations) {
		this.locations = locations;
	}

	public DepartmentBL getDepartmentBL() {
		return departmentBL;
	}

	public void setDepartmentBL(DepartmentBL departmentBL) {
		this.departmentBL = departmentBL;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Long getCmpDeptLocId() {
		return cmpDeptLocId;
	}

	public void setCmpDeptLocId(Long cmpDeptLocId) {
		this.cmpDeptLocId = cmpDeptLocId;
	}

	public Long getParentDepartmentId() {
		return parentDepartmentId;
	}

	public void setParentDepartmentId(Long parentDepartmentId) {
		this.parentDepartmentId = parentDepartmentId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public Long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(Long combinationId) {
		this.combinationId = combinationId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public boolean isSharing() {
		return sharing;
	}

	public void setSharing(boolean sharing) {
		this.sharing = sharing;
	}

}
