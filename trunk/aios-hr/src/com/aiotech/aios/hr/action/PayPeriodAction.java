package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.PayPeriod;
import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.PayPeriodVO;
import com.aiotech.aios.hr.service.bl.PayPeriodBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class PayPeriodAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(PayPeriodAction.class);
	private Implementation implementation;
	private PayPeriodBL payPeriodBL;
	private List<Object> aaData;
	private Long payPeriodId;
	private String startDate;
	private String endDate;
	private String isDefault;
	private String isActive;
	private String periodName;
	private Long jobAssignmentId;
	private Byte payPolicy;
	private List<PayPeriodVO> periods;
	private Long recordId;
	private String periodFallOnStart;
	private Long payPeriodTransactionId;
	private String returnMessage;
	private String isFreeze;

	public String returnSuccess() {

		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String getPayPeriodList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();
			List<PayPeriod> payPeriods = payPeriodBL.getPayPeriodService()
					.getAllPayPeriod(getImplementation());

			if (payPeriods != null && payPeriods.size() > 0) {
				PayPeriodVO payPeriodVO = null;
				for (PayPeriod list : payPeriods) {
					payPeriodVO = new PayPeriodVO();
					payPeriodVO.setPayPeriodId(list.getPayPeriodId());
					payPeriodVO.setPeriodName(list.getPeriodName());
					payPeriodVO.setStartDate(DateFormat
							.convertDateToString(list.getYearStartDate() + ""));
					payPeriodVO.setEndDate(DateFormat.convertDateToString(list
							.getYearEndDate() + ""));
					if (list.getIsDefault() != null
							&& list.getIsDefault() == true)
						payPeriodVO.setIsDefaultView("YES");
					else
						payPeriodVO.setIsDefaultView("NO");

					if (list.getIsActive() != null
							&& list.getIsActive() == true)
						payPeriodVO.setIsActiveView("Enabled");
					else
						payPeriodVO.setIsActiveView("Disabled");

					aaData.add(payPeriodVO);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String getActivePayPeriodList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			periods = new ArrayList<PayPeriodVO>();
			PayPeriodVO payPeriodVOTemp = new PayPeriodVO();
			payPeriodVOTemp.setPayPolicy(payPolicy);
			payPeriodVOTemp.setJobAssignmentId(jobAssignmentId);
			List<PayPeriodTransaction> payPeriodTransactions = payPeriodBL
					.findActivePayPeriods(payPeriodVOTemp);

			if (payPeriodTransactions != null
					&& payPeriodTransactions.size() > 0) {
				PayPeriodVO payPeriodVO = null;
				for (PayPeriodTransaction list : payPeriodTransactions) {
					payPeriodVO = new PayPeriodVO();
					payPeriodVO.setPayPeriodTransactionId(list
							.getPayPeriodTransactionId());
					payPeriodVO.setStartDate(DateFormat
							.convertDateToString(list.getStartDate() + ""));
					payPeriodVO.setEndDate(DateFormat.convertDateToString(list
							.getEndDate() + ""));
					payPeriodVO.setPayMonth(list.getPayMonth());
					periods.add(payPeriodVO);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getPayPeriodAdd() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			if (recordId != null && recordId > 0) {
				payPeriodId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (payPeriodId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(AttendancePolicy.class.getName());
					comment = payPeriodBL.getCommentBL()
							.getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}

			PayPeriodVO payPeriodVO = null;
			if (payPeriodId != null && payPeriodId > 0) {
				payPeriodVO = new PayPeriodVO();
				PayPeriod payPeriod = payPeriodBL.getPayPeriodService()
						.getPayPeriodById(payPeriodId);
				payPeriodVO = payPeriodBL.convertEntityIntoVO(payPeriod);
				// Apply the Conversion to set the Entity value into EntityVO

			}

			ServletActionContext.getRequest().setAttribute("PAY_PERIOD_INFO",
					payPeriodVO);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String savePayPeriod() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		String returnMessage = "SUCCESS";
		boolean periodFlag = false;
		try {
			if (periodFallOnStart != null
					&& periodFallOnStart.equalsIgnoreCase("true"))
				periodFlag = true;
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			PayPeriod payPeriod = new PayPeriod();
			if (payPeriodId != null && payPeriodId > 0) {
				payPeriod = payPeriodBL.getPayPeriodService().getPayPeriodById(
						payPeriodId);
				payPeriod.setUpdateDate(new Date());
				payPeriod.setPersonByUpdatedBy(person);
			} else {
				payPeriod.setCreatedDate(new Date());
				payPeriod.setPersonByCreatedBy(person);
				payPeriod.setUpdateDate(new Date());
				payPeriod.setPersonByUpdatedBy(person);
			}
			payPeriod.setPeriodName(periodName);
			payPeriod.setYearStartDate(DateFormat
					.convertStringToDate(startDate));
			payPeriod.setYearEndDate(DateFormat.convertStringToDate(endDate));
			payPeriod.setImplementation(getImplementation());
			if (isDefault != null && isDefault.equalsIgnoreCase("true"))
				payPeriod.setIsDefault(true);
			else
				payPeriod.setIsDefault(false);

			if (isActive != null && isActive.equalsIgnoreCase("true"))
				payPeriod.setIsActive(true);
			else
				payPeriod.setIsActive(false);

			// Pay Period Transactions

			List<PayPeriodTransaction> payPeriodTransactions = null;
			if (payPeriodId == null || payPeriodId == 0) {
				PayPeriodTransaction payPeriodTransaction = null;
				PayPeriodVO payPeriodVO = new PayPeriodVO();
				payPeriodVO.setStartDate(startDate);
				payPeriodVO.setEndDate(endDate);
				payPeriodVO.setIsDefault(periodFlag);
				List<PayPeriodVO> payPeriodVOs = payPeriodBL
						.generatePeriodTransaction(payPeriodVO);
				if (payPeriodVOs != null && payPeriodVOs.size() > 0) {
					payPeriodTransactions = new ArrayList<PayPeriodTransaction>();
					for (PayPeriodVO payPeriodVO2 : payPeriodVOs) {
						payPeriodTransaction = new PayPeriodTransaction();
						payPeriodTransaction.setStartDate(DateFormat
								.convertStringToDate(payPeriodVO2
										.getStartDate()));
						payPeriodTransaction
								.setEndDate(DateFormat
										.convertStringToDate(payPeriodVO2
												.getEndDate()));
						payPeriodTransaction.setPayMonth(payPeriodVO2
								.getPayMonth());
						payPeriodTransaction.setPayWeek(payPeriodVO2
								.getPayWeek());
						payPeriodTransaction.setPayPeriod(payPeriod);
						payPeriodTransaction.setRecordOrder(payPeriodVO2
								.getOrder());
						payPeriodTransaction.setIsWeek(payPeriodVO2
								.getPayMonth() != null ? false : true);
						payPeriodTransaction.setIsMonth(payPeriodVO2
								.getPayMonth() != null ? true : false);
						payPeriodTransactions.add(payPeriodTransaction);
					}
				}
				payPeriodTransaction = new PayPeriodTransaction();
				payPeriodTransaction.setStartDate(DateFormat
						.convertStringToDate(startDate));
				payPeriodTransaction.setEndDate(DateFormat
						.convertStringToDate(endDate));
				payPeriodTransaction.setIsYear(true);
				payPeriodTransaction.setPayPeriod(payPeriod);
				payPeriodTransaction.setIsWeek(false);
				payPeriodTransaction.setIsMonth(false);
				payPeriodTransaction.setRecordOrder((int) 1);
				payPeriodTransactions.add(payPeriodTransaction);

			}
			returnMessage = payPeriodBL.savePayPeriod(payPeriod,
					payPeriodTransactions);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("saveInterview() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (ArithmeticException ae) {
			ServletActionContext
					.getRequest()
					.setAttribute(
							"RETURN_MESSAGE",
							"Failure in creating the notification for interviewer! Application access must be provided to interviewers");
			return ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			if (payPeriodId != null && payPeriodId > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveInterview() Info unsuccessful Return");
			return ERROR;
		} finally {

		}
	}

	public String deletePayPeriod() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			PayPeriod payPeriod = new PayPeriod();
			String returnMessage = "SUCCESS";
			if (payPeriodId != null && payPeriodId > 0) {
				payPeriod = payPeriodBL.getPayPeriodService().getPayPeriodById(
						payPeriodId);

			}
			returnMessage = payPeriodBL.deletePayPeriod(
					payPeriod,
					new ArrayList<PayPeriodTransaction>(payPeriod
							.getPayPeriodTransactions()));

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("deletePayPeriod() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deletePayPeriod() Info unsuccessful Return");
			return ERROR;
		}
	}

	public String updatePayPeriodTransaction() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			PayPeriodTransaction payPeriod = null;
			if (payPeriodTransactionId != null && payPeriodTransactionId > 0) {
				payPeriod = payPeriodBL.getPayPeriodService()
						.getPayPeriodTransactionById(payPeriodTransactionId);
				if (isFreeze != null && isFreeze.equalsIgnoreCase("true"))
					payPeriod.setIsFreeze(true);
				else
					payPeriod.setIsFreeze(false);
				payPeriodBL.getPayPeriodService().savePayPeriodTransaction(
						payPeriod);
				LOGGER.info("updatePayPeriodTransaction() Info Save Sucessfull Return");
				returnMessage = "SUCCESS";
			}else{
				returnMessage = "Error occured!! Can not freeze the period";
			}
			
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			returnMessage = "Error occured!! Can not freeze the period";
			LOGGER.error("updatePayPeriodTransaction() Info unsuccessful Return");
			return ERROR;
		}
	}

	public String getPeriodTransactionList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			List<PayPeriodVO> payPeriodVOs = null;
			if (payPeriodId != null && payPeriodId > 0) {
				PayPeriod payPeriod = payPeriodBL.getPayPeriodService()
						.getPayPeriodById(payPeriodId);
				payPeriodVOs = payPeriodBL
						.convertTransactionEntityIntoVO(new ArrayList<PayPeriodTransaction>(
								payPeriod.getPayPeriodTransactions()));

				Collections.sort(payPeriodVOs, new Comparator<PayPeriodVO>() {
					public int compare(PayPeriodVO m1, PayPeriodVO m2) {
						return m1.getPayPeriodTransactionId().compareTo(
								m2.getPayPeriodTransactionId());
					}
				});

			} else {
				boolean periodFlag = false;
				if (periodFallOnStart != null
						&& periodFallOnStart.equalsIgnoreCase("true"))
					periodFlag = true;
				payPeriodVOs = new ArrayList<PayPeriodVO>();
				PayPeriodVO payPeriodVO = new PayPeriodVO();
				payPeriodVO.setStartDate(startDate);
				payPeriodVO.setEndDate(endDate);
				payPeriodVO.setIsDefault(periodFlag);
				payPeriodVOs = payPeriodBL
						.generatePeriodTransaction(payPeriodVO);
			}

			ServletActionContext.getRequest().setAttribute(
					"PAY_PERIOD_TRANSACTION", payPeriodVOs);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public PayPeriodBL getPayPeriodBL() {
		return payPeriodBL;
	}

	public void setPayPeriodBL(PayPeriodBL payPeriodBL) {
		this.payPeriodBL = payPeriodBL;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Long getPayPeriodId() {
		return payPeriodId;
	}

	public void setPayPeriodId(Long payPeriodId) {
		this.payPeriodId = payPeriodId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getPeriodName() {
		return periodName;
	}

	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}

	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}

	public Byte getPayPolicy() {
		return payPolicy;
	}

	public void setPayPolicy(Byte payPolicy) {
		this.payPolicy = payPolicy;
	}

	public List<PayPeriodVO> getPeriods() {
		return periods;
	}

	public void setPeriods(List<PayPeriodVO> periods) {
		this.periods = periods;
	}

	public String getPeriodFallOnStart() {
		return periodFallOnStart;
	}

	public void setPeriodFallOnStart(String periodFallOnStart) {
		this.periodFallOnStart = periodFallOnStart;
	}

	public Long getPayPeriodTransactionId() {
		return payPeriodTransactionId;
	}

	public void setPayPeriodTransactionId(Long payPeriodTransactionId) {
		this.payPeriodTransactionId = payPeriodTransactionId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getIsFreeze() {
		return isFreeze;
	}

	public void setIsFreeze(String isFreeze) {
		this.isFreeze = isFreeze;
	}

}
