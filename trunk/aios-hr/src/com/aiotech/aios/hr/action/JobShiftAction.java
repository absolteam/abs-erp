package com.aiotech.aios.hr.action;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.Job;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobShift;
import com.aiotech.aios.hr.domain.entity.WorkingShift;
import com.aiotech.aios.hr.domain.entity.vo.JobAssignmentVO;
import com.aiotech.aios.hr.domain.entity.vo.JobShiftVO;
import com.aiotech.aios.hr.service.bl.JobShiftBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class JobShiftAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(JobShiftAction.class);
	JobShiftBL jobShiftBL;
	Implementation implementation;

	private Long jobShiftId;
	private Long workingShiftId;
	private Integer id;
	private String shiftLineDetail;

	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private String returnMessage;
	private String persons;
	private String fromDate;
	private String toDate;
	private String printableContent;

	public String returnSuccess() {
		ServletActionContext.getRequest().setAttribute("rowid", id);
		ServletActionContext.getRequest().setAttribute("workingShiftId",
				workingShiftId);

		return SUCCESS;
	}

	// Listing JSON
	public String getJobShiftListJson() {

		try {
			List<WorkingShift> workingshifts = jobShiftBL.getJobBL()
					.getJobService()
					.getAllShiftWithEmployee(getImplementation());
			
			workingshifts = jobShiftBL.getWorkingShiftList(workingshifts);
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", workingshifts.size());
			jsonResponse.put("iTotalDisplayRecords", workingshifts.size());
			JSONArray data = new JSONArray();
			for (WorkingShift list : workingshifts) {

				JSONArray array = new JSONArray();
				array.add(list.getWorkingShiftId());
				array.add(list.getWorkingShiftName() + "");
				array.add(list.getStartTime() + "");
				array.add(list.getEndTime() + "");
				array.add(list.getLateAttendance() + "");
				array.add(list.getSeviorLateAttendance() + "");
				array.add(list.getBufferTime() + "");
				array.add(list.getBreakHours() + "");
				if (list.getJobShifts() != null)
					array.add(jobShiftBL.getAssignedEmployees(list) + "");
				else
					array.add("-Not Assigned-");

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getAssignShift() {
		try {
			List<WorkingShift> workingshifts = jobShiftBL.getJobBL()
					.getJobService().getAllShifts(getImplementation());
			ServletActionContext.getRequest().setAttribute(
					"WORKING_SHIFT_LIST", workingshifts);

			List<WorkingShift> tempWorkingshifts = jobShiftBL
					.getJobShiftService().getShiftWithEmployee(workingShiftId);
			tempWorkingshifts = jobShiftBL
					.getWorkingShiftList(tempWorkingshifts);
			WorkingShift workingShift = null;
			if (tempWorkingshifts != null && tempWorkingshifts.size() > 0) {
				workingShift = new WorkingShift();
				for (WorkingShift workingShift1 : tempWorkingshifts) {
					if ((long) workingShift1.getWorkingShiftId() == (long) workingShiftId) {
						workingShift = workingShift1;
						break;
					}

				}
			}
			ServletActionContext.getRequest().setAttribute("WORKING_SHIFT",
					workingShift);
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -60);
			java.util.Date cutoffDate = cal.getTime();
			List<JobShift> jobShifts = new ArrayList<JobShift>(
					workingShift.getJobShifts());
			for (JobShift jobShift : workingShift.getJobShifts()) {
				if (jobShift.getEffectiveStartDate() != null
						&& DateFormat.compareDates(cutoffDate,
								jobShift.getEffectiveStartDate()) == false
						&& DateFormat.compareDates(cutoffDate,
								jobShift.getEffectiveEndDate()) == false)
					jobShifts.remove(jobShift);
			}

			Collections.sort(jobShifts, new Comparator<JobShift>() {
				public int compare(JobShift m1, JobShift m2) {
					return m2.getJobShiftId().compareTo(m1.getJobShiftId());
				}
			});
			ServletActionContext.getRequest().setAttribute("JOB_SHIFT_LIST",
					jobShifts);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String saveAssignShiftEmployee() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			List<WorkingShift> tempWorkingshifts = jobShiftBL
					.getJobShiftService().getShiftWithEmployee(workingShiftId);
			tempWorkingshifts = jobShiftBL
					.getWorkingShiftList(tempWorkingshifts);
			returnMessage = jobShiftBL.saveJobShift(
					jobShiftBL.getShiftLineDetail(shiftLineDetail),
					tempWorkingshifts.get(0));
			if (!returnMessage.equals("SUCCESS"))
				returnMessage = "Assignment Failure";

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			returnMessage = "Assignment Failure";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return ERROR;
		}
	}

	// Listing JSON
	public String getShiftJobList() {

		try {
			List<Job> jobs = jobShiftBL.getJobBL().getJobService()
					.getShiftWithJob(getImplementation());

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", jobs.size());
			jsonResponse.put("iTotalDisplayRecords", jobs.size());
			JSONArray data = new JSONArray();
			for (Job list : jobs) {
				list = jobShiftBL.getJobBL().jobRemoveInactiveRecForShift(list);
				JSONArray array = new JSONArray();
				array.add(list.getJobId());
				array.add(list.getJobNumber() + "");
				array.add(list.getJobName() + "");
				String employeeList = "";
				for (JobAssignment jobAssignment : list.getJobAssignments()) {
					if (jobAssignment.getPerson() != null)
						employeeList += jobAssignment.getPerson()
								.getFirstName()
								+ " "
								+ jobAssignment.getPerson().getLastName()
								+ " ,";
				}
				array.add(employeeList + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String viewWorkingShiftCalendar() {
		try {
			Date fromDate1 = null;
			Date toDate1 = null;
			JobAssignment jobAssignment = null;
			if (fromDate != null && !fromDate.equals("")) {
				fromDate1 = DateFormat.convertStringToDate(fromDate);
			}

			if (toDate != null && !toDate.equals("")) {
				toDate1 = DateFormat.convertStringToDate(toDate);
			}
			List<JobShiftVO> jobShiftVos = new ArrayList<JobShiftVO>();
			List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();

			if (persons != null && !persons.equals("")) {
				String[] personIdsArr = persons.split(",");
				for (String string : personIdsArr) {
					jobAssignment = jobShiftBL.getJobAssignmentBL()
							.getJobAssignmentService()
							.getJobAssignmentInfo(Long.valueOf(string));
					jobAssignments.add(jobAssignment);
				}

			} else {
				jobAssignments = jobShiftBL.getJobAssignmentBL()
						.getJobAssignmentService()
						.getAllActiveJobAssignment(getImplementation());

			}

			jobShiftVos.addAll(jobShiftBL.getJobAssignmentBL()
					.fetchAssignmentBasedShifts(jobAssignments, fromDate1,
							toDate1));

			Configuration config = new Configuration();
			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");

			config.setDirectoryForTemplateLoading(new File(confProps
					.getProperty("file.drive")
					+ "aios/PrintTemplate/hr/"
					+ getImplementation().getImplementationId()));

			String scheme = ServletActionContext.getRequest().getScheme();
			String host = ServletActionContext.getRequest().getHeader("Host");

			// includes leading forward slash
			String contextPath = ServletActionContext.getRequest()
					.getContextPath();

			String urlPath = scheme + "://" + host + contextPath;

			Template template = config
					.getTemplate("html-working-shift-template.ftl");
			Map<String, Object> rootMap = new HashMap<String, Object>();

			if (getImplementation().getMainLogo() != null) {

				byte[] bFile = FileUtil.getBytes(new File(getImplementation()
						.getMainLogo()));
				bFile = Base64.encodeBase64(bFile);

				rootMap.put("logo", new String(bFile));
			} else {
				rootMap.put("logo", "");
			}

			rootMap.put("urlPath", urlPath);
			Map<String, JobShiftVO> dateLinkeMap = new LinkedHashMap<String, JobShiftVO>();
			LocalDate start = new LocalDate(fromDate1);
			LocalDate end = new LocalDate(toDate1);
			end = end.plusDays(1);
			JobShiftVO tempVo = null;
			while (start.isBefore(end)) {
				tempVo = new JobShiftVO();
				tempVo.setDateView(DateTimeFormat.forPattern("dd-MMM-yyyy")
						.print(start));
				tempVo.setDayName(DateTimeFormat.forPattern("EEEE").print(start));
				dateLinkeMap.put(DateTimeFormat.forPattern("dd-MMM-yyyy")
						.print(start), tempVo);
				start = start.plusDays(1);
			}
			Map<Long, JobAssignmentVO> dataMaps = new HashMap<Long, JobAssignmentVO>();
			List<JobShiftVO> newList = null;
			JobAssignmentVO jVo = null;
			for (JobShiftVO vo : jobShiftVos) {
				if (dataMaps.containsKey(vo.getJobAssignment().getPerson()
						.getPersonId())) {
					jVo = dataMaps.get(vo.getJobAssignment().getPerson()
							.getPersonId());
					newList = jVo.getJobShiftVOs();
					newList.add(vo);
					jVo.setJobShiftVOs(newList);
				} else {
					jVo = new JobAssignmentVO();
					jVo.setPersonName(vo.getEmployeeName());
					newList = new ArrayList<JobShiftVO>();
					newList.add(vo);
					jVo.setJobShiftVOs(newList);

				}
				dataMaps.put(vo.getJobAssignment().getPerson().getPersonId(),
						jVo);
			}
			
			Map<Long, JobAssignmentVO> dataMapsFinal = new HashMap<Long, JobAssignmentVO>();
			for (Map.Entry<Long, JobAssignmentVO> entry : dataMaps.entrySet())
			{
				jVo=entry.getValue();
				Collections.sort(jVo.getJobShiftVOs(), new Comparator<JobShiftVO>() {
					public int compare(JobShiftVO m1, JobShiftVO m2) {
						return m1.getDate().compareTo(m2.getDate());
					}
				});
			    dataMapsFinal.put(entry.getKey(),jVo);
			}

			rootMap.put("EMPLOYEE_SHIFTS", dataMapsFinal);
			rootMap.put("DATE_MAPS", dateLinkeMap);
			rootMap.put("fromDate", fromDate);
			rootMap.put("toDate", toDate);

			Writer out = new StringWriter();
			template.process(rootMap, out);

			printableContent = out.toString();

			return "template";

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public JobShiftBL getJobShiftBL() {
		return jobShiftBL;
	}

	public void setJobShiftBL(JobShiftBL jobShiftBL) {
		this.jobShiftBL = jobShiftBL;
	}

	public Long getJobShiftId() {
		return jobShiftId;
	}

	public void setJobShiftId(Long jobShiftId) {
		this.jobShiftId = jobShiftId;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public Long getWorkingShiftId() {
		return workingShiftId;
	}

	public void setWorkingShiftId(Long workingShiftId) {
		this.workingShiftId = workingShiftId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getShiftLineDetail() {
		return shiftLineDetail;
	}

	public void setShiftLineDetail(String shiftLineDetail) {
		this.shiftLineDetail = shiftLineDetail;
	}

	public String getPersons() {
		return persons;
	}

	public void setPersons(String persons) {
		this.persons = persons;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}
}
