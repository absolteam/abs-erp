package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.RecruitmentSource;
import com.aiotech.aios.hr.domain.entity.vo.RecruitmentSourceVO;
import com.aiotech.aios.hr.service.bl.RecruitmentSourceBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class RecruitmentSourceAction extends ActionSupport{
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(JobAssignmentAction.class);
	private Implementation implementation;
	private RecruitmentSourceBL recruitmentSourceBL;
	private List<Object> aaData;
	private RecruitmentSource recruitmentSource;
	private Long recruitmentSourceId;
	private Long recordId;
	
	public String returnSuccess() {

		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	public String getRecruitmentSourceList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			AIOSCommons.removeUploaderSession(session, "uploadedDocs", -1L,
					"RecruitmentSource");
			aaData = new ArrayList<Object>();
			List<RecruitmentSource> recruitmentSources = recruitmentSourceBL
					.getRecruitmentSourceService().getAllRecruitmentSource(
							getImplementation());

			if (recruitmentSources != null && recruitmentSources.size() > 0) {
				RecruitmentSourceVO recruitmentSourceVO = null;
				for (RecruitmentSource list : recruitmentSources) {
					recruitmentSourceVO = new RecruitmentSourceVO();
					recruitmentSourceVO.setRecruitmentSourceId(list
							.getRecruitmentSourceId());
					recruitmentSourceVO.setProviderName(list.getProviderName());
					recruitmentSourceVO.setSourceType(list.getLookupDetail()
							.getDisplayName());
					recruitmentSourceVO.setManager(list.getManager());
					recruitmentSourceVO.setWebsite(list.getWebsite());
					recruitmentSourceVO.setLoginId(list.getLoginId());
					recruitmentSourceVO.setPassword(list.getPassword());
					recruitmentSourceVO.setFullAddress((recruitmentSourceBL
							.getLocationBL().getLocationFullAddress(
									list.getLocation().getLocationId())
							.getFullAddress()));
					aaData.add(recruitmentSourceVO);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	public String getRecruitmentSourceAdd() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			AIOSCommons.removeUploaderSession(session, "uploadedDocs", -1L,
					"RecruitmentSource");
			RecruitmentSourceVO recruitmentSourceVO = null;
			if(recordId!=null && recordId>0){
				recruitmentSourceId=recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (recruitmentSourceId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(AttendancePolicy.class.getName());
					comment = recruitmentSourceBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			}
			if (recruitmentSourceId != null && recruitmentSourceId > 0) {
				recruitmentSourceVO = new RecruitmentSourceVO();
				recruitmentSource = recruitmentSourceBL.getRecruitmentSourceService()
						.getRecruitmentSourceById(recruitmentSourceId);
				recruitmentSourceVO=recruitmentSourceBL.convertEntityIntoVO(recruitmentSource);
			
			
			} else {
				recruitmentSourceVO = new RecruitmentSourceVO();
				
				recruitmentSourceVO.setCreatedDateDisplay(DateFormat
						.convertSystemDateToString(new Date()));
			}
			

			
			List<LookupDetail> recruitmentResources = recruitmentSourceBL
					.getLookupMasterBL().getActiveLookupDetails(
							"RECRUITMENT_RESOURCES", true);


			ServletActionContext.getRequest().setAttribute(
					"RECRUITMENT_RESOURCES", recruitmentResources);
			
			ServletActionContext.getRequest().setAttribute("RECRUITMENT_SOURCE",
					recruitmentSourceVO);

			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}
	
	public String saveRecruitmentSource() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		long documentRecId = -1;
		String returnMessage = null;
		try {

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			if (recruitmentSource.getRecruitmentSourceId() != null
					&& recruitmentSource.getRecruitmentSourceId() > 0) {

			} else {
				recruitmentSource.setCreatedDate(new Date());
				recruitmentSource.setPerson(person);
			}

			
			recruitmentSource.setImplementation(getImplementation());
			recruitmentSource.setIsApprove(Byte
					.valueOf(Constants.RealEstate.Status.NoDecession.getCode()
							+ ""));
			
			recruitmentSourceBL.saveRecruitementSource(recruitmentSource);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("saveRecruitmentSource() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			if (recruitmentSource.getRecruitmentSourceId() != null
					&& recruitmentSource.getRecruitmentSourceId() > 0) 

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveRecruitmentSource() Info unsuccessful Return");
			return ERROR;
		} finally {
			if (returnMessage!=null && returnMessage.equals("SUCCESS"))
				AIOSCommons.removeUploaderSession(session, "uploadedDocs",
						documentRecId, "OpenPosition");
		}
	}
	
	public String deleteRecruitmentSource() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			if (recruitmentSourceId != null && recruitmentSourceId > 0) {
				recruitmentSource = recruitmentSourceBL
						.getRecruitmentSourceService()
						.getRecruitmentSourceById(recruitmentSourceId);
			}

			recruitmentSourceBL.deleteRecruitmentSource(recruitmentSource);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("deleteRecruitmentSource() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deleteRecruitmentSource() Info unsuccessful Return");
			return ERROR;
		}
	}

	
	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}
	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}
	public RecruitmentSourceBL getRecruitmentSourceBL() {
		return recruitmentSourceBL;
	}
	public void setRecruitmentSourceBL(RecruitmentSourceBL recruitmentSourceBL) {
		this.recruitmentSourceBL = recruitmentSourceBL;
	}
	public List<Object> getAaData() {
		return aaData;
	}
	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public RecruitmentSource getRecruitmentSource() {
		return recruitmentSource;
	}

	public void setRecruitmentSource(RecruitmentSource recruitmentSource) {
		this.recruitmentSource = recruitmentSource;
	}

	public Long getRecruitmentSourceId() {
		return recruitmentSourceId;
	}

	public void setRecruitmentSourceId(Long recruitmentSourceId) {
		this.recruitmentSourceId = recruitmentSourceId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
