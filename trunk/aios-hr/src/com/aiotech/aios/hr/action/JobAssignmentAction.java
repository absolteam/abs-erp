package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.dao.DataIntegrityViolationException;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Designation;
import com.aiotech.aios.hr.domain.entity.Job;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.PersonBank;
import com.aiotech.aios.hr.domain.entity.vo.JobAssignmentVO;
import com.aiotech.aios.hr.domain.entity.vo.JobShiftVO;
import com.aiotech.aios.hr.domain.entity.vo.JobVO;
import com.aiotech.aios.hr.service.bl.JobAssignmentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class JobAssignmentAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(JobAssignmentAction.class);

	private Long jobId;
	private Long jobAssignmentId;
	private String jobAssignmentNumber;
	private Long swipeId;
	private Boolean isActive;
	private Long cmpDeptLocId;
	private Long companyId;
	private Long departmentId;
	private Long locationId;
	private Long designationId;
	private Boolean isLocalPay;
	private String effectiveDate;
	private String endDate;
	private Long personId;
	private String payMode;
	private Long personBankId;
	private Boolean isPrimary;
	private JobAssignmentBL jobAssignmentBL;
	private Implementation implementation;
	private Byte payPolicy;
	private Long recordId;
	private List<Object> aaData;
	private String employeeSheet;
	private String returnMessage;
	private String accomodationType;
	private String rejoiningDate;

	public String returnSuccess() {
		return SUCCESS;
	}

	// Listing JSON
	public String getJobAssignmentList() {

		try {
			List<JobAssignment> jobAssignments = jobAssignmentBL
					.getJobAssignmentService().getAllJobAssignment(
							getImplementation());
			List<JobAssignmentVO> jobAssignmentVOs = jobAssignmentBL
					.convertEntitiesTOVOs(jobAssignments);

			JSONObject jsonResponse = new JSONObject();
			if (jobAssignments != null && jobAssignments.size() > 0) {
				jsonResponse.put("iTotalRecords", jobAssignmentVOs.size());
				jsonResponse.put("iTotalDisplayRecords",
						jobAssignmentVOs.size());
			}
			JSONArray data = new JSONArray();
			for (JobAssignmentVO list : jobAssignmentVOs) {

				JSONArray array = new JSONArray();
				array.add(list.getJobAssignmentId() + "");
				array.add(list.getJobAssignmentNumber() + "");
				array.add(list.getJobName() + "");
				array.add(list.getPersonName() + "");
				array.add(list.getDesignationName() + "");
				array.add(list.getDepartmentLocationName() + "");
				array.add(list.getSwipeId() + "");
				array.add(list.getIsActiveStr() + "");
				array.add(list.getEffectiveDateStr() + "");
				array.add(list.getEndDateStr() + "");

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info(" getWorkingShiftList() Sucessfull Return");
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(" getWorkingShiftList() unsuccessful Return");
			return ERROR;
		}
	}

	public String getJobAssignmentJsonCommon() {

		try {
			List<JobAssignment> jobAssignments = jobAssignmentBL
					.getJobAssignmentService().getAllActiveJobAssignment(
							getImplementation());
			List<JobAssignmentVO> jobAssignmentVOs = jobAssignmentBL
					.convertEntitiesTOVOs(jobAssignments);

			aaData = new ArrayList<Object>();
			JobAssignmentVO vo = new JobAssignmentVO();
			for (JobAssignmentVO list : jobAssignmentVOs) {
				vo = new JobAssignmentVO();
				vo.setJobAssignmentId(list.getJobAssignmentId());
				vo.setJobAssignmentNumber(list.getJobAssignmentNumber());
				vo.setJobName(list.getJobName());
				vo.setPersonName(list.getPersonName());
				vo.setDesignationName(list.getDesignationName());
				vo.setDepartmentName(list.getDepartmentName());
				vo.setLocationName(list.getDepartmentLocationName());
				vo.setSwipeId(list.getSwipeId());
				vo.setMobile(list.getMobile());
				vo.setEmail(list.getEmail());
				aaData.add(vo);
			}

			LOGGER.info(" getWorkingShiftList() Sucessfull Return");
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(" getWorkingShiftList() unsuccessful Return");
			return ERROR;
		}
	}

	public String getCommonJobList() {

		try {
			List<Job> jobs = jobAssignmentBL.getJobBL().getJobService()
					.getAllJob(getImplementation());
			List<JobVO> jobVOs = jobAssignmentBL.getJobBL()
					.convertEntitiesTOVOs(jobs);

			JSONObject jsonResponse = new JSONObject();
			if (jobVOs != null && jobVOs.size() > 0) {
				jsonResponse.put("iTotalRecords", jobVOs.size());
				jsonResponse.put("iTotalDisplayRecords", jobVOs.size());
			}

			JSONArray data = new JSONArray();
			for (JobVO list : jobVOs) {
				if (designationId != null
						&& designationId > 0
						&& list.getDesignation() != null
						&& !list.getDesignation().getDesignationId()
								.equals(designationId))
					continue;

				JSONArray array = new JSONArray();
				array.add(list.getJobId() + "");
				array.add(list.getJobName() + "");
				array.add(list.getJobNumber() + "");
				/*
				 * if (list.getDesignation() != null) {
				 * array.add(list.getDesignation().getDesignationName() + "");
				 * array.add(list.getDesignation().getGrade().getGradeName() +
				 * ""); } else { array.add(""); array.add(""); }
				 */
				array.add(list.getPayList() + "");
				array.add(list.getLeaveList() + "");
				array.add(list.getShiftList() + "");
				array.add(list.getAssignmentList() + "");
				if (list.getDesignation() != null
						&& list.getDesignation().getDesignationId() != null) {
					array.add(list.getDesignation().getDesignationId() + "");
				} else {
					array.add("0");
				}
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info(" getWorkingShiftList() Sucessfull Return");
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(" getWorkingShiftList() unsuccessful Return");
			return ERROR;
		}
	}

	public String getEmploymentContractInfo() {

		try {
			JobAssignmentVO jobAssignmentVO = jobAssignmentBL
					.getEmployeeFullDetails(jobAssignmentId);

			ServletActionContext.getRequest().getSession()
					.setAttribute("EMP_CONTRACT_DATA", jobAssignmentVO);

			LOGGER.info(" getEmploymentContractInfo() Sucessfull Return");
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(" getEmploymentContractInfo() unsuccessful Return");
			return ERROR;
		}
	}

	public String getJobAssignmentAdd() {
		try {
			JobAssignment jobAssignment = new JobAssignment();
			JobAssignmentVO jobAssignmentVO = new JobAssignmentVO();
			if (recordId != null && recordId > 0) {
				jobAssignmentId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (jobAssignmentId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(AttendancePolicy.class.getName());
					comment = jobAssignmentBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			if (jobAssignmentId != null && jobAssignmentId > 0) {
				jobAssignment = jobAssignmentBL.getJobAssignmentService()
						.getJobAssignmentInfo(jobAssignmentId);
				jobAssignmentVO = jobAssignmentBL
						.convertEntityToVO(jobAssignment);

				/*
				 * List<JobShiftVO> jobShitVos = jobAssignmentBL
				 * .fetchLocationBasedShifts(jobAssignment
				 * .getCmpDeptLocation().getLocation() .getLocationId(), new
				 * Date(), new Date()); if(jobShitVos!=null &&
				 * jobShitVos.size()>0) LOGGER.error(jobShitVos.size()>0);
				 */

			} else {
				List<Integer> intelist = new ArrayList<Integer>();
				List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();
				jobAssignments = jobAssignmentBL.getJobAssignmentService()
						.getAllJobAssignment(getImplementation());
				if (jobAssignments != null && jobAssignments.size() > 0) {
					for (int i = 0; i < jobAssignments.size(); i++) {
						intelist.add(Integer.parseInt(jobAssignments.get(i)
								.getJobAssignmentNumber()));
					}

					int temp = Collections.max(intelist);
					temp += 1;
					jobAssignmentVO.setJobAssignmentNumber(temp + "");
				} else {
					jobAssignmentVO.setJobAssignmentNumber(1000 + "");
				}

				jobAssignmentVO.setEffectiveDateStr(DateFormat
						.convertSystemDateToString(new Date()));
			}

			List<LookupDetail> accomodations = jobAssignmentBL
					.getLookupMasterBL().getActiveLookupDetails(
							"ACCOMODATION_OPTIONS", true);

			ServletActionContext.getRequest().setAttribute(
					"ACCOMODATION_OPTIONS", accomodations);

			Map<Byte, String> payPolicy = jobAssignmentBL.getJobBL()
					.payPolicy();

			ServletActionContext.getRequest().setAttribute("JOB_ASSIGNMENT",
					jobAssignmentVO);
			ServletActionContext.getRequest().setAttribute("PAY_POLICY",
					payPolicy);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;

		}
	}

	public String getJobAssignmentCopy() {
		try {
			JobAssignment jobAssignment = new JobAssignment();
			JobAssignmentVO jobAssignmentVO = new JobAssignmentVO();
			if (recordId != null && recordId > 0) {
				jobAssignmentId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (jobAssignmentId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(AttendancePolicy.class.getName());
					comment = jobAssignmentBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			if (jobAssignmentId != null && jobAssignmentId > 0) {
				jobAssignment = jobAssignmentBL.getJobAssignmentService()
						.getJobAssignmentInfo(jobAssignmentId);
				jobAssignmentVO = jobAssignmentBL
						.convertEntityToVO(jobAssignment);
				List<Integer> intelist = new ArrayList<Integer>();
				List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();
				jobAssignments = jobAssignmentBL.getJobAssignmentService()
						.getAllJobAssignment(getImplementation());
				if (jobAssignments != null && jobAssignments.size() > 0) {
					for (int i = 0; i < jobAssignments.size(); i++) {
						intelist.add(Integer.parseInt(jobAssignments.get(i)
								.getJobAssignmentNumber()));
					}

					int temp = Collections.max(intelist);
					temp += 1;
					jobAssignmentVO.setJobAssignmentNumber(temp + "");
				} else {
					jobAssignmentVO.setJobAssignmentNumber(1000 + "");
				}

				jobAssignmentVO.setEffectiveDateStr(DateFormat
						.convertSystemDateToString(new Date()));
			}
			Map<Byte, String> payPolicy = jobAssignmentBL.getJobBL()
					.payPolicy();
			jobAssignmentVO.setJobAssignmentId(null);
			ServletActionContext.getRequest().setAttribute("JOB_ASSIGNMENT",
					jobAssignmentVO);
			ServletActionContext.getRequest().setAttribute("PAY_POLICY",
					payPolicy);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;

		}
	}

	public String saveJobAssignment() {
		try {
			JobAssignment jobAssignment = new JobAssignment();
			jobAssignment.setJobAssignmentNumber(jobAssignmentNumber);
			String returnMessage = "SUCCESS";

			if (jobAssignmentId != null && jobAssignmentId > 0) {
				jobAssignment = jobAssignmentBL.getJobAssignmentService()
						.getJobAssignmentInfo(jobAssignmentId);
			} else {
				List<Integer> intelist = new ArrayList<Integer>();
				List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();
				jobAssignments = jobAssignmentBL.getJobAssignmentService()
						.getAllJobAssignment(getImplementation());
				if (jobAssignments != null && jobAssignments.size() > 0) {
					for (int i = 0; i < jobAssignments.size(); i++) {
						intelist.add(Integer.parseInt(jobAssignments.get(i)
								.getJobAssignmentNumber()));
					}

					int temp = Collections.max(intelist);
					temp += 1;
					jobAssignment.setJobAssignmentNumber(temp + "");
				} else {
					jobAssignment.setJobAssignmentNumber(1000 + "");
				}
			}

			if (personId != null && personId > 0) {
				Person person = jobAssignmentBL.getPersonBL()
						.getPersonService().getPersonDetails(personId);
				jobAssignment.setPerson(person);
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Choose Employee!!");
				return ERROR;
			}

			if (designationId != null && designationId > 0) {
				Designation designation = new Designation();
				designation.setDesignationId(designationId);
				jobAssignment.setDesignation(designation);
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Choose Designation Information!!");
				return ERROR;
			}

			if (cmpDeptLocId != null && cmpDeptLocId > 0) {
				CmpDeptLoc cmpDeptLocation = new CmpDeptLoc();
				cmpDeptLocation.setCmpDeptLocId(cmpDeptLocId);
				jobAssignment.setCmpDeptLocation(cmpDeptLocation);
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE",
						"Choose Department or Branch Information!!");
				return ERROR;
			}

			if (jobId != null && jobId > 0) {
				Job job = new Job();
				job.setJobId(jobId);
				jobAssignment.setJob(job);
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Choose Job Information!!");
				return ERROR;
			}
			if (effectiveDate != null)
				jobAssignment.setEffectiveDate(DateFormat
						.convertStringToDate(effectiveDate));
			if (endDate != null)
				jobAssignment.setEndDate(DateFormat
						.convertStringToDate(endDate));

			if (rejoiningDate != null) {
				jobAssignment.setRejoiningDate(DateFormat
						.convertStringToDate(rejoiningDate));
				jobAssignment.setFreeze(false);
			}

			if (personBankId != null && personBankId > 0) {
				PersonBank personBank = new PersonBank();
				personBank.setPersonBankId(personBankId);
				jobAssignment.setPersonBank(personBank);
			}
			jobAssignment.setPayPolicy(payPolicy);
			jobAssignment.setSwipeId(swipeId);
			jobAssignment.setPayMode(payMode);
			jobAssignment.setIsLocalPay(isLocalPay);
			jobAssignment.setIsPrimary(isPrimary);
			jobAssignment.setImplementation(getImplementation());
			jobAssignment.setIsActive(isActive);
			if (accomodationType != null && !accomodationType.equals(""))
				jobAssignment.setLookupKey(accomodationType);

			List<JobAssignment> jobAssignments = jobAssignmentBL
					.getJobAssignmentService().getAllJobAssignment(
							getImplementation());
			returnMessage = jobAssignmentBL.saveJobAssignment(jobAssignment,
					jobAssignments);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);

			LOGGER.info("saveJobAssignment() Sucessfull Return");
			return SUCCESS;
		} catch (DataIntegrityViolationException ex) {
			if (ex.getMessage().contains("ASIOERR6666"))
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", ex.getMessage().split("_")[1]);
			LOGGER.error("saveJobAssignment() Unsuccessful Return");
			return ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			if (jobAssignmentId > 0)
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveJobAssignment() Unsuccessful Return");
			return ERROR;
		}
	}

	public String deleteJobAssignment() {
		String returnMessage = "SUCCESS";
		try {
			JobAssignment jobAssignment = null;

			if (jobAssignmentId != null && jobAssignmentId > 0) {
				jobAssignment = jobAssignmentBL.getJobAssignmentService()
						.getJobAssignmentInfo(jobAssignmentId);

				jobAssignmentBL.deleteJobAssignment(jobAssignment);
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "No record selected to delete");
			}
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("deleteJobAssignment() Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			returnMessage = "Sorry!..Information is in use already.";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.error("deleteJobAssignment() unsuccessful Return");
			return ERROR;
		}
	}

	public String insertEmployees() {
		try {

			jobAssignmentBL.processEmployeeBulkEntries(employeeSheet);
			returnMessage = "SUCCESS";
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String updateEmployees() {
		try {

			jobAssignmentBL.processEmployeeBulkUpdates(employeeSheet);
			returnMessage = "SUCCESS";
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}

	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}

	public String getJobAssignmentNumber() {
		return jobAssignmentNumber;
	}

	public void setJobAssignmentNumber(String jobAssignmentNumber) {
		this.jobAssignmentNumber = jobAssignmentNumber;
	}

	public Long getSwipeId() {
		return swipeId;
	}

	public void setSwipeId(Long swipeId) {
		this.swipeId = swipeId;
	}

	public Long getCmpDeptLocId() {
		return cmpDeptLocId;
	}

	public void setCmpDeptLocId(Long cmpDeptLocId) {
		this.cmpDeptLocId = cmpDeptLocId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public Long getDesignationId() {
		return designationId;
	}

	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}

	public Boolean getIsLocalPay() {
		return isLocalPay;
	}

	public void setIsLocalPay(Boolean isLocalPay) {
		this.isLocalPay = isLocalPay;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public JobAssignmentBL getJobAssignmentBL() {
		return jobAssignmentBL;
	}

	public void setJobAssignmentBL(JobAssignmentBL jobAssignmentBL) {
		this.jobAssignmentBL = jobAssignmentBL;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getPayMode() {
		return payMode;
	}

	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}

	public Long getPersonBankId() {
		return personBankId;
	}

	public void setPersonBankId(Long personBankId) {
		this.personBankId = personBankId;
	}

	public Boolean getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	public Byte getPayPolicy() {
		return payPolicy;
	}

	public void setPayPolicy(Byte payPolicy) {
		this.payPolicy = payPolicy;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getEmployeeSheet() {
		return employeeSheet;
	}

	public void setEmployeeSheet(String employeeSheet) {
		this.employeeSheet = employeeSheet;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getAccomodationType() {
		return accomodationType;
	}

	public void setAccomodationType(String accomodationType) {
		this.accomodationType = accomodationType;
	}

	public String getRejoiningDate() {
		return rejoiningDate;
	}

	public void setRejoiningDate(String rejoiningDate) {
		this.rejoiningDate = rejoiningDate;
	}

}
