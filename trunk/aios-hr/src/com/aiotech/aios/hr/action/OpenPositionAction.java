package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Designation;
import com.aiotech.aios.hr.domain.entity.OpenPosition;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.OpenPositionVO;
import com.aiotech.aios.hr.service.bl.OpenPositionBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class OpenPositionAction extends ActionSupport  {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(JobAssignmentAction.class);
	private Implementation implementation;
	private OpenPositionBL openPositionBL;
	private List<Object> aaData;
	public String returnSuccess() {

		return SUCCESS;
	}
	
	private OpenPosition openPosition;
	private Long openPositionId;
	private boolean isReduce;
	private Long designationId;
	private Long cmpDeptLocId;
	private OpenPositionVO openPositionVO;
	private String startDate;
	private String endDate;
	private Long recordId;
	
	@SuppressWarnings("unchecked")
	public String getOpenPositionList() {
		try {
			aaData = new ArrayList<Object>();
			List<OpenPosition> openPositions = openPositionBL
					.getOpenPositionService().getAllOpenPosition(
							getImplementation());

			if (openPositions != null && openPositions.size() > 0) {
				Map<Integer, String> statusTypes = openPositionBL.statusType();
				OpenPositionVO openPositionVO = null;
				for (OpenPosition list : openPositions) {
					openPositionVO = new OpenPositionVO();
					openPositionVO.setOpenPositionId(list.getOpenPositionId());
					openPositionVO.setDesignationName(list.getDesignation()
							.getDesignationName());
					openPositionVO.setGradeName(list.getDesignation()
							.getGrade().getGradeName());
					openPositionVO.setCompanyName(list.getCmpDeptLocation()
							.getCompany().getCompanyName());
					openPositionVO.setDepartmentName(list.getCmpDeptLocation()
							.getDepartment().getDepartmentName());
					openPositionVO.setLocationName(list.getCmpDeptLocation()
							.getLocation().getLocationName());
					openPositionVO.setNumberOfPosition(list
							.getNumberOfPosition());
					openPositionVO.setStatusDisplay(statusTypes.get(Integer
							.valueOf(list.getStatus() + "")));
					openPositionVO.setPositionReferenceNumber(list
							.getPositionReferenceNumber());
					aaData.add(openPositionVO);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	public String getOpenPositionAdd() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			AIOSCommons.removeUploaderSession(session, "uploadedDocs", -1L,
					"OpenPosition");
			OpenPositionVO openPositionVO = null;
			if(recordId!=null && recordId>0){
				openPositionId=recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (openPositionId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(AttendancePolicy.class.getName());
					comment = openPositionBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			}
			if (openPositionId != null && openPositionId > 0) {
				openPositionVO = new OpenPositionVO();
				openPosition = openPositionBL.getOpenPositionService()
						.getOpenPositionById(openPositionId);
				OpenPositionVO openPositionVOTemp = openPositionBL
						.findOpenPosition(openPosition);
				//Apply the Conversion to set the Entity value into EntityVO 
				openPositionVO=openPositionBL.convertEntityIntoVO(openPosition);
				openPositionVO.setNumberOfCount(openPositionVOTemp
						.getNumberOfCount());
				openPositionVO
						.setCreatedDateDisplay(DateFormat
								.convertDateToString(openPosition
										.getCreatedDate() + ""));
				openPositionVOTemp = null;
			} else {
				openPositionVO = new OpenPositionVO();
				openPositionVO.setPositionReferenceNumber(openPositionBL
						.getRefereceNum());
				openPositionVO.setCreatedDateDisplay(DateFormat
						.convertSystemDateToString(new Date()));
			}
			Map<String, String> genderTypes = openPositionBL.genderType();
			Map<Integer, String> statusTypes = openPositionBL.statusType();

			List<LookupDetail> jobTypes = openPositionBL.getLookupMasterBL()
					.getActiveLookupDetails("JOB_TYPE", true);

			List<LookupDetail> interviewModes = openPositionBL
					.getLookupMasterBL().getActiveLookupDetails(
							"INTERVIEW_MODE", true);

			List<LookupDetail> requiredExperiences = openPositionBL
					.getLookupMasterBL().getActiveLookupDetails("EXPERIENCES",
							true);

			List<LookupDetail> responsibilites = openPositionBL
					.getLookupMasterBL().getActiveLookupDetails(
							"RESPONSIBILITES", true);

			List<LookupDetail> qualifications = openPositionBL
					.getLookupMasterBL().getActiveLookupDetails(
							"QUALIFICATIONS", true);

			List<LookupDetail> recruitmentResources = openPositionBL
					.getLookupMasterBL().getActiveLookupDetails(
							"RECRUITMENT_RESOURCES", true);

			List<LookupDetail> skills = openPositionBL.getLookupMasterBL()
					.getActiveLookupDetails("SKILLS", true);

			List<LookupDetail> ageLimit = openPositionBL.getLookupMasterBL()
					.getActiveLookupDetails("AGE_LIMIT", true);

			List<LookupDetail> requiredTravel = openPositionBL
					.getLookupMasterBL().getActiveLookupDetails("TRAVEL_TYPE",
							true);

			List<LookupDetail> requiredExperience = openPositionBL
					.getLookupMasterBL().getActiveLookupDetails(
							"EXPERIENCE_PERIOD", true);

			ServletActionContext.getRequest().setAttribute("RESPONSIBILITES",
					responsibilites);
			ServletActionContext.getRequest().setAttribute("QUALIFICATIONS",
					qualifications);
			ServletActionContext.getRequest().setAttribute(
					"RECRUITMENT_RESOURCES", recruitmentResources);
			ServletActionContext.getRequest().setAttribute("SKILLS", skills);
			ServletActionContext.getRequest().setAttribute("EXPERIENCES",
					requiredExperiences);
			ServletActionContext.getRequest().setAttribute("JOB_TYPES",
					jobTypes);
			ServletActionContext.getRequest().setAttribute("TRAVEL_TYPE",
					requiredTravel);
			ServletActionContext.getRequest().setAttribute("EXPERIENCE_PERIOD",
					requiredExperience);
			ServletActionContext.getRequest().setAttribute("INTERVIEW_MODES",
					interviewModes);
			ServletActionContext.getRequest().setAttribute("GENDER_TYPE",
					genderTypes);
			ServletActionContext.getRequest().setAttribute("STATUS_TYPE",
					statusTypes);
			ServletActionContext.getRequest().setAttribute("AGE_LIMIT",
					ageLimit);
			ServletActionContext.getRequest().setAttribute("OPEN_POSITION",
					openPositionVO);

			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}
	
	public String findOpenPosition() {
		try {

			if (designationId != null && cmpDeptLocId != null
					&& designationId > 0 && cmpDeptLocId > 0) {
				openPositionVO = new OpenPositionVO();
				openPosition = new OpenPosition();
				Designation designation = new Designation();
				designation.setDesignationId(designationId);
				openPosition.setDesignation(designation);
				CmpDeptLoc cmpDeptLocation = new CmpDeptLoc();
				cmpDeptLocation.setCmpDeptLocId(cmpDeptLocId);
				openPosition.setCmpDeptLocation(cmpDeptLocation);
				openPositionVO = openPositionBL
						.findOpenPosition(openPosition);
			}

			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}
	
	
	
	public String saveOpenPosition() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		long documentRecId = -1;
		String returnMessage = null;
		try {

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			if (openPosition.getOpenPositionId() != null
					&& openPosition.getOpenPositionId() > 0) {

			} else {
				openPosition.setCreatedDate(new Date());
				openPosition.setPerson(person);
				// Increase the reference number count
				openPosition.setPositionReferenceNumber(openPositionBL
						.saveReferenceNum());
			}

			if (startDate != null)
				openPosition.setDateOpened(DateFormat
						.convertStringToDate(startDate));

			if (endDate != null)
				openPosition.setTargetDate(DateFormat
						.convertStringToDate(endDate));

			openPosition.setImplementation(getImplementation());
			openPosition.setIsApprove(Byte
					.valueOf(Constants.RealEstate.Status.NoDecession.getCode()
							+ ""));

			openPositionBL.saveOpenPosition(openPosition);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("saveOpenPosition() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			if (openPosition.getOpenPositionId() != null
					&& openPosition.getOpenPositionId() > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveOpenPosition() Info unsuccessful Return");
			return ERROR;
		} finally {
			if (returnMessage!=null && returnMessage.equals("SUCCESS"))
				AIOSCommons.removeUploaderSession(session, "uploadedDocs",
						documentRecId, "OpenPosition");
		}
	}
	
	public String deleteOpenPosition() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			if (openPositionId != null
					&& openPositionId> 0) {
				openPosition = openPositionBL.getOpenPositionService()
				.getOpenPositionById(openPositionId);
			} 
			
			openPositionBL.deleteOpenPosition(openPosition);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("deleteOpenPosition() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		
			ServletActionContext.getRequest().setAttribute(
					"RETURN_MESSAGE", "Delete Failure");
			LOGGER.error("deleteOpenPosition() Info unsuccessful Return");
			return ERROR;
		}
	}



	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	
	


	

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public OpenPositionBL getOpenPositionBL() {
		return openPositionBL;
	}

	public void setOpenPositionBL(OpenPositionBL openPositionBL) {
		this.openPositionBL = openPositionBL;
	}

	public OpenPosition getOpenPosition() {
		return openPosition;
	}

	public void setOpenPosition(OpenPosition openPosition) {
		this.openPosition = openPosition;
	}

	public Long getOpenPositionId() {
		return openPositionId;
	}

	public void setOpenPositionId(Long openPositionId) {
		this.openPositionId = openPositionId;
	}

	public boolean isReduce() {
		return isReduce;
	}

	public void setReduce(boolean isReduce) {
		this.isReduce = isReduce;
	}

	public Long getDesignationId() {
		return designationId;
	}

	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}

	public Long getCmpDeptLocId() {
		return cmpDeptLocId;
	}

	public void setCmpDeptLocId(Long cmpDeptLocId) {
		this.cmpDeptLocId = cmpDeptLocId;
	}

	public OpenPositionVO getOpenPositionVO() {
		return openPositionVO;
	}

	public void setOpenPositionVO(OpenPositionVO openPositionVO) {
		this.openPositionVO = openPositionVO;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	
}
