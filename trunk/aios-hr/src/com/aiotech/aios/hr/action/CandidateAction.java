package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.Candidate;
import com.aiotech.aios.hr.domain.entity.InterviewProcess;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.CandidateVO;
import com.aiotech.aios.hr.domain.entity.vo.InterviewVO;
import com.aiotech.aios.hr.service.bl.CandidateBL;
import com.aiotech.aios.hr.service.bl.InterviewBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class CandidateAction extends ActionSupport{
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(CandidateAction.class);
	private Implementation implementation;
	private CandidateBL candidateBL;
	private InterviewBL interviewBL;
	private List<Object> aaData;
	private Candidate candidate;
	private Long candidateId;
	private String expectedJoiningDate;
	private Long recordId;
	
	public String returnSuccess() {
		Map<Integer, String> rattings = interviewBL.rattings();
		ServletActionContext.getRequest().setAttribute("RATTING",
				rattings);
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	public String getCandidateList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();
			List<Candidate> candidates = candidateBL
					.getCandidateService().getAllCandidate(
							getImplementation());

			if (candidates != null && candidates.size() > 0) {
				Map<Integer, String> statusTypes = candidateBL.statusType();
				CandidateVO candidateVO = null;
				for (Candidate list : candidates) {
					candidateVO = new CandidateVO();
					candidateVO.setCandidateId(list.getCandidateId());
					if(list.getLookupDetail()!=null)
					candidateVO.setAvailability(list.getLookupDetail()
							.getDisplayName());
					candidateVO.setSourceType(list.getRecruitmentSource()
							.getLookupDetail().getDisplayName());
					candidateVO.setProviderName(list.getRecruitmentSource()
							.getProviderName());
					if(list
							.getAdvertisingPanel()!=null)
					candidateVO.setAdvertisingReference(list
							.getAdvertisingPanel().getReferenceNumber());
					candidateVO.setPositionReferenceNumber(list
							.getOpenPosition().getPositionReferenceNumber());
					candidateVO.setCandidateName(list.getPersonByPersonId()
							.getFirstName()
							+ " "
							+ list.getPersonByPersonId().getLastName());
					if(list.getExpectedJoiningDate()!=null)
					candidateVO.setExpectedJoiningDateDisplay(DateFormat
							.convertDateToString(list.getExpectedJoiningDate()
									+ ""));
					candidateVO.setStatusDisplay(statusTypes.get(Integer
							.valueOf(list.getStatus())));
					aaData.add(candidateVO);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	public String getCandidateAdd() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			CandidateVO candidateVO = null;
			if(recordId!=null && recordId>0){
				candidateId=recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (candidateId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(AttendancePolicy.class.getName());
					comment = candidateBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			}
			if (candidateId != null && candidateId > 0) {
				candidateVO = new CandidateVO();
				candidate = candidateBL.getCandidateService().getCandiateById(
						candidateId);

				// Apply the Conversion to set the Entity value into EntityVO
				candidateVO = candidateBL.convertEntityIntoVO(candidate);

			} else {
				candidateVO = new CandidateVO();
				candidateVO.setCreatedDateDisplay(DateFormat
						.convertSystemDateToString(new Date()));
			}
			Map<Integer, String> statusTypes = candidateBL.statusType();

			List<LookupDetail> availabilities = candidateBL.getLookupMasterBL()
					.getActiveLookupDetails("CANDIDATE_AVAILABILITY", true);

			ServletActionContext.getRequest().setAttribute(
					"CANDIDATE_AVAILABILITY", availabilities);
			ServletActionContext.getRequest().setAttribute("STATUS_TYPE",
					statusTypes);

			ServletActionContext.getRequest().setAttribute("CANDIDATE_INFO",
					candidateVO);

			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getCandidateShortList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();
			List<Candidate> candidates = candidateBL.getCandidateService()
					.getCandidateByStatus(
							getImplementation(),
							(byte) Constants.HR.InterviewStatus.Interviewed
									.getCode());

			if (candidates != null && candidates.size() > 0) {
				List<CandidateVO> candidateVOs = new ArrayList<CandidateVO>();
				Map<Integer, String> rattings = interviewBL.rattings();
				Map<Integer, String> statusTypes = candidateBL.statusType();
				CandidateVO candidateVO = null;
				for (Candidate list : candidates) {
					candidateVO = new CandidateVO();
					candidateVO.setCandidateId(list.getCandidateId());

					candidateVO.setPositionReferenceNumber(list
							.getOpenPosition().getPositionReferenceNumber());
					candidateVO.setCandidateName(list.getPersonByPersonId()
							.getFirstName()
							+ " "
							+ list.getPersonByPersonId().getLastName());

					candidateVO.setStatusDisplay(statusTypes.get(Integer
							.valueOf(list.getStatus())));
					InterviewVO interViewVo = interviewBL
							.calculateScoreAndRating(
									new ArrayList<InterviewProcess>(list
											.getInterviewProcesses()),
									new InterviewVO());
					if (interViewVo != null) {
						candidateVO.setRating(rattings.get(Integer
								.valueOf(interViewVo.getAverateRateing())));
						candidateVO.setRatingPoints(Integer.valueOf(interViewVo
								.getAverateRateing()));
						candidateVO.setScore(interViewVo.getTotalScore());
					}

					candidateVO
							.setDoShortList("<a href=\"#\" style=\"text-decoration: none; color: grey\" class=\"send_offer\">Send Offer</a>");
					candidateVOs.add(candidateVO);

				}

				Collections.sort(candidateVOs, new Comparator<CandidateVO>() {
					public int compare(CandidateVO m1, CandidateVO m2) {
						return m2.getRatingPoints().compareTo(
								m1.getRatingPoints());
					}
				});

				aaData.addAll(candidateVOs);
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	
	public String candidateSendOffer() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			if (candidateId != null && candidateId > 0) {
				candidateBL.sendForOfferLetter(candidateId,
						(byte) Constants.HR.InterviewStatus.ShortListed
								.getCode());
			}
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("deleteInterview() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deleteInterview() Info unsuccessful Return");
			return ERROR;
		}
	}
	

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public CandidateBL getCandidateBL() {
		return candidateBL;
	}

	public void setCandidateBL(CandidateBL candidateBL) {
		this.candidateBL = candidateBL;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	public Long getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(Long candidateId) {
		this.candidateId = candidateId;
	}

	public String getExpectedJoiningDate() {
		return expectedJoiningDate;
	}

	public void setExpectedJoiningDate(String expectedJoiningDate) {
		this.expectedJoiningDate = expectedJoiningDate;
	}

	public InterviewBL getInterviewBL() {
		return interviewBL;
	}

	public void setInterviewBL(InterviewBL interviewBL) {
		this.interviewBL = interviewBL;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
	
}
