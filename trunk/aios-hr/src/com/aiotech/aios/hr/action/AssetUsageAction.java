package com.aiotech.aios.hr.action;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.AssetCreation;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.AssetUsage;
import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.AssetUsageVO;
import com.aiotech.aios.hr.service.bl.AssetUsageBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class AssetUsageAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(AssetUsageAction.class);
	private Implementation implementation;
	private Integer id;
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private String returnMessage;
	private Long assetUsageId;
	private Long locationId;
	private Long personId;
	private Long productId;
	private Long assignedById;
	private String assignedDate;
	private String returnDate;
	private String description;
	private Byte status;
	private Long recordId;
	private AssetUsageBL assetUsageBL;
	private Long messageId;
	private Long alertId;
	private String printableContent;
	private String serialNumber;
	private long assetType;
	private long assetColor;
	private String brand;
	private String assetModel;
	private String manufacturer;
	private int usefulLife;
	private String purchaseDate;
	private String assetName;
	private long assetId;
	private String assetIds;

	public String returnSuccess() {

		return SUCCESS;
	}

	// Listing JSON
	public String getAssetUsageList() {

		try {
			List<AssetUsage> assetUsages = assetUsageBL.getAssetUsageService()
					.getAssetUsageList(getImplementation());
			List<AssetUsageVO> assetUsageVos = assetUsageBL
					.convertEntitiesToVOs(assetUsages);
			JSONObject jsonResponse = new JSONObject();
			if (assetUsages != null && assetUsages.size() > 0) {
				jsonResponse.put("iTotalRecords", assetUsages.size());
				jsonResponse.put("iTotalDisplayRecords", assetUsages.size());
			}
			JSONArray data = new JSONArray();
			for (AssetUsageVO list : assetUsageVos) {

				JSONArray array = new JSONArray();
				array.add(list.getAssetUsageId() + "");
				if (list.getPersonByPersonId() != null)
					array.add(list.getPersonByPersonId().getFirstName() + " "
							+ list.getPersonByPersonId().getLastName() + "");
				else
					array.add("-NA-");
				array.add(list.getAssetName() + "");
				array.add(list.getSerialNumber() + "");
				array.add(list.getAssetTypeView() + "");
				array.add(list.getBrand() + "");
				array.add(list.getManufacturer() + "");
				array.add(list.getAssetModel() + "");
				
				
				array.add(list.getAllocatedDate() + "");
				if (list.getAssetStatus() != null && list.getAssetStatus() == 1) {
					array.add("ALLOCATED");
				} else if (list.getAssetStatus() != null
						&& list.getAssetStatus() == 2) {
					array.add("RETURNED");
				} else if (list.getAssetStatus() != null
						&& list.getAssetStatus() == 3) {
					array.add("DAMAGED");
				} else if (list.getAssetStatus() != null
						&& list.getAssetStatus() == 4) {
					array.add("LOST");
				} else {
					array.add("-NA-");
				}
				if (list.getPersonByCreatedBy() != null)
					array.add(list.getPersonByCreatedBy().getFirstName() + " "
							+ list.getPersonByCreatedBy().getLastName() + "");
				else
					array.add("-NA-");
				if (list.getCmpDeptLocation() != null
						&& list.getCmpDeptLocation().getDepartment() != null) {
					array.add(list.getCmpDeptLocation().getDepartment()
							.getDepartmentName()
							+ "-"
							+ list.getCmpDeptLocation().getLocation()
									.getLocationName());
				} else {
					array.add("");
				}
				array.add(list.getDescription() + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Asset Usage List Sucessfull Return");
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Asset Usage List unsuccessful Return");
			return ERROR;
		}
	}

	public String getAssetUsageAdd() {
		try {
			CommentVO comment = new CommentVO();

			if (recordId != null && recordId > 0) {
				assetUsageId = recordId;
				comment.setRecordId(Long.parseLong(assetUsageId + ""));
				comment.setUseCase("com.aiotech.aios.hr.domain.entity.IdentityAvailability");
				comment = assetUsageBL.getCommentBL().getCommentInfo(comment);
			}
			AssetUsage assetUsage = null;
			AssetUsageVO assetUsageVO = new AssetUsageVO();
			if (assetUsageId != null && assetUsageId > 0) {
				assetUsage = assetUsageBL.getAssetUsageService()
						.getAssetUsageInfo(assetUsageId);
				assetUsageVO = assetUsageBL.convertEntityToVO(assetUsage);
			} else {
				assetUsageVO.setAllocationDate(DateFormat
						.convertSystemDateToStringTimeFormat(new Date()));
			}
			ServletActionContext.getRequest().setAttribute("ASSET_USAGE_INFO",
					assetUsageVO);

			List<LookupDetail> assetTypes = assetUsageBL.getLookupMasterBL()
					.getActiveLookupDetails("ASSET_TYPE", true);
			ServletActionContext.getRequest().setAttribute("ASSET_TYPES",
					assetTypes);
			List<LookupDetail> assetColors = assetUsageBL.getLookupMasterBL()
					.getActiveLookupDetails("ASSET_COLOR", true);
			ServletActionContext.getRequest().setAttribute("ASSET_COLORS",
					assetColors);
			LOGGER.info("Asset Usage Info Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			LOGGER.error("Asset Usage Info unsuccessful Return");
			return ERROR;
		}
	}

	public String assetAllocationPrintOut() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();

			AssetUsage assetUsage = null;
			AssetUsageVO assetUsageVO = null;
			List<AssetUsageVO> assetUsageVOs = null;
			if (assetIds != null && !assetIds.equals("")) {
				String[] leaveIdArr = assetIds.split(",");
				if (leaveIdArr.length > 1) {
					assetUsageVOs = new ArrayList<AssetUsageVO>();
					for (String string : leaveIdArr) {
						assetUsage = assetUsageBL.getAssetUsageService()
								.getAssetUsageInfo(Long.valueOf(string));
						assetUsageVO = assetUsageBL
								.convertEntityToVO(assetUsage);
						assetUsageVOs.add(assetUsageVO);
						assetUsageVO=null;
					}
				} else if (leaveIdArr.length == 1) {
					assetUsageVO = new AssetUsageVO();
					assetUsage = assetUsageBL.getAssetUsageService()
							.getAssetUsageInfo(Long.valueOf(leaveIdArr[0]));
					assetUsageVO = assetUsageBL.convertEntityToVO(assetUsage);
				} else {
					assetUsageVO = new AssetUsageVO();
					assetUsageVO.setAllocationDate(DateFormat
							.convertSystemDateToStringTimeFormat(new Date()));
				}

			}

			Configuration config = new Configuration();
			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");

			config.setDirectoryForTemplateLoading(new File(confProps
					.getProperty("file.drive")
					+ "aios/PrintTemplate/hr/"
					+ getImplementation().getImplementationId()));

			String scheme = ServletActionContext.getRequest().getScheme();
			String host = ServletActionContext.getRequest().getHeader("Host");

			// includes leading forward slash
			String contextPath = ServletActionContext.getRequest()
					.getContextPath();

			String urlPath = scheme + "://" + host + contextPath;
			Template template = null;
			if(assetUsageVO!=null){
				template = config
						.getTemplate("html-assetusage-template.ftl");
			}else if(assetUsageVOs!=null){
				template = config
				.getTemplate("html-assetusage-multiple-template.ftl");
			}
			Map<String, Object> rootMap = new HashMap<String, Object>();

			if (getImplementation().getMainLogo() != null) {

				byte[] bFile = FileUtil.getBytes(new File(getImplementation()
						.getMainLogo()));
				bFile = Base64.encodeBase64(bFile);

				rootMap.put("logo", new String(bFile));
			} else {
				rootMap.put("logo", "");
			}

			rootMap.put("urlPath", urlPath);
			rootMap.put("ASSETVO", assetUsageVO);
			rootMap.put("ASSETVO_MULTIPLE", assetUsageVOs);
			if(assetUsageVOs!=null && assetUsageVOs.size()>0)
				rootMap.put("ASSET_MASTER", assetUsageVOs.get(0));
			else
				rootMap.put("ASSET_MASTER", null);
			
			Writer out = new StringWriter();
			template.process(rootMap, out);
			printableContent = out.toString();
			// System.out.println(printableContent);

			return "template";
		} catch (Exception e) {
			LOGGER.error("Asset Usage Info unsuccessful Return");
			return ERROR;
		}
	}

	public String saveAssetUsage() {
		try {
			AssetUsage assetUsage = new AssetUsage();
			if (assetUsageId != null && assetUsageId > 0) {
				assetUsage = assetUsageBL.getAssetUsageService()
						.getAssetUsageInfo(assetUsageId);
			} else {
				assetUsage.setCreatedDate(new Date());
			}
			
			if(locationId!=null && locationId>0){
				CmpDeptLoc cmpDeptLocation = new CmpDeptLoc();
				cmpDeptLocation.setCmpDeptLocId(locationId);
				assetUsage.setCmpDeptLocation(cmpDeptLocation);
			}else{
				assetUsage.setCmpDeptLocation(null);
			}
			Product product = new Product();
			product.setProductId(productId);
			assetUsage.setProduct(product);

			Person person = null;
			if (personId != null && personId > 0) {
				person = new Person();
				person.setPersonId(personId);
				assetUsage.setPersonByPersonId(person);
			}
			Person assignedperson = null;
			if (assignedById != null && assignedById > 0) {
				assignedperson = new Person();
				assignedperson.setPersonId(assignedById);
				assetUsage.setPersonByCreatedBy(assignedperson);
			}
			if (assignedDate != null)
				assetUsage.setAllocatedDate(DateFormat
						.convertStringToDate(assignedDate));

			if (returnDate != null)
				assetUsage.setReturedDate(DateFormat
						.convertStringToDate(returnDate));

			assetUsage.setAssetName(assetName);
			assetUsage.setAssetModel(assetModel);
			assetUsage.setSerialNumber(serialNumber);
			assetUsage.setManufacturer(manufacturer);
			if (assetType > 0) {
				LookupDetail ld = new LookupDetail();
				ld.setLookupDetailId(assetType);
				assetUsage.setLookupDetailByAssetType(ld);
			}

			if (assetColor > 0) {
				LookupDetail ld = new LookupDetail();
				ld.setLookupDetailId(assetColor);
				assetUsage.setLookupDetailByAssetColor(ld);
			}

			assetUsage.setBrand(brand);
			assetUsage.setUsefulLife(usefulLife);

			if (purchaseDate != null)
				assetUsage.setPurchaseDate(DateFormat
						.convertStringToDate(purchaseDate));

			if (assetId > 0) {
				AssetCreation ld = new AssetCreation();
				ld.setAssetCreationId(assetId);
				assetUsage.setAssetCreation(ld);
			}

			assetUsage.setDescription(description);
			assetUsage.setAssetStatus(status);
			assetUsage.setIsActive(true);
			assetUsage.setImplementation(getImplementation());
			assetUsageBL.saveAssetUsage(assetUsage, alertId);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("Asset Usage Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			if (assetUsageId > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("Asset Usage Info unsuccessful Return");
			return ERROR;
		}
	}

	public String deleteAssetUsage() {
		try {
			if (assetUsageId != null && assetUsageId > 0) {
				assetUsageBL.deleteAssetUsage(assetUsageId, messageId);
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "No record selected to delete");
			}

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("Asset Usage Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure.. Information is in use already.");

			LOGGER.error("Asset Usage Info unsuccessful Return");
			return ERROR;
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public AssetUsageBL getAssetUsageBL() {
		return assetUsageBL;
	}

	public void setAssetUsageBL(AssetUsageBL assetUsageBL) {
		this.assetUsageBL = assetUsageBL;
	}

	public Long getAssetUsageId() {
		return assetUsageId;
	}

	public void setAssetUsageId(Long assetUsageId) {
		this.assetUsageId = assetUsageId;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getAssignedById() {
		return assignedById;
	}

	public void setAssignedById(Long assignedById) {
		this.assignedById = assignedById;
	}

	public String getAssignedDate() {
		return assignedDate;
	}

	public void setAssignedDate(String assignedDate) {
		this.assignedDate = assignedDate;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public long getAssetType() {
		return assetType;
	}

	public void setAssetType(long assetType) {
		this.assetType = assetType;
	}

	public long getAssetColor() {
		return assetColor;
	}

	public void setAssetColor(long assetColor) {
		this.assetColor = assetColor;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getAssetModel() {
		return assetModel;
	}

	public void setAssetModel(String assetModel) {
		this.assetModel = assetModel;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public int getUsefulLife() {
		return usefulLife;
	}

	public void setUsefulLife(int usefulLife) {
		this.usefulLife = usefulLife;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getAssetIds() {
		return assetIds;
	}

	public void setAssetIds(String assetIds) {
		this.assetIds = assetIds;
	}
}
