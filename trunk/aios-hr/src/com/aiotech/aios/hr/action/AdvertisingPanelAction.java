package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.AdvertisingPanel;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.AdvertisingPanelVO;
import com.aiotech.aios.hr.service.bl.AdvertisingPanelBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class AdvertisingPanelAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(JobAssignmentAction.class);
	private Implementation implementation;
	private AdvertisingPanelBL advertisingPanelBL;
	private List<Object> aaData;
	private AdvertisingPanel advertisingPanel;
	private Long advertisingPanelId;
	private String postedDate;
	private Long openPositionId;
	private Long recruitmentSourceId;
	private Long recordId;

	public String returnSuccess() {

		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String getAdvertisingPanelList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();
			List<AdvertisingPanel> advertisingPanels = null;
			if (openPositionId != null && openPositionId > 0)
				advertisingPanels = advertisingPanelBL
						.getAdvertisingPanelService()
						.getAllAdvertisingListWithPosition(openPositionId,
								recruitmentSourceId);
			else
				advertisingPanels = advertisingPanelBL
						.getAdvertisingPanelService().getAllAdvertisingList(
								getImplementation());

			if (advertisingPanels != null && advertisingPanels.size() > 0) {
				Map<Integer, String> statusTypes = advertisingPanelBL
						.statusType();
				AdvertisingPanelVO advertisingPanelVO = null;
				for (AdvertisingPanel list : advertisingPanels) {
					advertisingPanelVO = new AdvertisingPanelVO();
					advertisingPanelVO.setAdvertisingPanelId(list
							.getAdvertisingPanelId());
					advertisingPanelVO.setSourceType(list
							.getRecruitmentSource().getLookupDetail()
							.getDisplayName());
					advertisingPanelVO.setReferenceNumber(list
							.getReferenceNumber());
					advertisingPanelVO.setProviderName(list
							.getRecruitmentSource().getProviderName());
					advertisingPanelVO.setPositionReferenceNumber(list
							.getOpenPosition().getPositionReferenceNumber());
					advertisingPanelVO.setPostedBy(list.getPersonByPostedBy()
							.getFirstName()
							+ " "
							+ list.getPersonByPostedBy().getLastName());
					advertisingPanelVO.setPostedDateDisplay(DateFormat
							.convertDateToString(list.getPostedDate() + ""));
					advertisingPanelVO.setTaskDisplay(statusTypes.get(Integer
							.valueOf(list.getTask())));
					aaData.add(advertisingPanelVO);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getAdvertisingPanelAdd() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			AdvertisingPanelVO advertisingPanelVO = new AdvertisingPanelVO();
			if (recordId != null && recordId > 0) {
				advertisingPanelId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (advertisingPanelId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(AttendancePolicy.class.getName());
					comment = advertisingPanelBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			if (advertisingPanelId != null && advertisingPanelId > 0) {
				advertisingPanel = advertisingPanelBL
						.getAdvertisingPanelService().getAdvertisingPanelById(
								advertisingPanelId);
				advertisingPanelVO = advertisingPanelBL
						.convertEntityIntoVO(advertisingPanel);

			} else {
				advertisingPanelVO.setReferenceNumber(advertisingPanelBL
						.getRefereceNum());
				advertisingPanelVO.setPostedDateDisplay(DateFormat
						.convertSystemDateToString(new Date()));
			}
			Map<Integer, String> statusTypes = advertisingPanelBL.statusType();

			ServletActionContext.getRequest().setAttribute("ADVERTISING_PANEL",
					advertisingPanelVO);

			ServletActionContext.getRequest().setAttribute("STATUS_TYPE",
					statusTypes);
			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}

	public String saveAdvertisingPanel() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		String returnMessage = null;
		try {

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			if (advertisingPanel.getAdvertisingPanelId() != null
					&& advertisingPanel.getAdvertisingPanelId() > 0) {

			} else {
				advertisingPanel.setPersonByPostedBy(person);
				advertisingPanel.setReferenceNumber(advertisingPanelBL
						.saveReferenceNum());
			}

			if (postedDate != null)
				advertisingPanel.setPostedDate(DateFormat
						.convertStringToDate(postedDate));

			advertisingPanel.setImplementation(getImplementation());
			advertisingPanel.setIsApprove(Byte
					.valueOf(Constants.RealEstate.Status.NoDecession.getCode()
							+ ""));

			advertisingPanelBL.saveAdvertisingPanel(advertisingPanel);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("saveAdvertisingPanel() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			if (advertisingPanel.getAdvertisingPanelId() != null
					&& advertisingPanel.getAdvertisingPanelId() > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveAdvertisingPanel() Info unsuccessful Return");
			return ERROR;
		}
	}

	public String deleteAdvertisingPanel() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			if (advertisingPanelId != null && advertisingPanelId > 0) {
				advertisingPanel = advertisingPanelBL
						.getAdvertisingPanelService().getAdvertisingPanelById(
								advertisingPanelId);
			}

			advertisingPanelBL.deleteAdvertisingPanel(advertisingPanel);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("deleteAdvertisingPanel() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deleteAdvertisingPanel() Info unsuccessful Return");
			return ERROR;
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public AdvertisingPanelBL getAdvertisingPanelBL() {
		return advertisingPanelBL;
	}

	public void setAdvertisingPanelBL(AdvertisingPanelBL advertisingPanelBL) {
		this.advertisingPanelBL = advertisingPanelBL;
	}

	public AdvertisingPanel getAdvertisingPanel() {
		return advertisingPanel;
	}

	public void setAdvertisingPanel(AdvertisingPanel advertisingPanel) {
		this.advertisingPanel = advertisingPanel;
	}

	public Long getAdvertisingPanelId() {
		return advertisingPanelId;
	}

	public void setAdvertisingPanelId(Long advertisingPanelId) {
		this.advertisingPanelId = advertisingPanelId;
	}

	public String getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(String postedDate) {
		this.postedDate = postedDate;
	}

	public Long getOpenPositionId() {
		return openPositionId;
	}

	public void setOpenPositionId(Long openPositionId) {
		this.openPositionId = openPositionId;
	}

	public Long getRecruitmentSourceId() {
		return recruitmentSourceId;
	}

	public void setRecruitmentSourceId(Long recruitmentSourceId) {
		this.recruitmentSourceId = recruitmentSourceId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

}
