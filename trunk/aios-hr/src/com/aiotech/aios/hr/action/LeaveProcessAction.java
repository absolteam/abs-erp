package com.aiotech.aios.hr.action;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.joda.time.LocalDate;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobLeave;
import com.aiotech.aios.hr.domain.entity.JobPayrollElement;
import com.aiotech.aios.hr.domain.entity.LeaveProcess;
import com.aiotech.aios.hr.domain.entity.Payroll;
import com.aiotech.aios.hr.domain.entity.PayrollElement;
import com.aiotech.aios.hr.domain.entity.PayrollTransaction;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.SwipeInOut;
import com.aiotech.aios.hr.domain.entity.vo.JobPayrollElementVO;
import com.aiotech.aios.hr.domain.entity.vo.LeaveProcessVO;
import com.aiotech.aios.hr.domain.entity.vo.PayrollTransactionVO;
import com.aiotech.aios.hr.domain.entity.vo.PayrollVO;
import com.aiotech.aios.hr.domain.entity.vo.ResignationTerminationVO;
import com.aiotech.aios.hr.service.bl.LeaveProcessBL;
import com.aiotech.aios.hr.service.bl.PayrollBL;
import com.aiotech.aios.hr.service.bl.PersonBL;
import com.aiotech.aios.hr.to.LeaveProcessTO;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class LeaveProcessAction extends ActionSupport {

	private static final Logger LOGGER = LogManager
			.getLogger(LeaveProcessAction.class);

	private LeaveProcessBL leaveProcessBL;
	private PayrollBL payrollBL;
	private Implementation implementation;
	private CommentBL commentBL;

	private Long leaveProcessId;
	private Long jobLeaveId;
	private String fromDate;
	private String toDate;
	private String reason;
	private String contactOnLeave;
	private boolean halfDay;
	private String returnMessage;
	private Long recordId;
	private String comments;
	private Long personId;
	private Long jobAssignmentId;
	private Double days;
	private Double weekendDays;
	private Double holidayDays;
	private String printableContent;
	private boolean encash;
	private Long parentLeaveId;
	private boolean settleDues;
	private String leaveIds;
	private boolean holdUntilRejoining;

	public String getApplyLeave() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			LeaveProcess leaveProcess = null;
			if (leaveProcessId != null && leaveProcessId != 0) {
				leaveProcess = new LeaveProcess();

			}
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			List<JobLeave> jobLeaves = leaveProcessBL.getJobService()
					.getAllJobLeaveByPerson(person);
			ServletActionContext.getRequest().setAttribute("JOB_LEAVE_LIST",
					jobLeaves);
			ServletActionContext.getRequest().setAttribute("LEAVE_PROCESS",
					leaveProcess);
			ServletActionContext.getRequest().setAttribute("CURRENT_DATE",
					DateFormat.convertSystemDateToString(new Date()));
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getLeaveManagment() {
		try {

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getLeaveProcessListJson() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			List<LeaveProcess> leaveProcesses = new ArrayList<LeaveProcess>();
			JSONObject jsonResponse = new JSONObject();
			Person person = new Person();
			person = leaveProcessBL.getPersonBL().getPersonService()
					.getPersonDetails(user.getPersonId());
			leaveProcesses = leaveProcessBL.getLeaveProcessService()
					.getAllLeaveProcessBasedOnPerson(person);
			if (leaveProcesses != null && leaveProcesses.size() > 0) {
				jsonResponse.put("iTotalRecords", leaveProcesses.size());
				jsonResponse.put("iTotalDisplayRecords", leaveProcesses.size());
			}
			JSONArray data = new JSONArray();
			for (LeaveProcess list : leaveProcesses) {
				JSONArray array = new JSONArray();
				array.add(list.getLeaveProcessId() + "");
				array.add(list.getJobLeave().getLeave().getDisplayName() + "");
				array.add(DateFormat.convertDateToString(list.getFromDate()
						.toString()) + "");
				array.add(DateFormat.convertDateToString(list.getToDate()
						.toString()) + "");
				if (list.getHalfDay() != null && list.getHalfDay() == true)
					array.add("Yes");
				else
					array.add("No");
				// array.add(leaveProcessBL.numberOfDays(list.getFromDate(),
				// list.getToDate(), list.getHalfDay()));
				array.add(list.getDays() + "");
				array.add(list.getReason());
				array.add(list.getContactOnLeave());
				array.add(DateFormat.convertDateToString(list.getCreatedDate()
						.toString()) + "");
				if (list.getStatus() != null)
					array.add(Constants.HR.LeaveStatus.get(list.getStatus())
							.name() + "");
				else
					array.add("");
				if (list.getIsApprove() != null
						&& WorkflowConstants.Status.Published.getCode() == (byte) list
								.getIsApprove())
					array.add("Approved");
				else if (WorkflowConstants.Status.Deny.getCode() == (byte) list
						.getIsApprove())
					array.add("Rejected");
				else
					array.add("Pending");

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getLeaveProcessHistoryListJson() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			List<LeaveProcess> leaveProcesses = new ArrayList<LeaveProcess>();
			JSONObject jsonResponse = new JSONObject();
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			List<LeaveProcess> leaveProcessTemp = leaveProcessBL
					.getLeaveProcessService().getAllLeaveProcessBasedOnPerson(
							person);
			if (leaveProcessTemp != null && leaveProcessTemp.size() > 0) {
				for (LeaveProcess leaveProcess : leaveProcessTemp) {
					if (leaveProcess.getLeaveProcessId() != null
							&& !leaveProcess.getLeaveProcessId().equals(
									leaveProcessId))
						leaveProcesses.add(leaveProcess);
				}
			}
			if (leaveProcesses != null && leaveProcesses.size() > 0) {
				jsonResponse.put("iTotalRecords", leaveProcesses.size());
				jsonResponse.put("iTotalDisplayRecords", leaveProcesses.size());
			}
			JSONArray data = new JSONArray();
			for (LeaveProcess list : leaveProcesses) {
				JSONArray array = new JSONArray();
				array.add(list.getLeaveProcessId() + "");
				array.add(list.getJobLeave().getLeave().getDisplayName() + "");
				array.add(DateFormat.convertDateToString(list.getFromDate()
						.toString()) + "");
				array.add(DateFormat.convertDateToString(list.getToDate()
						.toString()) + "");
				if (list.getHalfDay() != null && list.getHalfDay() == true)
					array.add("Yes");
				else
					array.add("No");
				// array.add(leaveProcessBL.numberOfDays(list.getFromDate(),
				// list.getToDate(), list.getHalfDay()));
				array.add(list.getDays() + "");
				array.add(list.getReason());
				array.add(list.getContactOnLeave());
				array.add(DateFormat.convertDateToString(list.getCreatedDate()
						.toString()) + "");
				if (list.getStatus() != null)
					array.add(Constants.HR.LeaveStatus.get(list.getStatus())
							.name() + "");
				else
					array.add("");
				if (list.getIsApprove() != null
						&& WorkflowConstants.Status.Published.getCode() == (byte) list
								.getIsApprove())
					array.add("Approved");
				else if (WorkflowConstants.Status.Deny.getCode() == (byte) list
						.getIsApprove())
					array.add("Rejected");
				else
					array.add("Pending");

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getAllEmployeeLeaves() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			JSONObject jsonResponse = new JSONObject();
			List<LeaveProcess> leaveProcesses = leaveProcessBL
					.getLeaveProcessService().getAllLeaveProcessList(
							getImplementation());

			if (leaveProcesses != null && leaveProcesses.size() > 0) {
				jsonResponse.put("iTotalRecords", leaveProcesses.size());
				jsonResponse.put("iTotalDisplayRecords", leaveProcesses.size());
			}
			JSONArray data = new JSONArray();
			for (LeaveProcess list : leaveProcesses) {
				JSONArray array = new JSONArray();
				array.add(list.getLeaveProcessId() + "");
				JobAssignment jobAssignment = new JobAssignment();
				jobAssignment = list.getJobAssignment();
				array.add(jobAssignment.getPerson().getFirstName() + " "
						+ jobAssignment.getPerson().getLastName() + " - "
						+ jobAssignment.getPerson().getPersonNumber());
				array.add(jobAssignment.getDesignation().getDesignationName());
				array.add(jobAssignment.getCmpDeptLocation().getCompany()
						.getCompanyName()
						+ " >> "
						+ jobAssignment.getCmpDeptLocation().getDepartment()
								.getDepartmentName()
						+ " >> "
						+ jobAssignment.getCmpDeptLocation().getLocation()
								.getLocationName());
				array.add(list.getJobLeave().getLeave().getDisplayName() + "");
				array.add(DateFormat.convertDateToString(list.getFromDate()
						.toString()) + "");
				array.add(DateFormat.convertDateToString(list.getToDate()
						.toString()) + "");
				if (list.getHalfDay() != null && list.getHalfDay() == true)
					array.add("Yes");
				else
					array.add("No");
				// array.add(leaveProcessBL.numberOfDays(list.getFromDate(),
				// list.getToDate(), list.getHalfDay()));
				array.add(list.getDays() + "");
				array.add(list.getReason());
				array.add(list.getContactOnLeave());
				array.add(DateFormat.convertDateToString(list.getCreatedDate()
						.toString()) + "");
				if (list.getStatus() != null)
					array.add(Constants.HR.LeaveStatus.get(list.getStatus())
							.name() + "");
				else
					array.add("");
				if (list.getIsApprove() != null
						&& WorkflowConstants.Status.Published.getCode() == (byte) list
								.getIsApprove())
					array.add("Approved");
				else if (WorkflowConstants.Status.Deny.getCode() == (byte) list
						.getIsApprove())
					array.add("Rejected");
				else
					array.add("Pending");

				if (list.getEncash() != null && list.getEncash() == true)
					array.add("Yes");
				else
					array.add("No");

				if (list.getSettleDues() != null
						&& list.getSettleDues() == true)
					array.add("Yes");
				else
					array.add("No");

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getLeaveHistory() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			JobAssignment jobAssignment = new JobAssignment();
			if (jobAssignmentId == null || jobAssignmentId == 0) {
				jobAssignment = new JobAssignment();
				List<JobAssignment> jobAssignments = leaveProcessBL
						.getJobService().getJobAssignmentByPerson(
								user.getPersonId());
				if (jobAssignments != null && jobAssignments.size() > 0)
					jobAssignment = jobAssignments.get(0);

				jobAssignmentId = jobAssignment.getJobAssignmentId();
			}
			LeaveProcessTO leaveTO = leaveProcessBL.getLeaveHistory(
					jobAssignmentId, null, null);
			ServletActionContext.getRequest().setAttribute("LEAVE_MASTER",
					leaveTO);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getLeaveTypes() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Person person = new Person();
			person.setPersonId(personId);
			List<JobLeave> jobLeaves = leaveProcessBL.getJobService()
					.getAllJobLeaveByPerson(person);
			ServletActionContext.getRequest().setAttribute("JOB_LEAVE_LIST",
					jobLeaves);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getLeaveAvailability() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			JobAssignment jobAssignment = new JobAssignment();

			if (jobAssignmentId == null || jobAssignmentId == 0) {
				User user = (User) sessionObj.get("USER");
				jobAssignment = new JobAssignment();
				List<JobAssignment> jobAssignments = leaveProcessBL
						.getJobService().getJobAssignmentByPerson(
								user.getPersonId());
				if (jobAssignments != null && jobAssignments.size() > 0)
					jobAssignment = jobAssignments.get(0);
			} else {
				jobAssignment = leaveProcessBL.getJobService()
						.getJobAssignmentInfo(jobAssignmentId);

			}
			LeaveProcess leaveProcess = new LeaveProcess();
			if (fromDate != null && !fromDate.equals(""))
				leaveProcess.setFromDate(DateFormat
						.convertStringToDate(fromDate));
			else
				leaveProcess.setFromDate(new Date());
			if (toDate != null && !toDate.equals(""))
				leaveProcess.setToDate(DateFormat.convertStringToDate(toDate));
			else
				leaveProcess.setToDate(new Date());

			List<LeaveProcessTO> leaveProcessList = leaveProcessBL
					.findLeaveAvailability(jobAssignment, new Date());
			ServletActionContext.getRequest().setAttribute(
					"LEAVE_AVAILABILITY", leaveProcessList);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String saveApplyLeave() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			JobAssignment jobAssignment = new JobAssignment();
			List<JobAssignment> jobAssignments = leaveProcessBL.getJobService()
					.getJobAssignmentByPerson(user.getPersonId());
			if (jobAssignments != null && jobAssignments.size() > 0)
				jobAssignment = jobAssignments.get(0);
			LeaveProcess leaveProcess = new LeaveProcess();
			leaveProcess.setToDate(DateFormat.convertStringToDate(toDate));
			JobLeave jobLeaveTemp = leaveProcessBL.getJobService()
					.getJobLeaveInfo(jobLeaveId);
			List<LeaveProcessTO> leaveBalances = leaveProcessBL
					.findLeaveAvailability(jobAssignment,
							DateFormat.convertStringToDate(fromDate));
			for (LeaveProcessTO leaveProcessTO : leaveBalances) {
				if (leaveProcessTO.getLeaveId() != null
						&& (long) jobLeaveTemp.getLeave().getLeaveId() == (long) leaveProcessTO
								.getLeaveId()
						&& (double) days > Double.valueOf(leaveProcessTO
								.getEligibleDays())) {
					returnMessage = "Can not apply the leave more than the eligible days !";
					ServletActionContext.getRequest().setAttribute(
							"RETURN_MESSAGE", returnMessage);
					return SUCCESS;
				}
			}

			if (leaveProcessId != null && leaveProcessId != 0) {
				LeaveProcess leaveProcessEdit = leaveProcessBL
						.getLeaveProcessService().getLeaveProcess(
								leaveProcessId);
				leaveProcess.setLeaveProcessId(leaveProcessId);
				leaveProcess.setCreatedDate(leaveProcessEdit.getCreatedDate());
				leaveProcess.setPerson(leaveProcessEdit.getPerson());
				leaveProcess.setStatus(leaveProcessEdit.getStatus());
				leaveProcess.setIsApprove(leaveProcessEdit.getIsApprove());
				leaveProcess.setJobAssignment(leaveProcessEdit
						.getJobAssignment());
				if (leaveProcess.getToDate().after(new Date())
						&& leaveProcess.getIsApprove() == 3)
					leaveProcess.setStatus(Byte
							.parseByte(Constants.HR.LeaveStatus.Pending
									.getCode() + ""));
				else
					leaveProcess.setStatus(Byte
							.parseByte(Constants.HR.LeaveStatus.Taken.getCode()
									+ ""));
			} else {
				leaveProcess.setCreatedDate(new Date());
				Person person = new Person();
				person.setPersonId(user.getPersonId());
				leaveProcess.setPerson(person);
				leaveProcess.setJobAssignment(jobAssignment);
				if (leaveProcess.getToDate().after(new Date()))
					leaveProcess.setStatus(Byte
							.parseByte(Constants.HR.LeaveStatus.Pending
									.getCode() + ""));
				else
					leaveProcess.setStatus(Byte
							.parseByte(Constants.HR.LeaveStatus.Taken.getCode()
									+ ""));

				leaveProcess.setIsApprove(Byte
						.parseByte(Constants.RealEstate.Status.NoDecession
								.getCode() + ""));
			}

			JobLeave jobLeave = new JobLeave();
			jobLeave.setJobLeaveId(jobLeaveId);
			leaveProcess.setJobLeave(jobLeave);
			leaveProcess.setFromDate(DateFormat.convertStringToDate(fromDate));
			leaveProcess.setToDate(DateFormat.convertStringToDate(toDate));
			if (reason == null)
				leaveProcess.setReason("");
			else
				leaveProcess.setReason(reason);
			leaveProcess.setContactOnLeave(contactOnLeave);
			leaveProcess.setHalfDay(halfDay);
			if (days > 0) {
				leaveProcess.setDays(days);
				leaveProcess.setWeekendDays(weekendDays);
				leaveProcess.setHolidayDays(holidayDays);
			} else {
				returnMessage = "Leave request must be greater then Zero days";
				return ERROR;
			}
			leaveProcess.setImplementation(getImplementation());
			leaveProcess.setEncash(encash);
			leaveProcess.setSettleDues(settleDues);
			leaveProcess.setParentLeaveId(parentLeaveId);
			double perDayBasic = 0.0;
			double totalAmount = 0.0;
			if ((leaveProcess.getEncash() != null && leaveProcess.getEncash())
					|| (leaveProcess.getSettleDues() != null && leaveProcess
							.getSettleDues())) {
				JobPayrollElementVO jobPayrollElementVO = new JobPayrollElementVO();
				jobPayrollElementVO.setJobAssignmentId(leaveProcess
						.getJobAssignment().getJobAssignmentId());
				List<JobPayrollElement> jobPayrollElements = leaveProcessBL
						.getJobService().getJobPayrollByJobAssignment(
								jobPayrollElementVO);
				Double basicAmount = 0.0;
				for (JobPayrollElement jobPayrollElement : jobPayrollElements) {
					if (jobPayrollElement.getPayrollElementByPayrollElementId()
							.getElementCode().equalsIgnoreCase("BASIC")
							|| jobPayrollElement
									.getPayrollElementByPayrollElementId()
									.getElementCode().equalsIgnoreCase("HRA")) {
						basicAmount += jobPayrollElement.getAmount();
					}
				}
				perDayBasic = (basicAmount * 12) / 360;
				totalAmount = perDayBasic * leaveProcess.getDays();
				leaveProcess.setTotalAmount(totalAmount);
			}

			leaveProcessBL.saveLeaveProcess(leaveProcess);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String saveApplyLeaveByHr() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			JobAssignment jobAssignment = leaveProcessBL.getJobAssignmentBL()
					.getJobAssignmentService()
					.getJobAssignmentInfo(jobAssignmentId);
			LeaveProcess leaveProcess = new LeaveProcess();
			leaveProcess.setToDate(DateFormat.convertStringToDate(toDate));

			Boolean isLeaveRestrictionEnabled = session
					.getAttribute("apply_leave_restriction") != null ? ((String) session
					.getAttribute("apply_leave_restriction"))
					.equalsIgnoreCase("true") : false;

			if (isLeaveRestrictionEnabled) {
				LeaveProcess editLeave = leaveProcessBL
						.getLeaveProcessService().getLeaveProcess(
								leaveProcessId);
				if (editLeave != null
						&& editLeave.getEarlierReturnDays() != null
						&& editLeave.getEarlierReturnDays() > 0) {
					returnMessage = "Can not modify! Leave rejoinig has been updated";
					ServletActionContext.getRequest().setAttribute(
							"RETURN_MESSAGE", returnMessage);
					return SUCCESS;
				}

				// Restrict the leave application before six month completion
				// from the joining date

				LocalDate frmDate = new LocalDate(jobAssignment.getPerson()
						.getValidFromDate());
				LocalDate toDate = new LocalDate(
						DateFormat.convertStringToDate(fromDate));
				LocalDate periodEligibleDt = frmDate.plusMonths(6);
				if (periodEligibleDt.isAfter(toDate)) {
					returnMessage = "Can not apply! Employee have to complete six month of work";
					ServletActionContext.getRequest().setAttribute(
							"RETURN_MESSAGE", returnMessage);
					return SUCCESS;
				}

				// Exceeding the number of day restriction
				JobLeave jobLeaveTemp = leaveProcessBL.getJobService()
						.getJobLeaveInfo(jobLeaveId);
				List<LeaveProcessTO> leaveBalances = leaveProcessBL
						.findLeaveAvailability(jobAssignment,
								DateFormat.convertStringToDate(fromDate));
				for (LeaveProcessTO leaveProcessTO : leaveBalances) {
					if (leaveProcessTO.getLeaveId() != null
							&& (long) jobLeaveTemp.getLeave().getLeaveId() == (long) leaveProcessTO
									.getLeaveId()
							&& (double) days > Double.valueOf(leaveProcessTO
									.getAvailableDays())) {
						if (leaveProcessId != null && leaveProcessId != 0) {
							Double totalDays = editLeave.getDays()
									+ Double.valueOf(leaveProcessTO
											.getAvailableDays());
							if ((double) days > totalDays) {
								returnMessage = "Can not apply! Maximum available days is ("
										+ totalDays + ")";
								ServletActionContext.getRequest().setAttribute(
										"RETURN_MESSAGE", returnMessage);
								return SUCCESS;
							}
						} else {
							returnMessage = "Can not apply! Maximum available days is ("
									+ leaveProcessTO.getAvailableDays() + ")";
							ServletActionContext.getRequest().setAttribute(
									"RETURN_MESSAGE", returnMessage);
							return SUCCESS;
						}
					}
				}

			}

			if (leaveProcessId != null && leaveProcessId != 0) {
				leaveProcess = leaveProcessBL.getLeaveProcessService()
						.getLeaveProcess(leaveProcessId);
			} else {
				leaveProcess.setCreatedDate(new Date());
				Person person = new Person();
				person.setPersonId(personId);
				leaveProcess.setPerson(person);
				leaveProcess.setJobAssignment(jobAssignment);
				if (leaveProcess.getToDate().after(new Date()))
					leaveProcess.setStatus(Byte
							.parseByte(Constants.HR.LeaveStatus.Pending
									.getCode() + ""));
				else
					leaveProcess.setStatus(Byte
							.parseByte(Constants.HR.LeaveStatus.Taken.getCode()
									+ ""));

				leaveProcess.setIsApprove(Byte
						.parseByte(Constants.RealEstate.Status.NoDecession
								.getCode() + ""));

				leaveProcess
						.setJobLeave(leaveProcessBL.getJobAssignmentBL()
								.getJobBL().getJobService()
								.getJobLeaveInfo(jobLeaveId));
			}

			leaveProcess.setFromDate(DateFormat.convertStringToDate(fromDate));
			leaveProcess.setToDate(DateFormat.convertStringToDate(toDate));
			if (reason == null)
				leaveProcess.setReason("");
			else
				leaveProcess.setReason(reason);
			leaveProcess.setContactOnLeave(contactOnLeave);
			leaveProcess.setHalfDay(halfDay);
			if (days > 0) {
				leaveProcess.setDays(days);
				leaveProcess.setWeekendDays(weekendDays);
				leaveProcess.setHolidayDays(holidayDays);
			} else {
				returnMessage = "Leave request must be greater then Zero days";
				return ERROR;
			}
			leaveProcess.setImplementation(getImplementation());
			leaveProcess.setEncash(encash);
			leaveProcess.setSettleDues(settleDues);
			leaveProcess.setParentLeaveId(parentLeaveId);
			leaveProcess.setHoldUntilRejoining(holdUntilRejoining);
			double perDayBasic = 0.0;
			double totalAmount = 0.0;
			if ((leaveProcess.getEncash() != null && leaveProcess.getEncash())
					|| (leaveProcess.getSettleDues() != null && leaveProcess
							.getSettleDues())) {
				JobPayrollElementVO jobPayrollElementVO = new JobPayrollElementVO();
				jobPayrollElementVO.setJobAssignmentId(leaveProcess
						.getJobAssignment().getJobAssignmentId());
				List<JobPayrollElement> jobPayrollElements = leaveProcessBL
						.getJobService().getJobPayrollByJobAssignment(
								jobPayrollElementVO);
				Double basicAmount = 0.0;
				for (JobPayrollElement jobPayrollElement : jobPayrollElements) {
					if ((leaveProcess.getSettleDues() != null && leaveProcess
							.getSettleDues())) {
						if ((jobPayrollElement
								.getPayrollElementByPayrollElementId()
								.getElementCode().equalsIgnoreCase("BASIC") || jobPayrollElement
								.getPayrollElementByPayrollElementId()
								.getElementCode().equalsIgnoreCase("HRA"))) {
							basicAmount += jobPayrollElement.getAmount();
						}
					} else {
						basicAmount += jobPayrollElement.getAmount();
					}
				}
				perDayBasic = (basicAmount * 12) / 360;
				totalAmount = perDayBasic * leaveProcess.getDays();
				leaveProcess.setTotalAmount(totalAmount);
			}
			leaveProcessBL.setPayrollBL(getPayrollBL());

			leaveProcessBL.saveLeaveProcess(leaveProcess);
			leaveProcessBL.setPayrollBL(null);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext
					.getRequest()
					.setAttribute("RETURN_MESSAGE",
							"leave configuration error or Leave pay transaction has been closed.");
			return ERROR;
		}
	}

	public String approveLeave() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			if (leaveProcessId != null && leaveProcessId != 0) {
				LeaveProcess leaveProcessEdit = leaveProcessBL
						.getLeaveProcessService().getLeaveProcess(
								leaveProcessId);
				JobLeave jobLeave = new JobLeave();
				jobLeave.setJobLeaveId(jobLeaveId);
				leaveProcessEdit.setJobLeave(jobLeave);
				leaveProcessEdit.setHalfDay(halfDay);
				// leaveProcessEdit.setIsApprove(Byte.parseByte(Constants.RealEstate.Status.Approved.getCode()+""));
				leaveProcessBL.saveLeaveProcess(leaveProcessEdit);

				Comment comment = new Comment();
				comment.setComment(comments);
				comment.setCreatedDate(new Date());
				comment.setRecordId(leaveProcessEdit.getLeaveProcessId());

				comment.setPersonId(user.getPersonId());
				comment.setUseCase("com.aiotech.aios.hr.domain.entity.LeaveProcess");
				commentBL.getCommentService().saveComment(comment);

				returnMessage = "SUCCESS";

			}

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getEditApplyLeave() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			/*
			 * if(recordId!=null && recordId>0) leaveProcessId=recordId;
			 */

			LeaveProcess leaveProcess = null;
			List<JobLeave> jobLeaves = null;
			if (leaveProcessId != null && leaveProcessId != 0) {
				leaveProcess = new LeaveProcess();
				leaveProcess = leaveProcessBL.getLeaveProcessService()
						.getLeaveProcess(leaveProcessId);
				jobLeaves = leaveProcessBL.getJobService()
						.getAllJobLeaveByPerson(
								leaveProcess.getJobAssignment().getPerson());

			}

			// Comment Information --Added by rafiq
			Comment comment = new Comment();

			if (leaveProcessId != 0) {
				try {
					comment.setRecordId(leaveProcessId);
					comment.setUseCase("com.aiotech.aios.hr.domain.entity.LeaveProcess");
					comment = commentBL.getCommentInfo(comment);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			ServletActionContext.getRequest().getSession()
					.setAttribute("COMMENT_IFNO", comment);
			ServletActionContext.getRequest().setAttribute("JOB_LEAVE_LIST",
					jobLeaves);
			ServletActionContext.getRequest().setAttribute("LEAVE_PROCESS",
					leaveProcess);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getLeavePrintData() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Boolean isTemplatePrintEnabled = session
					.getAttribute("print_leave_template") != null ? ((String) session
					.getAttribute("print_leave_template"))
					.equalsIgnoreCase("true") : false;
			LeaveProcess leaveProcess = null;
			List<JobLeave> jobLeaves = null;
			List<LeaveProcessTO> leaveProcessList = new ArrayList<LeaveProcessTO>();
			LeaveProcessTO to = null;
			Date fromDate = null;
			Date toDate = null;
			Double noDays = 0.0;
			List<Date> startDates = new ArrayList<Date>();
			List<Date> endDates = new ArrayList<Date>();
			if (leaveIds != null && !leaveIds.equals("")) {
				String[] leaveIdArr = leaveIds.split(",");
				for (String string : leaveIdArr) {
					leaveProcess = new LeaveProcess();
					leaveProcess = leaveProcessBL.getLeaveProcessService()
							.getLeaveProcess(Long.valueOf(string));

					jobLeaves = leaveProcessBL
							.getJobService()
							.getAllJobLeaveByPerson(
									leaveProcess.getJobAssignment().getPerson());
					to = leaveProcessBL.convertToTO(leaveProcess);
					if (leaveProcess.getEncash() == null
							|| !leaveProcess.getEncash()) {
						startDates.add(leaveProcess.getFromDate());
						endDates.add(leaveProcess.getToDate());
						noDays += Double.valueOf(to.getNumberOfDays());
					}
					leaveProcessList.add(to);

				}

				Collections.sort(startDates, new Comparator<Date>() {
					public int compare(Date m1, Date m2) {
						return m1.compareTo(m2);
					}
				});
				if (startDates.size() > 0)
					fromDate = startDates.get(0);

				Collections.sort(endDates, new Comparator<Date>() {
					public int compare(Date m1, Date m2) {
						return m2.compareTo(m1);
					}
				});
				if (endDates.size() > 0)
					toDate = endDates.get(0);

				Collections.sort(leaveProcessList,
						new Comparator<LeaveProcessTO>() {
							public int compare(LeaveProcessTO m1,
									LeaveProcessTO m2) {
								return m1.getLeaveProcessId().compareTo(
										m2.getLeaveProcessId());
							}
						});

			}
			if (!isTemplatePrintEnabled) {
				ServletActionContext.getRequest().setAttribute(
						"JOB_LEAVE_LIST", jobLeaves);
				ServletActionContext.getRequest().setAttribute("LEAVE_PROCESS",
						leaveProcess);
				return "default";
			} else {
				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/hr/"
						+ getImplementation().getImplementationId()));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config
						.getTemplate("html-leave-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();

				if (getImplementation().getMainLogo() != null) {

					byte[] bFile = FileUtil.getBytes(new File(
							getImplementation().getMainLogo()));
					bFile = Base64.encodeBase64(bFile);

					rootMap.put("logo", new String(bFile));
				} else {
					rootMap.put("logo", "");
				}

				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date());
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);
				rootMap.put("urlPath", urlPath);
				rootMap.put("requestedDate", to.getCreatedDate());
				rootMap.put("fromDate",
						(fromDate != null ? fromDate : to.getFromDate()));
				rootMap.put("toDate",
						(toDate != null ? toDate : to.getToDate()));
				rootMap.put("numberOfDays",
						(noDays != 0.0 ? noDays : to.getNumberOfDays()));
				rootMap.put("employee", to.getPersonName());
				rootMap.put("company", to.getCompanyName());
				rootMap.put("department", to.getDepartmentName());
				rootMap.put("designation", to.getDesignationName());
				rootMap.put("location", to.getLocationName());
				rootMap.put("contactOnLeave", to.getContactOnLeave());
				rootMap.put("employeeID", to.getPersonNumber());
				rootMap.put("leaveType", to.getLeaveType());
				rootMap.put("orderNumber", to.getPersonNumber() + "-" + month
						+ "" + day + "/" + year);
				if (leaveProcessList.size() == 1 && !to.isEncash()) {
					rootMap.put("totalAmount", 0.0);
					rootMap.put("encash", false);
				} else {
					rootMap.put(
							"totalAmount",
							(leaveProcessList.size() > 1 ? 0.0 : to
									.getTotalAmount()));
					rootMap.put("encash", (leaveProcessList.size() > 1 ? false
							: to.isEncash()));
				}
				rootMap.put("leaveList", leaveProcessList);

				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
				// System.out.println(printableContent);

				return "template";
			}

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getLeaveDuesData() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Boolean isTemplatePrintEnabled = session
					.getAttribute("print_leave_due_template") != null ? ((String) session
					.getAttribute("print_leave_due_template"))
					.equalsIgnoreCase("true") : false;
			LeaveProcess leaveProcess = null;
			List<JobLeave> jobLeaves = null;
			List<LeaveProcessTO> leaveProcessList = new ArrayList<LeaveProcessTO>();
			LeaveProcessTO to = null;
			Date fromDate = null;
			Date toDate = null;
			Double noDays = 0.0;
			Payroll payroll = null;
			PayrollVO payrollVO = null;
			List<Date> startDates = new ArrayList<Date>();
			List<Date> endDates = new ArrayList<Date>();
			double finalAmount = 0.0;
			if (leaveIds != null && !leaveIds.equals("")) {
				String[] leaveIdArr = leaveIds.split(",");
				for (String string : leaveIdArr) {
					leaveProcess = new LeaveProcess();
					leaveProcess = leaveProcessBL.getLeaveProcessService()
							.getLeaveProcess(Long.valueOf(string));

					jobLeaves = leaveProcessBL
							.getJobService()
							.getAllJobLeaveByPerson(
									leaveProcess.getJobAssignment().getPerson());
					to = leaveProcessBL.convertToTO(leaveProcess);
					boolean addFlag = false;
					if ((leaveProcess.getEncash() != null && leaveProcess
							.getEncash())
							|| (leaveProcess.getSettleDues() != null && leaveProcess
									.getSettleDues())) {
						finalAmount += to.getTotalAmount();
						addFlag = true;
					}

					payroll = payrollBL.getPayrollService()
							.getPayrollDetailByUseCase(
									leaveProcess.getLeaveProcessId(),
									LeaveProcess.class.getSimpleName());
					if (payroll != null
							&& payroll.getPayrollTransactions() != null) {
						LeaveProcessTO tempTo = null;
						for (PayrollTransaction pt : payroll
								.getPayrollTransactions()) {
							tempTo = new LeaveProcessTO();
							BeanUtils.copyProperties(tempTo, to);
							tempTo.setNote(pt.getNote());
							tempTo.setLeaveType("Salary");
							tempTo.setFromDate(DateFormat
									.convertDateToString(pt.getStartDate() + ""));
							tempTo.setToDate(DateFormat.convertDateToString(pt
									.getEndDate() + ""));
							LocalDate startDate = new LocalDate(
									pt.getStartDate());
							LocalDate endDate = new LocalDate(pt.getEndDate());
							tempTo.setTotalAmount(pt.getAmount());
							finalAmount += pt.getAmount();

							int counts = 0;
							while (startDate.isBefore(endDate)) {
								startDate = startDate.plusDays(1);
								counts++;
							}
							tempTo.setNumberOfDays(counts + "");
							leaveProcessList.add(tempTo);
						}
					}
					if (addFlag)
						leaveProcessList.add(to);

				}

				Collections.sort(leaveProcessList,
						new Comparator<LeaveProcessTO>() {
							public int compare(LeaveProcessTO m1,
									LeaveProcessTO m2) {
								return m1.getLeaveProcessId().compareTo(
										m2.getLeaveProcessId());
							}
						});

			}
			JobPayrollElementVO jobPayrollElementVO = new JobPayrollElementVO();
			jobPayrollElementVO.setJobAssignmentId(leaveProcess
					.getJobAssignment().getJobAssignmentId());
			List<JobPayrollElement> jobPayrollElements = leaveProcessBL
					.getJobService().getJobPayrollByJobAssignment(
							jobPayrollElementVO);
			Double basicAmount = 0.0;
			for (JobPayrollElement jobPayrollElement : jobPayrollElements) {
				if (jobPayrollElement.getPayrollElementByPayrollElementId()
						.getElementCode().equalsIgnoreCase("BASIC")) {
					basicAmount = jobPayrollElement.getAmount();
					break;
				}
			}
			if (!isTemplatePrintEnabled) {
				ServletActionContext.getRequest().setAttribute(
						"JOB_LEAVE_LIST", jobLeaves);
				ServletActionContext.getRequest().setAttribute("LEAVE_PROCESS",
						leaveProcess);
				return "default";
			} else {
				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/hr/"
						+ getImplementation().getImplementationId()));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config
						.getTemplate("html-leave-due-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();

				if (getImplementation().getMainLogo() != null) {

					byte[] bFile = FileUtil.getBytes(new File(
							getImplementation().getMainLogo()));
					bFile = Base64.encodeBase64(bFile);

					rootMap.put("logo", new String(bFile));
				} else {
					rootMap.put("logo", "");
				}

				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date());
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);
				rootMap.put("urlPath", urlPath);
				rootMap.put("requestedDate", to.getCreatedDate());
				rootMap.put("numberOfDays",
						(noDays != 0.0 ? noDays : to.getNumberOfDays()));
				rootMap.put("employee", to.getPersonName());
				rootMap.put("company", to.getCompanyName());
				rootMap.put("department", to.getDepartmentName());
				rootMap.put("designation", to.getDesignationName());
				rootMap.put("location", to.getLocationName());
				rootMap.put("contactOnLeave", to.getContactOnLeave());
				rootMap.put("employeeID", to.getPersonNumber());
				rootMap.put("leaveType", to.getLeaveType());
				rootMap.put("basic", AIOSCommons.formatAmount(basicAmount));
				rootMap.put("orderNumber", to.getPersonNumber() + "-" + month
						+ "" + day + "/" + year);
				rootMap.put("finalTotalAmount", finalAmount);
				double netAmount = (Double.valueOf(finalAmount));
				String decimalAmount = String.valueOf(AIOSCommons
						.formatAmount(netAmount));
				decimalAmount = decimalAmount.substring(decimalAmount
						.lastIndexOf('.') + 1);
				String amountInWords = AIOSCommons
						.convertAmountToWords(netAmount);
				if (Double.parseDouble(decimalAmount) > 0) {
					amountInWords = amountInWords
							.concat(" and ")
							.concat(AIOSCommons
									.convertAmountToWords(decimalAmount))
							.concat(" Fils ");
					String replaceWord = "Only";
					amountInWords = amountInWords.replaceAll(replaceWord, "");
					amountInWords = amountInWords.concat(" " + replaceWord);
				}
				rootMap.put("finalTotalAmountInWords", amountInWords);
				rootMap.put("leaveList", leaveProcessList);

				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
				// System.out.println(printableContent);

				return "template";
			}

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String cancelLeave() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			LeaveProcess leaveProcess = new LeaveProcess();
			if (leaveProcessId != null && leaveProcessId != 0) {
				leaveProcess = leaveProcessBL.getLeaveProcessService()
						.getLeaveProcess(leaveProcessId);
				leaveProcess.setStatus(Byte
						.parseByte(Constants.HR.LeaveStatus.Cancelled.getCode()
								+ ""));
				leaveProcess.setIsApprove(Byte
						.parseByte(Constants.RealEstate.Status.Deny.getCode()
								+ ""));
				leaveProcessBL.saveLeaveProcess(leaveProcess);
				returnMessage = "SUCCESS";
			}
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String deleteLeave() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			LeaveProcess leaveProcess = new LeaveProcess();
			if (leaveProcessId != null && leaveProcessId != 0) {
				leaveProcess = leaveProcessBL.getLeaveProcessService()
						.getLeaveProcess(leaveProcessId);
				leaveProcessBL.setPayrollBL(getPayrollBL());
				leaveProcessBL.deleteLeaveProcess(leaveProcess);
				leaveProcessBL.setPayrollBL(null);
				returnMessage = "SUCCESS";
			}
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String updateLeaveReconcilation() {
		try {

			leaveProcessBL.updateLeaveReconcilation();

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String leaveExcluding() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			LeaveProcess leaveProcess = new LeaveProcess();
			JobAssignment jobAssignment = new JobAssignment();
			jobAssignment.setJobAssignmentId(jobAssignmentId);
			if (jobAssignmentId == null || jobAssignmentId == 0) {
				List<JobAssignment> jobAssignments = leaveProcessBL
						.getJobService().getJobAssignmentByPerson(
								user.getPersonId());
				if (jobAssignments != null && jobAssignments.size() > 0)
					jobAssignment = jobAssignments.get(0);
			} else {
				jobAssignment.setJobAssignmentId(jobAssignmentId);

			}

			leaveProcess.setJobAssignment(jobAssignment);
			leaveProcess.setFromDate(DateFormat.convertStringToDate(fromDate));
			leaveProcess.setToDate(DateFormat.convertStringToDate(toDate));
			LeaveProcessVO leaveProcessVO = leaveProcessBL
					.leaveExcludes(leaveProcess);

			ServletActionContext.getRequest().setAttribute("LEAVE_DAYS",
					leaveProcessVO);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String correctionInLeaveReconciliation() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			leaveProcessBL.reconciliationTableUpdate();

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String checkDuplicateRejoiningHold() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			JobAssignment jobAssignment = leaveProcessBL.getJobAssignmentBL()
					.getJobAssignmentService()
					.getJobAssignmentInfo(jobAssignmentId);
			List<LeaveProcess> leavePRocesses = leaveProcessBL
					.getLeaveProcessService().getAllLeaveProcessBasedOnPerson(
							jobAssignment.getPerson());
			if (leavePRocesses != null && leavePRocesses.size() > 0) {
				for (LeaveProcess leaveProcess : leavePRocesses) {
					if (leaveProcess.getHoldUntilRejoining() != null
							&& leaveProcess.getHoldUntilRejoining()
							&& (leaveProcessId == null || (long) leaveProcess
									.getLeaveProcessId() != (long) leaveProcessId)) {

						ServletActionContext
								.getRequest()
								.setAttribute(
										"RETURN_MESSAGE",
										"There is already rejoining hold option is checked for this leave "
												+ leaveProcess.getJobLeave()
														.getLeave()
														.getDisplayName()
												+ " period of ( "
												+ DateFormat
														.convertDateToString(leaveProcess
																.getFromDate()
																+ "")
												+ " - "
												+ DateFormat
														.convertDateToString(leaveProcess
																.getToDate()
																+ "")
												+ " ) "
												+ leaveProcess.getDays()
												+ " days. Do you want set the rejoing hold option with this new?");
						break;
					} else {
						ServletActionContext.getRequest().setAttribute(
								"RETURN_MESSAGE", "SUCCESS");
					}
				}
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "SUCCESS");
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public LeaveProcessBL getLeaveProcessBL() {
		return leaveProcessBL;
	}

	public void setLeaveProcessBL(LeaveProcessBL leaveProcessBL) {
		this.leaveProcessBL = leaveProcessBL;
	}

	public Long getLeaveProcessId() {
		return leaveProcessId;
	}

	public void setLeaveProcessId(Long leaveProcessId) {
		this.leaveProcessId = leaveProcessId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getContactOnLeave() {
		return contactOnLeave;
	}

	public void setContactOnLeave(String contactOnLeave) {
		this.contactOnLeave = contactOnLeave;
	}

	public boolean isHalfDay() {
		return halfDay;
	}

	public void setHalfDay(boolean halfDay) {
		this.halfDay = halfDay;
	}

	public Long getJobLeaveId() {
		return jobLeaveId;
	}

	public void setJobLeaveId(Long jobLeaveId) {
		this.jobLeaveId = jobLeaveId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}

	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}

	public Double getDays() {
		return days;
	}

	public void setDays(Double days) {
		this.days = days;
	}

	public Double getWeekendDays() {
		return weekendDays;
	}

	public void setWeekendDays(Double weekendDays) {
		this.weekendDays = weekendDays;
	}

	public Double getHolidayDays() {
		return holidayDays;
	}

	public void setHolidayDays(Double holidayDays) {
		this.holidayDays = holidayDays;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}

	public boolean isEncash() {
		return encash;
	}

	public void setEncash(boolean encash) {
		this.encash = encash;
	}

	public Long getParentLeaveId() {
		return parentLeaveId;
	}

	public void setParentLeaveId(Long parentLeaveId) {
		this.parentLeaveId = parentLeaveId;
	}

	public boolean isSettleDues() {
		return settleDues;
	}

	public void setSettleDues(boolean settleDues) {
		this.settleDues = settleDues;
	}

	public String getLeaveIds() {
		return leaveIds;
	}

	public void setLeaveIds(String leaveIds) {
		this.leaveIds = leaveIds;
	}

	public PayrollBL getPayrollBL() {
		return payrollBL;
	}

	public void setPayrollBL(PayrollBL payrollBL) {
		this.payrollBL = payrollBL;
	}

	public boolean isHoldUntilRejoining() {
		return holdUntilRejoining;
	}

	public void setHoldUntilRejoining(boolean holdUntilRejoining) {
		this.holdUntilRejoining = holdUntilRejoining;
	}

}
