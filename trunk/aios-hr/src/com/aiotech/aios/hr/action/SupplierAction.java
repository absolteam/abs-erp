package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.CreditTerm;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.service.CurrencyService;
import com.aiotech.aios.accounts.to.SupplierTO;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.SupplierClassification;
import com.aiotech.aios.common.to.Constants.Accounts.SupplierType;
import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.service.bl.SupplierBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class SupplierAction extends ActionSupport {

	private static final long serialVersionUID = 1947735206955381208L;

	private SupplierBL supplierBL;

	private static final Logger LOGGER = LogManager
			.getLogger(SupplierAction.class);

	private Implementation implementation = null;
	private Integer supplierNumber;
	private CurrencyService currencyService;
	private long supplierId;
	private Byte supplierAcType;
	private Byte acClassification;
	private long paymentTermId;
	private double creditLimit;
	private double openingBalance;
	private String bankName;
	private String branchName;
	private String ibanNumber;
	private String accountNumber;
	private long personId;
	private long cmpDeptLocId;
	private long combinationId;
	private String pageInfo;
	private Long recordId;

	private List<SupplierTO> supplierToList;
	private List<Object> aaData;

	public String returnSuccess() {
		return SUCCESS;
	}

	// Show all Supplier Details
	public String showAllSupplierDetails() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showAllSupplierDetails");
			aaData = supplierBL.getAllSuppliers(pageInfo);
			LOGGER.info("Module: Accounts : Method: showAllSupplierDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showAllSupplierDetails Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showSupplierAdd() {
		try {
			List<Supplier> supplierList = this.getSupplierBL().getAllSuppliers(
					getImplementationId());
			if (null != supplierList && supplierList.size() > 0) {
				List<Integer> supplierNo = new ArrayList<Integer>();
				for (Supplier list : supplierList)
					supplierNo.add(Integer.parseInt(list.getSupplierNumber()));
				supplierNumber = Collections.max(supplierNo);
				supplierNumber += 1;
			} else
				supplierNumber = 10000;
			Map<Byte, SupplierType> supplierType = new HashMap<Byte, SupplierType>();
			for (SupplierType e : EnumSet.allOf(SupplierType.class))
				supplierType.put(e.getCode(), e);
			Map<Byte, SupplierClassification> supplierClassification = new HashMap<Byte, SupplierClassification>();
			for (SupplierClassification e : EnumSet
					.allOf(SupplierClassification.class))
				supplierClassification.put(e.getCode(), e);
			List<CreditTerm> paymentTermList = supplierBL.getCreditTermBL()
					.getCreditTermService()
					.getAllSupplierCreditTerms(getImplementationId());
			List<Currency> currencyList = currencyService
					.getAllCurrency(getImplementationId());
			ServletActionContext.getRequest().setAttribute("SUPPLIER_ACTYPE",
					supplierType);
			ServletActionContext.getRequest().setAttribute(
					"SUPPLIER_CLASSIFICATION", supplierClassification);
			ServletActionContext.getRequest().setAttribute("PAYMENT_TERM_LIST",
					paymentTermList);
			ServletActionContext.getRequest().setAttribute("SUPPLIER_NUMBER",
					supplierNumber);
			ServletActionContext.getRequest().setAttribute("CURRENCY_LIST",
					currencyList);
		} catch (Exception ex) {
			LOGGER.info("Method showSupplierAdd " + ex);
		}
		return SUCCESS;
	}

	public String showSupplierUpdate() {
		try {
			LOGGER.info("Inside Method showSupplierUpdate ");
			if(recordId!=null && recordId>0){
				supplierId=recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (supplierId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(DirectPayment.class.getName());
					comment = supplierBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			}
			Supplier supplier = this.getSupplierBL().getSupplierService()
					.getSupplierById(supplierId);
			Map<Byte, SupplierType> supplierType = new HashMap<Byte, SupplierType>();
			for (SupplierType e : EnumSet.allOf(SupplierType.class)) {
				supplierType.put(e.getCode(), e);
			}
			Map<Byte, SupplierClassification> supplierClassification = new HashMap<Byte, SupplierClassification>();
			for (SupplierClassification e : EnumSet
					.allOf(SupplierClassification.class)) {
				supplierClassification.put(e.getCode(), e);
			}
			List<CreditTerm> paymentTermList = supplierBL.getCreditTermBL()
					.getCreditTermService()
					.getAllSupplierCreditTerms(getImplementationId());
			List<Currency> currencyList = currencyService
					.getAllCurrency(getImplementationId());
			ServletActionContext.getRequest().setAttribute("SUPPLIER_ACTYPE",
					supplierType);
			ServletActionContext.getRequest().setAttribute(
					"SUPPLIER_CLASSIFICATION", supplierClassification);
			ServletActionContext.getRequest().setAttribute("PAYMENT_TERM_LIST",
					paymentTermList);
			ServletActionContext.getRequest().setAttribute("SUPPLIER_NUMBER",
					supplierNumber);
			ServletActionContext.getRequest().setAttribute("CURRENCY_LIST",
					currencyList);
			ServletActionContext.getRequest().setAttribute(
					"SUPPLIER_INFORMATION", supplier);
		} catch (Exception ex) {
			LOGGER.info("Method showSupplierUpdate " + ex);
			return ERROR;
		}
		LOGGER.info("Inside Method showSupplierUpdate SUCCESS");
		return SUCCESS;
	}

	public String supplierDelete() {
		try {
			LOGGER.info("Inside Method supplierDelete ");
			Supplier supplier = this.getSupplierBL().getSupplierService()
					.getSupplierById(supplierId);
			this.getSupplierBL().deleteSupplier(supplier);
			LOGGER.info("Inside Method supplierDelete SUCCESS");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Method showSupplierAdd " + ex);
			return ERROR;
		}
	}

	public String execute() {
		try {
			List<Supplier> supplierList = supplierBL
					.getAllSuppliers(getImplementationId());

			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (Supplier supplier : supplierList) {
				array = new JSONArray();
				array.add(supplier.getSupplierId());
				array.add(supplier.getSupplierNumber());
				if (null != supplier.getPerson()){
					array.add(supplier.getPerson().getFirstName() + " "
							+ supplier.getPerson().getLastName());
					array.add("Peson");
				}
				else {
					String cmpName = (supplier.getCompany() != null ? supplier
							.getCompany().getCompanyName() : supplier
							.getCmpDeptLocation().getCompany().getCompanyName());
					array.add(cmpName + "");
					array.add("Company");
				}
				array.add(Constants.Accounts.SupplierType.get(supplier
						.getSupplierType()));
				array.add(Constants.Accounts.SupplierClassification
						.get(supplier.getClassification()));
				if (null != supplier.getCreditTerm())
					array.add(supplier.getCreditTerm().getName());
				else
					array.add("");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String supplierUpdate() {
		try {
			Supplier supplier = new Supplier();
			supplier.setSupplierId(supplierId);
			supplier.setSupplierNumber(String.valueOf(supplierNumber));
			supplier.setSupplierType(supplierAcType);
			supplier.setClassification(acClassification);
			CreditTerm creditTerm = null;
			if (paymentTermId > 0) {
				creditTerm = new CreditTerm();
				creditTerm.setCreditTermId(paymentTermId);
			}
			supplier.setCreditLimit(creditLimit);
			supplier.setOpeningBalance(openingBalance);
			supplier.setCreditTerm(creditTerm);
			supplier.setBankName(bankName);
			supplier.setBranchName(branchName);
			supplier.setIbanNumber(ibanNumber);
			supplier.setAccountNumber(accountNumber);
			supplier.setImplementation(implementation);
			Combination combination = new Combination();
			combination.setCombinationId(combinationId);
			supplier.setCombination(combination);
			if (personId > 0) {
				Person person = new Person();
				person.setPersonId(personId);
				supplier.setPerson(person);
			} else {
				CmpDeptLoc cmpDeptLoc = new CmpDeptLoc();
				cmpDeptLoc.setCmpDeptLocId(cmpDeptLocId);
				supplier.setCmpDeptLocation(cmpDeptLoc);
			}
			supplierBL.updateSupplier(supplier);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String showSupplier() {
		LOGGER.info("Inside Module: Accounts : Method: showSupplier");
		try {
			List<Supplier> supplierList = this.getSupplierBL()
					.getSupplierService()
					.getAllSuppliers(getImplementationId());
			List<SupplierTO> supplierToList = convertListTO(supplierList);
			ServletActionContext.getRequest().setAttribute("SUPPLIER_INFO",
					supplierToList);
			ServletActionContext.getRequest().setAttribute("PAGE_INFO",
					pageInfo);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showSupplier: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String getSupplierList() {
		LOGGER.info("Inside Module: Accounts : Method: getSupplierList");
		try {
			List<Supplier> supplierList = this.getSupplierBL()
					.getSupplierService()
					.getAllSuppliers(getImplementationId());
			supplierToList = convertListTO(supplierList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: getSupplierList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showSupplierByReceipts() {
		LOGGER.info("Inside Module: Accounts : Method: showSupplierByReceipts");
		try {
			List<Supplier> supplierList = this.getSupplierBL()
					.getSupplierService()
					.getSuppliersByReceipts(getImplementationId());
			List<SupplierTO> supplierToList = convertListTO(supplierList);
			ServletActionContext.getRequest().setAttribute("SUPPLIER_INFO",
					supplierToList);
			ServletActionContext.getRequest().setAttribute("PAGE_INFO",
					pageInfo);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showSupplierByReceipts: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public static List<SupplierTO> convertListTO(List<Supplier> supplierList)
			throws Exception {
		List<SupplierTO> supplierTOList = new ArrayList<SupplierTO>();
		for (Supplier list : supplierList) {
			supplierTOList.add(convertTO(list));
		}
		return supplierTOList;
	}

	public static SupplierTO convertTO(Supplier supplier) {
		SupplierTO supplierTO = new SupplierTO();
		supplierTO.setSupplierId(supplier.getSupplierId());
		supplierTO.setSupplierNumber(supplier.getSupplierNumber());
		supplierTO.setCombinationId(supplier.getCombination()
				.getCombinationId());
		if (null != supplier.getPerson())
			supplierTO.setSupplierName(supplier.getPerson().getFirstName()
					+ " " + supplier.getPerson().getLastName());
		else
			supplierTO
					.setSupplierName(null != supplier.getCmpDeptLocation() ? supplier
							.getCmpDeptLocation().getCompany().getCompanyName()
							: supplier.getCompany().getCompanyName());
		supplierTO.setType(Constants.Accounts.SupplierType.get(
				supplier.getSupplierType()).toString());
		if (null != supplier.getCreditTerm())
			supplierTO.setPaymentTermId(supplier.getCreditTerm()
					.getCreditTermId());
		return supplierTO;

	}

	public Implementation getImplementationId() {
		implementation = new Implementation();
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public SupplierBL getSupplierBL() {
		return supplierBL;
	}

	public void setSupplierBL(SupplierBL supplierBL) {
		this.supplierBL = supplierBL;
	}

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Integer getSupplierNumber() {
		return supplierNumber;
	}

	public void setSupplierNumber(Integer supplierNumber) {
		this.supplierNumber = supplierNumber;
	}

	public CurrencyService getCurrencyService() {
		return currencyService;
	}

	public void setCurrencyService(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}

	public long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(long supplierId) {
		this.supplierId = supplierId;
	}

	public Byte getSupplierAcType() {
		return supplierAcType;
	}

	public void setSupplierAcType(Byte supplierAcType) {
		this.supplierAcType = supplierAcType;
	}

	public Byte getAcClassification() {
		return acClassification;
	}

	public void setAcClassification(Byte acClassification) {
		this.acClassification = acClassification;
	}

	public long getPaymentTermId() {
		return paymentTermId;
	}

	public void setPaymentTermId(long paymentTermId) {
		this.paymentTermId = paymentTermId;
	}

	public double getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(double creditLimit) {
		this.creditLimit = creditLimit;
	}

	public double getOpeningBalance() {
		return openingBalance;
	}

	public void setOpeningBalance(double openingBalance) {
		this.openingBalance = openingBalance;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getIbanNumber() {
		return ibanNumber;
	}

	public void setIbanNumber(String ibanNumber) {
		this.ibanNumber = ibanNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public long getCmpDeptLocId() {
		return cmpDeptLocId;
	}

	public void setCmpDeptLocId(long cmpDeptLocId) {
		this.cmpDeptLocId = cmpDeptLocId;
	}

	public long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}

	public String getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(String pageInfo) {
		this.pageInfo = pageInfo;
	}

	public List<SupplierTO> getSupplierToList() {
		return supplierToList;
	}

	public void setSupplierToList(List<SupplierTO> supplierToList) {
		this.supplierToList = supplierToList;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

}
