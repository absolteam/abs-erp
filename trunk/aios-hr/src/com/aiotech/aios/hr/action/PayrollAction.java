package com.aiotech.aios.hr.action;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.Location;
import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;
import com.aiotech.aios.hr.domain.entity.Payroll;
import com.aiotech.aios.hr.domain.entity.PayrollElement;
import com.aiotech.aios.hr.domain.entity.PayrollTransaction;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.SwipeInOut;
import com.aiotech.aios.hr.domain.entity.vo.JobPayrollElementVO;
import com.aiotech.aios.hr.domain.entity.vo.PayrollVO;
import com.aiotech.aios.hr.service.bl.PayrollBL;
import com.aiotech.aios.hr.to.PayrollTO;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class PayrollAction extends ActionSupport {
	private static final Logger LOGGER = LogManager
			.getLogger(PayrollAction.class);
	private PayrollBL payrollBL;
	private CommentBL commentBL;
	private Implementation implementation;

	private Long companyId;
	private String fromDate;
	private String toDate;
	private String returnMessage;
	private Long payrollId;
	private String persons;
	private Long recordId;
	private Integer id;
	private String payLineDetail;
	private Double netAmount;
	private String payMonth;
	private InputStream fileInputStream;
	private Long accountId;
	private String payYear;
	private Double exchangeRate;
	private Byte payPolicy;
	private Byte payType;
	private Long jobAssignmentId;
	private String transactionDate;
	private Long payPeriodTransactionId;
	private String printCall;
	private String viewRedirect;

	private Long alertId;
	private Long messageId;
	private Long payPeriodId;
	private String printableContent;
	private long locationId;

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public String getPayllrollExecution() {
		try {
			List<Company> cmpanies = payrollBL.getCompanyBL()
					.getCompanyService().getOnlyCompanies(getImplementation());
			ServletActionContext.getRequest().setAttribute("COMPANY_LIST",
					cmpanies);
			Map<Byte, String> payPolicy = payrollBL.getJobBL().payPolicy();
			ServletActionContext.getRequest().setAttribute("PAY_POLICY",
					payPolicy);
			List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();
			jobAssignments = payrollBL.getJobAssignmentBL()
					.getJobAssignmentService()
					.getAllActiveJobAssignment(getImplementation());

			ServletActionContext.getRequest().setAttribute("PERSON_LIST",
					jobAssignments);

			String[] nameOfMonth = new DateFormatSymbols().getMonths();
			ServletActionContext.getRequest().setAttribute("PAYMONTH_LIST",
					nameOfMonth);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getWPSExecution() {
		try {
			List<Company> cmpanies = payrollBL.getCompanyBL()
					.getCompanyService().getOnlyCompanies(getImplementation());
			ServletActionContext.getRequest().setAttribute("COMPANY_LIST",
					cmpanies);
			Map<Byte, String> payPolicy = payrollBL.getJobBL().payPolicy();
			ServletActionContext.getRequest().setAttribute("PAY_POLICY",
					payPolicy);
			String[] nameOfMonth = new DateFormatSymbols().getMonths();
			ServletActionContext.getRequest().setAttribute("PAYMONTH_LIST",
					nameOfMonth);
			List<Location> locations = payrollBL.getJobAssignmentBL()
					.getCmpDeptLocBL().getLocationBL().getLocationService()
					.getAllLocation(getImplementation());
			ServletActionContext.getRequest().setAttribute("LOCATION_LIST",
					locations);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getPayrollExecutionListJson() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			List<Payroll> payrolls = new ArrayList<Payroll>();
			JSONObject jsonResponse = new JSONObject();
			payrolls = payrollBL.getPayrollService().getAllPayroll(
					getImplementation());
			if (payrolls != null && payrolls.size() > 0) {
				jsonResponse.put("iTotalRecords", payrolls.size());
				jsonResponse.put("iTotalDisplayRecords", payrolls.size());
			}
			JSONArray data = new JSONArray();
			for (Payroll list : payrolls) {
				JSONArray array = new JSONArray();
				array.add(list.getPayrollId() + "");
				array.add(list.getJobAssignment().getPerson().getFirstName()
						+ " "
						+ list.getJobAssignment().getPerson().getLastName()
						+ "-"
						+ list.getJobAssignment().getPerson().getPersonNumber());
				array.add(list.getJobAssignment().getCmpDeptLocation()
						.getCompany().getCompanyName()
						+ "");
				array.add(list.getJobAssignment().getCmpDeptLocation()
						.getDepartment().getDepartmentName()
						+ "");
				array.add(list.getJobAssignment().getCmpDeptLocation()
						.getLocation().getLocationName()
						+ "");
				array.add(DateFormat.convertDateToString(list.getStartDate()
						.toString()) + "");
				array.add(DateFormat.convertDateToString(list.getEndDate()
						.toString()) + "");
				array.add(list.getPayMonth() + "");
				array.add(list.getNetAmount().toString() + "");
				array.add(DateFormat.convertDateToString(list.getCreatedDate()
						.toString()) + "");
				if (list.getStatus() != null)
					array.add(Constants.HR.PayStatus.get(list.getStatus())
							.name());
				else
					array.add("");

				if (list.getIsApprove() != null)
					array.add(WorkflowConstants.Status.get(list.getIsApprove())
							.name() + "");
				else
					array.add("");

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getPayrollAdjustmentListJson() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			List<Payroll> payrolls = new ArrayList<Payroll>();
			JSONObject jsonResponse = new JSONObject();
			payrolls = payrollBL.getPayrollService().getAllPayForAdjustment(
					getImplementation());
			if (payrolls != null && payrolls.size() > 0) {
				jsonResponse.put("iTotalRecords", payrolls.size());
				jsonResponse.put("iTotalDisplayRecords", payrolls.size());
			}
			JSONArray data = new JSONArray();
			for (Payroll list : payrolls) {
				JSONArray array = new JSONArray();
				array.add(list.getPayrollId() + "");
				array.add(list.getJobAssignment().getPerson().getFirstName()
						+ " "
						+ list.getJobAssignment().getPerson().getLastName()
						+ "-"
						+ list.getJobAssignment().getPerson().getPersonNumber());
				array.add(list.getJobAssignment().getCmpDeptLocation()
						.getCompany().getCompanyName()
						+ "");
				array.add(list.getJobAssignment().getCmpDeptLocation()
						.getDepartment().getDepartmentName()
						+ "");
				array.add(list.getJobAssignment().getCmpDeptLocation()
						.getLocation().getLocationName()
						+ "");
				array.add(DateFormat.convertDateToString(list.getStartDate()
						.toString()) + "");
				array.add(DateFormat.convertDateToString(list.getEndDate()
						.toString()) + "");
				array.add(list.getPayMonth() + "");
				if (list.getNetAmount() != null)
					array.add(list.getNetAmount().toString() + "");
				else
					array.add("");

				array.add(DateFormat.convertDateToString(list.getCreatedDate()
						.toString()) + "");
				if (list.getStatus() != null)
					array.add(Constants.HR.PayStatus.get(list.getStatus())
							.name());
				else
					array.add("");

				if (list.getIsApprove() != null)
					array.add(WorkflowConstants.Status.get(list.getIsApprove())
							.name() + "");
				else
					array.add("");

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String processPayllrollExecution() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();
			List<JobAssignment> jobAssignmentsTemp = new ArrayList<JobAssignment>();
			jobAssignments = payrollBL.getJobAssignmentBL()
					.getJobAssignmentService()
					.getAllActiveJobAssignment(getImplementation());
			if (persons != null && !persons.equals("")) {

				for (JobAssignment jobAssigment1 : jobAssignments) {
					String temps[] = persons.split(",");
					for (String personId : temps) {
						if (jobAssigment1.getJobAssignmentId().equals(
								Long.parseLong(personId)))
							jobAssignmentsTemp.add(jobAssigment1);
					}
				}

			}
			PayrollVO payrollVO = new PayrollVO();
			payrollVO.setJobAssignmentList(jobAssignmentsTemp);
			payrollVO.setPayPolicy(payPolicy);
			if (toDate != null && !toDate.equals(""))
				payrollVO.setCutOffDate(DateFormat.convertStringToDate(toDate));

			if (payMonth != null && payYear != null)
				payrollVO.setPayMonth(payMonth + "," + payYear);
			payrollBL.payrollExecution(payrollVO);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String deletePayroll() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			if (payrollId != null && payrollId > 0) {
				Payroll payroll = payrollBL.getPayrollService()
					.getPayrollDetail(payrollId);
				if (payroll.getPayPeriodTransaction().getIsFreeze() != null
						&& payroll.getPayPeriodTransaction().getIsFreeze()) {
					ServletActionContext.getRequest().setAttribute(
							"RETURN_MESSAGE",
							"Salary transaction period has been closed!");
					LOGGER.info("Salary transaction period has been closed!");
					return SUCCESS;
				}
				payrollBL.deletePayroll(payrollId);
				returnMessage = "SUCCESS";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String deletePayrollAll() {
		try {
			if (payLineDetail != null && !payLineDetail.equals("")) {
				payrollBL.deletePyarollBulk(payLineDetail);
				returnMessage = "SUCCESS";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
			}else if(payMonth!=null && !payMonth.equals("")){
				payrollBL.deleteAllPyarollByMonth(payMonth+","+payYear);
				returnMessage = "SUCCESS";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getEmployeePayllrollList() {
		try {
			List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();

			jobAssignments = payrollBL.getJobAssignmentBL()
					.getJobAssignmentService()
					.getAllActiveJobAssignment(getImplementation());

			if (payPolicy != null && payPolicy > 0) {
				List<JobAssignment> jobAssignmentsTemp = new ArrayList<JobAssignment>();
				jobAssignmentsTemp.addAll(jobAssignments);
				for (JobAssignment jobAssignment : jobAssignmentsTemp) {
					if (jobAssignment.getPayPolicy() == null
							|| ((byte) jobAssignment.getPayPolicy() != (byte) payPolicy))
						jobAssignments.remove(jobAssignment);
				}
			}

			ServletActionContext.getRequest().setAttribute("PERSON_LIST",
					jobAssignments);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getActiveJobAssignmentList() {
		try {
			List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();

			jobAssignments = payrollBL.getJobAssignmentBL()
					.getJobAssignmentService()
					.getAllActiveJobAssignment(getImplementation());

			ServletActionContext.getRequest().setAttribute("PERSON_LIST",
					jobAssignments);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getEmployeePayllrollListJson() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			List<Payroll> payrolls = new ArrayList<Payroll>();
			JSONObject jsonResponse = new JSONObject();
			payrolls = payrollBL.getPayrollService().getAllPayrollToEmployee(
					person);
			if (payrolls != null && payrolls.size() > 0) {
				jsonResponse.put("iTotalRecords", payrolls.size());
				jsonResponse.put("iTotalDisplayRecords", payrolls.size());
			}
			JSONArray data = new JSONArray();
			for (Payroll list : payrolls) {
				JSONArray array = new JSONArray();
				array.add(list.getPayrollId() + "");
				array.add(list.getJobAssignment().getPerson().getFirstName()
						+ " "
						+ list.getJobAssignment().getPerson().getLastName()
						+ "-"
						+ list.getJobAssignment().getPerson().getPersonNumber());
				array.add(list.getJobAssignment().getCmpDeptLocation()
						.getCompany().getCompanyName()
						+ "");
				array.add(list.getJobAssignment().getCmpDeptLocation()
						.getDepartment().getDepartmentName()
						+ "");
				array.add(list.getJobAssignment().getCmpDeptLocation()
						.getLocation().getLocationName()
						+ "");
				array.add(DateFormat.convertDateToString(list.getStartDate()
						.toString()) + "");
				array.add(DateFormat.convertDateToString(list.getEndDate()
						.toString()) + "");
				array.add(list.getPayMonth() + "");
				array.add(list.getNetAmount().toString() + "");
				array.add(DateFormat.convertDateToString(list.getCreatedDate()
						.toString()) + "");
				if (list.getStatus() != null)
					array.add(Constants.HR.PayStatus.get(list.getStatus())
							.name());
				else
					array.add("");

				if (list.getIsApprove() != null)
					array.add(WorkflowConstants.Status.get(list.getIsApprove())
							.name() + "");
				else
					array.add("");

				array.add("<a href=\"#\" style=\"text-decoration: none; color: grey\" class=\"download\">Download</a>");

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String payrollView() {
		try {
			PayrollTO payTO = new PayrollTO();
			List<PayrollTO> payrollTransTOS = new ArrayList<PayrollTO>();
			List<PayrollTO> lopList = new ArrayList<PayrollTO>();
			List<PayrollTO> otList = new ArrayList<PayrollTO>();
			Double deductionTotal = 0.0;
			Double earningTotal = 0.0;

			if (recordId != null && recordId > 0) {
				payrollId = recordId;
				viewRedirect = "Notification";
			}

			if (payrollId != null && payrollId > 0) {
				payrollBL.getPayrollVerification(payrollId);

				payTO = payrollBL.getPayrollInfoForApproval(payrollId);
				payrollTransTOS = payTO.getPayrollTransactions();
				if (payTO.getPayrollDeductions() != null)
					payrollTransTOS.addAll(payTO.getPayrollDeductions());
				lopList = payrollBL.getLOPDetails(payrollId);
				for (PayrollTO payrollTO : lopList) {
					deductionTotal += AIOSCommons
							.formatAmountToDouble(payrollTO.getAmount());
				}

				otList = payrollBL.getOTDetails(payrollId);
				for (PayrollTO payrollTO : otList) {
					earningTotal += AIOSCommons.formatAmountToDouble(payrollTO
							.getAmount());
				}
			}
			// Comment Information --Added by rafiq
			Comment comment = new Comment();

			if (payrollId != 0) {
				try {
					comment.setRecordId(payrollId);
					comment.setUseCase("com.aiotech.aios.hr.domain.entity.Payroll");
					comment = commentBL.getCommentInfo(comment);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			ServletActionContext.getRequest().getSession()
					.setAttribute("COMMENT_IFNO", comment);

			ServletActionContext.getRequest().setAttribute("PAYROLL_MASTER",
					payTO);
			
			Collections.sort(payrollTransTOS, new Comparator<PayrollTO>() {
				public int compare(PayrollTO m1, PayrollTO m2) {
					if(m1.getFromDate()!=null)
						return m1.getFromDate().compareTo(m2.getFromDate());
					else
						return 1;
				}
			});
			ServletActionContext.getRequest().setAttribute(
					"PAYROLL_TRANSACTION_LIST", payrollTransTOS);
			
			Collections.sort(lopList, new Comparator<PayrollTO>() {
				public int compare(PayrollTO m1, PayrollTO m2) {
					if(m1.getFromDate()!=null)
						return m1.getFromDate().compareTo(m2.getFromDate());
					else
						return 1;
				}
			});
			ServletActionContext.getRequest().setAttribute("LOP_LIST", lopList);
			ServletActionContext.getRequest().setAttribute("LOP_MASTER",
					AIOSCommons.formatAmount(deductionTotal));
			
			Collections.sort(otList, new Comparator<PayrollTO>() {
				public int compare(PayrollTO m1, PayrollTO m2) {
					if(m1.getFromDate()!=null)
						return m1.getFromDate().compareTo(m2.getFromDate());
					else
						return 1;
				}
			});
			ServletActionContext.getRequest().setAttribute("OT_LIST", otList);
			ServletActionContext.getRequest().setAttribute("OT_MASTER",
					AIOSCommons.formatAmount(earningTotal));
			ServletActionContext.getRequest().setAttribute("viewRedirect",
					viewRedirect);
			recordId = null;
			payrollId = null;
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getPayrollVerification() {

		try {

			if (payrollId != null && payrollId > 0) {
				payrollBL.getPayrollVerification(payrollId);

			}
			recordId = null;
			payrollId = null;
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getPayrollVerificationAddrow() {
		List<PayrollElement> payrollElement = payrollBL.getPayrollService()
				.getAllPayrollElement(getImplementation());
		ServletActionContext.getRequest().setAttribute("PAYROLL_ELEMENTS",
				payrollElement);
		if (null == id)
			id = 0;
		ServletActionContext.getRequest().setAttribute("rowId", id);
		return SUCCESS;
	}

	public String savePayroll() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			if (payrollId != null && payrollId > 0) {
				Payroll payroll = payrollBL.getPayrollService()
						.getPayrollDetail(payrollId);
				if (payroll.getPayPeriodTransaction().getIsFreeze() != null
						&& payroll.getPayPeriodTransaction().getIsFreeze()) {
					ServletActionContext.getRequest().setAttribute(
							"RETURN_MESSAGE",
							"Salary transaction period has been closed!");
					LOGGER.info("Salary transaction period has been closed!");
					return SUCCESS;
				}
				payroll.setNetAmount(netAmount);
				payrollBL.savePayroll(payroll,
						getPayLineDetails(payroll, payLineDetail));
				returnMessage = "SUCCESS";
			}

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public List<PayrollTransaction> getPayLineDetails(Payroll payroll,
			String payLineDetail) {
		List<PayrollTransaction> payrollTransactions = new ArrayList<PayrollTransaction>(
				payroll.getPayrollTransactions());
		Map<Long, PayrollTransaction> transMap = new HashMap<Long, PayrollTransaction>();

		for (PayrollTransaction payrollTransaction : payrollTransactions) {
			transMap.put(payrollTransaction.getPayrollTransactionId(),
					payrollTransaction);
		}

		if (payLineDetail != null && !payLineDetail.equals("")) {
			payrollTransactions = new ArrayList<PayrollTransaction>();
			String[] recordArray = new String[payLineDetail.split("##").length];
			recordArray = payLineDetail.split("##");
			for (String record : recordArray) {
				PayrollTransaction payrollTransaction = new PayrollTransaction();
				PayrollElement payrollElement = new PayrollElement();
				String[] dataArray = new String[record.split("@#").length];
				dataArray = record.split("@#");

				if (!dataArray[0].equals("-1")
						&& !dataArray[0].trim().equals("")) {
					if (transMap.containsKey(Long.parseLong(dataArray[0])))
						payrollTransaction = transMap.get(Long
								.parseLong(dataArray[0]));
				} else {
					payrollTransaction.setCreatedDate(new Date());
					payrollTransaction
							.setStatus(Constants.HR.PayTransactionStatus.New
									.getCode());
					payrollTransaction.setPerson(payroll.getPerson());
					payrollTransaction.setImplementation(getImplementation());
				}
				if (!dataArray[1].equals("-1")
						&& !dataArray[1].trim().equals("")) {
					payrollElement.setPayrollElementId(Long
							.parseLong(dataArray[1]));
					payrollTransaction.setPayrollElement(payrollElement);
				}
				if (!dataArray[2].equals("-1")
						&& !dataArray[2].trim().equals(""))
					payrollTransaction.setAmount(Double.valueOf(dataArray[2]));

				if (!dataArray[3].equals("-1")
						&& !dataArray[3].trim().equals(""))
					payrollTransaction.setNote(dataArray[3]);

				payrollTransactions.add(payrollTransaction);
			}
		}
		return payrollTransactions;
	}

	public List<Payroll> getPayDetails(String payLineDetail) {
		List<Payroll> payrolls = new ArrayList<Payroll>();
		if (payLineDetail != null && !payLineDetail.equals("")) {
			String[] recordArray = new String[payLineDetail.split("##").length];
			recordArray = payLineDetail.split("##");
			for (String record : recordArray) {
				Payroll payroll = new Payroll();
				String[] dataArray = new String[record.split("@").length];
				dataArray = record.split("@");
				if (!dataArray[0].equals("-1")
						&& !dataArray[0].trim().equals(""))
					payroll.setPayrollId(Long.parseLong(dataArray[0]));
				if (!dataArray[1].equals("-1")
						&& !dataArray[1].trim().equals("")) {
					payroll.setStatus(Byte.parseByte(dataArray[1]));
				}

				payrolls.add(payroll);
			}
		}
		return payrolls;
	}

	public String wpsExecution() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();
			Map<Byte, String> payPolicy = payrollBL.getJobBL().payPolicy();
			ServletActionContext.getRequest().setAttribute("PAY_POLICY",
					payPolicy);
			jobAssignments = payrollBL.getJobAssignmentBL()
					.getJobAssignmentService()
					.getAllActiveJobAssignment(getImplementation());
			if (jobAssignments != null && jobAssignments.size() > 0) {
				JobAssignment jobAssignment = jobAssignments.get(0);
				PayrollVO payrollVO = new PayrollVO();
				payrollVO.setRecordType("SCR");
				payrollVO.setLabourCardId(jobAssignment.getCmpDeptLocation()
						.getCompany().getLabourCardId());
				BankAccount bankAccount = null;
				if (accountId != null && accountId > 0)
					bankAccount = payrollBL.getPayrollService()
							.getBankAccountInfo(accountId);
				if (bankAccount != null)
					payrollVO.setRoutingCode(bankAccount.getRountingNumber());

				SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd");
				payrollVO.setCreatedDateStr(outFormat.format(new Date()));

				SimpleDateFormat timeFormat = new SimpleDateFormat("HHMM");
				payrollVO.setCreatedTime(timeFormat.format(new Date()));

				List<PayrollVO> payrollVOs = payrollBL.wpsExecution(
						jobAssignments, payMonth + "," + payYear);
				if (payrollVOs != null && payrollVOs.size() > 0) {

					SimpleDateFormat formatter = new SimpleDateFormat(
							"MMM,yyyy");
					Date tempDate = formatter.parse(payrollVOs.get(0)
							.getPayMonth());
					SimpleDateFormat outFormat1 = new SimpleDateFormat("MMyyyy");
					payrollVO.setPayMonth(outFormat1.format(tempDate));

					payrollVO.setNumberOfRecord(payrollVOs.size());
					Double totalSum = 0.0;
					for (PayrollVO payrollVO2 : payrollVOs) {
						totalSum += payrollVO2.getNetAmount();
					}
					payrollVO.setNetAmountStr(AIOSCommons
							.roundTwoDecimalsWPS(totalSum));
					payrollVO.setCurrency("AED");
					payrollVO.setDocumentryField("");
				}

				ServletActionContext.getRequest().setAttribute("PAYROLL_EDR",
						payrollVOs);
				ServletActionContext.getRequest().setAttribute("PAYROLL_SCR",
						payrollVO);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String wpsDownload() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();
			jobAssignments = payrollBL.getJobAssignmentBL()
					.getJobAssignmentService()
					.getAllActiveJobAssignment(getImplementation());
			if (jobAssignments != null && jobAssignments.size() > 0) {

				String str = "";

				JobAssignment jobAssignment = jobAssignments.get(0);
				PayrollVO payrollVO = new PayrollVO();
				payrollVO.setRecordType("SCR");
				payrollVO.setLabourCardId(jobAssignment.getCmpDeptLocation()
						.getCompany().getLabourCardId());
				BankAccount bankAccount = null;
				if (accountId != null && accountId > 0)
					bankAccount = payrollBL.getPayrollService()
							.getBankAccountInfo(accountId);
				if (bankAccount != null)
					payrollVO.setRoutingCode(bankAccount.getRountingNumber());

				SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd");
				payrollVO.setCreatedDateStr(outFormat.format(new Date()));

				SimpleDateFormat timeFormat = new SimpleDateFormat("HHMM");
				payrollVO.setCreatedTime(timeFormat.format(new Date()));

				List<PayrollVO> payrollVOs = payrollBL.wpsExecution(
						jobAssignments, payMonth + "," + payYear);
				if (payrollVOs != null && payrollVOs.size() > 0) {

					SimpleDateFormat formatter = new SimpleDateFormat(
							"MMM,yyyy");
					Date tempDate = formatter.parse(payrollVOs.get(0)
							.getPayMonth());
					SimpleDateFormat outFormat1 = new SimpleDateFormat("MMyyyy");
					payrollVO.setPayMonth(outFormat1.format(tempDate));

					payrollVO.setNumberOfRecord(payrollVOs.size());
					Double totalSum = 0.0;
					for (PayrollVO payrollVO2 : payrollVOs) {
						str += payrollVO2.getRecordType()
								+ ","
								+ (payrollVO2.getLabourCardId() != null ? payrollVO2
										.getLabourCardId() : "")
								+ ","
								+ (payrollVO2.getRoutingCode() != null ? payrollVO2
										.getRoutingCode() : "")
								+ ","
								+ (payrollVO2.getIbanNumber() != null ? payrollVO2
										.getIbanNumber() : "")
								+ ","
								+ (payrollVO2.getPeriodStart() != null ? payrollVO2
										.getPeriodStart() : "")
								+ ","
								+ (payrollVO2.getPeriodEnd() != null ? payrollVO2
										.getPeriodEnd() : "")
								+ ","
								+ (payrollVO2.getNumberOfDays() != null ? payrollVO2
										.getNumberOfDays() : "")
								+ ","
								+ (payrollVO2.getNetAmountStr() != null ? payrollVO2
										.getNetAmountStr() : "")
								+ ","
								+ (payrollVO2.getVariableSalary() != null ? payrollVO2
										.getVariableSalary() : "")
								+ ","
								+ (payrollVO2.getLopDays() != null ? payrollVO2
										.getLopDays() : "") + "\r\n";
						totalSum += payrollVO2.getNetAmount();
					}
					payrollVO.setNetAmountStr(AIOSCommons
							.roundTwoDecimalsWPS(totalSum));
					payrollVO.setCurrency("AED");
					payrollVO.setDocumentryField("");
				}

				str += payrollVO.getRecordType()
						+ ","
						+ (payrollVO.getLabourCardId() != null ? payrollVO
								.getLabourCardId() : "")
						+ ","
						+ (payrollVO.getRoutingCode() != null ? payrollVO
								.getRoutingCode() : "")
						+ ","
						+ (payrollVO.getCreatedDateStr() != null ? payrollVO
								.getCreatedDateStr() : "")
						+ ","
						+ (payrollVO.getCreatedTime() != null ? payrollVO
								.getCreatedTime() : "")
						+ ","
						+ (payrollVO.getPayMonth() != null ? payrollVO
								.getPayMonth() : "")
						+ ","
						+ (payrollVO.getNumberOfRecord() != null ? payrollVO
								.getNumberOfRecord() : "")
						+ ","
						+ (payrollVO.getNetAmountStr() != null ? payrollVO
								.getNetAmountStr() : "")
						+ ","
						+ (payrollVO.getCurrency() != null ? payrollVO
								.getCurrency() : "")
						+ ","
						+ (payrollVO.getDocumentryField() != null ? payrollVO
								.getDocumentryField() : "");

				InputStream is = new ByteArrayInputStream(str.getBytes());
				fileInputStream = is;
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getPayrollListToUpdate() {
		try {

			List<PayrollVO> payrollVOs = payrollBL.getPayrollUpdate();
			Collections.sort(payrollVOs, new Comparator<PayrollVO>() {
				public int compare(PayrollVO m1, PayrollVO m2) {
					return m1.getStatus().compareTo(m2.getStatus());
				}
			});

			ServletActionContext.getRequest().setAttribute("PAYROLL_LIST",
					payrollVOs);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String updatePayStatus() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			if (accountId != null && accountId > 0 && payLineDetail != null
					&& !payLineDetail.equalsIgnoreCase("")) {

				returnMessage = payrollBL.updatePayStatus(accountId,
						getPayDetails(payLineDetail), exchangeRate);
			}

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return ERROR;
		}
	}

	public String getPayrollReportPrint() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Boolean isTemplatePrintEnabled = session
					.getAttribute("print_payroll_template") != null ? ((String) session
					.getAttribute("print_payroll_template"))
					.equalsIgnoreCase("true") : false;

			List<PayrollVO> payrollVOs = null;
			List<PayrollVO> finalPayrollVOs = null;
			PayrollVO payrollVO = new PayrollVO();
			payrollVOs = payrollBL.getPayrollToReportPrint(payMonth + ","
					+ payYear);
			// payrollVO.setPayMonth(payMonth + "," + payYear);
			payrollVO.setCreatedDateStr(DateFormat
					.convertSystemDateToString(new Date()));
			Double totalBasic = 0.0;
			Double totalEarning = 0.0;
			Double totalDeduction = 0.0;
			Double totalGross = 0.0;
			Double totalNet = 0.0;
			Double totalOT = 0.0;
			if (payrollVOs != null && payrollVOs.size() > 0) {
				finalPayrollVOs = new ArrayList<PayrollVO>(payrollVOs);
				for (PayrollVO payrollVOTemp : payrollVOs) {
					if (locationId != 0
							&& payrollVOTemp.getJobAssignment()
									.getCmpDeptLocation().getLocation()
									.getLocationId() != locationId) {
						finalPayrollVOs.remove(payrollVOTemp);
						continue;
					}

					if (payrollVOTemp.getBasic() != null)
						totalBasic = totalBasic
								+ AIOSCommons
										.formatAmountToDouble(payrollVOTemp
												.getBasic());

					if (payrollVOTemp.getOverTime() != null)
						totalOT = totalOT
								+ AIOSCommons
										.formatAmountToDouble(payrollVOTemp
												.getOverTime());

					if (payrollVOTemp.getGrossPay() != null)
						totalGross = totalGross
								+ AIOSCommons
										.formatAmountToDouble(payrollVOTemp
												.getGrossPay());

					if (payrollVOTemp.getOtherEarning() != null)
						totalEarning = totalEarning
								+ AIOSCommons
										.formatAmountToDouble(payrollVOTemp
												.getOtherEarning());
					if (payrollVOTemp.getOtherDeduction() != null)
						totalDeduction = totalDeduction
								+ AIOSCommons
										.formatAmountToDouble(payrollVOTemp
												.getOtherDeduction());
					if (payrollVOTemp.getNetAmount() != null)
						totalNet = totalNet
								+ AIOSCommons
										.formatAmountToDouble(payrollVOTemp
												.getNetAmount());
					for (Map.Entry<String, String> entry : payrollVOTemp
							.getPayrollCodeMaps().entrySet()) {
						if (!entry.getKey().equals("BASIC"))
							payrollVO.getPayrollCodeMaps().put(entry.getKey(),
									entry.getValue());
					}

				}
				payrollVO.setTotalBasic(AIOSCommons.formatAmount(totalBasic));
				payrollVO.setTotalGross(AIOSCommons.formatAmount(totalGross));
				payrollVO.setTotalEarning(AIOSCommons
						.formatAmount(totalEarning));
				payrollVO.setTotalDeduction(AIOSCommons
						.formatAmount(totalDeduction));
				payrollVO.setTotalNet(AIOSCommons.formatAmount(totalNet));
				payrollVO.setOverTime(AIOSCommons.formatAmount(totalOT));
				payrollVO.setCompanyName(payrollVOs.get(0).getCompanyName());

				Collections.sort(payrollVOs, new Comparator<PayrollVO>() {
					public int compare(PayrollVO m1, PayrollVO m2) {
						return m1.getEmployeeName().compareTo(
								m2.getEmployeeName());
					}
				});
				
			}
			if (!isTemplatePrintEnabled) {
				ServletActionContext.getRequest().setAttribute(
						"PAYROLL_MASTER", payrollVO);
				ServletActionContext.getRequest().setAttribute(
						"PAYROLL_DETAILS", finalPayrollVOs);
				ServletActionContext.getRequest().setAttribute("printCall",
						printCall);
				return "default";
			} else {
				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/hr/"
						+ getImplementation().getImplementationId()));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config
						.getTemplate("html-payroll-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();

				if (getImplementation().getMainLogo() != null) {

					byte[] bFile = FileUtil.getBytes(new File(
							getImplementation().getMainLogo()));
					bFile = Base64.encodeBase64(bFile);

					rootMap.put("logo", new String(bFile));
				} else {
					rootMap.put("logo", "");
				}

				if(finalPayrollVOs!=null){
					Collections.sort(finalPayrollVOs, new Comparator<PayrollVO>() {
						public int compare(PayrollVO m1, PayrollVO m2) {
							return m1.getEmployeeName().compareTo(
									m2.getEmployeeName());
						}
					});
					
					Collections.sort(finalPayrollVOs, new Comparator<PayrollVO>() {
						public int compare(PayrollVO m1, PayrollVO m2) {
							return m1.getDepartmentName().compareTo(
									m2.getDepartmentName());
						}
					});

				}
				
				rootMap.put("urlPath", urlPath);
				rootMap.put("countCol", payrollVO.getPayrollCodeMaps().size());
				rootMap.put("PAYROLL_MASTER", payrollVO);
				rootMap.put("PAYROLL_DETAILS", finalPayrollVOs);
				rootMap.put("selectedMonth", payMonth);
				rootMap.put("selectedYear", payYear);
				rootMap.put("company", getImplementation().getCompanyName());
				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
				// System.out.println(printableContent);

				return "template";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String savePayTransactionByPeriod() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		String returnMessage = "SUCCESS";
		try {

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			Payroll payroll = new Payroll();
			;
			PayPeriodTransaction payPeriod = payrollBL.getPayPeriodBL()
					.getPayPeriodService()
					.getPayPeriodTransactionById(payPeriodId);
			if (payPeriod.getIsFreeze() != null && payPeriod.getIsFreeze()) {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE",
						"Salary transaction period has been closed!");
				LOGGER.info("Salary transaction period has been closed!");
				return SUCCESS;
			}

			if (payrollId != null && payrollId > 0) {

				payroll = this
						.getPayrollBL()
						.getPayrollService()
						.getPayrollDetailByUseCase(jobAssignmentId,
								"PayPeriod" + payPeriodId);

			} else {
				payroll = new Payroll();

			}
			JobAssignment jobAssignment = null;
			if (jobAssignmentId != null) {
				jobAssignment = this.getPayrollBL().getJobAssignmentBL()
						.getJobAssignmentService()
						.getJobAssignmentInfo(jobAssignmentId);
				jobAssignment.setJobAssignmentId(jobAssignmentId);
				payroll.setJobAssignment(jobAssignment);
			}
			payroll.setPayPeriodTransaction(payPeriod);
			payroll.setUseCase("PayPeriod" + payPeriodId);
			payroll.setRecordId(jobAssignmentId);
			payroll.setImplementation(getImplementation());
			payroll.setPerson(jobAssignment.getPerson());
			payroll.setJobAssignment(jobAssignment);
			payroll.setStatus(Constants.HR.PayStatus.InProcess.getCode());

			payroll.setIsApprove(Byte
					.parseByte(WorkflowConstants.Status.Published.getCode()
							+ ""));
			payroll.setIsSalary(false);
			payroll.setCreatedDate(new Date());
			payroll.setStartDate(payPeriod.getStartDate());
			payroll.setEndDate(payPeriod.getEndDate());
			payroll.setIsLedgerTransaction(false);
			List<PayrollTransaction> transactions = getPayLineDetails(payroll,
					payLineDetail);

			payrollBL.directSavePayroll(payroll, transactions);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("resignationTermination() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (ArithmeticException ae) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Failure in creating the resignationTermination");
			return ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			if (payrollId != null && payrollId > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("resignationTermination() Info unsuccessful Return");
			return ERROR;
		} finally {

		}
	}

	public String getPayTransaction() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			List<JobAssignment> persons = payrollBL.getJobAssignmentBL()
					.getJobAssignmentService()
					.getAllActiveJobAssignment(getImplementation());
			ServletActionContext.getRequest().setAttribute(
					"JOB_ASSIGNMENT_LIST", persons);

			String[] nameOfMonth = new DateFormatSymbols().getMonths();
			ServletActionContext.getRequest().setAttribute("PAYMONTH_LIST",
					nameOfMonth);

			Map<Byte, String> payPolicy = payrollBL.getJobBL().payPolicy();
			Map<Byte, String> payMode = payrollBL.getJobBL().payMode();
			ServletActionContext.getRequest().setAttribute("PAY_POLICY",
					payPolicy);
			ServletActionContext.getRequest().setAttribute("PAY_MODE", payMode);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return ERROR;
		}
	}

	public String getPayTransactionElements() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			JobPayrollElementVO payVO = new JobPayrollElementVO();
			payVO.setJobAssignmentId(jobAssignmentId);
			payVO.setPayPolicy(payPolicy);
			payVO.setPayType(payType);
			payVO.setPayMonth(payMonth);
			payVO.setPayYear(payYear);
			List<JobPayrollElementVO> jobPayrollElementVOs = payrollBL
					.jobPayrollElements(payVO);
			ServletActionContext.getRequest().setAttribute("JOB_PAY_ELEMENTS",
					jobPayrollElementVOs);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return ERROR;
		}
	}

	public String getBasicPay() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			JobPayrollElementVO payVO = new JobPayrollElementVO();
			payVO.setJobAssignmentId(jobAssignmentId);
			payVO.setPayPolicy(payPolicy);
			payVO.setPayType(payType);
			payVO.setPayMonth(payMonth);
			payVO.setPayYear(payYear);
			List<JobPayrollElementVO> jobPayrollElementVOs = payrollBL
					.getBasicPayrollElements(payVO);
			if (jobPayrollElementVOs != null && jobPayrollElementVOs.size() > 0)
				ServletActionContext.getRequest().setAttribute("basicAmount",
						jobPayrollElementVOs.get(0).getAmount());

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return ERROR;
		}
	}

	public String periodBasedTransaction() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			PayrollVO payrollVO = null;
			if (jobAssignmentId != null && jobAssignmentId > 0) {

				payrollVO = new PayrollVO();
				payrollVO.setJobAssignmentId(jobAssignmentId);
				payrollVO.setPayPeriodId(payPeriodId);
				payrollVO = payrollBL.periodBasedTransaction(payrollVO);
				List<PayrollElement> payrollElements = payrollBL
						.getPayrollService().getAllPayrollElement(
								getImplementation());

				ServletActionContext.getRequest().setAttribute(
						"PAYROLL_ELEMENTS", payrollElements);
			}

			ServletActionContext.getRequest().setAttribute("PAYROLL_PROCESS",
					payrollVO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	// Getter & setter
	public PayrollBL getPayrollBL() {
		return payrollBL;
	}

	public void setPayrollBL(PayrollBL payrollBL) {
		this.payrollBL = payrollBL;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Long getPayrollId() {
		return payrollId;
	}

	public void setPayrollId(Long payrollId) {
		this.payrollId = payrollId;
	}

	public String getPersons() {
		return persons;
	}

	public void setPersons(String persons) {
		this.persons = persons;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPayLineDetail() {
		return payLineDetail;
	}

	public void setPayLineDetail(String payLineDetail) {
		this.payLineDetail = payLineDetail;
	}

	public Double getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(Double netAmount) {
		this.netAmount = netAmount;
	}

	public String getPayMonth() {
		return payMonth;
	}

	public void setPayMonth(String payMonth) {
		this.payMonth = payMonth;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getPayYear() {
		return payYear;
	}

	public void setPayYear(String payYear) {
		this.payYear = payYear;
	}

	public Double getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(Double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public Byte getPayPolicy() {
		return payPolicy;
	}

	public void setPayPolicy(Byte payPolicy) {
		this.payPolicy = payPolicy;
	}

	public Byte getPayType() {
		return payType;
	}

	public void setPayType(Byte payType) {
		this.payType = payType;
	}

	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}

	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Long getPayPeriodTransactionId() {
		return payPeriodTransactionId;
	}

	public void setPayPeriodTransactionId(Long payPeriodTransactionId) {
		this.payPeriodTransactionId = payPeriodTransactionId;
	}

	public String getPrintCall() {
		return printCall;
	}

	public void setPrintCall(String printCall) {
		this.printCall = printCall;
	}

	public String getViewRedirect() {
		return viewRedirect;
	}

	public void setViewRedirect(String viewRedirect) {
		this.viewRedirect = viewRedirect;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public Long getPayPeriodId() {
		return payPeriodId;
	}

	public void setPayPeriodId(Long payPeriodId) {
		this.payPeriodId = payPeriodId;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}

	public long getLocationId() {
		return locationId;
	}

	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}

}
