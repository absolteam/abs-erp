
package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobPayrollElement;
import com.aiotech.aios.hr.domain.entity.PayByCount;
import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.JobPayrollElementVO;
import com.aiotech.aios.hr.domain.entity.vo.PayByCountVO;
import com.aiotech.aios.hr.domain.entity.vo.PayPeriodVO;
import com.aiotech.aios.hr.service.bl.PayByCountBL;
import com.aiotech.aios.hr.service.bl.PayrollBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class PayByCountAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(PayByCountAction.class);
	private Implementation implementation;
	private PayByCountBL payByCountBL;
	private PayrollBL payrollBL;
	private String transactionDate;
	private Integer id;
	private Long jobAssignmentId;
	private Long jobPayrollElementId;
	private List<Object> aaData;
	private Byte payPolicy;
	private Double amount;
	private String fromDate;
	private String toDate;
	private String receiptNumber;
	private String isFinanceImpact;
	private Long payPeriodTransactionId;
	private String description;

	private Long payByCountId;
	private Integer count;
	private Byte status;
	
	public String returnSuccess() {
		ServletActionContext.getRequest().setAttribute("jobAssignmentId",
				jobAssignmentId);
		ServletActionContext.getRequest().setAttribute("jobPayrollElementId",
				jobPayrollElementId);
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String getPayByCountList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();
			// Find the pay period of current transaction
			PayPeriodVO payPeriodVO = new PayPeriodVO();
			payPeriodVO.setJobAssignmentId(jobAssignmentId);
			payPeriodVO.setTransactionDate(DateFormat
					.convertStringToDate(transactionDate));
			payPeriodVO.setPayPolicy(payPolicy);
			payPeriodVO.setPayPeriodTransactionId(payPeriodTransactionId);

			PayPeriodTransaction payPeriodTransaction = payByCountBL
					.getPayPeriodBL().findPayPeriod(payPeriodVO);

			PayByCountVO payVO = new PayByCountVO();
			if (payPeriodTransaction != null)
				payVO.setPayPeriodTransaction(payPeriodTransaction);

			payVO.setJobAssignmentId(jobAssignmentId);
			payVO.setJobPayrollElementId(jobPayrollElementId);

			List<PayByCount> counts = payByCountBL
					.getPayByCountService()
					.getAllPayByCountByCriteria(payVO);

			if (counts != null && counts.size() > 0) {
				PayByCountVO vo = null;
				for (PayByCount list : counts) {
					vo = new PayByCountVO();
					vo.setPayByCountId(list.getPayByCountId());
					
					vo.setFromDateView(DateFormat.convertDateToString(list
							.getFromDate().toString()));
					vo.setToDateView(DateFormat.convertDateToString(list
							.getToDate().toString()));
					vo.setCount(list.getCount());
					vo.setAmount(list.getAmount());
					if (list.getIsFinanceImpact() != null
							&& list.getIsFinanceImpact() == true)
						vo.setIsFinanceView("YES");
					else
						vo.setIsFinanceView("NO");

					aaData.add(vo);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getPayByCount() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			PayByCountVO payByCountVO = null;
			JobPayrollElementVO jobPayrollElementVO = null;
			if (payByCountId != null && payByCountId > 0) {
				PayByCount payByCount = payByCountBL
						.getPayByCountService().getPayByCountById(
								payByCountId);
				if (payByCount != null)
					payByCountVO = payByCountBL
							.convertEntityToVO(payByCount);
			}

			// Find the Maximum provided & actual earned amount
			{
				// Find the pay period of current transaction
				PayPeriodVO payPeriodVO = new PayPeriodVO();
				payPeriodVO.setJobAssignmentId(jobAssignmentId);
				payPeriodVO.setTransactionDate(DateFormat
						.convertStringToDate(transactionDate));
				payPeriodVO.setPayPolicy(payPolicy);
				payPeriodVO.setPayPeriodTransactionId(payPeriodTransactionId);

				PayPeriodTransaction payPeriodTransaction = payByCountBL
						.getPayPeriodBL().findPayPeriod(payPeriodVO);
				PayByCountVO payByCountVOTemp = new PayByCountVO();
				if (payPeriodTransaction != null)
					payByCountVOTemp
							.setPayPeriodTransaction(payPeriodTransaction);

				payByCountVOTemp.setJobAssignmentId(jobAssignmentId);
				payByCountVOTemp
						.setJobPayrollElementId(jobPayrollElementId);

				List<PayByCount> payCounts = payByCountBL
						.getPayByCountService()
						.getAllPayByCountByCriteria(
								payByCountVOTemp);

				jobPayrollElementVO = payrollBL.getJobPayrollInfomration(
						jobPayrollElementId, new ArrayList<Object>(
								payCounts), payPeriodTransaction);

				ServletActionContext.getRequest().setAttribute("JOBPAY_INFO",
						jobPayrollElementVO);
			}


			ServletActionContext.getRequest().setAttribute("ALLOWANCE",
					payByCountVO);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String savePayByCount() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		String returnMessage = "SUCCESS";
		try {

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			PayByCount payByCount = new PayByCount();
			if (payByCountId != null && payByCountId > 0) {
				payByCount = payByCountBL.getPayByCountService()
						.getPayByCountById(payByCountId);

			} else {
				payByCount.setCreatedDate(new Date());
				payByCount.setPersonByCreatedBy(person);
			}
			payByCount.setAmount(amount);
			if (fromDate != null && !fromDate.equals(""))
				payByCount.setFromDate(DateFormat
						.convertStringToDate(fromDate));
			if (toDate != null && !toDate.equals(""))
				payByCount.setToDate(DateFormat
						.convertStringToDate(toDate));

			if (fromDate != null && !fromDate.equals(""))
				payByCount.setFromDate(DateFormat
						.convertStringToDate(fromDate));
			
			
			payByCount.setImplementation(getImplementation());

			payByCount.setReceiptNumber(receiptNumber);
			if (isFinanceImpact != null
					&& isFinanceImpact.equalsIgnoreCase("true"))
				payByCount.setIsFinanceImpact(true);
			else
				payByCount.setIsFinanceImpact(false);
			if (jobAssignmentId != null) {
				JobAssignment jobAssignment = new JobAssignment();
				jobAssignment.setJobAssignmentId(jobAssignmentId);
				payByCount.setJobAssignment(jobAssignment);
			}
			if (jobPayrollElementId != null) {
				JobPayrollElement jobPayrollElement = new JobPayrollElement();
				jobPayrollElement.setJobPayrollElementId(jobPayrollElementId);
				payByCount.setJobPayrollElement(jobPayrollElement);
			}
			
			if (payPeriodTransactionId != null) {
				PayPeriodTransaction payPeriodTransaction = new PayPeriodTransaction();
				payPeriodTransaction.setPayPeriodTransactionId(payPeriodTransactionId);
				payByCount.setPayPeriodTransaction(payPeriodTransaction);
			}
			
			payByCount.setDescription(description);
			payByCount.setCount(count);
			
			returnMessage = payByCountBL
					.savePayByCount(payByCount);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("payByCount() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (ArithmeticException ae) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Failure in creating the payByCount");
			return ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			if (payByCountId != null && payByCountId > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("savepayByCountId() Info unsuccessful Return");
			return ERROR;
		} finally {

		}
	}

	public String deletePayByCount() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			String returnMessage = "SUCCESS";
			PayByCount payByCount = new PayByCount();
			if (payByCountId != null && payByCountId > 0) {
				payByCount = payByCountBL.getPayByCountService()
						.getPayByCountById(payByCountId);

			}
			returnMessage = payByCountBL
					.deletePayByCount(payByCount);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("deletePayByCount() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deletePayByCount() Info unsuccessful Return");
			return ERROR;
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public PayrollBL getPayrollBL() {
		return payrollBL;
	}

	public void setPayrollBL(PayrollBL payrollBL) {
		this.payrollBL = payrollBL;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}

	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}

	public Long getJobPayrollElementId() {
		return jobPayrollElementId;
	}

	public void setJobPayrollElementId(Long jobPayrollElementId) {
		this.jobPayrollElementId = jobPayrollElementId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Byte getPayPolicy() {
		return payPolicy;
	}

	public void setPayPolicy(Byte payPolicy) {
		this.payPolicy = payPolicy;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getIsFinanceImpact() {
		return isFinanceImpact;
	}

	public void setIsFinanceImpact(String isFinanceImpact) {
		this.isFinanceImpact = isFinanceImpact;
	}

	public Long getPayPeriodTransactionId() {
		return payPeriodTransactionId;
	}

	public void setPayPeriodTransactionId(Long payPeriodTransactionId) {
		this.payPeriodTransactionId = payPeriodTransactionId;
	}



	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public PayByCountBL getPayByCountBL() {
		return payByCountBL;
	}

	public void setPayByCountBL(PayByCountBL payByCountBL) {
		this.payByCountBL = payByCountBL;
	}

	public Long getPayByCountId() {
		return payByCountId;
	}

	public void setPayByCountId(Long payByCountId) {
		this.payByCountId = payByCountId;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}}
