package com.aiotech.aios.hr.action;

import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.PersonBank;
import com.aiotech.aios.hr.service.bl.PersonBankBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class PersonBankAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(PersonBankAction.class);
	private PersonBankBL personBankBL;
	private Integer id;
	private Implementation implementation;
	private String returnMessage;
	private Long personId;
	private Long personBankId;
	private String bankName;
	private String accountNumber;
	private String routingCode;
	private String iban;
	private String branchName;
	private String description;
	private String bankCode;
	private String accountTitle;
	private String branchCity;
	private Boolean isActive;
	private Long recordId;

	// JSON Listing Redirect
	public String returnSuccess() {
		if (null == id)
			id = 0;
		ServletActionContext.getRequest().setAttribute("rowId", id);
		ServletActionContext.getRequest().setAttribute("PERSON_ID", personId);

		return SUCCESS;
	}

	// Listing JSON
	public String getAllPersonBank() {

		try {
			List<PersonBank> personBanks = personBankBL.getPersonBankService()
					.getAllPersonBank(getImplementation());
			JSONObject jsonResponse = new JSONObject();
			if (personBanks != null && personBanks.size() > 0) {
				jsonResponse.put("iTotalRecords", personBanks.size());
				jsonResponse.put("iTotalDisplayRecords", personBanks.size());
			}
			JSONArray data = new JSONArray();
			for (PersonBank list : personBanks) {

				JSONArray array = new JSONArray();
				array.add(list.getPersonBankId());
				if (list.getPerson() != null)
					array.add(list.getPerson().getFirstName() + " "
							+ list.getPerson().getLastName() + " ["
							+ list.getPerson().getPersonNumber() + "]");
				else
					array.add("");
				array.add(list.getAccountNumber() + "");
				array.add(list.getBankName() + "");
				array.add(list.getIban() + "");
				array.add(list.getRoutingCode() + "");
				array.add(list.getBranchName() + "");
				if (list.getIsActive())
					array.add("Enabled");
				else
					array.add("Disabled");
				array.add(list.getDescription() + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Listing JSON
	public String getPersonBankList() {

		try {
			List<PersonBank> personBanks = personBankBL.getPersonBankService()
					.getPersonBanksByPerson(personId);
			JSONObject jsonResponse = new JSONObject();
			if (personBanks != null && personBanks.size() > 0) {
				jsonResponse.put("iTotalRecords", personBanks.size());
				jsonResponse.put("iTotalDisplayRecords", personBanks.size());
			}
			JSONArray data = new JSONArray();
			for (PersonBank list : personBanks) {

				JSONArray array = new JSONArray();
				array.add(list.getPersonBankId());
				array.add(list.getPerson().getFirstName() + " "
						+ list.getPerson().getLastName() + " ["
						+ list.getPerson().getPersonNumber() + "]");
				array.add(list.getAccountNumber() + "");
				array.add(list.getBankName() + "");
				array.add(list.getIban() + "");
				array.add(list.getRoutingCode() + "");
				array.add(list.getBranchName() + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getPersonBankAdd() {
		try {
			PersonBank personBank = null;
			if(recordId!=null && recordId>0){
				personBankId=recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (personBankId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(AttendancePolicy.class.getName());
					comment = personBankBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			}
			if (personBankId != null && personBankId > 0) {
				personBank = personBankBL.getPersonBankService()
						.getPersonBankInfo(personBankId);
			}
			ServletActionContext.getRequest().setAttribute("PERSON_BANK_INFO",
					personBank);
			LOGGER.info("getPersonBankAdd() Info Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			LOGGER.error("getPersonBankAdd() Info unsuccessful Return");
			return ERROR;
		}
	}

	public String savePersonBank() {
		try {
			PersonBank personBank = new PersonBank();
			if (personBankId != null && personBankId > 0) {
				personBank = personBankBL.getPersonBankService()
						.getPersonBankInfo(personBankId);
			}

			Person person = null;
			if (personId != null && personId > 0) {
				person = new Person();
				person.setPersonId(personId);
				personBank.setPerson(person);
			}
			personBank.setAccountTitle(accountTitle);
			personBank.setAccountNumber(accountNumber);
			personBank.setBankCode(bankCode);
			personBank.setBankName(bankName);
			personBank.setBranchName(branchName);
			personBank.setIban(iban);
			personBank.setImplementation(getImplementation());
			personBank.setIsActive(isActive);
			personBank.setRoutingCode(routingCode);
			personBank.setBranchCity(branchCity);
			personBank.setDescription(description);
			personBankBL.savePersonBank(personBank);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("savePersonBank() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			if (personBankId > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("savePersonBank() Info unsuccessful Return");
			return ERROR;
		}
	}

	public String deletePersonBank() {
		try {
			if (personBankId != null && personBankId > 0) {

				personBankBL.deletePersonBank(personBankId);
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "No record selected to delete");
			}

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("deleteAssetUsage() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure.. Information is in use already.");

			LOGGER.error("deleteAssetUsage() Info unsuccessful Return");
			return ERROR;
		}
	}

	public PersonBankBL getPersonBankBL() {
		return personBankBL;
	}

	public void setPersonBankBL(PersonBankBL personBankBL) {
		this.personBankBL = personBankBL;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public Long getPersonBankId() {
		return personBankId;
	}

	public void setPersonBankId(Long personBankId) {
		this.personBankId = personBankId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getRoutingCode() {
		return routingCode;
	}

	public void setRoutingCode(String routingCode) {
		this.routingCode = routingCode;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getAccountTitle() {
		return accountTitle;
	}

	public void setAccountTitle(String accountTitle) {
		this.accountTitle = accountTitle;
	}

	public String getBranchCity() {
		return branchCity;
	}

	public void setBranchCity(String branchCity) {
		this.branchCity = branchCity;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

}
