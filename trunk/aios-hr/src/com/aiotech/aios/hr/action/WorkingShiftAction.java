package com.aiotech.aios.hr.action;

import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Leave;
import com.aiotech.aios.hr.domain.entity.WorkingShift;
import com.aiotech.aios.hr.domain.entity.vo.WorkingShiftVO;
import com.aiotech.aios.hr.service.bl.WorkingShiftBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class WorkingShiftAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(WorkingShiftAction.class);
	private Implementation implementation;
	private Integer id;
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private String returnMessage;
	private Long workingShiftId;
	private String workingShiftName;
	private String startTime;
	private String endTime;
	private String lateAttendance;
	private String seviorLateAttendance;
	private String bufferTime;
	private String breakHours;
	private String irregularDay;
	private Boolean isActive;
	private Boolean isDefault;
	private String breakStart;
	private String breakEnd;
	private Long recordId;
	private Long cmpDeptLocId;

	private WorkingShiftBL workingShiftBL;

	public String returnSuccess() {

		return SUCCESS;
	}

	// Listing JSON
	public String getWorkingShiftList() {

		try {
			List<WorkingShift> workingShifts = workingShiftBL
					.getWorkingShiftService().getWorkingShiftList(
							getImplementation());
			List<WorkingShiftVO> workingShiftVOs = workingShiftBL
					.convertEntitiesTOVOs(workingShifts);

			JSONObject jsonResponse = new JSONObject();
			if (workingShifts != null && workingShifts.size() > 0) {
				jsonResponse.put("iTotalRecords", workingShifts.size());
				jsonResponse.put("iTotalDisplayRecords", workingShifts.size());
			}
			JSONArray data = new JSONArray();
			for (WorkingShiftVO list : workingShiftVOs) {

				JSONArray array = new JSONArray();
				array.add(list.getWorkingShiftId() + "");
				array.add(list.getWorkingShiftName() + "");
				array.add(list.getStartTimeStr() + "");
				array.add(list.getEndTimeStr() + "");
				array.add(list.getLateAttendanceStr() + "");
				array.add(list.getSeviorLateAttendanceStr() + "");
				array.add(list.getBufferTimeStr() + "");
				array.add(list.getBreakHoursStr() + "");
				array.add(list.getIrregularDay() + "");
				if(list.getCmpDeptLocation()!=null)
					array.add(list.getCmpDeptLocation().getLocation().getLocationName() + "");
				else
					array.add("");
				array.add(list.getIsDefaultStr() + "");
				array.add(list.getIsActiveStr() + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info(" getWorkingShiftList() Sucessfull Return");
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(" getWorkingShiftList() unsuccessful Return");
			return ERROR;
		}
	}

	public String getWorkingShiftAdd() {
		try {
			WorkingShift workingShift = new WorkingShift();
			WorkingShiftVO workingShiftVO = null;
			if(recordId!=null && recordId>0){
				workingShiftId=recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (workingShiftId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(WorkingShift.class.getName());
					comment = workingShiftBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			}
			if (workingShiftId != null && workingShiftId > 0) {
				workingShift = workingShiftBL.getWorkingShiftService()
						.getWorkingShiftInfo(workingShiftId);
				workingShiftVO = workingShiftBL.convertEntityToVO(workingShift);
			}

			ServletActionContext.getRequest().setAttribute(
					"WORKING_SHIFT_INFO", workingShiftVO);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;

		}
	}

	public String saveWorkingShift() {
		try {

			// List<WorkingShift>
			// workingShifts=workingShiftBL.getWorkingShiftService().getWorkingShiftList(getImplementation());

			WorkingShift workingShift = new WorkingShift();
			if (workingShiftId != null && workingShiftId > 0) {
				workingShift = workingShiftBL.getWorkingShiftService()
						.getWorkingShiftInfo(workingShiftId);
			}

			workingShift.setWorkingShiftName(workingShiftName);

			if (startTime != null && !startTime.equals(""))
				workingShift.setStartTime(DateFormat
						.convertStringToTime(startTime));
			if (endTime != null && !endTime.equals(""))
				workingShift
						.setEndTime(DateFormat.convertStringToTime(endTime));
			if (bufferTime != null && !bufferTime.equals(""))
				workingShift.setBufferTime(DateFormat
						.convertStringToTime(bufferTime));
			else
				workingShift.setBufferTime(null);
			
			if (breakHours != null && !breakHours.trim().equals(""))
				workingShift.setBreakHours(DateFormat
						.convertStringToTime(breakHours));
			else
				workingShift.setBreakHours(null);
			
			if (lateAttendance != null && !lateAttendance.trim().equals(""))
				workingShift.setLateAttendance(DateFormat
						.convertStringToTime(lateAttendance));
			else
				workingShift.setLateAttendance(null);
			
			if (seviorLateAttendance != null
					&& !seviorLateAttendance.trim().equals(""))
				workingShift.setSeviorLateAttendance(DateFormat
						.convertStringToTime(seviorLateAttendance));
			else
				workingShift.setSeviorLateAttendance(null);

			if (breakStart != null && !breakStart.trim().equals(""))
				workingShift.setBreakStart(DateFormat
						.convertStringToTime(breakStart));
			else
				workingShift.setBreakStart(null);
			
			if (breakEnd != null && !breakEnd.trim().equals(""))
				workingShift.setBreakEnd(DateFormat
						.convertStringToTime(breakEnd));
			else
				workingShift.setBreakEnd(null);

			if (irregularDay != null && !irregularDay.equals(""))
				workingShift.setIrregularDay(irregularDay);
			
			if(cmpDeptLocId!=null && cmpDeptLocId>0){
				CmpDeptLoc cmpDeptLocation=new CmpDeptLoc();
				cmpDeptLocation.setCmpDeptLocId(cmpDeptLocId);
				workingShift.setCmpDeptLocation(cmpDeptLocation);
			}else{
				workingShift.setCmpDeptLocation(null);
			}

			workingShift.setIsActive(isActive);
			workingShift.setIsDefault(isDefault);
			workingShift.setImplementation(getImplementation());
			workingShiftBL.saveWorkingShift(workingShift);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");

			LOGGER.info("saveWorkingShift() Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			if (workingShiftId > 0)
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveWorkingShift() Unsuccessful Return");
			return ERROR;
		}
	}

	public String deleteWorkingShift() {
		try {
			if (workingShiftId != null && workingShiftId > 0) {
				workingShiftBL.deleteWorkingShift(workingShiftId);
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "No record selected to delete");
			}
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("deleteWorkingShift() Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deleteWorkingShift() unsuccessful Return");
			return ERROR;
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Long getWorkingShiftId() {
		return workingShiftId;
	}

	public void setWorkingShiftId(Long workingShiftId) {
		this.workingShiftId = workingShiftId;
	}

	public WorkingShiftBL getWorkingShiftBL() {
		return workingShiftBL;
	}

	public void setWorkingShiftBL(WorkingShiftBL workingShiftBL) {
		this.workingShiftBL = workingShiftBL;
	}

	public String getWorkingShiftName() {
		return workingShiftName;
	}

	public void setWorkingShiftName(String workingShiftName) {
		this.workingShiftName = workingShiftName;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getLateAttendance() {
		return lateAttendance;
	}

	public void setLateAttendance(String lateAttendance) {
		this.lateAttendance = lateAttendance;
	}

	public String getSeviorLateAttendance() {
		return seviorLateAttendance;
	}

	public void setSeviorLateAttendance(String seviorLateAttendance) {
		this.seviorLateAttendance = seviorLateAttendance;
	}

	public String getBufferTime() {
		return bufferTime;
	}

	public void setBufferTime(String bufferTime) {
		this.bufferTime = bufferTime;
	}

	public String getBreakHours() {
		return breakHours;
	}

	public void setBreakHours(String breakHours) {
		this.breakHours = breakHours;
	}

	public String getIrregularDay() {
		return irregularDay;
	}

	public void setIrregularDay(String irregularDay) {
		this.irregularDay = irregularDay;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public String getBreakStart() {
		return breakStart;
	}

	public void setBreakStart(String breakStart) {
		this.breakStart = breakStart;
	}

	public String getBreakEnd() {
		return breakEnd;
	}

	public void setBreakEnd(String breakEnd) {
		this.breakEnd = breakEnd;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getCmpDeptLocId() {
		return cmpDeptLocId;
	}

	public void setCmpDeptLocId(Long cmpDeptLocId) {
		this.cmpDeptLocId = cmpDeptLocId;
	}
}
