package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.action.GLCombinationAction;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.CreditTerm;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.ShippingDetail;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.service.GLChartOfAccountService;
import com.aiotech.aios.accounts.service.GLCombinationService;
import com.aiotech.aios.accounts.service.bl.CustomerBL;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.CompanyTypeAllocation;
import com.aiotech.aios.hr.domain.entity.Department;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.domain.entity.Location;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.CompanyVO;
import com.aiotech.aios.hr.domain.entity.vo.PersonVO;
import com.aiotech.aios.hr.service.DepartmentService;
import com.aiotech.aios.hr.service.LocationService;
import com.aiotech.aios.hr.service.bl.CompanyBL;
import com.aiotech.aios.hr.service.bl.SupplierBL;
import com.aiotech.aios.hr.to.CompanyTO;
import com.aiotech.aios.hr.to.converter.CompanyTOConvert;
import com.aiotech.aios.system.bl.CommonBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class CompanyAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(CompanyAction.class);

	private long companyId;
	private String companyName;
	private String companyNameArabic;
	private String addEditFlag;

	private int companyTypeId;
	private String companyTypeName;
	private String description;
	private long tradeId;
	private String tradeName;
	private String id;
	private String companyAddress;
	private String companyPobox;
	private String companyFax;
	private String companyPhone;
	private String companyOrigin;
	private String tradeNoIssueDate;
	private Boolean isCustomer;
	private Boolean isSupplier;
	private Byte supplierAcType;
	private Byte acClassification;
	private long paymentTermId;
	private Double creditLimit;
	private Double openingBalance;
	private String bankName;
	private String branchName;
	private String ibanNumber;
	private String accountNumber;
	private Integer supplierNumber;
	private Long currencyId;
	private String routingCode;
	private String mobile;
	private String address;
	private Long accountId;
	private String labourCardId;
	private Long supplierId;

	private Long customerId;
	private String customerNumber;
	private Integer customerAcType;
	private Long creditTermId;
	private Long saleRepresentativeId;
	private String billingAddress;
	private Long refferalSource;
	private Long shippingMethod;
	private String customerDescription;
	private String shippingDetails;

	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private long departmentId;
	private long locationId;
	private String locations;
	private long identityId;
	private int identityType;
	private String identityNumber;
	private String issuedPlace;
	private String expireDate;
	private String identityCode;
	private Long tenantCombinationId;
	private Long supplierCombinationId;
	private Long customerCombinationId;
	private Long ownerCombinationId;
	private String companyTypes;
	private String firstName;
	private byte memberCarType;
	private long customerRecordId;
	private int cardNumber;

	// Accounts Integration
	private long combinationId;
	private GLChartOfAccountService accountService;
	private GLCombinationService combinationService;
	private GLCombinationAction combinationAction;
	private SupplierBL supplierBL;
	private CustomerBL customerBL;
	Implementation implementation = null;

	List<CompanyTO> companyTOs = null;
	CompanyTO companyTO = null;
	List<Company> companys = null;
	Company company = null;
	private Identity identity = null;
	private List<Identity> identitys = null;
	private Object personObject;
	private DepartmentService departmentService;
	private LocationService locationService;

	CompanyBL companyBL;
	DocumentBL documentBL;
	CommonBL commonBL;
	private Long recordId;

	public void getImplementId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		implementation = new Implementation();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");

		}
	}

	// JSON Listing Redirect
	public String returnSuccess() {
		ServletActionContext.getRequest().setAttribute("COMPANY_TYPES",
				companyTypes);
		ServletActionContext.getRequest().setAttribute("rowid", id);
		ServletActionContext.getRequest().setAttribute("AddEditFlag",
				addEditFlag);
		return SUCCESS;
	}

	// Listing JSON
	public String execute() {

		try {
			companyTOs = new ArrayList<CompanyTO>();
			// Set the value to push to Personal Details DAO
			companys = new ArrayList<Company>();
			// Call Implementation
			getImplementId();
			companyTOs = companyBL.companyList(implementation);
			if (companyTOs != null && companyTOs.size() > 0) {

				iTotalRecords = companyTOs.size();
				iTotalDisplayRecords = companyTOs.size();
			}

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (CompanyTO list : companyTOs) {

				JSONArray array = new JSONArray();
				array.add(list.getCompanyId());
				array.add(list.getCompanyName());
				array.add(list.getCompanyTypes());
				array.add(list.getDescription());
				array.add(list.getTradeName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Listing JSON
	public String getCompanyList() {

		try {
			companyTOs = new ArrayList<CompanyTO>();
			// Set the value to push to Personal Details DAO
			companys = new ArrayList<Company>();
			// Call Implementation
			getImplementId();
			companyTOs = companyBL.companyList(implementation);
			if (companyTOs != null && companyTOs.size() > 0) {

				iTotalRecords = companyTOs.size();
				iTotalDisplayRecords = companyTOs.size();
			}

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (CompanyTO list : companyTOs) {

				JSONArray array = new JSONArray();
				array.add(list.getCompanyId());
				array.add(list.getCompanyName());
				array.add(list.getCompanyTypes());
				array.add(list.getDescription());
				array.add(list.getTradeName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// JSON Listing Redirect
	public String getLocationList() {
		ServletActionContext.getRequest().setAttribute("rowid", id);
		ServletActionContext.getRequest().setAttribute("EXIST_LOCATIONS",
				locations);
		return SUCCESS;
	}

	/*
	 * // Listing JSON public String getLocationListJson() {
	 * 
	 * try { companyTOs = new ArrayList<CompanyTO>(); // Set the value to push
	 * to Personal Details DAO companys = new ArrayList<Company>();
	 * 
	 * 
	 * //Call Implementation getImplementId(); List<Location>
	 * locationList=this.getLocationService().getAllLocation(implementation); if
	 * (locationList != null && locationList.size() > 0) {
	 * 
	 * iTotalRecords=locationList.size();
	 * iTotalDisplayRecords=locationList.size(); }
	 * 
	 * JSONObject jsonResponse = new JSONObject();
	 * jsonResponse.put("iTotalRecords", iTotalRecords);
	 * jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords); JSONArray
	 * data= new JSONArray(); for(Location location :locationList){ JSONArray
	 * array= new JSONArray(); array.add(location.getLocationId()+"");
	 * array.add(location.getLocationName()+"");
	 * array.add(location.getAddress()+"");
	 * array.add(location.getCityOrTown()+"");
	 * array.add(location.getCountry()+"");
	 * array.add(location.getDescription()+""); data.add(array); }
	 * jsonResponse.put("aaData", data);
	 * ServletActionContext.getRequest().setAttribute("jsonlist", jsonResponse);
	 * return SUCCESS;
	 * 
	 * } catch (Exception e) { e.printStackTrace(); return ERROR; } }
	 */
	public String addCompany() {

		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			AIOSCommons.removeUploaderSession(session, "companyDocs", -1L,
					"Company");
			session.removeAttribute("AIOS-doc-" + "Company-" + session.getId());

			session.removeAttribute("COMPANY_IDENTITY_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("COMPANY_IDENTITY_TYPE_"
					+ sessionObj.get("jSessionId"));
			companyTOs = new ArrayList<CompanyTO>();
			company = new Company();
			companyTO = new CompanyTO();
			Combination tenantCombination = null;
			Combination customerCombination = null;
			Combination supplierCombination = null;
			Combination ownerCombination = null;
			identitys = new ArrayList<Identity>();
			List<CompanyVO> identityList = null;
			// List<CmpDeptLocation> companySetups=null;
			if (recordId != null && recordId > 0) {
				companyId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (companyId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(AttendancePolicy.class.getName());
					comment = companyBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			if (companyId != 0) {
				company = companyBL.getCompanyService().getCompanyInfo(
						companyId);
				AIOSCommons.removeUploaderSession(session, "companyDocs",
						companyId, "Company");
				identitys = new ArrayList<Identity>(company.getIdentities());
				identityList = companyBL.convertIdentityToVOList(identitys);
				companyTO = CompanyTOConvert.convertToTO(company);

			}

			{
				tenantCombination = companyBL.findCombinationInfo(company
						.getCompanyName() + "-TENANT");

				if (null != company.getCustomers()
						&& company.getCustomers().size() > 0) {
					List<Customer> customers = new ArrayList<Customer>(
							company.getCustomers());
					customerCombination = companyBL.getCombinationService()
							.getCombinationAllAccountDetail(
									customers.get(0).getCombination()
											.getCombinationId());
				} else if (null != company.getSuppliers()
						&& company.getSuppliers().size() > 0) {
					List<Supplier> suppliers = new ArrayList<Supplier>(
							company.getSuppliers());
					supplierCombination = companyBL.getCombinationService()
							.getCombinationAllAccountDetail(
									suppliers.get(0).getCombination()
											.getCombinationId());
				}

				ownerCombination = companyBL.findCombinationInfo(company
						.getCompanyName() + "-OWNER");
			}

			ServletActionContext.getRequest().setAttribute("IDENTITY_LIST",
					identityList);

			session.setAttribute(
					"COMPANY_IDENTITY_" + sessionObj.get("jSessionId"),
					identitys);

			// companyTOs = companyBL.getTypeList();
			List<LookupDetail> companyTypeList = companyBL.getLookupMasterBL()
					.getActiveLookupDetails("COMPANY_TYPE", true);
			List<LookupDetail> companyIdentityTypes = companyBL
					.getLookupMasterBL().getActiveLookupDetails(
							"COMPANY_IDENTITY_LIST", false);
			session.setAttribute(
					"COMPANY_IDENTITY_TYPE_" + sessionObj.get("jSessionId"),
					companyIdentityTypes);

			getImplementId();
			List<Department> departmentList = this.getDepartmentService()
					.getAllDepartments(implementation);
			List<Location> locationList = this.getLocationService()
					.getAllLocation(implementation);
			ServletActionContext.getRequest()
					.setAttribute("COMPANY", companyTO);
			ServletActionContext.getRequest().setAttribute("COMPANY_TYPE",
					companyTypeList);
			ServletActionContext.getRequest().setAttribute("DEPARTMENT_LIST",
					departmentList);
			ServletActionContext.getRequest().setAttribute("LOCATION_LIST",
					locationList);
			ServletActionContext.getRequest().setAttribute("TENANT_ACCOUNT",
					tenantCombination);
			ServletActionContext.getRequest().setAttribute("SUPPLIER_ACCOUNT",
					supplierCombination);
			ServletActionContext.getRequest().setAttribute("CUSTOMER_ACCOUNT",
					customerCombination);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Save Person Customer Live & Local
	public String updateSpecialCustomer() {
		try {
			Company company = companyBL.getCompanyService()
					.getCompanyByCustomerId(customerId);
			PersonVO personVO = new PersonVO();
			company.setCompanyPhone(mobile);
			company.setCompanyAddress(address);
			company.setCompanyName(firstName);
			companyBL.udpateCompany(company);
			personVO.setCustomerName(firstName);
			personVO.setCustomerId(customerId);
			personObject = personVO;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String saveCompany() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		long documentRecId = -1;
		try {
			company = new Company();
			companyTO = new CompanyTO();
			CompanyVO companyVO = new CompanyVO();
			Company companyToEdit = null;
			List<Identity> tempidentitys = null;
			if (companyId != 0) {
				company.setCompanyId(companyId);
				companyToEdit = companyBL.getCompanyService().getCompanyInfo(
						companyId);
				tempidentitys = new ArrayList<Identity>(
						companyToEdit.getIdentities());
			}
			getImplementId();
			company.setImplementation(implementation);
			company.setCompanyName(companyName);
			company.setCompanyNameArabic(AIOSCommons
					.stringToUTFBytes(companyNameArabic));
			company.setDescription(description);
			// company.setCompanyType(companyTypeId);
			company.setTradeNo(tradeName);
			company.setCompanyAddress(companyAddress);
			company.setCompanyPobox(companyPobox);
			company.setCompanyFax(companyFax);
			company.setCompanyPhone(companyPhone);
			company.setCompanyOrigin(companyOrigin);
			company.setTradeNoIssueDate(DateFormat
					.convertStringToDate(tradeNoIssueDate));

			company.setLabourCardId(labourCardId);

			LOGGER.info("Creating Supplier " + isSupplier);
			if (isSupplier) {
				CmpDeptLoc cmpDeptLoc = new CmpDeptLoc();
				Location location = new Location();
				Department department = new Department();
				location.setLocationId(locationId);
				department.setDepartmentId(departmentId);
				cmpDeptLoc.setLocation(location);
				cmpDeptLoc.setDepartment(department);
				cmpDeptLoc.setCompany(company);
				cmpDeptLoc.setIsActive(true);
				// companyBL.savecmpDeptLoc(cmpDeptLoc);
				Supplier supplier = new Supplier();
				supplier.setSupplierType(supplierAcType);
				supplier.setClassification(acClassification);
				CreditTerm paymentTerm = null;
				if (paymentTermId > 0) {
					paymentTerm = new CreditTerm();
					paymentTerm.setCreditTermId(paymentTermId);
				}
				supplier.setCreditLimit(creditLimit);
				supplier.setOpeningBalance(openingBalance);
				supplier.setCreditTerm(paymentTerm);
				supplier.setBankName(bankName);
				supplier.setBranchName(branchName);
				supplier.setIbanNumber(ibanNumber);
				supplier.setAccountNumber(accountNumber);
				supplier.setImplementation(implementation);
				if (null != supplierId && supplierId > 0) {
					supplier.setSupplierId(supplierId);
				} else {
					List<Supplier> supplierList = this.getSupplierBL()
							.getAllSuppliers(implementation);
					if (null != supplierList && supplierList.size() > 0) {
						List<Integer> supplierNo = new ArrayList<Integer>();
						for (Supplier list : supplierList) {
							supplierNo.add(Integer.parseInt(list
									.getSupplierNumber()));
						}
						supplierNumber = Collections.max(supplierNo);
						supplierNumber += 1;
					} else {
						supplierNumber = 14895;
					}
				}
				supplier.setSupplierNumber(String.valueOf(supplierNumber));
				companyVO.setSupplier(supplier);
				companyVO.setCurrencyId(currencyId);
				// supplierBL.saveSupplier(supplier, combinationId, currencyId);
			} else if (isCustomer) {
				CmpDeptLoc cmpDeptLoc = new CmpDeptLoc();
				Location location = new Location();
				Department department = new Department();
				location.setLocationId(locationId);
				department.setDepartmentId(departmentId);
				cmpDeptLoc.setLocation(location);
				cmpDeptLoc.setDepartment(department);
				cmpDeptLoc.setCompany(company);
				cmpDeptLoc.setIsActive(true);
				// companyBL.savecmpDeptLoc(cmpDeptLoc);
				Customer customer = new Customer();
				customer.setCustomerType(null != customerAcType
						&& customerAcType > 0 ? customerAcType : null);
				LookupDetail lookupDetail = null;
				if (null != refferalSource && refferalSource > 0) {
					lookupDetail = new LookupDetail();
					lookupDetail.setLookupDetailId(refferalSource);
					customer.setLookupDetailByRefferalSource(lookupDetail);
				}
				if (null != shippingMethod && shippingMethod > 0) {
					lookupDetail = new LookupDetail();
					lookupDetail.setLookupDetailId(shippingMethod);
					customer.setLookupDetailByShippingMethod(lookupDetail);
				}
				customer.setBillingAddress(billingAddress);
				customer.setCreditLimit(null != creditLimit && creditLimit > 0 ? creditLimit
						: null);
				customer.setOpeningBalance(null != openingBalance
						&& openingBalance > 0 ? openingBalance : null);
				customer.setDescription(customerDescription);
				customer.setImplementation(implementation);
				customer.setMemberType(memberCarType > 0 ? memberCarType : null);
				customer.setMemberCardNumber(customerId > 0 && cardNumber > 0 ? cardNumber
						: null);
				if (null != saleRepresentativeId && saleRepresentativeId > 0) {
					Person person = new Person();
					person.setPersonId(saleRepresentativeId);
					customer.setPersonBySaleRepresentative(person);
				}
				if (null != creditTermId && creditTermId > 0) {
					CreditTerm creditTerm = new CreditTerm();
					creditTerm.setCreditTermId(creditTermId);
					customer.setCreditTerm(creditTerm);
				}
				String[] lineDetailArray = splitValues(shippingDetails, "#@");
				ShippingDetail shippingDetails = null;
				List<ShippingDetail> shippingDetailList = new ArrayList<ShippingDetail>();
				for (String detail : lineDetailArray) {
					String[] detailString = splitValues(detail, "__");
					if (detailString.length <= 1)
						continue;
					shippingDetails = new ShippingDetail();
					shippingDetails.setName(detailString[0]);
					shippingDetails.setContactPerson(detailString[1]);
					if (null != detailString[2]
							&& !("").equals(detailString[2])
							&& !("##").equals(detailString[2]))
						shippingDetails.setContactNo(detailString[2]);
					if (null != detailString[3]
							&& !("").equals(detailString[3])
							&& !("##").equals(detailString[3]))
						shippingDetails.setAddress(detailString[3]);
					if (Long.parseLong(detailString[4].trim()) > 0)
						shippingDetails.setShippingId(Long
								.parseLong(detailString[4].trim()));
					shippingDetailList.add(shippingDetails);
				}
				if (null != customerId && customerId > 0) {
					customer.setCustomerId(customerId);
					customer.setCustomerNumber(customerNumber);
				} else {
					List<Customer> customerList = this.getCustomerBL()
							.getCustomerService()
							.getAllCustomers(implementation);
					if (null != customerList && customerList.size() > 0) {
						List<Long> customerNoList = new ArrayList<Long>();
						for (Customer list : customerList) {
							customerNoList.add(Long.parseLong(list
									.getCustomerNumber()));
						}
						long customerNo = Collections.max(customerNoList);
						customer.setCustomerNumber(String.valueOf(++customerNo));

					} else {
						customer.setCustomerNumber("1000");
					}
				}
				companyVO.setCustomer(customer);
				companyVO.setShippingDetailList(shippingDetailList);
				// this.getCustomerBL().saveCustomer(customer,
				// shippingDetailList,companyName);
			}

			@SuppressWarnings("unchecked")
			List<Identity> identitys = (ArrayList<Identity>) session
					.getAttribute("COMPANY_IDENTITY_"
							+ sessionObj.get("jSessionId"));
			if (companyId != 0) {
				List<Identity> identityDeleteList = companyBL
						.getIdentityToDelete(identitys, tempidentitys);
				if (identityDeleteList != null && identityDeleteList.size() > 0)
					companyBL.getCompanyService().deleteIdentityAll(
							identityDeleteList);
			}

			// Multiple Person Types
			companyVO.setCompanyTypes(companyTypes);

			// Account integration
			companyVO.setCompanyId(company.getCompanyId());
			companyVO.setCompanyName(company.getCompanyName());

			if (tenantCombinationId != null && tenantCombinationId > 0)
				companyVO.setTenantCombinationId(tenantCombinationId);

			if (supplierCombinationId != null && supplierCombinationId > 0)
				companyVO.setSupplierCombinationId(supplierCombinationId);

			if (customerCombinationId != null && customerCombinationId > 0)
				companyVO.setCustomerCombinationId(customerCombinationId);

			companyBL.saveCompany(identitys, company, companyVO);

			if (companyId > 0) {
				documentRecId = companyId;
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "SUCCESS");
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "SUCCESS");
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			if (companyId > 0) {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			}
			return ERROR;
		} finally {
			AIOSCommons.removeUploaderSession(session, "companyDocs",
					documentRecId, "Company");
		}
	}

	public String deleteCompany() {

		try {
			company = new Company();
			companyTO = new CompanyTO();
			companys = new ArrayList<Company>();
			String message = null;
			message = commonBL.deleteCompanyDependancyCheck(companyId);
			if (companyId != 0 && message == null) {

				companyBL.deleteCompany(companyId);
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "SUCCESS");

			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", message);
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure / Information is in use already.");
			return ERROR;
		}
	}

	public String identityAdd() {
		try {

			List<LookupDetail> companyIdentityTypeList = companyBL
					.getLookupMasterBL().getActiveLookupDetails(
							"COMPANY_IDENTITY_LIST", false);
			ServletActionContext.getRequest().setAttribute("rowid", id);
			ServletActionContext.getRequest().setAttribute("AddEditFlag",
					addEditFlag);
			ServletActionContext.getRequest().setAttribute(
					"IDENTITY_TYPE_LIST", companyIdentityTypeList);
			return SUCCESS;

		} catch (Exception e) {
			return ERROR;
		}
	}

	public String saveCompanyIdentity() {

		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			identity = new Identity();
			List<Identity> identitys = null;
			Company company = new Company();
			if (companyId != 0) {
				company.setCompanyId(companyId);
				identity.setCompany(company);

			}
			if (identityId > 0)
				identity.setIdentityId(identityId);
			identity.setIdentityType(identityType);
			List<LookupDetail> lookupDetails = (List<LookupDetail>) session
					.getAttribute("COMPANY_IDENTITY_TYPE_"
							+ sessionObj.get("jSessionId"));
			for (LookupDetail lookupDetail : lookupDetails) {
				if (lookupDetail.getDataId().equals(identityType))
					identity.setLookupDetail(lookupDetail);
			}
			identity.setIdentityNumber(identityNumber);
			identity.setIssuedPlace(issuedPlace);
			identity.setIdentityCode(identityCode);
			identity.setIsActive(true);
			if (expireDate != null && !expireDate.equals(""))
				identity.setExpireDate(DateFormat
						.convertStringToDate(expireDate));
			identity.setDescription(description);

			if (session.getAttribute("COMPANY_IDENTITY_"
					+ sessionObj.get("jSessionId")) == null) {
				identitys = new ArrayList<Identity>();
			} else {
				identitys = (List<Identity>) session
						.getAttribute("COMPANY_IDENTITY_"
								+ sessionObj.get("jSessionId"));
			}
			if (addEditFlag != null && addEditFlag.equals("A")) {
				LOGGER.info("Add Process");
				identitys.add(identity);
			} else if (addEditFlag.equals("E")) {
				LOGGER.info("Edit Process");
				identitys.set(Integer.parseInt(id) - 1, identity);
			} else if (addEditFlag.equals("D")) {
				LOGGER.info("Delete Process");
				identitys.remove(Integer.parseInt(id) - 1);
			}
			session.setAttribute(
					"COMPANY_IDENTITY_" + sessionObj.get("jSessionId"),
					identitys);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg", "Error");
			return ERROR;
		}
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getCompanyTypeId() {
		return companyTypeId;
	}

	public void setCompanyTypeId(int companyTypeId) {
		this.companyTypeId = companyTypeId;
	}

	public String getCompanyTypeName() {
		return companyTypeName;
	}

	public void setCompanyTypeName(String companyTypeName) {
		this.companyTypeName = companyTypeName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getTradeId() {
		return tradeId;
	}

	public void setTradeId(long tradeId) {
		this.tradeId = tradeId;
	}

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public CompanyBL getCompanyBL() {
		return companyBL;
	}

	public void setCompanyBL(CompanyBL companyBL) {
		this.companyBL = companyBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}

	public GLChartOfAccountService getAccountService() {
		return accountService;
	}

	public void setAccountService(GLChartOfAccountService accountService) {
		this.accountService = accountService;
	}

	public GLCombinationService getCombinationService() {
		return combinationService;
	}

	public void setCombinationService(GLCombinationService combinationService) {
		this.combinationService = combinationService;
	}

	public GLCombinationAction getCombinationAction() {
		return combinationAction;
	}

	public void setCombinationAction(GLCombinationAction combinationAction) {
		this.combinationAction = combinationAction;
	}

	public String getCompanyNameArabic() {
		return companyNameArabic;
	}

	public void setCompanyNameArabic(String companyNameArabic) {
		this.companyNameArabic = companyNameArabic;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public String getCompanyPobox() {
		return companyPobox;
	}

	public String getCompanyFax() {
		return companyFax;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public void setCompanyPobox(String companyPobox) {
		this.companyPobox = companyPobox;
	}

	public void setCompanyFax(String companyFax) {
		this.companyFax = companyFax;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public String getTradeNoIssueDate() {
		return tradeNoIssueDate;
	}

	public void setTradeNoIssueDate(String tradeNoIssueDate) {
		this.tradeNoIssueDate = tradeNoIssueDate;
	}

	public String getCompanyOrigin() {
		return companyOrigin;
	}

	public void setCompanyOrigin(String companyOrigin) {
		this.companyOrigin = companyOrigin;
	}

	public Boolean getIsSupplier() {
		return isSupplier;
	}

	public void setIsSupplier(Boolean isSupplier) {
		this.isSupplier = isSupplier;
	}

	public CommonBL getCommonBL() {
		return commonBL;
	}

	public void setCommonBL(CommonBL commonBL) {
		this.commonBL = commonBL;
	}

	public SupplierBL getSupplierBL() {
		return supplierBL;
	}

	public void setSupplierBL(SupplierBL supplierBL) {
		this.supplierBL = supplierBL;
	}

	public long getPaymentTermId() {
		return paymentTermId;
	}

	public void setPaymentTermId(long paymentTermId) {
		this.paymentTermId = paymentTermId;
	}

	public Double getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(Double creditLimit) {
		this.creditLimit = creditLimit;
	}

	public Double getOpeningBalance() {
		return openingBalance;
	}

	public void setOpeningBalance(Double openingBalance) {
		this.openingBalance = openingBalance;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getIbanNumber() {
		return ibanNumber;
	}

	public void setIbanNumber(String ibanNumber) {
		this.ibanNumber = ibanNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public Integer getSupplierNumber() {
		return supplierNumber;
	}

	public void setSupplierNumber(Integer supplierNumber) {
		this.supplierNumber = supplierNumber;
	}

	public Byte getSupplierAcType() {
		return supplierAcType;
	}

	public void setSupplierAcType(Byte supplierAcType) {
		this.supplierAcType = supplierAcType;
	}

	public Byte getAcClassification() {
		return acClassification;
	}

	public void setAcClassification(Byte acClassification) {
		this.acClassification = acClassification;
	}

	public DepartmentService getDepartmentService() {
		return departmentService;
	}

	public void setDepartmentService(DepartmentService departmentService) {
		this.departmentService = departmentService;
	}

	public LocationService getLocationService() {
		return locationService;
	}

	public void setLocationService(LocationService locationService) {
		this.locationService = locationService;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public long getLocationId() {
		return locationId;
	}

	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public String getLocations() {
		return locations;
	}

	public void setLocations(String locations) {
		this.locations = locations;
	}

	public String getRoutingCode() {
		return routingCode;
	}

	public void setRoutingCode(String routingCode) {
		this.routingCode = routingCode;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getLabourCardId() {
		return labourCardId;
	}

	public void setLabourCardId(String labourCardId) {
		this.labourCardId = labourCardId;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public Boolean getIsCustomer() {
		return isCustomer;
	}

	public void setIsCustomer(Boolean isCustomer) {
		this.isCustomer = isCustomer;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public Integer getCustomerAcType() {
		return customerAcType;
	}

	public void setCustomerAcType(Integer customerAcType) {
		this.customerAcType = customerAcType;
	}

	public Long getCreditTermId() {
		return creditTermId;
	}

	public void setCreditTermId(Long creditTermId) {
		this.creditTermId = creditTermId;
	}

	public Long getSaleRepresentativeId() {
		return saleRepresentativeId;
	}

	public void setSaleRepresentativeId(Long saleRepresentativeId) {
		this.saleRepresentativeId = saleRepresentativeId;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public Long getRefferalSource() {
		return refferalSource;
	}

	public void setRefferalSource(Long refferalSource) {
		this.refferalSource = refferalSource;
	}

	public Long getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(Long shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public String getCustomerDescription() {
		return customerDescription;
	}

	public void setCustomerDescription(String customerDescription) {
		this.customerDescription = customerDescription;
	}

	public String getShippingDetails() {
		return shippingDetails;
	}

	public void setShippingDetails(String shippingDetails) {
		this.shippingDetails = shippingDetails;
	}

	public CustomerBL getCustomerBL() {
		return customerBL;
	}

	public void setCustomerBL(CustomerBL customerBL) {
		this.customerBL = customerBL;
	}

	public String getAddEditFlag() {
		return addEditFlag;
	}

	public void setAddEditFlag(String addEditFlag) {
		this.addEditFlag = addEditFlag;
	}

	public String getIdentityCode() {
		return identityCode;
	}

	public void setIdentityCode(String identityCode) {
		this.identityCode = identityCode;
	}

	public long getIdentityId() {
		return identityId;
	}

	public void setIdentityId(long identityId) {
		this.identityId = identityId;
	}

	public int getIdentityType() {
		return identityType;
	}

	public void setIdentityType(int identityType) {
		this.identityType = identityType;
	}

	public String getIdentityNumber() {
		return identityNumber;
	}

	public void setIdentityNumber(String identityNumber) {
		this.identityNumber = identityNumber;
	}

	public String getIssuedPlace() {
		return issuedPlace;
	}

	public void setIssuedPlace(String issuedPlace) {
		this.issuedPlace = issuedPlace;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public Identity getIdentity() {
		return identity;
	}

	public void setIdentity(Identity identity) {
		this.identity = identity;
	}

	public List<Identity> getIdentitys() {
		return identitys;
	}

	public void setIdentitys(List<Identity> identitys) {
		this.identitys = identitys;
	}

	public Long getTenantCombinationId() {
		return tenantCombinationId;
	}

	public void setTenantCombinationId(Long tenantCombinationId) {
		this.tenantCombinationId = tenantCombinationId;
	}

	public Long getSupplierCombinationId() {
		return supplierCombinationId;
	}

	public void setSupplierCombinationId(Long supplierCombinationId) {
		this.supplierCombinationId = supplierCombinationId;
	}

	public Long getCustomerCombinationId() {
		return customerCombinationId;
	}

	public void setCustomerCombinationId(Long customerCombinationId) {
		this.customerCombinationId = customerCombinationId;
	}

	public Long getOwnerCombinationId() {
		return ownerCombinationId;
	}

	public void setOwnerCombinationId(Long ownerCombinationId) {
		this.ownerCombinationId = ownerCombinationId;
	}

	public String getCompanyTypes() {
		return companyTypes;
	}

	public void setCompanyTypes(String companyTypes) {
		this.companyTypes = companyTypes;
	}

	public byte getMemberCarType() {
		return memberCarType;
	}

	public void setMemberCarType(byte memberCarType) {
		this.memberCarType = memberCarType;
	}

	public int getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(int cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getCustomerRecordId() {
		return customerRecordId;
	}

	public void setCustomerRecordId(long customerRecordId) {
		this.customerRecordId = customerRecordId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Object getPersonObject() {
		return personObject;
	}

	public void setPersonObject(Object personObject) {
		this.personObject = personObject;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

}
