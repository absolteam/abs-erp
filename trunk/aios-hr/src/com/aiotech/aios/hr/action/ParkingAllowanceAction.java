package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobPayrollElement;
import com.aiotech.aios.hr.domain.entity.ParkingAllowance;
import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.JobPayrollElementVO;
import com.aiotech.aios.hr.domain.entity.vo.ParkingAllowanceVO;
import com.aiotech.aios.hr.domain.entity.vo.PayPeriodVO;
import com.aiotech.aios.hr.service.bl.ParkingAllowanceBL;
import com.aiotech.aios.hr.service.bl.PayrollBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ParkingAllowanceAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(ParkingAllowanceAction.class);
	private Implementation implementation;
	private ParkingAllowanceBL parkingAllowanceBL;
	private PayrollBL payrollBL;
	private String transactionDate;
	private Integer id;
	private Long jobAssignmentId;
	private Long jobPayrollElementId;
	private List<Object> aaData;
	private Byte payPolicy;
	private Double amount;
	private String fromDate;
	private String toDate;
	private String receiptNumber;
	private String isFinanceImpact;
	private Long payPeriodTransactionId;
	private String description;

	private Long parkingAllowanceId;
	private Long cardCompany;
	private Long cardType;
	private Long parkingType;
	private String cardNumber;
	private String purchaseDate;

	public String returnSuccess() {
		ServletActionContext.getRequest().setAttribute("jobAssignmentId",
				jobAssignmentId);
		ServletActionContext.getRequest().setAttribute("jobPayrollElementId",
				jobPayrollElementId);
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String getParkingList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();

			// Find the pay period of current transaction
			PayPeriodVO payPeriodVO = new PayPeriodVO();
			payPeriodVO.setJobAssignmentId(jobAssignmentId);
			payPeriodVO.setTransactionDate(DateFormat
					.convertStringToDate(transactionDate));
			payPeriodVO.setPayPolicy(payPolicy);
			payPeriodVO.setPayPeriodTransactionId(payPeriodTransactionId);

			PayPeriodTransaction payPeriodTransaction = parkingAllowanceBL
					.getPayPeriodBL().findPayPeriod(payPeriodVO);

			ParkingAllowanceVO parkingAllowanceVO = new ParkingAllowanceVO();
			if (payPeriodTransaction != null)
				parkingAllowanceVO
						.setPayPeriodTransaction(payPeriodTransaction);

			parkingAllowanceVO.setJobAssignmentId(jobAssignmentId);
			parkingAllowanceVO.setJobPayrollElementId(jobPayrollElementId);

			List<ParkingAllowance> parkingAllowances = parkingAllowanceBL
					.getParkingAllowanceService()
					.getAllParkingAllowanceByCriteria(parkingAllowanceVO);

			if (parkingAllowances != null && parkingAllowances.size() > 0) {
				ParkingAllowanceVO vo = null;
				for (ParkingAllowance list : parkingAllowances) {
					vo = new ParkingAllowanceVO();
					vo.setParkingAllowanceId(list.getParkingAllowanceId());
					if (list.getLookupDetailByCardCompany() != null)
						vo.setCardCompany(list.getLookupDetailByCardCompany()
								.getDisplayName());
					if (list.getLookupDetailByCardType() != null)
						vo.setCardType(list.getLookupDetailByCardType()
								.getDisplayName());

					if (list.getLookupDetailByParkingType() != null)
						vo.setParkingType(list.getLookupDetailByParkingType()
								.getDisplayName());

					vo.setCardNumber(list.getCardNumber());

					vo.setFromDateView(DateFormat.convertDateToString(list
							.getFromDate().toString()));
					vo.setToDateView(DateFormat.convertDateToString(list
							.getToDate().toString()));
					vo.setAmount(list.getAmount());
					if (list.getIsFinanceImpact() != null
							&& list.getIsFinanceImpact() == true)
						vo.setIsFinanceView("YES");
					else
						vo.setIsFinanceView("NO");

					aaData.add(vo);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getParkingAllowance() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			ParkingAllowanceVO parkingAllowanceVO = null;
			JobPayrollElementVO jobPayrollElementVO = null;
			if (parkingAllowanceId != null && parkingAllowanceId > 0) {
				ParkingAllowance parkingAllowance = parkingAllowanceBL
						.getParkingAllowanceService().getParkingAllowanceById(
								parkingAllowanceId);
				if (parkingAllowance != null)
					parkingAllowanceVO = parkingAllowanceBL
							.convertEntityToVO(parkingAllowance);
			}

			// Find the Maximum provided & actual earned amount
			{
				// Find the pay period of current transaction
				PayPeriodVO payPeriodVO = new PayPeriodVO();
				payPeriodVO.setJobAssignmentId(jobAssignmentId);
				payPeriodVO.setTransactionDate(DateFormat
						.convertStringToDate(transactionDate));
				payPeriodVO.setPayPolicy(payPolicy);
				payPeriodVO.setPayPeriodTransactionId(payPeriodTransactionId);

				PayPeriodTransaction payPeriodTransaction = parkingAllowanceBL
						.getPayPeriodBL().findPayPeriod(payPeriodVO);
				ParkingAllowanceVO parkingAllowanceTempVO = new ParkingAllowanceVO();
				if (payPeriodTransaction != null)
					parkingAllowanceTempVO
							.setPayPeriodTransaction(payPeriodTransaction);

				parkingAllowanceTempVO.setJobAssignmentId(jobAssignmentId);
				parkingAllowanceTempVO
						.setJobPayrollElementId(jobPayrollElementId);

				List<ParkingAllowance> parkingAllowances = parkingAllowanceBL
						.getParkingAllowanceService()
						.getAllParkingAllowanceByCriteria(
								parkingAllowanceTempVO);

				jobPayrollElementVO = payrollBL.getJobPayrollInfomration(
						jobPayrollElementId, new ArrayList<Object>(
								parkingAllowances), payPeriodTransaction);

				ServletActionContext.getRequest().setAttribute("JOBPAY_INFO",
						jobPayrollElementVO);
			}

			List<LookupDetail> parkingType = parkingAllowanceBL
					.getLookupMasterBL().getActiveLookupDetails("PARKING_TYPE",
							true);

			List<LookupDetail> companyType = parkingAllowanceBL
					.getLookupMasterBL().getActiveLookupDetails("CARD_COMPANY",
							true);

			List<LookupDetail> cardType = parkingAllowanceBL
					.getLookupMasterBL().getActiveLookupDetails("CARD_TYPE",
							true);

			ServletActionContext.getRequest().setAttribute("PARKING_TYPE",
					parkingType);
			ServletActionContext.getRequest().setAttribute("CARD_COMPANY",
					companyType);

			ServletActionContext.getRequest().setAttribute("CARD_TYPE",
					cardType);

			ServletActionContext.getRequest().setAttribute("ALLOWANCE",
					parkingAllowanceVO);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String saveParkingAllowance() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		String returnMessage = "SUCCESS";
		try {

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			ParkingAllowance parkingAllowance = new ParkingAllowance();
			if (parkingAllowanceId != null && parkingAllowanceId > 0) {
				parkingAllowance = parkingAllowanceBL
						.getParkingAllowanceService().getParkingAllowanceById(
								parkingAllowanceId);

			} else {
				parkingAllowance.setCreatedDate(new Date());
				parkingAllowance.setPerson(person);
			}
			parkingAllowance.setAmount(amount);
			if (fromDate != null && !fromDate.equals(""))
				parkingAllowance.setFromDate(DateFormat
						.convertStringToDate(fromDate));
			if (toDate != null && !toDate.equals(""))
				parkingAllowance.setToDate(DateFormat
						.convertStringToDate(toDate));
			
			if (fromDate != null && !fromDate.equals(""))
				parkingAllowance.setFromDate(DateFormat
						.convertStringToDate(fromDate));
			parkingAllowance.setImplementation(getImplementation());

			parkingAllowance.setReceiptNumber(receiptNumber);
			if (isFinanceImpact != null
					&& isFinanceImpact.equalsIgnoreCase("true"))
				parkingAllowance.setIsFinanceImpact(true);
			else
				parkingAllowance.setIsFinanceImpact(false);
			if (jobAssignmentId != null) {
				JobAssignment jobAssignment = new JobAssignment();
				jobAssignment.setJobAssignmentId(jobAssignmentId);
				parkingAllowance.setJobAssignment(jobAssignment);
			}
			if (jobPayrollElementId != null) {
				JobPayrollElement jobPayrollElement = new JobPayrollElement();
				jobPayrollElement.setJobPayrollElementId(jobPayrollElementId);
				parkingAllowance.setJobPayrollElement(jobPayrollElement);
			}
			if (parkingType != null && parkingType > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(parkingType);
				parkingAllowance.setLookupDetailByParkingType(lookupDetail);
			}
			if (cardCompany != null && cardCompany > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(cardCompany);
				parkingAllowance.setLookupDetailByCardCompany(lookupDetail);
			}
			if (cardType != null && cardType > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(cardType);
				parkingAllowance.setLookupDetailByCardType(lookupDetail);
			}

			if (payPeriodTransactionId != null) {
				PayPeriodTransaction payPeriodTransaction = new PayPeriodTransaction();
				payPeriodTransaction.setPayPeriodTransactionId(payPeriodTransactionId);
				parkingAllowance.setPayPeriodTransaction(payPeriodTransaction);
			}
			parkingAllowance.setDescription(description);
			parkingAllowance.setCardNumber(cardNumber);

			returnMessage = parkingAllowanceBL
					.saveParkingAllowance(parkingAllowance);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("saveParkingAllowance() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (ArithmeticException ae) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Failure in creating the PhoneAllowance");
			return ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			if (parkingAllowanceId != null && parkingAllowanceId > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveParkingAllowance() Info unsuccessful Return");
			return ERROR;
		} finally {

		}
	}

	public String deleteParkingAllowance() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			String returnMessage = "SUCCESS";
			ParkingAllowance parkingAllowance = new ParkingAllowance();
			if (parkingAllowanceId != null && parkingAllowanceId > 0) {
				parkingAllowance = parkingAllowanceBL
						.getParkingAllowanceService().getParkingAllowanceById(
								parkingAllowanceId);

			}
			returnMessage = parkingAllowanceBL
					.deleteParkingAllowance(parkingAllowance);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("deleteParkingAllowance() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deleteParkingAllowance() Info unsuccessful Return");
			return ERROR;
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public PayrollBL getPayrollBL() {
		return payrollBL;
	}

	public void setPayrollBL(PayrollBL payrollBL) {
		this.payrollBL = payrollBL;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}

	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}

	public Long getJobPayrollElementId() {
		return jobPayrollElementId;
	}

	public void setJobPayrollElementId(Long jobPayrollElementId) {
		this.jobPayrollElementId = jobPayrollElementId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Byte getPayPolicy() {
		return payPolicy;
	}

	public void setPayPolicy(Byte payPolicy) {
		this.payPolicy = payPolicy;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getIsFinanceImpact() {
		return isFinanceImpact;
	}

	public void setIsFinanceImpact(String isFinanceImpact) {
		this.isFinanceImpact = isFinanceImpact;
	}

	public Long getPayPeriodTransactionId() {
		return payPeriodTransactionId;
	}

	public void setPayPeriodTransactionId(Long payPeriodTransactionId) {
		this.payPeriodTransactionId = payPeriodTransactionId;
	}

	public ParkingAllowanceBL getParkingAllowanceBL() {
		return parkingAllowanceBL;
	}

	public void setParkingAllowanceBL(ParkingAllowanceBL parkingAllowanceBL) {
		this.parkingAllowanceBL = parkingAllowanceBL;
	}

	public Long getParkingAllowanceId() {
		return parkingAllowanceId;
	}

	public void setParkingAllowanceId(Long parkingAllowanceId) {
		this.parkingAllowanceId = parkingAllowanceId;
	}

	public Long getCardType() {
		return cardType;
	}

	public void setCardType(Long cardType) {
		this.cardType = cardType;
	}

	public Long getParkingType() {
		return parkingType;
	}

	public void setParkingType(Long parkingType) {
		this.parkingType = parkingType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Long getCardCompany() {
		return cardCompany;
	}

	public void setCardCompany(Long cardCompany) {
		this.cardCompany = cardCompany;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
