package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobPayrollElement;
import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.PhoneAllowance;
import com.aiotech.aios.hr.domain.entity.vo.JobPayrollElementVO;
import com.aiotech.aios.hr.domain.entity.vo.PayPeriodVO;
import com.aiotech.aios.hr.domain.entity.vo.PhoneAllowanceVO;
import com.aiotech.aios.hr.service.bl.PayrollBL;
import com.aiotech.aios.hr.service.bl.PhoneAllowanceBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class PhoneAllowanceAction extends ActionSupport{

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(PhoneAllowanceAction.class);
	private Implementation implementation;
	private PhoneAllowanceBL phoneAllowanceBL;
	private PayrollBL payrollBL;
	private String transactionDate;
	private Integer id;
	private Long jobAssignmentId;
	private Long jobPayrollElementId;
	private List<Object> aaData;
	private Byte payPolicy;
	private Double amount;
	private String fromDate;
	private String toDate;
	private String receiptNumber;
	private String isFinanceImpact;
	private Long payPeriodTransactionId;
	private String description;


	private Long phoneAllowanceId;
	private Long networkProvider;
	private Long subscriptionType;
	
	public String returnSuccess() {
		ServletActionContext.getRequest().setAttribute("jobAssignmentId",
				jobAssignmentId);
		ServletActionContext.getRequest().setAttribute("jobPayrollElementId",
				jobPayrollElementId);
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String getPhoneList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();

			// Find the pay period of current transaction
			PayPeriodVO payPeriodVO = new PayPeriodVO();
			payPeriodVO.setJobAssignmentId(jobAssignmentId);
			payPeriodVO.setTransactionDate(DateFormat
					.convertStringToDate(transactionDate));
			payPeriodVO.setPayPolicy(payPolicy);
			payPeriodVO.setPayPeriodTransactionId(payPeriodTransactionId);

			PayPeriodTransaction payPeriodTransaction = phoneAllowanceBL
					.getPayPeriodBL().findPayPeriod(payPeriodVO);

			PhoneAllowanceVO phoneAllowanceVO = new PhoneAllowanceVO();
			if (payPeriodTransaction != null)
				phoneAllowanceVO
						.setPayPeriodTransaction(payPeriodTransaction);

			phoneAllowanceVO.setJobAssignmentId(jobAssignmentId);
			phoneAllowanceVO.setJobPayrollElementId(jobPayrollElementId);

			List<PhoneAllowance> phoneAllowances = phoneAllowanceBL
					.getPhoneAllowanceService()
					.getAllPhoneAllowanceByCriteria(phoneAllowanceVO);
			
			

			if (phoneAllowances != null && phoneAllowances.size() > 0) {
				PhoneAllowanceVO phoneVO = null;
				for (PhoneAllowance list : phoneAllowances) {
					phoneVO = new PhoneAllowanceVO();
					phoneVO.setPhoneAllowanceId(list.getPhoneAllowanceId());
					if(list.getLookupDetailBySubscriptionType()!=null)
					phoneVO.setSubscriptionType(list.getLookupDetailBySubscriptionType().getDisplayName());
					if(list.getLookupDetailByNetworkProvider()!=null)
					phoneVO.setNetworkProvider(list.getLookupDetailByNetworkProvider().getDisplayName());
				
					phoneVO
							.setFromDateView(DateFormat
									.convertDateToString(list.getFromDate()
											.toString()));
					phoneVO.setToDateView(DateFormat
							.convertDateToString(list.getToDate().toString()));
					phoneVO.setAmount(list.getAmount());
					if (list.getIsFinanceImpact() != null
							&& list.getIsFinanceImpact() == true)
						phoneVO.setIsFinanceView("YES");
					else
						phoneVO.setIsFinanceView("NO");

					aaData.add(phoneVO);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getPhoneAllowance() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			PhoneAllowanceVO phoneAllowanceVO = null;
			JobPayrollElementVO jobPayrollElementVO=null;
			if (phoneAllowanceId != null && phoneAllowanceId > 0) {
				PhoneAllowance phoneAllowance = phoneAllowanceBL
						.getPhoneAllowanceService()
						.getPhoneAllowanceById(phoneAllowanceId);
				if (phoneAllowance != null)
					phoneAllowanceVO = phoneAllowanceBL
							.convertEntityToVO(phoneAllowance);
			}
			
			//Find the Maximum provided & actual earned amount
			{
				// Find the pay period of current transaction
				PayPeriodVO payPeriodVO = new PayPeriodVO();
				payPeriodVO.setJobAssignmentId(jobAssignmentId);
				payPeriodVO.setTransactionDate(DateFormat
						.convertStringToDate(transactionDate));
				payPeriodVO.setPayPolicy(payPolicy);
				payPeriodVO.setPayPeriodTransactionId(payPeriodTransactionId);

				PayPeriodTransaction payPeriodTransaction = phoneAllowanceBL
						.getPayPeriodBL().findPayPeriod(payPeriodVO);
				PhoneAllowanceVO phoneAllowanceTempVO = new PhoneAllowanceVO();
				if (payPeriodTransaction != null)
					phoneAllowanceTempVO
							.setPayPeriodTransaction(payPeriodTransaction);

				phoneAllowanceTempVO.setJobAssignmentId(jobAssignmentId);
				phoneAllowanceTempVO
						.setJobPayrollElementId(jobPayrollElementId);

				List<PhoneAllowance> phoneAllowances = phoneAllowanceBL
						.getPhoneAllowanceService()
						.getAllPhoneAllowanceByCriteria(phoneAllowanceTempVO);

				jobPayrollElementVO = payrollBL.getJobPayrollInfomration(
						jobPayrollElementId, new ArrayList<Object>(
								phoneAllowances), payPeriodTransaction);
				
				ServletActionContext.getRequest().setAttribute("JOBPAY_INFO",
						jobPayrollElementVO);
			}
			
			
			List<LookupDetail> subscriptionType = phoneAllowanceBL
					.getLookupMasterBL().getActiveLookupDetails(
							"SUBSCRIPTION_TYPE", true);

			List<LookupDetail> netWorkProvider = phoneAllowanceBL
					.getLookupMasterBL().getActiveLookupDetails("NETWORK_PROVIDER",
							true);

			ServletActionContext.getRequest().setAttribute("SUBSCRIPTION_TYPE",
					subscriptionType);
			ServletActionContext.getRequest().setAttribute("NETWORK_PROVIDER",
					netWorkProvider);

			ServletActionContext.getRequest().setAttribute("PHONE",
					phoneAllowanceVO);
			
			
			
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	
	public String savePhoneAllowance() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		String returnMessage = "SUCCESS";
		try {

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			PhoneAllowance phoneAllowance = new PhoneAllowance();
			if (phoneAllowanceId != null && phoneAllowanceId > 0) {
				phoneAllowance = phoneAllowanceBL.getPhoneAllowanceService()
						.getPhoneAllowanceById(phoneAllowanceId);

			} else {
				phoneAllowance.setCreatedDate(new Date());
				phoneAllowance.setPerson(person);
			}
			phoneAllowance.setAmount(amount);
			if (fromDate != null && !fromDate.equals(""))
				phoneAllowance.setFromDate(DateFormat.convertStringToDate(fromDate));
			if (toDate != null && !toDate.equals(""))
				phoneAllowance.setToDate(DateFormat.convertStringToDate(toDate));
			phoneAllowance.setImplementation(getImplementation());
			phoneAllowance.setReceiptNumber(receiptNumber);
			if (isFinanceImpact != null
					&& isFinanceImpact.equalsIgnoreCase("true"))
				phoneAllowance.setIsFinanceImpact(true);
			else
				phoneAllowance.setIsFinanceImpact(false);
			if (jobAssignmentId != null) {
				JobAssignment jobAssignment = new JobAssignment();
				jobAssignment.setJobAssignmentId(jobAssignmentId);
				phoneAllowance.setJobAssignment(jobAssignment);
			}
			if (jobPayrollElementId != null) {
				JobPayrollElement jobPayrollElement = new JobPayrollElement();
				jobPayrollElement.setJobPayrollElementId(jobPayrollElementId);
				phoneAllowance.setJobPayrollElement(jobPayrollElement);
			}
			if (networkProvider != null && networkProvider > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(networkProvider);
				phoneAllowance.setLookupDetailByNetworkProvider(lookupDetail);
			}
			if (subscriptionType != null && subscriptionType > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(subscriptionType);
				phoneAllowance.setLookupDetailBySubscriptionType(lookupDetail);
			}
			if (payPeriodTransactionId != null) {
				PayPeriodTransaction payPeriodTransaction = new PayPeriodTransaction();
				payPeriodTransaction.setPayPeriodTransactionId(payPeriodTransactionId);
				phoneAllowance.setPayPeriodTransaction(payPeriodTransaction);
			}
			phoneAllowance.setDescription(description);
			returnMessage = phoneAllowanceBL.savePhoneAllowance(phoneAllowance);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("savePhoneAllowance() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (ArithmeticException ae) {
			ServletActionContext
					.getRequest()
					.setAttribute(
							"RETURN_MESSAGE",
							"Failure in creating the PhoneAllowance");
			return ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			if (phoneAllowanceId != null && phoneAllowanceId > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveInterview() Info unsuccessful Return");
			return ERROR;
		} finally {

		}
	}
	
	

	public String deletePhoneAllowance() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			String returnMessage = "SUCCESS";
			PhoneAllowance phoneAllowance = new PhoneAllowance();
			if (phoneAllowanceId != null && phoneAllowanceId > 0) {
				phoneAllowance = phoneAllowanceBL.getPhoneAllowanceService()
						.getPhoneAllowanceById(phoneAllowanceId);

			}
			returnMessage = phoneAllowanceBL.deletePhoneAllowance(phoneAllowance);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("deletePhoneAllowance() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deletePhoneAllowance() Info unsuccessful Return");
			return ERROR;
		}
	}

	
	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}
	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}
	public PayrollBL getPayrollBL() {
		return payrollBL;
	}
	public void setPayrollBL(PayrollBL payrollBL) {
		this.payrollBL = payrollBL;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}
	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}
	public Long getJobPayrollElementId() {
		return jobPayrollElementId;
	}
	public void setJobPayrollElementId(Long jobPayrollElementId) {
		this.jobPayrollElementId = jobPayrollElementId;
	}
	public List<Object> getAaData() {
		return aaData;
	}
	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}
	public Byte getPayPolicy() {
		return payPolicy;
	}
	public void setPayPolicy(Byte payPolicy) {
		this.payPolicy = payPolicy;
	}
	public PhoneAllowanceBL getPhoneAllowanceBL() {
		return phoneAllowanceBL;
	}
	public void setPhoneAllowanceBL(PhoneAllowanceBL phoneAllowanceBL) {
		this.phoneAllowanceBL = phoneAllowanceBL;
	}

	public Long getPhoneAllowanceId() {
		return phoneAllowanceId;
	}

	public void setPhoneAllowanceId(Long phoneAllowanceId) {
		this.phoneAllowanceId = phoneAllowanceId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getIsFinanceImpact() {
		return isFinanceImpact;
	}

	public void setIsFinanceImpact(String isFinanceImpact) {
		this.isFinanceImpact = isFinanceImpact;
	}

	public Long getNetworkProvider() {
		return networkProvider;
	}

	public void setNetworkProvider(Long networkProvider) {
		this.networkProvider = networkProvider;
	}

	public Long getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(Long subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	public Long getPayPeriodTransactionId() {
		return payPeriodTransactionId;
	}

	public void setPayPeriodTransactionId(Long payPeriodTransactionId) {
		this.payPeriodTransactionId = payPeriodTransactionId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
}
