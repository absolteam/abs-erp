package com.aiotech.aios.hr.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Designation;
import com.aiotech.aios.hr.domain.entity.Job;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobLeave;
import com.aiotech.aios.hr.domain.entity.JobPayrollElement;
import com.aiotech.aios.hr.domain.entity.JobShift;
import com.aiotech.aios.hr.domain.entity.Leave;
import com.aiotech.aios.hr.domain.entity.PayrollElement;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.PersonBank;
import com.aiotech.aios.hr.domain.entity.WorkingShift;
import com.aiotech.aios.hr.domain.entity.vo.WorkingShiftVO;
import com.aiotech.aios.hr.service.bl.JobBL;
import com.aiotech.aios.hr.to.HrPersonalDetailsTO;
import com.aiotech.aios.hr.to.converter.HrPersonalDetailsTOConverter;
import com.aiotech.aios.system.domain.entity.Country;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class JobAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager.getLogger(JobAction.class);

	private Long jobId;
	private String jobNumber;
	private String effectiveStartDate;
	private String effectiveEndDate;
	private Long swipeId;
	private Date createdDate;
	private Byte status;
	private String jobName;
	//
	private Long cmpDeptLocId;
	private Long companyId;
	private Long departmentId;
	private Long locationId;

	//
	private Long designationId;
	private Designation designation;
	private String designationName;
	private String description;

	//
	private Long gradeId;
	private String gradeName;
	private Integer sequence;

	private Long personId;
	private String allowanceLineDetail;
	private String leaveLineDetail;
	private String shiftLineDetail;
	private String assignmentLineDetail;

	private Integer id;

	private JobBL jobBL;
	private Implementation implementation;
	private String returnMessage;
	private Long jobAssignmentId;

	// JSON Listing Redirect
	public String returnSuccess() {

		if (null == id)
			id = 0;
		ServletActionContext.getRequest().setAttribute("rowId", id);
		ServletActionContext.getRequest().setAttribute("JOB_DESIGNATION_ID",
				designationId);
		return SUCCESS;
	}

	// Listing JSON
	public String getJobWorkingShiftList() {

		try {
			List<WorkingShift> workingShifts = jobBL.getWorkingShiftBL()
					.getWorkingShiftService()
					.getWorkingShiftList(getImplementation());
			List<WorkingShiftVO> workingShiftVOs = jobBL.getWorkingShiftBL()
					.convertEntitiesTOVOs(workingShifts);

			JSONObject jsonResponse = new JSONObject();
			if (workingShifts != null && workingShifts.size() > 0) {
				jsonResponse.put("iTotalRecords", workingShifts.size());
				jsonResponse.put("iTotalDisplayRecords", workingShifts.size());
			}
			JSONArray data = new JSONArray();
			for (WorkingShiftVO list : workingShiftVOs) {

				JSONArray array = new JSONArray();
				array.add(list.getWorkingShiftId() + "");
				array.add(list.getWorkingShiftName() + "");
				array.add(list.getStartTimeStr() + "");
				array.add(list.getEndTimeStr() + "");
				array.add(list.getLateAttendanceStr() + "");
				array.add(list.getSeviorLateAttendanceStr() + "");
				array.add(list.getBufferTimeStr() + "");
				array.add(list.getBreakHoursStr() + "");
				array.add(list.getIrregularDay() + "");
				array.add(list.getIsDefaultStr() + "");
				array.add(list.getIsActiveStr() + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info(" getWorkingShiftList() Sucessfull Return");
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(" getWorkingShiftList() unsuccessful Return");
			return ERROR;
		}
	}

	// Listing JSON
	public String execute() {

		try {
			List<Job> jobs = new ArrayList<Job>();
			jobs = jobBL.getJobService().getAllJobWithoutStatus(
					getImplementation());
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("iTotalRecords", jobs.size());
			jsonResponse.put("iTotalDisplayRecords", jobs.size());
			JSONArray data = new JSONArray();
			for (Job list : jobs) {
				list = jobBL.jobRemoveInactiveRec(list);
				JSONArray array = new JSONArray();
				array.add(list.getJobId());
				array.add(list.getJobNumber() + "");
				array.add(list.getJobName() + "");
				if (list.getDesignation() != null) {
					array.add(list.getDesignation().getDesignationName() + "");
					array.add(list.getDesignation().getGrade().getGradeName()
							+ "");
				} else {
					array.add("");
					array.add("");
				}
				String name = "";
				int i = 1;
				for (JobAssignment jobAssignment : list.getJobAssignments()) {
					name += i
							+ ") "
							+ jobAssignment.getPerson().getFirstName()
							+ " "
							+ jobAssignment.getPerson().getLastName()
							+ " ["
							+ jobAssignment.getDesignation()
									.getDesignationName() + "]";
					if (i < list.getJobAssignments().size())
						name += " ,";
					i++;
				}
				array.add(name);
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String viewJob() {
		try {

			if ((jobId == null || jobId <= 0) && jobAssignmentId != null
					&& jobAssignmentId > 0) {
				JobAssignment jobAssignment = jobBL.getJobService()
						.getJobAssignmentInfo(jobAssignmentId);
				if (jobAssignment != null)
					jobId = jobAssignment.getJob().getJobId();
			}

			addJob();
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		return SUCCESS;
	}

	// Listing the Personal Details
	public String addJob() {
		try {
			List<Integer> intelist = new ArrayList<Integer>();
			List<JobShift> tempjobShifts = null;
			Job job = new Job();
			if (jobId != null && jobId != 0) {
				job = jobBL.getJobService().getJobInfo(jobId);

				List<JobAssignment> tempjobAssignments = new ArrayList<JobAssignment>(
						job.getJobAssignments());
				List<JobLeave> tempjobLeavs = new ArrayList<JobLeave>(
						job.getJobLeaves());
				tempjobShifts = new ArrayList<JobShift>(job.getJobShifts());
				List<JobPayrollElement> tempjobPayrollElements = new ArrayList<JobPayrollElement>(
						job.getJobPayrollElements());

				for (JobAssignment jobAssignment : tempjobAssignments) {
					if (jobAssignment.getIsActive() == false)
						job.getJobAssignments().remove(jobAssignment);
				}
				for (JobLeave jobLeave : tempjobLeavs) {
					try {
						if (jobLeave.getLeave() == null
								|| jobLeave.getLeave().getDefaultDays() == null
								|| jobLeave.getIsActive() == false)
							job.getJobLeaves().remove(jobLeave);
					} catch (org.hibernate.LazyInitializationException ex) {
						job.getJobLeaves().remove(jobLeave);
					}
				}
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, -15);
				java.util.Date cutoffDate = cal.getTime();

				for (JobShift jobShift : tempjobShifts) {
					if (jobShift.getIsActive() == false
							|| (jobShift.getEffectiveStartDate() != null
									&& DateFormat.compareDates(cutoffDate,
											jobShift.getEffectiveStartDate()) == false && DateFormat
									.compareDates(cutoffDate,
											jobShift.getEffectiveEndDate()) == false))
						job.getJobShifts().remove(jobShift);
				}
				for (JobPayrollElement jobPayrollElement : tempjobPayrollElements) {
					if (jobPayrollElement.getIsActive() == false)
						job.getJobPayrollElements().remove(jobPayrollElement);
				}

				tempjobShifts = new ArrayList<JobShift>(job.getJobShifts());

				Collections.sort(tempjobShifts, new Comparator<JobShift>() {
					public int compare(JobShift m1, JobShift m2) {
						return m1.getJobShiftId().compareTo(m2.getJobShiftId());
					}
				});

			} else {
				List<Job> jobs = new ArrayList<Job>();
				jobs = jobBL.getJobService().getAllJob(getImplementation());
				if (jobs != null && jobs.size() > 0) {
					for (int i = 0; i < jobs.size(); i++) {
						intelist.add(Integer.parseInt(jobs.get(i)
								.getJobNumber()));
					}

					int temp = Collections.max(intelist);
					temp += 1;
					job.setJobNumber(temp + "");
				} else {
					job.setJobNumber(1000 + "");
				}
			}

			List<PayrollElement> payrollElements = jobBL.getJobService()
					.getAllPayrollElement(getImplementation());
			// List<Leave> leaves=
			// jobBL.getJobService().getAllLeave(getImplementation());
			List<WorkingShift> workingshifts = jobBL.getJobService()
					.getAllShifts(getImplementation());
			Map<Byte, String> payPolicy = jobBL.payPolicy();
			Map<Byte, String> payMode = jobBL.payMode();
			Map<Byte, String> calculationType = jobBL.calculationType();
			ServletActionContext.getRequest().setAttribute("PAY_POLICY",
					payPolicy);
			ServletActionContext.getRequest().setAttribute("PAY_MODE", payMode);
			ServletActionContext.getRequest().setAttribute("CALCULATION_TYPE",
					calculationType);
			ServletActionContext.getRequest().setAttribute("JOB", job);
			ServletActionContext.getRequest().setAttribute("JOB_SHIFT_LIST",
					tempjobShifts);
			ServletActionContext.getRequest().setAttribute(
					"PAYROLL_ELEMENT_LIST", payrollElements);
			// ServletActionContext.getRequest().setAttribute("LEAVE_TYPE_LIST",leaves);
			ServletActionContext.getRequest().setAttribute("SHIFT_TYPE_LIST",
					workingshifts);

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	// Allowance Add row
	public String jobAllowanceAddRow() {
		try {

			Map<Byte, String> payPolicy = jobBL.payPolicy();
			Map<Byte, String> payMode = jobBL.payMode();
			Map<Byte, String> calculationType = jobBL.calculationType();
			List<PayrollElement> payrollElement = jobBL.getJobService()
					.getAllPayrollElement(getImplementation());
			ServletActionContext.getRequest().setAttribute(
					"PAYROLL_ELEMENT_LIST", payrollElement);

			ServletActionContext.getRequest().setAttribute("PAY_POLICY",
					payPolicy);
			ServletActionContext.getRequest().setAttribute("PAY_MODE", payMode);
			ServletActionContext.getRequest().setAttribute("CALCULATION_TYPE",
					calculationType);
			if (null == id)
				id = 0;
			ServletActionContext.getRequest().setAttribute("rowId", id);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	// Leave Add row
	public String jobLeaveAddRow() {
		try {

			List<Leave> leaves = jobBL.getJobService().getAllLeave(
					getImplementation());
			ServletActionContext.getRequest().setAttribute("LEAVE_TYPE_LIST",
					leaves);
			if (null == id)
				id = 0;
			ServletActionContext.getRequest().setAttribute("rowId", id);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	// Shift Add row
	public String jobShiftAddRow() {
		try {

			List<WorkingShift> workingshifts = jobBL.getJobService()
					.getAllShifts(getImplementation());
			ServletActionContext.getRequest().setAttribute("SHIFT_TYPE_LIST",
					workingshifts);
			if (null == id)
				id = 0;
			ServletActionContext.getRequest().setAttribute("rowId", id);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	// Assignment Add row
	public String jobAssignmentAddRow() {
		try {

			if (null == id)
				id = 0;
			ServletActionContext.getRequest().setAttribute("rowId", id);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String getJobDesignationList() {
		try {

			JSONObject jsonResponse = new JSONObject();
			List<Designation> designation = jobBL.getJobService()
					.getAllDesignation(getImplementation());
			jsonResponse.put("iTotalRecords", designation.size());
			jsonResponse.put("iTotalDisplayRecords", designation.size());
			JSONArray data = new JSONArray();
			for (Designation list : designation) {

				JSONArray array = new JSONArray();
				array.add(list.getDesignationId());
				array.add(list.getDesignationName());
				array.add(list.getGrade().getGradeName());
				array.add(list.getDescription());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getJobLocationList() {
		try {
			JSONObject jsonResponse = new JSONObject();
			List<CmpDeptLoc> cmpDeptLocations = jobBL.getJobService()
					.getAllLocation(getImplementation());
			if (getImplementation().getCompanyKey() != null
					&& (getImplementation().getCompanyKey().equalsIgnoreCase(
							"gcc") || getImplementation().getCompanyKey()
							.equalsIgnoreCase("gcc-coffee"))) {
				cmpDeptLocations.addAll(jobBL.getJobService()
						.getAllSharedLocation(getImplementation()));
			}
			jsonResponse.put("iTotalRecords", cmpDeptLocations.size());
			jsonResponse.put("iTotalDisplayRecords", cmpDeptLocations.size());
			JSONArray data = new JSONArray();
			for (CmpDeptLoc list : cmpDeptLocations) {
				JSONArray array = new JSONArray();
				array.add(list.getCmpDeptLocId());
				array.add(list.getCompany().getCompanyName());
				array.add(list.getDepartment().getDepartmentName());
				array.add(list.getLocation().getLocationName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String saveJob() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Job job = new Job();
			List<Integer> intelist = new ArrayList<Integer>();
			if (jobId != 0) {
				job.setJobId(jobId);
				job = jobBL.getJobService().getJobInfo(jobId);
			} else {
				job.setCreatedDate(new Date());
				User user = (User) sessionObj.get("USER");
				Person person = new Person();
				person.setPersonId(user.getPersonId());
				job.setPerson(person);
				job.setEffectiveStartDate(new Date());

				// New Job Number Fetch
				List<Job> jobs = new ArrayList<Job>();
				jobs = jobBL.getJobService().getAllJob(getImplementation());
				if (jobs != null && jobs.size() > 0) {
					for (int i = 0; i < jobs.size(); i++) {
						intelist.add(Integer.parseInt(jobs.get(i)
								.getJobNumber()));
					}

					int temp = Collections.max(intelist);
					temp += 1;
					job.setJobNumber(temp + "");
				} else {
					job.setJobNumber(1000 + "");
				}
				job.setIsActive(true);

			}
			job.setJobName(jobName);
			job.setStatus(status);
			if (designationId != null) {
				Designation designation = new Designation();
				designation.setDesignationId(designationId);
				job.setDesignation(designation);
			}
			job.setImplementation(getImplementation());

			jobBL.jobSave(job, getAllowanceLineDetail(allowanceLineDetail),
					getLeaveLineDetail(leaveLineDetail),
					getShiftLineDetail(shiftLineDetail),
					getAssignmentLineDetail(assignmentLineDetail));
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public List<JobPayrollElement> getAllowanceLineDetail(
			String allowanceLineDetail) {
		List<JobPayrollElement> jobPayrollElements = new ArrayList<JobPayrollElement>();
		if (allowanceLineDetail != null && !allowanceLineDetail.equals("")) {
			String[] recordArray = new String[allowanceLineDetail.split("##").length];
			recordArray = allowanceLineDetail.split("##");
			for (String record : recordArray) {
				JobPayrollElement jobPayrollElement = new JobPayrollElement();
				PayrollElement payrollElement = new PayrollElement();
				PayrollElement payrollPercentageElement = null;
				String[] dataArray = new String[record.split("@").length];
				dataArray = record.split("@");
				if (!dataArray[0].equals("-1")
						&& !dataArray[0].trim().equals(""))
					jobPayrollElement.setJobPayrollElementId(Long
							.parseLong(dataArray[0]));
				if (!dataArray[1].equals("-1")
						&& !dataArray[1].trim().equals(""))
					payrollElement.setPayrollElementId(Long
							.parseLong(dataArray[1]));

				if (!dataArray[2].equals("-1")
						&& !dataArray[2].trim().equals(""))
					jobPayrollElement.setAmount(Double
							.parseDouble(dataArray[2]));

				if (!dataArray[3].equals("-1")
						&& !dataArray[3].trim().equals(""))
					jobPayrollElement
							.setPayPolicy(Byte.parseByte(dataArray[3]));

				if (!dataArray[4].equals("-1")
						&& !dataArray[4].trim().equals(""))
					jobPayrollElement.setPayType(Byte.parseByte(dataArray[4]));

				if (!dataArray[5].trim().equals("")
						&& !dataArray[5].trim().equals("-1"))
					jobPayrollElement.setCalculationType(Byte
							.parseByte(dataArray[5]));
				if (!dataArray[6].trim().equals("-1")
						&& !dataArray[6].trim().equals(""))
					jobPayrollElement.setCount(Integer.parseInt(dataArray[6]));

				if (!dataArray[7].trim().equals("-1")
						&& !dataArray[7].trim().equals("")) {
					payrollPercentageElement = new PayrollElement();
					payrollPercentageElement.setPayrollElementId(Long
							.parseLong(dataArray[7]));
					jobPayrollElement
							.setPayrollElementByPercentageElement(payrollPercentageElement);

				}
				jobPayrollElement
						.setPayrollElementByPayrollElementId(payrollElement);
				jobPayrollElement.setIsActive(true);
				jobPayrollElements.add(jobPayrollElement);
			}
		}
		return jobPayrollElements;
	}

	public List<JobLeave> getLeaveLineDetail(String leaveLineDetail) {
		List<JobLeave> jobLeaves = new ArrayList<JobLeave>();
		if (leaveLineDetail != null && !leaveLineDetail.equals("")) {
			String[] recordArray = new String[leaveLineDetail.split("##").length];
			recordArray = leaveLineDetail.split("##");
			for (String record : recordArray) {
				JobLeave jobLeave = new JobLeave();
				Leave leave = new Leave();
				String[] dataArray = new String[record.split("@").length];
				dataArray = record.split("@");
				if (!dataArray[0].equals("-1")
						&& !dataArray[0].trim().equals(""))
					jobLeave.setJobLeaveId(Long.parseLong(dataArray[0]));
				if (!dataArray[1].equals("-1")
						&& !dataArray[1].trim().equals(""))
					leave.setLeaveId(Long.parseLong(dataArray[1]));
				if (!dataArray[2].equals("-1")
						&& !dataArray[2].trim().equals(""))
					jobLeave.setDays(new BigDecimal(dataArray[2]));

				jobLeave.setLeave(leave);
				jobLeave.setIsActive(true);
				jobLeaves.add(jobLeave);
			}
		}
		return jobLeaves;
	}

	public List<JobShift> getShiftLineDetail(String shiftLineDetail) {
		List<JobShift> jobShifts = new ArrayList<JobShift>();
		if (shiftLineDetail != null && !shiftLineDetail.equals("")) {
			String[] recordArray = new String[shiftLineDetail.split("##").length];
			recordArray = shiftLineDetail.split("##");
			for (String record : recordArray) {
				JobShift jobShift = new JobShift();
				WorkingShift workingShift = new WorkingShift();
				String[] dataArray = new String[record.split("@").length];
				dataArray = record.split("@");
				if (!dataArray[0].equals("-1")
						&& !dataArray[0].trim().equals(""))
					jobShift.setJobShiftId(Long.parseLong(dataArray[0]));
				if (!dataArray[1].equals("-1")
						&& !dataArray[1].trim().equals(""))
					workingShift
							.setWorkingShiftId(Long.parseLong(dataArray[1]));
				if (!dataArray[2].equals("-1")
						&& !dataArray[2].trim().equals(""))
					jobShift.setEffectiveStartDate(DateFormat
							.convertStringToDate(dataArray[2]));
				if (!dataArray[3].equals("-1")
						&& !dataArray[3].trim().equals(""))
					jobShift.setEffectiveEndDate(DateFormat
							.convertStringToDate(dataArray[3]));

				jobShift.setIsActive(true);

				jobShift.setWorkingShift(workingShift);

				jobShifts.add(jobShift);
			}
		}
		return jobShifts;
	}

	public List<JobAssignment> getAssignmentLineDetail(
			String assignmentLineDetail) {
		List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();
		if (assignmentLineDetail != null && !assignmentLineDetail.equals("")) {
			String[] recordArray = new String[assignmentLineDetail.split("##").length];
			recordArray = assignmentLineDetail.split("##");
			for (String record : recordArray) {
				JobAssignment jobAssignment = new JobAssignment();
				Person person = new Person();
				Designation designation = new Designation();
				CmpDeptLoc cmpDeptLocation = new CmpDeptLoc();
				String[] dataArray = new String[record.split("@").length];
				dataArray = record.split("@");
				if (!dataArray[0].equals("-1")
						&& !dataArray[0].trim().equals(""))
					jobAssignment.setJobAssignmentId(Long
							.parseLong(dataArray[0]));
				if (!dataArray[1].equals("-1")
						&& !dataArray[1].trim().equals("")) {
					person.setPersonId(Long.parseLong(dataArray[1]));
					jobAssignment.setPerson(person);
				}
				if (!dataArray[2].equals("-1")
						&& !dataArray[2].trim().equals("")) {
					designation.setDesignationId(Long.parseLong(dataArray[2]));
					jobAssignment.setDesignation(designation);
				}
				if (!dataArray[3].equals("-1")
						&& !dataArray[3].trim().equals("")) {
					cmpDeptLocation.setCmpDeptLocId(Long
							.parseLong(dataArray[3]));
					jobAssignment.setCmpDeptLocation(cmpDeptLocation);
				}
				if (!dataArray[4].equals("-1")
						&& !dataArray[4].trim().equals(""))
					jobAssignment.setSwipeId(Long.parseLong(dataArray[4]));
				if (!dataArray[5].equals("-1")
						&& !dataArray[5].trim().equals("")) {
					PersonBank personBank = new PersonBank();
					personBank.setPersonBankId(Long.parseLong(dataArray[5]));
					jobAssignment.setPersonBank(personBank);
				}
				if (!dataArray[6].trim().equals("")
						&& dataArray[6].trim().equalsIgnoreCase("true"))
					jobAssignment.setIsLocalPay(true);
				else
					jobAssignment.setIsLocalPay(false);
				if (!dataArray[7].equals("-1")
						&& !dataArray[7].trim().equals(""))
					jobAssignment.setPayMode(dataArray[7]);

				if (!dataArray[8].equals("-1")
						&& !dataArray[8].trim().equals("")) {
					jobAssignment.setJobAssignmentNumber(dataArray[8]);
				} else {
					List<Integer> intelist = new ArrayList<Integer>();
					List<JobAssignment> jobs = new ArrayList<JobAssignment>();
					jobs = jobBL.getJobService().getAllJobAssignment(
							getImplementation());
					if (jobs != null && jobs.size() > 0) {
						for (int i = 0; i < jobs.size(); i++) {
							intelist.add(Integer.parseInt(jobs.get(i)
									.getJobAssignmentNumber()));
						}

						int temp = Collections.max(intelist);
						temp += 1;
						jobAssignment.setJobAssignmentNumber(temp + "");
					} else {
						jobAssignment.setJobAssignmentNumber(1000 + "");
					}
				}

				jobAssignment.setIsActive(true);
				jobAssignments.add(jobAssignment);
			}
		}
		return jobAssignments;
	}

	public String deleteJob() {
		try {

			Job job = new Job();
			if (jobId != null && jobId != 0) {
				job = jobBL.getJobService().getJobForDelete(jobId);
			}

			boolean returnFlag = jobBL.deleteJob(job);
			if (returnFlag == true)
				returnMessage = "SUCCESS";
			else
				returnMessage = "Can not delete. This Job is in Use.";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			returnMessage = "Can not delete. This Job is in Use.";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return ERROR;
		}
	}

	public String getAllEmployee() {
		try {
			JSONObject jsonResponse = new JSONObject();

			List<HrPersonalDetailsTO> personalDetailsList = new ArrayList<HrPersonalDetailsTO>();
			// Set the value to push to Personal Details DAO
			List<Person> person = jobBL.getPersonService()
					.getAllEmployeeForJob(getImplementation());

			personalDetailsList = HrPersonalDetailsTOConverter
					.convertToTOList(person);

			Country country = null;
			// PersonType personType=null;
			if (personalDetailsList != null && personalDetailsList.size() > 0) {
				country = new Country();
				// personType= new PersonType();
				for (int i = 0; i < personalDetailsList.size(); i++) {
					// Find the country
					country = jobBL.getPersonService().getCountry(
							Long.parseLong(personalDetailsList.get(i)
									.getCountryCode()));
					personalDetailsList.get(i).setCountryBirth(
							country.getCountryName());

					// personType=jobBL.getPersonService().getPersonType(Integer.parseInt(personalDetailsList.get(i).getPersonTypes()));
					// personalDetailsList.get(i).setPersonTypes(personType.getTypeName());
				}

				jsonResponse.put("iTotalRecords", personalDetailsList.size());
				jsonResponse.put("iTotalDisplayRecords",
						personalDetailsList.size());

			}

			JSONArray data = new JSONArray();
			for (HrPersonalDetailsTO list : personalDetailsList) {

				JSONArray array = new JSONArray();
				array.add(list.getPersonId());
				array.add(list.getFirstName());
				array.add(list.getLastName());
				array.add(list.getPersonTypes());
				array.add(list.getCountryBirth());
				array.add(list.getIdentification() + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	// Getter & Setter
	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public String getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public String getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(String effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	public Long getSwipeId() {
		return swipeId;
	}

	public void setSwipeId(Long swipeId) {
		this.swipeId = swipeId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public Long getCmpDeptLocId() {
		return cmpDeptLocId;
	}

	public void setCmpDeptLocId(Long cmpDeptLocId) {
		this.cmpDeptLocId = cmpDeptLocId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public Long getDesignationId() {
		return designationId;
	}

	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}

	public Designation getDesignation() {
		return designation;
	}

	public void setDesignation(Designation designation) {
		this.designation = designation;
	}

	public String getDesignationName() {
		return designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getGradeId() {
		return gradeId;
	}

	public void setGradeId(Long gradeId) {
		this.gradeId = gradeId;
	}

	public String getGradeName() {
		return gradeName;
	}

	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public JobBL getJobBL() {
		return jobBL;
	}

	public void setJobBL(JobBL jobBL) {
		this.jobBL = jobBL;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Implementation getImplementation() {

		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public String getAllowanceLineDetail() {
		return allowanceLineDetail;
	}

	public void setAllowanceLineDetail(String allowanceLineDetail) {
		this.allowanceLineDetail = allowanceLineDetail;
	}

	public String getLeaveLineDetail() {
		return leaveLineDetail;
	}

	public void setLeaveLineDetail(String leaveLineDetail) {
		this.leaveLineDetail = leaveLineDetail;
	}

	public String getShiftLineDetail() {
		return shiftLineDetail;
	}

	public void setShiftLineDetail(String shiftLineDetail) {
		this.shiftLineDetail = shiftLineDetail;
	}

	public String getAssignmentLineDetail() {
		return assignmentLineDetail;
	}

	public void setAssignmentLineDetail(String assignmentLineDetail) {
		this.assignmentLineDetail = assignmentLineDetail;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}

	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}
}
