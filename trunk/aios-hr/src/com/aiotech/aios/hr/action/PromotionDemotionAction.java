package com.aiotech.aios.hr.action;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.Job;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobPayrollElement;
import com.aiotech.aios.hr.domain.entity.OpenPosition;
import com.aiotech.aios.hr.domain.entity.PayrollElement;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.PromotionDemotion;
import com.aiotech.aios.hr.domain.entity.ResignationTermination;
import com.aiotech.aios.hr.domain.entity.vo.PayrollTransactionVO;
import com.aiotech.aios.hr.domain.entity.vo.PayrollVO;
import com.aiotech.aios.hr.domain.entity.vo.PromotionDemotionVO;
import com.aiotech.aios.hr.domain.entity.vo.ResignationTerminationVO;
import com.aiotech.aios.hr.service.bl.PromotionDemotionBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class PromotionDemotionAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(PromotionDemotionAction.class);
	private Implementation implementation;
	private PromotionDemotionBL promotionDemotionBL;
	private Long jobAssignmentId;
	private List<Object> aaData;
	private Double amount;
	private String fromDate;
	private String toDate;
	private String description;
	private Byte status;
	private Long promotionDemotionId;
	private Boolean positionAutoRelease;
	private String effectiveMonth;
	private String reason;
	private Long openPositionId;
	private Long requestedPerson;
	private Byte type;
	private String effectiveDate;
	private Long jobId;
	private String requestedDate;
	private Long recordId;
	private String printableContent;

	public String returnSuccess() {
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String getPromotionDemotionList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();

			List<PromotionDemotion> promotionDemotions = promotionDemotionBL
					.getPromotionDemotionService().getAllPromotionDemotion(
							getImplementation());

			if (promotionDemotions != null && promotionDemotions.size() > 0) {
				PromotionDemotionVO vo = null;
				for (PromotionDemotion list : promotionDemotions) {
					vo = new PromotionDemotionVO();
					vo.setPromotionDemotionId(list.getPromotionDemotionId());
					vo.setEmployeeName(list.getJobAssignment().getPerson()
							.getFirstName()
							+ " "
							+ list.getJobAssignment().getPerson().getLastName());
					vo.setJobNumber(list.getJobAssignment()
							.getJobAssignmentNumber());
					vo.setEffectiveDateView(DateFormat.convertDateToString(list
							.getEffectiveDate().toString()));
					vo.setEffectiveMonth(list.getEffectiveMonth());
					vo.setFromDateView(DateFormat.convertDateToString(list
							.getFromDate().toString()));
					vo.setToDateView(DateFormat.convertDateToString(list
							.getToDate().toString()));
					vo.setTypeView(Constants.HR.PromotionType.get(
							list.getType()).name());
					vo.setCurrentPosition(list.getJobAssignment()
							.getDesignation().getDesignationName());
					if (list.getOpenPosition() != null)
						vo.setNextPosition(list.getOpenPosition()
								.getDesignation().getDesignationName());

					vo.setRequestedBy(list.getPersonByRequestedBy()
							.getFirstName()
							+ " "
							+ list.getPersonByRequestedBy().getLastName());
					vo.setPromotionStatus(Constants.HR.PromotionStatus
							.get(list.getStatus()).name()
							.replaceAll("\\d+", "")
							.replaceAll("(.)([A-Z])", "$1 $2"));
					aaData.add(vo);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getPromotionDemotionAdd() {
		try {

			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			CommentVO comment = new CommentVO();

			if (recordId != null && recordId > 0) {
				promotionDemotionId = recordId;
				comment.setRecordId(Long.parseLong(promotionDemotionId + ""));
				comment.setUseCase("com.aiotech.aios.hr.domain.entity.IdentityAvailability");
				comment = promotionDemotionBL.getCommentBL().getCommentInfo(
						comment);
			}
			PromotionDemotionVO promotionDemotionVO = null;

			if (promotionDemotionId != null && promotionDemotionId > 0) {
				PromotionDemotion promotionDemotion = promotionDemotionBL
						.getPromotionDemotionService()
						.getPromotionDemotionById(promotionDemotionId);
				if (promotionDemotion != null)
					promotionDemotionVO = promotionDemotionBL
							.convertEntityToVO(promotionDemotion);
			}

			List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();

			jobAssignments = promotionDemotionBL.getJobAssignmentBL()
					.getJobAssignmentService()
					.getAllActiveJobAssignment(getImplementation());

			ServletActionContext.getRequest().setAttribute(
					"JOB_ASSIGNMENT_LIST", jobAssignments);
			Map<Byte, String> promotionTypes = promotionDemotionBL
					.promotionType();
			ServletActionContext.getRequest().setAttribute("PROMOTION_TYPE",
					promotionTypes);
			ServletActionContext.getRequest().setAttribute("PROMOTION",
					promotionDemotionVO);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String savePromotionDemotion() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		String returnMessage = "SUCCESS";
		try {

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			PromotionDemotion promotionDemotion = new PromotionDemotion();
			if (promotionDemotionId != null && promotionDemotionId > 0) {
				promotionDemotion = promotionDemotionBL
						.getPromotionDemotionService()
						.getPromotionDemotionById(promotionDemotionId);
				promotionDemotion.setUpdatedDate(new Date());
				promotionDemotion.setPersonByUpdatedBy(person);
			} else {
				promotionDemotion.setCreatedDate(new Date());
				promotionDemotion.setPersonByCreatedBy(person);
				promotionDemotion.setUpdatedDate(new Date());
				promotionDemotion.setPersonByUpdatedBy(person);
			}

			if (jobAssignmentId != null) {
				JobAssignment jobAssignment = new JobAssignment();
				jobAssignment.setJobAssignmentId(jobAssignmentId);
				promotionDemotion.setJobAssignment(jobAssignment);
			}

			if (fromDate != null && !fromDate.equals(""))
				promotionDemotion.setFromDate(DateFormat
						.convertStringToDate(fromDate));
			if (toDate != null && !toDate.equals(""))
				promotionDemotion.setToDate(DateFormat
						.convertStringToDate(toDate));

			if (effectiveDate != null && !effectiveDate.equals(""))
				promotionDemotion.setEffectiveDate(DateFormat
						.convertStringToDate(effectiveDate));

			if (requestedDate != null && !requestedDate.equals(""))
				promotionDemotion.setRequestedDate(DateFormat
						.convertStringToDate(requestedDate));

			if (promotionDemotion.getFromDate() == null) {
				promotionDemotion.setFromDate(promotionDemotion
						.getEffectiveDate());
				promotionDemotion.setToDate(promotionDemotion
						.getEffectiveDate());
			}

			promotionDemotion.setImplementation(getImplementation());

			if (openPositionId != null) {
				OpenPosition openPosition = new OpenPosition();
				openPosition.setOpenPositionId(openPositionId);
				promotionDemotion.setOpenPosition(openPosition);
			}

			if (jobId != null) {
				Job job = new Job();
				job.setJobId(jobId);
				promotionDemotion.setJob(job);
			}

			if (requestedPerson != null) {
				Person reqPerson = new Person();
				reqPerson.setPersonId(requestedPerson);
				promotionDemotion.setPersonByRequestedBy(reqPerson);
			}
			promotionDemotion.setReason(reason);
			promotionDemotion.setDescription(description);
			promotionDemotion.setType(type);
			promotionDemotion.setStatus(Constants.HR.PromotionStatus.Created
					.getCode());
			returnMessage = promotionDemotionBL
					.savePromotionDemotion(promotionDemotion);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("savePromotionDemotion() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (ArithmeticException ae) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Failure in creating the savePromotionDemotion");
			return ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			if (promotionDemotionId != null && promotionDemotionId > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("savePromotionDemotion() Info unsuccessful Return");
			return ERROR;
		} finally {

		}
	}

	public String promotionDemationPrintOut() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Boolean isTemplatePrintEnabled = session
					.getAttribute("print_promotion_template") != null ? ((String) session
					.getAttribute("print_promotion_template"))
					.equalsIgnoreCase("true") : false;
			PromotionDemotionVO vo = null;

			if (promotionDemotionId != null && promotionDemotionId > 0) {
				PromotionDemotion promotionDemotion = promotionDemotionBL
						.getPromotionDemotionService()
						.getPromotionDemotionById(promotionDemotionId);
				if (promotionDemotion != null)
					vo = promotionDemotionBL
							.convertEntityToVO(promotionDemotion);

			}

			if (!isTemplatePrintEnabled) {
				ServletActionContext.getRequest().setAttribute(
						"PROMOTION_DEMOTION_INFO", vo);
				return "default";
			} else {
				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/hr/"
						+ getImplementation().getImplementationId()));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config
						.getTemplate("html-promotion-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();

				if (getImplementation().getMainLogo() != null) {

					byte[] bFile = FileUtil.getBytes(new File(
							getImplementation().getMainLogo()));
					bFile = Base64.encodeBase64(bFile);

					rootMap.put("logo", new String(bFile));
				} else {
					rootMap.put("logo", "");
				}

				rootMap.put("urlPath", urlPath);
				rootMap.put("requestedDate", vo.getCreatedDateView());
				rootMap.put("employee", vo.getPersonName());
				rootMap.put("department", vo.getDepartmentName());
				rootMap.put("designation", vo.getDesignationName());
				if (vo.getOpenPosition() != null
						&& vo.getOpenPosition().getDesignation() != null)
					rootMap.put("newDesignation", vo.getOpenPosition()
							.getDesignation().getDesignationName());
				else
					rootMap.put("newDesignation", "-NA-");

				rootMap.put("location", vo.getLocationName());
				rootMap.put("company", vo.getCompanyName());
				rootMap.put("employeeID", vo.getPersonNumber());
				rootMap.put("actionType", vo.getTypeView().toUpperCase());
				rootMap.put("fromDate", vo.getEffectiveDateView());

				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date());
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);
				rootMap.put("orderNumber", vo.getJobAssignment()
						.getJobAssignmentNumber()
						+ "-"
						+ month
						+ ""
						+ day
						+ "/" + year);
				rootMap.put("remarks", vo.getReason());
				rootMap.put("comments", vo.getDescription());

				// Cross Salary
				List<JobPayrollElement> payrollElements = promotionDemotionBL
						.getJobAssignmentBL().getJobBL().getJobService()
						.getAllJobPayrollElement(vo.getJob());
				Double corssSalary = 0.0;
				String salaryDetails = "";
				int count = 1;
				for (JobPayrollElement jobPayrollElement : payrollElements) {

					corssSalary += jobPayrollElement.getAmount();
					if (count == 1) {
						salaryDetails += jobPayrollElement
								.getPayrollElementByPayrollElementId()
								.getElementName()
								+ " of AED " + jobPayrollElement.getAmount();

					} else {
						salaryDetails += " ,"
								+ jobPayrollElement
										.getPayrollElementByPayrollElementId()
										.getElementName() + " of AED "
								+ jobPayrollElement.getAmount();

					}
					count++;
				}

				rootMap.put("salaryDetails", salaryDetails);
				rootMap.put("grossSalary", corssSalary);

				// Cross Salary
				payrollElements = promotionDemotionBL
						.getJobAssignmentBL()
						.getJobBL()
						.getJobService()
						.getAllJobPayrollElement(vo.getJobAssignment().getJob());
				corssSalary = 0.0;
				salaryDetails = "";
				count = 1;
				for (JobPayrollElement jobPayrollElement : payrollElements) {

					corssSalary += jobPayrollElement.getAmount();
					if (count == 1) {
						salaryDetails += jobPayrollElement
								.getPayrollElementByPayrollElementId()
								.getElementName()
								+ " of AED " + jobPayrollElement.getAmount();

					} else {
						salaryDetails += " ,"
								+ jobPayrollElement
										.getPayrollElementByPayrollElementId()
										.getElementName() + " of AED "
								+ jobPayrollElement.getAmount();

					}
					count++;
				}

				rootMap.put("oldSalaryDetails", salaryDetails);
				rootMap.put("oldGrossSalary", corssSalary);

				String decimalAmount = String.valueOf(AIOSCommons
						.formatAmount(corssSalary));
				decimalAmount = decimalAmount.substring(decimalAmount
						.lastIndexOf('.') + 1);
				String amountInWords = AIOSCommons
						.convertAmountToWords(corssSalary);
				if (Double.parseDouble(decimalAmount) > 0) {
					amountInWords = amountInWords
							.concat(" and ")
							.concat(AIOSCommons
									.convertAmountToWords(decimalAmount))
							.concat(" Fils ");
					String replaceWord = "Only";
					amountInWords = amountInWords.replaceAll(replaceWord, "");
					amountInWords = amountInWords.concat(" " + replaceWord);
				}
				rootMap.put("grossSalaryInWords", amountInWords);
				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
				// System.out.println(printableContent);

				return "template";
			}

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String deletePromotionDemotion() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			String returnMessage = "SUCCESS";
			PromotionDemotion promotionDemotion = new PromotionDemotion();
			if (promotionDemotionId != null && promotionDemotionId > 0) {
				promotionDemotion = promotionDemotionBL
						.getPromotionDemotionService()
						.getPromotionDemotionById(promotionDemotionId);

			}
			promotionDemotionBL.deletePromotionDemotion(promotionDemotion);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("deletePayByCount() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deletePayByCount() Info unsuccessful Return");
			return ERROR;
		}
	}

	public String approvePromotionDemotion() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		String returnMessage = "SUCCESS";
		try {

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			PromotionDemotion promotionDemotion = new PromotionDemotion();
			if (promotionDemotionId != null && promotionDemotionId > 0) {
				promotionDemotion = promotionDemotionBL
						.getPromotionDemotionService()
						.getPromotionDemotionById(promotionDemotionId);
				promotionDemotion.setUpdatedDate(new Date());
				promotionDemotion.setPersonByUpdatedBy(person);
				promotionDemotion
						.setStatus(Constants.HR.PromotionStatus.Approved
								.getCode());
			}
			returnMessage = promotionDemotionBL
					.savePromotionDemotion(promotionDemotion);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("savePromotionDemotion() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (ArithmeticException ae) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Failure in creating the savePromotionDemotion");
			return ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			if (promotionDemotionId != null && promotionDemotionId > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("savePromotionDemotion() Info unsuccessful Return");
			return ERROR;
		} finally {

		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}

	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public PromotionDemotionBL getPromotionDemotionBL() {
		return promotionDemotionBL;
	}

	public void setPromotionDemotionBL(PromotionDemotionBL promotionDemotionBL) {
		this.promotionDemotionBL = promotionDemotionBL;
	}

	public Long getPromotionDemotionId() {
		return promotionDemotionId;
	}

	public void setPromotionDemotionId(Long promotionDemotionId) {
		this.promotionDemotionId = promotionDemotionId;
	}

	public Boolean getPositionAutoRelease() {
		return positionAutoRelease;
	}

	public void setPositionAutoRelease(Boolean positionAutoRelease) {
		this.positionAutoRelease = positionAutoRelease;
	}

	public String getEffectiveMonth() {
		return effectiveMonth;
	}

	public void setEffectiveMonth(String effectiveMonth) {
		this.effectiveMonth = effectiveMonth;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Long getOpenPositionId() {
		return openPositionId;
	}

	public void setOpenPositionId(Long openPositionId) {
		this.openPositionId = openPositionId;
	}

	public Long getRequestedPerson() {
		return requestedPerson;
	}

	public void setRequestedPerson(Long requestedPerson) {
		this.requestedPerson = requestedPerson;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public String getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(String requestedDate) {
		this.requestedDate = requestedDate;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}
}
