package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.HR.InterviewStatus;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Candidate;
import com.aiotech.aios.hr.domain.entity.Interview;
import com.aiotech.aios.hr.domain.entity.InterviewProcess;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.CandidateVO;
import com.aiotech.aios.hr.domain.entity.vo.InterviewVO;
import com.aiotech.aios.hr.domain.entity.vo.JobAssignmentVO;
import com.aiotech.aios.hr.service.bl.InterviewBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class InterviewAction extends ActionSupport{
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(InterviewAction.class);
	private Implementation implementation;
	private InterviewBL interviewBL;
	private List<Object> aaData;
	private Interview interview;
	private Long interviewId;
	private Integer id;
	private String interviewDate;
	private String interviewTime;
	private String candidates;
	private String interviewers;
	private Long recordId;
	private Long alertId;
	private Long interviewProcessId;
	private Byte rating;
	private String score;
	private String remarks;
	private Byte status;
	public String returnSuccess() {

		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	public String getInterviewList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();
			List<Interview> interviews = interviewBL.getInterviewService()
					.getAllInterview(getImplementation());
			if (interviews != null && interviews.size() > 0) {
				InterviewVO interviewVO = null;
				for (Interview list : interviews) {
					interviewVO = new InterviewVO();
					InterviewVO temp = interviewBL.convertEntityIntoVO(list);
					interviewVO.setInterviewId(temp.getInterviewId());
					interviewVO.setPositionReferenceNumber(temp
							.getPositionReferenceNumber());
					interviewVO.setDesignationName(temp.getDesignationName());
					interviewVO.setQuestionType(temp.getQuestionType());
					interviewVO.setRoundView(temp.getRoundView());
					interviewVO.setInterviewDateView(temp.getInterviewDateView()+" "+temp.getInterviewTimeView());
					interviewVO.setRoom(temp.getRoom());
					interviewVO.setAddress(temp.getAddress());
					interviewVO.setStatusView(InterviewStatus.get(list.getStatus()).name());
					aaData.add(interviewVO);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	public String getInterviewAdd() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			InterviewVO interviewVO = null;
			if (interviewId != null && interviewId > 0) {
				interviewVO = new InterviewVO();
				interview = interviewBL.getInterviewService().getInterviewById(
						interviewId);
				interviewVO = interviewBL.convertEntityIntoVO(interview);
				// Apply the Conversion to set the Entity value into EntityVO

			} else {
				interviewVO = new InterviewVO();
				interviewVO.setCreatedDateView(DateFormat
						.convertSystemDateToString(new Date()));
			}
			Map<Integer, String> statuses = interviewBL.status();
			Map<Integer, String> rounds = interviewBL.rounds();
			

			List<LookupDetail> interviewHalls = interviewBL.getLookupMasterBL()
					.getActiveLookupDetails("INTERVIEW_HALL", true);

			List<Candidate> candidates = interviewBL.getCandidateBL()
					.getCandidateService().getAllCandidate(getImplementation());
			List<CandidateVO> candidateVOs = interviewBL.getCandidateBL()
					.convertAllEntityToVO(candidates);

			List<JobAssignment> jobAssignments = interviewBL
					.getJobAssignmentBL().getJobAssignmentService()
					.getAllActiveJobAssignment(getImplementation());
			List<JobAssignmentVO> jobAssignmentVOs = interviewBL
					.getJobAssignmentBL().convertEntitiesTOVOs(jobAssignments);

			ServletActionContext.getRequest().setAttribute("INTERVIWERS",
					jobAssignmentVOs);
			ServletActionContext.getRequest().setAttribute("CANDIDATES",
					candidateVOs);
			ServletActionContext.getRequest().setAttribute("INTERVIEW_HALL",
					interviewHalls);
			ServletActionContext.getRequest().setAttribute("STATUS", statuses);
			ServletActionContext.getRequest().setAttribute("ROUNDS", rounds);
			ServletActionContext.getRequest().setAttribute("INTERVIEW",
					interviewVO);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	public String getInterviewUpdate() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			AIOSCommons.removeUploaderSession(session, "uploadedDocs", -1L,
			"InterviewProcess");

			InterviewVO interviewVO = null;
			if (recordId != null && recordId > 0) {
				interviewVO = new InterviewVO();
				InterviewProcess interviewProcess = interviewBL
						.getInterviewService()
						.getInterviewProcessById(recordId);
				interviewVO = interviewBL.convertEntityIntoVO(interviewProcess);
				// Apply the Conversion to set the Entity value into EntityVO

			} else {
				interviewVO = new InterviewVO();
				interviewVO.setCreatedDateView(DateFormat
						.convertSystemDateToString(new Date()));
			}
			Map<Integer, String> statuses = interviewBL.status();
			Map<Integer, String> rounds = interviewBL.rounds();
			Map<Integer, String> rattings = interviewBL.rattings();

			List<LookupDetail> interviewHalls = interviewBL.getLookupMasterBL()
					.getActiveLookupDetails("INTERVIEW_HALL", true);

			List<Candidate> candidates = interviewBL.getCandidateBL()
					.getCandidateService().getAllCandidate(getImplementation());
			List<CandidateVO> candidateVOs = interviewBL.getCandidateBL()
					.convertAllEntityToVO(candidates);

			List<JobAssignment> jobAssignments = interviewBL
					.getJobAssignmentBL().getJobAssignmentService()
					.getAllActiveJobAssignment(getImplementation());
			List<JobAssignmentVO> jobAssignmentVOs = interviewBL
					.getJobAssignmentBL().convertEntitiesTOVOs(jobAssignments);

			ServletActionContext.getRequest().setAttribute("INTERVIWERS",
					jobAssignmentVOs);
			ServletActionContext.getRequest().setAttribute("CANDIDATES",
					candidateVOs);
			ServletActionContext.getRequest().setAttribute("INTERVIEW_HALL",
					interviewHalls);
			ServletActionContext.getRequest().setAttribute("STATUS", statuses);
			ServletActionContext.getRequest().setAttribute("ROUNDS", rounds);
			ServletActionContext.getRequest().setAttribute("RATTING", rattings);
			ServletActionContext.getRequest().setAttribute("INTERVIEW",
					interviewVO);
			ServletActionContext.getRequest().setAttribute("alertId",
					alertId);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	public String saveInterview() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		String returnMessage = null;
		try {

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			if (interview.getInterviewId() != null
					&& interview.getInterviewId() > 0) {

			} else {
				interview.setCreatedDate(new Date());
				interview.setInterviewId(null);
			}

			if (interviewDate != null && interviewTime != null)
				interview.setInterviewTime(DateFormat
						.convertStringToDateTimeFormat(interviewDate + " "
								+ interviewTime));

			interview.setImplementation(getImplementation());
			interview.setIsApprove(Byte
					.valueOf(Constants.RealEstate.Status.NoDecession.getCode()
							+ ""));
			InterviewVO interviewVO = new InterviewVO();

			interviewVO.setInterviewersArray(interview.getInterviewersGroup());
			interviewVO.setCandidatesArray(candidates);

			interviewBL.saveInterview(interview, interviewVO);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("saveInterview() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (ArithmeticException ae) {
			ServletActionContext
					.getRequest()
					.setAttribute("RETURN_MESSAGE",
							"Failure in creating the notification for interviewer! Application access must be provided to interviewers");
			return ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			if (interview.getInterviewId() != null
					&& interview.getInterviewId() > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveInterview() Info unsuccessful Return");
			return ERROR;
		} finally {

		}
	}
	
	public String deleteInterview() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			InterviewVO interviewVO = null;
			if (interviewId != null && interviewId > 0) {
				interview = interviewBL.getInterviewService().getInterviewById(
						interviewId);
				List<Alert> alerts = new ArrayList<Alert>();
				interviewVO = new InterviewVO();
				if (interview != null) {
					for (InterviewProcess interviewProcess : interview
							.getInterviewProcesses()) {
						List<Alert> alertstemp = interviewBL
								.getAlertBL()
								.getAlertService()
								.getAlertAllRecordInfo(
										interviewProcess
												.getInterviewProcessId(),
										"InterviewProcess");
						if (alertstemp != null && alertstemp.size() > 0)
							alerts.addAll(alertstemp);
					}
				}
				interviewVO.setAlerts(alerts);

			}
			interviewBL.deleteInterview(interview, interviewVO);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("deleteInterview() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deleteInterview() Info unsuccessful Return");
			return ERROR;
		}
	}
	

	public String updateInterviewProcess() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		try {

			InterviewProcess interviewProcess = interviewBL
					.getInterviewService().getInterviewProcessById(
							interviewProcessId);

			interviewProcess.setRating(rating);
			interviewProcess.setRemarks(remarks);
			interviewProcess.setScore(score);
			interviewProcess.setStatus(status);
			interviewBL.saveInterviewProcess(interviewProcess, alertId);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("saveInterview() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (ArithmeticException ae) {
			ServletActionContext
					.getRequest()
					.setAttribute(
							"RETURN_MESSAGE",
							"Failure in creating the notification for interviewer! Application access must be provided to interviewers");
			return ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Update Failure");

			return ERROR;
		} finally {

		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public InterviewBL getInterviewBL() {
		return interviewBL;
	}

	public void setInterviewBL(InterviewBL interviewBL) {
		this.interviewBL = interviewBL;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Interview getInterview() {
		return interview;
	}

	public void setInterview(Interview interview) {
		this.interview = interview;
	}

	public Long getInterviewId() {
		return interviewId;
	}

	public void setInterviewId(Long interviewId) {
		this.interviewId = interviewId;
	}

	public String getInterviewDate() {
		return interviewDate;
	}

	public void setInterviewDate(String interviewDate) {
		this.interviewDate = interviewDate;
	}

	public String getInterviewTime() {
		return interviewTime;
	}

	public void setInterviewTime(String interviewTime) {
		this.interviewTime = interviewTime;
	}

	public String getCandidates() {
		return candidates;
	}

	public void setCandidates(String candidates) {
		this.candidates = candidates;
	}

	public String getInterviewers() {
		return interviewers;
	}

	public void setInterviewers(String interviewers) {
		this.interviewers = interviewers;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public Long getInterviewProcessId() {
		return interviewProcessId;
	}

	public void setInterviewProcessId(Long interviewProcessId) {
		this.interviewProcessId = interviewProcessId;
	}

	public Byte getRating() {
		return rating;
	}

	public void setRating(Byte rating) {
		this.rating = rating;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}
}
