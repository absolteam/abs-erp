package com.aiotech.aios.hr.action;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Attendance;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.SwipeInOut;
import com.aiotech.aios.hr.domain.entity.vo.AttendanceVO;
import com.aiotech.aios.hr.service.bl.AttendanceBL;
import com.aiotech.aios.hr.to.AttendanceTO;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class AttendanceAction extends ActionSupport {
	private AttendanceBL attendanceBL;
	private Implementation implementation;
	private CommentBL commentBL;
	private Long personId;
	private Long recordId;
	private Long attendanceId;
	private Long deductionType;
	private String comments;
	private boolean halfDay;
	private String returnMessage;
	private String reason;
	private Integer id;
	private String attendanceDate;
	private String swipeLineDetail;
	private Long jobId;
	private Long jobAssignmentId;
	private String fromDate;
	private String toDate;
	private Long companyId;
	private Integer calcultionMethod;
	private String format;
	private boolean absent;
	private String persons;

	List<AttendanceTO> attendanceTOs;
	private Map<String, Object> jasperRptParams = new HashMap<String, Object>();
	private InputStream fileInputStream;

	private static final Properties AIOS_PROPERTIES = new Properties();
	static {
		try {
			AIOS_PROPERTIES.load(AttendanceBL.class.getClassLoader()
					.getResourceAsStream("application.jmx.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String returnSuccess() {
		ServletActionContext.getRequest().setAttribute("rowid", id);
		/*
		 * try {
		 * 
		 * Client client = Client.create();
		 * 
		 * WebResource webResource = client .resource(
		 * "http://localhost:8081/aios-web/service/rest/getPointOfSaleSync");
		 * SalesInvoiceVO vo = webResource.type(MediaType.APPLICATION_JSON)
		 * .get(SalesInvoiceVO.class);
		 * 
		 * client = Client.create(); webResource = client .resource(
		 * "http://localhost:8081/aios-web/service/rest/sendpointOfSaleSync");
		 * ClientResponse response = webResource.type(MediaType.APPLICATION_XML)
		 * .post(ClientResponse.class, vo); if (response.getStatus() != 200) {
		 * throw new RuntimeException("Failed : HTTP error code : " +
		 * response.getStatus()); } String output =
		 * response.getEntity(String.class); System.out.println(output);
		 * 
		 * } catch (Exception e) {
		 * 
		 * e.printStackTrace();
		 * 
		 * }
		 */
		return SUCCESS;
	}

	public String getAttendanceListJson() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			List<Attendance> attendancesses = new ArrayList<Attendance>();
			JSONObject jsonResponse = new JSONObject();
			attendancesses = attendanceBL.getAttendanceService()
					.getAllAttendanceBasedOnPerson(person);
			if (attendancesses != null && attendancesses.size() > 0) {
				jsonResponse.put("iTotalRecords", attendancesses.size());
				jsonResponse.put("iTotalDisplayRecords", attendancesses.size());
				Collections.sort(attendancesses, new Comparator<Attendance>() {
					public int compare(Attendance m1, Attendance m2) {
						return m2.getAttendanceDate().compareTo(
								m1.getAttendanceDate());
					}
				});
			}
			JSONArray data = new JSONArray();
			for (Attendance list : attendancesses) {
				JSONArray array = new JSONArray();
				array.add(list.getAttendanceId() + "");
				array.add(DateFormat.convertDateToString(list
						.getAttendanceDate().toString()) + "");
				if (list.getIsAbsent() != null && list.getIsAbsent() == true)
					array.add("Yes");
				else
					array.add("No");
				array.add(list.getTimeIn() + "");
				array.add(list.getTimeOut() + "");
				array.add(list.getLateIn() + "");
				array.add(list.getLateOut() + "");
				array.add(list.getEarlyIn() + "");
				array.add(list.getEarlyOut() + "");
				// Time Loss Or Excess
				if (list.getTimeLoss() != null
						&& !list.getTimeLoss().equals(""))
					array.add("Loss :" + list.getTimeLoss());
				else if (list.getTimeExcess() != null
						&& !list.getTimeExcess().equals(""))
					array.add("Excess : " + list.getTimeExcess());
				else
					array.add("-NA-");

				array.add(list.getTotalHours() + "");
				if (list.getIsDeviation() != null
						&& list.getIsDeviation() == true)
					array.add("Yes");
				else
					array.add("No");
				if (list.getIsApprove() != null
						&& (Constants.RealEstate.Status
								.get(list.getIsApprove()).name()
								.equals("Approved") || Constants.RealEstate.Status
								.get(list.getIsApprove()).name()
								.equals("ApprovedRead"))) {
					array.add("Approved");
					array.add(attendanceBL.attendanceProcessTypeInfo(list
							.getDeductionType()) + "");
				} else if ((Constants.RealEstate.Status
						.get(list.getIsApprove()).name().equals("Disapproved") || Constants.RealEstate.Status
						.get(list.getIsApprove()).name()
						.equals("DisapprovedRead"))) {
					array.add("Rejected");
					array.add(attendanceBL.attendanceProcessTypeInfo(list
							.getDeductionType()) + "");

				} else {
					array.add("Pending");
					array.add("-NA-");

				}
				array.add(list.getSystemSuggesion() + "");
				if (list.getHalfDay() != null && list.getHalfDay() == true)
					array.add("Yes");
				else
					array.add("No");
				array.add(list.getReason() + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getAttendanceExecutionListJson() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			List<Attendance> attendancesses = new ArrayList<Attendance>();
			JSONObject jsonResponse = new JSONObject();
			attendancesses = attendanceBL.getAttendanceService()
					.getAllAttendance(getImplementation());
			if (attendancesses != null && attendancesses.size() > 0) {
				jsonResponse.put("iTotalRecords", attendancesses.size());
				jsonResponse.put("iTotalDisplayRecords", attendancesses.size());
				Collections.sort(attendancesses, new Comparator<Attendance>() {
					public int compare(Attendance m1, Attendance m2) {
						return m2.getAttendanceDate().compareTo(
								m1.getAttendanceDate());
					}
				});
			}
			JSONArray data = new JSONArray();
			for (Attendance list : attendancesses) {
				JSONArray array = new JSONArray();
				array.add(list.getAttendanceId() + "");
				JobAssignment jobAssignment = list.getJobAssignment();

				array.add(jobAssignment.getPerson().getFirstName() + " "
						+ jobAssignment.getPerson().getLastName());
				array.add(jobAssignment.getDesignation().getDesignationName());
				array.add(jobAssignment.getCmpDeptLocation().getCompany()
						.getCompanyName()
						+ " >> "
						+ jobAssignment.getCmpDeptLocation().getDepartment()
								.getDepartmentName()
						+ " >> "
						+ jobAssignment.getCmpDeptLocation().getLocation()
								.getLocationName());
				array.add(DateFormat.convertDateToString(list
						.getAttendanceDate().toString()) + "");

				// Time in
				if (list.getTimeIn() != null && !list.getTimeIn().equals(""))
					array.add(DateFormat.convertTimeToString(list.getTimeIn()
							.toString()) + "");
				else
					array.add("-NA-");

				// Time out
				if (list.getTimeOut() != null && !list.getTimeOut().equals(""))
					array.add(DateFormat.convertTimeToString(list.getTimeOut()
							.toString()) + "");
				else
					array.add("-NA-");

				// Time Loss Or Excess
				if (list.getTimeLoss() != null
						&& !list.getTimeLoss().equals(""))
					array.add("Loss :" + list.getTimeLoss());
				else if (list.getTimeExcess() != null
						&& !list.getTimeExcess().equals(""))
					array.add("Excess : " + list.getTimeExcess());
				else
					array.add("-NA-");

				// Total Hours
				if (list.getTotalHours() != null
						&& !list.getTotalHours().equals(""))
					array.add(list.getTotalHours() + "");
				else
					array.add("-NA-");

				// Is APProve
				if (list.getIsApprove() != null
						&& (Constants.RealEstate.Status
								.get(list.getIsApprove()).name()
								.equals("Approved") || Constants.RealEstate.Status
								.get(list.getIsApprove()).name()
								.equals("ApprovedRead"))) {
					array.add("Approved");
					array.add(attendanceBL.attendanceProcessTypeInfo(list
							.getDeductionType()) + "");
				} else if ((Constants.RealEstate.Status
						.get(list.getIsApprove()).name().equals("Disapproved") || Constants.RealEstate.Status
						.get(list.getIsApprove()).name()
						.equals("DisapprovedRead"))) {
					array.add("Rejected");
					array.add(attendanceBL.attendanceProcessTypeInfo(list
							.getDeductionType()) + "");

				} else {
					array.add("Pending");
					array.add("-NA-");

				}

				// Is half day
				if (list.getHalfDay() != null && list.getHalfDay() == true)
					array.add("Yes");
				else
					array.add("No");

				// Is Absent
				if (list.getIsAbsent() != null && list.getIsAbsent() == true)
					array.add("Yes");
				else
					array.add("No");

				// System Suggestion
				array.add(list.getSystemSuggesion() + "");

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getAttendanceDeviationProcess() {
		try {

			Attendance attendance = attendanceBL
					.getAttendanceDeviationProcess(recordId);
			ServletActionContext.getRequest().setAttribute(
					"ATTENDANCE_PROCESS", attendance);
			JobAssignment jobAssigment = attendance.getJobAssignment();
			AttendancePolicy attedancePolicy = attendanceBL
					.getAttendanceService().getAttendancePolicyInfo(
							jobAssigment.getDesignation().getGrade()
									.getGradeId(), getImplementation());
			double compOffDays = 0.0;

			Date firstDayOfYear = new Date();
			Date lastDayOfYear = new Date();

			int year = Calendar.getInstance().get(Calendar.YEAR);
			Calendar cal = Calendar.getInstance();
			// Current Date

			// First Day of Year
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, 0); // 0 = jan
			cal.set(Calendar.DAY_OF_YEAR, 1);
			firstDayOfYear = cal.getTime();

			// Last Day of Year
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, 11); // 11 = december
			cal.set(Calendar.DAY_OF_MONTH, 31); // new years eve
			lastDayOfYear = cal.getTime();

			// Comment Information --Added by rafiq
			Comment comment = new Comment();

			if (recordId != 0) {
				try {
					comment.setRecordId(recordId);
					comment.setUseCase("com.aiotech.aios.hr.domain.entity.Attendance");
					comment = commentBL.getCommentInfo(comment);
				} catch (Exception e) {
					e.printStackTrace();
				}

				List<Attendance> compOffList = attendanceBL
						.getAttendanceService().getAllAttendanceForCompOff(
								attendance.getJobAssignment().getPerson(),
								firstDayOfYear, lastDayOfYear);
				if (compOffList != null && compOffList.size() > 0) {
					for (Attendance attendance1 : compOffList) {
						if (attendance1.getDeductionType() == 4
								&& attendance1.getHalfDay() == true) {
							compOffDays = compOffDays + 0.5;
						} else if (attendance1.getDeductionType() == 4) {
							compOffDays = compOffDays + 1;
						} else if (attendance1.getDeductionType() == 5
								&& attendance1.getHalfDay() == true) {
							compOffDays = compOffDays - 0.5;
						} else if (attendance1.getDeductionType() == 5) {
							compOffDays = compOffDays - 1;
						}
					}
				}

			}
			ServletActionContext.getRequest().getSession()
					.setAttribute("COMMENT_IFNO", comment);
			ServletActionContext.getRequest().setAttribute("ATTENDANCE_POLICY",
					attedancePolicy);
			ServletActionContext.getRequest().setAttribute("COMPOFF_DAYS",
					compOffDays);
			ServletActionContext.getRequest().setAttribute("COMPOFF_DAYS",
					compOffDays);
			Map<Integer, String> calculationMethods = attendanceBL
					.calculationMethods();
			ServletActionContext.getRequest().setAttribute(
					"CALCULATION_METHOD", calculationMethods);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getAttendanceProcessEmployeeUpdate() {
		try {

			Attendance attendance = attendanceBL
					.getAttendanceDeviationProcess(attendanceId);
			ServletActionContext.getRequest().setAttribute(
					"ATTENDANCE_PROCESS", attendance);
			JobAssignment jobAssigment = attendance.getJobAssignment();
			AttendancePolicy attedancePolicy = attendanceBL
					.getAttendanceService().getAttendancePolicyInfo(
							jobAssigment.getDesignation().getGrade()
									.getGradeId(), getImplementation());
			double compOffDays = 0.0;

			Date firstDayOfYear = new Date();
			Date lastDayOfYear = new Date();

			int year = Calendar.getInstance().get(Calendar.YEAR);
			Calendar cal = Calendar.getInstance();
			// Current Date

			// First Day of Year
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, 0); // 0 = jan
			cal.set(Calendar.DAY_OF_YEAR, 1);
			firstDayOfYear = cal.getTime();

			// Last Day of Year
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, 11); // 11 = december
			cal.set(Calendar.DAY_OF_MONTH, 31); // new years eve
			lastDayOfYear = cal.getTime();

			// Comment Information --Added by rafiq
			Comment comment = new Comment();

			if (attendanceId != 0) {
				try {
					comment.setRecordId(attendanceId);
					comment.setUseCase("com.aiotech.aios.hr.domain.entity.Attendance");
					comment = commentBL.getCommentInfo(comment);
				} catch (Exception e) {
					e.printStackTrace();
				}

				List<Attendance> compOffList = attendanceBL
						.getAttendanceService().getAllAttendanceForCompOff(
								attendance.getJobAssignment().getPerson(),
								firstDayOfYear, lastDayOfYear);
				if (compOffList != null && compOffList.size() > 0) {
					for (Attendance attendance1 : compOffList) {
						if (attendance1.getDeductionType() == null)
							continue;

						if (attendance1.getDeductionType() == 4
								&& attendance1.getHalfDay() == true) {
							compOffDays = compOffDays + 0.5;
						} else if (attendance1.getDeductionType() == 4) {
							compOffDays = compOffDays + 1;
						} else if (attendance1.getDeductionType() == 5
								&& attendance1.getHalfDay() == true) {
							compOffDays = compOffDays - 0.5;
						} else if (attendance1.getDeductionType() == 5) {
							compOffDays = compOffDays - 1;
						}
					}
				}
			}
			ServletActionContext.getRequest().getSession()
					.setAttribute("COMMENT_IFNO", comment);
			ServletActionContext.getRequest().setAttribute("ATTENDANCE_POLICY",
					attedancePolicy);
			ServletActionContext.getRequest().setAttribute("COMPOFF_DAYS",
					compOffDays);
			Map<Integer, String> calculationMethods = attendanceBL
					.calculationMethods();
			ServletActionContext.getRequest().setAttribute(
					"CALCULATION_METHOD", calculationMethods);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getAttendanceListForApprovalJson() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Person person = new Person();
			person.setPersonId(personId);
			List<Attendance> attendancessesTemp = new ArrayList<Attendance>();
			List<Attendance> attendancesses = new ArrayList<Attendance>();
			JSONObject jsonResponse = new JSONObject();
			attendancessesTemp = attendanceBL.getAttendanceService()
					.getAllAttendanceBasedOnPerson(person);
			for (Attendance attendance : attendancessesTemp) {
				if (!attendance.getAttendanceId().equals(attendanceId))
					attendancesses.add(attendance);
			}
			if (attendancesses != null && attendancesses.size() > 0) {
				jsonResponse.put("iTotalRecords", attendancesses.size());
				jsonResponse.put("iTotalDisplayRecords", attendancesses.size());
				Collections.sort(attendancesses, new Comparator<Attendance>() {
					public int compare(Attendance m1, Attendance m2) {
						return m2.getAttendanceDate().compareTo(
								m1.getAttendanceDate());
					}
				});

			}
			JSONArray data = new JSONArray();
			for (Attendance list : attendancesses) {
				JSONArray array = new JSONArray();
				array.add(list.getAttendanceId() + "");
				array.add(DateFormat.convertDateToString(list
						.getAttendanceDate().toString()) + "");
				// Time in
				if (list.getTimeIn() != null && !list.getTimeIn().equals(""))
					array.add(DateFormat.convertTimeToString(list.getTimeIn()
							.toString()) + "");
				else
					array.add("-NA-");

				// Time out
				if (list.getTimeOut() != null && !list.getTimeOut().equals(""))
					array.add(DateFormat.convertTimeToString(list.getTimeOut()
							.toString()) + "");
				else
					array.add("-NA-");
				array.add(list.getLateIn() + "");
				array.add(list.getLateOut() + "");
				array.add(list.getEarlyIn() + "");
				array.add(list.getEarlyOut() + "");
				array.add(list.getTimeLoss() + "");
				array.add(list.getTimeExcess() + "");
				array.add(list.getTotalHours() + "");
				if (list.getIsDeviation() != null
						&& list.getIsDeviation() == true)
					array.add("Yes");
				else
					array.add("No");
				if (list.getIsApprove() != null
						&& (Constants.RealEstate.Status
								.get(list.getIsApprove()).name()
								.equals("Approved") || Constants.RealEstate.Status
								.get(list.getIsApprove()).name()
								.equals("ApprovedRead")))
					array.add("Approved");
				else if ((Constants.RealEstate.Status.get(list.getIsApprove())
						.name().equals("Disapproved") || Constants.RealEstate.Status
						.get(list.getIsApprove()).name()
						.equals("DisapprovedRead")))
					array.add("Rejected");
				else
					array.add("Pending");
				array.add(list.getSystemSuggesion() + "");
				array.add(attendanceBL.attendanceProcessTypeInfo(list
						.getDeductionType()) + "");
				if (list.getHalfDay() != null && list.getHalfDay() == true)
					array.add("Yes");
				else
					array.add("No");

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getAttendanceDeviationSave() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			if (attendanceId != null && attendanceId != 0) {
				Attendance attendance = attendanceBL.getAttendanceService()
						.getAttendanceInfoForDevationProcess(attendanceId);
				attendance.setDeductionType(Byte.parseByte(deductionType + ""));
				if (halfDay == true)
					attendance.setHalfDay(true);
				else if (halfDay == false)
					attendance.setHalfDay(false);
				// attendance.setIsApprove(Byte.parseByte(Constants.RealEstate.Status.Approved.getCode()+""));
				attendance.setComment(comments);
				attendance.setCalcultionMethod(calcultionMethod);
				attendanceBL.getAttendanceDeviationSave(attendance);
				returnMessage = "SUCCESS";

			}

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getAttendanceSave() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			if (attendanceId != null && attendanceId != 0) {
				Attendance attendance = attendanceBL.getAttendanceService()
						.getAttendanceInfoForDevationProcess(attendanceId);
				attendance.setReason(reason);
				attendanceBL.getAttendanceService().saveAttendance(attendance);

			}

			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			returnMessage = "Sorry, Update Error.";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return ERROR;
		}
	}

	public String updateAttendnaceDecision() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			List<Attendance> attendanceList = null;
			if (swipeLineDetail != null && !swipeLineDetail.equals("")) {
				attendanceList = new ArrayList<Attendance>();
				Attendance attendance = null;
				String[] recordArray = new String[swipeLineDetail.split("##").length];
				recordArray = swipeLineDetail.split("##");
				for (String record : recordArray) {
					String[] dataArray = new String[record.split("@").length];
					dataArray = record.split("@");
					attendance = attendanceBL.getAttendanceService()
							.getAttendanceInfoForDevationProcess(
									Long.valueOf(dataArray[0]));
					if (dataArray[1] != null && !dataArray[1].equals("-1"))
						attendance.setDeductionType(Byte.valueOf(dataArray[1]));

					if (dataArray[2] != null && !dataArray[2].equals("-1"))
						attendance.setCalcultionMethod(Integer
								.valueOf(dataArray[2]));

					if (dataArray[3] != null && !dataArray[3].equals("-1"))
						attendance.setComment(dataArray[3]);
					
					attendanceList.add(attendance);
				}

				attendanceBL.getAttendanceService().saveAllAttendance(
						attendanceList);

			}

			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			returnMessage = "Sorry, Update Error.";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return ERROR;
		}
	}

	public String deleteAttendance() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			if (attendanceId != null && attendanceId != 0) {
				Attendance attendance = attendanceBL.getAttendanceService()
						.getAttendanceInfoFindById(attendanceId);
				attendanceBL.deleteAttendance(attendance);

			}

			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			returnMessage = "Deletion Failure";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			e.printStackTrace();
			return ERROR;
		}
	}

	public String hrAttendanceSave() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			AttendanceVO attendanceVO = new AttendanceVO();
			attendanceVO.setIsAbsent(absent);
			if (attendanceDate != null && !attendanceDate.equals(""))
				attendanceVO.setAttendanceDate(DateFormat
						.convertStringToDate(attendanceDate));

			attendanceBL.hrSaveAttendanceManual(
					getSwipeLineDetail(swipeLineDetail), jobAssignmentId,
					attendanceVO);

			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			returnMessage = "Please check the Shift/Other Configuration";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			e.printStackTrace();
			return ERROR;
		}
	}

	public List<SwipeInOut> getSwipeLineDetail(String swipeLineDetail) {
		List<SwipeInOut> swipeInOuts = new ArrayList<SwipeInOut>();
		if (swipeLineDetail != null && !swipeLineDetail.equals("")) {
			String[] recordArray = new String[swipeLineDetail.split("##").length];
			recordArray = swipeLineDetail.split("##");
			for (String record : recordArray) {
				SwipeInOut swipeInOut = new SwipeInOut();
				String[] dataArray = new String[record.split("@").length];
				dataArray = record.split("@");
				// if(!dataArray[0].equals("-1") &&
				// !dataArray[0].trim().equals(""))
				// swipeInOut.setSwipeInOutId(Integer.parseInt(dataArray[0]));
				if (!dataArray[1].equals("-1")
						&& !dataArray[1].trim().equals(""))
					swipeInOut.setAttendanceDate(DateFormat
							.convertStringToDate(dataArray[1]));
				if (!dataArray[2].equals("-1")
						&& !dataArray[2].trim().equals(""))
					swipeInOut.setTimeInOutType(dataArray[2]);
				if (!dataArray[3].equals("-1")
						&& !dataArray[3].trim().equals(""))
					swipeInOut.setTimeInOut(DateFormat
							.convertStringToTime(dataArray[3]));
				if (!dataArray[4].equals("-1")
						&& !dataArray[4].trim().equals(""))
					swipeInOut.setSwipeId(Long.parseLong(dataArray[4]));

				swipeInOut.setStatus(Byte.parseByte("1"));
				swipeInOut.setImplementation(getImplementation());

				swipeInOuts.add(swipeInOut);
			}
		}
		return swipeInOuts;
	}

	public String getAddAttendanceHR() {
		try {
			AttendancePolicy attedancePolicy = new AttendancePolicy();
			Attendance attendance = new Attendance();
			List<SwipeInOut> swipeInOuts = new ArrayList<SwipeInOut>();
			if (attendanceId != null && attendanceId > 0) {
				attendance = attendanceBL
						.getAttendanceDeviationProcess(attendanceId);
				JobAssignment jobAssigment = attendance.getJobAssignment();
				attedancePolicy = attendanceBL.getAttendanceService()
						.getAttendancePolicyInfo(
								jobAssigment.getDesignation().getGrade()
										.getGradeId(), getImplementation());

				swipeInOuts = attendanceBL.getAttendanceService()
						.getAllSwipeInOutWithDate(jobAssigment.getSwipeId(),
								getImplementation(),
								attendance.getAttendanceDate());
			}
			ServletActionContext.getRequest().setAttribute("ATTENDANCE_POLICY",
					attedancePolicy);
			ServletActionContext.getRequest().setAttribute(
					"ATTENDANCE_PROCESS", attendance);
			ServletActionContext.getRequest().setAttribute(
					"ATTENDANCE_SWIPE_LIST", swipeInOuts);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getEmployeeDesignationJson() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			List<JobAssignment> jobs = new ArrayList<JobAssignment>();
			JSONObject jsonResponse = new JSONObject();
			jobs = attendanceBL.getJobService().getAllActiveJobAssignment(
					getImplementation());
			if (jobs != null && jobs.size() > 0) {
				jsonResponse.put("iTotalRecords", jobs.size());
				jsonResponse.put("iTotalDisplayRecords", jobs.size());
			}
			JSONArray data = new JSONArray();
			for (JobAssignment list : jobs) {
				JSONArray array = new JSONArray();
				array.add(list.getJobAssignmentId() + "");
				array.add(list.getPerson().getPersonId() + "");
				array.add(list.getPerson().getFirstName() + " "
						+ list.getPerson().getLastName() + " - "
						+ list.getPerson().getPersonNumber());
				array.add(list.getDesignation().getDesignationName());
				array.add(list.getDesignation().getGrade().getGradeName());
				array.add(list.getCmpDeptLocation().getCompany()
						.getCompanyName());
				array.add(list.getCmpDeptLocation().getDepartment()
						.getDepartmentName());
				array.add(list.getCmpDeptLocation().getLocation()
						.getLocationName());
				array.add(list.getSwipeId() + "");
				array.add(list.getJob().getJobId() + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getSwipeInOutAddRow() {
		if (null == id)
			id = 0;

		ServletActionContext.getRequest().setAttribute("ATTENDANCE_DATE",
				attendanceDate);
		ServletActionContext.getRequest().setAttribute("rowId", id);
		return SUCCESS;
	}

	public String read() {
		try {
			boolean returnFlag = attendanceBL.attedanceReader(null, null);
			if (returnFlag == true)
				returnMessage = "SUCCESS";
			else
				returnMessage = "Attendance Information/File Not Available";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			returnMessage = "Attendance Information/File Not Available";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
		}
		return SUCCESS;
	}

	public String calculate() {
		try {
			attendanceBL
					.attendaceExcution(
							DateFormat.convertStringToDate(fromDate),
							DateFormat.convertStringToDate(toDate),
							getImplementation(),persons);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			returnMessage = "Wrong or Missing configuration!..Check the Shift & Attendance policy for each employee";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
		}
		return SUCCESS;
	}

	// ------------------------------Report
	// mapping----------------------------------
	public String getAttendancePunchReport() {

		return SUCCESS;
	}

	public String attendancePunchJson() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			AttendanceTO attendanceTO = new AttendanceTO();
			JSONObject jsonResponse = new JSONObject();
			if (personId > 0 && fromDate != null && !fromDate.equals("")) {
				attendanceTO = attendanceBL.punchReport(personId,
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate));
			} else {
				List<AttendanceTO> attendanceTOs = new ArrayList<AttendanceTO>();
				attendanceTO.setAttendanceHistoryList(attendanceTOs);
			}
			if (attendanceTO.getAttendanceHistoryList() != null
					&& attendanceTO.getAttendanceHistoryList().size() > 0) {
				jsonResponse.put("iTotalRecords", attendanceTO
						.getAttendanceHistoryList().size());
				jsonResponse.put("iTotalDisplayRecords", attendanceTO
						.getAttendanceHistoryList().size());
			}
			JSONArray data = new JSONArray();
			if (attendanceTO.getAttendanceHistoryList() != null
					&& attendanceTO.getAttendanceHistoryList().size() > 0) {
				for (AttendanceTO list : attendanceTO
						.getAttendanceHistoryList()) {
					JSONArray array = new JSONArray();
					array.add(list.getAttendanceId() + "");
					array.add(list.getAttedanceDate() + "");
					array.add(list.getInTime() + "");
					array.add(list.getOutTime() + "");
					array.add(list.getInPart() + "");
					array.add(list.getOutPart() + "");
					array.add(list.getTimeLossOrExcess() + "");
					array.add(list.getDecision() + "");
					array.add(list.getTotalHours() + "");
					data.add(array);
				}
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String punchPrint() {
		try {
			AttendanceTO attendanceTO = new AttendanceTO();
			if (personId != null && personId > 0 && fromDate != null
					&& !fromDate.equals("")) {
				attendanceTO = attendanceBL.punchReport(personId,
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate));
			} else if (fromDate != null && !fromDate.equals("")) {
				attendanceTO = attendanceBL.punchReport(null,
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate));
				attendanceTO.setPersonName("-All-");
				attendanceTO.setCompanyName("-All-");
				attendanceTO.setDepartmentName("-All-");
				attendanceTO.setLocationName("-All-");
				attendanceTO.setDesignationName("-All-");
			}
			attendanceTO.setFromDate(fromDate);
			attendanceTO.setToDate(toDate);
			attendanceTO.setCreatedDateStr(DateFormat
					.convertSystemDateToString(new Date()));
			ServletActionContext.getRequest().setAttribute("ATTENDANCE_MASTER",
					attendanceTO);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String punchPDF() {
		try {
			AttendanceTO attendanceTO = new AttendanceTO();

			List<AttendanceTO> tempList = new ArrayList<AttendanceTO>();
			if (personId != null && personId > 0 && fromDate != null
					&& !fromDate.equals("")) {

				tempList.add(attendanceBL.punchReport(personId,
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate)));

			} /*
			 * else if (fromDate != null && !fromDate.equals("")) { attendanceTO
			 * = attendanceBL.punchReport(null,
			 * DateFormat.convertStringToDate(fromDate),
			 * DateFormat.convertStringToDate(toDate));
			 * attendanceTO.setPersonName("-All-");
			 * attendanceTO.setCompanyName("-All-");
			 * attendanceTO.setDepartmentName("-All-");
			 * attendanceTO.setLocationName("-All-");
			 * attendanceTO.setDesignationName("-All-"); }
			 */
			attendanceTO.setFromDate(fromDate);
			attendanceTO.setToDate(toDate);
			attendanceTO.setCreatedDateStr(DateFormat
					.convertSystemDateToString(new Date()));

			attendanceTOs = new ArrayList<AttendanceTO>();
			attendanceTO.setAttendanceHistoryList(tempList);

			attendanceTO.setLinkLabel("HTML".equals(getFormat()) ? "Download"
					: "");
			attendanceTO
					.setAnchorExpression("HTML".equals(getFormat()) ? "get_hr_employee_leave_history_pdf.action?format=PDF"
							: "#");

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			attendanceTOs.add(attendanceTO);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String punchCompanyPDF() {
		try {
			attendanceTOs = new ArrayList<AttendanceTO>();
			AttendanceTO attendanceTO = new AttendanceTO();
			if (fromDate != null && !fromDate.equals("")) {

				List<JobAssignment> jobs = attendanceBL
						.getJobAssignmentBL()
						.getJobAssignmentService()
						.getAllJobAssignmentsWhichHasAttendenceByCompanyId(
								companyId);

				List<AttendanceTO> tempList = new ArrayList<AttendanceTO>();
				for (JobAssignment jobAssignment : jobs) {
					tempList.add(attendanceBL.punchReport(jobAssignment
							.getPerson().getPersonId(), DateFormat
							.convertStringToDate(fromDate), DateFormat
							.convertStringToDate(toDate)));
				}
				attendanceTO.setAttendanceHistoryList(tempList);

			} else {

				attendanceTO.setAttendanceHistoryList(attendanceTOs);
			}
			attendanceTO.setFromDate(fromDate);
			attendanceTO.setToDate(toDate);
			attendanceTO.setCreatedDateStr(DateFormat
					.convertSystemDateToString(new Date()));

			attendanceTO.setLinkLabel("HTML".equals(getFormat()) ? "Download"
					: "");
			attendanceTO
					.setAnchorExpression("HTML".equals(getFormat()) ? "company_punch_report_pdf.action?format=PDF"
							: "#");

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			attendanceTOs.add(attendanceTO);
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		return SUCCESS;
	}

	public String punchXLS() {
		try {

			List<AttendanceTO> tempList = new ArrayList<AttendanceTO>();
			if (personId != null && personId > 0 && fromDate != null
					&& !fromDate.equals("")) {

				tempList.add(attendanceBL.punchReport(personId,
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate)));

			}

			InputStream is = new ByteArrayInputStream(prepateXLSData(tempList)
					.getBytes());
			fileInputStream = is;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		return SUCCESS;
	}

	public String punchCompanyXLS() {
		try {
			List<AttendanceTO> tempList = new ArrayList<AttendanceTO>();

			if (fromDate != null && !fromDate.equals("")) {

				List<JobAssignment> jobs = attendanceBL
						.getJobAssignmentBL()
						.getJobAssignmentService()
						.getAllJobAssignmentsWhichHasAttendenceByCompanyId(
								companyId);

				for (JobAssignment jobAssignment : jobs) {
					tempList.add(attendanceBL.punchReport(jobAssignment
							.getPerson().getPersonId(), DateFormat
							.convertStringToDate(fromDate), DateFormat
							.convertStringToDate(toDate)));
				}

			}
			InputStream is = new ByteArrayInputStream(prepateXLSData(tempList)
					.getBytes());
			fileInputStream = is;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		return SUCCESS;
	}

	private String prepateXLSData(List<AttendanceTO> list) throws Exception {

		String str = "";
		str = "Punch Report\n\n";
		for (AttendanceTO to : list) {

			str += "Name," + to.getPersonName();
			str += "\nCompany," + to.getCompanyName() + "\nDepartment,"
					+ to.getDepartmentName() + "\nLoccation,"
					+ to.getLocationName() + "\n\nPunch Swipe Information\n";

			str += "Attendance Date,Time In,Time Out,In (Late/Early),Out (Late/Early),Time Loss/Excess,Decision,Total Hours";
			for (AttendanceTO leaveProcessTO : to.getAttendanceHistoryList()) {

				str += "\n";
				str += leaveProcessTO.getAttedanceDate() + ","
						+ leaveProcessTO.getInTime() + ","
						+ leaveProcessTO.getOutTime() + ","
						+ leaveProcessTO.getInPart() + ","
						+ leaveProcessTO.getOutPart() + ","
						+ leaveProcessTO.getTimeLossOrExcess() + ","
						+ leaveProcessTO.getDecision() + ","
						+ leaveProcessTO.getTotalHours();

			}
			str += "\n\n";

		}

		return str;
	}

	public String punchXLSDownload() {

		return SUCCESS;
	}

	public String punchPDFDownload() {

		return SUCCESS;
	}

	public String getAttendanceCompanyPunchReport() {
		try {

			List<Company> companys = attendanceBL.getCompanyService()
					.getOnlyCompanies(getImplementation());
			ServletActionContext.getRequest().setAttribute("COMPANY_LIST",
					companys);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String attendanceCompanyPunchJson() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			AttendanceTO attendanceTO = new AttendanceTO();
			JSONObject jsonResponse = new JSONObject();
			if (fromDate != null && !fromDate.equals("")) {
				attendanceTO = attendanceBL.companyPunchReport(companyId,
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate));
			} else {
				List<AttendanceTO> attendanceTOs = new ArrayList<AttendanceTO>();
				attendanceTO.setAttendanceHistoryList(attendanceTOs);
			}
			if (attendanceTO.getAttendanceHistoryList() != null
					&& attendanceTO.getAttendanceHistoryList().size() > 0) {
				jsonResponse.put("iTotalRecords", attendanceTO
						.getAttendanceHistoryList().size());
				jsonResponse.put("iTotalDisplayRecords", attendanceTO
						.getAttendanceHistoryList().size());
			}
			JSONArray data = new JSONArray();
			if (attendanceTO.getAttendanceHistoryList() != null)
				for (AttendanceTO list : attendanceTO
						.getAttendanceHistoryList()) {
					JSONArray array = new JSONArray();
					array.add(list.getAttendanceId() + "");
					array.add(list.getPersonName() + "");
					array.add(list.getDesignationName() + "");
					array.add(list.getCompanyName() + " >> "
							+ list.getDepartmentName() + "  >> "
							+ list.getLocationName());
					array.add(list.getAttedanceDate() + "");
					array.add(list.getInTime() + "");
					array.add(list.getOutTime() + "");
					array.add(list.getInPart() + "");
					array.add(list.getOutPart() + "");
					array.add(list.getTimeLossOrExcess() + "");
					array.add(list.getDecision() + "");
					array.add(list.getTotalHours() + "");
					data.add(array);
				}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String companyPunchPrint() {
		try {
			AttendanceTO attendanceTO = new AttendanceTO();
			if (fromDate != null && !fromDate.equals("")) {

				List<JobAssignment> jobs = attendanceBL
						.getJobAssignmentBL()
						.getJobAssignmentService()
						.getAllJobAssignmentsWhichHasAttendenceByCompanyId(
								companyId);

				List<AttendanceTO> tempList = new ArrayList<AttendanceTO>();
				for (JobAssignment jobAssignment : jobs) {
					tempList.add(attendanceBL.punchReport(jobAssignment
							.getPerson().getPersonId(), DateFormat
							.convertStringToDate(fromDate), DateFormat
							.convertStringToDate(toDate)));
				}
				attendanceTO.setAttendanceHistoryList(tempList);

			} else {
				List<AttendanceTO> attendanceTOs = new ArrayList<AttendanceTO>();
				attendanceTO.setAttendanceHistoryList(attendanceTOs);
			}
			attendanceTO.setFromDate(fromDate);
			attendanceTO.setToDate(toDate);
			attendanceTO.setCreatedDateStr(DateFormat
					.convertSystemDateToString(new Date()));
			ServletActionContext.getRequest().setAttribute("ATTENDANCE_MASTER",
					attendanceTO);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getAllProcessedAttendanceForDecision() {
		try {
			AttendanceTO attendanceTO = new AttendanceTO();
			List<AttendanceTO> tempList = null;
			if (fromDate != null && !fromDate.equals("")) {
				Map<Long, Long> jobAssignmentIdMap = null;
				if (persons != null && !persons.equals("")) {
					jobAssignmentIdMap = new HashMap<Long, Long>();
					String temps[] = persons.split(",");
					for (String personId : temps) {
						jobAssignmentIdMap.put(Long.valueOf(personId),
								Long.valueOf(personId));
					}

				}
				List<JobAssignment> jobs = attendanceBL.getJobAssignmentBL()
						.getJobAssignmentService()
						.getAllJobAssignment(getImplementation());

				tempList = new ArrayList<AttendanceTO>();
				for (JobAssignment jobAssignment : jobs) {
					if (jobAssignmentIdMap != null
							&& !jobAssignmentIdMap.containsKey(jobAssignment
									.getJobAssignmentId()))
						continue;
					
					AttendanceTO to = attendanceBL.punchReport(jobAssignment
							.getPerson().getPersonId(), DateFormat
							.convertStringToDate(fromDate), DateFormat
							.convertStringToDate(toDate));
					if (to != null && to.getAttendanceHistoryList() != null) {
						for (AttendanceTO attTo : to.getAttendanceHistoryList()) {
							tempList.add(attTo);

						}
					}
				}

			}
			attendanceTO.setFromDate(fromDate);
			attendanceTO.setToDate(toDate);
			attendanceTO.setCreatedDateStr(DateFormat
					.convertSystemDateToString(new Date()));
			ServletActionContext.getRequest().setAttribute("ATTENDANCE_MASTER",
					attendanceTO);
			ServletActionContext.getRequest().setAttribute("ATTENDNACE_LIST",
					tempList);
			Map<Integer, String> calculationMethods = attendanceBL
					.calculationMethods();
			ServletActionContext.getRequest().setAttribute(
					"CALCULATION_METHOD", calculationMethods);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String updateInOut() {
		try {
			if (fromDate != null && !fromDate.equals("")) {
				Map<Long, Long> jobAssignmentIdMap = null;
				if (persons != null && !persons.equals("")) {
					jobAssignmentIdMap = new HashMap<Long, Long>();
					String temps[] = persons.split(",");
					for (String personId : temps) {
						jobAssignmentIdMap.put(Long.valueOf(personId),
								Long.valueOf(personId));
					}

				}
				List<JobAssignment> jobs = attendanceBL.getJobAssignmentBL()
						.getJobAssignmentService()
						.getAllJobAssignment(getImplementation());

				for (JobAssignment jobAssignment : jobs) {
					if (jobAssignmentIdMap != null
							&& !jobAssignmentIdMap.containsKey(jobAssignment
									.getJobAssignmentId()))
						continue;
					
					attendanceBL.updateSwipeInOut(jobAssignment, DateFormat
							.convertStringToDate(fromDate), DateFormat
							.convertStringToDate(toDate));
					
				}

			}
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			returnMessage = "ERROR";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			e.printStackTrace();
			return ERROR;
		}
	}
	
	// Consolidated Punch Information
	public String getAttendanceConsolidatedReport() {
		try {

			List<Company> companys = attendanceBL.getCompanyService()
					.getOnlyCompanies(getImplementation());
			ServletActionContext.getRequest().setAttribute("COMPANY_LIST",
					companys);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String attendanceConsolidatedJson() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			AttendanceTO attendanceTO = new AttendanceTO();
			JSONObject jsonResponse = new JSONObject();
			if (fromDate != null && !fromDate.equals("")) {
				attendanceTO = attendanceBL.consolidatedPunchReport(companyId,
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate));
			} else {
				List<AttendanceTO> attendanceTOs = new ArrayList<AttendanceTO>();
				attendanceTO.setAttendanceHistoryList(attendanceTOs);
			}
			if (attendanceTO.getAttendanceHistoryList() != null
					&& attendanceTO.getAttendanceHistoryList().size() > 0) {
				jsonResponse.put("iTotalRecords", attendanceTO
						.getAttendanceHistoryList().size());
				jsonResponse.put("iTotalDisplayRecords", attendanceTO
						.getAttendanceHistoryList().size());
			}
			JSONArray data = new JSONArray();
			if (attendanceTO.getAttendanceHistoryList() != null)
				for (AttendanceTO list : attendanceTO
						.getAttendanceHistoryList()) {
					JSONArray array = new JSONArray();
					array.add(list.getAttendanceId() + "");
					array.add(list.getPersonName() + "");
					array.add(list.getDesignationName() + "");
					array.add(list.getCompanyName() + " >> "
							+ list.getDepartmentName() + "  >> "
							+ list.getLocationName());
					array.add(list.getLateInDays() + "");
					array.add(list.getLateOutDays() + "");
					array.add(list.getEarlyInDays() + "");
					array.add(list.getEarlyOutDays() + "");
					array.add(list.getTimeLossDyas() + "");
					array.add(list.getTimeExcessDays() + "");
					array.add(list.getAbsentDays() + "");
					array.add(Math.round(Double.parseDouble(list
							.getTotalHours())) + "");
					data.add(array);
				}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String consolidatedPrint() {
		try {
			AttendanceTO attendanceTO = new AttendanceTO();
			if (fromDate != null && !fromDate.equals("")) {
				attendanceTO = attendanceBL.consolidatedPunchReport(companyId,
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate));
			} else {
				List<AttendanceTO> attendanceTOs = new ArrayList<AttendanceTO>();
				attendanceTO.setAttendanceHistoryList(attendanceTOs);
			}
			attendanceTO.setFromDate(fromDate);
			attendanceTO.setToDate(toDate);
			attendanceTO.setCreatedDateStr(DateFormat
					.convertSystemDateToString(new Date()));
			ServletActionContext.getRequest().setAttribute("ATTENDANCE_MASTER",
					attendanceTO);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String consolidatedPDF() {
		try {
			AttendanceTO attendanceTO = new AttendanceTO();
			/* List<AttendanceTO> tempList = new ArrayList<AttendanceTO>(); */
			if (fromDate != null && !fromDate.equals("")) {
				attendanceTO = attendanceBL.consolidatedPunchReport(companyId,
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate));
			} /*
			 * else { List<AttendanceTO> attendanceTOs = new
			 * ArrayList<AttendanceTO>();
			 * attendanceTO.setAttendanceHistoryList(attendanceTOs); }
			 */
			attendanceTO.setFromDate(fromDate);
			attendanceTO.setToDate(toDate);
			attendanceTO.setCreatedDateStr(DateFormat
					.convertSystemDateToString(new Date()));

			attendanceTO.setLinkLabel("HTML".equals(getFormat()) ? "Download"
					: "");
			attendanceTO
					.setAnchorExpression("HTML".equals(getFormat()) ? "get_hr_employee_leave_history_pdf.action?format=PDF"
							: "#");

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			attendanceTOs = new ArrayList<AttendanceTO>();

			attendanceTOs.add(attendanceTO);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String consolidiatedPunchXLS() {
		try {
			AttendanceTO attendanceTO = new AttendanceTO();

			String str = "";
			if (fromDate != null && !fromDate.equals("")) {
				attendanceTO = attendanceBL.consolidatedPunchReport(companyId,
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate));

				str += "Employee,Designation,Location,Late In Day(s),Late Out Day(s),Early In Day(s),Early Out Day(s),Time Loss Day(s), Time Excess Day(s), Absent Day(s), Total Hours";
				for (AttendanceTO leaveProcessTO : attendanceTO
						.getAttendanceHistoryList()) {

					str += "\n";
					str += leaveProcessTO.getPersonName() + ","
							+ leaveProcessTO.getDesignationName() + ","
							+ leaveProcessTO.getCompanyName() + ">>"
							+ leaveProcessTO.getDepartmentName() + ">>"
							+ leaveProcessTO.getLocationName() + ","
							+ leaveProcessTO.getLateInDays() + ","
							+ leaveProcessTO.getLateOutDays() + ","
							+ leaveProcessTO.getEarlyInDays() + ","
							+ leaveProcessTO.getEarlyOutDays() + ","
							+ leaveProcessTO.getTimeLossDyas() + ","
							+ leaveProcessTO.getTimeExcessDays() + ","
							+ leaveProcessTO.getAbsentDays() + ","
							+ leaveProcessTO.getTotalHours();

				}
				str += "\n\n";

			}
			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Attendance Upload
	private File fileUpload;
	private String fileUploadContentType;
	private String fileUploadFileName;

	public String getFileUploadContentType() {
		return fileUploadContentType;
	}

	public void setFileUploadContentType(String fileUploadContentType) {
		this.fileUploadContentType = fileUploadContentType;
	}

	public String getFileUploadFileName() {
		return fileUploadFileName;
	}

	public void setFileUploadFileName(String fileUploadFileName) {
		this.fileUploadFileName = fileUploadFileName;
	}

	public File getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(File fileUpload) {
		this.fileUpload = fileUpload;
	}

	public String execute() throws Exception {
		try {

			boolean returnFlag = attendanceBL.attedanceReader(fileUpload,
					fileUploadFileName);
			if (returnFlag)
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "SUCCESS");

		} catch (Exception e) {
			e.printStackTrace();

			fileUpload = null;
		}
		return SUCCESS;

	}

	public String display() {

		return NONE;
	}

	// Getter Setter
	public AttendanceBL getAttendanceBL() {
		return attendanceBL;
	}

	public void setAttendanceBL(AttendanceBL attendanceBL) {
		this.attendanceBL = attendanceBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getAttendanceId() {
		return attendanceId;
	}

	public void setAttendanceId(Long attendanceId) {
		this.attendanceId = attendanceId;
	}

	public boolean isHalfDay() {
		return halfDay;
	}

	public void setHalfDay(boolean halfDay) {
		this.halfDay = halfDay;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Long getDeductionType() {
		return deductionType;
	}

	public void setDeductionType(Long deductionType) {
		this.deductionType = deductionType;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAttendanceDate() {
		return attendanceDate;
	}

	public void setAttendanceDate(String attendanceDate) {
		this.attendanceDate = attendanceDate;
	}

	public String getSwipeLineDetail() {
		return swipeLineDetail;
	}

	public void setSwipeLineDetail(String swipeLineDetail) {
		this.swipeLineDetail = swipeLineDetail;
	}

	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}

	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public List<AttendanceTO> getAttendanceTOs() {
		return attendanceTOs;
	}

	public void setAttendanceTOs(List<AttendanceTO> attendanceTOs) {
		this.attendanceTOs = attendanceTOs;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Map<String, Object> getJasperRptParams() {
		return jasperRptParams;
	}

	public void setJasperRptParams(Map<String, Object> jasperRptParams) {
		this.jasperRptParams = jasperRptParams;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public Integer getCalcultionMethod() {
		return calcultionMethod;
	}

	public void setCalcultionMethod(Integer calcultionMethod) {
		this.calcultionMethod = calcultionMethod;
	}

	public boolean absent() {
		return absent;
	}

	public void setAbsent(boolean absent) {
		this.absent = absent;
	}

	public String getPersons() {
		return persons;
	}

	public void setPersons(String persons) {
		this.persons = persons;
	}

}
