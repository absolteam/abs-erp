package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.Candidate;
import com.aiotech.aios.hr.domain.entity.CandidateOffer;
import com.aiotech.aios.hr.domain.entity.EosPolicy;
import com.aiotech.aios.hr.domain.entity.InterviewProcess;
import com.aiotech.aios.hr.domain.entity.PayrollElement;
import com.aiotech.aios.hr.domain.entity.vo.CandidateVO;
import com.aiotech.aios.hr.domain.entity.vo.EosPolicyVO;
import com.aiotech.aios.hr.domain.entity.vo.InterviewVO;
import com.aiotech.aios.hr.service.bl.PayrollElementBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class PayrollElementAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(PayrollElementAction.class);
	private PayrollElementBL payrollElementBL;
	private Implementation implementation;

	private Long payrollElementId;
	private String elementCode;
	private String elementName;
	private String elementNature;
	private String elementType;
	private Long combinationId;
	private Long reverseElementId;
	private String elementNameArabic;

	private Integer id;
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private String returnMessage;
	private List<Object> aaData;
	private Long recordId;

	public String returnSuccess() {

		return SUCCESS;
	}

	// Listing JSON
	public String getPayrollElementJson() {

		try {
			List<PayrollElement> payelements = payrollElementBL
					.getPayrollElementService().getPayrollElementList(
							getImplementation());
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", payelements.size());
			jsonResponse.put("iTotalDisplayRecords", payelements.size());
			JSONArray data = new JSONArray();
			for (PayrollElement list : payelements) {

				JSONArray array = new JSONArray();
				array.add(list.getPayrollElementId() + "");
				array.add(list.getElementCode() + "");
				array.add(list.getElementName());
				array.add(list.getElementNature() + "");
				array.add(list.getElementType() + "");
				if (list.getPayrollElement() != null)
					array.add(list.getPayrollElement().getElementName());
				else
					array.add("-NA-");

				Combination elementCombination = null;
				if (list.getCombination() != null
						&& list.getCombination().getCombinationId() != null) {
					elementCombination = payrollElementBL
							.getCombinationService()
							.getCombinationAllAccountDetail(
									list.getCombination().getCombinationId());
				}
				String temp = "";
				if (elementCombination != null
						&& elementCombination.getAccountByAnalysisAccountId() != null) {
					temp = elementCombination.getAccountByAnalysisAccountId()
							.getCode()
							+ "["
							+ elementCombination.getAccountByCompanyAccountId()
									.getAccount()
							+ "."
							+ elementCombination
									.getAccountByCostcenterAccountId()
									.getAccount()
							+ "."
							+ elementCombination.getAccountByNaturalAccountId()
									.getAccount()
							+ "."
							+ elementCombination
									.getAccountByAnalysisAccountId().getCode()
							+ "]";
					array.add(temp + "");

				} else if (elementCombination != null
						&& elementCombination.getAccountByNaturalAccountId() != null) {
					temp = elementCombination.getAccountByNaturalAccountId()
							.getCode()
							+ "["
							+ elementCombination.getAccountByCompanyAccountId()
									.getAccount()
							+ "."
							+ elementCombination
									.getAccountByCostcenterAccountId()
									.getAccount()
							+ "."
							+ elementCombination.getAccountByNaturalAccountId()
									.getAccount() + "]";
					array.add(temp + "");
				} else {
					array.add("--Not Assigned--");
				}
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getPayrollElement() {

		try {
			PayrollElement payrollElement = null;
			String accountName = "";
			Long accountId = null;
			if (recordId != null && recordId > 0) {
				payrollElementId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (payrollElementId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(AttendancePolicy.class.getName());
					comment = payrollElementBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			String elementNameArabic = "";
			if (payrollElementId != null && payrollElementId > 0) {
				payrollElement = payrollElementBL.getPayrollElementService()
						.getPayrollElementInfo(payrollElementId);
				Combination elementCombination = null;
				if (payrollElement.getCombination() != null
						&& payrollElement.getCombination().getCombinationId() != null) {
					elementCombination = payrollElementBL
							.getCombinationService()
							.getCombinationAllAccountDetail(
									payrollElement.getCombination()
											.getCombinationId());
				}
				if (elementCombination != null
						&& elementCombination.getAccountByAnalysisAccountId() != null) {
					accountName = elementCombination
							.getAccountByAnalysisAccountId().getCode()
							+ "["
							+ elementCombination.getAccountByCompanyAccountId()
									.getAccount()
							+ "."
							+ elementCombination
									.getAccountByCostcenterAccountId()
									.getAccount()
							+ "."
							+ elementCombination.getAccountByNaturalAccountId()
									.getAccount()
							+ "."
							+ elementCombination
									.getAccountByAnalysisAccountId().getCode()
							+ "]";
					accountId = elementCombination.getCombinationId();

				} else if (elementCombination != null
						&& elementCombination.getAccountByNaturalAccountId() != null) {
					accountName = elementCombination
							.getAccountByNaturalAccountId().getCode()
							+ "["
							+ elementCombination.getAccountByCompanyAccountId()
									.getAccount()
							+ "."
							+ elementCombination
									.getAccountByCostcenterAccountId()
									.getAccount()
							+ "."
							+ elementCombination.getAccountByNaturalAccountId()
									.getAccount() + "]";
					accountId = elementCombination.getCombinationId();
				} else {
					accountName = "--Not Assigned--";
				}
				elementNameArabic = AIOSCommons.bytesToUTFString(payrollElement
						.getElementNameArabic());
			}
			List<PayrollElement> payelements = payrollElementBL
					.getPayrollElementService().getPayrollElementList(
							getImplementation());
			ServletActionContext.getRequest().setAttribute("PAYROLL_ELEMENT",
					payrollElement);
			ServletActionContext.getRequest().setAttribute("elementNameArabic",
					elementNameArabic);
			ServletActionContext.getRequest().setAttribute("COMBINATION_NAME",
					accountName);
			ServletActionContext.getRequest().setAttribute("COMBINATION_ID",
					accountId);
			ServletActionContext.getRequest().setAttribute("REVERSE_ELEMENT",
					payelements);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String savePayrollElement() {
		try {

			PayrollElement payrollElement = new PayrollElement();
			if (payrollElementId != null && payrollElementId > 0) {
				payrollElement = payrollElementBL.getPayrollElementService()
						.getPayrollElementInfo(payrollElementId);
				Combination combination = new Combination();
				combination.setCombinationId(combinationId);
				payrollElement.setCombination(combination);
			}
			if (reverseElementId != null && reverseElementId > 0) {
				PayrollElement reversePayrollElement = new PayrollElement();
				reversePayrollElement.setPayrollElementId(reverseElementId);
				payrollElement.setPayrollElement(reversePayrollElement);
			}
			payrollElement.setElementName(elementName);
			payrollElement.setElementNameArabic(AIOSCommons
					.stringToUTFBytes(elementNameArabic));
			payrollElementBL.savePayrollElement(payrollElement);

			returnMessage = "SUCCESS";

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			returnMessage = "Update Failure";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return ERROR;
		}
	}

	public String deletePayrollElement() {
		try {
			PayrollElement payrollElement = new PayrollElement();
			if (payrollElementId != null && payrollElementId > 0) {
				payrollElement = payrollElementBL.getPayrollElementService()
						.getPayrollElementInfo(payrollElementId);
				payrollElementBL.getPayrollElementService()
						.payrollElementDelete(payrollElement);
			}
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			returnMessage = "Can't Delete Information is in use already";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return ERROR;
		}
	}

	public String getEOSPolicyList() {
		try {

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			return ERROR;
		}
	}

	public String getEOSPolicyListJson() {
		try {

			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();
			List<EosPolicy> eosPolicies = payrollElementBL
					.getPayrollElementService().getAllEosPolicy();

			if (eosPolicies != null && eosPolicies.size() > 0) {
				List<EosPolicyVO> eosPoliciesVOs = new ArrayList<EosPolicyVO>();

				EosPolicyVO eosPolicyVO = null;
				for (EosPolicy eosPolicy : eosPolicies) {
					eosPolicyVO = new EosPolicyVO();
					eosPolicyVO.setEosPolicyId(eosPolicy.getEosPolicyId());
					eosPolicyVO.setServiceYears(eosPolicy.getServiceYears());
					eosPolicyVO.setPayDays(eosPolicy.getPayDays());
					eosPolicyVO.setPolicyName(eosPolicy.getPolicyName());
					eosPolicyVO.setPayElements(eosPolicy.getPayElements());
					eosPolicyVO.setType(eosPolicy.getType());
					if (eosPolicyVO.getType() != null
							&& (byte) eosPolicy.getType() == (byte) 1) {
						eosPolicyVO.setTypeName("Resignation");
					} else {
						eosPolicyVO.setTypeName("Termination");
					}
					eosPoliciesVOs.add(eosPolicyVO);

				}

				aaData.addAll(eosPoliciesVOs);
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			return ERROR;
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public PayrollElementBL getPayrollElementBL() {
		return payrollElementBL;
	}

	public void setPayrollElementBL(PayrollElementBL payrollElementBL) {
		this.payrollElementBL = payrollElementBL;
	}

	public Long getPayrollElementId() {
		return payrollElementId;
	}

	public void setPayrollElementId(Long payrollElementId) {
		this.payrollElementId = payrollElementId;
	}

	public String getElementCode() {
		return elementCode;
	}

	public void setElementCode(String elementCode) {
		this.elementCode = elementCode;
	}

	public String getElementName() {
		return elementName;
	}

	public void setElementName(String elementName) {
		this.elementName = elementName;
	}

	public Long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(Long combinationId) {
		this.combinationId = combinationId;
	}

	public String getElementNature() {
		return elementNature;
	}

	public void setElementNature(String elementNature) {
		this.elementNature = elementNature;
	}

	public String getElementType() {
		return elementType;
	}

	public void setElementType(String elementType) {
		this.elementType = elementType;
	}

	public Long getReverseElementId() {
		return reverseElementId;
	}

	public void setReverseElementId(Long reverseElementId) {
		this.reverseElementId = reverseElementId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getElementNameArabic() {
		return elementNameArabic;
	}

	public void setElementNameArabic(String elementNameArabic) {
		this.elementNameArabic = elementNameArabic;
	}
}
