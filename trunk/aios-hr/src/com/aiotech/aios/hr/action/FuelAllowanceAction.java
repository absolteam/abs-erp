package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.FuelAllowance;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobPayrollElement;
import com.aiotech.aios.hr.domain.entity.MileageLog;
import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.FuelAllowanceVO;
import com.aiotech.aios.hr.domain.entity.vo.JobPayrollElementVO;
import com.aiotech.aios.hr.domain.entity.vo.PayPeriodVO;
import com.aiotech.aios.hr.service.bl.FuelAllowanceBL;
import com.aiotech.aios.hr.service.bl.PayrollBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class FuelAllowanceAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(FuelAllowanceAction.class);
	private Implementation implementation;
	private FuelAllowanceBL fuelAllowanceBL;
	private PayrollBL payrollBL;
	private String transactionDate;
	private Integer id;
	private Long jobAssignmentId;
	private Long jobPayrollElementId;
	private List<Object> aaData;
	private Byte payPolicy;
	private String fromDate;
	private String toDate;
	private String receiptNumber;
	private String isFinanceImpact;
	private Double amount;
	private String description;
	private Long payPeriodTransactionId;

	private Long fuelAllowanceId;
	private Long cardType;
	private Long fuelCompany;
	private Long vehicleNumber;
	private String cardNumber;

	private Long mileageLogId;
	private Long source;
	private Long destination;
	private Long personId;
	private String date;
	private String time;
	private Double start;
	private Double end;
	private String purpose;
	private Long recordId;

	public String returnSuccess() {
		ServletActionContext.getRequest().setAttribute("jobAssignmentId",
				jobAssignmentId);
		ServletActionContext.getRequest().setAttribute("jobPayrollElementId",
				jobPayrollElementId);
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String getFuelAllowanceList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();

			// Find the pay period of current transaction
			PayPeriodVO payPeriodVO = new PayPeriodVO();
			payPeriodVO.setJobAssignmentId(jobAssignmentId);
			payPeriodVO.setTransactionDate(DateFormat
					.convertStringToDate(transactionDate));
			payPeriodVO.setPayPolicy(payPolicy);
			payPeriodVO.setPayPeriodTransactionId(payPeriodTransactionId);
			
			PayPeriodTransaction payPeriodTransaction = fuelAllowanceBL
					.getPayPeriodBL().findPayPeriod(payPeriodVO);

			FuelAllowanceVO allowanceVO = new FuelAllowanceVO();
			if (payPeriodTransaction != null)
				allowanceVO.setPayPeriodTransaction(payPeriodTransaction);

			allowanceVO.setJobAssignmentId(jobAssignmentId);
			allowanceVO.setJobPayrollElementId(jobPayrollElementId);

			List<FuelAllowance> allowances = fuelAllowanceBL
					.getFuelAllowanceService().getAllFuelAllowanceByCriteria(
							allowanceVO);

			if (allowances != null && allowances.size() > 0) {
				FuelAllowanceVO vO = null;
				for (FuelAllowance list : allowances) {
					vO = new FuelAllowanceVO();
					vO.setFuelAllowanceId(list.getFuelAllowanceId());

					if (list.getLookupDetailByCardType() != null)
						vO.setCardType(list.getLookupDetailByCardType()
								.getDisplayName());

					if (list.getLookupDetailByFuelCompany() != null)
						vO.setFuelCompany(list.getLookupDetailByFuelCompany()
								.getDisplayName());

					if (list.getLookupDetailByVehicleNumber() != null)
						vO.setVehicleNumber(list
								.getLookupDetailByVehicleNumber()
								.getDisplayName());
					vO.setCardNumber(list.getCardNumber());
					vO.setFromDateView(DateFormat.convertDateToString(list
							.getFromDate().toString()));
					vO.setToDateView(DateFormat.convertDateToString(list
							.getToDate().toString()));
					vO.setAmount(list.getAmount());
					if (list.getIsFinanceImpact() != null
							&& list.getIsFinanceImpact() == true)
						vO.setIsFinanceView("YES");
					else
						vO.setIsFinanceView("NO");

					aaData.add(vO);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String getMileageLogList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();

			List<MileageLog> mileages = fuelAllowanceBL
					.getFuelAllowanceService().getAllMileageLog(
							getImplementation());

			if (mileages != null && mileages.size() > 0) {
				FuelAllowanceVO allowanceVO = null;
				for (MileageLog mileageLog : mileages) {
					allowanceVO = new FuelAllowanceVO();
					allowanceVO.setMileageLogId(mileageLog.getMileageLogId());
					if (mileageLog.getLookupDetailByVehicleNumber() != null)
						allowanceVO.setVehicleNumber(mileageLog
								.getLookupDetailByVehicleNumber()
								.getDisplayName());

					if (mileageLog.getLookupDetailBySource() != null)
						allowanceVO.setSource(mileageLog
								.getLookupDetailBySource().getDisplayName());

					if (mileageLog.getLookupDetailByDestination() != null)
						allowanceVO.setDestination(mileageLog
								.getLookupDetailByDestination().getDisplayName());

					allowanceVO.setDriver(mileageLog.getJobAssignment().getPerson()
							.getFirstName()
							+ " "
							+ mileageLog.getJobAssignment().getPerson().getLastName());
					allowanceVO.setStart(mileageLog.getStart().toString());
					allowanceVO.setEnd(mileageLog.getEnd().toString());
					allowanceVO.setMileage((mileageLog
							.getEnd() -mileageLog.getStart()) + "");

					allowanceVO.setDate(DateFormat
							.convertDateToString(mileageLog.getDate()
									.toString()));

					allowanceVO.setTime(DateFormat
							.convertDateToString(mileageLog.getDate()
									.toString())
							+ " "
							+ DateFormat.convertTimeToString(mileageLog
									.getTime().toString()));
					allowanceVO.setPurpose(mileageLog.getPurpose());
					aaData.add(allowanceVO);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getFuelAllowance() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			FuelAllowanceVO allowanceVO = null;
			if (fuelAllowanceId != null && fuelAllowanceId > 0) {
				FuelAllowance allowance = fuelAllowanceBL
						.getFuelAllowanceService().getFuelAllowanceById(
								fuelAllowanceId);
				if (allowance != null)
					allowanceVO = fuelAllowanceBL.convertEntityToVO(allowance);
			}

			// Find the Maximum provided & actual earned amount
			{
				// Find the pay period of current transaction
				JobPayrollElementVO jobPayrollElementVO = null;
				PayPeriodVO payPeriodVO = new PayPeriodVO();
				payPeriodVO.setJobAssignmentId(jobAssignmentId);
				payPeriodVO.setTransactionDate(DateFormat
						.convertStringToDate(transactionDate));
				payPeriodVO.setPayPolicy(payPolicy);
				payPeriodVO.setPayPeriodTransactionId(payPeriodTransactionId);

				PayPeriodTransaction payPeriodTransaction = fuelAllowanceBL
						.getPayPeriodBL().findPayPeriod(payPeriodVO);
				FuelAllowanceVO allowanceTempVO = new FuelAllowanceVO();
				if (payPeriodTransaction != null)
					allowanceTempVO
							.setPayPeriodTransaction(payPeriodTransaction);

				allowanceTempVO.setJobAssignmentId(jobAssignmentId);
				allowanceTempVO.setJobPayrollElementId(jobPayrollElementId);

				List<FuelAllowance> allowances = fuelAllowanceBL
						.getFuelAllowanceService()
						.getAllFuelAllowanceByCriteria(allowanceTempVO);

				jobPayrollElementVO = payrollBL.getJobPayrollInfomration(
						jobPayrollElementId, new ArrayList<Object>(allowances),
						payPeriodTransaction);

				ServletActionContext.getRequest().setAttribute("JOBPAY_INFO",
						jobPayrollElementVO);
			}

			List<LookupDetail> vechicleNumber = fuelAllowanceBL
					.getLookupMasterBL().getActiveLookupDetails(
							"VEHICLE_NUMBER", true);

			List<LookupDetail> fuelCompany = fuelAllowanceBL
					.getLookupMasterBL().getActiveLookupDetails("FUEL_COMPANY",
							true);

			List<LookupDetail> cardType = fuelAllowanceBL.getLookupMasterBL()
					.getActiveLookupDetails("CARD_TYPE", true);

			ServletActionContext.getRequest().setAttribute("VEHICLE_NUMBER",
					vechicleNumber);
			ServletActionContext.getRequest().setAttribute("FUEL_COMPANY",
					fuelCompany);

			ServletActionContext.getRequest().setAttribute("CARD_TYPE",
					cardType);
			ServletActionContext.getRequest().setAttribute("ALLOWANCE",
					allowanceVO);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Add row
	public String allowanceAddRow() {
		try {

			if (null == id)
				id = 0;
			ServletActionContext.getRequest().setAttribute("rowId", id);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String saveFuelAllowance() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		String returnMessage = "SUCCESS";
		try {

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			FuelAllowance allowance = new FuelAllowance();
			if (fuelAllowanceId != null && fuelAllowanceId > 0) {
				allowance = fuelAllowanceBL.getFuelAllowanceService()
						.getFuelAllowanceById(fuelAllowanceId);

			} else {
				allowance.setCreatedDate(new Date());
				allowance.setPerson(person);
			}
			allowance.setAmount(amount);
			if (fromDate != null && !fromDate.equals(""))
				allowance.setFromDate(DateFormat.convertStringToDate(fromDate));
			if (toDate != null && !toDate.equals(""))
				allowance.setToDate(DateFormat.convertStringToDate(toDate));
			allowance.setImplementation(getImplementation());

			if (isFinanceImpact != null
					&& isFinanceImpact.equalsIgnoreCase("true"))
				allowance.setIsFinanceImpact(true);
			else
				allowance.setIsFinanceImpact(false);
			if (jobAssignmentId != null) {
				JobAssignment jobAssignment = new JobAssignment();
				jobAssignment.setJobAssignmentId(jobAssignmentId);
				allowance.setJobAssignment(jobAssignment);
			}
			if (jobPayrollElementId != null) {
				JobPayrollElement jobPayrollElement = new JobPayrollElement();
				jobPayrollElement.setJobPayrollElementId(jobPayrollElementId);
				allowance.setJobPayrollElement(jobPayrollElement);
			}
			if (vehicleNumber != null && vehicleNumber > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(vehicleNumber);
				allowance.setLookupDetailByVehicleNumber(lookupDetail);
			}
			allowance.setReceiptNumber(receiptNumber);

			if (fuelCompany != null && fuelCompany > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(fuelCompany);
				allowance.setLookupDetailByFuelCompany(lookupDetail);
			}
			if (cardType != null && cardType > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(cardType);
				allowance.setLookupDetailByCardType(lookupDetail);
			}
			if (payPeriodTransactionId != null) {
				PayPeriodTransaction payPeriodTransaction = new PayPeriodTransaction();
				payPeriodTransaction.setPayPeriodTransactionId(payPeriodTransactionId);
				allowance.setPayPeriodTransaction(payPeriodTransaction);
			}
			allowance.setCardNumber(cardNumber);
			allowance.setDescription(description);

			returnMessage = fuelAllowanceBL.saveFuelAllowance(allowance);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("saveFuelAllowance() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (ArithmeticException ae) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Failure in creating the Allowance");
			return ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			if (fuelAllowanceId != null && fuelAllowanceId > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveFuelAllowance() Info unsuccessful Return");
			return ERROR;
		} finally {

		}
	}

	public String saveMileageLog() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		String returnMessage = "SUCCESS";
		try {

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			MileageLog mileageLog = new MileageLog();
			if (mileageLogId != null && mileageLogId > 0) {
				mileageLog = fuelAllowanceBL.getFuelAllowanceService()
						.getMileageLogById(mileageLogId);

			} else {
				mileageLog.setCreatedDate(new Date());
				mileageLog.setPersonByCreatedBy(person);
			}
			if (date != null && !date.equals(""))
				mileageLog.setDate(DateFormat.convertStringToDate(date));
			if (time != null && !time.equals(""))
				mileageLog.setTime(DateFormat.convertStringToTime(time));

			mileageLog.setImplementation(getImplementation());

			if (fuelAllowanceId != null) {
				FuelAllowance fuelAllowance = new FuelAllowance();
				fuelAllowance.setFuelAllowanceId(fuelAllowanceId);
				mileageLog.setFuelAllowance(fuelAllowance);
			}

			if (jobAssignmentId != null) {
				JobAssignment personTemp = new JobAssignment();
				personTemp.setJobAssignmentId(jobAssignmentId);
				mileageLog.setJobAssignment(personTemp);
			}

			if (vehicleNumber != null && vehicleNumber > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(vehicleNumber);
				mileageLog.setLookupDetailByVehicleNumber(lookupDetail);
			}

			if (source != null && source > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(source);
				mileageLog.setLookupDetailBySource(lookupDetail);
			}
			if (destination != null && destination > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(destination);
				mileageLog.setLookupDetailByDestination(lookupDetail);
			}

			mileageLog.setStart(start);
			mileageLog.setEnd(end);
			mileageLog.setPurpose(purpose);

			returnMessage = fuelAllowanceBL.saveMileageLog(mileageLog);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("saveMileageLog() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (ArithmeticException ae) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Failure in creating the Allowance");
			return ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			if (mileageLogId != null && mileageLogId > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveMileageLog() Info unsuccessful Return");
			return ERROR;
		} finally {

		}
	}

	public String deleteFuelAllowance() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			FuelAllowance allowance = new FuelAllowance();
			String returnMessage = "SUCCESS";
			if (fuelAllowanceId != null && fuelAllowanceId > 0) {
				allowance = fuelAllowanceBL.getFuelAllowanceService()
						.getFuelAllowanceById(fuelAllowanceId);

			}
			returnMessage = fuelAllowanceBL.deleteFuelAllowance(allowance);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("deleteFuelAllowance() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deleteFuelAllowance() Info unsuccessful Return");
			return ERROR;
		}
	}

	public String deleteMileageLog() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			MileageLog allowance = new MileageLog();
			String returnMessage = "SUCCESS";
			if (mileageLogId != null && mileageLogId > 0) {
				allowance = fuelAllowanceBL.getFuelAllowanceService()
						.getMileageLogById(mileageLogId);

			}
			returnMessage = fuelAllowanceBL.deleteMileageLog(allowance);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("deleteMileageLog() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deleteMileageLog() Info unsuccessful Return");
			return ERROR;
		}
	}
	
	
	public String getMileageLogAdd() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			FuelAllowanceVO allowanceVO = null;
			if(recordId!=null && recordId>0){
				mileageLogId=recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (mileageLogId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(AttendancePolicy.class.getName());
					comment = fuelAllowanceBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			}
			if (mileageLogId != null && mileageLogId > 0) {
				MileageLog allowance = fuelAllowanceBL
						.getFuelAllowanceService().getMileageLogById(
								mileageLogId);
				if (allowance != null)
					allowanceVO = fuelAllowanceBL.convertEntityToVO(allowance);
			}
			List<FuelAllowance> allowances =null;
			if(allowanceVO!=null);
				allowances = fuelAllowanceBL
				.getFuelAllowanceService().getAllFuelAllowanceByCriteria(
						allowanceVO);

			ServletActionContext.getRequest()
			.setAttribute("FUEL_ALLOWANCES", allowances);
			
			List<LookupDetail> vechicleNumber = fuelAllowanceBL
					.getLookupMasterBL().getActiveLookupDetails(
							"VEHICLE_NUMBER", true);

			List<LookupDetail> location = fuelAllowanceBL.getLookupMasterBL()
					.getActiveLookupDetails("LOCATION", true);

			List<JobAssignment> persons = payrollBL.getJobAssignmentBL()
					.getJobAssignmentService()
					.getAllActiveJobAssignment(getImplementation());
			ServletActionContext.getRequest().setAttribute(
					"JOB_ASSIGNMENT_LIST", persons);
			ServletActionContext.getRequest().setAttribute("VEHICLE_NUMBER",
					vechicleNumber);
			ServletActionContext.getRequest()
					.setAttribute("LOCATION", location);

			ServletActionContext.getRequest().setAttribute("ALLOWANCE",
					allowanceVO);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}

	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}

	public Long getJobPayrollElementId() {
		return jobPayrollElementId;
	}

	public void setJobPayrollElementId(Long jobPayrollElementId) {
		this.jobPayrollElementId = jobPayrollElementId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Byte getPayPolicy() {
		return payPolicy;
	}

	public void setPayPolicy(Byte payPolicy) {
		this.payPolicy = payPolicy;
	}

	public PayrollBL getPayrollBL() {
		return payrollBL;
	}

	public void setPayrollBL(PayrollBL payrollBL) {
		this.payrollBL = payrollBL;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getIsFinanceImpact() {
		return isFinanceImpact;
	}

	public void setIsFinanceImpact(String isFinanceImpact) {
		this.isFinanceImpact = isFinanceImpact;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getFuelAllowanceId() {
		return fuelAllowanceId;
	}

	public void setFuelAllowanceId(Long fuelAllowanceId) {
		this.fuelAllowanceId = fuelAllowanceId;
	}

	public Long getCardType() {
		return cardType;
	}

	public void setCardType(Long cardType) {
		this.cardType = cardType;
	}

	public Long getFuelCompany() {
		return fuelCompany;
	}

	public void setFuelCompany(Long fuelCompany) {
		this.fuelCompany = fuelCompany;
	}

	public Long getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(Long vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public FuelAllowanceBL getFuelAllowanceBL() {
		return fuelAllowanceBL;
	}

	public void setFuelAllowanceBL(FuelAllowanceBL fuelAllowanceBL) {
		this.fuelAllowanceBL = fuelAllowanceBL;
	}

	public Long getMileageLogId() {
		return mileageLogId;
	}

	public void setMileageLogId(Long mileageLogId) {
		this.mileageLogId = mileageLogId;
	}

	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Double getStart() {
		return start;
	}

	public void setStart(Double start) {
		this.start = start;
	}

	public Double getEnd() {
		return end;
	}

	public void setEnd(Double end) {
		this.end = end;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public Long getPayPeriodTransactionId() {
		return payPeriodTransactionId;
	}

	public void setPayPeriodTransactionId(Long payPeriodTransactionId) {
		this.payPeriodTransactionId = payPeriodTransactionId;
	}

	public Long getSource() {
		return source;
	}

	public void setSource(Long source) {
		this.source = source;
	}

	public Long getDestination() {
		return destination;
	}

	public void setDestination(Long destination) {
		this.destination = destination;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}