package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Allowance;
import com.aiotech.aios.hr.domain.entity.AllowanceTransaction;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobPayrollElement;
import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.AllowanceVO;
import com.aiotech.aios.hr.domain.entity.vo.JobPayrollElementVO;
import com.aiotech.aios.hr.domain.entity.vo.PayPeriodVO;
import com.aiotech.aios.hr.service.bl.AllowanceBL;
import com.aiotech.aios.hr.service.bl.PayrollBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class AllowanceAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(AllowanceAction.class);
	private Implementation implementation;
	private AllowanceBL allowanceBL;
	private PayrollBL payrollBL;
	private String transactionDate;
	private Integer id;
	private Long jobAssignmentId;
	private Long jobPayrollElementId;
	private List<Object> aaData;
	private Byte payPolicy;
	private String fromDate;
	private String toDate;
	private String receiptNumber;
	private String isFinanceImpact;
	private Double amount;
	private String description;
	private Long payPeriodTransactionId;

	private Long allowanceId;
	private Long allowanceType;
	private Long allowanceSubType;
	private String paymentDetails;

	public String returnSuccess() {
		ServletActionContext.getRequest().setAttribute("jobAssignmentId",
				jobAssignmentId);
		ServletActionContext.getRequest().setAttribute("jobPayrollElementId",
				jobPayrollElementId);
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String getAllowanceList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();

			// Find the pay period of current transaction
			PayPeriodVO payPeriodVO = new PayPeriodVO();
			payPeriodVO.setJobAssignmentId(jobAssignmentId);
			payPeriodVO.setTransactionDate(DateFormat
					.convertStringToDate(transactionDate));
			payPeriodVO.setPayPolicy(payPolicy);
			payPeriodVO.setPayPeriodTransactionId(payPeriodTransactionId);

			PayPeriodTransaction payPeriodTransaction = allowanceBL
					.getPayPeriodBL().findPayPeriod(payPeriodVO);

			AllowanceVO allowanceVO = new AllowanceVO();
			if (payPeriodTransaction != null)
				allowanceVO.setPayPeriodTransaction(payPeriodTransaction);

			allowanceVO.setJobAssignmentId(jobAssignmentId);
			allowanceVO.setJobPayrollElementId(jobPayrollElementId);

			List<Allowance> allowances = allowanceBL.getAllowanceService()
					.getAllAllowanceByCriteria(allowanceVO);

			if (allowances != null && allowances.size() > 0) {
				AllowanceVO vO = null;
				for (Allowance list : allowances) {
					vO = new AllowanceVO();
					vO.setAllowanceId(list.getAllowanceId());
					vO.setAllowanceType(list.getLookupDetailByAllowanceType()
							.getDisplayName());
					if (list.getLookupDetailByAllowanceSubType() != null)
						vO.setAllowanceSubType(list
								.getLookupDetailByAllowanceSubType()
								.getDisplayName());
					vO.setFromDateView(DateFormat.convertDateToString(list
							.getFromDate().toString()));
					vO.setToDateView(DateFormat.convertDateToString(list
							.getToDate().toString()));
					vO.setAmount(list.getAmount());
					if (list.getIsFinanceImpact() != null
							&& list.getIsFinanceImpact() == true)
						vO.setIsFinanceView("YES");
					else
						vO.setIsFinanceView("NO");

					aaData.add(vO);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getAllowance() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			AllowanceVO allowanceVO = null;
			if (allowanceId != null && allowanceId > 0) {
				Allowance allowance = allowanceBL.getAllowanceService()
						.getAllowanceById(allowanceId);
				if (allowance != null)
					allowanceVO = allowanceBL.convertEntityToVO(allowance);
			}

			// Find the Maximum provided & actual earned amount
			{
				// Find the pay period of current transaction
				JobPayrollElementVO jobPayrollElementVO = null;
				PayPeriodVO payPeriodVO = new PayPeriodVO();
				payPeriodVO.setJobAssignmentId(jobAssignmentId);
				payPeriodVO.setTransactionDate(DateFormat
						.convertStringToDate(transactionDate));
				payPeriodVO.setPayPolicy(payPolicy);
				payPeriodVO.setPayPeriodTransactionId(payPeriodTransactionId);

				PayPeriodTransaction payPeriodTransaction = allowanceBL
						.getPayPeriodBL().findPayPeriod(payPeriodVO);
				AllowanceVO allowanceTempVO = new AllowanceVO();
				if (payPeriodTransaction != null)
					allowanceTempVO
							.setPayPeriodTransaction(payPeriodTransaction);

				allowanceTempVO.setJobAssignmentId(jobAssignmentId);
				allowanceTempVO.setJobPayrollElementId(jobPayrollElementId);

				List<Allowance> allowances = allowanceBL.getAllowanceService()
						.getAllAllowanceByCriteria(allowanceTempVO);

				jobPayrollElementVO = payrollBL.getJobPayrollInfomration(
						jobPayrollElementId, new ArrayList<Object>(allowances),
						payPeriodTransaction);

				ServletActionContext.getRequest().setAttribute("JOBPAY_INFO",
						jobPayrollElementVO);
			}

			List<LookupDetail> typeList = allowanceBL.getLookupMasterBL()
					.getActiveLookupDetails("ALLOWANCE_TYPE", true);

			List<LookupDetail> subType = allowanceBL.getLookupMasterBL()
					.getActiveLookupDetails("ALLOWANCE_SUB_TYPE", true);

			ServletActionContext.getRequest().setAttribute("ALLOWANCE_TYPE",
					typeList);
			ServletActionContext.getRequest().setAttribute("ALLOWANCE_SUB_TYPE",
					subType);
			List<LookupDetail> paymentType = allowanceBL.getLookupMasterBL()
			.getActiveLookupDetails("PAYMENT_TYPE", true);
				ServletActionContext.getRequest().setAttribute("PAYMENT_TYPE",
			paymentType);
			ServletActionContext.getRequest().setAttribute("ALLOWANCE",
					allowanceVO);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Add row
	public String allowanceAddRow() {
		try {

			List<LookupDetail> paymentType = allowanceBL.getLookupMasterBL()
					.getActiveLookupDetails("PAYMENT_TYPE", true);
			ServletActionContext.getRequest().setAttribute("PAYMENT_TYPE",
					paymentType);
			if (null == id)
				id = 0;
			ServletActionContext.getRequest().setAttribute("rowId", id);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String saveAllowance() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		String returnMessage = "SUCCESS";
		try {

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			Allowance allowance = new Allowance();
			if (allowanceId != null && allowanceId > 0) {
				allowance = allowanceBL.getAllowanceService().getAllowanceById(
						allowanceId);

			} else {
				allowance.setCreatedDate(new Date());
				allowance.setPerson(person);
			}
			allowance.setAmount(amount);
			if (fromDate != null && !fromDate.equals(""))
				allowance.setFromDate(DateFormat.convertStringToDate(fromDate));
			if (toDate != null && !toDate.equals(""))
				allowance.setToDate(DateFormat.convertStringToDate(toDate));
			allowance.setImplementation(getImplementation());
			allowance.setReceiptNumber(receiptNumber);
			if (isFinanceImpact != null
					&& isFinanceImpact.equalsIgnoreCase("true"))
				allowance.setIsFinanceImpact(true);
			else
				allowance.setIsFinanceImpact(false);
			if (jobAssignmentId != null) {
				JobAssignment jobAssignment = new JobAssignment();
				jobAssignment.setJobAssignmentId(jobAssignmentId);
				allowance.setJobAssignment(jobAssignment);
			}
			if (jobPayrollElementId != null) {
				JobPayrollElement jobPayrollElement = new JobPayrollElement();
				jobPayrollElement.setJobPayrollElementId(jobPayrollElementId);
				allowance.setJobPayrollElement(jobPayrollElement);
			}

			if (allowanceType != null && allowanceType > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(allowanceType);
				allowance.setLookupDetailByAllowanceType(lookupDetail);
			}
			if (allowanceSubType != null && allowanceSubType > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(allowanceSubType);
				allowance.setLookupDetailByAllowanceSubType(lookupDetail);
			}
			if (payPeriodTransactionId != null) {
				PayPeriodTransaction payPeriodTransaction = new PayPeriodTransaction();
				payPeriodTransaction.setPayPeriodTransactionId(payPeriodTransactionId);
				allowance.setPayPeriodTransaction(payPeriodTransaction);
			}
			allowance.setDescription(description);

			returnMessage = allowanceBL.saveAllowance(allowance,
					getPaymentList(paymentDetails));

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("saveAllowance() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (ArithmeticException ae) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Failure in creating the Allowance");
			return ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			if (allowanceId != null && allowanceId > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveAllowance() Info unsuccessful Return");
			return ERROR;
		} finally {

		}
	}

	public List<AllowanceTransaction> getPaymentList(String contractPaymetns) {
		List<AllowanceTransaction> allowanceList = new ArrayList<AllowanceTransaction>();
		if (contractPaymetns != null && !contractPaymetns.equals("")) {
			String[] recordArray = new String[contractPaymetns
					.split("(\\#\\@)").length];
			recordArray = contractPaymetns.split("(\\#\\@)");
			for (String record : recordArray) {
				AllowanceTransaction payment = new AllowanceTransaction();
				LookupDetail lookupDetail = new LookupDetail();

				String[] dataArray = new String[record.split("(\\$\\@)").length];
				dataArray = record.split("(\\$\\@)");
				if (!dataArray[0].trim().equals("-1")
						&& !dataArray[0].trim().equals(""))
					payment.setAllowanceTransactionId(Long
							.parseLong(dataArray[0]));
				if (!dataArray[1].trim().equals("-1")
						&& !dataArray[1].trim().equals("")) {
					lookupDetail
							.setLookupDetailId(Long.parseLong(dataArray[1]));
					payment.setLookupDetail(lookupDetail);
				}
				if (!dataArray[2].trim().equals("-1")
						&& !dataArray[2].trim().equals(""))
					payment.setAmount(Double.parseDouble(dataArray[2]));

				if (!dataArray[3].trim().equals("-1")
						&& !dataArray[3].trim().equals(""))
					payment.setDescription(dataArray[3]);

				allowanceList.add(payment);
			}
		}
		return allowanceList;
	}

	public String deleteAllowance() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			Allowance allowance = new Allowance();
			String returnMessage = "SUCCESS";
			if (allowanceId != null && allowanceId > 0) {
				allowance = allowanceBL.getAllowanceService().getAllowanceById(
						allowanceId);

			}
			returnMessage = allowanceBL.deleteAllowance(
					allowance,
					new ArrayList<AllowanceTransaction>(allowance
							.getAllowanceTransactions()));

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("deleteAllowance() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deleteAllowance() Info unsuccessful Return");
			return ERROR;
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}

	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}

	public Long getJobPayrollElementId() {
		return jobPayrollElementId;
	}

	public void setJobPayrollElementId(Long jobPayrollElementId) {
		this.jobPayrollElementId = jobPayrollElementId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Byte getPayPolicy() {
		return payPolicy;
	}

	public void setPayPolicy(Byte payPolicy) {
		this.payPolicy = payPolicy;
	}

	public PayrollBL getPayrollBL() {
		return payrollBL;
	}

	public void setPayrollBL(PayrollBL payrollBL) {
		this.payrollBL = payrollBL;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getIsFinanceImpact() {
		return isFinanceImpact;
	}

	public void setIsFinanceImpact(String isFinanceImpact) {
		this.isFinanceImpact = isFinanceImpact;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getAllowanceId() {
		return allowanceId;
	}

	public void setAllowanceId(Long allowanceId) {
		this.allowanceId = allowanceId;
	}

	public Long getAllowanceType() {
		return allowanceType;
	}

	public void setAllowanceType(Long allowanceType) {
		this.allowanceType = allowanceType;
	}

	public Long getAllowanceSubType() {
		return allowanceSubType;
	}

	public void setAllowanceSubType(Long allowanceSubType) {
		this.allowanceSubType = allowanceSubType;
	}

	public String getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public AllowanceBL getAllowanceBL() {
		return allowanceBL;
	}

	public void setAllowanceBL(AllowanceBL allowanceBL) {
		this.allowanceBL = allowanceBL;
	}

	public Long getPayPeriodTransactionId() {
		return payPeriodTransactionId;
	}

	public void setPayPeriodTransactionId(Long payPeriodTransactionId) {
		this.payPeriodTransactionId = payPeriodTransactionId;
	}

}
