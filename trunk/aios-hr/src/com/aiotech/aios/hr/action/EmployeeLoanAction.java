package com.aiotech.aios.hr.action;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.PaymentRequest;
import com.aiotech.aios.accounts.domain.entity.PaymentRequestDetail;
import com.aiotech.aios.accounts.domain.entity.vo.PaymentRequestDetailVO;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.LoanFrequency;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.EmployeeLoan;
import com.aiotech.aios.hr.domain.entity.EmployeeLoanRepayment;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.EmployeeLoanVO;
import com.aiotech.aios.hr.domain.entity.vo.EmployeePaymentVO;
import com.aiotech.aios.hr.service.bl.EmployeeLoanBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * @author Mohamed Rabeek
 * 
 */
public class EmployeeLoanAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(JobAssignmentAction.class);
	private Implementation implementation;
	private List<EmployeeLoanVO> employeeLoanVOs;
	private EmployeeLoanBL employeeLoanBL;
	private List<Object> aaData;

	public String returnSuccess() {

		return SUCCESS;
	}

	private Double sanctionedAmount;
	private String dueStartDate;
	private Integer numberOfRepayment;
	private char paymentFrequency;
	private Double fixedInterest;
	private Double interestPercentage;
	private Integer intrest;
	private EmployeeLoan employeeLoan = new EmployeeLoan();
	private Long employeeLoanId;
	private Long alertId;
	private Long recordId;
	private Long messageId;
	private String workflowReturnMessage;
	private int processType;
	private String printableContent;

	@SuppressWarnings("unchecked")
	public String getEmployeeLoanList() {
		try {
			aaData = new ArrayList<Object>();
			List<EmployeeLoan> employeeLoans = employeeLoanBL
					.getEmployeeLoanService().getAllEmployeeLoan(
							getImplementation());

			if (employeeLoans != null && employeeLoans.size() > 0) {

				Map<Integer, String> financeTypes = employeeLoanBL
						.financeTypes();
				Map<Integer, String> repaymentTypes = employeeLoanBL
						.repaymentTypes();
				Map<Integer, String> intrestTyps = employeeLoanBL.intrestTyps();
				EmployeeLoanVO employeeLoanVo = null;
				for (EmployeeLoan list : employeeLoans) {
					employeeLoanVo = new EmployeeLoanVO();
					employeeLoanVo.setEmployeeLoanId(list.getEmployeeLoanId());
					employeeLoanVo
							.setEmployeeName(list.getJobAssignment()
									.getPerson().getFirstName()
									+ " "
									+ list.getJobAssignment().getPerson()
											.getLastName());
					employeeLoanVo.setRequestedDate(list.getRequestedDate());
					employeeLoanVo.setSanctionedAmount(list
							.getSanctionedAmount());
					employeeLoanVo.setNumberOfRepayment(list
							.getNumberOfRepayment());
					employeeLoanVo.setFinanceTypeView(financeTypes.get(list
							.getFinanceType()));
					employeeLoanVo.setIntrestView(intrestTyps.get(list
							.getIntrest()));
					employeeLoanVo.setRepaymentTypeView(repaymentTypes.get(list
							.getRepaymentType()));

					employeeLoanVo
							.setPaymentFrequencyView(Constants.Accounts.LoanFrequency
									.get(list.getPaymentFrequency()).name());
					employeeLoanVo.setDueStartDateView(DateFormat
							.convertDateToString(list.getDueStartDate() + ""));
					employeeLoanVo.setRequestedDateView(DateFormat
							.convertDateToString(list.getRequestedDate() + ""));
					aaData.add(employeeLoanVo);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getEmployeeLoanAdd() {
		try {

			if (recordId != null && recordId > 0)
				employeeLoanId = recordId;

			Map<Integer, String> financeTypes = employeeLoanBL.financeTypes();
			Map<Integer, String> repaymentTypes = employeeLoanBL
					.repaymentTypes();
			Map<Integer, String> securityDocuments = employeeLoanBL
					.securityDocuments();
			Map<Integer, String> intrestTyps = employeeLoanBL.intrestTyps();
			LoanFrequency[] frequencyTypes = employeeLoanBL.frequencyTypes();
			EmployeeLoan employeeLoan = null;
			EmployeeLoanVO employeeLoanVO = null;
			if (employeeLoanId != null && employeeLoanId > 0) {
				employeeLoan = employeeLoanBL.getEmployeeLoanService()
						.getEmployeeLoanDetail(employeeLoanId);

				employeeLoanVO = employeeLoanBL
						.convertEntityIntoVO(employeeLoan);
				List<EmployeeLoanVO> employeeLoanVOs = employeeLoanBL
						.generateRepaymentSchedule(employeeLoan);
				ServletActionContext.getRequest().setAttribute(
						"EMPLOYEE_LOAN_REPAYMENT", employeeLoanVOs);
			}
			ServletActionContext.getRequest().setAttribute("FINANCE_TYPE_LIST",
					financeTypes);
			ServletActionContext.getRequest().setAttribute(
					"REPAYMENT_TYPE_LIST", repaymentTypes);
			ServletActionContext.getRequest().setAttribute(
					"SECURITY_TYPE_LIST", securityDocuments);
			ServletActionContext.getRequest().setAttribute("INTREST_TYPE_LIST",
					intrestTyps);
			ServletActionContext.getRequest().setAttribute(
					"FREQUENCY_TYPE_LIST", frequencyTypes);

			// Comment Information --Added by rafiq
			CommentVO comment = new CommentVO();
			if (employeeLoan != null && employeeLoan.getEmployeeLoanId() != 0) {
				comment.setRecordId(employeeLoan.getEmployeeLoanId());
				comment.setUseCase(EmployeeLoan.class.getName());
				comment = employeeLoanBL.getCommentBL().getCommentInfo(comment);
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
			}
			ServletActionContext.getRequest().setAttribute("EMPLOYEE_LOAN",
					employeeLoanVO);

			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}

	public String getRepaymentSchedule() {
		try {

			EmployeeLoan employeeLoan = new EmployeeLoan();
			if (employeeLoanId == null || employeeLoanId == 0) {
				employeeLoan.setPaymentFrequency(paymentFrequency);
				employeeLoan.setNumberOfRepayment(numberOfRepayment);
				employeeLoan.setDueStartDate(DateFormat
						.convertStringToDate(dueStartDate));
				employeeLoan.setSanctionedAmount(sanctionedAmount);
				employeeLoan.setIntrest(intrest);
				employeeLoan.setFixedInterest(fixedInterest);
				employeeLoan.setInterestPercentage(interestPercentage);
			}

			employeeLoan.setEmployeeLoanId(employeeLoanId);
			List<EmployeeLoanVO> employeeLoanVOs = employeeLoanBL
					.generateRepaymentSchedule(employeeLoan);
			ServletActionContext.getRequest().setAttribute(
					"EMPLOYEE_LOAN_REPAYMENT", employeeLoanVOs);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String saveEmployeeLoan() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			EmployeeLoanVO employeeLoanVO = null;
			if (employeeLoan.getEmployeeLoanId() != null
					&& employeeLoan.getEmployeeLoanId() > 0) {
				employeeLoanVO = new EmployeeLoanVO();
				EmployeeLoan editEmployeeLoan = new EmployeeLoan();
				editEmployeeLoan = employeeLoanBL
						.getEmployeeLoanService()
						.getEmployeeLoanDetail(employeeLoan.getEmployeeLoanId());
				employeeLoan.setCreatedDate(editEmployeeLoan.getCreatedDate());
				employeeLoan.setPersonByCreatedBy(editEmployeeLoan
						.getPersonByCreatedBy());
				if (editEmployeeLoan.getEmployeeLoanRepayments() != null)
					employeeLoanVO
							.setRepaymentList(new ArrayList<EmployeeLoanRepayment>(
									editEmployeeLoan
											.getEmployeeLoanRepayments()));
				employeeLoanBL.getEmployeeLoanService()
						.deleteEmployeeLoanRepayments(
								new ArrayList<EmployeeLoanRepayment>(
										editEmployeeLoan
												.getEmployeeLoanRepayments()));
				employeeLoanVO.setOldAmount(editEmployeeLoan
						.getSanctionedAmount());
				employeeLoanVO.setEditFlag(true);
				employeeLoanVO.setDeleteFlag(false);
				employeeLoanVO.setOldAmount(editEmployeeLoan
						.getSanctionedAmount());
			} else {
				employeeLoanVO = new EmployeeLoanVO();
				employeeLoanVO.setEditFlag(false);
				employeeLoanVO.setDeleteFlag(false);
				employeeLoan.setCreatedDate(new Date());
				employeeLoan.setPersonByCreatedBy(person);
			}
			HttpServletRequest request = ServletActionContext.getRequest();
			String sanctionedDate = request.getParameter("requestedDate");
			employeeLoan.setDueStartDate(DateFormat
					.convertStringToDate(dueStartDate));
			employeeLoan.setRequestedDate(DateFormat
					.convertStringToDate(sanctionedDate));
			employeeLoan.setSanctionedDate(DateFormat
					.convertStringToDate(sanctionedDate));
			employeeLoan.setImplementation(getImplementation());
			employeeLoan.setIsApprove(Byte
					.valueOf(Constants.RealEstate.Status.NoDecession.getCode()
							+ ""));
			employeeLoan.setRequestedAmount(employeeLoan.getSanctionedAmount());
			employeeLoan.setStatus(false);
			employeeLoanBL.saveEmployeeLoan(employeeLoan, employeeLoanVO,
					alertId);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("saveEmployeeLoan() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			if (employeeLoan.getEmployeeLoanId() != null
					&& employeeLoan.getEmployeeLoanId() > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveEmployeeLoan() Info unsuccessful Return");
			return ERROR;
		}
	}

	public String approvalEmployeeLoan() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			EmployeeLoanVO employeeLoanVO = null;
			if (employeeLoanId != null && employeeLoanId > 0) {
				employeeLoanVO = new EmployeeLoanVO();
				employeeLoan = employeeLoanBL.getEmployeeLoanService()
						.getEmployeeLoanDetail(employeeLoanId);
				if (WorkflowConstants.ReturnStatus.AddApproved.name().equals(
						workflowReturnMessage)) {
					employeeLoanVO.setDeleteFlag(false);
					employeeLoanVO.setEditFlag(false);
				} else if (WorkflowConstants.ReturnStatus.ModificationApproved
						.name().equals(workflowReturnMessage)) {
					employeeLoanVO.setEditFlag(true);
					employeeLoanVO.setDeleteFlag(false);
				} else if (WorkflowConstants.ReturnStatus.DeleteApproved.name()
						.equals(workflowReturnMessage)) {
					employeeLoanVO.setEditFlag(false);
					employeeLoanVO.setDeleteFlag(true);
				}

				employeeLoanVO.setMessageId(messageId);
				employeeLoanBL
						.approveEmployeeLoan(employeeLoan, employeeLoanVO);

			}

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("saveEmployeeLoan() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			if (employeeLoan.getEmployeeLoanId() != null
					&& employeeLoan.getEmployeeLoanId() > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveEmployeeLoan() Info unsuccessful Return");
			return ERROR;
		}
	}

	public String deleteEmployeeLoan() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			EmployeeLoanVO employeeLoanVO = null;
			if (employeeLoanId != null && employeeLoanId > 0) {
				employeeLoan = employeeLoanBL.getEmployeeLoanService()
						.getEmployeeLoanDetail(employeeLoanId);
			}

			employeeLoanBL.deleteEmployeeLoan(employeeLoan, employeeLoanVO,
					messageId);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("deleteEmployeeLoan() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deleteEmployeeLoan() Info unsuccessful Return");
			return ERROR;
		}
	}

	public String employeeDirectPayment() {

		LOGGER.info("Inside Module: Accounts : Method: employeeDirectPayment");
		try {
			if (recordId != null && recordId != null) {
				EmployeePaymentVO vo = new EmployeePaymentVO();
				employeeLoan = employeeLoanBL.getEmployeeLoanService()
						.getEmployeeLoanDetail(recordId);
				vo.setMessageId(alertId);
				vo.setObject(employeeLoan);
				vo.setRecordId(recordId);
				vo.setUseCase("EmployeeLoan");
				messageId = alertId;
				alertId = 0L;
				employeeLoanBL.getEmployeePaymentBL().employeeDirectPayment(vo);
			}

			LOGGER.info("Module: Accounts : Method: employeeDirectPayment SUCCESS");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: employeeDirectPayment Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String employeeLoanPrintOut() {
		try {
			LOGGER.info("Inside Module: HR : Method: employeeLoanPrintOut");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Boolean templatePrintEnabled = session
					.getAttribute("print_employeeadvance_template") != null ? ((String) session
					.getAttribute("print_employeeadvance_template"))
					.equalsIgnoreCase("true") : false;

			EmployeeLoan employeeLoan = null;
			EmployeeLoanVO employeeLoanVO = null;
			if (employeeLoanId != null && employeeLoanId > 0) {
				employeeLoan = employeeLoanBL.getEmployeeLoanService()
						.getEmployeeLoanDetail(employeeLoanId);

				employeeLoanVO = employeeLoanBL
						.convertEntityIntoVO(employeeLoan);
				List<EmployeeLoanVO> employeeLoanVOs = employeeLoanBL
						.generateRepaymentSchedule(employeeLoan);
				ServletActionContext.getRequest().setAttribute(
						"EMPLOYEE_LOAN_REPAYMENT", employeeLoanVOs);
			}

			if (templatePrintEnabled) {
				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/hr/"
						+ getImplementation().getImplementationId() + "/"));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config
						.getTemplate("html-employeeadvance-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();
				rootMap.put("datetime", DateFormat
						.convertDateToString(employeeLoanVO.getRequestedDate()
								.toString()));
				rootMap.put("requestBy", employeeLoanVO.getPersonName());
				rootMap.put("personName", employeeLoanVO.getPersonName());
				rootMap.put("personNumber", employeeLoanVO.getJobAssignment()
						.getJobAssignmentNumber());
				rootMap.put("department", employeeLoanVO.getDepartmentName());
				rootMap.put("location", employeeLoanVO.getLocationName());
				rootMap.put("designation", employeeLoanVO.getDesignationName());
				rootMap.put("numberOfSettlment", employeeLoanVO.getNumberOfRepayment());
				rootMap.put("type", employeeLoanVO.getFinanceTypeView().toUpperCase());
				
				String amountInWords = AIOSCommons
						.convertAmountToWords(employeeLoanVO
								.getSanctionedAmount());
				String decimalAmount = String.valueOf(AIOSCommons
						.formatAmount(employeeLoanVO.getSanctionedAmount()));
				decimalAmount = decimalAmount.substring(decimalAmount
						.lastIndexOf('.') + 1);
				if (Double.parseDouble(decimalAmount) > 0) {
					amountInWords = amountInWords
							.concat(" and ")
							.concat(AIOSCommons
									.convertAmountToWords(decimalAmount))
							.concat(" Fils ");
					String replaceWord = "Only";
					amountInWords = amountInWords.replaceAll(replaceWord, "");
					amountInWords = amountInWords.concat(" " + replaceWord);
				}
				rootMap.put("amountInWords", amountInWords);
				rootMap.put("totalAmount", AIOSCommons
						.formatAmount(employeeLoanVO.getSanctionedAmount()));
				rootMap.put("paymentDetails",
						employeeLoanVO.getLoanRepayments());
				rootMap.put("urlPath", urlPath);
				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
			}
			LOGGER.info("Module: Accounts : Method: employeeLoanPrintOut: Action Success");
			return "template";
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: employeeLoanPrintOut Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public List<EmployeeLoanVO> getEmployeeLoanVOs() {
		return employeeLoanVOs;
	}

	public void setEmployeeLoanVOs(List<EmployeeLoanVO> employeeLoanVOs) {
		this.employeeLoanVOs = employeeLoanVOs;
	}

	public EmployeeLoanBL getEmployeeLoanBL() {
		return employeeLoanBL;
	}

	public void setEmployeeLoanBL(EmployeeLoanBL employeeLoanBL) {
		this.employeeLoanBL = employeeLoanBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Double getSanctionedAmount() {
		return sanctionedAmount;
	}

	public void setSanctionedAmount(Double sanctionedAmount) {
		this.sanctionedAmount = sanctionedAmount;
	}

	public String getDueStartDate() {
		return dueStartDate;
	}

	public void setDueStartDate(String dueStartDate) {
		this.dueStartDate = dueStartDate;
	}

	public Integer getNumberOfRepayment() {
		return numberOfRepayment;
	}

	public void setNumberOfRepayment(Integer numberOfRepayment) {
		this.numberOfRepayment = numberOfRepayment;
	}

	public void setPaymentFrequency(char paymentFrequency) {
		this.paymentFrequency = paymentFrequency;
	}

	public char getPaymentFrequency() {
		return paymentFrequency;
	}

	public Double getFixedInterest() {
		return fixedInterest;
	}

	public void setFixedInterest(Double fixedInterest) {
		this.fixedInterest = fixedInterest;
	}

	public Double getInterestPercentage() {
		return interestPercentage;
	}

	public void setInterestPercentage(Double interestPercentage) {
		this.interestPercentage = interestPercentage;
	}

	public Integer getIntrest() {
		return intrest;
	}

	public void setIntrest(Integer intrest) {
		this.intrest = intrest;
	}

	public EmployeeLoan getEmployeeLoan() {
		return employeeLoan;
	}

	public void setEmployeeLoan(EmployeeLoan employeeLoan) {
		this.employeeLoan = employeeLoan;
	}

	public Long getEmployeeLoanId() {
		return employeeLoanId;
	}

	public void setEmployeeLoanId(Long employeeLoanId) {
		this.employeeLoanId = employeeLoanId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public String getWorkflowReturnMessage() {
		return workflowReturnMessage;
	}

	public void setWorkflowReturnMessage(String workflowReturnMessage) {
		this.workflowReturnMessage = workflowReturnMessage;
	}

	public int getProcessType() {
		return processType;
	}

	public void setProcessType(int processType) {
		this.processType = processType;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}

}
