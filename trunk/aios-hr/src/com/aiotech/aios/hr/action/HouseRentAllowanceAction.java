package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.HouseRentAllowance;
import com.aiotech.aios.hr.domain.entity.HraContractPayment;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobPayrollElement;
import com.aiotech.aios.hr.domain.entity.Location;
import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.PhoneAllowance;
import com.aiotech.aios.hr.domain.entity.vo.HouseRentAllowanceVO;
import com.aiotech.aios.hr.domain.entity.vo.JobPayrollElementVO;
import com.aiotech.aios.hr.domain.entity.vo.PayPeriodVO;
import com.aiotech.aios.hr.domain.entity.vo.PhoneAllowanceVO;
import com.aiotech.aios.hr.service.bl.HouseRentAllowanceBL;
import com.aiotech.aios.hr.service.bl.PayrollBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class HouseRentAllowanceAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(HouseRentAllowanceAction.class);
	private Implementation implementation;
	private HouseRentAllowanceBL houseRentAllowanceBL;
	private PayrollBL payrollBL;
	private String transactionDate;
	private Integer id;
	private Long jobAssignmentId;
	private Long jobPayrollElementId;
	private List<Object> aaData;
	private Byte payPolicy;
	private String fromDate;
	private String toDate;
	private String receiptNumber;
	private String isFinanceImpact;
	private Double amount;
	private String description;
	private Long payPeriodTransactionId;
	
	private Long locationId;
	private Long houseRentAllowanceId;
	private String contractPayments;
	private String contractNumber;
	private Long propertyId;

	public String returnSuccess() {
		ServletActionContext.getRequest().setAttribute("jobAssignmentId",
				jobAssignmentId);
		ServletActionContext.getRequest().setAttribute("jobPayrollElementId",
				jobPayrollElementId);
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String getHouseRentAllowanceList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();

			// Find the pay period of current transaction
			PayPeriodVO payPeriodVO = new PayPeriodVO();
			payPeriodVO.setJobAssignmentId(jobAssignmentId);
			payPeriodVO.setTransactionDate(DateFormat
					.convertStringToDate(transactionDate));
			payPeriodVO.setPayPolicy(payPolicy);
			payPeriodVO.setPayPeriodTransactionId(payPeriodTransactionId);
			PayPeriodTransaction payPeriodTransaction = houseRentAllowanceBL
					.getPayPeriodBL().findPayPeriod(payPeriodVO);

			HouseRentAllowanceVO houseRentAllowanceVO = new HouseRentAllowanceVO();
			if (payPeriodTransaction != null)
				houseRentAllowanceVO
						.setPayPeriodTransaction(payPeriodTransaction);

			houseRentAllowanceVO.setJobAssignmentId(jobAssignmentId);
			houseRentAllowanceVO.setJobPayrollElementId(jobPayrollElementId);

			List<HouseRentAllowance> hras = houseRentAllowanceBL
					.getHouseRentAllowanceService()
					.getAllHouseRentAllowanceByCriteria(houseRentAllowanceVO);

			if (hras != null && hras.size() > 0) {
				HouseRentAllowanceVO houseRentVO = null;
				for (HouseRentAllowance list : hras) {
					houseRentVO = new HouseRentAllowanceVO();
					houseRentVO.setHouseRentAllowanceId(list
							.getHouseRentAllowanceId());
					houseRentVO.setPropertyName(list.getLookupDetail()
							.getDisplayName());
					if (list.getContractNumber() != null)
						houseRentVO.setContractNumber(list.getContractNumber());
					houseRentVO
							.setFromDateView(DateFormat
									.convertDateToString(list.getFromDate()
											.toString()));
					houseRentVO.setToDateView(DateFormat
							.convertDateToString(list.getToDate().toString()));
					houseRentVO.setAmount(list.getAmount());
					if (list.getIsFinanceImpact() != null
							&& list.getIsFinanceImpact() == true)
						houseRentVO.setIsFinanceView("YES");
					else
						houseRentVO.setIsFinanceView("NO");

					aaData.add(houseRentVO);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getHouseRentAllowance() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HouseRentAllowanceVO houseRentAllowanceVO = null;
			if (houseRentAllowanceId != null && houseRentAllowanceId > 0) {
				HouseRentAllowance hra = houseRentAllowanceBL
						.getHouseRentAllowanceService()
						.getHouseRentAllowanceById(houseRentAllowanceId);
				if (hra != null)
					houseRentAllowanceVO = houseRentAllowanceBL
							.convertEntityToVO(hra);
			}
			
			//Find the Maximum provided & actual earned amount
			{
				// Find the pay period of current transaction
				JobPayrollElementVO jobPayrollElementVO=null;
				PayPeriodVO payPeriodVO = new PayPeriodVO();
				payPeriodVO.setJobAssignmentId(jobAssignmentId);
				payPeriodVO.setTransactionDate(DateFormat
						.convertStringToDate(transactionDate));
				payPeriodVO.setPayPolicy(payPolicy);
				payPeriodVO.setPayPeriodTransactionId(payPeriodTransactionId);
				PayPeriodTransaction payPeriodTransaction = houseRentAllowanceBL
						.getPayPeriodBL().findPayPeriod(payPeriodVO);
				HouseRentAllowanceVO houseRentAllowanceTempVO = new HouseRentAllowanceVO();
				if (payPeriodTransaction != null)
					houseRentAllowanceTempVO
							.setPayPeriodTransaction(payPeriodTransaction);

				houseRentAllowanceTempVO.setJobAssignmentId(jobAssignmentId);
				houseRentAllowanceTempVO
						.setJobPayrollElementId(jobPayrollElementId);

				List<HouseRentAllowance> houseRentAllowance = houseRentAllowanceBL
						.getHouseRentAllowanceService()
						.getAllHouseRentAllowanceByCriteria(houseRentAllowanceTempVO);

				jobPayrollElementVO = payrollBL.getJobPayrollInfomration(
						jobPayrollElementId, new ArrayList<Object>(
								houseRentAllowance), payPeriodTransaction);
				
				ServletActionContext.getRequest().setAttribute("JOBPAY_INFO",
						jobPayrollElementVO);
			}
			
			List<LookupDetail> propertyList = houseRentAllowanceBL
					.getLookupMasterBL().getActiveLookupDetails(
							"PROPERTY_NAME", true);

			List<LookupDetail> paymentType = houseRentAllowanceBL
					.getLookupMasterBL().getActiveLookupDetails("PAYMENT_TYPE",
							true);

			ServletActionContext.getRequest().setAttribute("PROPERTY_NAME",
					propertyList);
			ServletActionContext.getRequest().setAttribute("PAYMENT_TYPE",
					paymentType);

			ServletActionContext.getRequest().setAttribute("HRA",
					houseRentAllowanceVO);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Add row
	public String hraPaymentAddRow() {
		try {

			List<LookupDetail> paymentType = houseRentAllowanceBL
					.getLookupMasterBL().getActiveLookupDetails("PAYMENT_TYPE",
							true);
			ServletActionContext.getRequest().setAttribute("PAYMENT_TYPE",
					paymentType);
			if (null == id)
				id = 0;
			ServletActionContext.getRequest().setAttribute("rowId", id);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String saveHouseRentAllowance() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		String returnMessage = "SUCCESS";
		try {

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			HouseRentAllowance hra = new HouseRentAllowance();
			if (houseRentAllowanceId != null && houseRentAllowanceId > 0) {
				hra = houseRentAllowanceBL.getHouseRentAllowanceService()
						.getHouseRentAllowanceById(houseRentAllowanceId);

			} else {
				hra.setCreatedDate(new Date());
				hra.setPerson(person);
			}
			hra.setAmount(amount);
			hra.setReceiptNumber(receiptNumber);
			hra.setContractNumber(contractNumber);
			if (fromDate != null && !fromDate.equals(""))
				hra.setFromDate(DateFormat.convertStringToDate(fromDate));
			if (toDate != null && !toDate.equals(""))
				hra.setToDate(DateFormat.convertStringToDate(toDate));
			hra.setImplementation(getImplementation());
			hra.setIsApprove((byte) Constants.RealEstate.Status.Publish
					.getCode());
			if (isFinanceImpact != null
					&& isFinanceImpact.equalsIgnoreCase("true"))
				hra.setIsFinanceImpact(true);
			else
				hra.setIsFinanceImpact(false);
			if (jobAssignmentId != null) {
				JobAssignment jobAssignment = new JobAssignment();
				jobAssignment.setJobAssignmentId(jobAssignmentId);
				hra.setJobAssignment(jobAssignment);
			}
			if (jobPayrollElementId != null) {
				JobPayrollElement jobPayrollElement = new JobPayrollElement();
				jobPayrollElement.setJobPayrollElementId(jobPayrollElementId);
				hra.setJobPayrollElement(jobPayrollElement);
			}
			if (locationId != null && locationId > 0) {
				Location location = new Location();
				location.setLocationId(locationId);
				hra.setLocation(location);
			}
			if (propertyId != null && propertyId > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(propertyId);
				hra.setLookupDetail(lookupDetail);
			}
			
			if (payPeriodTransactionId != null) {
				PayPeriodTransaction payPeriodTransaction = new PayPeriodTransaction();
				payPeriodTransaction.setPayPeriodTransactionId(payPeriodTransactionId);
				hra.setPayPeriodTransaction(payPeriodTransaction);
			}
			
			hra.setDescription(description);

			returnMessage = houseRentAllowanceBL.saveHouseRentAllowance(hra,
					getContractPaymentDetails(contractPayments));

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("saveHouseRentAllowance() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (ArithmeticException ae) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Failure in creating the HouseRentAllowance");
			return ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			if (houseRentAllowanceId != null && houseRentAllowanceId > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveHouseRentAllowance() Info unsuccessful Return");
			return ERROR;
		} finally {

		}
	}

	public List<HraContractPayment> getContractPaymentDetails(
			String contractPaymetns) {
		List<HraContractPayment> contractPaymetnList = new ArrayList<HraContractPayment>();
		if (contractPaymetns != null && !contractPaymetns.equals("")) {
			String[] recordArray = new String[contractPaymetns
					.split("(\\#\\@)").length];
			recordArray = contractPaymetns.split("(\\#\\@)");
			for (String record : recordArray) {
				HraContractPayment contractPayment = new HraContractPayment();
				LookupDetail lookupDetail = new LookupDetail();

				String[] dataArray = new String[record.split("(\\$\\@)").length];
				dataArray = record.split("(\\$\\@)");
				if (!dataArray[0].trim().equals("-1")
						&& !dataArray[0].trim().equals(""))
					contractPayment.setHraContractPaymentId(Long
							.parseLong(dataArray[0]));
				if (!dataArray[1].trim().equals("-1")
						&& !dataArray[1].trim().equals("")) {
					lookupDetail
							.setLookupDetailId(Long.parseLong(dataArray[1]));
					contractPayment.setLookupDetail(lookupDetail);
				}
				if (!dataArray[2].trim().equals("-1")
						&& !dataArray[2].trim().equals(""))
					contractPayment.setAmount(Double.parseDouble(dataArray[2]));

				if (!dataArray[3].trim().equals("-1")
						&& !dataArray[3].trim().equals(""))
					contractPayment.setDescription(dataArray[3]);

				contractPaymetnList.add(contractPayment);
			}
		}
		return contractPaymetnList;
	}

	public String deleteHouseRentAllowance() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			HouseRentAllowance hra = new HouseRentAllowance();
			String returnMessage = "SUCCESS";
			if (houseRentAllowanceId != null && houseRentAllowanceId > 0) {
				hra = houseRentAllowanceBL.getHouseRentAllowanceService()
						.getHouseRentAllowanceById(houseRentAllowanceId);

			}
			returnMessage = houseRentAllowanceBL.deleteHouseRentAllowance(
					hra,
					new ArrayList<HraContractPayment>(hra
							.getHraContractPayments()));

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("deleteHouseRentAllowance() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deleteHouseRentAllowance() Info unsuccessful Return");
			return ERROR;
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public HouseRentAllowanceBL getHouseRentAllowanceBL() {
		return houseRentAllowanceBL;
	}

	public void setHouseRentAllowanceBL(
			HouseRentAllowanceBL houseRentAllowanceBL) {
		this.houseRentAllowanceBL = houseRentAllowanceBL;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}

	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}

	public Long getJobPayrollElementId() {
		return jobPayrollElementId;
	}

	public void setJobPayrollElementId(Long jobPayrollElementId) {
		this.jobPayrollElementId = jobPayrollElementId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Byte getPayPolicy() {
		return payPolicy;
	}

	public void setPayPolicy(Byte payPolicy) {
		this.payPolicy = payPolicy;
	}

	public PayrollBL getPayrollBL() {
		return payrollBL;
	}

	public void setPayrollBL(PayrollBL payrollBL) {
		this.payrollBL = payrollBL;
	}

	public Long getHouseRentAllowanceId() {
		return houseRentAllowanceId;
	}

	public void setHouseRentAllowanceId(Long houseRentAllowanceId) {
		this.houseRentAllowanceId = houseRentAllowanceId;
	}

	public String getContractPayments() {
		return contractPayments;
	}

	public void setContractPayments(String contractPayments) {
		this.contractPayments = contractPayments;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getIsFinanceImpact() {
		return isFinanceImpact;
	}

	public void setIsFinanceImpact(String isFinanceImpact) {
		this.isFinanceImpact = isFinanceImpact;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public Long getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Long propertyId) {
		this.propertyId = propertyId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getPayPeriodTransactionId() {
		return payPeriodTransactionId;
	}

	public void setPayPeriodTransactionId(Long payPeriodTransactionId) {
		this.payPeriodTransactionId = payPeriodTransactionId;
	}

}
