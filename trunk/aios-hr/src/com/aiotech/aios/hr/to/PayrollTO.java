package com.aiotech.aios.hr.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class PayrollTO implements Serializable{

	private Long payrollId;
	private String personName;
	private String companyName;
	private String departmentName;
	private String locationName;
	private String noOfDays;
	private String fromDate;
	private String toDate;
	private String createdDate;
	private String amount;
	private String netAmount;
	private String elementName;
	private String elementType;
	private String elementNature;
	private String amountInWords;
	private String earningAmount;
	private String deductionAmount;
	private String anchorExpression;
	private String linkLabel;
	private String approveAnchorExpression;
	private String approveLinkLabel;
	private String rejectAnchorExpression;
	private String rejectLinkLabel;
	private String earningTotal;
	private String deductionTotal;
	private String accountNumber;
	private String routingCode;
	private String payMonth;
	private String details;
	private String netAmountInWords;
	private String personNumber;
	private String designationName;
	private String basicAmount;
	private String salaryType;
	private String daysWorked;
	private String paymentMode;
	private String currency;
	private String createdDateStr;
	private Date dateStart;
	private Date dateEnd;
	private String lopDays;
	
	private List<PayrollTO> payrollTransactions;
	private List<PayrollTO> payrollDeductions;
	public Long getPayrollId() {
		return payrollId;
	}
	public void setPayrollId(Long payrollId) {
		this.payrollId = payrollId;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getNoOfDays() {
		return noOfDays;
	}
	public void setNoOfDays(String noOfDays) {
		this.noOfDays = noOfDays;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getNetAmount() {
		return netAmount;
	}
	public void setNetAmount(String netAmount) {
		this.netAmount = netAmount;
	}
	public String getElementName() {
		return elementName;
	}
	public void setElementName(String elementName) {
		this.elementName = elementName;
	}
	public String getElementType() {
		return elementType;
	}
	public void setElementType(String elementType) {
		this.elementType = elementType;
	}
	public String getElementNature() {
		return elementNature;
	}
	public void setElementNature(String elementNature) {
		this.elementNature = elementNature;
	}
	public String getAmountInWords() {
		return amountInWords;
	}
	public void setAmountInWords(String amountInWords) {
		this.amountInWords = amountInWords;
	}
	public String getEarningAmount() {
		return earningAmount;
	}
	public void setEarningAmount(String earningAmount) {
		this.earningAmount = earningAmount;
	}
	public String getDeductionAmount() {
		return deductionAmount;
	}
	public void setDeductionAmount(String deductionAmount) {
		this.deductionAmount = deductionAmount;
	}
	public List<PayrollTO> getPayrollTransactions() {
		return payrollTransactions;
	}
	public void setPayrollTransactions(List<PayrollTO> payrollTransactions) {
		this.payrollTransactions = payrollTransactions;
	}
	public String getAnchorExpression() {
		return anchorExpression;
	}
	public void setAnchorExpression(String anchorExpression) {
		this.anchorExpression = anchorExpression;
	}
	public String getLinkLabel() {
		return linkLabel;
	}
	public void setLinkLabel(String linkLabel) {
		this.linkLabel = linkLabel;
	}
	public String getApproveAnchorExpression() {
		return approveAnchorExpression;
	}
	public void setApproveAnchorExpression(String approveAnchorExpression) {
		this.approveAnchorExpression = approveAnchorExpression;
	}
	public String getApproveLinkLabel() {
		return approveLinkLabel;
	}
	public void setApproveLinkLabel(String approveLinkLabel) {
		this.approveLinkLabel = approveLinkLabel;
	}
	public String getRejectAnchorExpression() {
		return rejectAnchorExpression;
	}
	public void setRejectAnchorExpression(String rejectAnchorExpression) {
		this.rejectAnchorExpression = rejectAnchorExpression;
	}
	public String getRejectLinkLabel() {
		return rejectLinkLabel;
	}
	public void setRejectLinkLabel(String rejectLinkLabel) {
		this.rejectLinkLabel = rejectLinkLabel;
	}
	public String getEarningTotal() {
		return earningTotal;
	}
	public void setEarningTotal(String earningTotal) {
		this.earningTotal = earningTotal;
	}
	public String getDeductionTotal() {
		return deductionTotal;
	}
	public void setDeductionTotal(String deductionTotal) {
		this.deductionTotal = deductionTotal;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getRoutingCode() {
		return routingCode;
	}
	public void setRoutingCode(String routingCode) {
		this.routingCode = routingCode;
	}
	public String getPayMonth() {
		return payMonth;
	}
	public void setPayMonth(String payMonth) {
		this.payMonth = payMonth;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getNetAmountInWords() {
		return netAmountInWords;
	}
	public void setNetAmountInWords(String netAmountInWords) {
		this.netAmountInWords = netAmountInWords;
	}
	public String getPersonNumber() {
		return personNumber;
	}
	public void setPersonNumber(String personNumber) {
		this.personNumber = personNumber;
	}
	public String getDesignationName() {
		return designationName;
	}
	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	public String getBasicAmount() {
		return basicAmount;
	}
	public void setBasicAmount(String basicAmount) {
		this.basicAmount = basicAmount;
	}
	public String getSalaryType() {
		return salaryType;
	}
	public void setSalaryType(String salaryType) {
		this.salaryType = salaryType;
	}
	public String getDaysWorked() {
		return daysWorked;
	}
	public void setDaysWorked(String daysWorked) {
		this.daysWorked = daysWorked;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public List<PayrollTO> getPayrollDeductions() {
		return payrollDeductions;
	}
	public void setPayrollDeductions(List<PayrollTO> payrollDeductions) {
		this.payrollDeductions = payrollDeductions;
	}
	public String getCreatedDateStr() {
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	public Date getDateStart() {
		return dateStart;
	}
	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}
	public Date getDateEnd() {
		return dateEnd;
	}
	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}
	public String getLopDays() {
		return lopDays;
	}
	public void setLopDays(String lopDays) {
		this.lopDays = lopDays;
	}
}
