package com.aiotech.aios.hr.to;

/*******************************************************************************
*
* Create Log
* -----------------------------------------------------------------------------
* Ver 		Created Date 	Created By                 Description
* -----------------------------------------------------------------------------
* 1.0		04 Oct 2010 	Nagarajan T.	 		   Initial Version

******************************************************************************/

import java.io.Serializable;
import java.util.List;

import com.aiotech.aios.hr.domain.entity.PersonTypeAllocation;



public class HrPersonalDetailsTO implements Serializable{
	private String personFullName;
	private String personFullNameArabic;
	private String firstName;
	private String firstNameArabic;
	private String lastName;	
	private String lastNameArabic;
	private String title;
	private String prefix;
	private String suffix;
	private String middleName;
	private String middleNameArabic;
	private String personTypes;
	private String emirateId;
	private int identification;
	private String effectiveDatesFrom;
	private String effectiveDatesTo;
	private String latestStartDate;
	private String birthDate;
	private String townBirth;
	private String regionBirth;
	private String countryBirth;
	private String personGroup;
	private int personFlag;
	private String gender;
	private String genderCaption;
	private String maritalStatusCaption;
	private String nationalityCaption;
	private String personGroupCaption;
	private String status;
	private String age;
	private String maritalStatus;
	private String maritalStatusName;
	private String nationality;
	private String nationalityTypeId;
	private String nationalityTypeName;
	private int disabled;
	private String imageUpload;
	private String office;
	private String location;
	private String mailStop;
	private String emailAddress;
	private String mailTo;
	private int exists;
	private String holdApplicationUntil;
	private String lastUpdatedResume;
	private String honors;
	private String preferredName;
	private String previousLastName;
	private String availabilitySchedules;
	private Integer fullTimeAvailability;
	private String correspondenceLanguage;
	private String deathDate;
	private String studentStatus;
	private String dateLastVerified;
	private int military;
	private int secondPassport;
	private int recId;
	private int personId;
	private int sessionId;
	private String createdDate;
	
	private String description;
	private String expirationdate;
	private String identificationId;
	private String identificationDesc;
	private String identificationExpDate;
	private int commissionRuleId;
 	
	private String sql_return_msg;
	private int sql_error_num;
	private int sql_return_status;
	
	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	private String sidx;
	private int count;
	private Integer id;
	private List cell;
	
	private String applicantId; 
	//work flow
	private String notificationId; 
	private String notificationType;
	private String companyId;
	private String workflowId;
	private String workflowName;
	private String workflowStatus;
	private String categoryId; 
	private String notifyFunctionId;
	private String functionId;
	private String wfFunctionType;
	private String workflowParam;	
	private String mappingId;
	private String attribute1;
	private String applicationId;
	private String dmsTrnId;
	
	private int sessionPersonId;
	private String countryCode;
	private String countryName;
	private String addEditFlag;
	private String address;
	private String email;
	private String phone;
	private String mobile;
	private long personAddressId;
	private String residentialAddress;
	private String postBoxNumber;
	private String permanentAddress;
	private String alternateMobile;
	private String telephone;
	private String alternateTelephone;
	private long identityId;
	private int identityType;
	private String identityTypeName;
	private String identityNumber;
	private String issuedPlace;
	private String expireDate;
	private long dependentId;
	private int relationshipId;
	private String relationship;
	private String dependentName;
	private String dependentMobile;
	private String dependentPhone;
	private String dependentMail;
	private boolean emergency;
	private boolean dependentflag;
	private boolean benefit;
	private String dependentAddress;
	private String  dependentDescription;
	private String tradeLicenseNumber;
	private String tradeIssuedPlace;
	private String tradeExpireDate;
	private String tradeDescription;
	private String personName;
	private String daysLeft;
	private List<PersonTypeAllocation> personTypeAllocations;
	private String personTypeIds;
	private String profilePic;
	
	public int getSessionId() {
		return sessionId;
	}
	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}
	public int getPersonId() {
		return personId;
	}
	public void setPersonId(int personId) {
		this.personId = personId;
	}
	public String getSql_return_msg() {
		return sql_return_msg;
	}
	public void setSql_return_msg(String sql_return_msg) {
		this.sql_return_msg = sql_return_msg;
	}
	public int getSql_error_num() {
		return sql_error_num;
	}
	public void setSql_error_num(int sql_error_num) {
		this.sql_error_num = sql_error_num;
	}
	public int getSql_return_status() {
		return sql_return_status;
	}
	public void setSql_return_status(int sql_return_status) {
		this.sql_return_status = sql_return_status;
	}
	public int getRecId() {
		return recId;
	}
	public void setRecId(int recId) {
		this.recId = recId;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List getCell() {
		return cell;
	}
	public void setCell(List cell) {
		this.cell = cell;
	}
	public Integer getGridFrom() {
		return gridFrom;
	}
	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}
	public Integer getGridTo() {
		return gridTo;
	}
	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}
	public String getGridSord() {
		return gridSord;
	}
	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	public String getLatestStartDate() {
		return latestStartDate;
	}
	public void setLatestStartDate(String latestStartDate) {
		//this.latestStartDate = GetFormatedDate.getFormatedDate(latestStartDate, "yyyy-MM-dd", "sort");
		this.latestStartDate = latestStartDate;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getHoldApplicationUntil() {
		return holdApplicationUntil;
	}
	public void setHoldApplicationUntil(String holdApplicationUntil) {
		//this.holdApplicationUntil =GetFormatedDate.getFormatedDate(holdApplicationUntil, "yyyy-MM-dd", "sort");
		this.holdApplicationUntil = holdApplicationUntil;
	}
	public String getAvailabilitySchedules() {
		return availabilitySchedules;
	}
	public void setAvailabilitySchedules(String availabilitySchedules) {
		this.availabilitySchedules = availabilitySchedules;
	}
	public String getCorrespondenceLanguage() {
		return correspondenceLanguage;
	}
	public void setCorrespondenceLanguage(String correspondenceLanguage) {
		this.correspondenceLanguage = correspondenceLanguage;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getPersonTypes() {
		return personTypes;
	}
	public void setPersonTypes(String personTypes) {
		this.personTypes = personTypes;
	}
	public String getEmirateId() {
		return emirateId;
	}
	public void setEmirateId(String emirateId) {
		this.emirateId = emirateId;
	}
	public int getIdentification() {
		return identification;
	}
	public void setIdentification(int identification) {
		this.identification = identification;
	}
	public String getEffectiveDatesFrom() {
		return effectiveDatesFrom;
	}
	public void setEffectiveDatesFrom(String effectiveDatesFrom) {
		//this.effectiveDatesFrom= GetFormatedDate.getFormatedDate(effectiveDatesFrom, "yyyy-MM-dd", "sort");
		this.effectiveDatesFrom = effectiveDatesFrom;
	}
	public String getEffectiveDatesTo() {
		return effectiveDatesTo;
	}
	public void setEffectiveDatesTo(String effectiveDatesTo) {
		//this.effectiveDatesTo =GetFormatedDate.getFormatedDate(effectiveDatesTo, "yyyy-MM-dd", "sort");
		this.effectiveDatesTo = effectiveDatesTo;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getTownBirth() {
		return townBirth;
	}
	public void setTownBirth(String townBirth) {
		this.townBirth = townBirth;
	}
	public String getRegionBirth() {
		return regionBirth;
	}
	public void setRegionBirth(String regionBirth) {
		this.regionBirth = regionBirth;
	}
	public String getCountryBirth() {
		return countryBirth;
	}
	public void setCountryBirth(String countryBirth) {
		this.countryBirth = countryBirth;
	}
	

	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public int getDisabled() {
		return disabled;
	}
	public void setDisabled(int disabled) {
		this.disabled = disabled;
	}
	public String getImageUpload() {
		return imageUpload;
	}
	public void setImageUpload(String imageUpload) {
		this.imageUpload = imageUpload;
	}
	public String getOffice() {
		return office;
	}
	public void setOffice(String office) {
		this.office = office;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getMailStop() {
		return mailStop;
	}
	public void setMailStop(String mailStop) {
		this.mailStop = mailStop;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getMailTo() {
		return mailTo;
	}
	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}
	public int getExists() {
		return exists;
	}
	public void setExists(int exists) {
		this.exists = exists;
	}

	public String getLastUpdatedResume() {
		return lastUpdatedResume;
	}
	public void setLastUpdatedResume(String lastUpdatedResume) {
		//this.lastUpdatedResume =GetFormatedDate.getFormatedDate(lastUpdatedResume, "yyyy-MM-dd", "sort");
		this.lastUpdatedResume = lastUpdatedResume;
	}
	public String getHonors() {
		return honors;
	}
	public void setHonors(String honors) {
		this.honors = honors;
	}
	public String getPreferredName() {
		return preferredName;
	}
	public void setPreferredName(String preferredName) {
		this.preferredName = preferredName;
	}
	public String getPreviousLastName() {
		return previousLastName;
	}
	public void setPreviousLastName(String previousLastName) {
		this.previousLastName = previousLastName;
	}
	public Integer getFullTimeAvailability() {
		return fullTimeAvailability;
	}
	public void setFullTimeAvailability(Integer fullTimeAvailability) {
		this.fullTimeAvailability = fullTimeAvailability;
	}
	
	public String getDeathDate() {
		return deathDate;
	}
	public void setDeathDate(String deathDate) {
		this.deathDate = deathDate;
	}
	public String getStudentStatus() {
		return studentStatus;
	}
	public void setStudentStatus(String studentStatus) {
		this.studentStatus = studentStatus;
	}
	public String getDateLastVerified() {
		return dateLastVerified;
	}
	public void setDateLastVerified(String dateLastVerified) {
		this.dateLastVerified = dateLastVerified;
	}
	public int getMilitary() {
		return military;
	}
	public void setMilitary(int military) {
		this.military = military;
	}
	public int getSecondPassport() {
		return secondPassport;
	}
	public void setSecondPassport(int secondPassport) {
		this.secondPassport = secondPassport;
	}
	public int getPersonFlag() {
		return personFlag;
	}
	public void setPersonFlag(int personFlag) {
		this.personFlag = personFlag;
	}
	public String getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getWorkflowId() {
		return workflowId;
	}
	public void setWorkflowId(String workflowId) {
		this.workflowId = workflowId;
	}
	public String getWorkflowName() {
		return workflowName;
	}
	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}
	public String getWorkflowStatus() {
		return workflowStatus;
	}
	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getNotifyFunctionId() {
		return notifyFunctionId;
	}
	public void setNotifyFunctionId(String notifyFunctionId) {
		this.notifyFunctionId = notifyFunctionId;
	}
	public String getFunctionId() {
		return functionId;
	}
	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}
	public String getWfFunctionType() {
		return wfFunctionType;
	}
	public void setWfFunctionType(String wfFunctionType) {
		this.wfFunctionType = wfFunctionType;
	}
	public String getWorkflowParam() {
		return workflowParam;
	}
	public void setWorkflowParam(String workflowParam) {
		this.workflowParam = workflowParam;
	}
	public String getMappingId() {
		return mappingId;
	}
	public void setMappingId(String mappingId) {
		this.mappingId = mappingId;
	}
	public String getAttribute1() {
		return attribute1;
	}
	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getApplicantId() {
		return applicantId;
	}
	public void setApplicantId(String applicantId) {
		this.applicantId = applicantId;
	}
	public int getSessionPersonId() {
		return sessionPersonId;
	}
	public void setSessionPersonId(int sessionPersonId) {
		this.sessionPersonId = sessionPersonId;
	}
	public void setNationalityTypeId(String nationalityTypeId) {
		this.nationalityTypeId = nationalityTypeId;
	}
	public String getNationalityTypeId() {
		return nationalityTypeId;
	}
	public void setNationalityTypeName(String nationalityTypeName) {
		this.nationalityTypeName = nationalityTypeName;
	}
	public String getNationalityTypeName() {
		return nationalityTypeName;
	}
	public void setMaritalStatusName(String maritalStatusName) {
		this.maritalStatusName = maritalStatusName;
	}
	public String getMaritalStatusName() {
		return maritalStatusName;
	}
	public String getDmsTrnId() {
		return dmsTrnId;
	}
	public void setDmsTrnId(String dmsTrnId) {
		this.dmsTrnId = dmsTrnId;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getExpirationdate() {
		return expirationdate;
	}
	public void setExpirationdate(String expirationdate) {
		this.expirationdate = expirationdate;
	}
	public String getIdentificationId() {
		return identificationId;
	}
	public void setIdentificationId(String identificationId) {
		this.identificationId = identificationId;
	}
	public String getIdentificationDesc() {
		return identificationDesc;
	}
	public void setIdentificationDesc(String identificationDesc) {
		this.identificationDesc = identificationDesc;
	}
	public String getIdentificationExpDate() {
		return identificationExpDate;
	}
	public void setIdentificationExpDate(String identificationExpDate) {
		this.identificationExpDate = identificationExpDate;
	}
	public String getPersonGroup() {
		return personGroup;
	}
	public void setPersonGroup(String personGroup) {
		this.personGroup = personGroup;
	}
	public String getAddEditFlag() {
		return addEditFlag;
	}
	public void setAddEditFlag(String addEditFlag) {
		this.addEditFlag = addEditFlag;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public long getPersonAddressId() {
		return personAddressId;
	}
	public void setPersonAddressId(long personAddressId) {
		this.personAddressId = personAddressId;
	}
	public String getResidentialAddress() {
		return residentialAddress;
	}
	public void setResidentialAddress(String residentialAddress) {
		this.residentialAddress = residentialAddress;
	}
	public String getPostBoxNumber() {
		return postBoxNumber;
	}
	public void setPostBoxNumber(String postBoxNumber) {
		this.postBoxNumber = postBoxNumber;
	}
	public String getPermanentAddress() {
		return permanentAddress;
	}
	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}
	public String getAlternateMobile() {
		return alternateMobile;
	}
	public void setAlternateMobile(String alternateMobile) {
		this.alternateMobile = alternateMobile;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getAlternateTelephone() {
		return alternateTelephone;
	}
	public void setAlternateTelephone(String alternateTelephone) {
		this.alternateTelephone = alternateTelephone;
	}
	public long getIdentityId() {
		return identityId;
	}
	public void setIdentityId(long identityId) {
		this.identityId = identityId;
	}
	public int getIdentityType() {
		return identityType;
	}
	public void setIdentityType(int identityType) {
		this.identityType = identityType;
	}
	public String getIdentityNumber() {
		return identityNumber;
	}
	public void setIdentityNumber(String identityNumber) {
		this.identityNumber = identityNumber;
	}
	public String getIssuedPlace() {
		return issuedPlace;
	}
	public void setIssuedPlace(String issuedPlace) {
		this.issuedPlace = issuedPlace;
	}
	public String getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	public long getDependentId() {
		return dependentId;
	}
	public void setDependentId(long dependentId) {
		this.dependentId = dependentId;
	}
	public int getRelationshipId() {
		return relationshipId;
	}
	public void setRelationshipId(int relationshipId) {
		this.relationshipId = relationshipId;
	}
	public String getDependentName() {
		return dependentName;
	}
	public void setDependentName(String dependentName) {
		this.dependentName = dependentName;
	}
	public String getDependentMobile() {
		return dependentMobile;
	}
	public void setDependentMobile(String dependentMobile) {
		this.dependentMobile = dependentMobile;
	}
	public String getDependentPhone() {
		return dependentPhone;
	}
	public void setDependentPhone(String dependentPhone) {
		this.dependentPhone = dependentPhone;
	}
	public String getDependentMail() {
		return dependentMail;
	}
	public void setDependentMail(String dependentMail) {
		this.dependentMail = dependentMail;
	}
	public boolean isEmergency() {
		return emergency;
	}
	public void setEmergency(boolean emergency) {
		this.emergency = emergency;
	}
	
	public boolean isBenefit() {
		return benefit;
	}
	public void setBenefit(boolean benefit) {
		this.benefit = benefit;
	}
	public String getDependentAddress() {
		return dependentAddress;
	}
	public void setDependentAddress(String dependentAddress) {
		this.dependentAddress = dependentAddress;
	}
	public String getDependentDescription() {
		return dependentDescription;
	}
	public void setDependentDescription(String dependentDescription) {
		this.dependentDescription = dependentDescription;
	}
	public boolean isDependentflag() {
		return dependentflag;
	}
	public void setDependentflag(boolean dependentflag) {
		this.dependentflag = dependentflag;
	}
	public String getTradeLicenseNumber() {
		return tradeLicenseNumber;
	}
	public void setTradeLicenseNumber(String tradeLicenseNumber) {
		this.tradeLicenseNumber = tradeLicenseNumber;
	}
	public String getTradeIssuedPlace() {
		return tradeIssuedPlace;
	}
	public void setTradeIssuedPlace(String tradeIssuedPlace) {
		this.tradeIssuedPlace = tradeIssuedPlace;
	}
	public String getTradeExpireDate() {
		return tradeExpireDate;
	}
	public void setTradeExpireDate(String tradeExpireDate) {
		this.tradeExpireDate = tradeExpireDate;
	}
	public String getTradeDescription() {
		return tradeDescription;
	}
	public void setTradeDescription(String tradeDescription) {
		this.tradeDescription = tradeDescription;
	}
	public String getIdentityTypeName() {
		return identityTypeName;
	}
	public void setIdentityTypeName(String identityTypeName) {
		this.identityTypeName = identityTypeName;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	
	public String getFirstNameArabic() {
		return firstNameArabic;
	}
	public String getLastNameArabic() {
		return lastNameArabic;
	}
	public String getMiddleNameArabic() {
		return middleNameArabic;
	}
	public void setFirstNameArabic(String firstNameArabic) {
		this.firstNameArabic = firstNameArabic;
	}
	public void setLastNameArabic(String lastNameArabic) {
		this.lastNameArabic = lastNameArabic;
	}
	public void setMiddleNameArabic(String middleNameArabic) {
		this.middleNameArabic = middleNameArabic;
	}
	public String getDaysLeft() {
		return daysLeft;
	}
	public void setDaysLeft(String daysLeft) {
		this.daysLeft = daysLeft;
	}
	public List<PersonTypeAllocation> getPersonTypeAllocations() {
		return personTypeAllocations;
	}
	public void setPersonTypeAllocations(
			List<PersonTypeAllocation> personTypeAllocations) {
		this.personTypeAllocations = personTypeAllocations;
	}
	public String getPersonTypeIds() {
		return personTypeIds;
	}
	public void setPersonTypeIds(String personTypeIds) {
		this.personTypeIds = personTypeIds;
	}
	public String getProfilePic() {
		return profilePic;
	}
	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPersonFullNameArabic() {
		return personFullNameArabic;
	}
	public void setPersonFullNameArabic(String personFullNameArabic) {
		this.personFullNameArabic = personFullNameArabic;
	}
	public String getPersonFullName() {
		return personFullName;
	}
	public void setPersonFullName(String personFullName) {
		this.personFullName = personFullName;
	}
	public String getGenderCaption() {
		return genderCaption;
	}
	public void setGenderCaption(String genderCaption) {
		this.genderCaption = genderCaption;
	}
	public String getMaritalStatusCaption() {
		return maritalStatusCaption;
	}
	public void setMaritalStatusCaption(String maritalStatusCaption) {
		this.maritalStatusCaption = maritalStatusCaption;
	}
	public String getNationalityCaption() {
		return nationalityCaption;
	}
	public void setNationalityCaption(String nationalityCaption) {
		this.nationalityCaption = nationalityCaption;
	}
	public String getPersonGroupCaption() {
		return personGroupCaption;
	}
	public void setPersonGroupCaption(String personGroupCaption) {
		this.personGroupCaption = personGroupCaption;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public int getCommissionRuleId() {
		return commissionRuleId;
	}
	public void setCommissionRuleId(int commissionRuleId) {
		this.commissionRuleId = commissionRuleId;
	}
}
