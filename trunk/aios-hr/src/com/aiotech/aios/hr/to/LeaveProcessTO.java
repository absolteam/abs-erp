package com.aiotech.aios.hr.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.Leave;
import com.aiotech.aios.hr.domain.entity.Payroll;

public class LeaveProcessTO implements Serializable{
	private Long leaveProcessId;
	private Long jobLeaveId;
	private String fromDate;
	private String toDate;
	private String reason;
	private String contactOnLeave;
	private boolean halfDay;
	private String returnMessage;
	private String personName;
	private String locationName;
	private String statusStr;
	private String halfDayStr;
	private String approvedBy;
	private String createdDate;
	private String anchorExpression;
	private String linkLabel;
	private String approveAnchorExpression;
	private String approveLinkLabel;
	private String rejectAnchorExpression;
	private String rejectLinkLabel;
	private String leaveType;
	private String companyName;
	private String departmentName;
	private String numberOfDays;
	private String isApprove;
	private Date personJoinedDate;
	private List<LeaveProcessTO> leaveHistoryList;
	private List<LeaveProcessTO> leaveAvailabilityList;
	private String providedDays;
	private String takenDays;
	private String availableDays;
	private String eligibleDays;
	private Long leaveId;
	private Long jobAssignmentId;
	private JobAssignment jobAssignment;
	private Leave leave;
	private String designationName;
	private String personNumber;
	private String totalAmountStr;
	private Double totalAmount;
	private boolean encash;
	private Payroll payroll;
	private String note;
	private String joiningDate;
	
	private List<LeaveProcessTO> subList;
	
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public Long getLeaveProcessId() {
		return leaveProcessId;
	}
	public void setLeaveProcessId(Long leaveProcessId) {
		this.leaveProcessId = leaveProcessId;
	}
	public Long getJobLeaveId() {
		return jobLeaveId;
	}
	public void setJobLeaveId(Long jobLeaveId) {
		this.jobLeaveId = jobLeaveId;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getContactOnLeave() {
		return contactOnLeave;
	}
	public void setContactOnLeave(String contactOnLeave) {
		this.contactOnLeave = contactOnLeave;
	}
	public boolean isHalfDay() {
		return halfDay;
	}
	public void setHalfDay(boolean halfDay) {
		this.halfDay = halfDay;
	}
	public String getReturnMessage() {
		return returnMessage;
	}
	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getStatusStr() {
		return statusStr;
	}
	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}
	public String getHalfDayStr() {
		return halfDayStr;
	}
	public void setHalfDayStr(String halfDayStr) {
		this.halfDayStr = halfDayStr;
	}
	public String getApprovedBy() {
		return approvedBy;
	}
	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}
	public String getAnchorExpression() {
		return anchorExpression;
	}
	public void setAnchorExpression(String anchorExpression) {
		this.anchorExpression = anchorExpression;
	}
	public String getLinkLabel() {
		return linkLabel;
	}
	public void setLinkLabel(String linkLabel) {
		this.linkLabel = linkLabel;
	}
	public String getApproveAnchorExpression() {
		return approveAnchorExpression;
	}
	public void setApproveAnchorExpression(String approveAnchorExpression) {
		this.approveAnchorExpression = approveAnchorExpression;
	}
	public String getApproveLinkLabel() {
		return approveLinkLabel;
	}
	public void setApproveLinkLabel(String approveLinkLabel) {
		this.approveLinkLabel = approveLinkLabel;
	}
	public String getRejectAnchorExpression() {
		return rejectAnchorExpression;
	}
	public void setRejectAnchorExpression(String rejectAnchorExpression) {
		this.rejectAnchorExpression = rejectAnchorExpression;
	}
	public String getRejectLinkLabel() {
		return rejectLinkLabel;
	}
	public void setRejectLinkLabel(String rejectLinkLabel) {
		this.rejectLinkLabel = rejectLinkLabel;
	}
	public String getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public List<LeaveProcessTO> getLeaveHistoryList() {
		return leaveHistoryList;
	}
	public void setLeaveHistoryList(List<LeaveProcessTO> leaveHistoryList) {
		this.leaveHistoryList = leaveHistoryList;
	}
	public String getNumberOfDays() {
		return numberOfDays;
	}
	public void setNumberOfDays(String numberOfDays) {
		this.numberOfDays = numberOfDays;
	}
	public String getIsApprove() {
		return isApprove;
	}
	public void setIsApprove(String isApprove) {
		this.isApprove = isApprove;
	}
	public List<LeaveProcessTO> getLeaveAvailabilityList() {
		return leaveAvailabilityList;
	}
	public void setLeaveAvailabilityList(List<LeaveProcessTO> leaveAvailabilityList) {
		this.leaveAvailabilityList = leaveAvailabilityList;
	}
	public Date getPersonJoinedDate() {
		return personJoinedDate;
	}
	public void setPersonJoinedDate(Date personJoinedDate) {
		this.personJoinedDate = personJoinedDate;
	}
	public String getProvidedDays() {
		return providedDays;
	}
	public void setProvidedDays(String providedDays) {
		this.providedDays = providedDays;
	}
	public String getAvailableDays() {
		return availableDays;
	}
	public void setAvailableDays(String availableDays) {
		this.availableDays = availableDays;
	}
	public Long getLeaveId() {
		return leaveId;
	}
	public void setLeaveId(Long leaveId) {
		this.leaveId = leaveId;
	}
	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}
	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}
	public String getTakenDays() {
		return takenDays;
	}
	public void setTakenDays(String takenDays) {
		this.takenDays = takenDays;
	}
	public JobAssignment getJobAssignment() {
		return jobAssignment;
	}
	public void setJobAssignment(JobAssignment jobAssignment) {
		this.jobAssignment = jobAssignment;
	}
	public Leave getLeave() {
		return leave;
	}
	public void setLeave(Leave leave) {
		this.leave = leave;
	}
	public String getEligibleDays() {
		return eligibleDays;
	}
	public void setEligibleDays(String eligibleDays) {
		this.eligibleDays = eligibleDays;
	}
	public List<LeaveProcessTO> getSubList() {
		return subList;
	}
	public void setSubList(List<LeaveProcessTO> subList) {
		this.subList = subList;
	}
	public String getDesignationName() {
		return designationName;
	}
	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	public String getPersonNumber() {
		return personNumber;
	}
	public void setPersonNumber(String personNumber) {
		this.personNumber = personNumber;
	}
	public String getTotalAmountStr() {
		return totalAmountStr;
	}
	public void setTotalAmountStr(String totalAmountStr) {
		this.totalAmountStr = totalAmountStr;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public boolean isEncash() {
		return encash;
	}
	public void setEncash(boolean encash) {
		this.encash = encash;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getJoiningDate() {
		return joiningDate;
	}
	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}
	
	
	
	
}
