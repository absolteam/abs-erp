package com.aiotech.aios.hr.to;

import java.io.Serializable;
import java.util.List;

import com.aiotech.aios.hr.domain.entity.Attendance;

public class AttendanceTO extends Attendance implements Serializable {
	private Long attendanceId;
	private String fromDate;
	private String toDate;
	private String reason;
	private String personName;
	private String locationName;
	private String statusStr;
	private String createdDateStr;
	private String anchorExpression;
	private String linkLabel;
	private String approveAnchorExpression;
	private String approveLinkLabel;
	private String rejectAnchorExpression;
	private String rejectLinkLabel;
	private String companyName;
	private String departmentName;
	private String numberOfDays;
	private String isApproveStr;
	private String inTime;
	private String outTime;
	private String attedanceDate;
	private String lateStr;
	private String earlyStr;
	private String timeLossOrExcess;
	private String totalHours;
	private String decision;
	private String designationName;
	private Integer lateInDays;
	private Integer lateOutDays;
	private Integer earlyInDays;
	private Integer earlyOutDays;
	private Integer timeLossDyas;
	private Integer timeExcessDays;
	private String inPart;
	private String outPart;
	private int absentDays;
	private List<AttendanceTO> attendanceHistoryList;
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getStatusStr() {
		return statusStr;
	}
	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}
	public String getCreatedDateStr() {
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	public String getAnchorExpression() {
		return anchorExpression;
	}
	public void setAnchorExpression(String anchorExpression) {
		this.anchorExpression = anchorExpression;
	}
	public String getLinkLabel() {
		return linkLabel;
	}
	public void setLinkLabel(String linkLabel) {
		this.linkLabel = linkLabel;
	}
	public String getApproveAnchorExpression() {
		return approveAnchorExpression;
	}
	public void setApproveAnchorExpression(String approveAnchorExpression) {
		this.approveAnchorExpression = approveAnchorExpression;
	}
	public String getApproveLinkLabel() {
		return approveLinkLabel;
	}
	public void setApproveLinkLabel(String approveLinkLabel) {
		this.approveLinkLabel = approveLinkLabel;
	}
	public String getRejectAnchorExpression() {
		return rejectAnchorExpression;
	}
	public void setRejectAnchorExpression(String rejectAnchorExpression) {
		this.rejectAnchorExpression = rejectAnchorExpression;
	}
	public String getRejectLinkLabel() {
		return rejectLinkLabel;
	}
	public void setRejectLinkLabel(String rejectLinkLabel) {
		this.rejectLinkLabel = rejectLinkLabel;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getNumberOfDays() {
		return numberOfDays;
	}
	public void setNumberOfDays(String numberOfDays) {
		this.numberOfDays = numberOfDays;
	}
	public String getIsApproveStr() {
		return isApproveStr;
	}
	public void setIsApproveStr(String isApproveStr) {
		this.isApproveStr = isApproveStr;
	}
	public String getInTime() {
		return inTime;
	}
	public void setInTime(String inTime) {
		this.inTime = inTime;
	}
	public String getOutTime() {
		return outTime;
	}
	public void setOutTime(String outTime) {
		this.outTime = outTime;
	}
	public String getAttedanceDate() {
		return attedanceDate;
	}
	public void setAttedanceDate(String attedanceDate) {
		this.attedanceDate = attedanceDate;
	}
	public String getLateStr() {
		return lateStr;
	}
	public void setLateStr(String lateStr) {
		this.lateStr = lateStr;
	}
	public String getEarlyStr() {
		return earlyStr;
	}
	public void setEarlyStr(String earlyStr) {
		this.earlyStr = earlyStr;
	}
	public String getTimeLossOrExcess() {
		return timeLossOrExcess;
	}
	public void setTimeLossOrExcess(String timeLossOrExcess) {
		this.timeLossOrExcess = timeLossOrExcess;
	}
	public String getTotalHours() {
		return totalHours;
	}
	public void setTotalHours(String totalHours) {
		this.totalHours = totalHours;
	}
	public String getDecision() {
		return decision;
	}
	public void setDecision(String decision) {
		this.decision = decision;
	}
	public List<AttendanceTO> getAttendanceHistoryList() {
		return attendanceHistoryList;
	}
	public void setAttendanceHistoryList(List<AttendanceTO> attendanceHistoryList) {
		this.attendanceHistoryList = attendanceHistoryList;
	}
	public String getDesignationName() {
		return designationName;
	}
	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	public Integer getLateInDays() {
		return lateInDays;
	}
	public void setLateInDays(Integer lateInDays) {
		this.lateInDays = lateInDays;
	}
	public Integer getLateOutDays() {
		return lateOutDays;
	}
	public void setLateOutDays(Integer lateOutDays) {
		this.lateOutDays = lateOutDays;
	}
	public Integer getEarlyInDays() {
		return earlyInDays;
	}
	public void setEarlyInDays(Integer earlyInDays) {
		this.earlyInDays = earlyInDays;
	}
	public Integer getEarlyOutDays() {
		return earlyOutDays;
	}
	public void setEarlyOutDays(Integer earlyOutDays) {
		this.earlyOutDays = earlyOutDays;
	}
	public Integer getTimeLossDyas() {
		return timeLossDyas;
	}
	public void setTimeLossDyas(Integer timeLossDyas) {
		this.timeLossDyas = timeLossDyas;
	}
	public Integer getTimeExcessDays() {
		return timeExcessDays;
	}
	public void setTimeExcessDays(Integer timeExcessDays) {
		this.timeExcessDays = timeExcessDays;
	}
	public Long getAttendanceId() {
		return attendanceId;
	}
	public void setAttendanceId(Long attendanceId) {
		this.attendanceId = attendanceId;
	}
	public String getInPart() {
		return inPart;
	}
	public void setInPart(String inPart) {
		this.inPart = inPart;
	}
	public String getOutPart() {
		return outPart;
	}
	public void setOutPart(String outPart) {
		this.outPart = outPart;
	}
	public int getAbsentDays() {
		return absentDays;
	}
	public void setAbsentDays(int absentDays) {
		this.absentDays = absentDays;
	}
	
}
