package com.aiotech.aios.hr.to;

import java.io.Serializable;
import java.util.List;

public class CompanyTO implements Serializable {
	private long companyId;
	private String companyName;
	private String companyNameArabic;
	private int companyTypeId;
	private String companyTypeName;
	private String description;
	private long tradeId;
	private String tradeName;
	private String id;
	private String companyOrigin;
	private String tradeNoIssueDate;

	private String companyAddress;
	private String companyPobox;
	private String companyFax;
	private String companyPhone;
	private String accountNumber;
	private String bankAccountId;
	private String labourCardId;
	private String companyTypes;
	private String companyTypeDataIds;
	
	private List<CompanyTO> departments;
	public String getCompanyAddress() {
		return companyAddress;
	}
	public String getCompanyPobox() {
		return companyPobox;
	}
	public String getCompanyFax() {
		return companyFax;
	}
	public String getCompanyPhone() {
		return companyPhone;
	}
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	public void setCompanyPobox(String companyPobox) {
		this.companyPobox = companyPobox;
	}
	public void setCompanyFax(String companyFax) {
		this.companyFax = companyFax;
	}
	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public int getCompanyTypeId() {
		return companyTypeId;
	}
	public void setCompanyTypeId(int companyTypeId) {
		this.companyTypeId = companyTypeId;
	}
	public String getCompanyTypeName() {
		return companyTypeName;
	}
	public void setCompanyTypeName(String companyTypeName) {
		this.companyTypeName = companyTypeName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getTradeId() {
		return tradeId;
	}
	public void setTradeId(long tradeId) {
		this.tradeId = tradeId;
	}
	public String getTradeName() {
		return tradeName;
	}
	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCompanyNameArabic() {
		return companyNameArabic;
	}
	public void setCompanyNameArabic(String companyNameArabic) {
		this.companyNameArabic = companyNameArabic;
	}

	public String getCompanyOrigin() {
		return companyOrigin;
	}
	public String getTradeNoIssueDate() {
		return tradeNoIssueDate;
	}
	public void setCompanyOrigin(String companyOrigin) {
		this.companyOrigin = companyOrigin;
	}
	public void setTradeNoIssueDate(String tradeNoIssueDate) {
		this.tradeNoIssueDate = tradeNoIssueDate;
	}
	public List<CompanyTO> getDepartments() {
		return departments;
	}
	public void setDepartments(List<CompanyTO> departments) {
		this.departments = departments;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getBankAccountId() {
		return bankAccountId;
	}
	public void setBankAccountId(String bankAccountId) {
		this.bankAccountId = bankAccountId;
	}
	public String getLabourCardId() {
		return labourCardId;
	}
	public void setLabourCardId(String labourCardId) {
		this.labourCardId = labourCardId;
	}
	public String getCompanyTypes() {
		return companyTypes;
	}
	public void setCompanyTypes(String companyTypes) {
		this.companyTypes = companyTypes;
	}
	public String getCompanyTypeDataIds() {
		return companyTypeDataIds;
	}
	public void setCompanyTypeDataIds(String companyTypeDataIds) {
		this.companyTypeDataIds = companyTypeDataIds;
	}
}
