package com.aiotech.aios.hr.to.converter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Dependent;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.PersonTypeAllocation;
import com.aiotech.aios.hr.to.HrPersonalDetailsTO;

public class HrPersonalDetailsTOConverter {

	public static List<HrPersonalDetailsTO> convertToTOList(List<Person> person)
			throws Exception {

		List<HrPersonalDetailsTO> tosList = new ArrayList<HrPersonalDetailsTO>();

		for (Person pers : person) {
			tosList.add(convertPersonToTO(pers));
		}

		return tosList;
	}

	public static HrPersonalDetailsTO convertPersonToTO(Person person)
			throws Exception {
		HrPersonalDetailsTO infoTO = new HrPersonalDetailsTO();

		if (person.getPersonId() != null && person.getPersonId() != 0)
			infoTO.setPersonId(Integer
					.parseInt(person.getPersonId().toString()));

		if (person.getPersonNumber() != null)
			infoTO.setIdentification(Integer.parseInt(person.getPersonNumber()
					.toString()));
		infoTO.setFirstName(person.getFirstName());
		infoTO.setMiddleName(person.getMiddleName());
		infoTO.setLastName(person.getLastName());
		infoTO.setFirstNameArabic(AIOSCommons.bytesToUTFString(person
				.getFirstNameArabic()));
		infoTO.setMiddleNameArabic(AIOSCommons.bytesToUTFString(person
				.getMiddleNameArabic()));
		infoTO.setLastNameArabic(AIOSCommons.bytesToUTFString(person
				.getLastNameArabic()));
		infoTO.setPrefix(person.getPrefix());
		infoTO.setSuffix(person.getSuffix());

		infoTO.setPersonFullName("");
		if (infoTO.getPrefix() != null && !infoTO.getPrefix().equals(""))
			infoTO.setPersonFullName(infoTO.getPersonFullName() + " "
					+ infoTO.getPrefix());

		if (infoTO.getFirstName() != null && !infoTO.getFirstName().equals(""))
			infoTO.setPersonFullName(infoTO.getPersonFullName() + " "
					+ infoTO.getFirstName());

		if (infoTO.getMiddleName() != null
				&& !infoTO.getMiddleName().equals(""))
			infoTO.setPersonFullName(infoTO.getPersonFullName() + " "
					+ infoTO.getMiddleName());

		if (infoTO.getLastName() != null && !infoTO.getLastName().equals(""))
			infoTO.setPersonFullName(infoTO.getPersonFullName() + " "
					+ infoTO.getLastName());

		if (infoTO.getSuffix() != null && !infoTO.getSuffix().equals(""))
			infoTO.setPersonFullName(infoTO.getPersonFullName() + " "
					+ infoTO.getSuffix());

		if (person.getPersonTypeAllocations() != null
				&& person.getPersonTypeAllocations().size() > 0) {
			for (PersonTypeAllocation personTypeAllocation : person
					.getPersonTypeAllocations()) {
				infoTO.setPersonTypes(personTypeAllocation.getLookupDetail()
						.getDataId().toString());
			}
		}

		if (person.getValidFromDate() != null)
			infoTO.setEffectiveDatesFrom(DateFormat.convertDateToString(person
					.getValidFromDate().toString()));

		if (person.getValidToDate() != null)
			infoTO.setEffectiveDatesTo(DateFormat.convertDateToString(person
					.getValidToDate().toString()));

		if (person.getBirthDate() != null)
			infoTO.setBirthDate(DateFormat.convertDateToString(person
					.getBirthDate().toString()));

		infoTO.setCountryBirth(person.getCountryBirth());
		infoTO.setCountryCode(person.getNationality());
		infoTO.setRegionBirth(person.getRegionBirth());
		infoTO.setTownBirth(person.getTownBirth());
		infoTO.setPersonGroup(person.getPersonGroup() + "");
		if (person.getGender() != null)
			infoTO.setGender(person.getGender().toString());

		infoTO.setMaritalStatus(person.getMaritalStatus() + "");
		infoTO.setNationality(person.getNationality());

		infoTO.setPersonFlag(null != person.getStatus() ? person.getStatus()
				: 1);
		infoTO.setResidentialAddress(person.getResidentialAddress());
		infoTO.setPermanentAddress(person.getPermanentAddress());
		infoTO.setPostBoxNumber(person.getPostboxNumber());
		infoTO.setEmail(person.getEmail());
		if (person.getMobile() != null)
			infoTO.setMobile(person.getMobile().toString());
		if (person.getAlternateMobile() != null)
			infoTO.setAlternateMobile(person.getAlternateMobile().toString());
		if (person.getTelephone() != null)
			infoTO.setTelephone(person.getTelephone().toString());
		if (person.getAlternateTelephone() != null)
			infoTO.setAlternateTelephone(person.getAlternateTelephone()
					.toString());
		infoTO.setTradeLicenseNumber(person.getTradeLicenseNumber());
		infoTO.setTradeIssuedPlace(person.getIssuedPlace());
		if (person.getExpireDate() != null)
			infoTO.setTradeExpireDate(DateFormat.convertDateToString(person
					.getExpireDate().toString()));
		infoTO.setTradeDescription(person.getDescription());
		if (person.getPersonTypeAllocations() != null
				&& person.getPersonTypeAllocations().size() > 0) {
			String tempPersonTypes = "";
			String tempPersonIdTypes = "";
			infoTO.setPersonTypeAllocations(new ArrayList<PersonTypeAllocation>(
					person.getPersonTypeAllocations()));
			for (PersonTypeAllocation personTypeAllocation : person
					.getPersonTypeAllocations()) {
				tempPersonTypes += personTypeAllocation.getLookupDetail()
						.getDisplayName() + ",";
				tempPersonIdTypes += personTypeAllocation.getLookupDetail()
						.getDataId() + ",";
			}
			infoTO.setPersonTypes(tempPersonTypes);
			infoTO.setPersonTypeIds(tempPersonIdTypes);
		}

		if (person.getCompany() != null
				&& person.getCompany().getCompanyId() != null)
			infoTO.setCompanyId(person.getCompany().getCompanyId() + "");

		return infoTO;
	}

	public static List<HrPersonalDetailsTO> convertIdentityToTOList(
			List<Identity> identitys) throws Exception {

		List<HrPersonalDetailsTO> tosList = new ArrayList<HrPersonalDetailsTO>();

		for (Identity identity : identitys) {
			tosList.add(convertIdentityToTO(identity));
		}

		return tosList;
	}

	public static HrPersonalDetailsTO convertIdentityToTO(Identity identity)
			throws Exception {
		HrPersonalDetailsTO infoTO = new HrPersonalDetailsTO();

		List<HrPersonalDetailsTO> identityTypeList = identificationTypeList();
		infoTO.setIdentityId(identity.getIdentityId());
		infoTO.setIdentityType(identity.getIdentityType());
		for (HrPersonalDetailsTO hrPersonalDetailsTO : identityTypeList) {
			if (hrPersonalDetailsTO.getIdentityType() == identity
					.getIdentityType())
				infoTO.setIdentityTypeName(hrPersonalDetailsTO
						.getIdentityTypeName());
		}
		infoTO.setIdentityNumber(identity.getIdentityNumber());
		infoTO.setIssuedPlace(identity.getIssuedPlace());
		if (identity.getExpireDate() != null) {
			infoTO.setExpireDate(DateFormat.convertDateToString(identity
					.getExpireDate().toString()));
			infoTO.setDaysLeft(DateFormat.dayVariance(new Date(),
					identity.getExpireDate())
					+ "");
		}
		infoTO.setDescription(identity.getDescription());
		return infoTO;
	}

	public static HrPersonalDetailsTO convertIdentityToTO(
			HrPersonalDetailsTO infoTO, Identity identity) throws Exception {

		List<HrPersonalDetailsTO> identityTypeList = identificationTypeList();
		infoTO.setIdentityId(identity.getIdentityId());
		infoTO.setIdentityType(identity.getIdentityType());
		for (HrPersonalDetailsTO hrPersonalDetailsTO : identityTypeList) {
			if (hrPersonalDetailsTO.getIdentityType() == identity
					.getIdentityType())
				infoTO.setIdentityTypeName(hrPersonalDetailsTO
						.getIdentityTypeName());
		}
		infoTO.setIdentityNumber(identity.getIdentityNumber());
		infoTO.setIssuedPlace(identity.getIssuedPlace());
		if (identity.getExpireDate() != null) {
			infoTO.setExpireDate(DateFormat.convertDateToString(identity
					.getExpireDate().toString()));
			infoTO.setDaysLeft(DateFormat.dayVariance(new Date(),
					identity.getExpireDate())
					+ "");
		}
		infoTO.setDescription(identity.getDescription());
		return infoTO;
	}

	public static List<HrPersonalDetailsTO> convertDependentToTOList(
			List<Dependent> dependents) throws Exception {

		List<HrPersonalDetailsTO> tosList = new ArrayList<HrPersonalDetailsTO>();

		for (Dependent dependent : dependents) {
			tosList.add(convertDependentToTO(dependent));
		}

		return tosList;
	}

	public static HrPersonalDetailsTO convertDependentToTO(Dependent dependent)
			throws Exception {
		HrPersonalDetailsTO infoTO = new HrPersonalDetailsTO();
		infoTO.setDependentId(dependent.getDependentId());
		infoTO.setRelationshipId(dependent.getRelationship());
		List<HrPersonalDetailsTO> dependentTypeList = dependentTypeList();
		infoTO.setDependentName(dependent.getName());
		for (HrPersonalDetailsTO hrPersonalDetailsTO : dependentTypeList) {
			if (hrPersonalDetailsTO.getRelationshipId() == dependent
					.getRelationship())
				infoTO.setRelationship(hrPersonalDetailsTO.getRelationship());
		}
		infoTO.setDependentAddress(dependent.getAddress());
		if (dependent.getMobile() != null)
			infoTO.setDependentMobile(dependent.getMobile().toString());
		if (dependent.getTelephone() != null)
			infoTO.setDependentPhone(dependent.getTelephone().toString());
		infoTO.setDependentMail(dependent.getEmail());
		infoTO.setEmergency(dependent.getEmergency());
		infoTO.setDependentflag(dependent.getDependent());
		infoTO.setBenefit(dependent.getBenefit());
		infoTO.setDependentDescription(dependent.getDescription());
		return infoTO;
	}

	public static List<HrPersonalDetailsTO> identificationTypeList()
			throws Exception {
		List<HrPersonalDetailsTO> identificationTypeList = new ArrayList<HrPersonalDetailsTO>();
		HrPersonalDetailsTO typeTo = null;
		typeTo = new HrPersonalDetailsTO();
		typeTo.setIdentityTypeName("National ID");
		typeTo.setIdentityType(1);
		identificationTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setIdentityTypeName("Labour ID");
		typeTo.setIdentityType(2);
		identificationTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setIdentityTypeName("Passport");
		typeTo.setIdentityType(3);
		identificationTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setIdentityTypeName("Insurance Card");
		typeTo.setIdentityType(4);
		identificationTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setIdentityTypeName("Residence Visa");
		typeTo.setIdentityType(5);
		identificationTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setIdentityTypeName("Driving License");
		typeTo.setIdentityType(6);
		identificationTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setIdentityTypeName("Labour Contract");
		typeTo.setIdentityType(7);
		identificationTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setIdentityTypeName("Company Contract");
		typeTo.setIdentityType(8);
		identificationTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setIdentityTypeName("Employeement Visa");
		typeTo.setIdentityType(9);
		identificationTypeList.add(typeTo);

		return identificationTypeList;
	}

	public static List<HrPersonalDetailsTO> dependentTypeList()
			throws Exception {
		List<HrPersonalDetailsTO> dependnetTypeList = new ArrayList<HrPersonalDetailsTO>();
		HrPersonalDetailsTO typeTo = null;
		typeTo = new HrPersonalDetailsTO();
		typeTo.setRelationshipId(1);
		typeTo.setRelationship("Father");
		dependnetTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setRelationshipId(2);
		typeTo.setRelationship("Mother");
		dependnetTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setRelationshipId(3);
		typeTo.setRelationship("Brother");
		dependnetTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setRelationshipId(4);
		typeTo.setRelationship("Sister");
		dependnetTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setRelationshipId(5);
		typeTo.setRelationship("Uncle");
		dependnetTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setRelationshipId(6);
		typeTo.setRelationship("Son");
		dependnetTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setRelationshipId(7);
		typeTo.setRelationship("Daughter");
		dependnetTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setRelationshipId(8);
		typeTo.setRelationship("Wife");
		dependnetTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setRelationshipId(9);
		typeTo.setRelationship("Husband");
		dependnetTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setRelationshipId(10);
		typeTo.setRelationship("Adopted Son");
		dependnetTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setRelationshipId(11);
		typeTo.setRelationship("Adopted Daughter");
		dependnetTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setRelationshipId(12);
		typeTo.setRelationship("Gaurdian");
		dependnetTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setRelationshipId(13);
		typeTo.setRelationship("Aunty");
		dependnetTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setRelationshipId(14);
		typeTo.setRelationship("Grand Father");
		dependnetTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setRelationshipId(16);
		typeTo.setRelationship("Grand Mother");
		dependnetTypeList.add(typeTo);

		typeTo = new HrPersonalDetailsTO();
		typeTo.setRelationshipId(17);
		typeTo.setRelationship("Other");
		dependnetTypeList.add(typeTo);

		return dependnetTypeList;
	}
}
