package com.aiotech.aios.hr.to.converter;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.CompanyTypeAllocation;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.to.CompanyTO;
import com.aiotech.aios.hr.to.HrPersonalDetailsTO;


public class CompanyTOConvert {

	public static List<CompanyTO> convertToTOList(
			List<Company> companys) throws ParseException, UnsupportedEncodingException {

		List<CompanyTO> tosList = new ArrayList<CompanyTO>();
		for (Company cals : companys) {
			tosList.add(convertToTO(cals));
		}

		return tosList;
	}

	public static CompanyTO convertToTO(Company company) throws ParseException {
		
		CompanyTO infoTO = new CompanyTO();
		infoTO.setCompanyId(company.getCompanyId());
		infoTO.setCompanyName(company.getCompanyName());
		if(company.getCompanyNameArabic() != null)
			infoTO.setCompanyNameArabic(AIOSCommons.bytesToUTFString(company.getCompanyNameArabic()));
		infoTO.setDescription(company.getDescription());
		//infoTO.setCompanyTypeId(company.getCompanyType());
		infoTO.setCompanyName(company.getCompanyName());
		infoTO.setCompanyFax(company.getCompanyFax());
		infoTO.setCompanyPhone(company.getCompanyPhone());
		infoTO.setCompanyPobox(company.getCompanyPobox());
		infoTO.setCompanyAddress(company.getCompanyAddress());	
		infoTO.setCompanyOrigin(company.getCompanyOrigin());
		if(company.getTradeNo()!=null)
			infoTO.setTradeName(company.getTradeNo());
		if(company.getTradeNoIssueDate()!=null)
			infoTO.setTradeNoIssueDate(DateFormat.convertDateToString(company.getTradeNoIssueDate().toString()));
		if(company.getBankAccount()!=null){
			infoTO.setAccountNumber(company.getBankAccount().getAccountNumber());
			infoTO.setBankAccountId(company.getBankAccount().getBankAccountId()+"");
		}
		infoTO.setLabourCardId(company.getLabourCardId());
		if(company.getCompanyTypeAllocations()!=null){
			String types="";
			String ids="";
			for (CompanyTypeAllocation companyTypeAllocation : company.getCompanyTypeAllocations()) {
				if(companyTypeAllocation.getLookupDetail()!=null && companyTypeAllocation.getLookupDetail().getLookupDetailId()!=null){
					types=types+companyTypeAllocation.getLookupDetail().getDisplayName()+",";
					ids=ids+companyTypeAllocation.getLookupDetail().getDataId()+",";
				}
			}
			infoTO.setCompanyTypes(types);
			infoTO.setCompanyTypeDataIds(ids);
		}
			
		return infoTO;
	}
	

}
