package com.aiotech.aios.hr.to;

public class SupplierTO {

	private Long supplierId;
	private String supplierName;
	private String supplierType;
	private String supplierAddress;
	private String supplierContact;
	private String supplierStatus;
	private String supplierActiveSince;
	private String supplierFax;
	private String supplierDescription;
	private String supplierOrigin;
	
	public Long getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}	
	public String getSupplierOrigin() {
		return supplierOrigin;
	}
	public void setSupplierOrigin(String supplierOrigin) {
		this.supplierOrigin = supplierOrigin;
	}
	public String getSupplierFax() {
		return supplierFax;
	}
	public String getSupplierDescription() {
		return supplierDescription;
	}
	public void setSupplierFax(String supplierFax) {
		this.supplierFax = supplierFax;
	}
	public void setSupplierDescription(String supplierDescription) {
		this.supplierDescription = supplierDescription;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public String getSupplierType() {
		return supplierType;
	}
	public String getSupplierAddress() {
		return supplierAddress;
	}
	public String getSupplierContact() {
		return supplierContact;
	}
	public String getSupplierStatus() {
		return supplierStatus;
	}
	public String getSupplierActiveSince() {
		return supplierActiveSince;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public void setSupplierType(String supplierType) {
		this.supplierType = supplierType;
	}
	public void setSupplierAddress(String supplierAddress) {
		this.supplierAddress = supplierAddress;
	}
	public void setSupplierContact(String supplierContact) {
		this.supplierContact = supplierContact;
	}
	public void setSupplierStatus(String supplierStatus) {
		this.supplierStatus = supplierStatus;
	}
	public void setSupplierActiveSince(String supplierActiveSince) {
		this.supplierActiveSince = supplierActiveSince;
	}
}
