package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.hr.domain.entity.CandidateOffer;
import com.aiotech.aios.hr.domain.entity.Job;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobLeave;
import com.aiotech.aios.hr.domain.entity.JobPayrollElement;
import com.aiotech.aios.hr.domain.entity.JobShift;
import com.aiotech.aios.hr.domain.entity.vo.CandidateVO;
import com.aiotech.aios.hr.domain.entity.vo.LocationVO;
import com.aiotech.aios.hr.service.CandidateOfferService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class CandidateOfferBL {
	private CandidateOfferService candidateOfferService;
	private CandidateBL candidateBL;
	private InterviewBL interviewBL;
	private JobBL jobBL;
	private LocationBL locationBL;
	private CommentBL commentBL;
	private JobAssignmentBL jobAssignmentBL;
	private WorkflowEnterpriseService workflowEnterpriseService;

	public CandidateVO convertEntityIntoVO(CandidateOffer candidateOffer) {
		CandidateVO candidateVO = null;
		try {
			candidateVO = candidateBL.convertEntityIntoVO(candidateOffer
					.getCandidate());
			candidateVO.setCandidateOfferId(candidateOffer
					.getCandidateOfferId());
			candidateVO.setCandidateOffer(candidateOffer);
			Job job = null;
			if (candidateOffer.getJob() != null) {
				job = jobBL.getJobService().getJobInfo(
						candidateOffer.getJob().getJobId());
				candidateVO.setJobVO(jobBL.convertEntityToVO(job));
				if (candidateVO.getJobVO().getJobPayrollElements() != null
						&& candidateVO.getJobVO().getJobPayrollElements()
								.size() > 0) {
					List<CandidateVO> payrElements = new ArrayList<CandidateVO>();
					CandidateVO payElment = null;
					for (JobPayrollElement jobPayrollElement : candidateVO
							.getJobVO().getJobPayrollElements()) {
						payElment = new CandidateVO();
						payElment.setElementName(jobPayrollElement
								.getPayrollElementByPayrollElementId()
								.getElementName());
						payElment.setAmount(jobPayrollElement.getAmount() + "");
						payrElements.add(payElment);
					}

					candidateVO.setSalaryElements(payrElements);
				}

				if (candidateVO.getJobVO().getJobLeaves() != null
						&& candidateVO.getJobVO().getJobLeaves().size() > 0) {
					List<CandidateVO> leaves = new ArrayList<CandidateVO>();
					CandidateVO leave = null;
					for (JobLeave jobLeave : candidateVO.getJobVO()
							.getJobLeaves()) {
						leave = new CandidateVO();
						leave.setLeaveType(jobLeave.getLeave()
								.getLookupDetail().getDisplayName());
						leave.setDays((jobLeave.getLeave().getDefaultDays() != null ? jobLeave
								.getLeave().getDefaultDays() : "")
								+ "");
						if (jobLeave.getLeave().getIsYearly() != null
								&& jobLeave.getLeave().getIsYearly())
							leave.setTerm("Yearly");
						else if (jobLeave.getLeave().getIsMonthly() != null
								&& jobLeave.getLeave().getIsMonthly())
							leave.setTerm("Monthly");
						else
							leave.setTerm("InService");
						leaves.add(leave);
					}

					candidateVO.setLeaves(leaves);
				}
				if (candidateVO.getJobVO().getJobShifts() != null
						&& candidateVO.getJobVO().getJobShifts().size() > 0) {
					List<CandidateVO> shifts = new ArrayList<CandidateVO>();
					CandidateVO shift = null;
					for (JobShift jobShift : candidateVO.getJobVO()
							.getJobShifts()) {
						if (jobShift.getIsActive() == null
								|| !jobShift.getIsActive())
							continue;

						shift = new CandidateVO();
						shift.setStartTime(jobShift.getWorkingShift()
								.getStartTime() + "");
						shift.setEndTime(jobShift.getWorkingShift()
								.getEndTime() + "");
						shift.setIrregularDay(jobShift.getWorkingShift()
								.getIrregularDay());
						shifts.add(shift);
					}

					candidateVO.setShifts(shifts);
				}
				if (candidateVO.getOpenPosition().getCmpDeptLocation()
						.getLocation() != null) {
					LocationVO locationVO = locationBL
							.getLocationFullAddress(candidateVO
									.getOpenPosition().getCmpDeptLocation()
									.getLocation().getLocationId());

					candidateVO.setAddress(locationVO.getFullAddress());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return candidateVO;
	}

	public String saveCandidateOffer(CandidateOffer candidateOffer) {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			candidateOfferService.saveCandidateOffer(candidateOffer);

			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					"CandidateOffer", candidateOffer.getCandidateOfferId(),
					user, workflowDetailVo);

			return "SUCCESS";
		} catch (Exception e) {
			return "ERROR";
		}
	}

	public void doCandidateOfferBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		CandidateOffer candidateOffer = this.getCandidateOfferService()
				.getCandiateOfferById(recordId);
		if (workflowDetailVO.isDeleteFlag()) {

		} else {
			candidateOffer.getCandidate().setStatus(
					(byte) Constants.HR.InterviewStatus.Offered.getCode());
			candidateBL.saveCandidate(candidateOffer.getCandidate());
		}
	}

	public String finaliseCandidateOffer(CandidateOffer candidateOffer,
			List<JobAssignment> jobAssignmentsTemp) {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Long tempJobAssignmentId = null;
			if (candidateOffer != null
					&& candidateOffer.getJobAssignment() != null
					&& candidateOffer.getJobAssignment().getIsActive()) {
				JobAssignment jobAssignment = candidateOffer.getJobAssignment();
				jobAssignmentBL.getJobAssignmentService().saveJobAssignment(
						jobAssignment);
				candidateOffer.setJobAssignment(jobAssignment);
			}

			candidateOfferService.saveCandidateOffer(candidateOffer);

			if (candidateOffer != null
					&& candidateOffer.getJobAssignment() != null
					&& !candidateOffer.getJobAssignment().getIsActive()) {
				JobAssignment jobAssignmentId = new JobAssignment();
				jobAssignmentId.setJobAssignmentId(tempJobAssignmentId);
				jobAssignmentBL.deleteJobAssignment(jobAssignmentId);

			}

			if (candidateOffer != null && candidateOffer.getCandidate() != null) {
				candidateBL.saveCandidate(candidateOffer.getCandidate());
			}

			return "SUCCESS";
		} catch (Exception e) {
			return "ERROR";
		}
	}

	public CandidateOfferService getCandidateOfferService() {
		return candidateOfferService;
	}

	public void setCandidateOfferService(
			CandidateOfferService candidateOfferService) {
		this.candidateOfferService = candidateOfferService;
	}

	public CandidateBL getCandidateBL() {
		return candidateBL;
	}

	public void setCandidateBL(CandidateBL candidateBL) {
		this.candidateBL = candidateBL;
	}

	public InterviewBL getInterviewBL() {
		return interviewBL;
	}

	public void setInterviewBL(InterviewBL interviewBL) {
		this.interviewBL = interviewBL;
	}

	public JobBL getJobBL() {
		return jobBL;
	}

	public void setJobBL(JobBL jobBL) {
		this.jobBL = jobBL;
	}

	public LocationBL getLocationBL() {
		return locationBL;
	}

	public void setLocationBL(LocationBL locationBL) {
		this.locationBL = locationBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public JobAssignmentBL getJobAssignmentBL() {
		return jobAssignmentBL;
	}

	public void setJobAssignmentBL(JobAssignmentBL jobAssignmentBL) {
		this.jobAssignmentBL = jobAssignmentBL;
	}

}
