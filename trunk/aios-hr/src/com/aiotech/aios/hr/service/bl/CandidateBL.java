package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.Candidate;
import com.aiotech.aios.hr.domain.entity.CandidateOffer;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.CandidateVO;
import com.aiotech.aios.hr.domain.entity.vo.PersonVO;
import com.aiotech.aios.hr.service.CandidateOfferService;
import com.aiotech.aios.hr.service.CandidateService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class CandidateBL {

	private CandidateService candidateService;
	private LookupMasterBL lookupMasterBL;
	private Implementation implementation;
	private CandidateOfferService candidateOfferService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	@SuppressWarnings("unused")
	public List<CandidateVO> convertAllEntityToVO(List<Candidate> candidates)
			throws Exception {
		List<CandidateVO> candidateVOs = null;
		if (candidates != null && candidates.size() > 0) {
			candidateVOs = new ArrayList<CandidateVO>();
			for (Candidate candidate : candidates) {
				candidateVOs.add(convertEntityIntoVO(candidate));
			}
		}
		return candidateVOs;
		}
	
	public CandidateVO convertEntityIntoVO(Candidate candidate)
			throws Exception {
		CandidateVO candidateVO = new CandidateVO();
		// Copy EmployeeLoan class properties value into VO properties.
		BeanUtils.copyProperties(candidateVO, candidate);
		if (candidate.getCreatedDate() != null)
			candidateVO.setCreatedDateDisplay(DateFormat
					.convertDateToString(candidate.getCreatedDate() + ""));
		if (candidate.getExpectedJoiningDate() != null)
			candidateVO.setExpectedJoiningDateDisplay(DateFormat
					.convertDateToString(candidate.getExpectedJoiningDate()
							+ ""));
		PersonVO personVO=new PersonVO();
		candidateVO.setPersonVO(personVO);
		candidateVO.getPersonVO().setPersonName(
				candidate.getPersonByPersonId().getFirstName() + ""
						+ candidate.getPersonByPersonId().getLastName());
		candidateVO.getPersonVO().setPersonNumber(
				candidate.getPersonByPersonId().getPersonNumber());
		return candidateVO;
	}
	
	public void sendForOfferLetter(Long candidateId, byte status) {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		Person person = new Person();
		person.setPersonId(user.getPersonId());
		updateCandidateStatus(candidateId, status);
		CandidateOffer candidateOffer = new CandidateOffer();
		Candidate candidate = new Candidate();
		candidate.setCandidateId(candidateId);
		candidateOffer.setCandidate(candidate);
		candidateOffer.setIsApprove((byte) WorkflowConstants.Status.NoDecession
				.getCode());
		candidateOffer.setCreatedDate(new Date());
		candidateOffer.setPerson(person);
		candidateOffer.setImplementation(getImplementation());
		candidateOfferService.saveCandidateOffer(candidateOffer);
	}
	
	public void updateCandidateStatus(Long candidateId, byte status) {
		try {
			Candidate candidate = candidateService.getCandiateById(candidateId);
			if (candidate != null) {
				candidate.setStatus(status);
				candidateService.saveCandidate(candidate);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	

	public Map<Integer, String> statusType() {
		Map<Integer, String> types = new HashMap<Integer, String>();
		types.put(Constants.HR.InterviewStatus.Initiated.getCode(), "Initiated");
		types.put(Constants.HR.InterviewStatus.Scheduled.getCode(), "Scheduled");
		types.put(Constants.HR.InterviewStatus.Interviewed.getCode(),
				"Interviewed");
		types.put(Constants.HR.InterviewStatus.ShortListed.getCode(),
				"Short Listed");
		types.put(Constants.HR.InterviewStatus.Offered.getCode(), "Offered");
		types.put(Constants.HR.InterviewStatus.OfferAccepted.getCode(),
				"Offer Accepted");
		types.put(Constants.HR.InterviewStatus.JobAssigned.getCode(),
				"Job Assigned");
		types.put(Constants.HR.InterviewStatus.OfferDenied.getCode(),
				"Offer Denied");
		types.put(Constants.HR.InterviewStatus.Holded.getCode(), "Holded");
		types.put(Constants.HR.InterviewStatus.Rejected.getCode(), "Rejected");
		return types;
	}
	
	
	
	public void saveCandidate(Candidate candidate) throws Exception{
		Map<String, Object> sessionObj = ActionContext.getContext()
		.getSession();
		User user = (User) sessionObj.get("USER");

		candidate.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (candidate != null
				&& candidate.getCandidateId() != null
				&& candidate.getCandidateId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		candidateService.saveCandidate(candidate);
		
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Candidate.class.getSimpleName(),
				candidate.getCandidateId(), user,
				workflowDetailVo);
	}

	public void doCandidateBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		
		if (workflowDetailVO.isDeleteFlag()) {
		} else {
		}
	}
	public CandidateService getCandidateService() {
		return candidateService;
	}

	public void setCandidateService(CandidateService candidateService) {
		this.candidateService = candidateService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public CandidateOfferService getCandidateOfferService() {
		return candidateOfferService;
	}

	public void setCandidateOfferService(CandidateOfferService candidateOfferService) {
		this.candidateOfferService = candidateOfferService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

}
