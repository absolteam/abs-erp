package com.aiotech.aios.hr.service.bl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;
import org.joda.time.LocalDate;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.OpenPosition;
import com.aiotech.aios.hr.domain.entity.PromotionDemotion;
import com.aiotech.aios.hr.domain.entity.QuestionBank;
import com.aiotech.aios.hr.domain.entity.QuestionInfo;
import com.aiotech.aios.hr.domain.entity.vo.PromotionDemotionVO;
import com.aiotech.aios.hr.service.OpenPositionService;
import com.aiotech.aios.hr.service.PromotionDemotionService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class PromotionDemotionBL {

	private PromotionDemotionService promotionDemotionService;
	private OpenPositionService openPositionService;
	private PayPeriodBL payPeriodBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private JobAssignmentBL jobAssignmentBL;
	private Implementation implementation;
	private CommentBL commentBL;

	public PromotionDemotionVO convertEntityToVO(
			PromotionDemotion promotionDemotion) {
		PromotionDemotionVO promotionVO = new PromotionDemotionVO();
		try {
			// Copy Entity class properties value into VO properties.
			BeanUtils.copyProperties(promotionVO, promotionDemotion);

			if (promotionDemotion.getFromDate() != null)
				promotionVO.setFromDateView(DateFormat
						.convertDateToString(promotionDemotion.getFromDate()
								+ ""));

			if (promotionDemotion.getToDate() != null)
				promotionVO
						.setToDateView(DateFormat
								.convertDateToString(promotionDemotion
										.getToDate() + ""));

			if (promotionDemotion.getEffectiveDate() != null)
				promotionVO.setEffectiveDateView(DateFormat
						.convertDateToString(promotionDemotion
								.getEffectiveDate() + ""));

			if (promotionDemotion.getCreatedDate() != null)
				promotionVO.setCreatedDateView(DateFormat
						.convertDateToString(promotionDemotion.getCreatedDate()
								+ ""));

			if (promotionDemotion.getRequestedDate() != null)
				promotionVO.setRequestedDateView(DateFormat
						.convertDateToString(promotionDemotion
								.getRequestedDate() + ""));
			Map<Byte, String> maps = promotionType();
			promotionVO.setTypeView(maps.get(promotionDemotion.getType()));

			JobAssignment jobAssigment = jobAssignmentBL
					.getJobAssignmentService().getJobAssignmentInfo(
							promotionDemotion.getJobAssignment()
									.getJobAssignmentId());

			promotionVO.setPersonNumber(jobAssigment.getPerson()
					.getPersonNumber());
			promotionVO.setPersonName(jobAssigment.getPerson().getFirstName()
					+ " " + jobAssigment.getPerson().getLastName());
			promotionVO.setCompanyName(jobAssigment.getCmpDeptLocation()
					.getCompany().getCompanyName());
			promotionVO.setDesignationName(jobAssigment.getDesignation()
					.getDesignationName());
			promotionVO.setDepartmentName(jobAssigment.getCmpDeptLocation()
					.getDepartment().getDepartmentName());
			promotionVO.setLocationName(jobAssigment.getCmpDeptLocation()
					.getLocation().getLocationName());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return promotionVO;
	}

	public String savePromotionDemotion(PromotionDemotion promotionDemotion) {
		String returnStatus = "SUCCESS";
		try {
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (promotionDemotion != null
					&& promotionDemotion.getPromotionDemotionId() != null
					&& promotionDemotion.getPromotionDemotionId() > 0) {

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			promotionDemotionService.savePromotionDemotion(promotionDemotion);

			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					"PromotionDemotion",
					promotionDemotion.getPromotionDemotionId(), user,
					workflowDetailVo);

		} catch (Exception e) {
			e.printStackTrace();
			returnStatus = "ERROR";
		}
		return returnStatus;
	}

	public void autoJobAssignment(Implementation imp) {
		try {
			List<Implementation> implementations = new ArrayList<Implementation>();
			if (imp != null) {
				implementations.add(imp);
			} else {
				implementations = jobAssignmentBL.getJobBL().getSystemService()
				.getAllImplementation();
			}
			for (Implementation implementation : implementations) {
				List<PromotionDemotion> promotionDemotions = promotionDemotionService
						.getAllPromotionDemotionToActivate(implementation);
				JobAssignment jobAssignment = null;
				JobAssignment newJobAssignment = null;
				OpenPosition oposition = null;
				if (promotionDemotions != null && promotionDemotions.size() > 0) {
					for (PromotionDemotion promotionDemotion : promotionDemotions) {
						newJobAssignment = new JobAssignment();
						oposition= new OpenPosition();
						jobAssignment= new JobAssignment();
						LocalDate effectiveDate = new LocalDate(
								promotionDemotion.getEffectiveDate());
						LocalDate currentDate = new LocalDate();
						if (effectiveDate.equals(currentDate)
								|| effectiveDate.isBefore(currentDate)) {
							jobAssignment = jobAssignmentBL
									.getJobAssignmentService()
									.getJobAssignmentInfo(
											promotionDemotion
													.getJobAssignment()
													.getJobAssignmentId());
							if (promotionDemotion.getOpenPosition() != null)
								oposition = openPositionService
										.getOpenPositionById(promotionDemotion
												.getOpenPosition()
												.getOpenPositionId());

							newJobAssignment.setPersonBank(jobAssignment
									.getPersonBank());
							newJobAssignment.setSwipeId(jobAssignment
									.getSwipeId());
							newJobAssignment.setIsLocalPay(jobAssignment
									.getIsLocalPay());
							newJobAssignment
									.setJobAssignmentNumber(jobAssignment
											.getJobAssignmentNumber());
							newJobAssignment.setPayMode(jobAssignment
									.getPayMode());
							newJobAssignment.setPayPolicy(jobAssignment
									.getPayPolicy());
							newJobAssignment.setJobAssignmentId(null);
							newJobAssignment.setJob(promotionDemotion.getJob());
							if (oposition != null
									&& oposition.getDesignation() != null) {
								newJobAssignment
										.setDesignation(oposition
												.getDesignation());
								newJobAssignment
										.setCmpDeptLocation(oposition
												.getCmpDeptLocation());
							} else {
								newJobAssignment.setDesignation(jobAssignment
										.getDesignation());
								newJobAssignment
										.setCmpDeptLocation(jobAssignment
												.getCmpDeptLocation());
							}
							newJobAssignment.setImplementation(jobAssignment
									.getImplementation());
							newJobAssignment.setLookupKey(jobAssignment
									.getLookupKey());
							newJobAssignment.setEffectiveDate(effectiveDate
									.toDate());

							newJobAssignment.setIsActive(true);
							newJobAssignment.setIsPrimary(true);
							newJobAssignment.setPerson(jobAssignment
									.getPerson());
							jobAssignmentBL.getJobAssignmentService()
									.saveJobAssignment(newJobAssignment);

							jobAssignment.setIsActive(false);
							jobAssignment.setIsPrimary(false);
							jobAssignment.setEndDate(effectiveDate.minusDays(1)
									.toDate());
							jobAssignmentBL.getJobAssignmentService()
									.saveJobAssignmentFromPromotion(
											jobAssignment);

							promotionDemotion
									.setStatus(Constants.HR.PromotionStatus.Activated
											.getCode());
							promotionDemotionService
									.savePromotionDemotion(promotionDemotion);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deletePromotionDemotion(PromotionDemotion promotionDemotion)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				PromotionDemotion.class.getSimpleName(),
				promotionDemotion.getPromotionDemotionId(), user,
				workflowDetailVo);
	}

	public void doPromotionDemotionBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		PromotionDemotion promotionDemotion = promotionDemotionService
				.getPromotionDemotionById(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			promotionDemotionService.deletePromotionDemotion(promotionDemotion);
		} else {
			autoJobAssignment(getImplementation());
		}
	}

	public Map<Byte, String> promotionType() {
		Map<Byte, String> types = new HashMap<Byte, String>();
		types.put(Constants.HR.PromotionType.Promotion.getCode(),
				Constants.HR.PromotionType.Promotion.name());
		types.put(Constants.HR.PromotionType.Demotion.getCode(),
				Constants.HR.PromotionType.Demotion.name());
		types.put(
				Constants.HR.PromotionType.PayIncrement.getCode(),
				Constants.HR.PromotionType.PayIncrement.name()
						.replaceAll("\\d+", "")
						.replaceAll("(.)([A-Z])", "$1 $2"));
		types.put(
				Constants.HR.PromotionType.PayDecrement.getCode(),
				Constants.HR.PromotionType.PayDecrement.name()
						.replaceAll("\\d+", "")
						.replaceAll("(.)([A-Z])", "$1 $2"));
		return types;
	}

	public PayPeriodBL getPayPeriodBL() {
		return payPeriodBL;
	}

	public void setPayPeriodBL(PayPeriodBL payPeriodBL) {
		this.payPeriodBL = payPeriodBL;
	}

	public PromotionDemotionService getPromotionDemotionService() {
		return promotionDemotionService;
	}

	public void setPromotionDemotionService(
			PromotionDemotionService promotionDemotionService) {
		this.promotionDemotionService = promotionDemotionService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public JobAssignmentBL getJobAssignmentBL() {
		return jobAssignmentBL;
	}

	public void setJobAssignmentBL(JobAssignmentBL jobAssignmentBL) {
		this.jobAssignmentBL = jobAssignmentBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public OpenPositionService getOpenPositionService() {
		return openPositionService;
	}

	public void setOpenPositionService(OpenPositionService openPositionService) {
		this.openPositionService = openPositionService;
	}

}
