package com.aiotech.aios.hr.service;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AttendancePolicyService {
	public AIOTechGenericDAO<AttendancePolicy> attendancePolicyDAO;
	
	public List<AttendancePolicy> getAttendancePolicyList(Implementation implementation) {
		return attendancePolicyDAO.findByNamedQuery("getAttendancePolicyList", implementation,
				(byte)WorkflowConstants.Status.Published.getCode());
	}
	
	public AttendancePolicy getAttendancePolicyInfo(Long attendancePolicyId) {
		List<AttendancePolicy> attendancePolicies=new ArrayList<AttendancePolicy>();
		attendancePolicies=attendancePolicyDAO.findByNamedQuery("getAttendancePolicyDetail", attendancePolicyId);
		if(attendancePolicies!=null && attendancePolicies.size()>0)
			return attendancePolicies.get(0);
		else
			return null;
	}
	
	public void saveAttendancePolicy(AttendancePolicy attendacePolicy){
		attendancePolicyDAO.saveOrUpdate(attendacePolicy);
	}
	
	public void deleteAttendancePolicy(AttendancePolicy attendacePolicy){
		attendancePolicyDAO.delete(attendacePolicy);
	}
	
	public AIOTechGenericDAO<AttendancePolicy> getAttendancePolicyDAO() {
		return attendancePolicyDAO;
	}
	public void setAttendancePolicyDAO(
			AIOTechGenericDAO<AttendancePolicy> attendancePolicyDAO) {
		this.attendancePolicyDAO = attendancePolicyDAO;
	}
}
