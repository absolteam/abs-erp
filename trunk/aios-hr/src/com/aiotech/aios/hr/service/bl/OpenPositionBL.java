package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.OpenPosition;
import com.aiotech.aios.hr.domain.entity.vo.OpenPositionVO;
import com.aiotech.aios.hr.service.OpenPositionService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class OpenPositionBL {

	private OpenPositionService openPositionService;
	private JobAssignmentBL jobAssignmentBL;
	private LookupMasterBL lookupMasterBL;
	private Implementation implementation;
	private DocumentBL documentBL;
	private ImageBL imageBL;
	private DirectoryBL directoryBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public String getRefereceNum() {
		String referenceStamp = SystemBL.getReferenceStamp(
				OpenPosition.class.getName(), getImplementation());
		return referenceStamp;
	}

	public String saveReferenceNum() {
		String referenceStamp = SystemBL.saveReferenceStamp(
				OpenPosition.class.getName(), getImplementation());
		return referenceStamp;
	}

	public void saveDocuments(OpenPosition openPosition, boolean editFlag)
			throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(openPosition,
				"uploadedDocs", editFlag);
		Map<String, String> dirs = docVO.getDirMap();

		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, openPosition.getOpenPositionId(), "OpenPosition",
					"uploadedDocs");
			docVO.setDirStruct(dirStucture);
		}

		documentBL.saveUploadDocuments(docVO);
	}

	public void saveOpenPosition(OpenPosition openPosition) {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			openPosition.setIsApprove((byte) WorkflowConstants.Status.Published
					.getCode());
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (openPosition != null
					&& openPosition.getOpenPositionId() != null
					&& openPosition.getOpenPositionId() > 0) {

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			if (openPosition != null
					&& openPosition.getOpenPositionId() != null
					&& openPosition.getOpenPositionId() > 0) {

				openPositionService.saveOpenPosition(openPosition);
				saveDocuments(openPosition, true);

			} else {
				openPositionService.saveOpenPosition(openPosition);
				saveDocuments(openPosition, false);
			}

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					OpenPosition.class.getSimpleName(),
					openPosition.getOpenPositionId(), user, workflowDetailVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteOpenPosition(OpenPosition openPosition) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				OpenPosition.class.getSimpleName(),
				openPosition.getOpenPositionId(), user, workflowDetailVo);
	}

	public void doOpenPositionBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		OpenPosition openPosition = openPositionService
				.getOpenPositionById(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			openPositionService.deleteOpenPosition(openPosition);
		} else {
		}
	}

	public Map<String, String> genderType() {
		Map<String, String> types = new HashMap<String, String>();
		types.put("M", "Male");
		types.put("F", "Female");
		types.put("B", "Both");
		return types;
	}

	public Map<Integer, String> statusType() {
		Map<Integer, String> types = new HashMap<Integer, String>();
		types.put(1, "Opened");
		types.put(2, "Suspended");
		types.put(3, "Closed");
		return types;
	}

	public OpenPositionVO findOpenPosition(OpenPosition openPosition) {
		OpenPositionVO openPositionVO = null;
		if (openPosition != null && openPosition.getDesignation() != null
				&& openPosition.getCmpDeptLocation() != null) {
			openPositionVO = new OpenPositionVO();
			OpenPosition existOpenPosition = openPositionService
					.getOpenPositionSameCombination(openPosition
							.getDesignation().getDesignationId(), openPosition
							.getCmpDeptLocation().getCmpDeptLocId());
			if (existOpenPosition != null) {
				openPositionVO.setNumberOfCount(existOpenPosition
						.getNumberOfPosition());
			} else {
				openPositionVO.setNumberOfCount(0);
			}

			// Get the information form Job Assignemnt table to calculate the
			// open Position information
			List<JobAssignment> jobAssignments = jobAssignmentBL
					.getJobAssignmentService()
					.getAllJobAssignmentForOpenPoistion(openPosition);
			if (jobAssignments != null && jobAssignments.size() > 0)
				openPositionVO.setNumberOfCount(openPositionVO
						.getNumberOfCount() - jobAssignments.size());
			else
				openPositionVO.setNumberOfCount(0);
		}
		return openPositionVO;
	}

	@SuppressWarnings("unused")
	public List<?> convertAllEntityToVO(List<OpenPosition> openPositions)
			throws Exception {
		List<OpenPositionVO> openPositionVOs = null;
		if (openPositionVOs != null && openPositionVOs.size() > 0) {
			openPositionVOs = new ArrayList<OpenPositionVO>();
			for (OpenPosition openPosition : openPositions) {
				openPositionVOs.add(convertEntityIntoVO(openPosition));
			}
		}
		return openPositionVOs;
	}

	public OpenPositionVO convertEntityIntoVO(OpenPosition openPosition)
			throws Exception {
		OpenPositionVO openPositionVO = new OpenPositionVO();
		// Copy EmployeeLoan class properties value into VO properties.
		BeanUtils.copyProperties(openPositionVO, openPosition);
		if (openPosition.getCreatedDate() != null)
			openPositionVO.setCreatedDateDisplay(DateFormat
					.convertDateToString(openPosition.getCreatedDate() + ""));
		if (openPosition.getDateOpened() != null)
			openPositionVO.setDateOpenedDisplay(DateFormat
					.convertDateToString(openPosition.getDateOpened() + ""));
		if (openPosition.getTargetDate() != null)
			openPositionVO.setTargetDateDisplay(DateFormat
					.convertDateToString(openPosition.getTargetDate() + ""));
		return openPositionVO;
	}

	public OpenPositionService getOpenPositionService() {
		return openPositionService;
	}

	public void setOpenPositionService(OpenPositionService openPositionService) {
		this.openPositionService = openPositionService;
	}

	public JobAssignmentBL getJobAssignmentBL() {
		return jobAssignmentBL;
	}

	public void setJobAssignmentBL(JobAssignmentBL jobAssignmentBL) {
		this.jobAssignmentBL = jobAssignmentBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
