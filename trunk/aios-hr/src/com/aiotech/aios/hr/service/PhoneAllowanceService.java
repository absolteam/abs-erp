package com.aiotech.aios.hr.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.hr.domain.entity.PhoneAllowance;
import com.aiotech.aios.hr.domain.entity.vo.PhoneAllowanceVO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class PhoneAllowanceService {
	public AIOTechGenericDAO<PhoneAllowance> phoneAllowanceDAO;

	public List<PhoneAllowance> getAllPhoneAllowanceByCriteria(
			PhoneAllowanceVO vO) {
		Criteria jobPayrollCriteria = phoneAllowanceDAO.createCriteria();

		jobPayrollCriteria.createAlias("jobAssignment", "ja");
		jobPayrollCriteria.createAlias("lookupDetailBySubscriptionType",
				"sType", CriteriaSpecification.LEFT_JOIN);
		jobPayrollCriteria.createAlias("lookupDetailByNetworkProvider",
				"nProvider", CriteriaSpecification.LEFT_JOIN);
		jobPayrollCriteria.createAlias("jobPayrollElement", "jpa");
		jobPayrollCriteria.createAlias("jpa.payrollElementByPayrollElementId",
				"pe");
		jobPayrollCriteria.createAlias("jpa.payrollElementByPercentageElement",
				"percentage", CriteriaSpecification.LEFT_JOIN);
		if (vO.getPayPeriodTransaction() != null
				&& vO.getPayPeriodTransaction().getStartDate() != null) {
			jobPayrollCriteria.add(Restrictions.conjunction().add(
					Restrictions.ge("fromDate", vO.getPayPeriodTransaction()
							.getStartDate())));
			jobPayrollCriteria.add(Restrictions.conjunction().add(
					Restrictions.le("toDate", vO.getPayPeriodTransaction()
							.getEndDate())));
		}

		if (vO.getJobAssignmentId() != null) {
			jobPayrollCriteria.add(Restrictions.eq("ja.jobAssignmentId",
					vO.getJobAssignmentId()));
		}

		if (vO.getJobPayrollElementId() != null) {
			jobPayrollCriteria.add(Restrictions.eq("jpa.jobPayrollElementId",
					vO.getJobPayrollElementId()));
		}

		return jobPayrollCriteria.list();
	}

	public List<PhoneAllowance> getAllPhoneAllowance(
			Implementation implementation) {
		return phoneAllowanceDAO.findByNamedQuery("getAllPhoneAllowance",
				implementation);
	}

	public PhoneAllowance getPhoneAllowanceById(Long phoneAllowanceId) {
		List<PhoneAllowance> phoneAllowances = phoneAllowanceDAO
				.findByNamedQuery("getPhoneAllowanceById", phoneAllowanceId);
		if (phoneAllowances != null && phoneAllowances.size() > 0)
			return phoneAllowances.get(0);
		else
			return null;
	}

	public void savePhoneAllowance(PhoneAllowance phoneAllowance) {
		phoneAllowanceDAO.saveOrUpdate(phoneAllowance);
	}

	public void deletePhoneAllowance(PhoneAllowance phoneAllowance) {
		phoneAllowanceDAO.delete(phoneAllowance);
	}

	public AIOTechGenericDAO<PhoneAllowance> getPhoneAllowanceDAO() {
		return phoneAllowanceDAO;
	}

	public void setPhoneAllowanceDAO(
			AIOTechGenericDAO<PhoneAllowance> phoneAllowanceDAO) {
		this.phoneAllowanceDAO = phoneAllowanceDAO;
	}
}
