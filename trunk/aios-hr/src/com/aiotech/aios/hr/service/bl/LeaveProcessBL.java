package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Days;
import org.joda.time.DurationFieldType;
import org.joda.time.LocalDate;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.JodaDateFormat;
import com.aiotech.aios.hr.domain.entity.EmployeeCalendar;
import com.aiotech.aios.hr.domain.entity.EmployeeCalendarPeriod;
import com.aiotech.aios.hr.domain.entity.Job;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobLeave;
import com.aiotech.aios.hr.domain.entity.Leave;
import com.aiotech.aios.hr.domain.entity.LeavePolicy;
import com.aiotech.aios.hr.domain.entity.LeaveProcess;
import com.aiotech.aios.hr.domain.entity.LeaveReconciliation;
import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;
import com.aiotech.aios.hr.domain.entity.Payroll;
import com.aiotech.aios.hr.domain.entity.PayrollElement;
import com.aiotech.aios.hr.domain.entity.PayrollTransaction;
import com.aiotech.aios.hr.domain.entity.vo.LeaveProcessVO;
import com.aiotech.aios.hr.domain.entity.vo.LeaveReconciliationVO;
import com.aiotech.aios.hr.domain.entity.vo.PayPeriodVO;
import com.aiotech.aios.hr.domain.entity.vo.PayrollTransactionVO;
import com.aiotech.aios.hr.domain.entity.vo.PayrollVO;
import com.aiotech.aios.hr.domain.entity.vo.ResignationTerminationVO;
import com.aiotech.aios.hr.service.JobService;
import com.aiotech.aios.hr.service.LeaveProcessService;
import com.aiotech.aios.hr.to.LeaveProcessTO;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class LeaveProcessBL {
	private LeaveProcessService leaveProcessService;
	private JobService jobService;
	private AttendanceBL attendanceBL;
	private JobAssignmentBL jobAssignmentBL;
	private EmployeeCalendarBL employeeCalendarBL;
	private Implementation implementation;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private PersonBL personBL;
	DirectoryBL directoryBL;
	DocumentBL documentBL;
	private PayrollBL payrollBL;

	public void saveLeaveProcess(LeaveProcess leaveProcess) {
		try {
			boolean editFlag = false;
			if (leaveProcess.getLeaveProcessId() != null
					&& leaveProcess.getLeaveProcessId() > 0) {
				editFlag = true;
			}

			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (leaveProcess != null
					&& leaveProcess.getLeaveProcessId() != null
					&& leaveProcess.getLeaveProcessId() > 0) {

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}

			leaveProcessService.saveLeaveProcess(leaveProcess);
			saveLeaveDocuments(leaveProcess, editFlag);
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					leaveProcess.getClass().getSimpleName(),
					leaveProcess.getLeaveProcessId(), user, workflowDetailVo);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void saveLeaveDocuments(LeaveProcess leaveProcess, boolean editFlag)
			throws Exception {

		DocumentVO docVO = documentBL.populateDocumentVO(leaveProcess,
				"leaveDocs", editFlag);
		Map<String, String> dirs = docVO.getDirMap();

		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, leaveProcess.getLeaveProcessId(), "LeaveProcess",
					"leaveDocs");
			docVO.setDirStruct(dirStucture);
		}

		documentBL.saveUploadDocuments(docVO);

	}

	public void deleteLeaveProcess(LeaveProcess leaveProcess) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				LeaveProcess.class.getSimpleName(),
				leaveProcess.getLeaveProcessId(), user, workflowDetailVo);
	}

	public void doLeaveProcessBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		LeaveProcess leaveProcess = this.getLeaveProcessService()
				.getLeaveProcess(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			// Update the job assignment with rejoining date as empty
			JobAssignment jobAssignment1 = jobAssignmentBL
					.getJobAssignmentService().getJobAssignmentInfo(
							leaveProcess.getJobAssignment()
									.getJobAssignmentId());
			if (jobAssignment1 != null
					&& leaveProcess.getHoldUntilRejoining() != null
					&& leaveProcess.getHoldUntilRejoining()) {
				jobAssignment1.setRejoiningDate(null);
				jobAssignment1.setFreeze(false);
				jobAssignment1.setTransactionReference(null);
				jobAssignmentBL.getJobAssignmentService().saveJobAssignment(
						jobAssignment1);
			}
			Payroll payroll = this
					.getPayrollBL()
					.getPayrollService()
					.getPayrollDetailByUseCase(
							leaveProcess.getLeaveProcessId(),
							LeaveProcess.class.getSimpleName());
			if (payroll != null)
				this.getPayrollBL()
						.getPayrollService()
						.deletePayroll(
								payroll,
								new ArrayList<PayrollTransaction>(payroll
										.getPayrollTransactions()));
			leaveProcessService.deleteLeaveProcess(leaveProcess);
		} else {
			// Update the job assignment with rejoining date as empty
			JobAssignment jobAssignment1 = jobAssignmentBL
					.getJobAssignmentService().getJobAssignmentInfo(
							leaveProcess.getJobAssignment()
									.getJobAssignmentId());
			if (jobAssignment1 != null
					&& leaveProcess.getHoldUntilRejoining() != null
					&& leaveProcess.getHoldUntilRejoining()) {
				jobAssignment1.setRejoiningDate(null);
				jobAssignment1.setFreeze(true);
				jobAssignment1.setTransactionReference(null);
				jobAssignmentBL.getJobAssignmentService().saveJobAssignment(
						jobAssignment1);
			} else {
				jobAssignment1.setRejoiningDate(null);
				jobAssignment1.setFreeze(false);
				jobAssignment1.setTransactionReference(null);
				jobAssignmentBL.getJobAssignmentService().saveJobAssignment(
						jobAssignment1);
			}
			Payroll payroll = null;
			PayrollVO payrollVO = null;
			if (leaveProcess.getJobLeave().getLeave().getLookupDetail()
					.getAccessCode().equals("AL")) {
				payroll = this
						.getPayrollBL()
						.getPayrollService()
						.getPayrollDetailByUseCase(
								leaveProcess.getLeaveProcessId(),
								LeaveProcess.class.getSimpleName());
				if (payroll != null)
					this.getPayrollBL()
							.getPayrollService()
							.deletePayroll(
									payroll,
									new ArrayList<PayrollTransaction>(payroll
											.getPayrollTransactions()));

				// Pending Salary
				payrollVO = new PayrollVO();
				List<PayrollTransactionVO> payrollTransactionVOs = new ArrayList<PayrollTransactionVO>();
				PayrollElement encashElement = null;
				ResignationTerminationVO vo = new ResignationTerminationVO();
				LocalDate date = new LocalDate(leaveProcess.getFromDate());
				vo.setEffectiveDate(date.minusDays(1).toDate());
				vo.setJobAssignmentId(leaveProcess.getJobAssignment()
						.getJobAssignmentId());

				if (leaveProcess.getEncash() == null
						|| !leaveProcess.getEncash())
					payrollTransactionVOs = this.recursivePayroll(vo,
							payrollVO, payrollTransactionVOs);

				PayPeriodVO payPeriodVO = new PayPeriodVO();
				payPeriodVO.setTransactionDate(date.toDate());
				payPeriodVO.setJobAssignmentId(leaveProcess.getJobAssignment()
						.getJobAssignmentId());
				List<PayPeriodTransaction> payPeriods = this.getPayrollBL()
						.getPayPeriodBL().getPayPeriodService()
						.findPayPeriod(payPeriodVO);
				payroll = new Payroll();
				JobAssignment jobAssignment = null;
				if (leaveProcess.getJobAssignment().getJobAssignmentId() != null) {
					jobAssignment = this
							.getPayrollBL()
							.getJobAssignmentBL()
							.getJobAssignmentService()
							.getJobAssignmentInfo(
									leaveProcess.getJobAssignment()
											.getJobAssignmentId());
					payroll.setJobAssignment(jobAssignment);
				}
				payroll.setPayPeriodTransaction(payPeriods.get(0));
				payroll.setUseCase(LeaveProcess.class.getSimpleName());
				payroll.setRecordId(leaveProcess.getLeaveProcessId());
				payroll.setImplementation(getImplementation());
				payroll.setPerson(jobAssignment.getPerson());
				payroll.setJobAssignment(jobAssignment);
				payroll.setStatus(Constants.HR.PayStatus.InProcess.getCode());

				payroll.setIsApprove(Byte
						.parseByte(WorkflowConstants.Status.Published.getCode()
								+ ""));
				payroll.setIsSalary(false);
				payroll.setCreatedDate(new Date());
				payroll.setStartDate(leaveProcess.getFromDate());
				payroll.setEndDate(leaveProcess.getToDate());
				payroll.setIsLedgerTransaction(false);
				payroll.setPayMonth("Leave Payment");
				List<PayrollTransaction> payTransList = new ArrayList<PayrollTransaction>();
				PayrollTransaction payrollTransaction = null;
				PayrollElement payrollElement = this.getPayrollBL()
						.getPayrollElementBL().getPayrollElementService()
						.getPayrollElementByCode(getImplementation(), "SALARY");
				if (payrollTransactionVOs != null)
					for (PayrollTransactionVO payrollTransactionVO : payrollTransactionVOs) {
						payrollTransaction = new PayrollTransaction();
						payrollTransaction.setCreatedDate(new Date());
						payrollTransaction
								.setStatus(Constants.HR.PayTransactionStatus.New
										.getCode());
						payrollTransaction.setPerson(payroll.getPerson());
						payrollTransaction
								.setImplementation(getImplementation());
						payrollTransaction.setPayrollElement(payrollElement);
						payrollTransaction.setStartDate(payrollTransactionVO
								.getStartDate());
						payrollTransaction.setEndDate(payrollTransactionVO
								.getEndDate());
						payrollTransaction.setAmount(payrollTransactionVO
								.getAmount());
						payrollTransaction
								.setPayPeriodTransaction(payrollTransactionVO
										.getPayPeriodTransaction());
						payrollTransaction.setNote(payrollTransactionVO
								.getNote());
						payrollTransaction.setNumberOfDays(payrollTransactionVO
								.getNumberOfDays());
						payTransList.add(payrollTransaction);
					}

				if (payTransList != null && payTransList.size() > 0)
					this.getPayrollBL()
							.directSavePayroll(payroll, payTransList);

			}
		}
	}

	public LeaveProcessTO getLeaveInfoForApproval(Long leaveProcessId)
			throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		LeaveProcessTO leaveTo = new LeaveProcessTO();
		LeaveProcess leaveProcess = this.getLeaveProcessService()
				.getLeaveProcess(leaveProcessId);
		Job job = jobService.getJobInfo(leaveProcess.getJobLeave().getJob()
				.getJobId());
		leaveProcess.getJobLeave().setJob(job);
		leaveTo = convertToTO(leaveProcess);

		List<LeaveProcess> leaveProcesses = new ArrayList<LeaveProcess>();
		JobAssignment jobAssigment = new JobAssignment();
		for (JobAssignment jobAssigment1 : job.getJobAssignments()) {
			jobAssigment = jobAssigment1;
		}
		leaveProcesses = this.getLeaveProcessService()
				.getAllLeaveProcessBasedOnPerson(jobAssigment.getPerson());
		if (leaveProcesses != null && leaveProcesses.size() > 0) {
			List<LeaveProcessTO> leaveProcessTOlist = new ArrayList<LeaveProcessTO>();
			List<LeaveProcessTO> leaveProcessTOlistTemp = new ArrayList<LeaveProcessTO>();
			leaveProcessTOlistTemp = convertToTOList(leaveProcesses);

			// Skipe the current request
			for (LeaveProcessTO leaveProcessTO : leaveProcessTOlistTemp) {
				if (leaveProcessTO.getLeaveProcessId() != null
						&& !leaveProcessTO.getLeaveProcessId().equals(
								leaveProcessId))
					leaveProcessTOlist.add(leaveProcessTO);
			}
			leaveTo.setLeaveHistoryList(leaveProcessTOlist);

		}

		leaveTo.setLeaveAvailabilityList(findLeaveAvailability(jobAssigment,
				new Date()));
		return leaveTo;
	}

	public LeaveProcessTO getLeaveHistory(Long jobAssignmentId, Date fromDate,
			Date toDate) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		LeaveProcessTO leaveTo = new LeaveProcessTO();
		LeaveProcess leaveProcess = new LeaveProcess();
		Date start = null;
		Date end = null;
		if (fromDate != null && toDate != null) {
			leaveTo.setFromDate(DateFormat.convertDateToString(fromDate
					.toString()));
			leaveTo.setToDate(DateFormat.convertDateToString(toDate.toString()));
		} else {
			Calendar cal = Calendar.getInstance();
			// First Day of Year
			cal.set(Calendar.DAY_OF_YEAR,
					cal.getActualMinimum(Calendar.DAY_OF_YEAR));
			start = cal.getTime();

			// Last Day of Month
			cal.set(Calendar.DAY_OF_YEAR,
					cal.getActualMaximum(Calendar.DAY_OF_YEAR));
			end = cal.getTime();
		}

		JobAssignment jobAssignment = this.getJobService()
				.getJobAssignmentInfo(jobAssignmentId);
		leaveProcess.setJobAssignment(jobAssignment);
		leaveTo.setPersonName(jobAssignment.getPerson().getFirstName() + " "
				+ jobAssignment.getPerson().getLastName());
		leaveTo.setCompanyName(jobAssignment.getCmpDeptLocation().getCompany()
				.getCompanyName());
		leaveTo.setDepartmentName(jobAssignment.getCmpDeptLocation()
				.getDepartment().getDepartmentName());
		leaveTo.setLocationName(jobAssignment.getCmpDeptLocation()
				.getLocation().getLocationName());
		leaveTo.setFromDate(DateFormat.convertSystemDateToString(start));
		if (end.after(new Date()))
			leaveTo.setToDate(DateFormat.convertSystemDateToString(new Date()));
		else
			leaveTo.setToDate(DateFormat.convertSystemDateToString(end));
		leaveTo.setCreatedDate(DateFormat.convertSystemDateToString(new Date()));

		List<LeaveProcess> leaveProcesses = this.getLeaveProcessService()
				.getLeavHistory(jobAssignment.getPerson(), start, end);
		if (leaveProcesses != null && leaveProcesses.size() > 0) {
			List<LeaveProcessTO> leaveProcessTOlist = new ArrayList<LeaveProcessTO>();
			leaveProcessTOlist = convertToTOList(leaveProcesses);
			leaveTo.setLeaveHistoryList(leaveProcessTOlist);
		}

		return leaveTo;
	}

	// Converter
	public List<LeaveProcessTO> convertToTOList(
			List<LeaveProcess> leaveProcesses) throws Exception {

		List<LeaveProcessTO> tosList = new ArrayList<LeaveProcessTO>();

		for (LeaveProcess leavepro : leaveProcesses) {
			Job job = jobService.getJobForDelete(leavepro.getJobLeave()
					.getJob().getJobId());
			leavepro.getJobLeave().setJob(job);
			tosList.add(convertToTO(leavepro));
		}

		return tosList;
	}

	public LeaveProcessTO convertToTO(LeaveProcess leaveProcess)
			throws Exception {
		LeaveProcessTO infoTO = new LeaveProcessTO();
		infoTO.setLeaveProcessId(leaveProcess.getLeaveProcessId());
		infoTO.setLeaveType(leaveProcess.getJobLeave().getLeave()
				.getDisplayName());
		infoTO.setLeaveId(leaveProcess.getJobLeave().getLeave().getLeaveId());
		infoTO.setLeave(leaveProcess.getJobLeave().getLeave());
		infoTO.setProvidedDays(leaveProcess.getJobLeave().getDays() + "");
		if (leaveProcess.getFromDate() != null
				&& leaveProcess.getFromDate().toString().length() > 10)
			infoTO.setFromDate(DateFormat
					.convertSystemDateToString(leaveProcess.getFromDate()));
		else
			infoTO.setFromDate(DateFormat.convertDateToString(leaveProcess
					.getFromDate().toString()));

		if (leaveProcess.getToDate() != null
				&& leaveProcess.getToDate().toString().length() > 10)
			infoTO.setToDate(DateFormat.convertSystemDateToString(leaveProcess
					.getToDate()));
		else
			infoTO.setToDate(DateFormat.convertDateToString(leaveProcess
					.getToDate().toString()));

		if (leaveProcess.getHalfDay() != null && leaveProcess.getHalfDay())
			infoTO.setHalfDayStr("Yes");
		else
			infoTO.setHalfDayStr("No");
		infoTO.setReason(leaveProcess.getReason());
		infoTO.setContactOnLeave(leaveProcess.getContactOnLeave());
		infoTO.setCreatedDate(DateFormat.convertDateToString(leaveProcess
				.getCreatedDate().toString()));
		if (leaveProcess.getStatus() != null)
			infoTO.setStatusStr(Constants.HR.LeaveStatus.get(
					leaveProcess.getStatus()).name()
					+ "");
		else
			infoTO.setStatusStr("Pending");

		if ((leaveProcess.getEncash() != null && leaveProcess.getEncash())
				|| (leaveProcess.getSettleDues() != null && leaveProcess
						.getSettleDues())) {
			infoTO.setEncash(leaveProcess.getEncash());
			infoTO.setTotalAmount(leaveProcess.getTotalAmount());
		} else {
			infoTO.setTotalAmount(0.0);
			infoTO.setEncash(false);
		}

		JobAssignment jobAssigment = new JobAssignment();
		jobAssigment = leaveProcess.getJobAssignment();
		infoTO.setJobAssignmentId(jobAssigment.getJobAssignmentId());
		infoTO.setJobAssignment(jobAssigment);
		infoTO.setPersonNumber(jobAssigment.getPerson().getPersonNumber());
		infoTO.setJobLeaveId(leaveProcess.getJobLeave().getJobLeaveId());
		infoTO.setPersonName(jobAssigment.getPerson().getFirstName() + " "
				+ jobAssigment.getPerson().getLastName());
		infoTO.setCompanyName(jobAssigment.getCmpDeptLocation().getCompany()
				.getCompanyName());
		infoTO.setDesignationName(jobAssigment.getDesignation()
				.getDesignationName());
		infoTO.setDepartmentName(jobAssigment.getCmpDeptLocation()
				.getDepartment().getDepartmentName());
		infoTO.setLocationName(jobAssigment.getCmpDeptLocation().getLocation()
				.getLocationName());

		if (leaveProcess.getIsApprove() != null
				&& WorkflowConstants.Status.Published.getCode() == (byte) leaveProcess
						.getIsApprove())
			infoTO.setIsApprove("Approved");
		else if (WorkflowConstants.Status.Deny.getCode() == (byte) leaveProcess
				.getIsApprove())
			infoTO.setIsApprove("Rejected");
		else
			infoTO.setIsApprove("Pending");
		// String
		// days=numberOfDays(leaveProcess.getFromDate(),leaveProcess.getToDate(),leaveProcess.getHalfDay());
		infoTO.setNumberOfDays(leaveProcess.getDays() + "");

		if (jobAssigment.getPerson().getValidFromDate() != null)
			infoTO.setPersonJoinedDate(jobAssigment.getPerson()
					.getValidFromDate());
		else
			infoTO.setPersonJoinedDate(new Date());

		return infoTO;
	}

	// Leave Availability Process
	public List<LeaveProcessTO> findLeaveAvailability(
			JobAssignment jobAssignment, Date currentDate) throws Exception {

		Date firstDayOfMonth = new Date();
		Date lastDayOfMonth = new Date();

		Calendar cal = Calendar.getInstance();
		// Current Date
		currentDate = cal.getTime();
		// First Day of Month
		cal.setTime(currentDate);
		cal.set(Calendar.DAY_OF_YEAR,
				cal.getActualMinimum(Calendar.DAY_OF_YEAR));
		firstDayOfMonth = cal.getTime();

		// Last Day of Month
		cal.setTime(currentDate);
		cal.set(Calendar.DAY_OF_YEAR,
				cal.getActualMaximum(Calendar.DAY_OF_YEAR));
		lastDayOfMonth = cal.getTime();
		List<LeaveProcessTO> leaveReconciliationTOs = new ArrayList<LeaveProcessTO>();
		List<LeaveReconciliation> leaveReconciliationListFromDB = leaveProcessService
				.getLeaveReconciliation(jobAssignment);
		List<LeaveProcessVO> leaveReconciliationVOs = leaveReconciliationProcess(
				jobAssignment, firstDayOfMonth, lastDayOfMonth, currentDate,
				leaveReconciliationListFromDB);
		for (LeaveProcessVO leaveProcessVO : leaveReconciliationVOs) {
			LeaveProcessTO leaveProcessTO = new LeaveProcessTO();
			leaveProcessTO.setLeaveId(leaveProcessVO
					.getNewLeaveReconciliation().getJobLeave().getLeave()
					.getLeaveId());

			leaveProcessTO.setEncash(leaveProcessVO.getNewLeaveReconciliation()
					.getJobLeave().getLeave().getCanEncash());
			leaveProcessTO.setPersonName(jobAssignment.getPerson()
					.getFirstName()
					+ " "
					+ jobAssignment.getPerson().getLastName());
			leaveProcessTO.setPersonNumber(jobAssignment.getPerson()
					.getPersonNumber());
			leaveProcessTO.setDesignationName(jobAssignment.getDesignation()
					.getDesignationName());
			if (jobAssignment.getPerson().getValidFromDate() != null)
				leaveProcessTO.setJoiningDate(DateFormat
						.convertDateToString(jobAssignment.getPerson()
								.getValidFromDate() + ""));

			if (jobAssignment.getCmpDeptLocation() != null
					&& jobAssignment.getCmpDeptLocation().getDepartment() != null) {
				leaveProcessTO.setDepartmentName(jobAssignment
						.getCmpDeptLocation().getDepartment()
						.getDepartmentName());
			}

			if (jobAssignment.getCmpDeptLocation() != null
					&& jobAssignment.getCmpDeptLocation().getCompany() != null) {
				leaveProcessTO.setCompanyName(jobAssignment
						.getCmpDeptLocation().getCompany().getCompanyName());
			}

			if (jobAssignment.getCmpDeptLocation() != null
					&& jobAssignment.getCmpDeptLocation().getLocation() != null) {
				leaveProcessTO.setLocationName(jobAssignment
						.getCmpDeptLocation().getLocation().getLocationName());
			}
			leaveProcessTO.setLeaveType(leaveProcessVO
					.getNewLeaveReconciliation().getJobLeave().getLeave()
					.getDisplayName());
			leaveProcessTO.setProvidedDays(leaveProcessVO
					.getNewLeaveReconciliation().getProvidedDays() + "");
			leaveProcessTO.setEligibleDays(leaveProcessVO
					.getNewLeaveReconciliation().getEligibleDays() + "");
			leaveProcessTO.setTakenDays(leaveProcessVO
					.getNewLeaveReconciliation().getTakendDays() + "");
			leaveProcessTO
					.setAvailableDays((leaveProcessVO
							.getNewLeaveReconciliation().getEligibleDays() - leaveProcessVO
							.getNewLeaveReconciliation().getTakendDays())
							+ "");

			leaveProcessTO.setAvailableDays(AIOSCommons.roundTwoDecimals(Double
					.valueOf(leaveProcessTO.getAvailableDays())));

			leaveReconciliationTOs.add(leaveProcessTO);
		}

		return leaveReconciliationTOs;
	}

	public void employeeLeaveReconciliation() {
		Date firstDayOfMonth = new Date();
		Date lastDayOfMonth = new Date();
		Date currentDate = new Date();

		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		// cal.add(Calendar.MONTH, -1);
		// First Day of Month
		cal.set(Calendar.DAY_OF_YEAR,
				cal.getActualMinimum(Calendar.DAY_OF_YEAR));
		firstDayOfMonth = cal.getTime();
		// Last Day of Month
		cal.set(Calendar.DAY_OF_YEAR,
				cal.getActualMaximum(Calendar.DAY_OF_YEAR));
		lastDayOfMonth = cal.getTime();

		List<JobAssignment> jobAssignments = jobAssignmentBL
				.getJobAssignmentService().getAllActiveJobAssignment(
						getImplementation());
		List<LeaveProcessVO> leaveReconciliationVOs = null;
		for (JobAssignment jobAssignment : jobAssignments) {
			List<LeaveReconciliation> leaveReconciliationListFromDB = leaveProcessService
					.getLeaveReconciliation(jobAssignment);

			leaveReconciliationVOs = leaveReconciliationProcess(jobAssignment,
					firstDayOfMonth, lastDayOfMonth, currentDate,
					leaveReconciliationListFromDB);

			for (LeaveReconciliation leaveReconciliation : leaveReconciliationListFromDB) {
				leaveReconciliation.setIsActive(false);
			}
		}
		// List<LeaveReconciliation> oldLeaveReconciliationRecords=new
		// ArrayList<LeaveReconciliation>();
		List<LeaveReconciliation> newLeaveReconciliationRecords = new ArrayList<LeaveReconciliation>();
		if (leaveReconciliationVOs != null) {
			for (LeaveProcessVO leaveProcessVO : leaveReconciliationVOs) {
				// oldLeaveReconciliationRecords.add(leaveReconciliationVOToEntity(leaveProcessVO.getOldLeaveReconciliationVO()));
				newLeaveReconciliationRecords.add(leaveProcessVO
						.getNewLeaveReconciliation());
			}
		}
		leaveProcessService
				.saveLeaveReconciliations(newLeaveReconciliationRecords);

	}

	public List<LeaveProcessVO> leaveReconciliationProcess(
			JobAssignment jobAssignment, Date firstDayOfMonth,
			Date lastDayOfMonth, Date cuttOfDate,
			List<LeaveReconciliation> leaveReconciliationListFromDB) {

		Map<String, LeaveReconciliation> leaveReconciliationMaps = new HashMap<String, LeaveReconciliation>();
		for (LeaveReconciliation leaveReconciliation : leaveReconciliationListFromDB) {
			leaveReconciliationMaps.put(leaveReconciliation.getLeave()
					.getLeaveId()
					+ "_"
					+ leaveReconciliation.getJobAssignment().getPerson()
							.getPersonId(), leaveReconciliation);
		}

		List<LeaveProcessVO> leaveReconciliationVOs = new ArrayList<LeaveProcessVO>();

		List<LeaveProcess> leaveProcesses = leaveProcessService
				.getAllLeaveProcessBasedOnPerson(jobAssignment.getPerson());
		for (JobLeave jobLeave : jobAssignment.getJob().getJobLeaves()) {
			if (jobLeave.getIsActive() == null || !jobLeave.getIsActive())
				continue;
			Double totalLeaveDaysDedution = 0.0;
			LeaveProcessVO leaveReconciliationVO = new LeaveProcessVO();
			LeaveReconciliation leaveReconciliation = new LeaveReconciliation();
			if (leaveReconciliationMaps.containsKey(jobLeave.getLeave()
					.getLeaveId()
					+ "_"
					+ jobAssignment.getPerson().getPersonId())) {
				LeaveReconciliation temp = leaveReconciliationMaps.get(jobLeave
						.getLeave().getLeaveId()
						+ "_"
						+ jobAssignment.getPerson().getPersonId());
				LocalDate localDate = new LocalDate(temp.getEndDate());
				leaveReconciliation
						.setStartDate(localDate.plusDays(1).toDate());
			} else {
				leaveReconciliation.setStartDate(firstDayOfMonth);
			}
			leaveReconciliation.setEndDate(lastDayOfMonth);
			leaveReconciliation.setImplementation(getImplementation());
			leaveReconciliation.setIsActive(true);
			leaveReconciliation.setJobLeave(jobLeave);
			leaveReconciliation.setLeave(jobLeave.getLeave());
			leaveReconciliation.setJobAssignment(jobAssignment);
			leaveReconciliation.setCreatedDate(new Date());
			leaveReconciliation.setTakendDays(0.0);
			if (leaveProcesses != null) {
				Map<LocalDate, LocalDate> leaveProcessMap = new HashMap<LocalDate, LocalDate>();

				// Conversion & Filter of Job Assignment Number & Between the
				// Leave Type
				List<LeaveProcessVO> leaveProcessVOs = new ArrayList<LeaveProcessVO>();
				for (LeaveProcess leaveProcess : leaveProcesses) {
					if (leaveProcess.getJobAssignment()
							.getJobAssignmentNumber()
							.equals(jobAssignment.getJobAssignmentNumber())
							&& jobLeave
									.getLeave()
									.getLeaveId()
									.equals(leaveProcess.getJobLeave()
											.getLeave().getLeaveId())) {
						// Trim the Leave period to Exclude the holiday &
						// weekend days
						LeaveProcessVO calendarPeriod = leaveExcludes(leaveProcess);
						leaveProcessVOs.add(calendarPeriod);
					}
				}

				// Trim the leave period according to the reconciliation period
				for (LeaveProcessVO leaveProcessPeriod : leaveProcessVOs) {
					boolean exsitFlag = false;
					totalLeaveDaysDedution += leaveProcessPeriod
							.getLeaveDaysDeduction();
					if (!DateFormat.compareDates(
							leaveProcessPeriod.getToDate(),
							leaveReconciliation.getStartDate())
							&& DateFormat.compareDates(
									leaveProcessPeriod.getFromDate(),
									leaveReconciliation.getEndDate())) {
						exsitFlag = true;
					} else if (DateFormat.compareDates(
							leaveProcessPeriod.getToDate(),
							leaveReconciliation.getStartDate())
							&& DateFormat.compareDates(
									leaveProcessPeriod.getFromDate(),
									leaveReconciliation.getEndDate())) {
						if (DateFormat.compareDates(
								leaveProcessPeriod.getFromDate(),
								leaveReconciliation.getStartDate())
								&& DateFormat.compareDates(
										leaveProcessPeriod.getFromDate(),
										leaveReconciliation.getEndDate()))
							exsitFlag = true;
					}

					if (DateFormat.compareDates(
							leaveProcessPeriod.getFromDate(),
							leaveReconciliation.getStartDate())) {
						leaveProcessPeriod.setFromDate(leaveReconciliation
								.getStartDate());
					}

					if (!DateFormat.compareDates(
							leaveProcessPeriod.getToDate(),
							leaveReconciliation.getEndDate())) {
						leaveProcessPeriod.setToDate(leaveReconciliation
								.getEndDate());
					}

					if (exsitFlag) {
						LocalDate start = new LocalDate(
								leaveProcessPeriod.getFromDate());
						LocalDate end = new LocalDate(
								leaveProcessPeriod.getToDate());
						int leavedays = Days.daysBetween(start, end).getDays() + 1;
						for (int i = 0; i < leavedays; i++) {
							LocalDate d = start.withFieldAdded(
									DurationFieldType.days(), i);
							// Excluded leave days also we should exclude from
							// reconciliation process
							if ((leaveProcessPeriod.getWeekendMap() != null && !leaveProcessPeriod
									.getWeekendMap().containsKey(d))
									|| (leaveProcessPeriod.getHolidayMap() != null && !leaveProcessPeriod
											.getHolidayMap().containsKey(d)))
								leaveProcessMap.put(d, d);
							else if (leaveProcessPeriod.getWeekendMap() == null
									&& leaveProcessPeriod.getHolidayMap() == null)
								leaveProcessMap.put(d, d);
						}

					}
				}
				leaveReconciliationVO.setLeaeveProcessMap(leaveProcessMap);
			}
			leaveReconciliationVO
					.setNewLeaveReconciliation(leaveReconciliation);
			// Leave days deduction
			leaveReconciliationVO.setLeaveDaysDeduction(totalLeaveDaysDedution);
			leaveReconciliationVO.setCutOffDate(cuttOfDate);
			leaveReconciliationVOs.add(leaveReconciliationVO);
		}

		if (jobAssignment != null && leaveReconciliationVOs != null)
			leaveReconciliationVOs = getLeaveReconciliationFinalProcess(
					jobAssignment, leaveReconciliationVOs,
					leaveReconciliationListFromDB);
		return leaveReconciliationVOs;

	}

	public List<LeaveProcessVO> getLeaveReconciliationFinalProcess(
			JobAssignment jobAssignment,
			List<LeaveProcessVO> newLeaveReconciliations,
			List<LeaveReconciliation> leaveReconciliationListFromDB) {

		Map<String, LeaveReconciliationVO> dbReconciliationMap = new HashMap<String, LeaveReconciliationVO>();
		for (LeaveReconciliation leaveReconciliation : leaveReconciliationListFromDB) {
			LeaveReconciliationVO leaveReconciliationVO = leaveReconciliationEntityToVO(leaveReconciliation);
			dbReconciliationMap.put(leaveReconciliation.getJobLeave()
					.getLeave().getLeaveId()
					+ "", leaveReconciliationVO);
		}
		// Finding total leaveDaysDeduction for annual leave eligible days
		// calculation
		Double totalDays = 0.0;
		for (LeaveProcessVO leaveReconciVO : newLeaveReconciliations) {
			totalDays += leaveReconciVO.getLeaveDaysDeduction();
		}
		for (LeaveProcessVO leaveReconciVO : newLeaveReconciliations) {

			if (leaveReconciVO.getNewLeaveReconciliation().getJobLeave()
					.getLeave().getIsYearly() != null
					&& leaveReconciVO.getNewLeaveReconciliation().getJobLeave()
							.getLeave().getIsYearly() == true) {

				leaveReconciVO = yearlyCalculation(leaveReconciVO,
						dbReconciliationMap);

			} else if (leaveReconciVO.getNewLeaveReconciliation().getJobLeave()
					.getLeave().getIsMonthly() != null
					&& leaveReconciVO.getNewLeaveReconciliation().getJobLeave()
							.getLeave().getIsMonthly() == true) {

				leaveReconciVO = monthlyCalculation(leaveReconciVO,
						dbReconciliationMap);

			} else if (leaveReconciVO.getNewLeaveReconciliation().getJobLeave()
					.getLeave().getIsWeekly() != null
					&& leaveReconciVO.getNewLeaveReconciliation().getJobLeave()
							.getLeave().getIsWeekly()) {

				leaveReconciVO = weeklyCalculation(leaveReconciVO,
						dbReconciliationMap);

			} else {
				leaveReconciVO = inServiceCalculation(leaveReconciVO,
						dbReconciliationMap);
			}

			// Finding total leaveDaysDeduction for annual leave eligible days
			// calculation

			if (leaveReconciVO.getNewLeaveReconciliation().getJobLeave()
					.getLeave().getLookupDetail().getAccessCode().equals("AL")) {
				leaveReconciVO.getNewLeaveReconciliation().setProvidedDays(
						(leaveReconciVO.getNewLeaveReconciliation()
								.getProvidedDays() != null ? leaveReconciVO
								.getNewLeaveReconciliation().getProvidedDays()
								: 0.0)
								- totalDays);

				leaveReconciVO.getNewLeaveReconciliation().setEligibleDays(
						(leaveReconciVO.getNewLeaveReconciliation()
								.getEligibleDays() != null ? leaveReconciVO
								.getNewLeaveReconciliation().getEligibleDays()
								: 0.0)
								- totalDays);
			}

		}
		return newLeaveReconciliations;
	}

	public LeaveProcessVO monthlyCalculation(LeaveProcessVO leaveReconciVO,
			Map<String, LeaveReconciliationVO> dbReconciliationMap) {
		LeaveReconciliationVO oldLeaveReconciliation = dbReconciliationMap
				.get(leaveReconciVO.getNewLeaveReconciliation().getJobLeave()
						.getLeave().getLeaveId()
						+ "");
		Leave leavePolicy = leaveReconciVO.getNewLeaveReconciliation()
				.getJobLeave().getLeave();
		boolean carryForward = false;
		double providedDays = 0.0;
		LocalDate lastDayOfYearLeave = new LocalDate(leaveReconciVO
				.getNewLeaveReconciliation().getEndDate());
		LocalDate currentDate = new LocalDate(new Date());

		providedDays = (leavePolicy.getDefaultDays() - (leaveReconciVO
				.getNewLeaveReconciliation().getProvidedDays() != null ? leaveReconciVO
				.getNewLeaveReconciliation().getProvidedDays() : 0.0));
		if (oldLeaveReconciliation != null) {
			oldLeaveReconciliation.setIsActive(false);
			leaveReconciVO.setOldLeaveReconciliationVO(oldLeaveReconciliation);
			// Leave Taken days calculation based on the period
			Integer carryCount = null;
			if (leavePolicy.getCarryForwardCount() != null
					&& leavePolicy.getCarryForwardCount() > 0) {
				carryForward = true;
				carryCount = Integer
						.valueOf((oldLeaveReconciliation
								.getCarryForwardedCount() != null ? oldLeaveReconciliation
								.getCarryForwardedCount() - 1 : leaveReconciVO
								.getLeaveProcess().getJobLeave().getLeave()
								.getCarryForwardCount())
								+ "");
			}

			if (carryForward && carryCount > 0) {

				leaveReconciVO.getNewLeaveReconciliation()
						.setProvidedDays(
								oldLeaveReconciliation.getProvidedDays()
										+ providedDays);
				leaveReconciVO.getNewLeaveReconciliation().setEligibleDays(
						oldLeaveReconciliation.getEligibleDays()
								+ leavePolicy.getDefaultDays());
				leaveReconciVO.getNewLeaveReconciliation().setTakendDays(
						oldLeaveReconciliation.getTakendDays()
								+ leaveReconciVO.getLeaeveProcessMap().size());
				leaveReconciVO.getNewLeaveReconciliation()
						.setCarryForwardedCount(carryCount);
			} else {

				leaveReconciVO.getNewLeaveReconciliation().setProvidedDays(
						providedDays);
				leaveReconciVO.getNewLeaveReconciliation().setEligibleDays(
						leavePolicy.getDefaultDays());
				if (currentDate.isEqual(lastDayOfYearLeave)
						|| currentDate.isBefore(lastDayOfYearLeave))
					leaveReconciVO
							.getNewLeaveReconciliation()
							.setTakendDays(
									(leaveReconciVO.getLeaeveProcessMap() != null ? leaveReconciVO
											.getLeaeveProcessMap().size() : 0.0));
				else
					leaveReconciVO.getNewLeaveReconciliation().setTakendDays(
							0.0);
				leaveReconciVO.getNewLeaveReconciliation()
						.setCarryForwardedCount(
								leavePolicy.getCarryForwardCount());
			}

			// Leave Provided days calculation based on the period

		} else {

			// Leave Taken days calculation based on the period
			if (leavePolicy.getCarryForwardCount() != null) {
				leaveReconciVO.getNewLeaveReconciliation()
						.setCarryForwardedCount(
								leavePolicy.getCarryForwardCount());
				carryForward = true;
			}

			leaveReconciVO
					.getNewLeaveReconciliation()
					.setTakendDays(
							Double.parseDouble((leaveReconciVO
									.getLeaeveProcessMap() != null ? leaveReconciVO
									.getLeaeveProcessMap().size() : 0)
									+ ""));
			leaveReconciVO.getNewLeaveReconciliation().setProvidedDays(
					providedDays);
			leaveReconciVO.getNewLeaveReconciliation().setEligibleDays(
					leavePolicy.getDefaultDays());
		}

		return leaveReconciVO;
	}

	public LeaveProcessVO yearlyCalculation(LeaveProcessVO leaveReconciVO,
			Map<String, LeaveReconciliationVO> dbReconciliationMap) {

		LocalDate startDate = new LocalDate(leaveReconciVO
				.getNewLeaveReconciliation().getStartDate());
		LocalDate endDate = new LocalDate(leaveReconciVO.getCutOffDate());
		LocalDate joiningDate = new LocalDate(leaveReconciVO
				.getNewLeaveReconciliation().getJobAssignment().getPerson()
				.getValidFromDate());
		int year = endDate.year().get();
		int motth = endDate.getMonthOfYear();
		int day = joiningDate.dayOfMonth().get();
		DateTime dateTime = new DateTime(year, motth, day, 0, 0, 0, 0);
		joiningDate = new LocalDate(dateTime);

		if (joiningDate.isBefore(endDate) || joiningDate.equals(endDate))
			endDate = joiningDate;

		Double monthCount = 0.0;
		while (startDate.isBefore(endDate)) {
			startDate = startDate.plusMonths(1);
			monthCount++;
		}

		LeaveReconciliationVO oldLeaveReconciliation = dbReconciliationMap
				.get(leaveReconciVO.getNewLeaveReconciliation().getJobLeave()
						.getLeave().getLeaveId()
						+ "");
		Leave leavePolicy = leaveReconciVO.getNewLeaveReconciliation()
				.getJobLeave().getLeave();
		boolean carryForward = false;
		double eligilbleDays = 0.0;
		eligilbleDays = (leavePolicy.getDefaultDays() * monthCount) / 12;

		if (oldLeaveReconciliation != null) {
			oldLeaveReconciliation.setIsActive(false);
			leaveReconciVO.setOldLeaveReconciliationVO(oldLeaveReconciliation);
			Integer carryCount = null;

			if (leavePolicy.getCarryForwardCount() != null
					&& leavePolicy.getCarryForwardCount() > 0) {
				carryForward = true;
				carryCount = (int) 2;
				/*
				 * carryCount = Integer .valueOf((oldLeaveReconciliation
				 * .getCarryForwardedCount() != null ? oldLeaveReconciliation
				 * .getCarryForwardedCount() - 1 : leaveReconciVO
				 * .getLeaveProcess().getJobLeave().getLeave()
				 * .getCarryForwardCount()) + "");
				 */
			}

			leaveReconciVO.getNewLeaveReconciliation().setProvidedDays(
					oldLeaveReconciliation.getProvidedDays()
							+ leavePolicy.getDefaultDays());
			leaveReconciVO.getNewLeaveReconciliation().setEligibleDays(
					oldLeaveReconciliation.getProvidedDays() + eligilbleDays);
			leaveReconciVO
					.getNewLeaveReconciliation()
					.setTakendDays(
							oldLeaveReconciliation.getTakendDays()
									+ (leaveReconciVO.getLeaeveProcessMap() != null ? leaveReconciVO
											.getLeaeveProcessMap().size() : 0));
			leaveReconciVO.getNewLeaveReconciliation().setCarryForwardedCount(
					leavePolicy.getCarryForwardCount());

		} else {

			if (leavePolicy.getCarryForwardCount() != null
					&& leavePolicy.getCarryForwardCount() > 0) {
				leaveReconciVO.getNewLeaveReconciliation()
						.setCarryForwardedCount(
								leavePolicy.getCarryForwardCount());
			}
			leaveReconciVO
					.getNewLeaveReconciliation()
					.setTakendDays(
							Double.parseDouble((leaveReconciVO
									.getLeaeveProcessMap() != null ? leaveReconciVO
									.getLeaeveProcessMap().size() : 0)
									+ ""));
			leaveReconciVO.getNewLeaveReconciliation().setProvidedDays(
					leavePolicy.getDefaultDays());
			leaveReconciVO.getNewLeaveReconciliation().setEligibleDays(
					eligilbleDays);

		}

		return leaveReconciVO;
	}

	public LeaveProcessVO weeklyCalculation(LeaveProcessVO leaveReconciVO,
			Map<String, LeaveReconciliationVO> dbReconciliationMap) {
		LeaveReconciliationVO oldLeaveReconciliation = dbReconciliationMap
				.get(leaveReconciVO.getNewLeaveReconciliation().getJobLeave()
						.getLeave().getLeaveId()
						+ "");
		Leave leavePolicy = leaveReconciVO.getNewLeaveReconciliation()
				.getJobLeave().getLeave();
		boolean carryForward = false;
		double providedDays = 0.0;
		LocalDate lastDayOfYearLeave = new LocalDate(leaveReconciVO
				.getNewLeaveReconciliation().getEndDate());
		LocalDate currentDate = new LocalDate(new Date());
		LocalDate dat = new LocalDate(leaveReconciVO.getFromDate());
		LocalDate startDate = dat.withWeekyear(currentDate.getYear())
				.withWeekOfWeekyear(1).withDayOfWeek(1);
		LocalDate endDate = new LocalDate(leaveReconciVO.getToDate());
		LocalDate thisMonday = startDate
				.withDayOfWeek(DateTimeConstants.FRIDAY);

		if (startDate.isAfter(thisMonday)) {
			startDate = thisMonday.plusWeeks(1); // start on next monday
		} else {
			startDate = thisMonday; // start on this monday
		}
		int counts = 0;
		while (startDate.isBefore(endDate)) {
			startDate = startDate.plusWeeks(1);
			counts++;
		}

		providedDays = counts;
		if (oldLeaveReconciliation != null) {
			oldLeaveReconciliation.setIsActive(false);
			leaveReconciVO.setOldLeaveReconciliationVO(oldLeaveReconciliation);
			// Leave Taken days calculation based on the period
			Integer carryCount = null;
			if (leavePolicy.getCarryForwardCount() != null
					&& leavePolicy.getCarryForwardCount() > 0) {
				carryForward = true;
				carryCount = (int) 2;
				/*
				 * carryCount = Integer .valueOf((oldLeaveReconciliation
				 * .getCarryForwardedCount() != null ? oldLeaveReconciliation
				 * .getCarryForwardedCount() - 1 : leavePolicy
				 * .getCarryForwardCount()) + "");
				 */
			}

			if (carryForward && carryCount > 0) {

				leaveReconciVO.getNewLeaveReconciliation()
						.setProvidedDays(
								oldLeaveReconciliation.getProvidedDays()
										+ providedDays);
				leaveReconciVO.getNewLeaveReconciliation()
						.setEligibleDays(
								oldLeaveReconciliation.getEligibleDays()
										+ providedDays);
				leaveReconciVO.getNewLeaveReconciliation().setTakendDays(
						oldLeaveReconciliation.getTakendDays()
								+ leaveReconciVO.getLeaeveProcessMap().size());
				leaveReconciVO.getNewLeaveReconciliation()
						.setCarryForwardedCount(carryCount);
			} else {

				leaveReconciVO.getNewLeaveReconciliation().setProvidedDays(
						providedDays);
				leaveReconciVO.getNewLeaveReconciliation().setEligibleDays(
						providedDays);
				if (currentDate.isEqual(lastDayOfYearLeave)
						|| currentDate.isBefore(lastDayOfYearLeave))
					leaveReconciVO
							.getNewLeaveReconciliation()
							.setTakendDays(
									(leaveReconciVO.getLeaeveProcessMap() != null ? leaveReconciVO
											.getLeaeveProcessMap().size() : 0.0));
				else
					leaveReconciVO.getNewLeaveReconciliation().setTakendDays(
							0.0);
				leaveReconciVO.getNewLeaveReconciliation()
						.setCarryForwardedCount(
								leavePolicy.getCarryForwardCount());
			}

			// Leave Provided days calculation based on the period

		} else {

			// Leave Taken days calculation based on the period
			if (leavePolicy.getCarryForwardCount() != null) {
				leaveReconciVO.getNewLeaveReconciliation()
						.setCarryForwardedCount(
								leavePolicy.getCarryForwardCount());
				carryForward = true;
			}

			leaveReconciVO
					.getNewLeaveReconciliation()
					.setTakendDays(
							Double.parseDouble((leaveReconciVO
									.getLeaeveProcessMap() != null ? leaveReconciVO
									.getLeaeveProcessMap().size() : 0)
									+ ""));
			leaveReconciVO.getNewLeaveReconciliation().setProvidedDays(
					providedDays);
			leaveReconciVO.getNewLeaveReconciliation().setEligibleDays(
					providedDays);
		}

		return leaveReconciVO;
	}

	public LeaveProcessVO inServiceCalculation(LeaveProcessVO leaveReconciVO,
			Map<String, LeaveReconciliationVO> dbReconciliationMap) {
		if (leaveReconciVO.getNewLeaveReconciliation() != null) {
			LeaveReconciliationVO oldLeaveReconciliation = dbReconciliationMap
					.get(leaveReconciVO.getNewLeaveReconciliation()
							.getJobLeave().getLeave().getLeaveId()
							+ "");
			Leave leavePolicy = leaveReconciVO.getNewLeaveReconciliation()
					.getJobLeave().getLeave();
			double takenDays = (leaveReconciVO.getLeaeveProcessMap() != null ? leaveReconciVO
					.getLeaeveProcessMap().size() : 0);
			leaveReconciVO.getNewLeaveReconciliation().setTakendDays(takenDays);
			leaveReconciVO
					.getNewLeaveReconciliation()
					.setProvidedDays(
							leavePolicy.getDefaultDays()
									* (leavePolicy.getIsServiceCount() != null ? leavePolicy
											.getIsServiceCount() : 0));
			leaveReconciVO
					.getNewLeaveReconciliation()
					.setEligibleDays(
							leavePolicy.getDefaultDays()
									* (leavePolicy.getIsServiceCount() != null ? leavePolicy
											.getIsServiceCount() : 0));
			if (oldLeaveReconciliation != null) {

				oldLeaveReconciliation.setIsActive(false);
				leaveReconciVO
						.setOldLeaveReconciliationVO(oldLeaveReconciliation);
				leaveReconciVO.getNewLeaveReconciliation().setTakendDays(
						oldLeaveReconciliation.getTakendDays() + takenDays);
			}

			if (leaveReconciVO.getNewLeaveReconciliation().getTakendDays() <= leavePolicy
					.getDefaultDays())
				leaveReconciVO.getNewLeaveReconciliation().setEligibleDays(
						leavePolicy.getDefaultDays());
		}
		return leaveReconciVO;
	}

	public LeaveProcessVO leaveExcludes(LeaveProcess leaveProcess) {
		LeaveProcessVO leaveProcessVO = new LeaveProcessVO();
		leaveProcessVO.setFromDate(leaveProcess.getFromDate());
		leaveProcessVO.setToDate(leaveProcess.getToDate());
		leaveProcessVO.setLeaveProcess(leaveProcess);
		leaveProcessVO.setLeaveDaysDeduction(leaveProcess
				.getServiceCutoffDays() != null ? leaveProcess
				.getServiceCutoffDays() : 0.0);

		if (leaveProcess != null) {
			JobAssignment jobAssignment = jobAssignmentBL
					.getJobAssignmentService().getJobAssignmentInfo(
							leaveProcess.getJobAssignment()
									.getJobAssignmentId());
			List<EmployeeCalendar> employeeCalendars = employeeCalendarBL
					.getEmployeeCalendarService().getEmployeeCalendarList(
							getImplementation());
			Double numberofDays = 0.0;
			if (leaveProcess.getHalfDay() != null && leaveProcess.getHalfDay()) {
				numberofDays = (numberofDays - 0.5);
			}

			EmployeeCalendar employeeCalendar = employeeCalendarBL
					.getEmployeeCalendarInfo(jobAssignment, employeeCalendars);

			LocalDate start = new LocalDate(leaveProcess.getFromDate());
			LocalDate end = new LocalDate(leaveProcess.getToDate());
			List<LocalDate> dates = new ArrayList<LocalDate>();
			int days = Days.daysBetween(start, end).getDays() + 1;
			for (int i = 0; i < days; i++) {
				LocalDate d = start.withFieldAdded(DurationFieldType.days(), i);
				dates.add(d);
			}
			List<LocalDate> weekendExcludeds = null;
			Map<LocalDate, LocalDate> weekendMap = new HashMap<LocalDate, LocalDate>();

			LeavePolicy leavePolicy = jobAssignment.getDesignation().getGrade()
					.getLeavePolicy();
			if (leavePolicy != null && leavePolicy.getIsWeekendExcluded()
					&& employeeCalendar != null
					&& employeeCalendar.getWeekend() != null
					&& !employeeCalendar.getWeekend().equals("")) {
				weekendExcludeds = JodaDateFormat.findWeekEndFromPeriod(
						employeeCalendar.getWeekend(), start, end);

				for (LocalDate loclDtd : weekendExcludeds) {
					weekendMap.put(loclDtd, loclDtd);
				}

			}
			if (weekendExcludeds != null && weekendExcludeds.size() > 0) {
				leaveProcessVO.setExcludedWeekendDays(weekendMap.size() + "");
				leaveProcessVO.setWeekendMap(weekendMap);
			}

			Map<LocalDate, LocalDate> holidaysMap = new HashMap<LocalDate, LocalDate>();
			if (leavePolicy != null && leavePolicy.getIsHolidayExcluded()) {
				List<LeaveProcessVO> leaveProcessVOs = new ArrayList<LeaveProcessVO>();
				for (EmployeeCalendarPeriod employeeCalendarPeriod : employeeCalendar
						.getEmployeeCalendarPeriods()) {
					LeaveProcessVO calendarPeriod = new LeaveProcessVO();
					calendarPeriod.setFromDate(employeeCalendarPeriod
							.getFromDate());
					calendarPeriod
							.setToDate(employeeCalendarPeriod.getToDate());
					leaveProcessVOs.add(calendarPeriod);
				}

				for (LeaveProcessVO employeeCalendarPeriod : leaveProcessVOs) {
					boolean exsitFlag = false;

					if (!DateFormat.compareDates(
							employeeCalendarPeriod.getToDate(),
							leaveProcess.getFromDate())
							&& DateFormat.compareDates(
									employeeCalendarPeriod.getFromDate(),
									leaveProcess.getToDate())) {
						exsitFlag = true;
					} else if (DateFormat.compareDates(
							employeeCalendarPeriod.getToDate(),
							leaveProcess.getFromDate())
							&& DateFormat.compareDates(
									employeeCalendarPeriod.getFromDate(),
									leaveProcess.getToDate())) {
						if (DateFormat.compareDates(
								employeeCalendarPeriod.getFromDate(),
								leaveProcess.getFromDate())
								&& DateFormat.compareDates(
										employeeCalendarPeriod.getFromDate(),
										leaveProcess.getToDate()))
							exsitFlag = true;
					}

					if (DateFormat.compareDates(
							employeeCalendarPeriod.getFromDate(),
							leaveProcess.getFromDate())) {
						employeeCalendarPeriod.setFromDate(leaveProcess
								.getFromDate());
					}

					if (!DateFormat.compareDates(
							employeeCalendarPeriod.getToDate(),
							leaveProcess.getToDate())) {
						employeeCalendarPeriod.setToDate(leaveProcess
								.getToDate());
					}

					if (exsitFlag) {
						LocalDate holidaystart = new LocalDate(
								employeeCalendarPeriod.getFromDate());
						LocalDate holidayend = new LocalDate(
								employeeCalendarPeriod.getToDate());
						int holidaydays = Days.daysBetween(holidaystart,
								holidayend).getDays() + 1;
						for (int i = 0; i < holidaydays; i++) {
							LocalDate d = holidaystart.withFieldAdded(
									DurationFieldType.days(), i);
							if (!weekendMap.containsKey(d)) {
								holidaysMap.put(d, d);
							}
						}
					}
				}
			}

			if (holidaysMap != null && holidaysMap.size() > 0) {
				leaveProcessVO.setExcludedHolidays(holidaysMap.size() + "");
				leaveProcessVO.setHolidayMap(holidaysMap);
			}
			numberofDays += dates.size();
			numberofDays -= holidaysMap.size();
			numberofDays -= weekendMap.size();
			leaveProcessVO.setTotalDays(numberofDays + "");

		}
		return leaveProcessVO;
	}

	public LeaveReconciliationVO leaveReconciliationEntityToVO(
			LeaveReconciliation leaveReconciliation) {
		LeaveReconciliationVO leaveReconciliationVO = new LeaveReconciliationVO();

		leaveReconciliationVO.setCarryForwardedCount(leaveReconciliation
				.getCarryForwardedCount());
		leaveReconciliationVO.setCreatedDate(leaveReconciliation
				.getCreatedDate());
		leaveReconciliationVO.setEligibleDays(leaveReconciliation
				.getEligibleDays());
		leaveReconciliationVO.setEndDate(leaveReconciliation.getEndDate());
		leaveReconciliationVO.setImplementation(leaveReconciliation
				.getImplementation());
		leaveReconciliationVO.setIsActive(leaveReconciliation.getIsActive());
		leaveReconciliationVO.setJobAssignment(leaveReconciliation
				.getJobAssignment());
		leaveReconciliationVO.setJobLeave(leaveReconciliation.getJobLeave());
		leaveReconciliationVO.setLeaveReconciliationId(leaveReconciliation
				.getLeaveReconciliationId());
		leaveReconciliationVO.setStartDate(leaveReconciliation.getStartDate());
		leaveReconciliationVO
				.setTakendDays(leaveReconciliation.getTakendDays());
		leaveReconciliationVO.setProvidedDays(leaveReconciliation
				.getProvidedDays());

		return leaveReconciliationVO;
	}

	public LeaveReconciliation leaveReconciliationVOToEntity(
			LeaveReconciliationVO leaveReconciliationVO) {
		LeaveReconciliation leaveReconciliation = null;
		if (leaveReconciliationVO != null) {
			leaveReconciliation = new LeaveReconciliation();
			leaveReconciliation.setCarryForwardedCount(leaveReconciliationVO
					.getCarryForwardedCount());
			leaveReconciliation.setCreatedDate(leaveReconciliationVO
					.getCreatedDate());
			leaveReconciliation.setEligibleDays(leaveReconciliationVO
					.getEligibleDays());
			leaveReconciliation.setEndDate(leaveReconciliationVO.getEndDate());
			leaveReconciliation.setImplementation(leaveReconciliationVO
					.getImplementation());
			leaveReconciliation
					.setIsActive(leaveReconciliationVO.getIsActive());
			leaveReconciliation.setJobAssignment(leaveReconciliationVO
					.getJobAssignment());
			leaveReconciliation
					.setJobLeave(leaveReconciliationVO.getJobLeave());
			leaveReconciliation.setLeaveReconciliationId(leaveReconciliationVO
					.getLeaveReconciliationId());
			leaveReconciliation.setStartDate(leaveReconciliationVO
					.getStartDate());
			leaveReconciliation.setTakendDays(leaveReconciliationVO
					.getTakendDays());
			leaveReconciliation.setProvidedDays(leaveReconciliationVO
					.getProvidedDays());
		}
		return leaveReconciliation;
	}

	public String numberOfDays(Date from, Date to, boolean halfday) {

		Double diffInDays = new Double(Math.abs((int) ((from.getTime() - to
				.getTime()) / (1000 * 60 * 60 * 24))) + 1);
		if (halfday) {
			diffInDays = (diffInDays - 0.5);
		}
		return diffInDays + "";
	}

	public int numberofMonth(Date from, Date to) {
		Calendar firstDate = Calendar.getInstance();
		firstDate.setTime(from);
		Calendar secondDate = Calendar.getInstance();
		secondDate.setTime(to);
		int months = (firstDate.get(Calendar.YEAR) - secondDate
				.get(Calendar.YEAR))
				* 12
				+ (firstDate.get(Calendar.MONTH) - secondDate
						.get(Calendar.MONTH))
				+ (firstDate.get(Calendar.DAY_OF_MONTH) >= secondDate
						.get(Calendar.DAY_OF_MONTH) ? 0 : -1);

		return months;
	}

	public void updateLeaveReconcilation() {
		List<JobAssignment> jobAssignments = jobAssignmentBL
				.getJobAssignmentService().getAllJobAssignment(
						getImplementation());
		List<LeaveReconciliation> leaveReconciliations = new ArrayList<LeaveReconciliation>();
		LeaveReconciliation leaveReconciliation = null;
		for (JobAssignment jobAssignment : jobAssignments) {
			LocalDate startDate = new LocalDate(2014, 6, 1);
			LocalDate joininDate = new LocalDate(jobAssignment.getPerson()
					.getValidFromDate());
			int day = joininDate.getDayOfMonth();
			LocalDate endDate = new LocalDate(2014, 12, day);
			if (endDate.isBefore(joininDate) || endDate.equals(joininDate))
				continue;

			if (joininDate.isAfter(startDate) || joininDate.equals(startDate))
				startDate = joininDate;

			LocalDate thisFriday = startDate
					.withDayOfWeek(DateTimeConstants.FRIDAY);

			LocalDate startDate1 = new LocalDate(startDate);
			LocalDate endDate1 = new LocalDate(endDate);

			if (startDate.isAfter(thisFriday)) {
				startDate = thisFriday.plusWeeks(1); // start on next
														// thisFriday
			} else {
				startDate = thisFriday; // start on this thisFriday
			}
			Double count = 0.0;
			while (startDate.isBefore(endDate)) {
				startDate = startDate.plusWeeks(1);
				count++;
			}

			startDate = new LocalDate(2014, 6, 1);
			endDate = new LocalDate(2014, 12, day);

			if (joininDate.isAfter(startDate) || joininDate.equals(startDate))
				startDate = joininDate;

			Double monthCount = 0.0;
			while (startDate.isBefore(endDate)) {
				startDate = startDate.plusMonths(1);
				monthCount++;
			}

			Double total = monthCount * 2.5;
			for (JobLeave jobLeave : jobAssignment.getJob().getJobLeaves()) {
				if (!jobLeave.getIsActive())
					continue;

				leaveReconciliation = new LeaveReconciliation();

				if ((long) jobLeave.getLeave().getLeaveId() == (long) 80) {
					leaveReconciliation.setJobLeave(jobLeave);
					leaveReconciliation.setLeave(jobLeave.getLeave());
					leaveReconciliation.setJobAssignment(jobAssignment);
					leaveReconciliation.setImplementation(getImplementation());
					leaveReconciliation.setIsActive(true);
					leaveReconciliation.setProvidedDays(total);
					leaveReconciliation.setEligibleDays(total);
					leaveReconciliation.setTakendDays(0.0);
					leaveReconciliation.setStartDate(startDate1.toDate());
					leaveReconciliation.setEndDate(endDate1.toDate());
					leaveReconciliation.setCreatedDate(new Date());
					leaveReconciliations.add(leaveReconciliation);
				} else if ((long) jobLeave.getLeave().getLeaveId() == (long) 83) {
					leaveReconciliation.setJobLeave(jobLeave);
					leaveReconciliation.setLeave(jobLeave.getLeave());
					leaveReconciliation.setJobAssignment(jobAssignment);
					leaveReconciliation.setImplementation(getImplementation());
					leaveReconciliation.setIsActive(true);
					leaveReconciliation.setProvidedDays(count);
					leaveReconciliation.setEligibleDays(count);
					leaveReconciliation.setTakendDays(0.0);
					leaveReconciliation.setStartDate(startDate1.toDate());
					leaveReconciliation.setEndDate(endDate1.toDate());
					leaveReconciliation.setCreatedDate(new Date());
					leaveReconciliations.add(leaveReconciliation);
				}

			}
		}
		leaveProcessService.saveLeaveReconciliations(leaveReconciliations);
	}

	public void reconciliationTableUpdate() {
		List<JobAssignment> jobAssignments = this.getJobAssignmentBL()
				.getJobAssignmentService()
				.getAllActiveJobAssignment(getImplementation());

		for (JobAssignment jobAssignment : jobAssignments) {
			Map<String, JobLeave> jobLeaveMap = new HashMap<String, JobLeave>();
			for (JobLeave jL : jobAssignment.getJob().getJobLeaves()) {
				if ((long) jL.getLeave().getLeaveId() == (long) 80)
					jobLeaveMap.put(jobAssignment.getJobAssignmentId() + "_"
							+ jL.getLeave().getLeaveId(), jL);
			}
			List<LeaveReconciliation> leaveReconciliations = leaveProcessService
					.getLeaveReconciliation(jobAssignment);
			for (LeaveReconciliation leaveReconciliation : leaveReconciliations) {
				String key = leaveReconciliation.getJobAssignment()
						.getJobAssignmentId()
						+ "_"
						+ leaveReconciliation.getLeave().getLeaveId();
				if (jobLeaveMap.containsKey(key))
					leaveReconciliation.setJobLeave(jobLeaveMap.get(key));
			}

			leaveProcessService.saveLeaveReconciliations(leaveReconciliations);
		}
	}

	public List<PayrollTransactionVO> recursivePayroll(
			ResignationTerminationVO vo, PayrollVO payrollVO,
			List<PayrollTransactionVO> payrollTransactionVOs) throws Exception {
		PayPeriodVO payPVo = new PayPeriodVO();
		JobAssignment jobAssignment = jobAssignmentBL.getJobAssignmentService()
				.getJobAssignmentInfo(vo.getJobAssignmentId());

		payPVo.setJobAssignmentId(vo.getJobAssignmentId());
		payPVo.setTransactionDate(vo.getEffectiveDate());
		boolean callFlag = true;
		List<PayPeriodTransaction> payPeriodTransactions = this.getPayrollBL()
				.getPayPeriodBL().getPayPeriodService().findPayPeriod(payPVo);
		if (payPeriodTransactions != null && payPeriodTransactions.size() > 0) {
			List<Payroll> payrolls = this
					.getPayrollBL()
					.getPayrollService()
					.getPayrollBasedonPayMonth(jobAssignment.getPerson(),
							payPeriodTransactions.get(0).getPayMonth());

			if (payrolls != null && payrolls.size() > 0)
				callFlag = false;
		}

		if (callFlag)
			payrollVO = this.getPayrollBL().getPendingSalaryForFinalSettlement(
					vo);
		else
			return null;

		PayrollElement encashElement = this.getPayrollBL()
				.getPayrollElementBL().getPayrollElementService()
				.getPayrollElementByCode(getImplementation(), "SALARY");
		PayrollTransactionVO payrollTransaction = new PayrollTransactionVO();
		if (payrollVO.getPayroll().getNetAmount() != null) {
			payrollVO.getPayroll().setNetAmount(
					Math.abs(payrollVO.getPayroll().getNetAmount()));
		}

		payrollTransaction.setPayMonth(payrollVO.getPayPeriodTransaction()
				.getPayMonth());
		payrollTransaction.setFromDate(DateFormat.convertDateToString(payrollVO
				.getPayroll().getStartDate() + ""));
		payrollTransaction.setToDate(DateFormat.convertDateToString(payrollVO
				.getPayroll().getEndDate() + ""));
		payrollTransaction.setStartDate(payrollVO.getPayroll().getStartDate());
		payrollTransaction.setEndDate(payrollVO.getPayroll().getEndDate());
		payrollTransaction.setPayPeriodTransaction(payrollVO
				.getPayPeriodTransaction());
		Integer numberOfDays = 1;
		numberOfDays = numberOfDays
				+ (int) DateFormat.dayDifference(payrollVO.getPayroll()
						.getStartDate(), payrollVO.getPayroll().getEndDate());
		if (payrollVO.getPayroll().getNumberOfDays() != numberOfDays) {
			Double amount = payrollVO.getPayroll().getNetAmount();
			amount = (amount / payrollVO.getPayroll().getNumberOfDays())
					* numberOfDays;
			payrollTransaction.setNumberOfDays(Double.valueOf(numberOfDays));
			payrollTransaction.setAmount(AIOSCommons.roundDecimals(amount));
			payrollTransaction.setCalculatedAmount(AIOSCommons
					.roundDecimals(amount));
		} else {
			payrollTransaction.setNumberOfDays(Double.valueOf(payrollVO
					.getPayroll().getNumberOfDays()));
			payrollTransaction.setAmount(AIOSCommons.roundDecimals(Double
					.valueOf(payrollVO.getPayroll().getNetAmount())));
			payrollTransaction.setCalculatedAmount(AIOSCommons
					.roundDecimals(Double.valueOf(payrollVO.getPayroll()
							.getNetAmount())));
		}
		payrollTransaction.setPayrollElement(encashElement);
		payrollTransaction.setStatus(Constants.HR.PayTransactionStatus.New
				.getCode());

		payrollTransaction.setNote("Salary for the month of("
				+ payrollVO.getPayPeriodTransaction().getPayMonth() + ")  ("
				+ numberOfDays + ") days");
		payrollTransactionVOs.add(payrollTransaction);
		LocalDate cutOff = new LocalDate(vo.getEffectiveDate());
		LocalDate periodEnd = new LocalDate(payrollVO.getPayPeriodTransaction()
				.getEndDate());
		if (cutOff.isAfter(periodEnd)) {
			vo.setPayroll(payrollVO.getPayroll());
			payrollVO = new PayrollVO();
			recursivePayroll(vo, payrollVO, payrollTransactionVOs);
		} else {
			payrollTransaction.setEndDate(cutOff.toDate());
		}

		return payrollTransactionVOs;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public LeaveProcessService getLeaveProcessService() {
		return leaveProcessService;
	}

	public void setLeaveProcessService(LeaveProcessService leaveProcessService) {
		this.leaveProcessService = leaveProcessService;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public AttendanceBL getAttendanceBL() {
		return attendanceBL;
	}

	public void setAttendanceBL(AttendanceBL attendanceBL) {
		this.attendanceBL = attendanceBL;
	}

	public JobAssignmentBL getJobAssignmentBL() {
		return jobAssignmentBL;
	}

	public void setJobAssignmentBL(JobAssignmentBL jobAssignmentBL) {
		this.jobAssignmentBL = jobAssignmentBL;
	}

	public EmployeeCalendarBL getEmployeeCalendarBL() {
		return employeeCalendarBL;
	}

	public void setEmployeeCalendarBL(EmployeeCalendarBL employeeCalendarBL) {
		this.employeeCalendarBL = employeeCalendarBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public PersonBL getPersonBL() {
		return personBL;
	}

	public void setPersonBL(PersonBL personBL) {
		this.personBL = personBL;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public PayrollBL getPayrollBL() {
		return payrollBL;
	}

	public void setPayrollBL(PayrollBL payrollBL) {
		this.payrollBL = payrollBL;
	}

}
