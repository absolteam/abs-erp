package com.aiotech.aios.hr.service.bl;

import java.util.Map;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.Designation;
import com.aiotech.aios.hr.service.CompanyService;
import com.aiotech.aios.hr.service.DesignationService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class DesignationBL {

	DesignationService designationService;
	CompanyService companyService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public String saveDesignation(Designation designation) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		designation.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (designation != null && designation.getDesignationId() != null
				&& designation.getDesignationId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		designationService.saveDesignation(designation);

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Designation.class.getSimpleName(),
				designation.getDesignationId(), user, workflowDetailVo);
		return "SUCCESS";
	}

	public void deleteDesignation(Long desingationId) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Designation.class.getSimpleName(), desingationId, user,
				workflowDetailVo);
	}

	public void doDesignationBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		Designation designation = this.getDesignationService()
				.getDesignationInfo(recordId);

		if (workflowDetailVO.isDeleteFlag()) {
			try {
				this.getDesignationService().deleteDesignation(designation);
			} catch (org.hibernate.exception.ConstraintViolationException cve) {
				designation.setIsActive(false);
				designation
						.setIsApprove((byte) WorkflowConstants.Status.Deleted
								.getCode());
				this.saveDesignation(designation);
			}
		} else {
		}
	}

	public DesignationService getDesignationService() {
		return designationService;
	}

	public void setDesignationService(DesignationService designationService) {
		this.designationService = designationService;
	}

	public CompanyService getCompanyService() {
		return companyService;
	}

	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
