package com.aiotech.aios.hr.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.aiotech.aios.hr.domain.entity.AcademicQualifications;
import com.aiotech.aios.hr.domain.entity.Dependent;
import com.aiotech.aios.hr.domain.entity.Experiences;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.PersonType;
import com.aiotech.aios.hr.domain.entity.PersonTypeAllocation;
import com.aiotech.aios.hr.domain.entity.Skills;
import com.aiotech.aios.system.domain.entity.Country;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class PersonService {
	private AIOTechGenericDAO<Person> personDAO;
	private AIOTechGenericDAO<PersonType> personTypeDAO;
	private AIOTechGenericDAO<Country> countryDAO;
	private AIOTechGenericDAO<Identity> identityDAO;
	private AIOTechGenericDAO<Dependent> dependentDAO;
	private AIOTechGenericDAO<PersonTypeAllocation> personTypeAllocationDAO;
	private AIOTechGenericDAO<AcademicQualifications> academicQualificationsDAO;
	private AIOTechGenericDAO<Experiences> experiencesDAO;
	private AIOTechGenericDAO<Skills> skillsDAO;

	public List<Person> getAllPerson(Implementation implementation)
			throws Exception {
		List<Person> persList = new ArrayList<Person>();
		persList = personDAO.findByNamedQuery("getAllPerson", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
		/*
		 * PersonType personType=new PersonType(); if (persList != null &&
		 * persList.size() > 0) { for (int i = 0; i < persList.size(); i++) {
		 * personType = personTypeDAO.findById(persList.get(i)
		 * .getPersonType().getPersonTypeId());
		 * persList.get(i).setPersonType(personType); } }
		 */
		return persList;

	}

	public List<Person> getAllPersonWithoutStatus(Implementation implementation)
			throws Exception {
		return personDAO.findByNamedQuery("getAllPersonWithoutStatus",
				implementation);
	}

	public List<Person> getCommonPersonList(String queryStr) throws Exception {
		Query qry = personDAO.getNamedQuery("getCommonPersonList");
		queryStr = qry.getQueryString() + queryStr;
		return personDAO.find(queryStr);

	}

	public List<Person> getPersonListForReport(String queryStr)
			throws Exception {
		Query qry = personDAO.getNamedQuery("getPersonListForReport");
		queryStr = qry.getQueryString() + queryStr;
		return personDAO.find(queryStr);

	}

	public List<Person> getAllSupplierPerson(Implementation implementation) {

		return personDAO.findByNamedQuery("getAllSupplierPerson",
				implementation);
	}

	public List<Person> getAllEmployeeBasedOnCompany(Long companyId) {

		return personDAO.findByNamedQuery("getAllEmployeeBasedOnCompany",
				companyId);
	}

	public List<Person> getAllEmployee(Implementation implementation) {

		return personDAO.findByNamedQuery("getAllEmployee", implementation);
	}

	public List<Person> getAllEmployeeForJob(Implementation implementation) {

		return personDAO.findByNamedQuery("getAllEmployeeForJob",
				implementation);
	}

	public List<Person> getAllEmployeeBasedOnJob(Implementation implementation) {

		return personDAO.findByNamedQuery("getAllEmployee", implementation);
	}

	public Country getCountry(Long countryId) throws Exception {

		return countryDAO.findById(countryId);
	}

	public PersonType getPersonType(int personTypeId) throws Exception {

		return personTypeDAO.findById(personTypeId);
	}

	public Person getPersonDetails(Long personId) throws Exception {
		List<Person> persons = new ArrayList<Person>();
		persons = personDAO.findByNamedQuery("getPersonalDetails", personId);
		if (persons != null && persons.size() > 0)
			return persons.get(0);
		else
			return null;
	}

	public List<Country> getAllCountry() throws Exception {
		return countryDAO.getAll();
	}

	public List<PersonType> getAllPersonType() throws Exception {
		return personTypeDAO.getAll();
	}

	public void savePerson(Person person, List<Identity> identitys,
			List<Dependent> dependents,
			List<AcademicQualifications> qualifications,
			List<Experiences> experiences, List<Skills> skills)
			throws Exception {
		personDAO.saveOrUpdate(person);
		if (identitys != null)
			for (Identity identity : identitys) {
				identity.setPerson(person);
			}
		if (dependents != null)
			for (Dependent dependent : dependents) {
				dependent.setPerson(person);
			}
		if (qualifications != null)
			for (AcademicQualifications qualification : qualifications) {
				qualification.setPerson(person);
			}
		if (experiences != null)
			for (Experiences experience : experiences) {
				experience.setPerson(person);
			}
		if (skills != null)
			for (Skills skill : skills) {
				skill.setPerson(person);
			}
		if (identitys != null)
			identityDAO.saveOrUpdateAll(identitys);
		if (dependents != null)
			dependentDAO.saveOrUpdateAll(dependents);
		if (qualifications != null)
			academicQualificationsDAO.saveOrUpdateAll(qualifications);
		if (experiences != null)
			experiencesDAO.saveOrUpdateAll(experiences);
		if (skills != null)
			skillsDAO.saveOrUpdateAll(skills);
	}

	public void deletePerson(Person person, List<Identity> identitys,
			List<Dependent> dependents,
			List<AcademicQualifications> qualifications,
			List<Experiences> experiences, List<Skills> skills,
			List<PersonTypeAllocation> personTypeAllocations) throws Exception {
		if (identitys != null && identitys.size() > 0)
			identityDAO.deleteAll(identitys);
		if (dependents != null && dependents.size() > 0)
			dependentDAO.deleteAll(dependents);
		if (skills != null && skills.size() > 0)
			skillsDAO.deleteAll(skills);
		if (qualifications != null && qualifications.size() > 0)
			academicQualificationsDAO.deleteAll(qualifications);
		if (experiences != null && experiences.size() > 0)
			experiencesDAO.deleteAll(experiences);
		if (personTypeAllocations != null && personTypeAllocations.size() > 0)
			personTypeAllocationDAO.deleteAll(personTypeAllocations);
		personDAO.delete(person);

	}

	public void deletePersonDetail(Person person,
			List<PersonTypeAllocation> personTypeAllocations,
			List<Identity> identitys,
			List<AcademicQualifications> qualifications,
			List<Experiences> experiences, List<Skills> skills,
			List<Dependent> dependents) throws Exception {
		if (null != identitys && identitys.size() > 0)
			identityDAO.deleteAll(identitys);
		if (null != qualifications && qualifications.size() > 0)
			academicQualificationsDAO.deleteAll(qualifications);
		if (null != experiences && experiences.size() > 0)
			experiencesDAO.deleteAll(experiences);
		if (null != skills && skills.size() > 0)
			skillsDAO.deleteAll(skills);
		if (null != dependents && dependents.size() > 0)
			dependentDAO.deleteAll(dependents);
		personTypeAllocationDAO.deleteAll(personTypeAllocations);
		personDAO.delete(person);

	}

	public void deleteIdentityAll(List<Identity> identitys) throws Exception {
		identityDAO.deleteAll(identitys);
	}

	public void deleteDependentAll(List<Dependent> dependents) throws Exception {
		dependentDAO.deleteAll(dependents);
	}

	public void deleteQualificationAll(
			List<AcademicQualifications> qualifications) throws Exception {
		academicQualificationsDAO.deleteAll(qualifications);
	}

	public void deleteExperienceAll(List<Experiences> experienceses)
			throws Exception {
		experiencesDAO.deleteAll(experienceses);
	}

	public void deleteSkillAll(List<Skills> skills) throws Exception {
		skillsDAO.deleteAll(skills);
	}

	public List<Identity> getAllIdentity(Person person) throws Exception {
		return identityDAO.findByNamedQuery("getAllIdentity", person);
	}

	public List<Identity> getIdentityExpiry(Date currentDate, Date date)
			throws Exception {
		return identityDAO.findByNamedQuery("getIdentityExpiry", currentDate,
				date);
	}

	public List<Identity> getAllIdentityOnlyByImplementation(
			Implementation implementation) throws Exception {
		return identityDAO.findByNamedQuery(
				"getAllIdentityOnlyByImplementation", implementation);
	}

	public List<AcademicQualifications> getAllAcademicQualifications(
			Person person) throws Exception {
		return academicQualificationsDAO.findByNamedQuery(
				"getAllAcademicQualifications", person);
	}

	public List<Experiences> getAllExperiences(Person person) throws Exception {
		return experiencesDAO.findByNamedQuery("getAllExperiences", person);
	}

	public List<Skills> getAllSkills(Person person) throws Exception {
		return skillsDAO.findByNamedQuery("getAllSkills", person);
	}

	public List<Dependent> getAllDependent(Person person) throws Exception {
		return dependentDAO.findByNamedQuery("getAllDependent", person);
	}

	public void savePersonTypes(List<PersonTypeAllocation> persontypes)
			throws Exception {
		personTypeAllocationDAO.saveOrUpdateAll(persontypes);
	}

	public void deletePersonTypes(List<PersonTypeAllocation> persontypes)
			throws Exception {
		personTypeAllocationDAO.deleteAll(persontypes);
	}

	public void savePersonType(PersonTypeAllocation persontype)
			throws Exception {
		personTypeAllocationDAO.saveOrUpdate(persontype);
	}

	public void deletePersonType(PersonTypeAllocation persontype)
			throws Exception {
		personTypeAllocationDAO.delete(persontype);
	}

	public List<PersonTypeAllocation> getAllPersonTypeAllocation(Person person)
			throws Exception {
		return personTypeAllocationDAO.findByNamedQuery(
				"getAllPersonTypeAllocation", person);
	}

	public List<Person> getPersonsByAssignment(String jobAssignmentIdsTemp) {
		List<Long> jobAssignmentIds = new ArrayList<Long>();
		for (String string : jobAssignmentIdsTemp.split(",")) {
			if (string != null && !string.equals(""))
				jobAssignmentIds.add(Long.valueOf(string));
		}
		Query query = personDAO.getNamedQuery("getPersonsByAssignment");
		query.setParameterList("jobAssignmentIds", jobAssignmentIds);
		return query.list();

	}

	public List<Person> getPersonByIdWithDesignation(Long personId)
			throws Exception {
		return personDAO.findByNamedQuery("getPersonsByIdWithDesignation",
				personId);
	}

	public void saveCustomerPerson(Person person) throws Exception {
		personDAO.saveOrUpdate(person);
	}

	public void savePersons(List<Person> persons) throws Exception {
		personDAO.saveOrUpdateAll(persons);
	}

	public Person getPersonByCustomerId(long customerId) {
		return personDAO.findByNamedQuery("getPersonByCustomerId", customerId)
				.get(0);
	}

	public Person getPersonBySecretPin(Implementation implementation,
			String employeeId, String employeeSecretPin) {
		List<Person> persons = personDAO.findByNamedQuery(
				"getPersonBySecretPin", implementation, employeeId,
				employeeSecretPin);
		return null != persons && persons.size() > 0 ? persons.get(0) : null;
	}

	public List<Person> getAllPersonWithOutImplementation() {

		return personDAO.getAll();
	}

	// Getter & Setter
	public AIOTechGenericDAO<Person> getPersonDAO() {
		return personDAO;
	}

	public void setPersonDAO(AIOTechGenericDAO<Person> personDAO) {
		this.personDAO = personDAO;
	}

	public AIOTechGenericDAO<PersonType> getPersonTypeDAO() {
		return personTypeDAO;
	}

	public void setPersonTypeDAO(AIOTechGenericDAO<PersonType> personTypeDAO) {
		this.personTypeDAO = personTypeDAO;
	}

	public AIOTechGenericDAO<Country> getCountryDAO() {
		return countryDAO;
	}

	public void setCountryDAO(AIOTechGenericDAO<Country> countryDAO) {
		this.countryDAO = countryDAO;
	}

	public Person getByPersonId(long personId) {
		return personDAO.findById(personId);
	}

	public Person getSelectedPersons(Person person) {
		return personDAO.findById(person.getPersonId());
	}

	public AIOTechGenericDAO<Identity> getIdentityDAO() {
		return identityDAO;
	}

	public void setIdentityDAO(AIOTechGenericDAO<Identity> identityDAO) {
		this.identityDAO = identityDAO;
	}

	public AIOTechGenericDAO<Dependent> getDependentDAO() {
		return dependentDAO;
	}

	public void setDependentDAO(AIOTechGenericDAO<Dependent> dependentDAO) {
		this.dependentDAO = dependentDAO;
	}

	public AIOTechGenericDAO<PersonTypeAllocation> getPersonTypeAllocationDAO() {
		return personTypeAllocationDAO;
	}

	public void setPersonTypeAllocationDAO(
			AIOTechGenericDAO<PersonTypeAllocation> personTypeAllocationDAO) {
		this.personTypeAllocationDAO = personTypeAllocationDAO;
	}

	public AIOTechGenericDAO<AcademicQualifications> getAcademicQualificationsDAO() {
		return academicQualificationsDAO;
	}

	public void setAcademicQualificationsDAO(
			AIOTechGenericDAO<AcademicQualifications> academicQualificationsDAO) {
		this.academicQualificationsDAO = academicQualificationsDAO;
	}

	public AIOTechGenericDAO<Experiences> getExperiencesDAO() {
		return experiencesDAO;
	}

	public void setExperiencesDAO(AIOTechGenericDAO<Experiences> experiencesDAO) {
		this.experiencesDAO = experiencesDAO;
	}

	public AIOTechGenericDAO<Skills> getSkillsDAO() {
		return skillsDAO;
	}

	public void setSkillsDAO(AIOTechGenericDAO<Skills> skillsDAO) {
		this.skillsDAO = skillsDAO;
	}
}
