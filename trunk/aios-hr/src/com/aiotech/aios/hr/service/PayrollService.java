package com.aiotech.aios.hr.service;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.ApplicationContext;
import org.springframework.util.StringUtils;
import org.springframework.web.context.ContextLoader;

import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.Payroll;
import com.aiotech.aios.hr.domain.entity.PayrollElement;
import com.aiotech.aios.hr.domain.entity.PayrollTransaction;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.PayrollVO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class PayrollService {
	AIOTechGenericDAO<Payroll> payrollDAO;
	AIOTechGenericDAO<PayrollTransaction> payrollTransactionDAO;
	AIOTechGenericDAO<PayrollElement> payrollElementDAO;
	AIOTechGenericDAO<Object> dao;
	AIOTechGenericDAO<BankAccount> bankAccountDAO;

	public List<Payroll> getAllPayroll(Implementation implementation) {
		return payrollDAO.findByNamedQuery("getAllPayroll", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<Payroll> getAllPayForAdjustment(Implementation implementation) {
		return payrollDAO.findByNamedQuery("getAllPayForAdjustment",
				implementation);
	}

	public List<Payroll> getAllPayrollToEmployee(Person person) {
		return payrollDAO.findByNamedQuery("getAllPayrollToEmployee", person,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public Payroll getAllPayrollToWPS(JobAssignment jobAssignment,
			String payMonth) {
		List<Payroll> parolls = new ArrayList<Payroll>();
		parolls = payrollDAO.findByNamedQuery("getAllPayrollToWPS",
				jobAssignment, payMonth);
		if (parolls != null && parolls.size() > 0)
			return parolls.get(0);
		else
			return null;
	}

	public List<Payroll> getAllCommittedPayrolls(Implementation implementation) {
		/*
		 * return payrollDAO.findByNamedQuery("getAllCommittedPayrolls",
		 * implementation,Constants.HR.PayStatus.Commit.getCode());
		 */
		return payrollDAO.findByNamedQuery("getAllPayroll", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());

	}

	public List<Payroll> getAllPayroll(Implementation implementation,
			String payMonth) {
		return payrollDAO.findByNamedQuery("getAllPayrollForMonth",
				implementation, payMonth,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<PayrollElement> getAllPayrollElement(
			Implementation implementation) {
		return payrollElementDAO.findByNamedQuery("getAllPayrollElement",
				implementation);
	}

	public Payroll getPayrollInfo(Long payrollId) {
		List<Payroll> payrolls = new ArrayList<Payroll>();
		payrolls = payrollDAO.findByNamedQuery("getPayrollInfo", payrollId);
		if (payrolls != null && payrolls.size() > 0)
			return payrolls.get(0);
		else
			return null;
	}

	public Payroll getPayrollInfoPlain(Long payrollId) {
		List<Payroll> payrolls = new ArrayList<Payroll>();
		payrolls = payrollDAO
				.findByNamedQuery("getPayrollInfoPlain", payrollId);
		if (payrolls != null && payrolls.size() > 0)
			return payrolls.get(0);
		else
			return null;
	}

	public Payroll getPayrollInfoFindById(Long payrollId) {
		return payrollDAO.findById(payrollId);
	}

	public Payroll getPayrollDetail(Long payrollId) {
		List<Payroll> payrolls = new ArrayList<Payroll>();
		payrolls = payrollDAO.findByNamedQuery("getPayrollDetail", payrollId,
				Constants.HR.PayTransactionStatus.Suspended.getCode());
		if (payrolls != null && payrolls.size() > 0)
			return payrolls.get(0);
		else
			return null;
	}

	public Payroll getPayrollDetailByUseCase(Long recordId, String useCase) {
		List<Payroll> payrolls = new ArrayList<Payroll>();
		payrolls = payrollDAO.findByNamedQuery("getPayrollDetailByUseCase",
				recordId, useCase);
		if (payrolls != null && payrolls.size() > 0)
			return payrolls.get(0);
		else
			return null;
	}

	public Payroll getPayrollBasedonJob(JobAssignment jobAssignment, Date date) {
		List<Payroll> payrolls = new ArrayList<Payroll>();
		payrolls = payrollDAO.findByNamedQuery("getPayrollBasedonJob",
				jobAssignment, date);
		if (payrolls != null && payrolls.size() > 0)
			return payrolls.get(0);
		else
			return null;
	}

	public Payroll getPayrollBasedonPerson(Person person) {
		List<Payroll> payrolls = new ArrayList<Payroll>();
		payrolls = payrollDAO.findByNamedQuery("getPayrollBasedonPerson",
				person.getPersonId());
		if (payrolls != null && payrolls.size() > 0)
			return payrolls.get(0);
		else
			return null;
	}

	public Payroll getPayrollBasedonPersonAndMonth(Person person,
			String payMonth) {
		List<Payroll> payrolls = new ArrayList<Payroll>();
		payrolls = payrollDAO.findByNamedQuery(
				"getPayrollBasedonPersonAndMonth", person.getPersonId(),
				payMonth);
		if (payrolls != null && payrolls.size() > 0)
			return payrolls.get(0);
		else
			return null;
	}

	public List<Payroll> getPayrollBasedonSameCombination(Person person,
			Long payPeriodTransaction) {
		return payrollDAO.findByNamedQuery("getPayrollBasedonSameCombination",
				person.getPersonId(), payPeriodTransaction,
				Constants.HR.PayStatus.Commit.getCode());
	}

	public List<Payroll> getPayrollBasedonPayMonth(Person person,
			String payMonth) {
		return payrollDAO.findByNamedQuery("getPayrollBasedonPayMonth",
				person.getPersonId(),payMonth);
	}

	public List<Payroll> getAllPayrollBasedonPayMonth(Implementation implementation,String payMonth) {
		return payrollDAO.findByNamedQuery("getAllPayrollBasedonPayMonth",
				implementation,payMonth);
	}

	public List<Object> getPayrollTransactionList(Long payrollId) {
		return dao.findByNamedQuery("getPayrollTransactionList", payrollId,
				Constants.HR.PayTransactionStatus.Suspended.getCode());
		// payrollTransactionDAO.findByNamedQuery("getPayrollTransactionList",
		// payrollId);
	}

	public List<PayrollTransaction> getAllPayrollTransaction(Long payrollId) {
		return payrollTransactionDAO.findByNamedQuery(
				"getAllPayrollTransaction", payrollId);
		// payrollTransactionDAO.findByNamedQuery("getPayrollTransactionList",
		// payrollId);
	}

	public List<PayrollTransaction> getLOPPayrollTransaction(Long payrollId) {
		return payrollTransactionDAO.findByNamedQuery(
				"getLOPPayrollTransaction", payrollId);
		// payrollTransactionDAO.findByNamedQuery("getPayrollTransactionList",
		// payrollId);
	}

	public List<PayrollTransaction> getOTPayrollTransaction(Long payrollId) {
		return payrollTransactionDAO.findByNamedQuery(
				"getOTPayrollTransaction", payrollId);
		// payrollTransactionDAO.findByNamedQuery("getPayrollTransactionList",
		// payrollId);
	}

	public List<PayrollTransaction> getPayTransactionByPayPeriodId(
			JobAssignment jobAssignment, Long payPeriodId) {
		return payrollTransactionDAO.findByNamedQuery(
				"getPayTransactionByPayPeriodId", jobAssignment, payPeriodId);
		// payrollTransactionDAO.findByNamedQuery("getPayrollTransactionList",
		// payrollId);
	}

	public void savePayroll(Payroll payroll) {
		payrollDAO.saveOrUpdate(payroll);
	}

	public void saveAllPayroll(List<Payroll> payrolls) {
		payrollDAO.saveOrUpdateAll(payrolls);
	}

	public void savePayrollTransaction(
			List<PayrollTransaction> payrollTransactions) {
		payrollTransactionDAO.saveOrUpdateAll(payrollTransactions);
	}

	public void deletePayroll(Payroll payroll,
			List<PayrollTransaction> payrollTransaction) {
		payrollTransactionDAO.deleteAll(payrollTransaction);
		payrollDAO.delete(payroll);
	}

	public void deletePayrollTransaction(
			List<PayrollTransaction> payrollTransactions) {
		payrollTransactionDAO.deleteAll(payrollTransactions);
	}
	/*
	 * public List<Payroll> criteriaSample(Double amount,String payMonth){
	 * String queryStr="";//
	 * "SELECT p FROM Payroll p JOIN FETCH p.payrollTransactions pt where p.netAmount>="
	 * +amount+" AND p.payMonth='"+payMonth+"'";
	 * 
	 * Query qry=payrollDAO.getNamedQuery("getPayrollBasedonCriteria");
	 * queryStr=
	 * qry.getQueryString()+" where p.netAmount>="+amount+" AND p.payMonth='"
	 * +payMonth+"'"; return payrollDAO.find(queryStr); }
	 */

	public BankAccount getBankAccountInfo(Long bankAccountId) {
		return bankAccountDAO.findById(bankAccountId);
	}

	public List<PayrollTransaction> getPayrollTransactionsByDiffValues(
			Long payrollElementId, String payMonth,
			Implementation implementation, Byte isApprove) throws Exception {

		Criteria payrollTransactionCriteria = payrollTransactionDAO
				.createCriteria();

		payrollTransactionCriteria.createAlias("payrollElement", "ele")
				.setFetchMode("ele", FetchMode.EAGER);

		payrollTransactionCriteria
				.createAlias("payroll", "pr", CriteriaSpecification.INNER_JOIN)
				.setFetchMode("payroll.jobAssignment", FetchMode.EAGER)
				.setFetchMode("payroll.jobAssignment.person", FetchMode.EAGER)
				.setFetchMode("payroll.jobAssignment.designation",
						FetchMode.EAGER);

		if (payrollElementId != null) {
			payrollTransactionCriteria.add(Restrictions.eq(
					"ele.payrollElementId", payrollElementId));
		}

		if (implementation != null) {
			payrollTransactionCriteria.createAlias("ele.implementation", "imp",
					CriteriaSpecification.INNER_JOIN);
			payrollTransactionCriteria.add(Restrictions.eq(
					"ele.implementation", implementation));
		}

		if (payMonth != null) {
			payrollTransactionCriteria.add(Restrictions.eq("pr.payMonth",
					payMonth));
		}
		if (isApprove != null) {
			payrollTransactionCriteria.add(Restrictions.eq("pr.isApprove",
					isApprove));
		}
		payrollTransactionCriteria.add(Restrictions.eq("status",(byte)1));
		return payrollTransactionCriteria.list();

	}

	public List<Object> getAllowanceList(PayrollVO payrollVO) {
		try {
			/* Get EntityDAO object */
			AIOTechGenericDAO<Object> dao = getEntityDAO(payrollVO.getUseCase());
			Criteria jobPayrollCriteria = dao.createCriteria();
			jobPayrollCriteria.createAlias("jobAssignment", "ja");
			jobPayrollCriteria.createAlias("jobPayrollElement", "jpa");
			jobPayrollCriteria.createAlias(
					"jpa.payrollElementByPayrollElementId", "pe");
			jobPayrollCriteria.createAlias(
					"jpa.payrollElementByPercentageElement", "percentage",
					CriteriaSpecification.LEFT_JOIN);

			if (payrollVO.getPayPeriodTransaction() != null
					&& payrollVO.getPayPeriodTransaction().getStartDate() != null) {
				jobPayrollCriteria.add(Restrictions.conjunction().add(
						Restrictions.ge("fromDate", payrollVO
								.getPayPeriodTransaction().getStartDate())));
				jobPayrollCriteria.add(Restrictions.conjunction().add(
						Restrictions.le("toDate", payrollVO
								.getPayPeriodTransaction().getEndDate())));
			}

			if (payrollVO.getJobAssignment().getJobAssignmentId() != null) {
				jobPayrollCriteria.add(Restrictions.eq("ja.jobAssignmentId",
						payrollVO.getJobAssignment().getJobAssignmentId()));
			}

			return jobPayrollCriteria.list();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static AIOTechGenericDAO<Object> getEntityDAO(
			String entityClassFullyQualifiedName) throws Exception {

		String entityClassName = getEntityClassName(entityClassFullyQualifiedName);

		/* Get EntityService bean object from Context */
		Object serviceClassObject = getEntityService(entityClassName);

		/* Get EntityDAO object injected into Service */
		return getEntityDAO(entityClassName, serviceClassObject);
	}

	public static String getEntityClassName(String entityClassFullyQualifiedName)
			throws Exception {

		String[] splittedName = entityClassFullyQualifiedName.split("entity.");
		return splittedName[splittedName.length - 1];
	}

	private static AIOTechGenericDAO<Object> getEntityDAO(
			String entityClassName, Object entityServiceObject)
			throws Exception {

		String entityDAOGetter = "get" + entityClassName + "DAO";

		Method getDAOmethod = entityServiceObject.getClass().getMethod(
				entityDAOGetter, null);

		return (AIOTechGenericDAO<Object>) getDAOmethod
				.invoke(entityServiceObject);
	}

	public static Object getEntityService(String entityClassName)
			throws Exception {

		ApplicationContext ctx = ContextLoader
				.getCurrentWebApplicationContext();

		return ctx.getBean(StringUtils.uncapitalize(entityClassName)
				+ "Service");
	}

	// Getter & Setter
	public AIOTechGenericDAO<Payroll> getPayrollDAO() {
		return payrollDAO;
	}

	public void setPayrollDAO(AIOTechGenericDAO<Payroll> payrollDAO) {
		this.payrollDAO = payrollDAO;
	}

	public AIOTechGenericDAO<PayrollTransaction> getPayrollTransactionDAO() {
		return payrollTransactionDAO;
	}

	public void setPayrollTransactionDAO(
			AIOTechGenericDAO<PayrollTransaction> payrollTransactionDAO) {
		this.payrollTransactionDAO = payrollTransactionDAO;
	}

	public AIOTechGenericDAO<PayrollElement> getPayrollElementDAO() {
		return payrollElementDAO;
	}

	public void setPayrollElementDAO(
			AIOTechGenericDAO<PayrollElement> payrollElementDAO) {
		this.payrollElementDAO = payrollElementDAO;
	}

	public AIOTechGenericDAO<Object> getDao() {
		return dao;
	}

	public void setDao(AIOTechGenericDAO<Object> dao) {
		this.dao = dao;
	}

	public AIOTechGenericDAO<BankAccount> getBankAccountDAO() {
		return bankAccountDAO;
	}

	public void setBankAccountDAO(AIOTechGenericDAO<BankAccount> bankAccountDAO) {
		this.bankAccountDAO = bankAccountDAO;
	}

}
