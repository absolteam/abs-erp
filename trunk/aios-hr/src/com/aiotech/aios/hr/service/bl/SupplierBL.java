package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.SupplierVO;
import com.aiotech.aios.accounts.service.SupplierService;
import com.aiotech.aios.accounts.service.bl.CreditTermBL;
import com.aiotech.aios.accounts.service.bl.TransactionBL;
import com.aiotech.aios.common.to.Constants.Accounts.SupplierType;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class SupplierBL {

	private SupplierService supplierService;
	private CreditTermBL creditTermBL;
	private TransactionBL transactionBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public List<Supplier> getAllSuppliers(Implementation implementation) {
		try {
			List<Supplier> supplierList = supplierService
					.getAllSuppliers(implementation);
			return supplierList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<Object> getAllSuppliers(String pageInfo) throws Exception {
		List<Supplier> suppliers = null;
		if (null != pageInfo && !("").equals(pageInfo)
				&& ("bankdeposit").equalsIgnoreCase(pageInfo))
			suppliers = supplierService
					.getSuppliersByReceipts(getImplementationId());
		else
			suppliers = supplierService.getAllSuppliers(getImplementationId());
		List<Object> supplierVOs = new ArrayList<Object>();
		if (null != suppliers && suppliers.size() > 0) {
			for (Supplier supplier : suppliers)
				supplierVOs.add(addSupplierVO(supplier));
		}
		return supplierVOs;
	}

	public List<SupplierVO> getAllSuppliersVo(Implementation implementation) {
		try {
			List<Supplier> suppliers = supplierService
					.getAllSuppliers(implementation);

			List<SupplierVO> supplierVOs = new ArrayList<SupplierVO>();
			if (null != suppliers && suppliers.size() > 0) {
				for (Supplier supplier : suppliers)
					supplierVOs.add(addSupplierVO(supplier));
			}
			return supplierVOs;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public SupplierVO addSupplierVO(Supplier supplier) {
		SupplierVO supplierVO = new SupplierVO();
		supplierVO.setSupplierId(supplier.getSupplierId());
		supplierVO.setSupplierNumber(supplier.getSupplierNumber());
		supplierVO.setType(null != supplier.getSupplierType() ? SupplierType
				.get(supplier.getSupplierType()).name() : null);
		supplierVO.setCombinationId(supplier.getCombination()
				.getCombinationId());
		supplierVO.setAccountCode(supplier
				.getCombination()
				.getAccountByAnalysisAccountId()
				.getAccount()
				.concat("["
						+ supplier.getCombination()
								.getAccountByAnalysisAccountId().getCode()
						+ "]"));
		if ((null != supplier.getCmpDeptLocation() && !("").equals(supplier
				.getCmpDeptLocation()))
				|| (null != supplier.getCompany() && !("").equals(supplier
						.getCompany()))) {
			supplierVO
					.setSupplierName(null != supplier.getCmpDeptLocation() ? supplier
							.getCmpDeptLocation().getCompany().getCompanyName()
							: supplier.getCompany().getCompanyName());
			supplierVO.setSupplierTypeName("Company");
		} else {
			supplierVO.setSupplierName(supplier.getPerson().getFirstName()
					.concat(" ").concat(supplier.getPerson().getLastName()));
			supplierVO.setSupplierTypeName("Person");
		}
		if (supplier.getCreditTerm() != null)
			supplierVO.setCreditTermId(supplier.getCreditTerm()
					.getCreditTermId());

		return supplierVO;
	}

	public void saveSupplier(Supplier supplier, Long combinationId,
			Long currencyId) throws Exception {

		supplier.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (supplier != null && supplier.getSupplierId() != null
				&& supplier.getSupplierId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}

		this.getSupplierService().save(supplier);
		if (supplier.getOpeningBalance() > 0) {
			List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
			TransactionDetail transactionDetail = transactionBL
					.createTransactionDetail(supplier.getOpeningBalance(),
							getImplementationId().getExpenseAccount(),
							TransactionType.Debit.getCode(), null, null, null);
			transactionDetails.add(transactionDetail);
			transactionDetail = new TransactionDetail();
			transactionDetail = transactionBL.createTransactionDetail(
					supplier.getOpeningBalance(), combinationId,
					TransactionType.Credit.getCode(), null, null, null);
			transactionDetails.add(transactionDetail);
			Transaction transaction = transactionBL.createTransaction(
					transactionBL.getCurrency().getCurrencyId(), null, Calendar
							.getInstance().getTime(), transactionBL
							.getCategory("Accounts Payables"), supplier
							.getSupplierId(), Supplier.class.getSimpleName());
			transactionBL.saveTransaction(transaction, transactionDetails);

		}

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Supplier.class.getSimpleName(), supplier.getSupplierId(), user,
				workflowDetailVo);
	}

	public void updateSupplier(Supplier supplier) throws Exception {
		this.getSupplierService().save(supplier);
	}

	public Implementation getImplementationId() {
		Implementation implementation = new Implementation();
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public void deleteSupplier(Supplier supplier) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Supplier.class.getSimpleName(), supplier.getSupplierId(), user,
				workflowDetailVo);
	}

	public void doSupplierBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		Supplier supplier = supplierService.getSupplierById(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			this.getSupplierService().deleteSupplier(supplier);
		} else {
		}
	}

	public SupplierService getSupplierService() {
		return supplierService;
	}

	public void setSupplierService(SupplierService supplierService) {
		this.supplierService = supplierService;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public CreditTermBL getCreditTermBL() {
		return creditTermBL;
	}

	public void setCreditTermBL(CreditTermBL creditTermBL) {
		this.creditTermBL = creditTermBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
