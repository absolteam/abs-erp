package com.aiotech.aios.hr.service;

import com.aiotech.aios.system.service.SystemService;

public class HrEnterpriseService {

	private PersonService personService;
	private CompanyService companyService;
	private SystemService systemService;
	private JobService jobService;
	private LeaveProcessService leaveProcessService;
	private AttendanceService attendanceService;
	private PayrollService payrollService;
	private MemoWarningService memoWarningService;
	private IdentityAvailabilityService identityAvailabilityService;
	private AssetUsageService assetUsageService;
	private PersonBankService personBankService;
	private JobAssignmentService jobAssignmentService;
	
	public PersonService getPersonService() {
		return personService;
	}
	public CompanyService getCompanyService() {
		return companyService;
	}
	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}
	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}
	public SystemService getSystemService() {
		return systemService;
	}
	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}
	public LeaveProcessService getLeaveProcessService() {
		return leaveProcessService;
	}
	public void setLeaveProcessService(LeaveProcessService leaveProcessService) {
		this.leaveProcessService = leaveProcessService;
	}
	public JobService getJobService() {
		return jobService;
	}
	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}
	public AttendanceService getAttendanceService() {
		return attendanceService;
	}
	public void setAttendanceService(AttendanceService attendanceService) {
		this.attendanceService = attendanceService;
	}
	public PayrollService getPayrollService() {
		return payrollService;
	}
	public void setPayrollService(PayrollService payrollService) {
		this.payrollService = payrollService;
	}
	public MemoWarningService getMemoWarningService() {
		return memoWarningService;
	}
	public void setMemoWarningService(MemoWarningService memoWarningService) {
		this.memoWarningService = memoWarningService;
	}
	public IdentityAvailabilityService getIdentityAvailabilityService() {
		return identityAvailabilityService;
	}
	public void setIdentityAvailabilityService(
			IdentityAvailabilityService identityAvailabilityService) {
		this.identityAvailabilityService = identityAvailabilityService;
	}
	public AssetUsageService getAssetUsageService() {
		return assetUsageService;
	}
	public void setAssetUsageService(AssetUsageService assetUsageService) {
		this.assetUsageService = assetUsageService;
	}
	public PersonBankService getPersonBankService() {
		return personBankService;
	}
	public void setPersonBankService(PersonBankService personBankService) {
		this.personBankService = personBankService;
	}
	public JobAssignmentService getJobAssignmentService() {
		return jobAssignmentService;
	}
	public void setJobAssignmentService(JobAssignmentService jobAssignmentService) {
		this.jobAssignmentService = jobAssignmentService;
	}
	
}
