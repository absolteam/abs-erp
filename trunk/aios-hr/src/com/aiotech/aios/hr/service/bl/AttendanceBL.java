package com.aiotech.aios.hr.service.bl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.DurationFieldType;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Attendance;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.EmployeeCalendar;
import com.aiotech.aios.hr.domain.entity.EmployeeCalendarPeriod;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobShift;
import com.aiotech.aios.hr.domain.entity.LeaveProcess;
import com.aiotech.aios.hr.domain.entity.SwipeInOut;
import com.aiotech.aios.hr.domain.entity.vo.AttendanceVO;
import com.aiotech.aios.hr.domain.entity.vo.SwipeInOutVO;
import com.aiotech.aios.hr.domain.entity.vo.SwipeVO;
import com.aiotech.aios.hr.service.AttendanceService;
import com.aiotech.aios.hr.service.CompanyService;
import com.aiotech.aios.hr.service.JobService;
import com.aiotech.aios.hr.service.LeaveProcessService;
import com.aiotech.aios.hr.to.AttendanceTO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.CommentService;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.csvreader.CsvReader;
import com.opensymphony.xwork2.ActionContext;

public class AttendanceBL {
	private AttendanceService attendanceService;
	private JobService jobService;
	private LeaveProcessService leaveProcessService;
	private SystemService systemService;
	private CompanyService companyService;
	private Implementation implementation;
	private Integer lateInDays;
	private Integer lateOutDays;
	private Integer earlyInDays;
	private Integer earlyOutDays;
	private Integer timeLossDyas;
	private Integer timeExcessDays;
	private CommentService commentService;
	private JobAssignmentBL jobAssignmentBL;
	private EmployeeCalendarBL employeeCalendarBL;
	private WorkflowEnterpriseService workflowEnterpriseService;

	private static final Properties AIOS_PROPERTIES = new Properties();
	static {
		try {
			AIOS_PROPERTIES.load(AttendanceBL.class.getClassLoader()
					.getResourceAsStream("application.jmx.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void hrSaveAttendanceManual(List<SwipeInOut> tempswipeInOuts,
			Long jobAssignmentId, AttendanceVO attendanceVO) throws Exception {

		List<SwipeInOutVO> swipeInOutVOs = new ArrayList<SwipeInOutVO>();
		Date tempAttendanceDate = new Date();
		SwipeInOutVO swipeInOutVO = null;
		List<SwipeInOut> swipeInOutList = null;
		for (SwipeInOut swipeInOut : tempswipeInOuts) {
			if (tempAttendanceDate.equals(swipeInOut.getAttendanceDate())) {
				swipeInOutList.add(swipeInOut);
			} else {
				swipeInOutList = new ArrayList<SwipeInOut>();
				swipeInOutVO = new SwipeInOutVO();
				swipeInOutList.add(swipeInOut);
				swipeInOutVO.setSwipeInOuts(swipeInOutList);
				swipeInOutVOs.add(swipeInOutVO);
			}
			tempAttendanceDate = swipeInOut.getAttendanceDate();
		}

		if (swipeInOutVOs != null && swipeInOutVOs.size() > 0) {
			for (SwipeInOutVO swipeInOutvo : swipeInOutVOs) {
				List<SwipeInOut> tempswipeInOuts1 = attendanceService
						.getAllSwipeInOutWithDate(swipeInOutvo.getSwipeInOuts()
								.get(0).getSwipeId(), swipeInOutvo
								.getSwipeInOuts().get(0).getImplementation(),
								swipeInOutvo.getSwipeInOuts().get(0)
										.getAttendanceDate());

				LocalDate lDate = new LocalDate(swipeInOutvo.getSwipeInOuts()
						.get(0).getAttendanceDate());

				List<SwipeInOut> swipeInOutsNextDay = attendanceService
						.getAllSwipeInOutWithDate(swipeInOutvo.getSwipeInOuts()
								.get(0).getSwipeId(), implementation, lDate
								.plusDays(1).toDate());

				if (tempswipeInOuts1 != null && tempswipeInOuts1.size() > 0)
					attendanceService.deleteSwipeInOutRecords(tempswipeInOuts1);

				attendanceService.saveSwipeInOutRecords(swipeInOutvo
						.getSwipeInOuts());
				attendaceExcutionManual(swipeInOutvo.getSwipeInOuts().get(0)
						.getAttendanceDate(), swipeInOutvo.getSwipeInOuts()
						.get(0).getImplementation(),
						jobService.getJobAssignmentInfo(jobAssignmentId),
						swipeInOutvo.getSwipeInOuts(), swipeInOutsNextDay);
			}
		} else {
			if (attendanceVO.getIsAbsent() != null
					&& attendanceVO.getIsAbsent()) {
				JobAssignment jobAssignment = jobService
						.getJobAssignmentInfo(jobAssignmentId);
				Attendance attendance = new Attendance();
				attendance.setAttendanceDate(attendanceVO.getAttendanceDate());
				attendance.setCreatedDate(new Date());
				attendance.setJobAssignment(jobAssignment);
				attendance.setPerson(jobAssignment.getPerson());
				attendance.setImplementation(getImplementation());
				attendance.setIsAbsent(attendanceVO.getIsAbsent());
				attendance.setIsDeviation(true);
				attendance.setSystemSuggesion("Absent 1 day LOP");
				attendance.setDeductionType(Byte.parseByte("2"));
				attendance.setHalfDay(false);
				attendanceService.saveAttendance(attendance);
				Map<String, Object> sessionObj = ActionContext.getContext()
						.getSession();
				User user = (User) sessionObj.get("USER");
				WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
				workflowEnterpriseService
						.generateNotificationsAgainstDataEntry(
								Attendance.class.getSimpleName(),
								attendance.getAttendanceId(), user,
								workflowDetailVo);
			}
		}

	}

	public void attendaceExcutionManual(Date date,
			Implementation implementation, JobAssignment jobAssigment,
			List<SwipeInOut> swipeInOuts, List<SwipeInOut> nextDaySwipeInOuts)
			throws Exception {
		if (date == null) {
			String wend = "SATURDAY";
			Calendar cal = Calendar.getInstance();
			java.util.Date currentDate = cal.getTime();
			if (wend.equalsIgnoreCase(DateFormat.getDay(new Date())))
				cal.add(Calendar.DATE, -2);
			else
				cal.add(Calendar.DATE, -1);
			date = cal.getTime();
			System.out.println("current date--->" + currentDate);
			System.out.println("1 day before--->" + date);
		}

		List<EmployeeCalendar> employeeCalendars = attendanceService
				.getEmployeeCalendar(implementation);
		AttendancePolicy attendancePolicy = attendanceService
				.getAttendancePolicyInfo(jobAssigment.getDesignation()
						.getGrade().getGradeId(), implementation);
		Attendance attendance = null;
		if (attendancePolicy != null) {
			attendance = attendanceDataManipulation(jobAssigment, swipeInOuts,
					attendancePolicy, date, implementation, employeeCalendars,
					nextDaySwipeInOuts);
			if (attendance != null) {
				attendance.setImplementation(implementation);
				Attendance tempAttendance = attendanceService
						.getAttendanceInfo(attendance);
				boolean deletedFlag = false;
				if (tempAttendance != null
						&& (tempAttendance.getIsApprove() != 1 || tempAttendance
								.getIsApprove() != 4)) {
					attendanceService.deleteAttendance(tempAttendance);
					deletedFlag = true;
				}

				attendance.setJobAssignment(jobAssigment);
				if (tempAttendance == null || deletedFlag == true) {
					attendanceService.saveAttendance(attendance);
					if (attendance.getRelativeId() == null) {
						attendance.setRelativeId(attendance.getAttendanceId());
						attendanceService.saveAttendance(attendance);
					}

					Map<String, Object> sessionObj = ActionContext.getContext()
							.getSession();
					User user = (User) sessionObj.get("USER");
					WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();

					workflowDetailVo
							.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
									.getCode());
					if (attendance.getIsDeviation() != null
							&& attendance.getIsDeviation())
						workflowEnterpriseService
								.generateNotificationsAgainstDataEntry(
										"Attendance",
										attendance.getAttendanceId(), user,
										workflowDetailVo);
				}
			}
		}
	}

	public boolean attedanceReader(File file, String fileName) {
		List<SwipeInOut> swipeInOuts = null;
		boolean returnFlag = true;
		if (file != null && fileName != null) {
			String fileType = (fileName.substring(
					fileName.lastIndexOf(".") + 1, fileName.length()))
					.toUpperCase();
			if (fileType.equalsIgnoreCase("TXT")
					|| fileType.equalsIgnoreCase("CSV")
					|| fileType.equalsIgnoreCase("XLS")) {

			} else {
				ServletActionContext
						.getRequest()
						.setAttribute("RETURN_MESSAGE",
								"Please upload a valid file, system can accept only txt,csv and xls formats");
				return false;
			}

			CsvReader reader = null;
			try {
				swipeInOuts = new ArrayList<SwipeInOut>();
				// File file=new File(attendancePath);
				boolean exists = file.exists();
				if (exists) {
					reader = new CsvReader(file.toString());
					reader.getTrimWhitespace();
					reader.setDelimiter(' ');
					while (reader.readRecord()) {
						if (reader.getCurrentRecord() > 0) {
							String[] datas = new String[reader.get(0)
									.split(",").length];
							datas = reader.get(0).split(",");
							SwipeInOut swipeInOut = new SwipeInOut();
							swipeInOut.setSwipeId(Long.parseLong(datas[0]));
							if (fileType.equalsIgnoreCase("TXT")) {
								java.text.DateFormat df = new SimpleDateFormat(
										"yyyyMMdd");
								Date date = df.parse(datas[1]);
								swipeInOut.setAttendanceDate(date);
								java.text.DateFormat sdf = new SimpleDateFormat(
										"HH:mm:ss");
								Date time = sdf.parse(datas[2] + ":" + datas[3]
										+ ":" + datas[4]);
								swipeInOut.setTimeInOut(time);
								if (datas[5].equals("1"))
									swipeInOut.setTimeInOutType("IN");
								else if (datas[5].equals("4"))
									swipeInOut.setTimeInOutType("OUT");
								else
									swipeInOut.setTimeInOutType("LUNCH");

								swipeInOut.setDoorNumber(datas[6]);
							} else {
								java.text.DateFormat df = new SimpleDateFormat(
										"dd-MM-yyyy");
								Date date = df.parse(datas[1]);
								swipeInOut.setAttendanceDate(date);
								java.text.DateFormat sdf = new SimpleDateFormat(
										"HH:mm");
								Date time = sdf.parse(datas[2]);
								swipeInOut.setTimeInOut(time);
								swipeInOut.setTimeInOutType(datas[3]);
								swipeInOut.setDoorNumber(datas[4]);
							}
							swipeInOut.setImplementation(getImplementation());
							swipeInOut.setStatus(Byte.parseByte("1"));
							swipeInOuts.add(swipeInOut);
						}

					}
					reader.close();

				} else {
					returnFlag = false;
					System.out.println("File Not Found");
				}
				if (swipeInOuts != null && swipeInOuts.size() > 0) {
					for (SwipeInOut swipeInOut : swipeInOuts) {
						List<SwipeInOut> tempswipeInOuts = attendanceService
								.getAllSwipeInOutWithDateReader(
										swipeInOut.getSwipeId(),
										implementation,
										swipeInOut.getAttendanceDate(),
										swipeInOut.getTimeInOutType());
						if (tempswipeInOuts != null && swipeInOuts.size() > 0)
							attendanceService
									.deleteSwipeInOutRecords(tempswipeInOuts);
					}

				}
				attendanceService.saveSwipeInOutRecords(swipeInOuts);

			} catch (FileNotFoundException e) {
				e.printStackTrace();
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE",
						"Attendance Information/File Not Available");
			} catch (IOException e) {
				e.printStackTrace();
				ServletActionContext
						.getRequest()
						.setAttribute("RETURN_MESSAGE",
								"Provided Information / Policy Configuration are not correct");
			} catch (Exception e) {
				e.printStackTrace();
				ServletActionContext
						.getRequest()
						.setAttribute("RETURN_MESSAGE",
								"Please upload a valid file, system can accept only txt,csv and xls formats");
			}
			/*
			 * if(swipeInOuts!=null && swipeInOuts.size()>0)
			 * attendaceExcution(swipeInOuts
			 * .get(0).getAttendanceDate(),implementation);
			 */
		} else {
			ServletActionContext
					.getRequest()
					.setAttribute("RETURN_MESSAGE",
							"Please upload a valid file, system can accept only txt,csv and xls formats");
		}
		return returnFlag;
	}

	public void attendaceExcution(Date fromDate, Date toDate,
			Implementation imp, String persons) throws Exception {
		Map<Long, Long> jobAssignmentIdMap = null;
		if (persons != null && !persons.equals("")) {
			jobAssignmentIdMap = new HashMap<Long, Long>();
			String temps[] = persons.split(",");
			for (String personId : temps) {
				jobAssignmentIdMap.put(Long.valueOf(personId),
						Long.valueOf(personId));
			}

		}

		if (fromDate == null) {
			String wend = "SATURDAY";
			Calendar cal = Calendar.getInstance();
			java.util.Date currentDate = cal.getTime();
			if (wend.equalsIgnoreCase(DateFormat.getDay(new Date())))
				cal.add(Calendar.DATE, -2);
			else
				cal.add(Calendar.DATE, -1);
			fromDate = cal.getTime();
			System.out.println("current date--->" + currentDate);
			System.out.println("1 day before--->" + fromDate);
		}
		LocalDate start = new LocalDate(fromDate);
		LocalDate end = new LocalDate(toDate);
		List<LocalDate> dates = new ArrayList<LocalDate>();
		int days = Days.daysBetween(start, end).getDays() + 1;
		for (int i = 0; i < days; i++) {
			LocalDate d = start.withFieldAdded(DurationFieldType.days(), i);
			dates.add(d);
		}

		List<Implementation> implementations = new ArrayList<Implementation>();
		// Split of Implementation
		if (imp == null) {
			implementations = systemService.getAllImplementation();
		} else {
			Implementation tempImp = systemService.getImplementation(imp);
			implementations.add(tempImp);
		}
		for (Implementation implementation : implementations) {
			List<JobAssignment> jobAssignments = jobService
					.getAllActiveJobAssignment(implementation);
			List<EmployeeCalendar> employeeCalendars = attendanceService
					.getEmployeeCalendar(implementation);

			for (JobAssignment jobAssigment : jobAssignments) {
				if (jobAssignmentIdMap != null
						&& !jobAssignmentIdMap.containsKey(jobAssigment
								.getJobAssignmentId()))
					continue;

				for (LocalDate tempDate : dates) {
					Date date = tempDate.toDateTimeAtStartOfDay().toDate();
					List<SwipeInOut> swipeInOuts = attendanceService
							.getAllSwipeInOutWithDate(
									jobAssigment.getSwipeId(), implementation,
									date);

					LocalDate lDate = new LocalDate(date);

					List<SwipeInOut> swipeInOutsNextDay = attendanceService
							.getAllSwipeInOutWithDate(
									jobAssigment.getSwipeId(), implementation,
									lDate.plusDays(1).toDate());

					AttendancePolicy attendancePolicy = attendanceService
							.getAttendancePolicyInfo(jobAssigment
									.getDesignation().getGrade().getGradeId(),
									implementation);

					Attendance attendance = null;
					if (attendancePolicy != null)
						attendance = attendanceDataManipulation(jobAssigment,
								swipeInOuts, attendancePolicy, date,
								implementation, employeeCalendars,
								swipeInOutsNextDay);
					if (attendance != null) {
						attendance.setImplementation(implementation);
						Attendance tempAttendance = attendanceService
								.getAttendanceInfo(attendance);
						if (tempAttendance != null)
							attendanceService.deleteAttendance(tempAttendance);
						attendance.setJobAssignment(jobAssigment);
						attendanceService.saveAttendance(attendance);
						Map<String, Object> sessionObj = ActionContext
								.getContext().getSession();
						User user = (User) sessionObj.get("USER");
						WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();

						workflowDetailVo
								.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
										.getCode());
						if (attendance.getIsDeviation() != null
								&& attendance.getIsDeviation())
							workflowEnterpriseService
									.generateNotificationsAgainstDataEntry(
											"Attendance",
											attendance.getAttendanceId(), user,
											workflowDetailVo);
					}
				}
			}
		}
	}

	public Attendance attendanceDataManipulation(JobAssignment jobAssignment,
			List<SwipeInOut> swipeInOuts, AttendancePolicy attendancePolicy,
			Date attDate, Implementation implementation,
			List<EmployeeCalendar> employeeCalendars,
			List<SwipeInOut> swipeInOutsNextDays) throws Exception {

		Attendance attendance = new Attendance();
		Double timeLossOrExcessDouble = 0.0;
		// Employee absent process
		List<EmployeeCalendarPeriod> employeeCalendarPeriods = new ArrayList<EmployeeCalendarPeriod>();
		List<LeaveProcess> leaveProcesses = leaveProcessService
				.getApprovedBasedOnPersonForParticularDate(
						jobAssignment.getPerson(), attDate, attDate);
		// Find week end
		EmployeeCalendar employeeCalendar = employeeCalendarBL
				.getEmployeeCalendarInfo(jobAssignment, employeeCalendars);
		boolean weekend = false;
		if (employeeCalendar != null && employeeCalendar.getWeekend() != null
				&& !employeeCalendar.getWeekend().equals("")) {
			String[] weekends = employeeCalendar.getWeekend().split(",");
			for (String wend : weekends) {
				if (wend.equalsIgnoreCase(DateFormat.getDay(attDate)))
					weekend = true;
			}
		}
		if (employeeCalendar != null && weekend == false)
			employeeCalendarPeriods = attendanceService
					.getEmployeeCalendarPeriods(attDate,
							employeeCalendar.getEmployeeCalendarId());

		if (swipeInOuts != null && swipeInOuts.size() > 0) {
			Double temptimeLossOrExcessDouble = 0.0;
			Double requiredTotalHoursDouble = 0.0;
			Double actualTotalHoursDouble = 0.0;
			Collections.sort(swipeInOuts, new Comparator<SwipeInOut>() {
				public int compare(SwipeInOut m1, SwipeInOut m2) {
					return m1.getTimeInOut().compareTo(m2.getTimeInOut());
				}
			});

			//
			int i = 0;
			Double outHours = 0.0;
			SwipeInOut timeIn = null;
			SwipeInOut timeOut = null;
			List<SwipeInOut> swipeInOutGLS = new ArrayList<SwipeInOut>();

			{
				// Fetch Previous day attendance entry to find the out of
				// yesterday to skip the next days entries
				LocalDate localdate = new LocalDate(attDate);
				localdate = localdate.minusDays(1);
				List<Attendance> attendances = attendanceService
						.getAllAttendancePunchReport(jobAssignment.getPerson()
								.getPersonId(), localdate.toDate(), localdate
								.toDate());
				Attendance perviousDayAttendance = null;
				if (attendances != null && attendances.size() > 0) {
					perviousDayAttendance = attendances.get(0);
				}

				swipeInOutGLS.addAll(swipeInOuts);
				for (SwipeInOut swipeInout : swipeInOutGLS) {
					int difference = 3;
					if (perviousDayAttendance != null) {
						LocalDate dt = new LocalDate(perviousDayAttendance.getAttendanceOutDate());
						LocalTime tim = new LocalTime(perviousDayAttendance.getTimeOut());
						DateTime lastDayOut = dt.toDateTime(tim);
						dt = new LocalDate(swipeInout.getAttendanceDate());
						tim = new LocalTime(swipeInout.getTimeInOut());
						DateTime currentDayIn = dt.toDateTime(tim);
						difference = Minutes.minutesBetween(lastDayOut,
								currentDayIn).getMinutes();
					}
					if (difference > 2
							&& swipeInout.getTimeInOutType().equalsIgnoreCase(
									"IN") && timeIn == null) {
						timeIn = new SwipeInOut();
						timeIn = swipeInout;
					} else if (i != 0
							&& i == swipeInOuts.size() - 1
							&& swipeInout.getTimeInOutType().equalsIgnoreCase(
									"OUT") && timeOut == null) {
						timeOut = new SwipeInOut();
						timeOut = swipeInout;
					}
					i++;
				}

				// Find the exit entries from next day punches
				if (swipeInOutsNextDays != null
						&& swipeInOutsNextDays.size() > 1 && timeOut == null) {
					i = 1;
					Collections.sort(swipeInOutsNextDays,
							new Comparator<SwipeInOut>() {
								public int compare(SwipeInOut m1, SwipeInOut m2) {
									return m1.getTimeInOut().compareTo(
											m2.getTimeInOut());
								}
							});
					Map<Integer, SwipeInOut> linkedMap = new LinkedHashMap<Integer, SwipeInOut>();
					for (SwipeInOut swipeInout : swipeInOutsNextDays) {
						linkedMap.put(i, swipeInout);
						i++;
					}
					LocalTime timeFirst = new LocalTime(linkedMap.get(1)
							.getTimeInOut());
					LocalTime timeSecond = new LocalTime(linkedMap.get(2)
							.getTimeInOut());
					if (timeFirst != null && timeSecond != null) {
						int minutes = Minutes.minutesBetween(timeFirst,
								timeSecond).getMinutes();
						if (minutes < 1)
							timeOut = linkedMap.get(2);
					}

				}

				// Get In As Out
				if (timeIn == null) {
					timeIn = new SwipeInOut();
					timeIn = swipeInOutGLS.get(0);
				}

				// special condition if in & out is exactly mid night
				// 24.00/00.00
				if (swipeInOutsNextDays != null
						&& swipeInOutsNextDays.size() > 0 && timeOut == null) {
					Collections.sort(swipeInOutsNextDays,
							new Comparator<SwipeInOut>() {
								public int compare(SwipeInOut m1, SwipeInOut m2) {
									return m1.getTimeInOut().compareTo(
											m2.getTimeInOut());
								}
							});

					LocalTime timeFirst = new LocalTime(swipeInOutGLS.get(
							swipeInOutGLS.size() - 1).getTimeInOut());
					LocalTime timeSecond = new LocalTime(swipeInOutsNextDays
							.get(0).getTimeInOut());
					if (timeFirst != null && timeSecond != null) {
						int minutes = Minutes.minutesBetween(timeFirst,
								timeSecond).getMinutes();
						if (minutes < 2)
							timeOut = swipeInOutsNextDays.get(0);
					}

				}

				// Get In As Out
				if (timeOut == null) {
					timeOut = new SwipeInOut();
					BeanUtils.copyProperties(timeOut, timeIn);
				}

				attendance.setAttendanceDate(timeIn.getAttendanceDate());
				attendance.setAttendanceOutDate(timeOut.getAttendanceDate());
				attendance.setTimeIn(timeIn.getTimeInOut());
				attendance.setTimeOut(timeOut.getTimeInOut());
				// Find actual Total working hours
				LocalDate dt = new LocalDate(timeIn.getAttendanceDate());
				LocalTime tim = new LocalTime(timeIn.getTimeInOut());
				DateTime start = dt.toDateTime(tim);
				dt = new LocalDate(timeOut.getAttendanceDate());
				tim = new LocalTime(timeOut.getTimeInOut());
				DateTime end = dt.toDateTime(tim);
				String startTime = "00:00";
				int minutes = Minutes.minutesBetween(start, end).getMinutes();
				int h = minutes / 60
						+ Integer.valueOf(startTime.substring(0, 1));
				int m = minutes % 60
						+ Integer.valueOf(startTime.substring(3, 4));
				if (h > 0 && m > 0)
					actualTotalHoursDouble = Double.valueOf(h + "." + m);
				else
					actualTotalHoursDouble = Double.valueOf(Math.abs(h) + "."
							+ Math.abs(m));
			}

			// get job shift & working shift
			List<JobShift> jobShifts = jobService
					.getAllJobShiftWithDateByAssignment(jobAssignment,
							attendance.getAttendanceDate());

			if (jobShifts == null || jobShifts.size() == 0) {
				jobShifts = jobService.getAllJobShiftWithDate(
						jobAssignment.getJob(), attendance.getAttendanceDate());
			}

			JobShift jobShift = null;
			for (JobShift jobShiftTemp : jobShifts) {
				if (jobShiftTemp.getWorkingShift().getIrregularDay() != null
						&& jobShiftTemp
								.getWorkingShift()
								.getIrregularDay()
								.trim()
								.equalsIgnoreCase(
										DateFormat.getDay(swipeInOuts.get(0)
												.getAttendanceDate()))) {
					jobShift = new JobShift();
					jobShift = jobShiftTemp;
					break;
				} else if (jobShiftTemp.getWorkingShift().getIrregularDay() == null
						|| jobShiftTemp.getWorkingShift().getIrregularDay()
								.trim().equals("")) {
					jobShift = new JobShift();
					jobShift = jobShiftTemp;
				}
			}

			// Fetch the shift from default configuration
			if (jobShift == null) {
				jobShifts = jobService
						.getAllJobShiftWithOutDateByAssignment(jobAssignment);
				for (JobShift jobShiftTemp : jobShifts) {
					if (jobShiftTemp.getWorkingShift().getIrregularDay() != null
							&& jobShiftTemp
									.getWorkingShift()
									.getIrregularDay()
									.trim()
									.equalsIgnoreCase(
											DateFormat.getDay(timeIn
													.getAttendanceDate()))) {
						jobShift = new JobShift();
						jobShift = jobShiftTemp;
						break;
					} else if (jobShiftTemp.getWorkingShift().getIrregularDay() == null
							|| jobShiftTemp.getWorkingShift().getIrregularDay()
									.trim().equals("")) {
						jobShift = new JobShift();
						jobShift = jobShiftTemp;
					}
				}

				if (jobShift == null)
					jobShifts = jobService
							.getAllJobShiftWithOutDate(jobAssignment.getJob());

				for (JobShift jobShiftTemp : jobShifts) {
					if (jobShiftTemp.getWorkingShift().getIrregularDay() != null
							&& jobShiftTemp
									.getWorkingShift()
									.getIrregularDay()
									.trim()
									.equalsIgnoreCase(
											DateFormat.getDay(timeIn
													.getAttendanceDate()))) {
						jobShift = new JobShift();
						jobShift = jobShiftTemp;
						break;
					} else if (jobShiftTemp.getWorkingShift().getIrregularDay() == null
							|| jobShiftTemp.getWorkingShift().getIrregularDay()
									.trim().equals("")) {
						jobShift = new JobShift();
						jobShift = jobShiftTemp;
					}
				}

			}

			Double breakhours = 0.0;
			if (jobShift != null
					&& jobShift.getWorkingShift().getBreakHours() != null)
				breakhours = Double.valueOf(DateFormat.getTimeOnly(
						jobShift.getWorkingShift().getBreakHours()).replace(
						":", "."));

			if (actualTotalHoursDouble > 0 && breakhours > 0) {
				LocalTime from = new LocalTime(
						DateFormat
								.convertDoubleToTimeStrFromat(actualTotalHoursDouble));
				LocalTime deductionTime = null;
				if (jobShift.getWorkingShift().getBreakHours() != null)
					deductionTime = new LocalTime(jobShift.getWorkingShift()
							.getBreakHours());
				from = from.minusMinutes(deductionTime.getMinuteOfHour());

				/*
				 * actualTotalHoursDouble = Double.valueOf(from.getHourOfDay() +
				 * "." + from.getMinuteOfHour());
				 */

			}

			attendance.setTotalHours((DateFormat
					.convertDoubleToTimeStr(actualTotalHoursDouble)));

			if (jobShift != null) {
				// Find Late in
				Double lateInDouble = DateFormat
						.timeDifference(attendance.getTimeIn(), jobShift
								.getWorkingShift().getStartTime());
				if (lateInDouble > 0.0) {
					attendance.setEarlyIn(DateFormat
							.convertDoubleToTimeStr(lateInDouble));
				} else {
					Double temp = Math.abs(lateInDouble);
					attendance.setLateIn(DateFormat
							.convertDoubleToTimeStr(temp));
				}
				// Find Early out
				Double earlyOutDouble = DateFormat.timeDifferencePasitive(
						attendance.getTimeOut(), jobShift.getWorkingShift()
								.getEndTime());
				if (earlyOutDouble > 0.0) {
					attendance.setLateOut(DateFormat
							.convertDoubleToTimeStr(earlyOutDouble));
				} else {
					Double temp = Math.abs(earlyOutDouble);
					attendance.setEarlyOut(DateFormat
							.convertDoubleToTimeStr(temp));
				}

				// Find required working hours
				/*
				 * requiredTotalHoursDouble =
				 * Math.abs(DateFormat.timeDifference(
				 * jobShift.getWorkingShift().getStartTime(), jobShift
				 * .getWorkingShift().getEndTime()));
				 */
				{
					// Find actual Total working hours
					LocalDate dt = new LocalDate(timeIn.getAttendanceDate());
					LocalTime tim = new LocalTime(jobShift.getWorkingShift()
							.getStartTime());
					DateTime start = dt.toDateTime(tim);
					dt = new LocalDate(timeIn.getAttendanceDate());
					LocalTime tim1 = new LocalTime(jobShift.getWorkingShift().getEndTime());
					DateTime end = dt.toDateTime(tim1);
					
					//Increase one day to set the exit is next day starting from today
					if(tim1.getHourOfDay()==0 || tim.getHourOfDay()>tim1.getHourOfDay()){
						end=end.plusDays(1);
					}
					String startTime = "00:00";
					int minutes = Minutes.minutesBetween(start, end)
							.getMinutes();
					int h = minutes / 60
							+ Integer.valueOf(startTime.substring(0, 1));
					int m = minutes % 60
							+ Integer.valueOf(startTime.substring(3, 4));
					if (h > 0 && m > 0)
						requiredTotalHoursDouble = Double.valueOf(h + "." + m);
					else
						requiredTotalHoursDouble = Double.valueOf(Math.abs(h)
								+ "." + Math.abs(m));

					attendance.setShiftHours(requiredTotalHoursDouble);
				}

				if (actualTotalHoursDouble > 0) {
					timeLossOrExcessDouble = DateFormat
							.timeDifference(
									DateFormat
											.convertDoubleToTimeStrFromat(requiredTotalHoursDouble),
									DateFormat
											.convertDoubleToTimeStrFromat(actualTotalHoursDouble));
				} else {
					timeLossOrExcessDouble = -(requiredTotalHoursDouble);
				}

				if (timeLossOrExcessDouble > 0.0) {
					temptimeLossOrExcessDouble = Double
							.valueOf(requiredTotalHoursDouble
									- (DateFormat.getTimeOnlyDouble(jobShift
											.getWorkingShift().getBufferTime())));
				}

				if (timeLossOrExcessDouble > 0.0) {
					attendance.setTimeExcess(DateFormat
							.convertDoubleToTimeStr(Math
									.abs(timeLossOrExcessDouble)));
				} else {
					attendance.setTimeLoss(DateFormat
							.convertDoubleToTimeStr(Math
									.abs(timeLossOrExcessDouble)));
				}
			} /*
			 * else { attendance = null; return attendance; }
			 */
			// Get system Suggestion
			systemSuggestionMessageSet(attendance, attendancePolicy, jobShift,
					null);

			attendance.setPerson(jobAssignment.getPerson());
			attendance.setJobShift(jobShift);
			attendance.setCreatedDate(new Date());
			attendance.setIsAbsent(false);
			// Deviation logic
			attendance.setIsDeviation(false);
			attendance.setIsApprove(Byte
					.parseByte(Constants.RealEstate.Status.Published.getCode()
							+ ""));
			// Comp off logic
			if ((attendancePolicy.getAllowCompoff() != null
					&& attendancePolicy.getAllowCompoff() == true && attendance
					.getTimeExcess() != null)
					|| (attendancePolicy.getAllowOt() != null
							&& attendancePolicy.getAllowOt() == true && attendance
							.getTimeExcess() != null)
					|| (attendance.getTimeLoss() != null && temptimeLossOrExcessDouble < requiredTotalHoursDouble)) {
				attendance.setIsDeviation(true);
				attendance.setIsApprove(Byte
						.parseByte(Constants.RealEstate.Status.NoDecession
								.getCode() + ""));
			}

			// Over Time/Leave Cancel
			if ((employeeCalendarPeriods != null && employeeCalendarPeriods
					.size() > 0)
					|| (leaveProcesses != null && leaveProcesses.size() > 0)
					|| (weekend == true)) {
				if (leaveProcesses != null && leaveProcesses.size() > 0) {
					for (LeaveProcess leaveProcess2 : leaveProcesses) {
						leaveProcess2.setStatus(Byte
								.parseByte(Constants.HR.LeaveStatus.NotTaken
										.getCode() + ""));
						leaveProcess2.setIsApprove(Byte
								.parseByte(Constants.RealEstate.Status.Deny
										.getCode() + ""));
						leaveProcessService.saveLeaveProcess(leaveProcess2);
					}
				} else {
					attendance.setTimeExcess(attendance.getTotalHours());
					attendance.setTimeLoss(null);
					attendance.setLateIn(null);
					attendance.setLateOut(null);
					attendance.setEarlyIn(null);
					attendance.setEarlyOut(null);
					attendance.setIsDeviation(true);
					attendance.setIsApprove(Byte
							.parseByte(Constants.RealEstate.Status.NoDecession
									.getCode() + ""));
					String oTorCoff = "Y";
					systemSuggestionMessageSet(attendance, attendancePolicy,
							jobShift, oTorCoff);
				}
			}

			if (attendance.getDeductionType() == 1) {
				attendance.setIsDeviation(false);
				attendance.setIsApprove(Byte
						.parseByte(Constants.RealEstate.Status.Published
								.getCode() + ""));
			}

		} else {
			attendance.setAttendanceDate(attDate);
			attendance.setTimeIn(null);
			attendance.setTimeOut(null);
			attendance.setTimeLoss(null);
			attendance.setLateIn(null);
			attendance.setLateOut(null);
			attendance.setEarlyIn(null);
			attendance.setEarlyOut(null);
			attendance.setPerson(jobAssignment.getPerson());
			// get job shift & working shift
			List<JobShift> jobShifts = jobService.getAllJobShiftWithDate(
					jobAssignment.getJob(), attendance.getAttendanceDate());
			JobShift jobShift = null;
			for (JobShift jobShiftTemp : jobShifts) {
				if (jobShiftTemp.getWorkingShift().getIrregularDay() != null
						&& jobShiftTemp
								.getWorkingShift()
								.getIrregularDay()
								.trim()
								.equalsIgnoreCase(
										DateFormat.getDay(swipeInOuts.get(0)
												.getAttendanceDate()))) {
					jobShift = new JobShift();
					jobShift = jobShiftTemp;
					break;
				} else if (jobShiftTemp.getWorkingShift().getIrregularDay() == null
						|| jobShiftTemp.getWorkingShift().getIrregularDay()
								.trim().equals("")) {
					jobShift = new JobShift();
					jobShift = jobShiftTemp;
				}
			}

			// Fetch the shift from default configuration

			if (jobShift == null) {
				jobShifts = jobService
						.getAllJobShiftWithOutDateByAssignment(jobAssignment);
				for (JobShift jobShiftTemp : jobShifts) {
					if (jobShiftTemp.getWorkingShift().getIrregularDay() != null
							&& jobShiftTemp
									.getWorkingShift()
									.getIrregularDay()
									.trim()
									.equalsIgnoreCase(
											DateFormat.getDay(attDate))) {
						jobShift = new JobShift();
						jobShift = jobShiftTemp;
						break;
					} else if (jobShiftTemp.getWorkingShift().getIrregularDay() == null
							|| jobShiftTemp.getWorkingShift().getIrregularDay()
									.trim().equals("")) {
						jobShift = new JobShift();
						jobShift = jobShiftTemp;
					}
				}

				if (jobShift == null)
					jobShifts = jobService
							.getAllJobShiftWithOutDate(jobAssignment.getJob());

				for (JobShift jobShiftTemp : jobShifts) {
					if (jobShiftTemp.getWorkingShift().getIrregularDay() != null
							&& jobShiftTemp
									.getWorkingShift()
									.getIrregularDay()
									.trim()
									.equalsIgnoreCase(
											DateFormat.getDay(attDate))) {
						jobShift = new JobShift();
						jobShift = jobShiftTemp;
						break;
					} else if (jobShiftTemp.getWorkingShift().getIrregularDay() == null
							|| jobShiftTemp.getWorkingShift().getIrregularDay()
									.trim().equals("")) {
						jobShift = new JobShift();
						jobShift = jobShiftTemp;
					}
				}

			}
			attendance.setJobShift(jobShift);
			attendance.setCreatedDate(new Date());
			attendance.setImplementation(implementation);
			attendance.setHalfDay(false);
			// Weekend/Holiday logic
			if ((employeeCalendarPeriods != null && employeeCalendarPeriods
					.size() > 0) || (weekend == true)) {

				attendance.setIsApprove(Byte
						.parseByte(Constants.RealEstate.Status.Published
								.getCode() + ""));
				// Deviation logic
				attendance.setIsDeviation(false);
				attendance.setIsAbsent(false);
				attendance.setSystemSuggesion("Weekend/Holiday");
				attendance.setDeductionType(Byte.parseByte("1"));

			} else if (leaveProcesses != null && leaveProcesses.size() > 0) {
				boolean isUnPaid = false;
				for (LeaveProcess leaveProcess2 : leaveProcesses) {
					// Find the leave type whether paid or unpaid
					isUnPaid = leaveProcess2.getJobLeave().getLeave()
							.getIsPaid();
					// Update Leave table with respective status onleave or
					// taken.
					if (leaveProcess2.getToDate().after(attDate)) {
						leaveProcess2.setStatus(Byte
								.parseByte(Constants.HR.LeaveStatus.OnLeave
										.getCode() + ""));
					} else if (leaveProcess2.getToDate().equals(attDate)) {
						leaveProcess2.setStatus(Byte
								.parseByte(Constants.HR.LeaveStatus.Taken
										.getCode() + ""));
					}
					leaveProcessService.saveLeaveProcess(leaveProcess2);
				}

				attendance.setIsApprove(Byte
						.parseByte(Constants.RealEstate.Status.Published
								.getCode() + ""));

				// Deviation logic
				attendance.setIsDeviation(false);
				attendance.setIsAbsent(false);
				if (isUnPaid) {
					attendance.setSystemSuggesion("UnPaid Leave");
					attendance.setDeductionType(Byte.parseByte("1"));
				} else {
					attendance.setSystemSuggesion("Paid Leave");
					attendance.setDeductionType(Byte.parseByte("1"));
				}
				attendance.setHalfDay(false);
			} else {
				// Absent logic
				attendance.setIsApprove(Byte
						.parseByte(Constants.RealEstate.Status.Published
								.getCode() + ""));

				// Deviation logic
				attendance.setIsDeviation(false);
				attendance.setIsAbsent(true);
				attendance.setSystemSuggesion("Absent 1 day LOP");
				attendance.setDeductionType(Byte.parseByte("2"));
				attendance.setHalfDay(false);
			}

		}
		return attendance;
	}

	public Attendance systemSuggestionMessageSet(Attendance attendance,
			AttendancePolicy attendancePolicy, JobShift jobShift,
			String oTorCoff) {
		Double compoffHalf = DateFormat.convertTimeToDouble(attendancePolicy
				.getCompoffHoursForHalfday());
		Double compoffFull = DateFormat.convertTimeToDouble(attendancePolicy
				.getCompoffHoursForFullday());
		Double otHalf = DateFormat.convertTimeToDouble(attendancePolicy
				.getOtHoursForHalfday());
		Double otFull = DateFormat.convertTimeToDouble(attendancePolicy
				.getOtHoursForFullday());
		Double lopHalf = DateFormat.convertTimeToDouble(attendancePolicy
				.getLopHoursForHalfday());
		Double lopFull = DateFormat.convertTimeToDouble(attendancePolicy
				.getLopHoursForFullday());
		Double lateAtt = 0.0;
		Double seviorLateAtt = 0.0;
		Double bufferTime = 0.0;
		/*
		 * Double
		 * actualtotalhours=Double.parseDouble(attendance.getTotalHours());
		 * Double expectedtotalhours= Math.abs((DateFormat.timeDifference
		 * (jobShift
		 * .getWorkingShift().getStartTime(),jobShift.getWorkingShift().
		 * getEndTime())));
		 */
		if (jobShift != null) {
			lateAtt = DateFormat.convertTimeToDouble(jobShift.getWorkingShift()
					.getLateAttendance());
			seviorLateAtt = DateFormat.convertTimeToDouble(jobShift
					.getWorkingShift().getSeviorLateAttendance());
			bufferTime = DateFormat.convertTimeToDouble(jobShift
					.getWorkingShift().getBufferTime());
		} else {
			attendance
					.setSystemSuggesion("No shift is specified to compare the working hours");
			attendance.setDeductionType(Byte.parseByte("1"));
			attendance
					.setCalcultionMethod(Constants.HR.AttendanceProcess.NoAction
							.getCode());
			return attendance;
		}
		attendance
				.setSystemSuggesion("No Action because of based on the policy there is no deviation");
		attendance.setDeductionType(Byte.parseByte("1"));
		attendance.setCalcultionMethod(Constants.HR.AttendanceProcess.NoAction
				.getCode());
		if (attendance.getTimeExcess() != null
				&& !attendance.getTimeExcess().trim().equals("")
				&& oTorCoff == null
				&& Double.valueOf(attendance.getTimeExcess()) > 1.0) {
			if (attendancePolicy.getHourlyProcess() != null
					&& attendancePolicy.getHourlyProcess()
					&& attendancePolicy.getAllowCompoff() != null
					&& attendancePolicy.getAllowCompoff()
					&& attendance.getTimeExcess() != null) {
				attendance.setDeviationHours((double) Math.round(Double
						.parseDouble(attendance.getTimeExcess())));
				attendance.setSystemSuggesion(attendance.getDeviationHours()
						+ " Hours compensatory off");
				attendance.setDeductionType(Byte.parseByte("4"));
				attendance
						.setCalcultionMethod(Constants.HR.AttendanceProcess.FullTime
								.getCode());
			} else if (attendancePolicy.getHourlyProcess() != null
					&& attendancePolicy.getHourlyProcess()
					&& attendancePolicy.getAllowOt() != null
					&& attendancePolicy.getAllowOt()
					&& attendance.getTimeExcess() != null) {
				attendance.setDeviationHours((double) Math.round(Double
						.parseDouble(attendance.getTimeExcess())));
				attendance.setSystemSuggesion(attendance.getDeviationHours()
						+ " Hours Over Time");
				attendance.setDeductionType(Byte.parseByte("3"));
				attendance
						.setCalcultionMethod(Constants.HR.AttendanceProcess.FullTime
								.getCode());
			} else if (attendancePolicy.getCompoffHoursForHalfday() != null
					&& attendance.getTimeExcess() != null
					&& compoffHalf <= Double.parseDouble(attendance
							.getTimeExcess())
					&& compoffFull > Double.parseDouble(attendance
							.getTimeExcess())) {
				attendance
						.setSystemSuggesion("Half a day compensatory off because of actual work hours more then that required work hours ");
				attendance.setDeductionType(Byte.parseByte("4"));
				attendance.setHalfDay(true);
				attendance
						.setCalcultionMethod(Constants.HR.AttendanceProcess.FullTime
								.getCode());
			} else if (attendancePolicy.getCompoffHoursForFullday() != null
					&& attendance.getTimeExcess() != null
					&& compoffFull <= Double.parseDouble(attendance
							.getTimeExcess())) {
				attendance
						.setSystemSuggesion("1 day compensatory off because of actual work hours more then that required work hours ");
				attendance.setDeductionType(Byte.parseByte("4"));
				attendance.setHalfDay(false);
				attendance
						.setCalcultionMethod(Constants.HR.AttendanceProcess.FullTime
								.getCode());
			} else if (attendancePolicy.getOtHoursForHalfday() != null
					&& attendance.getTimeExcess() != null
					&& otHalf <= Double.parseDouble(attendance.getTimeExcess())
					&& otFull > Double.parseDouble(attendance.getTimeExcess())) {
				attendance
						.setSystemSuggesion("Half a day OverTime because of actual work hours more then that required work hours ");
				attendance.setDeductionType(Byte.parseByte("3"));
				attendance.setHalfDay(true);
				attendance
						.setCalcultionMethod(Constants.HR.AttendanceProcess.FullTime
								.getCode());
			} else if (attendancePolicy.getOtHoursForFullday() != null
					&& attendance.getTimeExcess() != null
					&& otFull <= Double.parseDouble(attendance.getTimeExcess())) {
				attendance
						.setSystemSuggesion("1 day OverTime because of actual work hours more then that required work hours ");
				attendance.setDeductionType(Byte.parseByte("3"));
				attendance.setHalfDay(false);
				attendance
						.setCalcultionMethod(Constants.HR.AttendanceProcess.FullTime
								.getCode());
			} else {
				attendance
						.setSystemSuggesion("No Action because of based on the policy there is no deviation");
				attendance.setDeductionType(Byte.parseByte("1"));
				attendance
						.setCalcultionMethod(Constants.HR.AttendanceProcess.NoAction
								.getCode());
			}
		} else if (attendance.getTimeExcess() != null
				&& !attendance.getTimeExcess().trim().equals("")
				&& oTorCoff != null
				&& Double.valueOf(attendance.getTimeExcess()) > 1.0) {
			if (attendancePolicy.getHourlyProcess() != null
					&& attendancePolicy.getHourlyProcess()
					&& attendancePolicy.getAllowCompoff() != null
					&& attendancePolicy.getAllowCompoff()
					&& attendance.getTimeExcess() != null) {
				attendance.setDeviationHours((double) Math.round(Double
						.parseDouble(attendance.getTimeExcess())));
				attendance.setSystemSuggesion(attendance.getDeviationHours()
						+ " Hours compensatory off");
				attendance.setDeductionType(Byte.parseByte("4"));
				attendance
						.setCalcultionMethod(Constants.HR.AttendanceProcess.FullTime
								.getCode());
			} else if (attendancePolicy.getHourlyProcess() != null
					&& attendancePolicy.getHourlyProcess()
					&& attendancePolicy.getAllowOt() != null
					&& attendancePolicy.getAllowOt()
					&& attendance.getTimeExcess() != null) {
				if (Double.parseDouble(attendance.getTimeExcess()) > 1) {
					attendance.setDeviationHours((double) Math.round(Double
							.parseDouble(attendance.getTimeExcess())));
					attendance.setSystemSuggesion(attendance
							.getDeviationHours() + " Hours Over Time");
					attendance.setDeductionType(Byte.parseByte("3"));
					attendance
							.setCalcultionMethod(Constants.HR.AttendanceProcess.FullTime
									.getCode());
				}
			} else if (attendancePolicy.getCompoffHoursForHalfday() != null
					&& attendance.getTimeExcess() != null
					&& compoffHalf <= Double.parseDouble(attendance
							.getTimeExcess())
					&& compoffFull > Double.parseDouble(attendance
							.getTimeExcess())) {
				attendance
						.setSystemSuggesion("Half a day compensatory off because worked on Holdiay/Leave ");
				attendance.setDeductionType(Byte.parseByte("4"));
				attendance.setHalfDay(true);
			} else if (attendancePolicy.getCompoffHoursForFullday() != null
					&& attendance.getTimeExcess() != null
					&& compoffFull <= Double.parseDouble(attendance
							.getTimeExcess())) {
				attendance
						.setSystemSuggesion("1 day compensatory off because worked on Holdiay/Leave  ");
				attendance.setDeductionType(Byte.parseByte("4"));
				attendance.setHalfDay(false);
			} else if (attendancePolicy.getOtHoursForHalfday() != null
					&& attendance.getTimeExcess() != null
					&& otHalf <= Double.parseDouble(attendance.getTimeExcess())
					&& otFull > Double.parseDouble(attendance.getTimeExcess())) {
				attendance
						.setSystemSuggesion("Half a day Over Time because worked on Holdiay/Leave  ");
				attendance.setDeductionType(Byte.parseByte("3"));
				attendance.setHalfDay(true);
			} else if (attendancePolicy.getOtHoursForFullday() != null
					&& attendance.getTimeExcess() != null
					&& otFull <= Double.parseDouble(attendance.getTimeExcess())) {
				attendance
						.setSystemSuggesion("1 day Over Time because worked on Holdiay/Leave  ");
				attendance.setDeductionType(Byte.parseByte("3"));
				attendance.setHalfDay(false);
			} else {
				attendance
						.setSystemSuggesion("No Action because of based on the policy there is no deviation");
				attendance.setDeductionType(Byte.parseByte("1"));
			}
		} else if (attendance.getTimeLoss() != null
				&& !attendance.getTimeLoss().trim().equals("")
				&& Double.valueOf(attendance.getTimeLoss()) > 1.0) {
			if (attendancePolicy.getHourlyProcess() != null
					&& attendancePolicy.getHourlyProcess()
					&& attendance.getTimeLoss() != null) {
				if (Double.parseDouble(attendance.getTimeLoss()) > 1) {
					attendance.setDeviationHours((double) Math.round(Double
							.parseDouble(attendance.getTimeLoss())));
					attendance.setSystemSuggesion(attendance
							.getDeviationHours() + " Hours LOP");
					attendance.setDeductionType(Byte.parseByte("2"));
					attendance
							.setCalcultionMethod(Constants.HR.AttendanceProcess.FullTime
									.getCode());
				}
			} else if (attendancePolicy.getLopHoursForHalfday() != null
					&& lopHalf <= Double.parseDouble(attendance.getTimeLoss())
					&& lopFull > Double.parseDouble(attendance.getTimeLoss())) {
				attendance
						.setSystemSuggesion("Half a day LOP bacause time loss more then that attendance policy");
				attendance.setDeductionType(Byte.parseByte("2"));
				attendance.setHalfDay(true);
				attendance
						.setCalcultionMethod(Constants.HR.AttendanceProcess.FullTime
								.getCode());
			} else if (attendancePolicy.getLopHoursForFullday() != null
					&& lopFull <= Double.parseDouble(attendance.getTimeLoss())) {
				attendance
						.setSystemSuggesion("1 day LOP bacause time loss more then that attendance policy");
				attendance.setDeductionType(Byte.parseByte("2"));
				attendance.setHalfDay(false);
				attendance
						.setCalcultionMethod(Constants.HR.AttendanceProcess.FullTime
								.getCode());
			} else {
				attendance
						.setSystemSuggesion("No Action because of based on the policy there is no deviation");
				attendance.setDeductionType(Byte.parseByte("1"));
				attendance
						.setCalcultionMethod(Constants.HR.AttendanceProcess.NoAction
								.getCode());
			}
		} else {
			attendance
					.setSystemSuggesion("No Action because of based on the policy there is no deviation");
			attendance.setDeductionType(Byte.parseByte("1"));
			attendance
					.setCalcultionMethod(Constants.HR.AttendanceProcess.NoAction
							.getCode());
		}
		if (bufferTime != null && bufferTime>0.0 && attendance.getLateIn() != null
				&& bufferTime <= Double.valueOf(attendance.getLateIn())) {
			attendance
					.setSystemSuggesion("LOP because of Late(Exceeded buffer time) attendance");
			attendance.setDeductionType(Byte.parseByte("2"));
			attendance.setHalfDay(true);
			attendance
					.setCalcultionMethod(Constants.HR.AttendanceProcess.FullTime
							.getCode());
		}
		if (jobShift != null
				&& jobShift.getWorkingShift() != null
				&& jobShift.getWorkingShift().getLateAttendance() != null
				&& jobShift != null
				&& attendance.getLateIn() != null
				&& lateAtt != null
				&& lateAtt != 0.0
				&& lateAtt <= DateFormat.convertTimeToDouble(attendance
						.getTimeIn())) {
			attendance
					.setSystemSuggesion("Half a day LOP because of Late attendance");
			attendance.setDeductionType(Byte.parseByte("2"));
			attendance.setHalfDay(true);
			attendance
					.setCalcultionMethod(Constants.HR.AttendanceProcess.FullTime
							.getCode());

		}
		if (jobShift != null
				&& jobShift.getWorkingShift() != null
				&& jobShift.getWorkingShift().getSeviorLateAttendance() != null
				&& jobShift != null
				&& attendance.getLateIn() != null
				&& seviorLateAtt != null
				&& seviorLateAtt != 0.0
				&& seviorLateAtt <= DateFormat.convertTimeToDouble(attendance
						.getTimeIn())) {
			attendance
					.setSystemSuggesion("1 day LOP because of Sevior Late attendance");
			attendance.setDeductionType(Byte.parseByte("2"));
			attendance.setHalfDay(false);
			attendance
					.setCalcultionMethod(Constants.HR.AttendanceProcess.FullTime
							.getCode());

		}

		return attendance;
	}

	// Attendance Deviation process
	public Attendance getAttendanceDeviationProcess(Long attendanceId) {
		Attendance attendance = new Attendance();
		if (attendanceId != null && attendanceId > 0) {
			attendance = attendanceService
					.getAttendanceInfoForDevationProcess(attendanceId);
		}
		return attendance;
	}

	public String attendanceProcessTypeInfo(Byte id) {
		String str = "";
		if (id != null) {
			if (id == 1)
				str = "NoAction";
			else if (id == 2)
				str = "LOP /UnPaid";
			else if (id == 3)
				str = "OverTime";
			else if (id == 4)
				str = "CompOff";
			else if (id == 5)
				str = "COD-CompOff Deduction";
			else
				str = "Decision Pending";
		} else {
			str = "Decision Pending";
		}
		return str;
	}

	public AttendanceTO punchReport(Long personId, Date fromDate, Date toDate) {
		List<AttendanceTO> attendanceTOs = new ArrayList<AttendanceTO>();
		AttendanceTO atteTO = new AttendanceTO();
		List<Attendance> attendanceList = null;
		if (personId != null && personId > 0)
			attendanceList = attendanceService.getAllAttendancePunchReport(
					personId, fromDate, toDate);
		else
			attendanceList = attendanceService
					.getAllAttendancePunchReportBasedPeriod(
							getImplementation(), fromDate, toDate);
		if (attendanceList != null && attendanceList.size() > 0) {
			Collections.sort(attendanceList, new Comparator<Attendance>() {
				public int compare(Attendance m1, Attendance m2) {
					return m1
							.getJobAssignment()
							.getPerson()
							.getFirstName()
							.compareTo(
									m2.getJobAssignment().getPerson()
											.getFirstName());
				}
			});
			Collections.sort(attendanceList, new Comparator<Attendance>() {
				public int compare(Attendance m1, Attendance m2) {
					return m1.getAttendanceDate().compareTo(
							m2.getAttendanceDate());
				}
			});
			int absentCount = 0;
			for (Attendance attendance : attendanceList) {
				AttendanceTO to = new AttendanceTO();
				to = convertAttendanceIntoTO(attendance);
				if (attendance.getSystemSuggesion() != null
						&& attendance.getSystemSuggesion().equals(
								"Absent 1 day LOP")) {
					to = toUpdateForAbsentDay(to, "Absent");
					absentCount++;
				} else if (attendance.getSystemSuggesion() != null
						&& attendance.getSystemSuggesion().equals(
								"Weekend/Holiday")) {
					to = toUpdateForAbsentDay(to, "Weekend/Holiday");
				} else if (attendance.getSystemSuggesion() != null
						&& attendance.getSystemSuggesion().equals(
								"UnPaid Leave")) {
					to = toUpdateForAbsentDay(to, "UnPaid Leave");
				} else if (attendance.getSystemSuggesion() != null
						&& attendance.getSystemSuggesion().equals("Paid Leave")) {
					to = toUpdateForAbsentDay(to, "On Leave");
				}
				attendanceTOs.add(to);

			}
			// Assign first record detail as master data
			if (attendanceTOs != null && attendanceTOs.size() > 0) {
				atteTO.setPersonName(attendanceTOs.get(0).getPersonName());
				atteTO.setCompanyName(attendanceTOs.get(0).getCompanyName());
				atteTO.setLocationName(attendanceTOs.get(0).getLocationName());
				atteTO.setDepartmentName(attendanceTOs.get(0)
						.getDepartmentName());
				atteTO.setDesignationName(attendanceTOs.get(0)
						.getDesignationName());
				atteTO.setAttendanceHistoryList(attendanceTOs);

			}
			// Punch Report Loss Excess Days Mapping
			lateInDays = 0;
			lateOutDays = 0;
			earlyInDays = 0;
			earlyOutDays = 0;
			timeLossDyas = 0;
			timeExcessDays = 0;
			double grandTotal = 0.0;
			for (Attendance attendance : attendanceList) {
				if (attendance.getLateIn() != null
						&& !attendance.getLateIn().equals(""))
					lateInDays += 1;
				if (attendance.getLateOut() != null
						&& !attendance.getLateOut().equals(""))
					lateOutDays += 1;
				if (attendance.getEarlyIn() != null
						&& !attendance.getEarlyIn().equals(""))
					earlyInDays += 1;
				if (attendance.getEarlyOut() != null
						&& !attendance.getEarlyOut().equals(""))
					earlyOutDays += 1;
				if (attendance.getTimeLoss() != null
						&& !attendance.getTimeLoss().equals(""))
					timeLossDyas += 1;
				if (attendance.getTimeExcess() != null
						&& !attendance.getTimeExcess().equals(""))
					timeExcessDays += 1;

				if (attendance.getTotalHours() != null
						&& !attendance.getTotalHours().equals(""))
					grandTotal += Math.round(Double.valueOf(attendance
							.getTotalHours()));
			}

			atteTO.setLateInDays(lateInDays);
			atteTO.setLateOutDays(lateOutDays);
			atteTO.setEarlyInDays(earlyInDays);
			atteTO.setEarlyOutDays(earlyOutDays);
			atteTO.setTimeLossDyas(timeLossDyas);
			atteTO.setTimeExcessDays(timeExcessDays);
			atteTO.setNumberOfDays(absentCount + "");
			atteTO.setTotalHours(grandTotal + "");

		}
		return atteTO;
	}

	public void updateSwipeInOut(JobAssignment jobAssignment, Date fromDate,
			Date toDate) throws Exception {
		List<SwipeInOut> swipes = attendanceService.getSwipeInOutsByCriteria(
				fromDate, toDate, getImplementation(),
				jobAssignment.getSwipeId());

		Collections.sort(swipes, new Comparator<SwipeInOut>() {
			public int compare(SwipeInOut m1, SwipeInOut m2) {
				return m1.getTimeInOut().compareTo(m2.getTimeInOut());
			}
		});

		Collections.sort(swipes, new Comparator<SwipeInOut>() {
			public int compare(SwipeInOut m1, SwipeInOut m2) {
				return m1.getAttendanceDate().compareTo(m2.getAttendanceDate());
			}
		});

		DateTime previousDate = null;
		for (SwipeInOut swipeInOut : swipes) {
			LocalDate dt = new LocalDate(swipeInOut.getAttendanceDate());
			LocalTime tim = new LocalTime(swipeInOut.getTimeInOut());
			DateTime curretDate = dt.toDateTime(tim);
			int minutes = 2;
			if (previousDate != null)
				minutes = Minutes.minutesBetween(curretDate, previousDate)
						.getMinutes();
			if (previousDate != null && Math.abs(minutes) <= 1) {
				swipeInOut.setTimeInOutType("OUT");
			} else {
				swipeInOut.setTimeInOutType("IN");
			}
			previousDate = curretDate;

		}

		attendanceService.saveSwipeInOutRecords(swipes);
	}

	public DateTime dateAndTimeToDateTime(Date date, Date time) {
		String myDate = date + " " + time;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		java.util.Date utilDate = new java.util.Date();
		try {
			utilDate = sdf.parse(myDate);
		} catch (ParseException pe) {
			pe.printStackTrace();
		}
		DateTime dateTime = new DateTime(utilDate);

		return dateTime;
	}

	public AttendanceTO companyPunchReport(Long companyId, Date fromDate,
			Date toDate) {
		List<AttendanceTO> attendanceTOs = new ArrayList<AttendanceTO>();
		AttendanceTO atteTO = new AttendanceTO();
		List<Attendance> attendanceList = null;
		if (companyId != null && companyId > 0)
			attendanceList = attendanceService.getCompanyAttendancePunchReport(
					companyId, fromDate, toDate);
		else
			attendanceList = attendanceService
					.getAllAttendancePunchReportBasedPeriod(
							getImplementation(), fromDate, toDate);

		if (attendanceList != null && attendanceList.size() > 0) {
			Collections.sort(attendanceList, new Comparator<Attendance>() {
				public int compare(Attendance m1, Attendance m2) {
					return m1.getAttendanceDate().compareTo(
							m2.getAttendanceDate());
				}
			});
			int absentCount = 0;
			for (Attendance attendance : attendanceList) {
				AttendanceTO to = new AttendanceTO();
				to = convertAttendanceIntoTO(attendance);
				if (attendance.getSystemSuggesion() != null
						&& attendance.getSystemSuggesion().equals(
								"Absent 1 day LOP")) {
					to = toUpdateForAbsentDay(to, "Absent");
					absentCount++;
				} else if (attendance.getSystemSuggesion() != null
						&& attendance.getSystemSuggesion().equals(
								"Weekend/Holiday")) {
					to = toUpdateForAbsentDay(to, "Weekend/Holiday");
				} else if (attendance.getSystemSuggesion() != null
						&& attendance.getSystemSuggesion().equals(
								"UnPaid Leave")) {
					to = toUpdateForAbsentDay(to, "UnPaid Leave");
				} else if (attendance.getSystemSuggesion() != null
						&& attendance.getSystemSuggesion().equals("Paid Leave")) {
					to = toUpdateForAbsentDay(to, "On Leave");
				}
				attendanceTOs.add(to);

			}

			atteTO.setAttendanceHistoryList(attendanceTOs);
		}
		return atteTO;
	}

	public AttendanceTO convertAttendanceIntoTO(Attendance attendance) {
		AttendanceTO attendanceTo = new AttendanceTO();
		try {
			BeanUtils.copyProperties(attendanceTo, attendance);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		attendanceTo.setAttendanceId(attendance.getAttendanceId());
		attendanceTo.setPersonName(attendance.getPerson().getFirstName() + " "
				+ attendance.getPerson().getLastName());
		JobAssignment jobAssignment = attendance.getJobAssignment();

		attendanceTo.setCompanyName(jobAssignment.getCmpDeptLocation()
				.getCompany().getCompanyName());
		attendanceTo.setDepartmentName(jobAssignment.getCmpDeptLocation()
				.getDepartment().getDepartmentName());
		attendanceTo.setLocationName(jobAssignment.getCmpDeptLocation()
				.getLocation().getLocationName());
		attendanceTo.setDesignationName(jobAssignment.getDesignation()
				.getDesignationName());
		attendanceTo.setAttedanceDate(DateFormat.convertDateToString(attendance
				.getAttendanceDate().toString())
				+ " [ "
				+ DateFormat.getDay(attendance.getAttendanceDate()) + " ]");
		attendanceTo.setInTime(DateFormat.getTimeOnly(attendance.getTimeIn()));
		attendanceTo
				.setOutTime(DateFormat.getTimeOnly(attendance.getTimeOut()));
		if (attendance.getLateIn() != null
				&& !attendance.getLateIn().equals("")) {
			attendanceTo.setLateStr("Late In :" + attendance.getLateIn());
			attendanceTo.setInPart("Late In :" + attendance.getLateIn());
		} else {
			attendanceTo.setInPart("Early In :" + attendance.getEarlyIn());
		}
		if (attendance.getLateOut() != null
				&& !attendance.getLateOut().equals("")) {
			attendanceTo.setLateStr("Late Out :" + attendance.getLateOut());
			attendanceTo.setOutPart("Late Out :" + attendance.getLateOut());
		} else {
			attendanceTo.setOutPart("Early Out :" + attendance.getEarlyOut());
		}
		if (attendance.getEarlyIn() != null
				&& !attendance.getEarlyIn().equals(""))
			attendanceTo.setEarlyStr("Early In :" + attendance.getEarlyIn());
		if (attendance.getEarlyOut() != null
				&& !attendance.getEarlyOut().equals(""))
			attendanceTo.setEarlyStr("Early Out :" + attendance.getEarlyOut());
		if (attendance.getTimeLoss() != null
				&& !attendance.getTimeLoss().equals(""))
			attendanceTo.setTimeLossOrExcess("Time Loss :"
					+ attendance.getTimeLoss());
		if (attendance.getTimeExcess() != null
				&& !attendance.getTimeExcess().equals(""))
			attendanceTo.setTimeLossOrExcess("Time Excess :"
					+ attendance.getTimeExcess());

		attendanceTo.setTotalHours(attendance.getTotalHours());
		attendanceTo.setDecision(attendanceProcessTypeInfo(attendance
				.getDeductionType()));
		return attendanceTo;

	}

	public AttendanceTO toUpdateForAbsentDay(AttendanceTO atteTO,
			String decision) {
		atteTO.setDecision(decision);
		atteTO.setTotalHours("");
		atteTO.setIsApproveStr("-NA-");
		atteTO.setLateStr("");
		atteTO.setEarlyStr("");
		atteTO.setInTime("");
		atteTO.setOutTime("");
		atteTO.setInPart("");
		atteTO.setOutPart("");
		return atteTO;
	}

	// Consolidated report
	public AttendanceTO consolidatedPunchReport(Long companyId, Date fromDate,
			Date toDate) {
		List<AttendanceTO> attendanceTOs = new ArrayList<AttendanceTO>();
		AttendanceTO atteTO = new AttendanceTO();
		List<Attendance> attendanceList = null;
		if (companyId != null && companyId > 0)
			attendanceList = attendanceService.getCompanyAttendancePunchReport(
					companyId, fromDate, toDate);
		else
			attendanceList = attendanceService
					.getAllAttendancePunchReportBasedPeriod(
							getImplementation(), fromDate, toDate);

		if (attendanceList != null && attendanceList.size() > 0) {
			Map<Long, List<Attendance>> data = new HashMap<Long, List<Attendance>>();
			for (Attendance attendance : attendanceList) {
				List<Attendance> innerList = null;
				if (data != null
						&& !data.containsKey(attendance.getPerson()
								.getPersonId())) {
					innerList = new ArrayList<Attendance>();
					innerList.add(attendance);
					data.put(attendance.getPerson().getPersonId(), innerList);
				} else {
					data.get(attendance.getPerson().getPersonId()).add(
							attendance);
				}

			}
			Iterator it = data.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry) it.next();

				// Punch Report Loss Excess Days Mapping
				lateInDays = 0;
				lateOutDays = 0;
				earlyInDays = 0;
				earlyOutDays = 0;
				timeLossDyas = 0;
				timeExcessDays = 0;
				double grandTotal = 0.0;
				AttendanceTO to = new AttendanceTO();

				for (Attendance attendance : (List<Attendance>) pairs
						.getValue()) {
					JobAssignment jobAssignment = attendance.getJobAssignment();

					to.setPersonName(attendance.getPerson().getFirstName()
							+ " " + attendance.getPerson().getLastName());
					to.setCompanyName(jobAssignment.getCmpDeptLocation()
							.getCompany().getCompanyName());
					to.setDepartmentName(jobAssignment.getCmpDeptLocation()
							.getDepartment().getDepartmentName());
					to.setLocationName(jobAssignment.getCmpDeptLocation()
							.getLocation().getLocationName());
					to.setDesignationName(jobAssignment.getDesignation()
							.getDesignationName());

					if (attendance.getLateIn() != null
							&& !attendance.getLateIn().equals(""))
						lateInDays += 1;
					if (attendance.getLateOut() != null
							&& !attendance.getLateOut().equals(""))
						lateOutDays += 1;
					if (attendance.getEarlyIn() != null
							&& !attendance.getEarlyIn().equals(""))
						earlyInDays += 1;
					if (attendance.getEarlyOut() != null
							&& !attendance.getEarlyOut().equals(""))
						earlyOutDays += 1;
					if (attendance.getTimeLoss() != null
							&& !attendance.getTimeLoss().equals(""))
						timeLossDyas += 1;
					if (attendance.getTimeExcess() != null
							&& !attendance.getTimeExcess().equals(""))
						timeExcessDays += 1;

					if (attendance.getTotalHours() != null
							&& !attendance.getTotalHours().equals(""))
						grandTotal += Double
								.valueOf(attendance.getTotalHours());

					if (attendance.getSystemSuggesion() != null
							&& attendance.getSystemSuggesion().equals(
									"Absent 1 day LOP")) {
						to.setAbsentDays(to.getAbsentDays() + 1);
					}

				}
				to.setLateInDays(lateInDays);
				to.setLateOutDays(lateOutDays);
				to.setEarlyInDays(earlyInDays);
				to.setEarlyOutDays(earlyOutDays);
				to.setTimeLossDyas(timeLossDyas);
				to.setTimeExcessDays(timeExcessDays);
				to.setTotalHours(Math.round(grandTotal) + "");
				attendanceTOs.add(to);

			}
			atteTO.setAttendanceHistoryList(attendanceTOs);

		}
		return atteTO;
	}

	public void getAttendanceDeviationSave(Attendance attendance) {

		attendanceService.saveAttendance(attendance);

		try {
			if (attendance.getRelativeId() == null) {
				attendance.setRelativeId(attendance.getAttendanceId());
				attendanceService.saveAttendance(attendance);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		List<SwipeInOut> tempswipeInOuts = attendanceService
				.getAllSwipeInOutWithDate(attendance.getJobAssignment()
						.getSwipeId(), attendance.getImplementation(),
						attendance.getAttendanceDate());
		if (tempswipeInOuts != null && tempswipeInOuts.size() > 0) {
			for (SwipeInOut swipeInout : tempswipeInOuts)
				swipeInout.setStatus(Byte.parseByte("2"));

			attendanceService.saveSwipeInOutRecords(tempswipeInOuts);
		}

	}

	public void deleteAttendance(Attendance attendanceT) throws Exception {

		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Attendance.class.getSimpleName(),
				attendanceT.getAttendanceId(), user, workflowDetailVo);

	}

	public void doAttendanceBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		Attendance attendance = attendanceService
				.getAttendanceInfoForDevationProcess(recordId);

		if (workflowDetailVO.isDeleteFlag()) {
			attendanceService.deleteAttendance(attendance);
			List<SwipeInOut> tempswipeInOuts = attendanceService
					.getAllSwipeInOutWithDate(attendance.getJobAssignment()
							.getSwipeId(), attendance.getImplementation(),
							attendance.getAttendanceDate());
			attendanceService.deleteSwipeInOutRecords(tempswipeInOuts);
		} else {
		}
	}

	public List<SwipeInOutVO> getFilteredSwipeInOutsVO(Long jobAssignmentId,
			Date fromDate, Date toDate, Implementation implementation)
			throws Exception {
		List<SwipeInOutVO> swipeInOutVOs = new ArrayList<SwipeInOutVO>();
		try {
			List<SwipeInOut> swipeInOuts = null;
			/*
			 * if (jobAssignmentId != null) { List<Long> list = new
			 * ArrayList<Long>(); list.add(jobAssignmentId); swipeInOuts =
			 * attendanceService .getSwipeInOutsByJobAssignmentIds(list);
			 * 
			 * } else {
			 */
			swipeInOuts = attendanceService.getSwipeInOutsByCriteria(fromDate,
					toDate, implementation, jobAssignmentId);
			for (SwipeInOut swipe : swipeInOuts) {
				SwipeInOutVO swipeInOutVO = new SwipeInOutVO();
				swipeInOutVO.setImplementation(swipe.getImplementation());
				swipeInOutVO.setSwipeId(swipe.getSwipeId());
				swipeInOutVO.setAttendanceDate(swipe.getAttendanceDate());
				swipeInOutVO.setTimeInOutType(swipe.getTimeInOutType());
				swipeInOutVO.setTimeInOut(swipe.getTimeInOut());
				swipeInOutVO.setDoorNumber(swipe.getDoorNumber());
				swipeInOutVO.setStatus(swipe.getStatus());
				swipeInOutVO.setStrSwipeId(AIOSCommons.longToString(
						swipeInOutVO.getSwipeId(), 8));

				List<JobAssignment> jobAssignments = jobAssignmentBL
						.getJobAssignmentService()
						.getJobAssignmentAgainstSwipeId(swipe.getSwipeId(),implementation);
				if (jobAssignments != null && jobAssignments.size() > 0) {
					JobAssignmentBL jobAssignmentBL = new JobAssignmentBL();
					swipeInOutVO.setJobAssignmentVO(jobAssignmentBL
							.convertEntityToVO(jobAssignments.get(0)));
					swipeInOutVO.setEmployeeName(swipeInOutVO
							.getJobAssignmentVO().getPersonName());
					swipeInOutVO.setDesignation(swipeInOutVO
							.getJobAssignmentVO().getDesignationName());
				} else {
					swipeInOutVO.setEmployeeName("Not Yet Mapped");
					swipeInOutVO.setDesignation("-NA-");
				}

				swipeInOutVOs.add(swipeInOutVO);
			}
			Collections.sort(swipeInOutVOs, new Comparator<SwipeInOutVO>() {
				public int compare(SwipeInOutVO m1, SwipeInOutVO m2) {
					return m1.getTimeInOut().compareTo(m2.getTimeInOut());
				}
			});

			Collections.sort(swipeInOutVOs, new Comparator<SwipeInOutVO>() {
				public int compare(SwipeInOutVO m1, SwipeInOutVO m2) {
					return m1.getAttendanceDate().compareTo(
							m2.getAttendanceDate());
				}
			});

			Collections.sort(swipeInOutVOs, new Comparator<SwipeInOutVO>() {
				public int compare(SwipeInOutVO m1, SwipeInOutVO m2) {
					return m1.getEmployeeName().compareTo(m2.getEmployeeName());
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return swipeInOutVOs;
	}

	public String saveSwipes(SwipeVO vo) {
		String returnMessage = "SUCCESS";
		try {
			List<SwipeInOut> datas = new ArrayList<SwipeInOut>();
			SwipeInOut record = null;
			Implementation implementation = null;
			for (SwipeVO swipeVo : vo.getVos()) {
				record = new SwipeInOut();
				record.setAttendanceDate(swipeVo.getDate());
				record.setTimeInOut(swipeVo.getTime());
				record.setTimeInOutType(swipeVo.getInOutType());
				implementation = new Implementation();
				implementation.setImplementationId((long) swipeVo
						.getImplementationId());
				record.setImplementation(implementation);
				record.setDoorNumber(swipeVo.getDoorNumber());
				record.setSwipeId((long) swipeVo.getUserReference());
				record.setStatus((byte) 1);
				datas.add(record);

			}
			attendanceService.saveSwipeInOutRecords(datas);
		} catch (Exception e) {
			e.printStackTrace();
			returnMessage = "ERROR";
		}
		return returnMessage;
	}

	public Map<Integer, String> calculationMethods() {
		Map<Integer, String> types = new HashMap<Integer, String>();
		types.put(Constants.HR.AttendanceProcess.NoAction.getCode(),
				Constants.HR.AttendanceProcess.NoAction.name());
		types.put(Constants.HR.AttendanceProcess.TimeAndQuarter.getCode(),
				Constants.HR.AttendanceProcess.TimeAndQuarter.name());
		types.put(Constants.HR.AttendanceProcess.TimeAndHalf.getCode(),
				Constants.HR.AttendanceProcess.TimeAndHalf.name());
		types.put(Constants.HR.AttendanceProcess.FullTime.getCode(),
				Constants.HR.AttendanceProcess.FullTime.name());
		types.put(Constants.HR.AttendanceProcess.OneAndHalf.getCode(),
				Constants.HR.AttendanceProcess.OneAndHalf.name());
		types.put(Constants.HR.AttendanceProcess.DoubleTime.getCode(),
				Constants.HR.AttendanceProcess.DoubleTime.name());
		return types;
	}

	// Getter Setter
	public AttendanceService getAttendanceService() {
		return attendanceService;
	}

	public void setAttendanceService(AttendanceService attendanceService) {
		this.attendanceService = attendanceService;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public LeaveProcessService getLeaveProcessService() {
		return leaveProcessService;
	}

	public void setLeaveProcessService(LeaveProcessService leaveProcessService) {
		this.leaveProcessService = leaveProcessService;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Integer getLateInDays() {
		return lateInDays;
	}

	public void setLateInDays(Integer lateInDays) {
		this.lateInDays = lateInDays;
	}

	public Integer getLateOutDays() {
		return lateOutDays;
	}

	public void setLateOutDays(Integer lateOutDays) {
		this.lateOutDays = lateOutDays;
	}

	public Integer getEarlyInDays() {
		return earlyInDays;
	}

	public void setEarlyInDays(Integer earlyInDays) {
		this.earlyInDays = earlyInDays;
	}

	public Integer getEarlyOutDays() {
		return earlyOutDays;
	}

	public void setEarlyOutDays(Integer earlyOutDays) {
		this.earlyOutDays = earlyOutDays;
	}

	public Integer getTimeLossDyas() {
		return timeLossDyas;
	}

	public void setTimeLossDyas(Integer timeLossDyas) {
		this.timeLossDyas = timeLossDyas;
	}

	public Integer getTimeExcessDays() {
		return timeExcessDays;
	}

	public void setTimeExcessDays(Integer timeExcessDays) {
		this.timeExcessDays = timeExcessDays;
	}

	public CompanyService getCompanyService() {
		return companyService;
	}

	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}

	public CommentService getCommentService() {
		return commentService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public JobAssignmentBL getJobAssignmentBL() {
		return jobAssignmentBL;
	}

	public void setJobAssignmentBL(JobAssignmentBL jobAssignmentBL) {
		this.jobAssignmentBL = jobAssignmentBL;
	}

	public EmployeeCalendarBL getEmployeeCalendarBL() {
		return employeeCalendarBL;
	}

	public void setEmployeeCalendarBL(EmployeeCalendarBL employeeCalendarBL) {
		this.employeeCalendarBL = employeeCalendarBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

}
