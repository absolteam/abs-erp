package com.aiotech.aios.hr.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.hr.domain.entity.EmployeeLoan;
import com.aiotech.aios.hr.domain.entity.EmployeeLoanRepayment;
import com.aiotech.aios.hr.domain.entity.vo.PayrollVO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class EmployeeLoanService {

	public AIOTechGenericDAO<EmployeeLoan> employeeLoanDAO;
	public AIOTechGenericDAO<EmployeeLoanRepayment> employeeLoanRepaymentDAO;

	public List<EmployeeLoan> getAllEmployeeLoan(Implementation implementation) {
		return employeeLoanDAO.findByNamedQuery("getAllEmployeeLoan",
				implementation);
	}
	
	public EmployeeLoan getEmployeeLoanDetail(Long employeeLoanId) {
		List<EmployeeLoan> employeeLoans= employeeLoanDAO.findByNamedQuery("getEmployeeLoanDetail",
				employeeLoanId);
		
		if(employeeLoans!=null && employeeLoans.size()>0)
			return employeeLoans.get(0);
		else
			return null;
	}
	
	public List<EmployeeLoanRepayment> getEmployeeLoanRepayments(
			PayrollVO payrollVO) {
		try {
			Criteria jobPayrollCriteria = employeeLoanRepaymentDAO
					.createCriteria();
			jobPayrollCriteria.createAlias("employeeLoan", "el");
			jobPayrollCriteria.createAlias("el.jobAssignment", "ja",
					CriteriaSpecification.LEFT_JOIN);
			jobPayrollCriteria.createAlias("ja.person", "person",
					CriteriaSpecification.LEFT_JOIN);

			if (payrollVO.getPayPeriodTransaction() != null
					&& payrollVO.getPayPeriodTransaction().getStartDate() != null) {
				jobPayrollCriteria.add(Restrictions.conjunction().add(
						Restrictions.ge("dueDate", payrollVO
								.getPayPeriodTransaction().getStartDate())));
				jobPayrollCriteria.add(Restrictions.conjunction().add(
						Restrictions.le("dueDate", payrollVO
								.getPayPeriodTransaction().getEndDate())));
			}

			if (payrollVO.getJobAssignment().getPerson().getPersonId() != null) {
				jobPayrollCriteria
						.add(Restrictions.eq("person.personId", payrollVO
								.getJobAssignment().getPerson().getPersonId()));
			}

			jobPayrollCriteria.add(Restrictions.isNull("status"));
			// Repayment type must be salary
			jobPayrollCriteria.add(Restrictions.eq("el.repaymentType", 1));

			jobPayrollCriteria.add(Restrictions.eq("el.financeType",
					payrollVO.getLoanFinanceType()));

			return jobPayrollCriteria.list();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	

	public void employeeLoanSave(EmployeeLoan employeeLoan) {
		employeeLoanDAO.saveOrUpdate(employeeLoan);
	}

	public void employeeLoanRepaymentsSave(
			List<EmployeeLoanRepayment> employeeLoanRepayments) {
		employeeLoanRepaymentDAO.saveOrUpdateAll(employeeLoanRepayments);
	}
	
	public void deleteEmployeeLoanRepayments(
			List<EmployeeLoanRepayment> employeeLoanRepayments) {
		employeeLoanRepaymentDAO.deleteAll(employeeLoanRepayments);
	}
	
	public void employeeLoanDelete(EmployeeLoan employeeLoan) {
		employeeLoanDAO.delete(employeeLoan);
	}

	public void employeeLoanRepaymentsDelete(
			List<EmployeeLoanRepayment> employeeLoanRepayments) {
		employeeLoanRepaymentDAO.deleteAll(employeeLoanRepayments);
	}

	public AIOTechGenericDAO<EmployeeLoan> getEmployeeLoanDAO() {
		return employeeLoanDAO;
	}

	public void setEmployeeLoanDAO(
			AIOTechGenericDAO<EmployeeLoan> employeeLoanDAO) {
		this.employeeLoanDAO = employeeLoanDAO;
	}

	public AIOTechGenericDAO<EmployeeLoanRepayment> getEmployeeLoanRepaymentDAO() {
		return employeeLoanRepaymentDAO;
	}

	public void setEmployeeLoanRepaymentDAO(
			AIOTechGenericDAO<EmployeeLoanRepayment> employeeLoanRepaymentDAO) {
		this.employeeLoanRepaymentDAO = employeeLoanRepaymentDAO;
	}
}
