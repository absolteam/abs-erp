package com.aiotech.aios.hr.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.hr.domain.entity.PayByCount;
import com.aiotech.aios.hr.domain.entity.vo.PayByCountVO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class PayByCountService {
	public AIOTechGenericDAO<PayByCount> payByCountDAO;

	public List<PayByCount> getAllPayByCountByCriteria(
			PayByCountVO vO) {
			Criteria jobPayrollCriteria = payByCountDAO.createCriteria();

			jobPayrollCriteria.createAlias("jobAssignment", "ja");
			
			jobPayrollCriteria.createAlias("personByCreatedBy",
					"person", CriteriaSpecification.LEFT_JOIN);
			
			jobPayrollCriteria.createAlias("payrollElement",
					"pe", CriteriaSpecification.LEFT_JOIN);
			
			jobPayrollCriteria.createAlias("jobPayrollElement", "jpa");
			jobPayrollCriteria.createAlias("jpa.payrollElementByPayrollElementId",
					"payroll");
			
			if (vO.getPayPeriodTransaction() != null
					&& vO.getPayPeriodTransaction().getStartDate() != null) {
				jobPayrollCriteria.add(Restrictions.conjunction().add(
						Restrictions.ge("fromDate", vO.getPayPeriodTransaction()
								.getStartDate())));
				jobPayrollCriteria.add(Restrictions.conjunction().add(
						Restrictions.le("toDate", vO.getPayPeriodTransaction()
								.getEndDate())));
			}

			if (vO.getJobAssignmentId() != null) {
				jobPayrollCriteria.add(Restrictions.eq("ja.jobAssignmentId",
						vO.getJobAssignmentId()));
			}

			if (vO.getJobPayrollElementId() != null) {
				jobPayrollCriteria.add(Restrictions.eq("jpa.jobPayrollElementId",
						vO.getJobPayrollElementId()));
			}

			return jobPayrollCriteria.list();
	}

	public List<PayByCount> getAllPayByCount(
			Implementation implementation) {
		return payByCountDAO.findByNamedQuery("getAllPayByCount",
				implementation);
	}

	public PayByCount getPayByCountById(Long id) {
		List<PayByCount> allowances = payByCountDAO
				.findByNamedQuery("getPayByCountById", id);
		if (allowances != null && allowances.size() > 0)
			return allowances.get(0);
		else
			return null;
	}

	public void savePayByCount(PayByCount payByCount) {
		payByCountDAO.saveOrUpdate(payByCount);
	}

	public void deletePayByCount(PayByCount payByCount) {
		payByCountDAO.delete(payByCount);
	}


	public AIOTechGenericDAO<PayByCount> getPayByCountDAO() {
		return payByCountDAO;
	}

	public void setPayByCountDAO(AIOTechGenericDAO<PayByCount> payByCountDAO) {
		this.payByCountDAO = payByCountDAO;
	}


}
