package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Leave;
import com.aiotech.aios.hr.domain.entity.vo.LeaveVO;
import com.aiotech.aios.hr.service.LeaveService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class LeaveBL {
	
	private LeaveService leaveService;
	private LookupMasterBL lookupMasterBL;
	private LeavePolicyBL leavePolicyBL;
	private Implementation implementation;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	public void saveLeave(Leave leave) throws Exception{
		
		leave.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (leave != null
				&& leave.getLeaveId() != null
				&& leave.getLeaveId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		leaveService.saveLeave(leave);
		
		Map<String, Object> sessionObj = ActionContext.getContext()
		.getSession();
		User user = (User) sessionObj.get("USER");
		
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Leave.class.getSimpleName(),
				leave.getLeaveId(), user,
				workflowDetailVo);
	}
	
		
	public void deleteLeave(Long leaveId)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Leave.class.getSimpleName(), leaveId,
				user, workflowDetailVo);
	}
		
	public void doLeaveBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
			Leave leave=leaveService.getLeaveInfo(recordId);

		if (workflowDetailVO.isDeleteFlag()) {
			leaveService.deleteLeave(leave);
		} else {
		}
	}
	
	public List<LeaveVO> convertEntitiesTOVOs(
			List<Leave> leaves) throws Exception {

		List<LeaveVO> tosList = new ArrayList<LeaveVO>();

		for (Leave pers : leaves) {
			tosList.add(convertEntityToVO(pers));
		}

		return tosList;
	}
	
	public LeaveVO convertEntityToVO(Leave leave){
		LeaveVO leaveVO=new LeaveVO();
		leaveVO.setLeaveId(leave.getLeaveId());
		leaveVO.setDisplayName(leave.getDisplayName());
		leaveVO.setLookupDetail(leave.getLookupDetail());
		leaveVO.setImplementation(leave.getImplementation());
		leaveVO.setIsMonthly(leave.getIsMonthly());
		leaveVO.setIsYearly(leave.getIsYearly());
		leaveVO.setIsService(leave.getIsService());
		leaveVO.setIsWeekly(leave.getIsWeekly());
		leaveVO.setIsServiceCount(leave.getIsServiceCount());
		leaveVO.setDefaultDays(leave.getDefaultDays());
		leaveVO.setIsPaid(leave.getIsPaid());
		leaveVO.setFullPaidDays(leave.getFullPaidDays());
		leaveVO.setHalfPaidDays(leave.getHalfPaidDays());
		leaveVO.setCarryForwardCount(leave.getCarryForwardCount());
		leaveVO.setCanEncash(leave.getCanEncash());
		leaveVO.setPersonByCreatedBy(leave.getPersonByCreatedBy());
		leaveVO.setCreatedDate(leave.getCreatedDate());
		leaveVO.setPersonByUpdatedBy(leave.getPersonByUpdatedBy());
		leaveVO.setUpdatedDate(leave.getUpdatedDate());
		leaveVO.setIsActive(leave.getIsActive());
		//Lookup Detail Mapping
		if(leave.getLookupDetail()!=null){
			leaveVO.setLeaveCode(leave.getLookupDetail().getAccessCode());
			leaveVO.setLeaveType(leave.getLookupDetail().getDisplayName());
			leaveVO.setLeaveTypeId(leave.getLookupDetail().getLookupDetailId());
		}
		
		if(leave.getIsMonthly()!=null && leave.getIsMonthly()==true){
			leaveVO.setIsMonthlyStr("YES");
		}else{
			leaveVO.setIsMonthlyStr("NO");
		}
		
		if(leave.getIsYearly()!=null && leave.getIsYearly()==true){
			leaveVO.setIsYearlyStr("YES");
		}else{
			leaveVO.setIsYearlyStr("NO");
		}
		
		if(leave.getIsService()!=null && leave.getIsService()==true){
			leaveVO.setIsServiceStr("YES");
		}else{
			leaveVO.setIsServiceStr("NO");
		}
		
		if(leave.getIsPaid()!=null && leave.getIsPaid()==true){
			leaveVO.setIsPaidStr("YES");
		}else{
			leaveVO.setIsPaidStr("NO");
		}
	
		
		if(leave.getIsActive()!=null && leave.getIsActive()==true){
			leaveVO.setIsActiveStr("YES");
		}else{
			leaveVO.setIsActiveStr("NO");
		}
		
		if(leave.getCanEncash()!=null && leave.getCanEncash()==true){
			leaveVO.setCanEncashStr("YES");
		}else{
			leaveVO.setCanEncashStr("NO");
		}
		
		if(leave.getPersonByCreatedBy()!=null)
			leaveVO.setCreatedPerson(leave.getPersonByCreatedBy().getFirstName()+" "+leave.getPersonByCreatedBy().getLastName());
		
		if(leave.getCreatedDate()!=null)
			leaveVO.setCreatedDateStr(DateFormat.convertDateToString(leave.getCreatedDate()+""));
		
		if(leave.getPersonByUpdatedBy()!=null)
			leaveVO.setCreatedPerson(leave.getPersonByUpdatedBy().getFirstName()+" "+leave.getPersonByUpdatedBy().getLastName());
		
		if(leave.getUpdatedDate()!=null)
			leaveVO.setUpdatedDateStr(DateFormat.convertDateToString(leave.getUpdatedDate()+""));
		
	
		return leaveVO;
	}
	
	
	public LeaveService getLeaveService() {
		return leaveService;
	}

	public void setLeaveService(LeaveService leaveService) {
		this.leaveService = leaveService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public LeavePolicyBL getLeavePolicyBL() {
		return leavePolicyBL;
	}

	public void setLeavePolicyBL(LeavePolicyBL leavePolicyBL) {
		this.leavePolicyBL = leavePolicyBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}


	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}


	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}


	public CommentBL getCommentBL() {
		return commentBL;
	}


	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

}
