package com.aiotech.aios.hr.service;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.JobShift;
import com.aiotech.aios.hr.domain.entity.WorkingShift;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class JobShiftService {
	public AIOTechGenericDAO<WorkingShift> workingShiftDAO;
	public AIOTechGenericDAO<JobShift> jobShiftDAO;
	
	//Get shifts list
	public List<WorkingShift> getShiftWithEmployee(Long workingShiftId) {
		return workingShiftDAO.findByNamedQuery("getShiftWithEmployee", workingShiftId);
	}
	
	public List<WorkingShift> getAllActiveShift(Implementation implementation) {
		return workingShiftDAO.findByNamedQuery("getAllActiveShift", implementation);
	}
	public AIOTechGenericDAO<WorkingShift> getWorkingShiftDAO() {
		return workingShiftDAO;
	}
	public void setWorkingShiftDAO(AIOTechGenericDAO<WorkingShift> workingShiftDAO) {
		this.workingShiftDAO = workingShiftDAO;
	}
	public AIOTechGenericDAO<JobShift> getJobShiftDAO() {
		return jobShiftDAO;
	}
	public void setJobShiftDAO(AIOTechGenericDAO<JobShift> jobShiftDAO) {
		this.jobShiftDAO = jobShiftDAO;
	}
}
