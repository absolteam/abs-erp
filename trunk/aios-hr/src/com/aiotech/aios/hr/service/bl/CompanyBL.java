package com.aiotech.aios.hr.service.bl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Session;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.service.GLCombinationService;
import com.aiotech.aios.accounts.service.bl.CombinationBL;
import com.aiotech.aios.accounts.service.bl.CustomerBL;
import com.aiotech.aios.common.to.FileVO;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.CompanyTypeAllocation;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.domain.entity.vo.CompanyVO;
import com.aiotech.aios.hr.service.CmpDeptLocService;
import com.aiotech.aios.hr.service.CompanyService;
import com.aiotech.aios.hr.service.LocationService;
import com.aiotech.aios.hr.to.CompanyTO;
import com.aiotech.aios.hr.to.converter.CompanyTOConvert;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Document;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.service.DocumentService;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class CompanyBL {

	DocumentService documentService;
	CompanyService companyService;
	LocationService locationService;
	DirectoryBL directoryBL;
	DocumentBL documentBL;
	private SupplierBL supplierBL;
	private CustomerBL customerBL;
	private LookupMasterBL lookupMasterBL;
	private GLCombinationService combinationService;
	private CombinationBL combinationBL;
	private Implementation implementation;

	List<CompanyTO> companyTOs = null;
	CompanyTO companyTO = null;
	List<Company> companys = null;
	Company company = null;
	private CmpDeptLocService cmpDeptLocService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public List<CompanyTO> companyList(Implementation implementation)
			throws Exception {
		companyTOs = new ArrayList<CompanyTO>();
		List<CompanyTO> companyTOs1 = null;
		companys = companyService.getAllCompany(implementation);
		// companys=companyTypeFilteration("1",companys);
		companyTOs = CompanyTOConvert.convertToTOList(companys);
		for (CompanyTO comp : companyTOs) {
			companyTOs1 = new ArrayList<CompanyTO>();
			companyTOs1 = this.getTypeList();
			for (CompanyTO comp1 : companyTOs1) {
				if (comp.getCompanyTypeId() == comp1.getCompanyTypeId()) {
					comp.setCompanyTypeName(comp1.getCompanyTypeName());
				}

			}
		}
		return companyTOs;
	}

	public List<Company> commonCompanyList() {
		List<Company> onlyCompanys = new ArrayList<Company>();
		List<Company> companys = companyService
				.getAllCompany(getImplementation());
		if (companys != null && companys.size() > 0) {
			for (Company company : companys) {
				List<CompanyTypeAllocation> cmpTypAllocations = new ArrayList<CompanyTypeAllocation>(
						company.getCompanyTypeAllocations());
				for (CompanyTypeAllocation companyTypeAllocation : cmpTypAllocations) {
					if (companyTypeAllocation.getLookupDetail().getDataId() == 2
							|| companyTypeAllocation.getLookupDetail()
									.getDataId() == 6
							|| companyTypeAllocation.getLookupDetail()
									.getDataId() == 8
							|| companyTypeAllocation.getLookupDetail()
									.getDataId() == 12) {
						company.getCompanyTypeAllocations().remove(
								companyTypeAllocation);
					}
					if (company.getCompanyTypeAllocations() != null
							&& company.getCompanyTypeAllocations().size() > 0)
						onlyCompanys.add(company);
				}
			}
		}
		return onlyCompanys;
	}

	public void savecmpDeptLoc(CmpDeptLoc cmpDeptLocation) throws Exception {
		this.getCmpDeptLocService().saveCmpDeptLocation(cmpDeptLocation);
	}

	public void deleteCompany(Company company,
			List<CompanyTypeAllocation> companytypes) {
		try {
			companyService.deleteCompanyTypes(companytypes);
			companyService.deleteCompany(company);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void udpateCompany(Company company, Session session) {
		try {
			session.getTransaction().begin();
			companyService.updateCompany(company, session);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.clear();
			session.clear();
		}
	}

	public void udpateCompany(Company company) throws Exception {
		companyService.updateCompany(company);
	}

	public void saveCompany(List<Identity> identitys, Company company,
			CompanyVO companyVO) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		company.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (company != null && company.getCompanyId() != null
				&& company.getCompanyId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		Combination tenantCombination = null;
		Combination customerCombination = null;
		Combination supplierCombination = null;
		Combination ownerCombination = null;

		companyService.saveCompany(company, identitys);

		if (companyVO.getSupplier() != null)
			companyVO.getSupplier().setCompany(company);
		if (companyVO.getCustomer() != null)
			companyVO.getCustomer().setCompany(company);

		if (companyVO.getCompanyId() != null && companyVO.getCompanyId() > 0
				&& companyVO.getCompanyEdit() != null) {
			String currentName = company.getCompanyName();
			String oldName = companyVO.getCompanyEdit().getCompanyName();
			if (!currentName.equalsIgnoreCase(oldName)) {

				tenantCombination = getCombinationInfo(
						companyVO.getTenantCombinationId(), currentName
								+ "-TENANT", oldName + "-TENANT");

				customerCombination = getCombinationInfo(
						companyVO.getCustomerCombinationId(), currentName
								+ "-CUSTOMER", oldName + "-CUSTOMER");

				supplierCombination = getCombinationInfo(
						companyVO.getSupplierCombinationId(), currentName
								+ "-SUPPLIER", oldName + "-SUPPLIER");

				ownerCombination = getCombinationInfo(
						companyVO.getOwnerCombinationId(), currentName
								+ "-OWNER", oldName + "-OWNER");
			} else {

				tenantCombination = getCombinationInfo(
						companyVO.getTenantCombinationId(), currentName
								+ "-TENANT", null);

				customerCombination = getCombinationInfo(
						companyVO.getCustomerCombinationId(), currentName
								+ "-CUSTOMER", null);

				supplierCombination = getCombinationInfo(
						companyVO.getSupplierCombinationId(), currentName
								+ "-SUPPLIER", null);

				ownerCombination = getCombinationInfo(
						companyVO.getOwnerCombinationId(), currentName
								+ "-OWNER", null);
			}
		} else {

			tenantCombination = getCombinationInfo(
					companyVO.getTenantCombinationId(),
					companyVO.getCompanyName() + "-TENANT", null);

			customerCombination = getCombinationInfo(
					companyVO.getCustomerCombinationId(),
					companyVO.getCompanyName() + "-CUSTOMER", null);

			supplierCombination = getCombinationInfo(
					companyVO.getSupplierCombinationId(),
					companyVO.getCompanyName() + "-SUPPLIER", null);

			ownerCombination = getCombinationInfo(
					companyVO.getOwnerCombinationId(),
					companyVO.getCompanyName() + "-OWNER", null);
		}

		// CompanyType Allocation Call
		List<LookupDetail> personTypeList = lookupMasterBL
				.getActiveLookupDetails("COMPANY_TYPE", true);
		Map<Integer, LookupDetail> map = new HashMap<Integer, LookupDetail>();
		for (LookupDetail el : personTypeList) {
			map.put(el.getDataId(), el);
		}
		String[] lineDetailArray = splitValues(companyVO.getCompanyTypes(), ",");
		List<CompanyTypeAllocation> companyTypeAllocationList = new ArrayList<CompanyTypeAllocation>();
		for (String personTyp : lineDetailArray) {
			CompanyTypeAllocation companyTypeAllocation = new CompanyTypeAllocation();
			companyTypeAllocation.setLookupDetail(map.get(Integer
					.valueOf(personTyp)));
			companyTypeAllocation.setCompany(company);
			companyTypeAllocationList.add(companyTypeAllocation);
		}
		List<CompanyTypeAllocation> personTypeAllocationToDelete = companyService
				.getAllCompanyTypeAllocation(company);
		companyService.deleteCompanyTypes(personTypeAllocationToDelete);
		companyService.saveCompanyTypes(companyTypeAllocationList);

		// Supplier & Customer Data mapping
		if (/*
			 * companyVO.getCustomerCombinationId() != null &&
			 * companyVO.getCustomerCombinationId() > 0 &&
			 */companyVO.getCustomer() != null) {
			companyVO.getCustomer().setCombination(customerCombination);
			customerBL.saveCustomer(companyVO.getCustomer(),
					companyVO.getShippingDetailList());
		}
		if (/*
			 * companyVO.getSupplierCombinationId() != null &&
			 * companyVO.getSupplierCombinationId() > 0 &&
			 */companyVO.getSupplier() != null) {
			companyVO.getSupplier().setCombination(supplierCombination);
			supplierBL.saveSupplier(companyVO.getSupplier(),
					supplierCombination.getCombinationId(),
					companyVO.getCurrencyId());
		}

		if (companyVO.getCompanyId() != null && companyVO.getCompanyId() > 0) {
			saveCompanyDocuments(company, true);
		} else {
			saveCompanyDocuments(company, false);
		}

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Company.class.getSimpleName(), company.getCompanyId(), user,
				workflowDetailVo);
	}

	public Combination getCombinationInfo(Long natualAccountCombinationId,
			String currentName, String oldName) throws Exception {
		Combination combination = null;
		if (natualAccountCombinationId == null
				|| natualAccountCombinationId == 0)
			return combination;

		Account account = null;
		if (oldName != null && !oldName.equals("")) {
			account = combinationService.getAccountDetail(oldName,
					getImplementation());
			account.setAccount(currentName);
			combinationBL.getAccountBL().getAccountService()
					.updateAccount(account);
		} else {
			account = combinationService.getAccountDetail(currentName,
					getImplementation());
		}
		if (account != null) {
			combination = new Combination();
			combination.setCombinationId(natualAccountCombinationId);
			combination = combinationService
					.getCombinationAllAccountDetail(combination
							.getCombinationId());
		} else {

			combination = new Combination();
			combination.setCombinationId(natualAccountCombinationId);
			combination = combinationBL.createAnalysisCombination(combination,
					currentName, false);
		}
		return combination;
	}

	public Combination findCombinationInfo(String currentName) throws Exception {
		Combination combination = null;
		Account account = null;
		account = combinationService.getAccountDetail(currentName,
				getImplementation());
		if (account != null) {
			combination = combinationService.getCombinationByAccount(account
					.getAccountId());
			combination = combinationService
					.getCombinationAllAccountDetail(combination
							.getCombinationId());
		}
		return combination;
	}

	public List<Identity> getIdentityToDelete(List<Identity> identitys,
			List<Identity> identityEdits) {
		List<Identity> identityUpdates = new ArrayList<Identity>();

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		for (Identity identity : identitys) {
			if (identity.getIdentityId() != null
					&& identity.getIdentityId() > 0)
				listOne.add(identity.getIdentityId());
		}
		for (Identity identity : identityEdits) {
			if (identity.getIdentityId() != null
					&& identity.getIdentityId() > 0)
				listTwo.add(identity.getIdentityId());
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);

		// Delete Process
		for (Long lng : different) {
			for (Identity identity : identityEdits) {
				if (lng != null && identity.getIdentityId().equals(lng)) {
					identityUpdates.add(identity);
				}
			}
		}
		return identityUpdates;
	}

	public List<CompanyTO> getTypeList() {
		List<CompanyTO> companyTos = new ArrayList<CompanyTO>();
		companyTO = new CompanyTO();
		companyTO.setCompanyTypeId(1);
		companyTO.setCompanyTypeName("Information Technology");
		companyTos.add(companyTO);

		companyTO = new CompanyTO();
		companyTO.setCompanyTypeId(2);
		companyTO.setCompanyTypeName("Owner");
		companyTos.add(companyTO);

		companyTO = new CompanyTO();
		companyTO.setCompanyTypeId(3);
		companyTO.setCompanyTypeName("Finance");
		companyTos.add(companyTO);

		companyTO = new CompanyTO();
		companyTO.setCompanyTypeId(4);
		companyTO.setCompanyTypeName("Banking");
		companyTos.add(companyTO);

		companyTO = new CompanyTO();
		companyTO.setCompanyTypeId(5);
		companyTO.setCompanyTypeName("Shopping");
		companyTos.add(companyTO);

		companyTO = new CompanyTO();
		companyTO.setCompanyTypeId(6);
		companyTO.setCompanyTypeName("Tenant");
		companyTos.add(companyTO);

		companyTO = new CompanyTO();
		companyTO.setCompanyTypeId(7);
		companyTO.setCompanyTypeName("Real Estate");
		companyTos.add(companyTO);

		companyTO = new CompanyTO();
		companyTO.setCompanyTypeId(8);
		companyTO.setCompanyTypeName("Supplier");
		companyTos.add(companyTO);

		companyTO = new CompanyTO();
		companyTO.setCompanyTypeId(9);
		companyTO.setCompanyTypeName("Manufacturing");
		companyTos.add(companyTO);

		companyTO = new CompanyTO();
		companyTO.setCompanyTypeId(10);
		companyTO.setCompanyTypeName("Maintenance");
		companyTos.add(companyTO);

		companyTO = new CompanyTO();
		companyTO.setCompanyTypeId(11);
		companyTO.setCompanyTypeName("Construction");
		companyTos.add(companyTO);

		companyTO = new CompanyTO();
		companyTO.setCompanyTypeId(12);
		companyTO.setCompanyTypeName("Customer");
		companyTos.add(companyTO);

		return companyTos;
	}

	public void saveCompanyDocuments(Company company, boolean editFlag)
			throws Exception {

		DocumentVO docVO = documentBL.populateDocumentVO(company,
				"companyDocs", editFlag);
		Map<String, String> dirs = docVO.getDirMap();

		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, company.getCompanyId(), "Company", "companyDocs");
			docVO.setDirStruct(dirStucture);
		}

		documentBL.saveUploadDocuments(docVO);

	}

	private Document populateDocument(Document document,
			Map.Entry<String, FileVO> entry, Object object) throws Exception {
		document.setName(entry.getKey());
		document.setHashedName(entry.getValue().getHashedName());
		String[] name = object.getClass().getName().split("entity.");
		document.setTableName(name[name.length - 1]);
		String methodName = "get" + name[name.length - 1] + "Id";
		Method method = object.getClass().getMethod(methodName, null);
		document.setRecordId((Long) method.invoke(object, null));
		return document;
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public List<CompanyVO> convertIdentityToVOList(List<Identity> identitys)
			throws Exception {
		List<CompanyVO> tosList = new ArrayList<CompanyVO>();

		for (Identity identity : identitys) {
			tosList.add(convertIdentityToVO(identity));
		}

		return tosList;
	}

	public CompanyVO convertIdentityToVO(Identity identity) throws Exception {
		CompanyVO infoTO = new CompanyVO();

		infoTO.setIdentityId(identity.getIdentityId());
		infoTO.setIdentityType(identity.getIdentityType());

		if (identity.getLookupDetail() != null)
			infoTO.setIdentityTypeName(identity.getLookupDetail()
					.getDisplayName());

		infoTO.setIdentityNumber(identity.getIdentityNumber());
		infoTO.setIssuedPlace(identity.getIssuedPlace());
		infoTO.setIdentityCode(identity.getIdentityCode());
		if (identity.getExpireDate() != null) {
			infoTO.setExpireDate(DateFormat.convertDateToString(identity
					.getExpireDate().toString()));
			infoTO.setDaysLeft(DateFormat.dayVariance(new Date(),
					identity.getExpireDate())
					+ "");
		}
		infoTO.setDescription(identity.getDescription());
		return infoTO;
	}

	public List<Company> companyTypeFilteration(String companyTypes,
			List<Company> companies) {
		Map<Integer, Integer> companyTypesMap = new HashMap<Integer, Integer>();
		boolean exsitfalg = true;
		for (String string : companyTypes.split(",")) {
			if (string != null && !string.trim().equals(""))
				companyTypesMap.put(Integer.valueOf(string),
						Integer.valueOf(string));
		}
		List<Company> tempResult = new ArrayList<Company>();
		tempResult.addAll(companies);
		for (Company company : tempResult) {
			exsitfalg = false;
			for (CompanyTypeAllocation compTypeAllocation : company
					.getCompanyTypeAllocations()) {
				if (companyTypesMap.containsKey(compTypeAllocation
						.getLookupDetail().getDataId())) {
					exsitfalg = true;
					break;
				}
			}
			if (!exsitfalg)
				companies.remove(company);
		}

		return companies;
	}

	public void deleteCompany(Long companyId) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Company.class.getSimpleName(), companyId, user,
				workflowDetailVo);
	}

	public void doCompanyBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		Company company = this.getCompanyService().getCompanyInfo(recordId);
		if (workflowDetailVO.isDeleteFlag()) {

			companys.add(company);
			documentBL.deleteDocuments(new ArrayList<Object>(companys),
					Company.class.getSimpleName());
			List<CompanyTypeAllocation> companytypes = new ArrayList<CompanyTypeAllocation>(
					company.getCompanyTypeAllocations());
			this.deleteCompany(company, companytypes);
		} else {
		}
	}

	// Getter Setter
	public DocumentService getDocumentService() {
		return documentService;
	}

	public void setDocumentService(DocumentService documentService) {
		this.documentService = documentService;
	}

	public CompanyService getCompanyService() {
		return companyService;
	}

	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public CmpDeptLocService getCmpDeptLocService() {
		return cmpDeptLocService;
	}

	public void setCmpDeptLocService(CmpDeptLocService cmpDeptLocService) {
		this.cmpDeptLocService = cmpDeptLocService;
	}

	public LocationService getLocationService() {
		return locationService;
	}

	public void setLocationService(LocationService locationService) {
		this.locationService = locationService;
	}

	public SupplierBL getSupplierBL() {
		return supplierBL;
	}

	public void setSupplierBL(SupplierBL supplierBL) {
		this.supplierBL = supplierBL;
	}

	public CustomerBL getCustomerBL() {
		return customerBL;
	}

	public void setCustomerBL(CustomerBL customerBL) {
		this.customerBL = customerBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public GLCombinationService getCombinationService() {
		return combinationService;
	}

	public void setCombinationService(GLCombinationService combinationService) {
		this.combinationService = combinationService;
	}

	public CombinationBL getCombinationBL() {
		return combinationBL;
	}

	public void setCombinationBL(CombinationBL combinationBL) {
		this.combinationBL = combinationBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

}
