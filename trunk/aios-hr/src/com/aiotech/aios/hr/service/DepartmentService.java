package com.aiotech.aios.hr.service;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.Department;
import com.aiotech.aios.hr.domain.entity.Location;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class DepartmentService {

	private AIOTechGenericDAO<Department> departmentDAO;

	public List<Department> getAllDepartments(Implementation implementation) throws Exception{
		return departmentDAO.findByNamedQuery("getAllDepartments", implementation);
	}
	public List<Department> getAllDepartmentsBasedOnCompany(Long comapanyId) throws Exception{
		return departmentDAO.findByNamedQuery("getAllDepartmentsBasedOnCompany", comapanyId);
	}
	
	public Department getDepartmentById(Long departmentId) throws Exception{
		return departmentDAO.findById(departmentId);
	}
	
	public void saveDepartment(Department department)throws Exception{
		departmentDAO.saveOrUpdate(department);
	}
	
	public void deleteDepartment(Department department)throws Exception{
		departmentDAO.delete(department);
	}
	
	public AIOTechGenericDAO<Department> getDepartmentDAO() {
		return departmentDAO;
	}

	public void setDepartmentDAO(AIOTechGenericDAO<Department> departmentDAO) {
		this.departmentDAO = departmentDAO;
	}
}
