package com.aiotech.aios.hr.service;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.hr.domain.entity.EmployeeCalendar;
import com.aiotech.aios.hr.domain.entity.EmployeeCalendarPeriod;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class EmployeeCalendarService {

	public AIOTechGenericDAO<EmployeeCalendar> employeeCalendarDAO;
	public AIOTechGenericDAO<EmployeeCalendarPeriod> employeeCalendarPeriodDAO;

	// Get list
	public List<EmployeeCalendar> getEmployeeCalendarList(
			Implementation implementation) {
		return employeeCalendarDAO.findByNamedQuery("getEmployeeCalendarList",
				implementation);
	}

	// Get Info
	public EmployeeCalendar getEmployeeCalendarInfo(Long employeeCalendarId) {
		List<EmployeeCalendar> employeeCalendars = new ArrayList<EmployeeCalendar>();
		employeeCalendars = employeeCalendarDAO.findByNamedQuery(
				"getEmployeeCalendarInfo", employeeCalendarId);
		if (employeeCalendars != null && employeeCalendars.size() > 0)
			return employeeCalendars.get(0);
		else
			return null;
	}

	// Get Info
	public List<EmployeeCalendar> getEmployeeCalendarByEmployee(Long personId) {
		return employeeCalendarDAO.findByNamedQuery(
				"getEmployeeCalendarByEmployee", personId);

	}

	public void saveEmployeeCalendar(EmployeeCalendar employeeCalendar) {
		employeeCalendarDAO.saveOrUpdate(employeeCalendar);
	}

	public void saveEmployeeCalendarPeriods(
			List<EmployeeCalendarPeriod> employeeCalendarPeriods) {
		employeeCalendarPeriodDAO.saveOrUpdateAll(employeeCalendarPeriods);
	}

	public void deleteEmployeeCalendar(EmployeeCalendar employeeCalendar) {
		employeeCalendarDAO.delete(employeeCalendar);
	}

	public void deleteEmployeeCalendarPeriods(
			List<EmployeeCalendarPeriod> employeeCalendarPeriods) {
		employeeCalendarPeriodDAO.deleteAll(employeeCalendarPeriods);
	}

	public AIOTechGenericDAO<EmployeeCalendar> getEmployeeCalendarDAO() {
		return employeeCalendarDAO;
	}

	public void setEmployeeCalendarDAO(
			AIOTechGenericDAO<EmployeeCalendar> employeeCalendarDAO) {
		this.employeeCalendarDAO = employeeCalendarDAO;
	}

	public AIOTechGenericDAO<EmployeeCalendarPeriod> getEmployeeCalendarPeriodDAO() {
		return employeeCalendarPeriodDAO;
	}

	public void setEmployeeCalendarPeriodDAO(
			AIOTechGenericDAO<EmployeeCalendarPeriod> employeeCalendarPeriodDAO) {
		this.employeeCalendarPeriodDAO = employeeCalendarPeriodDAO;
	}
}
