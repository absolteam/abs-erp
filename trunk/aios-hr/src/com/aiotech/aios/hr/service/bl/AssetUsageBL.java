package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.aiotech.aios.accounts.domain.entity.AssetCreation;
import com.aiotech.aios.accounts.domain.entity.vo.AssetVO;
import com.aiotech.aios.accounts.service.bl.AssetCreationBL;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.AssetUsage;
import com.aiotech.aios.hr.domain.entity.vo.AssetUsageVO;
import com.aiotech.aios.hr.service.AssetUsageService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class AssetUsageBL {
	private AssetUsageService assetUsageService;
	private CommentBL commentBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private AssetCreationBL assetCreationBL;
	private LookupMasterBL lookupMasterBL;
	private DirectoryBL directoryBL;
	private DocumentBL documentBL;

	public void saveAssetUsage(AssetUsage assetUsage, Long alertId) {
		try {
			boolean editFlag = false;
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (assetUsage != null && assetUsage.getAssetUsageId() != null
					&& assetUsage.getAssetUsageId() > 0) {
				editFlag = true;
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			assetUsage.setIsApprove((byte) WorkflowConstants.Status.Published
					.getCode());
			workflowDetailVo.setRejectAlertId(alertId);
			assetUsageService.saveAssetUsage(assetUsage);
			saveDocuments(assetUsage, editFlag);
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					"AssetUsage", assetUsage.getAssetUsageId(), user,
					workflowDetailVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveDocuments(AssetUsage assetUsage, boolean editFlag)
			throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(assetUsage,
				"assetCustodyDocs", editFlag);
		Map<String, String> dirs = docVO.getDirMap();

		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, assetUsage.getAssetUsageId(),
					AssetUsage.class.getSimpleName(), "assetCustodyDocs");
			docVO.setDirStruct(dirStucture);
		}

		documentBL.saveUploadDocuments(docVO);
	}

	public List<AssetUsageVO> convertEntitiesToVOs(List<AssetUsage> assetUsages)
			throws Exception {

		List<AssetUsageVO> vosList = new ArrayList<AssetUsageVO>();

		for (AssetUsage asstUsage : assetUsages) {
			vosList.add(convertEntityToVO(asstUsage));
		}

		return vosList;
	}

	public void deleteAssetUsage(Long assetUsageId, Long messageId)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				AssetUsage.class.getSimpleName(), assetUsageId, user,
				workflowDetailVo);
	}

	public void doAssetUsageBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		AssetUsage assetUsage = this.getAssetUsageService().getAssetUsageInfo(
				recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			assetUsageService.deleteAssetUsage(assetUsage);
		} else {
		}
	}

	public AssetUsageVO convertEntityToVO(AssetUsage asstUsage)
			throws Exception {
		AssetUsageVO assetUsageVO = new AssetUsageVO();
		BeanUtils.copyProperties(assetUsageVO, asstUsage);
		assetUsageVO.setCmpDeptLocation(asstUsage.getCmpDeptLocation());
		if (asstUsage.getPersonByPersonId() != null)
			assetUsageVO.setPersonByPersonId(asstUsage.getPersonByPersonId());
		if (asstUsage.getProduct() != null)
			assetUsageVO.setProduct(asstUsage.getProduct());
		if (asstUsage.getPersonByCreatedBy() != null)
			assetUsageVO.setPersonByCreatedBy(asstUsage.getPersonByCreatedBy());

		if (asstUsage.getAllocatedDate() != null)
			assetUsageVO.setAllocationDate(DateFormat
					.convertDateToString(asstUsage.getAllocatedDate() + ""));

		if (asstUsage.getReturedDate() != null)
			assetUsageVO.setReturnDate(DateFormat.convertDateToString(asstUsage
					.getReturedDate() + ""));

		if (asstUsage.getPurchaseDate() != null)
			assetUsageVO.setPurchaseDateView(DateFormat
					.convertDateToString(asstUsage.getPurchaseDate() + ""));

		assetUsageVO.setDescription(asstUsage.getDescription());

		assetUsageVO.setAssetUsageId(asstUsage.getAssetUsageId());
		if (asstUsage.getAssetCreation() != null) {
			AssetVO assetVO = new AssetVO();
			try {
				AssetCreation assetCreation = assetCreationBL
						.getAssetCreationService().getAssetDetailInfo(
								asstUsage.getAssetCreation()
										.getAssetCreationId());
				if (assetCreation != null) {
					assetVO = assetCreationBL.addAssetToVO(assetCreation);

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			assetUsageVO.setAssetVO(assetVO);
		}

		if (assetUsageVO.getAssetName() == null
				&& asstUsage.getProduct() != null) {
			assetUsageVO.setAssetName(asstUsage.getProduct().getCode() + " - "
					+ asstUsage.getProduct().getProductName());
		}

		if (asstUsage.getCmpDeptLocation() != null
				&& asstUsage.getCmpDeptLocation().getDepartment() != null) {
			assetUsageVO.setCompanyNDepartmentName(asstUsage
					.getCmpDeptLocation().getDepartment().getDepartmentName()
					+ "-"
					+ asstUsage.getCmpDeptLocation().getLocation()
							.getLocationName());
		} else {
			assetUsageVO.setCompanyNDepartmentName("");
		}

		if (asstUsage.getPersonByPersonId() != null) {
			assetUsageVO.setEmployeeName(asstUsage.getPersonByPersonId()
					.getFirstName()
					+ " "
					+ asstUsage.getPersonByPersonId().getLastName() + "");
		} else {
			assetUsageVO.setEmployeeName("");
		}

		if (asstUsage.getLookupDetailByAssetType() != null) {
			assetUsageVO.setAssetTypeView(asstUsage
					.getLookupDetailByAssetType().getDisplayName());
		}

		if (asstUsage.getLookupDetailByAssetColor() != null) {
			assetUsageVO.setAssetColorView(asstUsage
					.getLookupDetailByAssetColor().getDisplayName());
		}
		return assetUsageVO;
	}

	public AssetUsageService getAssetUsageService() {
		return assetUsageService;
	}

	public void setAssetUsageService(AssetUsageService assetUsageService) {
		this.assetUsageService = assetUsageService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public AssetCreationBL getAssetCreationBL() {
		return assetCreationBL;
	}

	public void setAssetCreationBL(AssetCreationBL assetCreationBL) {
		this.assetCreationBL = assetCreationBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

}
