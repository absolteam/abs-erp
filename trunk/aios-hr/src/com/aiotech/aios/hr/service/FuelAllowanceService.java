package com.aiotech.aios.hr.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.hr.domain.entity.FuelAllowance;
import com.aiotech.aios.hr.domain.entity.MileageLog;
import com.aiotech.aios.hr.domain.entity.vo.FuelAllowanceVO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class FuelAllowanceService {
	public AIOTechGenericDAO<FuelAllowance> fuelAllowanceDAO;
	public AIOTechGenericDAO<MileageLog> mileageLogDAO;

	public List<FuelAllowance> getAllFuelAllowanceByCriteria(
			FuelAllowanceVO fuelAllowanceVO) {
		Criteria jobPayrollCriteria = fuelAllowanceDAO.createCriteria();

		jobPayrollCriteria.createAlias("jobAssignment", "ja");
		jobPayrollCriteria.createAlias("lookupDetailByVehicleNumber", "number");
		jobPayrollCriteria.createAlias("lookupDetailByFuelCompany", "company",
				CriteriaSpecification.LEFT_JOIN);
		jobPayrollCriteria.createAlias("lookupDetailByCardType", "card",
				CriteriaSpecification.LEFT_JOIN);
		jobPayrollCriteria.createAlias("jobPayrollElement", "jpa");
		jobPayrollCriteria.createAlias("jpa.payrollElementByPayrollElementId",
				"pe");
		jobPayrollCriteria.createAlias("jpa.payrollElementByPercentageElement",
				"percentage", CriteriaSpecification.LEFT_JOIN);

		if (fuelAllowanceVO!=null && fuelAllowanceVO.getPayPeriodTransaction() != null
				&& fuelAllowanceVO.getPayPeriodTransaction().getStartDate() != null) {
			jobPayrollCriteria.add(Restrictions.conjunction().add(
					Restrictions.ge("fromDate", fuelAllowanceVO
							.getPayPeriodTransaction().getStartDate())));
			jobPayrollCriteria.add(Restrictions.conjunction().add(
					Restrictions.le("toDate", fuelAllowanceVO
							.getPayPeriodTransaction().getEndDate())));
		}

		if (fuelAllowanceVO!=null && fuelAllowanceVO.getJobAssignmentId() != null) {
			jobPayrollCriteria.add(Restrictions.eq("ja.jobAssignmentId",
					fuelAllowanceVO.getJobAssignmentId()));
		}

		if (fuelAllowanceVO!=null && fuelAllowanceVO.getJobPayrollElementId() != null) {
			jobPayrollCriteria.add(Restrictions.eq("jpa.jobPayrollElementId",
					fuelAllowanceVO.getJobPayrollElementId()));
		}

		return jobPayrollCriteria.list();
	}

	public List<FuelAllowance> getAllFuelAllowance(Implementation implementation) {
		return fuelAllowanceDAO.findByNamedQuery("getAllFuelAllowance",
				implementation);
	}

	public List<MileageLog> getAllMileageLog(Implementation implementation) {
		return mileageLogDAO.findByNamedQuery("getAllMileageLog",
				implementation,
				(byte)WorkflowConstants.Status.Published.getCode());
	}

	public FuelAllowance getFuelAllowanceById(Long fuelAllowanceId) {
		List<FuelAllowance> allowances = fuelAllowanceDAO.findByNamedQuery(
				"getFuelAllowanceById", fuelAllowanceId);
		if (allowances != null && allowances.size() > 0)
			return allowances.get(0);
		else
			return null;
	}

	public MileageLog getMileageLogById(Long logId) {
		List<MileageLog> mileageLogs = mileageLogDAO.findByNamedQuery(
				"getMileageLogById", logId);
		if (mileageLogs != null && mileageLogs.size() > 0)
			return mileageLogs.get(0);
		else
			return null;
	}

	public void saveFuelAllowance(FuelAllowance fuelAllowance) {
		fuelAllowanceDAO.saveOrUpdate(fuelAllowance);
	}

	public void saveMileageLogs(List<MileageLog> logs) {
		mileageLogDAO.saveOrUpdateAll(logs);
	}

	public void saveMileageLog(MileageLog log) {
		mileageLogDAO.saveOrUpdate(log);
	}

	public void deleteFuelAllowance(FuelAllowance allowance) {
		fuelAllowanceDAO.delete(allowance);
	}

	public void deleteMileageLogs(List<MileageLog> logs) {
		mileageLogDAO.deleteAll(logs);
	}

	public void deleteMileageLog(MileageLog log) {
		mileageLogDAO.delete(log);
	}

	public AIOTechGenericDAO<FuelAllowance> getFuelAllowanceDAO() {
		return fuelAllowanceDAO;
	}

	public void setFuelAllowanceDAO(
			AIOTechGenericDAO<FuelAllowance> fuelAllowanceDAO) {
		this.fuelAllowanceDAO = fuelAllowanceDAO;
	}

	public AIOTechGenericDAO<MileageLog> getMileageLogDAO() {
		return mileageLogDAO;
	}

	public void setMileageLogDAO(AIOTechGenericDAO<MileageLog> mileageLogDAO) {
		this.mileageLogDAO = mileageLogDAO;
	}

}
