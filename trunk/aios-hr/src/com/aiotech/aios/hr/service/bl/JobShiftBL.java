package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Job;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobShift;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.WorkingShift;
import com.aiotech.aios.hr.service.JobShiftService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;

public class JobShiftBL {

	private JobBL jobBL;
	private JobShiftService jobShiftService;
	private Implementation implementation;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	private JobAssignmentBL jobAssignmentBL;

	// getAssignedEmployees
	public String getAssignedEmployees(WorkingShift workingShift) {
		String employeeList = "";
		Map<Long, Person> map = new HashMap();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -60);
		java.util.Date cutoffDate = cal.getTime();
		List<JobShift> jobShifts = new ArrayList<JobShift>(
				workingShift.getJobShifts());
		for (JobShift jobShift : workingShift.getJobShifts()) {
			if (!jobShift.getIsActive() || jobShift.getEffectiveStartDate() != null
					&& DateFormat.compareDates(cutoffDate,
							jobShift.getEffectiveStartDate()) == false
					&& DateFormat.compareDates(cutoffDate,
							jobShift.getEffectiveEndDate()) == false)
				jobShifts.remove(jobShift);
		}
		for (JobShift jobShift : jobShifts) {
			if (jobShift.getJob() != null) {
				for (JobAssignment jobAssignment : jobShift.getJob()
						.getJobAssignments()) {
					if (jobAssignment.getPerson() != null
							&& !map.containsKey(jobAssignment.getPerson()
									.getPersonId())) {
						employeeList += jobAssignment.getPerson()
								.getFirstName()
								+ " "
								+ jobAssignment.getPerson().getLastName()
								+ " , ";
						map.put(jobAssignment.getPerson().getPersonId(),
								jobAssignment.getPerson());
					}
				}
			} else if (jobShift.getJobAssignment() != null) {
				JobAssignment jobAssignment = jobShift.getJobAssignment();
				if (jobAssignment.getPerson() != null
						&& !map.containsKey(jobAssignment.getPerson()
								.getPersonId())) {
					employeeList += jobAssignment.getPerson().getFirstName()
							+ " " + jobAssignment.getPerson().getLastName()
							+ " , ";
					map.put(jobAssignment.getPerson().getPersonId(),
							jobAssignment.getPerson());
				}
			}
		}

		return employeeList;
	}

	public List<WorkingShift> getWorkingShiftList(
			List<WorkingShift> tempworkingshifts) {
		List<WorkingShift> workingshifts = new ArrayList<WorkingShift>();
		List<WorkingShift> finalworkingshifts = new ArrayList<WorkingShift>();
		List<WorkingShift> shifts = this.getJobShiftService()
				.getAllActiveShift(getImplementation());
		for (WorkingShift workingShift : tempworkingshifts) {
			if (workingShift.getJobShifts() != null
					&& workingShift.getJobShifts().size() > 0) {
				List<JobShift> jobShiftsTemp = new ArrayList<JobShift>(
						workingShift.getJobShifts());
				for (JobShift jobShift : jobShiftsTemp) {
					if (jobShift.getJob() != null
							&& jobShift.getJob().getJobAssignments() != null
							&& jobShift.getJob().getJobAssignments().size() > 0
							&& jobShift.getJob().getStatus() == 1
							&& jobShift.getIsActive() == true) {
						List<JobAssignment> jobAssignmentsTemp = new ArrayList<JobAssignment>(
								jobShift.getJob().getJobAssignments());
						for (JobAssignment jobAssignment : jobAssignmentsTemp) {
							if (jobAssignment.getIsActive() == true
									&& jobAssignment.getPerson().getStatus() == 1) {
								workingshifts.add(workingShift);
							} else {
								jobShift.getJob().getJobAssignments()
										.remove(jobAssignment);
								workingshifts.add(workingShift);
							}
						}
					} else if (jobShift.getJobAssignment() != null
							&& jobShift.getIsActive() == true) {
						if (jobShift.getJobAssignment().getIsActive() == true
								&& jobShift.getJobAssignment().getPerson()
										.getStatus() == 1) {
							workingshifts.add(workingShift);
						} else {
							workingshifts.add(workingShift);
						}
					} else {
						workingShift.getJobShifts().remove(jobShift);
						workingshifts.add(workingShift);
					}
				}
			} else {
				workingshifts.add(workingShift);
			}
		}
		long workShiftId = 0;
		Map<Long, WorkingShift> maps = new HashMap<Long, WorkingShift>();
		for (WorkingShift workShift : workingshifts) {
			if (!workShift.getWorkingShiftId().equals(workShiftId)) {
				finalworkingshifts.add(workShift);
				maps.put(workShift.getWorkingShiftId(), workShift);

			}
			workShiftId = workShift.getWorkingShiftId();
		}

		for (WorkingShift workingShift2 : shifts) {
			WorkingShift temp = maps.get(workingShift2.getWorkingShiftId());
			if (temp == null
					|| (long) workingShift2.getWorkingShiftId() != (long) temp
							.getWorkingShiftId())
				finalworkingshifts.add(workingShift2);
		}
		return finalworkingshifts;
	}

	public List<JobShift> getShiftLineDetail(String shiftLineDetail) {
		List<JobShift> jobShifts = new ArrayList<JobShift>();
		if (shiftLineDetail != null && !shiftLineDetail.equals("")) {
			String[] recordArray = new String[shiftLineDetail.split("##").length];
			recordArray = shiftLineDetail.split("##");
			for (String record : recordArray) {
				JobShift jobShift = new JobShift();
				WorkingShift workingShift = new WorkingShift();
				String[] dataArray = new String[record.split("@").length];
				dataArray = record.split("@");
				if (!dataArray[0].equals("-1")
						&& !dataArray[0].trim().equals(""))
					jobShift.setJobShiftId(Long.parseLong(dataArray[0]));
				if (!dataArray[1].equals("-1")
						&& !dataArray[1].trim().equals(""))
					workingShift
							.setWorkingShiftId(Long.parseLong(dataArray[1]));
				if (!dataArray[2].equals("-1")
						&& !dataArray[2].trim().equals(""))
					jobShift.setEffectiveStartDate(DateFormat
							.convertStringToDate(dataArray[2]));
				if (!dataArray[3].equals("-1")
						&& !dataArray[3].trim().equals(""))
					jobShift.setEffectiveEndDate(DateFormat
							.convertStringToDate(dataArray[3]));

				if (!dataArray[4].equals("-1")
						&& !dataArray[4].trim().equals("")) {
					Job job = new Job();
					job.setJobId(Long.parseLong(dataArray[4]));
					jobShift.setJob(job);
				}
				if (!dataArray[5].equals("-1")
						&& !dataArray[5].trim().equals("")) {
					JobAssignment jobAssignement = new JobAssignment();
					jobAssignement.setJobAssignmentId(Long
							.parseLong(dataArray[5]));
					jobShift.setJobAssignment(jobAssignement);
				}

				jobShift.setIsActive(true);

				jobShift.setWorkingShift(workingShift);

				jobShifts.add(jobShift);
			}
		}
		return jobShifts;
	}

	public String saveJobShift(List<JobShift> jobShifts,
			WorkingShift workingShift) {
		String returnMessage = "SUCCESS";

		List<JobShift> tempjobShifts = new ArrayList<JobShift>(
				workingShift.getJobShifts());
		List<JobShift> updateJobShifts = getJobShiftSimiler(jobShifts,
				tempjobShifts);
		if (updateJobShifts != null && updateJobShifts.size() > 0)
			jobBL.getJobService().saveJobShiftAll(updateJobShifts);

		jobBL.getJobService().saveJobShiftAll(jobShifts);
		return returnMessage;
	}

	public List<JobShift> getJobShiftSimiler(List<JobShift> jobShifts,
			List<JobShift> jobShiftEdits) {
		List<JobShift> jobShiftUpdates = new ArrayList<JobShift>();

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		for (JobShift jobShift : jobShifts) {
			if (jobShift.getJobShiftId() != null
					&& jobShift.getJobShiftId() > 0)
				listOne.add(jobShift.getJobShiftId());
		}
		for (JobShift jobShift : jobShiftEdits) {
			if (jobShift.getJobShiftId() != null
					&& jobShift.getJobShiftId() > 0)
				listTwo.add(jobShift.getJobShiftId());
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);

		// Delete Process
		for (Long lng : different) {
			for (JobShift jobShift : jobShiftEdits) {
				if (lng != null && jobShift.getJobShiftId().equals(lng)) {
					jobShift.setIsActive(false);
					jobShiftUpdates.add(jobShift);
				}
			}
		}
		return jobShiftUpdates;
	}

	public JobBL getJobBL() {
		return jobBL;
	}

	public void setJobBL(JobBL jobBL) {
		this.jobBL = jobBL;
	}

	public JobShiftService getJobShiftService() {
		return jobShiftService;
	}

	public void setJobShiftService(JobShiftService jobShiftService) {
		this.jobShiftService = jobShiftService;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public JobAssignmentBL getJobAssignmentBL() {
		return jobAssignmentBL;
	}

	public void setJobAssignmentBL(JobAssignmentBL jobAssignmentBL) {
		this.jobAssignmentBL = jobAssignmentBL;
	}

}
