package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Department;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.MemoWarning;
import com.aiotech.aios.hr.domain.entity.MemoWarningPerson;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.MemoWarningVO;
import com.aiotech.aios.hr.service.MemoWarningService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.EmailVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class MemoWarningBL {
	private MemoWarningService memoWarningService;
	private LookupMasterBL lookupMasterBL;
	private CompanyBL companyBL;
	private PersonBL personBL;
	private DepartmentBL departmentBL;
	private SystemService systemService;
	private CommentBL commentBL;
	private Implementation implementation;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private SystemBL systemBL;

	public String getRefereceNum() {
		String referenceStamp = SystemBL.getReferenceStamp(
				MemoWarning.class.getName(), getImplementation());
		return referenceStamp;
	}

	public String saveRefereceNum() {
		String referenceStamp = SystemBL.saveReferenceStamp(
				MemoWarning.class.getName(), getImplementation());
		return referenceStamp;
	}

	public void saveMemoWarning(MemoWarning memoWarning,
			List<MemoWarningPerson> memoWarningPersons, Long alertId) {
		try {

			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			workflowDetailVo.setRejectAlertId(alertId);
			if (memoWarning != null && memoWarning.getMemoWarningId() != null
					&& memoWarning.getMemoWarningId() > 0) {

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			if (memoWarning.getMemoWarningId() != null
					&& memoWarning.getMemoWarningId() > 0) {
				memoWarningService
						.deleteMemoWarningPersons(new ArrayList<MemoWarningPerson>(
								memoWarning.getMemoWarningPersons()));
			} else {
				memoWarning.setReferenceNumber(saveRefereceNum());
			}
			memoWarningService.saveMemoWarning(memoWarning);
			for (MemoWarningPerson memoWarningPerson : memoWarning
					.getMemoWarningPersons()) {
				memoWarningPerson.setMemoWarning(memoWarning);
			}
			memoWarningService.saveMemoWarningPersons(memoWarningPersons);

			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					"MemoWarning", memoWarning.getMemoWarningId(), user,
					workflowDetailVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<MemoWarningVO> convertEntitiesTOVOs(
			List<MemoWarning> memoWarnings) throws Exception {

		List<MemoWarningVO> tosList = new ArrayList<MemoWarningVO>();

		for (MemoWarning pers : memoWarnings) {
			tosList.add(convertEntityToVO(pers));
		}

		return tosList;
	}

	public MemoWarningVO convertEntityToVO(MemoWarning memoWarning) {
		MemoWarningVO memoWarningVO = new MemoWarningVO();
		List<MemoWarningVO> memoWarningPersonVOs = new ArrayList<MemoWarningVO>();
		List<MemoWarningVO> memoWarningPersonCCVOs = new ArrayList<MemoWarningVO>();
		memoWarningVO.setMemoWarningId(memoWarning.getMemoWarningId());
		memoWarningVO.setMemoWarning(memoWarning);
		memoWarningVO.setCreatedDateStr(DateFormat
				.convertDateToString(memoWarning.getCreatedDate() + ""));
		if (memoWarning.getType() == 1)
			memoWarningVO.setMemoType("MEMO");
		else
			memoWarningVO.setMemoType("WARNING");

		memoWarningVO.setProcessType(memoWarning.getProcessType());
		memoWarningVO.setProcessTypeName(processTypeNameFetch(memoWarning
				.getProcessType()));
		memoWarningVO.setMemoSubType(memoWarning.getLookupDetail()
				.getDisplayName());
		memoWarningVO.setTitle(memoWarning.getTitle());
		memoWarningVO.setSubject(memoWarning.getSubject());
		memoWarningVO.setMessage(memoWarning.getMessage());
		memoWarningVO.setDepartment(memoWarning.getDepartment());
		memoWarningVO.setCreatedDate(memoWarning.getCreatedDate());
		memoWarningVO.setStatus(memoWarning.getStatus());
		memoWarningVO.setIsApprove(memoWarning.getIsApprove());
		memoWarningVO.setNotes(memoWarning.getNotes());
		memoWarningVO.setReferenceNumber(memoWarning.getReferenceNumber());
		memoWarningVO.setPerson(memoWarning.getPerson());

		String employeeIds = "";
		String personIds = "";
		Map<Long, Department> departmentMap = new HashMap<Long, Department>();

		if (memoWarning.getMemoWarningPersons() != null
				&& memoWarning.getMemoWarningPersons().size() > 0) {
			for (MemoWarningPerson memoWarningPerson : memoWarning
					.getMemoWarningPersons()) {
				MemoWarningVO memoWarningPersonVO = new MemoWarningVO();
				memoWarningPersonVO
						.setJobAssignment(getJobAssignmentFromPerson(memoWarningPerson
								.getPerson()));
				memoWarningPersonVO.setMemoWarningPerson(memoWarningPerson);
				memoWarningPersonVO.setEmployeeName(memoWarningPersonVO
						.getMemoWarningPerson().getPerson().getFirstName()
						+ " "
						+ memoWarningPersonVO.getMemoWarningPerson()
								.getPerson().getLastName());
				memoWarningPersonVO.setEmployeeCode(memoWarningPersonVO
						.getMemoWarningPerson().getPerson().getPersonNumber());
				// Skip Job Information if Person is not an employee
				if (memoWarningPersonVO.getJobAssignment() != null) {
					departmentMap.put(memoWarningPersonVO.getJobAssignment()
							.getCmpDeptLocation().getDepartment()
							.getDepartmentId(), memoWarningPersonVO
							.getJobAssignment().getCmpDeptLocation()
							.getDepartment());
					memoWarningPersonVO
							.setEmployeeDesignation(memoWarningPersonVO
									.getJobAssignment().getDesignation()
									.getDesignationName());
					memoWarningPersonVO.setEmployeeCompany(memoWarningPersonVO
							.getJobAssignment().getCmpDeptLocation()
							.getCompany().getCompanyName());
					memoWarningPersonVO
							.setEmployeeDepartment(memoWarningPersonVO
									.getJobAssignment().getCmpDeptLocation()
									.getDepartment().getDepartmentName());
					memoWarningPersonVO.setEmployeeLocation(memoWarningPersonVO
							.getJobAssignment().getCmpDeptLocation()
							.getLocation().getLocationName());
				}
				if (memoWarningPerson.getIsCcFlag() != null
						&& memoWarningPerson.getIsCcFlag() == true) {
					personIds = personIds
							+ memoWarningPerson.getPerson().getPersonId() + ",";
					memoWarningPersonCCVOs.add(memoWarningPersonVO);
				} else {
					employeeIds = employeeIds
							+ memoWarningPerson.getPerson().getPersonId() + ",";
					memoWarningPersonVOs.add(memoWarningPersonVO);

				}
			}
		}

		memoWarningVO.setCcPersonIds(personIds);
		memoWarningVO.setMemoPersonIds(employeeIds);

		// Company & Department list split up work
		String[] companyDept = splitValues(memoWarning.getDepartment(), "&#");
		if (companyDept.length > 1)
			memoWarning.setDepartment(companyDept[1]);

		memoWarningVO.setCompanyId(companyDept[0]);

		// Employee && CC Peson List Adding into VOs
		memoWarningVO.setMemoWarningPersonCCVOs(memoWarningPersonCCVOs);
		memoWarningVO.setMemoWarningPersonVOs(memoWarningPersonVOs);

		// Memo or Warning Sample Template 'Mail TO' Option renduring.
		if (memoWarningPersonVOs != null && memoWarningPersonVOs.size() > 0) {
			memoWarningVO.setEmployeeName(memoWarningPersonVOs.get(0)
					.getEmployeeName());
			memoWarningVO.setEmployeeCode(memoWarningPersonVOs.get(0)
					.getEmployeeCode());
			memoWarningVO.setEmployeeDesignation(memoWarningPersonVOs.get(0)
					.getEmployeeDesignation());
			memoWarningVO.setEmployeeCompany(memoWarningPersonVOs.get(0)
					.getEmployeeCompany());
			memoWarningVO.setEmployeeDepartment(memoWarningPersonVOs.get(0)
					.getEmployeeDepartment());
			memoWarningVO.setEmployeeLocation(memoWarningPersonVOs.get(0)
					.getEmployeeLocation());
			memoWarningVO.setEmployeeCode(memoWarningPersonVOs.get(0)
					.getMemoWarningPerson().getPerson().getPersonNumber());
		}
		String toDetail = "";
		String dearDetail = "";
		memoWarningVO.setDepartmentNames("");
		List<Department> departmentsName = new ArrayList<Department>(
				departmentMap.values());
		for (Department department : departmentsName) {
			memoWarningVO.setDepartmentNames(department.getDepartmentName()
					+ "," + memoWarningVO.getDepartmentNames());
		}
		if (memoWarning.getProcessType() == 1) {// Company Based Processing
			toDetail = "<b> The " + memoWarningVO.getEmployeeCompany()
					+ "</b><br>";
			dearDetail = "Dear All,<br>";
		} else if (memoWarning.getProcessType() == 2) {// Department Based
														// Processing
			toDetail = "<b> The " + memoWarningVO.getDepartmentNames()
					+ ",<br>" + memoWarningVO.getEmployeeCompany() + "</b><br>";
			dearDetail = "Dear All,<br>";
		} else if (memoWarning.getProcessType() == 3) {// Person Based
														// Processing
			toDetail = "<b> " + memoWarningVO.getEmployeeName() + ",<br>"
					+ memoWarningVO.getEmployeeDesignation() + ",<br> Code : "
					+ memoWarningVO.getEmployeeCode() + "</b>";
			dearDetail = "Mr/Ms., " + memoWarningVO.getEmployeeName();
		}

		memoWarningVO.setToDetails(toDetail);
		memoWarningVO.setDearDetails(dearDetail);
		memoWarningVO.setMessageHtml(getEmailTemplate(memoWarningVO));
		return memoWarningVO;
	}

	public JobAssignment getJobAssignmentFromPerson(Person person) {
		List<JobAssignment> jobAssignments = null;
		JobAssignment jobAssignment = null;
		if (person.getJobAssignments() != null) {
			jobAssignments = new ArrayList<JobAssignment>(
					person.getJobAssignments());
			jobAssignment = new JobAssignment();
			for (JobAssignment jobAssign : jobAssignments) {
				if (jobAssign.getIsActive() != null && jobAssign.getIsActive())
					;
				jobAssignment = jobAssign;
			}
		}

		return jobAssignment;
	}

	public void deleteMemoWarning(Long memoWarningId, Long messageId)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				MemoWarning.class.getSimpleName(), memoWarningId, user,
				workflowDetailVo);
	}

	public void doMemoWarningBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		MemoWarning memoWarning = memoWarningService
				.getMemoWarningInfo(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			memoWarningService
					.deleteMemoWarningPersons(new ArrayList<MemoWarningPerson>(
							memoWarning.getMemoWarningPersons()));
			memoWarningService.deleteMemoWarning(memoWarning);
		} else {
		}
	}

	public void publishMemoWarning(Long memoWarningId) throws Exception {
		MemoWarning memoWarning = memoWarningService
				.getMemoWarningInfo(memoWarningId);
		MemoWarningVO memoWarningVO = convertEntityToVO(memoWarning);

		List<MemoWarningPerson> memoCCPersons = new ArrayList<MemoWarningPerson>();
		List<MemoWarningPerson> memoPersons = new ArrayList<MemoWarningPerson>();
		List<String> tolist = new ArrayList<String>();
		List<String> cclist = new ArrayList<String>();
		for (MemoWarningPerson memoWarningPerson : memoWarning
				.getMemoWarningPersons()) {
			if (memoWarningPerson.getIsCcFlag() != null
					&& memoWarningPerson.getIsCcFlag() == true) {
				memoCCPersons.add(memoWarningPerson);
				if (memoWarningPerson.getPerson().getEmail() != null)
					cclist.add(memoWarningPerson.getPerson().getEmail());
			} else {
				memoPersons.add(memoWarningPerson);
				if (memoWarningPerson.getPerson().getEmail() != null)
					tolist.add(memoWarningPerson.getPerson().getEmail());
			}
		}
		EmailVO emailVO = null;
		for (String to : tolist) {
			emailVO = new EmailVO();
			emailVO.setMessage(getEmailTemplate(memoWarningVO));
			emailVO.setSubject(memoWarningVO.getSubject());
			emailVO.setTo(to);
			if (cclist != null && cclist.size() > 0) {
				emailVO.setCc(cclist.get(0));
			}

			systemBL.sendEmail(emailVO);
		}
	}

	public String getPersonEmails(List<MemoWarningPerson> memoCCPersons) {
		String emails = "";
		for (MemoWarningPerson meoWarningPerson : memoCCPersons) {
			emails += meoWarningPerson.getPerson().getEmail() + ",";
		}

		return emails;
	}

	public String processTypeNameFetch(Byte type) {
		String typeName = "";
		if (type != null && type == 1) {
			typeName = "Company Specific";
		} else if (type != null && type == 2) {
			typeName = "Department Specific";
		} else if (type != null && type == 3) {
			typeName = "Employee Specific";
		}

		return typeName;
	}

	public String getEmailTemplate(MemoWarningVO memoWarningVO) {
		String emailTemplate = "<table align=center width=98% border=1 cellspacing=0 cellpadding=3 style='border:solid 1px #369;'>"
				+ "<tr style=background:#66A3FF;><td style='font-weight: bold;font-size: 25px;text-align: center;'>"
				+ "<span>"
				+ memoWarningVO.getMemoType()
				+ "-"
				+ memoWarningVO.getTitle()
				+ "</td><td></td>"
				+ "</tr><tr style='background:#EFF5FB;'><td style='font-weight: bold;font-size: 14px;'>"
				+ "Date  : "
				+ memoWarningVO.getCreatedDateStr()
				+ "</td><td style='font-weight: bold;font-size: 14px;'>"
				+ "Reference Number : "
				+ memoWarningVO.getReferenceNumber()
				+ "</td></tr><tr><td><br>"
				+ "<span style='color:#green;font-weight: bold;font-size: 14px;'>To,</span><br><br>"
				+ memoWarningVO.getToDetails()
				+ "<br><br><span style='color:#green;font-weight: bold;font-size: 14px;'>Subject: </span>"
				+ memoWarningVO.getSubject()
				+ "<br><br><br>"
				+ memoWarningVO.getDearDetails()
				+ "<br><br><br>"
				+ memoWarningVO.getMessage() + "<br></td></tr></table>";

		return emailTemplate;
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public MemoWarningService getMemoWarningService() {
		return memoWarningService;
	}

	public void setMemoWarningService(MemoWarningService memoWarningService) {
		this.memoWarningService = memoWarningService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public CompanyBL getCompanyBL() {
		return companyBL;
	}

	public void setCompanyBL(CompanyBL companyBL) {
		this.companyBL = companyBL;
	}

	public PersonBL getPersonBL() {
		return personBL;
	}

	public void setPersonBL(PersonBL personBL) {
		this.personBL = personBL;
	}

	public DepartmentBL getDepartmentBL() {
		return departmentBL;
	}

	public void setDepartmentBL(DepartmentBL departmentBL) {
		this.departmentBL = departmentBL;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public SystemBL getSystemBL() {
		return systemBL;
	}

	public void setSystemBL(SystemBL systemBL) {
		this.systemBL = systemBL;
	}
}
