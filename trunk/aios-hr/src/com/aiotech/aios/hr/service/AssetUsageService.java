package com.aiotech.aios.hr.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.hr.domain.entity.AssetUsage;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AssetUsageService {

	public AIOTechGenericDAO<AssetUsage> assetUsageDAO;

	public List<AssetUsage> getAssetUsageList(Implementation implementation) {
		return assetUsageDAO.findByNamedQuery("getAssetUsageList",
				implementation);
	}

	public AssetUsage getAssetUsageInfo(Long assetUsageId) {
		List<AssetUsage> assetUsages = assetUsageDAO.findByNamedQuery(
				"getAssetUsageInfo", assetUsageId);
		if (assetUsages != null && assetUsages.size() > 0)
			return assetUsages.get(0);
		else
			return null;
	}

	public void saveAssetUsage(AssetUsage assetUsage) {
		assetUsageDAO.saveOrUpdate(assetUsage);
	}

	public void deleteAssetUsage(AssetUsage assetUsage) {
		assetUsageDAO.delete(assetUsage);
	}

	public AIOTechGenericDAO<AssetUsage> getAssetUsageDAO() {
		return assetUsageDAO;
	}

	public void setAssetUsageDAO(AIOTechGenericDAO<AssetUsage> assetUsageDAO) {
		this.assetUsageDAO = assetUsageDAO;
	}

	public List<AssetUsage> getAssetUssageByCriteria(Long personId,
			Long productId, Long locationId, Long usageId, Date fromDate,
			Date toDate, Implementation implementation) throws Exception {

		Criteria assetUsageCriteria = assetUsageDAO.createCriteria();

		if (fromDate != null && toDate != null) {
			assetUsageCriteria.add(Restrictions.between("allocatedDate", fromDate, toDate));
		}

		if (implementation != null) {
			assetUsageCriteria.add(Restrictions.eq("implementation",
					implementation));
		}

		if (personId != null) {
			assetUsageCriteria.createAlias("personByPersonId", "person",
					CriteriaSpecification.INNER_JOIN).add(
					Restrictions.eq("personByPersonId.personId", personId));
		} else {
			assetUsageCriteria
					.setFetchMode("personByPersonId", FetchMode.EAGER);
		}

		if (locationId != null) {
			assetUsageCriteria
					.createAlias("cmpDeptLocation", "cmp",
							CriteriaSpecification.INNER_JOIN)
					.add(Restrictions.eq("cmp.cmpDeptLocId",
							locationId));
		} else {
			assetUsageCriteria.setFetchMode("cmpDeptLocation", FetchMode.EAGER)
					.setFetchMode("cmpDeptLocation.location", FetchMode.EAGER);
		}

		if (productId != null) {
			assetUsageCriteria.setFetchMode("product", FetchMode.EAGER).add(
					Restrictions.eq("product.productId", productId));
		} else {
			assetUsageCriteria.setFetchMode("product", FetchMode.EAGER);
		}

		assetUsageCriteria.setFetchMode("cmpDeptLocation.location",
				FetchMode.EAGER);
		assetUsageCriteria.setFetchMode("cmpDeptLocation.company",
				FetchMode.EAGER);
		assetUsageCriteria.setFetchMode("cmpDeptLocation.department",
				FetchMode.EAGER);

		assetUsageCriteria
				.setFetchMode("product.lookupDetail", FetchMode.EAGER);

		return assetUsageCriteria.list();
	}
}
