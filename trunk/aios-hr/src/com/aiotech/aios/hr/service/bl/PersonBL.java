package com.aiotech.aios.hr.service.bl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.ShippingDetail;
import com.aiotech.aios.accounts.domain.entity.vo.CustomerVO;
import com.aiotech.aios.accounts.service.GLCombinationService;
import com.aiotech.aios.accounts.service.bl.CombinationBL;
import com.aiotech.aios.accounts.service.bl.CommissionRuleBL;
import com.aiotech.aios.accounts.service.bl.CustomerBL;
import com.aiotech.aios.common.to.FileVO;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.AcademicQualifications;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Dependent;
import com.aiotech.aios.hr.domain.entity.Experiences;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.PersonTypeAllocation;
import com.aiotech.aios.hr.domain.entity.Skills;
import com.aiotech.aios.hr.domain.entity.vo.PersonVO;
import com.aiotech.aios.hr.service.CompanyService;
import com.aiotech.aios.hr.service.PersonService;
import com.aiotech.aios.hr.to.HrPersonalDetailsTO;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.domain.entity.Country;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Document;
import com.aiotech.aios.system.domain.entity.Image;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.service.DocumentService;
import com.aiotech.aios.system.service.ImageService;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class PersonBL {
	private PersonService personService;
	private DocumentService documentService;
	private ImageService imageService;
	private LookupMasterBL lookupMasterBL;
	DirectoryBL directoryBL;
	DocumentBL documentBL;
	ImageBL imageBL;
	private SupplierBL supplierBL;
	private CustomerBL customerBL;
	private CandidateBL candidateBL;
	private GLCombinationService combinationService;
	private CombinationBL combinationBL;
	private Implementation implementation;
	private SystemService systemService;
	private AlertBL alertBL;
	private CommissionRuleBL commissionRuleBL;
	private CompanyService companyService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public List<Person> getPersonList(String personTyeps) throws Exception {
		List<Person> persons = new ArrayList<Person>();
		persons = personService.getAllPerson(getImplementation());
		if (personTyeps != null && !personTyeps.equals("")
				&& !personTyeps.equals("ALL"))
			persons = personTypeFilteration(personTyeps, persons);
		return persons;
	}

	public Object getPersonDetails(long personId) throws Exception {
		Person person = personService.getByPersonId(personId);
		PersonVO personVO = addPersonVO(person);
		return personVO;
	}

	private PersonVO addPersonVO(Person person) throws Exception {
		PersonVO personVO = new PersonVO();
		Account account = combinationService.getAccountDetail(
				person.getFirstName() + " " + person.getLastName() + "-"
						+ person.getPersonNumber() + "-" + "EMPLOYEE",
				getImplementation());
		Combination combination = combinationService
				.getCombinationByAccount(account.getAccountId());
		personVO.setCombinationId(combination.getCombinationId());
		personVO.setAccountCode(account.getAccount().concat(
				"[" + account.getCode() + "]"));
		return personVO;
	}

	public void savePersonDocuments(Person person, boolean editFlag)
			throws Exception {

		DocumentVO docVO = documentBL.populateDocumentVO(person, "personDocs",
				editFlag);
		Map<String, String> dirs = docVO.getDirMap();

		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, person.getPersonId(), "Person", "personDocs");
			docVO.setDirStruct(dirStucture);
		}

		documentBL.saveUploadDocuments(docVO);
	}

	public void savePersonImages(Person person, boolean editFlag)
			throws Exception {
		DocumentVO docVO2 = new DocumentVO();
		docVO2.setDisplayPane("personImages");
		docVO2.setEdit(editFlag);
		docVO2.setObject(person);
		imageBL.saveUploadedImages(person, docVO2);
	}

	private Image populateImage(Map.Entry<String, FileVO> entry, Object object)
			throws Exception {
		Image image = new Image();
		image.setName(entry.getKey());
		image.setHashedName(entry.getValue().getHashedName());
		String[] name = object.getClass().getName().split("entity.");
		image.setTableName(name[name.length - 1]);

		String methodName = "get" + name[name.length - 1] + "Id";
		Method method = object.getClass().getMethod(methodName, null);
		image.setRecordId((Long) method.invoke(object, null));

		return image;
	}

	private Document populateDocument(Document document,
			Map.Entry<String, FileVO> entry, Object object) throws Exception {
		document.setName(entry.getKey());
		document.setHashedName(entry.getValue().getHashedName());
		String[] name = object.getClass().getName().split("entity.");
		document.setTableName(name[name.length - 1]);
		String methodName = "get" + name[name.length - 1] + "Id";
		Method method = object.getClass().getMethod(methodName, null);
		document.setRecordId((Long) method.invoke(object, null));
		return document;
	}

	public List<PersonVO> getAllAcadamicQualifications(Person person)
			throws Exception {
		List<PersonVO> personVOs = null;
		if (person != null && person.getPersonId() != null) {
			List<AcademicQualifications> aqs = personService
					.getAllAcademicQualifications(person);
			if (aqs != null && aqs.size() > 0) {
				personVOs = new ArrayList<PersonVO>();
				for (AcademicQualifications academicQualifications : aqs) {
					PersonVO personVO = new PersonVO();
					personVO.setAcademicQualifications(academicQualifications);
					personVO.setDegree(academicQualifications.getLookupDetail()
							.getDisplayName());
					personVO.setDegreeId(academicQualifications
							.getAcademicQualificationsId());
					personVO.setMajorSubject(academicQualifications
							.getMajorSubject());
					personVO.setResult(academicQualifications.getResult());
					personVO.setResultType(academicQualifications
							.getResultType());
					personVO.setCompletionYearDisplay(DateFormat
							.convertDateToString(academicQualifications
									.getCompletionYear() + ""));

					personVOs.add(personVO);
				}
			}
		}
		return personVOs;
	}

	public List<PersonVO> getAllExperiences(Person person) throws Exception {
		List<PersonVO> personVOs = null;
		if (person != null && person.getPersonId() != null) {
			List<Experiences> exps = personService.getAllExperiences(person);
			if (exps != null && exps.size() > 0) {
				personVOs = new ArrayList<PersonVO>();
				for (Experiences experience : exps) {
					PersonVO personVO = new PersonVO();
					personVO.setExperiences(experience);
					personVO.setExperiencesId(experience.getExperiencesId());
					personVO.setPreviousExperience(experience
							.getPreviousExperience());
					personVO.setCurrentPost(experience.getCurrentPost());
					personVO.setJobTitle(experience.getJobTitle());
					personVO.setCompanyName(experience.getCompanyName());
					personVO.setStartDateDisplay(DateFormat
							.convertDateToString(experience.getStartDate() + ""));
					personVO.setEndDateDisplay(DateFormat
							.convertDateToString(experience.getEndDate() + ""));
					personVOs.add(personVO);
				}
			}
		}
		return personVOs;
	}

	public List<PersonVO> getAllSkills(Person person) throws Exception {
		List<PersonVO> personVOs = null;
		if (person != null && person.getPersonId() != null) {
			List<Skills> skills = personService.getAllSkills(person);
			if (skills != null && skills.size() > 0) {
				personVOs = new ArrayList<PersonVO>();
				for (Skills skill : skills) {
					PersonVO personVO = new PersonVO();
					personVO.setSkills(skill);
					personVO.setSkillId(skill.getSkillsId());
					personVO.setSkillType(skill.getLookupDetail()
							.getDisplayName());
					personVO.setSkillLevel(skill.getSkillLevel());
					personVO.setCreatedDateDisplay(DateFormat
							.convertDateToString(skill.getCreatedDate() + ""));
					personVOs.add(personVO);
				}
			}
		}
		return personVOs;
	}

	public void udpateCustomerPerson(Person person) throws Exception {
		personService.saveCustomerPerson(person);
	}

	public List<String> fileReader(String fileName) {
		File file = new File(fileName);
		List<String> files = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			boolean exists = file.exists();
			if (exists) {
				for (String line1 = br.readLine(); line1 != null; line1 = br
						.readLine()) {
					files.add(line1);
				}
			}
			return files;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Map<Byte, List<CustomerVO>> addPersonCustomer(
			List<String> customerList) {
		Map<Byte, List<CustomerVO>> customersMap = null;
		try {
			customersMap = new HashMap<Byte, List<CustomerVO>>();
			List<CustomerVO> customerVOs = null;
			CustomerVO customer = null;
			byte key = 0;
			for (String string : customerList) {
				customer = new CustomerVO();
				String[] customerstr = splitValues(string, ",");
				key = (byte) (customerstr[1].equalsIgnoreCase("YES") ? 0 : 1);
				customerVOs = new ArrayList<CustomerVO>();
				if (null != customersMap && customersMap.containsKey(key))
					customerVOs.addAll(customersMap.get(key));
				customer.setCustomerName(customerstr[0]);
				customer.setMobileNumber(customerstr[2]);
				customer.setCombinationId(getImplementation()
						.getAccountsReceivable());
				customerVOs.add(customer);
				customersMap.put(key, customerVOs);
			}
		} catch (Exception e) {

		}
		return customersMap;
	}

	public Customer saveCustomerPerson(Person person, Customer customer,
			ShippingDetail shippingDetail) throws Exception {
		getImplementation();
		personService.saveCustomerPerson(person);
		customer.setPersonByPersonId(person);
		Combination customerCombination = getCombinationInfo(
				getImplementation().getAccountsReceivable(), person
						.getFirstName().concat(" " + person.getLastName())
						+ "-" + person.getPersonNumber() + "-CUSTOMER", null);
		customer.setCombination(customerCombination);
		customerBL.savePOSCustomer(customer, shippingDetail);
		return customer;
	}

	public void saveCustomerPerson(List<Person> persons,
			List<Customer> customers, List<ShippingDetail> shippingDetails)
			throws Exception {
		personService.savePersons(persons);
		Combination customerCombination = null;
		List<String> conflicts = new ArrayList<String>();
		long combinationId = 0;
		for (Customer customer : customers) {
			combinationId = customer.getCombination().getCombinationId();
			customerCombination = getCombinationInfo(
					customer.getCombination().getCombinationId(),
					customer.getPersonByPersonId()
							.getFirstName()
							.concat(null != customer.getPersonByPersonId()
									.getLastName() ? " "
									+ customer.getPersonByPersonId()
											.getLastName() : "")
							.concat("-CUSTOMER"), null);
			if ((long) customerCombination.getCombinationId() == combinationId) {
				conflicts.add(customer.getPersonByPersonId().getFirstName());
			}
			customer.setCombination(customerCombination);
		}
		customerBL.saveCustomers(customers, shippingDetails);
	}

	public void saveCustomerCompany(List<Company> companys,
			List<Customer> customers, List<ShippingDetail> shippingDetails)
			throws Exception {
		companyService.saveCompany(companys);
		Combination customerCombination = null;
		List<String> conflicts = new ArrayList<String>();
		long combinationId = 0;
		for (Customer customer : customers) {
			combinationId = customer.getCombination().getCombinationId();
			customerCombination = getCombinationInfo(customer.getCombination()
					.getCombinationId(), customer.getCompany().getCompanyName()
					.concat("-CUSTOMER"), null);
			if ((long) customerCombination.getCombinationId() == combinationId) {
				conflicts.add(customer.getCompany().getCompanyName());
			}
			customer.setCombination(customerCombination);
		}
		customerBL.saveCustomers(customers, shippingDetails);
	}

	public void savePerson(Person person, List<Identity> identitys,
			List<Dependent> dependents, PersonVO personVO,
			List<AcademicQualifications> qualifications,
			List<Experiences> experiences, List<Skills> skills, User user1)
			throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		person.setIsApprove((byte) WorkflowConstants.Status.Published.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (person != null && person.getPersonId() != null
				&& person.getPersonId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		Combination employeeCombination = null;
		Combination tenantCombination = null;
		Combination customerCombination = null;
		Combination supplierCombination = null;
		Combination ownerCombination = null;

		if (null != user1 && null != user1.getUsername())
			workflowEnterpriseService.getUserActionBL().getUserRoleService()
					.saveOrUpdateUser(user1);
		personService.savePerson(person, identitys, dependents, qualifications,
				experiences, skills);
		if (personVO.getSupplier() != null)
			personVO.getSupplier().setPerson(person);
		if (personVO.getCustomer() != null)
			personVO.getCustomer().setPersonByPersonId(person);

		if (personVO.getPersonId() != null && personVO.getPersonId() > 0
				&& personVO.getPersonEdit() != null) {
			String currentName = person.getFirstName() + " "
					+ person.getLastName();
			String oldName = personVO.getPersonEdit().getFirstName() + " "
					+ personVO.getPersonEdit().getLastName();
			if (!currentName.equalsIgnoreCase(oldName)) {
				employeeCombination = getCombinationInfo(
						personVO.getEmployeeCombinationId(),
						personVO.getPersonName() + "-"
								+ personVO.getPersonEdit().getPersonNumber()
								+ "-EMPLOYEE", oldName + "-"
								+ personVO.getPersonEdit().getPersonNumber()
								+ "-EMPLOYEE");

				tenantCombination = getCombinationInfo(
						personVO.getTenantCombinationId(),
						personVO.getPersonName() + "-"
								+ personVO.getPersonEdit().getPersonNumber()
								+ "-TENANT", oldName + "-"
								+ personVO.getPersonEdit().getPersonNumber()
								+ "-TENANT");

				customerCombination = getCombinationInfo(
						personVO.getCustomerCombinationId(),
						personVO.getPersonName() + "-"
								+ personVO.getPersonEdit().getPersonNumber()
								+ "-CUSTOMER", oldName + "-"
								+ personVO.getPersonEdit().getPersonNumber()
								+ "-CUSTOMER");

				supplierCombination = getCombinationInfo(
						personVO.getSupplierCombinationId(),
						personVO.getPersonName() + "-"
								+ personVO.getPersonEdit().getPersonNumber()
								+ "-SUPPLIER", oldName + "-"
								+ personVO.getPersonEdit().getPersonNumber()
								+ "-SUPPLIER");

				ownerCombination = getCombinationInfo(
						personVO.getOwnerCombinationId(),
						personVO.getPersonName() + "-"
								+ personVO.getPersonEdit().getPersonNumber()
								+ "-OWNER", oldName + "-"
								+ personVO.getPersonEdit().getPersonNumber()
								+ "-OWNER");
			} else {
				employeeCombination = getCombinationInfo(
						personVO.getEmployeeCombinationId(),
						personVO.getPersonName() + "-"
								+ personVO.getPersonEdit().getPersonNumber()
								+ "-EMPLOYEE", null);

				tenantCombination = getCombinationInfo(
						personVO.getTenantCombinationId(),
						personVO.getPersonName() + "-"
								+ personVO.getPersonEdit().getPersonNumber()
								+ "-TENANT", null);

				customerCombination = getCombinationInfo(
						personVO.getCustomerCombinationId(),
						personVO.getPersonName() + "-"
								+ personVO.getPersonEdit().getPersonNumber()
								+ "-CUSTOMER", null);

				supplierCombination = getCombinationInfo(
						personVO.getSupplierCombinationId(),
						personVO.getPersonName() + "-"
								+ personVO.getPersonEdit().getPersonNumber()
								+ "-SUPPLIER", null);

				ownerCombination = getCombinationInfo(
						personVO.getOwnerCombinationId(),
						personVO.getPersonName() + "-"
								+ personVO.getPersonEdit().getPersonNumber()
								+ "-OWNER", null);
			}
		} else {

			employeeCombination = getCombinationInfo(
					personVO.getEmployeeCombinationId(),
					personVO.getPersonName() + "-" + personVO.getPersonNumber()
							+ "-EMPLOYEE", null);

			tenantCombination = getCombinationInfo(
					personVO.getTenantCombinationId(), personVO.getPersonName()
							+ "-" + personVO.getPersonNumber() + "-TENANT",
					null);

			customerCombination = getCombinationInfo(
					personVO.getCustomerCombinationId(),
					personVO.getPersonName() + "-" + personVO.getPersonNumber()
							+ "-CUSTOMER", null);

			supplierCombination = getCombinationInfo(
					personVO.getSupplierCombinationId(),
					personVO.getPersonName() + "-" + personVO.getPersonNumber()
							+ "-SUPPLIER", null);

			ownerCombination = getCombinationInfo(
					personVO.getOwnerCombinationId(), personVO.getPersonName()
							+ "-" + personVO.getPersonNumber() + "-OWNER", null);
		}

		// PersonType Allocation Call
		List<LookupDetail> personTypeList = lookupMasterBL
				.getActiveLookupDetails("PERSON_TYPE", true);
		Map<Integer, LookupDetail> map = new HashMap<Integer, LookupDetail>();
		for (LookupDetail el : personTypeList) {
			map.put(el.getDataId(), el);
		}
		String[] lineDetailArray = splitValues(personVO.getPersonTypes(), ",");
		List<PersonTypeAllocation> personTypeAllocationList = new ArrayList<PersonTypeAllocation>();
		for (String personTyp : lineDetailArray) {
			PersonTypeAllocation personTypeAllocation = new PersonTypeAllocation();
			personTypeAllocation.setLookupDetail(map.get(Integer
					.valueOf(personTyp)));
			personTypeAllocation.setPerson(person);
			personTypeAllocationList.add(personTypeAllocation);
		}
		List<PersonTypeAllocation> personTypeAllocationToDelete = personService
				.getAllPersonTypeAllocation(person);
		personService.deletePersonTypes(personTypeAllocationToDelete);

		personService.savePersonTypes(personTypeAllocationList);

		if (personVO.getCustomerCombinationId() != null
				&& personVO.getCustomerCombinationId() > 0
				&& personVO.getCustomer() != null) {
			personVO.getCustomer().setCombination(customerCombination);
			customerBL.saveCustomer(personVO.getCustomer(),
					personVO.getShippingDetailList());
		}
		if (personVO.getSupplierCombinationId() != null
				&& personVO.getSupplierCombinationId() > 0
				&& personVO.getSupplier() != null) {
			personVO.getSupplier().setCombination(supplierCombination);
			supplierBL.saveSupplier(personVO.getSupplier(),
					supplierCombination.getCombinationId(),
					personVO.getCurrencyId());
		}
		if (personVO.getCandidate() != null
				&& personVO.getCandidate().getOpenPosition() != null) {
			personVO.getCandidate().setPersonByPersonId(person);
			candidateBL.saveCandidate(personVO.getCandidate());
		}

		if (personVO.getPersonId() != null && personVO.getPersonId() > 0) {
			savePersonImages(person, true);
			savePersonDocuments(person, true);
		} else {
			savePersonImages(person, false);
			savePersonDocuments(person, false);
		}

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Person.class.getSimpleName(), person.getPersonId(), user,
				workflowDetailVo);

	}

	public void deletePerson(Long personId) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Person.class.getSimpleName(), personId, user, workflowDetailVo);
	}

	public void doPersonBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		Person person = this.getPersonService().getPersonDetails(recordId);
		if (workflowDetailVO.isDeleteFlag()) {

			List<Person> persons = new ArrayList<Person>();
			imageBL.deleteImages(new ArrayList<Object>(persons));
			documentBL
					.deleteDocuments(new ArrayList<Object>(persons), "Person");
			List<Identity> identitys = this.getPersonService().getAllIdentity(
					person);
			List<Dependent> dependents = this.getPersonService()
					.getAllDependent(person);
			List<AcademicQualifications> qualifications = this
					.getPersonService().getAllAcademicQualifications(person);
			List<Experiences> experiences = this.getPersonService()
					.getAllExperiences(person);
			List<Skills> skills = this.getPersonService().getAllSkills(person);
			List<PersonTypeAllocation> personTypes = new ArrayList<PersonTypeAllocation>(
					person.getPersonTypeAllocations());
			this.getPersonService().deletePerson(person, identitys, dependents,
					qualifications, experiences, skills, personTypes);
		} else {
		}
	}

	public Combination getCombinationInfo(Long natualAccountCombinationId,
			String currentName, String oldName) throws Exception {
		Combination combination = null;
		if (natualAccountCombinationId == null
				|| natualAccountCombinationId == 0)
			return combination;

		Account account = null;
		if (oldName != null && !oldName.equals("")) {
			account = combinationService.getAccountDetail(oldName,
					getImplementation());
			if (null != account) {
				account.setAccount(currentName);
				combinationBL.getAccountBL().getAccountService()
						.updateAccount(account);
			}
		}
		if (account != null) {
			combination = new Combination();
			combination.setCombinationId(natualAccountCombinationId);
			combination = combinationService
					.getCombinationAllAccountDetail(combination
							.getCombinationId());
		} else {
			combination = new Combination();
			combination.setCombinationId(natualAccountCombinationId);
			combination = combinationBL.createAnalysisCombination(combination,
					currentName, true);
		}
		return combination;
	}

	public Combination findCombinationInfo(String currentName) throws Exception {
		Combination combination = null;
		Account account = null;
		account = combinationService.getAccountDetail(currentName,
				getImplementation());
		if (account != null) {
			combination = combinationService.getCombinationByAccount(account
					.getAccountId());
			combination = combinationService
					.getCombinationAllAccountDetail(combination
							.getCombinationId());
		}
		return combination;
	}

	public List<Identity> getIdentityToDelete(List<Identity> identitys,
			List<Identity> identityEdits) {
		List<Identity> identityUpdates = new ArrayList<Identity>();

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		for (Identity identity : identitys) {
			if (identity.getIdentityId() != null
					&& identity.getIdentityId() > 0)
				listOne.add(identity.getIdentityId());
		}
		for (Identity identity : identityEdits) {
			if (identity.getIdentityId() != null
					&& identity.getIdentityId() > 0)
				listTwo.add(identity.getIdentityId());
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);

		// Delete Process
		for (Long lng : different) {
			for (Identity identity : identityEdits) {
				if (lng != null && identity.getIdentityId().equals(lng)) {
					identityUpdates.add(identity);
				}
			}
		}
		return identityUpdates;
	}

	public List<Dependent> getDependentToDelete(List<Dependent> dependents,
			List<Dependent> dependentEdits) {
		List<Dependent> dependentUpdates = new ArrayList<Dependent>();

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		for (Dependent dependent : dependents) {
			if (dependent.getDependentId() != null
					&& dependent.getDependentId() > 0)
				listOne.add(dependent.getDependentId());
		}
		for (Dependent dependent : dependentEdits) {
			if (dependent.getDependentId() != null
					&& dependent.getDependentId() > 0)
				listTwo.add(dependent.getDependentId());
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);

		// Delete Process
		for (Long lng : different) {
			for (Dependent dependent : dependentEdits) {
				if (lng != null && dependent.getDependentId().equals(lng)) {
					dependentUpdates.add(dependent);
				}
			}
		}
		return dependentUpdates;
	}

	public List<AcademicQualifications> getQualificationToDelete(
			List<AcademicQualifications> qualifications,
			List<AcademicQualifications> qualificationEdits) {
		List<AcademicQualifications> qualificationUpdates = new ArrayList<AcademicQualifications>();

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		for (AcademicQualifications qualification : qualifications) {
			if (qualification.getAcademicQualificationsId() != null
					&& qualification.getAcademicQualificationsId() > 0)
				listOne.add(qualification.getAcademicQualificationsId());
		}
		for (AcademicQualifications qualification : qualificationEdits) {
			if (qualification.getAcademicQualificationsId() != null
					&& qualification.getAcademicQualificationsId() > 0)
				listTwo.add(qualification.getAcademicQualificationsId());
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);

		// Delete Process
		for (Long lng : different) {
			for (AcademicQualifications qualification : qualificationEdits) {
				if (lng != null
						&& qualification.getAcademicQualificationsId().equals(
								lng)) {
					qualificationUpdates.add(qualification);
				}
			}
		}
		return qualificationUpdates;
	}

	public List<Experiences> getExperienceToDelete(
			List<Experiences> experiences, List<Experiences> experienceEdits) {
		List<Experiences> experienceUpdates = new ArrayList<Experiences>();

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		for (Experiences exprience : experiences) {
			if (exprience.getExperiencesId() != null
					&& exprience.getExperiencesId() > 0)
				listOne.add(exprience.getExperiencesId());
		}
		for (Experiences exprience : experienceEdits) {
			if (exprience.getExperiencesId() != null
					&& exprience.getExperiencesId() > 0)
				listTwo.add(exprience.getExperiencesId());
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);

		// Delete Process
		for (Long lng : different) {
			for (Experiences exprience : experienceEdits) {
				if (lng != null && exprience.getExperiencesId().equals(lng)) {
					experienceUpdates.add(exprience);
				}
			}
		}
		return experienceUpdates;
	}

	public List<Skills> getSkillToDelete(List<Skills> skills,
			List<Skills> skillEdits) {
		List<Skills> skillUpdates = new ArrayList<Skills>();

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		for (Skills skill : skills) {
			if (skill.getSkillsId() != null && skill.getSkillsId() > 0)
				listOne.add(skill.getSkillsId());
		}
		for (Skills skill : skillEdits) {
			if (skill.getSkillsId() != null && skill.getSkillsId() > 0)
				listTwo.add(skill.getSkillsId());
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);

		// Delete Process
		for (Long lng : different) {
			for (Skills skill : skillEdits) {
				if (lng != null && skill.getSkillsId().equals(lng)) {
					skillUpdates.add(skill);
				}
			}
		}
		return skillUpdates;
	}

	public List<Person> commonPersonList(Long companyId, String departments)
			throws Exception {
		List<Person> persons = new ArrayList<Person>();
		String queryString = "";
		if (companyId > 0 && departments != null) {
			queryString = " AND d.departmentId in (" + departments + ")";
		} else if (companyId > 0 && departments == null) {
			queryString = " AND c.companyId = " + companyId;
		} else {
			queryString = " AND i.implementationId="
					+ getImplementation().getImplementationId();
		}
		persons = personService.getCommonPersonList(queryString);
		return persons;
	}

	public List<Person> personTypeFilteration(String personTypes,
			List<Person> persons) {
		Map<Integer, Integer> personTypesMap = new HashMap<Integer, Integer>();
		boolean exsitfalg = true;
		for (String string : splitValues(personTypes, ",")) {
			if (string != null && !string.trim().equals(""))
				personTypesMap.put(Integer.valueOf(string),
						Integer.valueOf(string));
		}
		if (personTypesMap != null && personTypes != null
				&& !personTypes.trim().equals("")) {
			List<Person> tempResult = new ArrayList<Person>();
			tempResult.addAll(persons);
			for (Person person : tempResult) {
				exsitfalg = false;
				for (PersonTypeAllocation perTypeAllocation : person
						.getPersonTypeAllocations()) {
					if (personTypesMap.containsKey(perTypeAllocation
							.getLookupDetail().getDataId())) {
						exsitfalg = true;
						break;
					}
				}
				if (!exsitfalg)
					persons.remove(person);
			}
		}
		return persons;
	}

	// Report
	public List<Person> getPersonReport(HrPersonalDetailsTO personFilter)
			throws Exception {
		List<Person> persons = new ArrayList<Person>();
		String queryString = " WHERE p.implementation="
				+ getImplementation().getImplementationId();

		queryString += " AND p.isApprove=1 ";

		if (personFilter.getPersonTypes() != null
				&& !personFilter.getPersonTypes().trim().equals(""))
			queryString += " AND ld.lookupDetailId IN ("
					+ personFilter.getPersonTypes() + ")";

		if (personFilter.getDisabled() > 0)
			queryString += " AND p.status=" + personFilter.getDisabled();

		if (personFilter.getGender() != null
				&& !personFilter.getGender().trim().equals("X"))
			queryString += " AND p.gender='" + personFilter.getGender() + "'";

		if (personFilter.getMaritalStatus() != null
				&& !personFilter.getMaritalStatus().trim().equals("X"))
			queryString += " AND p.maritalStatus='"
					+ personFilter.getMaritalStatus() + "'";

		if (personFilter.getNationality() != null
				&& !personFilter.getNationality().trim().equals(""))
			queryString += " AND p.nationality='"
					+ personFilter.getNationality() + "'";

		if (personFilter.getPersonGroup() != null
				&& !personFilter.getPersonGroup().trim().equals("X"))
			queryString += " AND p.personGroup='"
					+ personFilter.getPersonGroup() + "'";

		if (personFilter.getEffectiveDatesFrom() != null
				&& !personFilter.getEffectiveDatesFrom().trim().equals(""))
			queryString += " AND p.validFromDate <='"
					+ DateFormat.convertStringToDate(personFilter
							.getEffectiveDatesFrom()) + "'";

		if (personFilter.getEffectiveDatesTo() != null
				&& !personFilter.getEffectiveDatesTo().trim().equals(""))
			queryString += " AND p.validToDate >='"
					+ DateFormat.convertStringToDate(personFilter
							.getEffectiveDatesTo()) + "'";

		persons = personService.getPersonListForReport(queryString);

		return persons;
	}

	public List<PersonVO> convertToVOList(List<Person> person) throws Exception {

		List<PersonVO> tosList = new ArrayList<PersonVO>();

		for (Person pers : person) {
			tosList.add(personConverterFromEntityTOVO(pers));
		}

		return tosList;
	}

	public PersonVO personConverterFromEntityTOVO(Person person)
			throws Exception {
		PersonVO personVO = null;
		if (person != null) {
			personVO = new PersonVO();
			// Profile Picture update
			byte[] encoded = null;
			String pmg = null;
			byte[] fileByte = null;
			if (person.getProfilePic() != null) {
				String hashName = new String(person.getProfilePic());
				fileByte = this.getImageBL().showImageByHashedName(hashName,
						"AIOSProfilePicture");
				if (fileByte != null)
					encoded = Base64.encodeBase64(fileByte);

				pmg = new String(encoded);
			}
			BeanUtils.copyProperties(personVO, person);
			personVO.setProfilePicDisplay(pmg);

			personVO.setPersonName("");
			if (personVO.getPrefix() != null
					&& !personVO.getPrefix().equals(""))
				personVO.setPersonName(personVO.getPersonName() + " "
						+ personVO.getPrefix());

			if (personVO.getFirstName() != null
					&& !personVO.getFirstName().equals(""))
				personVO.setPersonName(personVO.getPersonName() + " "
						+ personVO.getFirstName());

			if (personVO.getMiddleName() != null
					&& !personVO.getMiddleName().equals(""))
				personVO.setPersonName(personVO.getPersonName() + " "
						+ personVO.getMiddleName());

			if (personVO.getLastName() != null
					&& !personVO.getLastName().equals(""))
				personVO.setPersonName(personVO.getPersonName() + " "
						+ personVO.getLastName());

			if (personVO.getFirstNameArabic() != null)

				personVO.setFirstNameArabicStr(AIOSCommons
						.bytesToUTFString(personVO.getFirstNameArabic()));

			if (personVO.getMiddleNameArabic() != null)
				personVO.setMiddleNameArabicStr(AIOSCommons
						.bytesToUTFString(personVO.getMiddleNameArabic()));

			if (personVO.getLastNameArabic() != null)
				personVO.setLastNameArabicStr(AIOSCommons
						.bytesToUTFString(personVO.getLastNameArabic()));

			if (personVO.getSuffix() != null
					&& !personVO.getSuffix().equals(""))
				personVO.setPersonName(personVO.getPersonName() + " "
						+ personVO.getSuffix());

			if (person.getPersonTypeAllocations() != null
					&& person.getPersonTypeAllocations().size() > 0) {
				for (PersonTypeAllocation personTypeAllocation : person
						.getPersonTypeAllocations()) {
					personVO.setPersonTypes(personTypeAllocation
							.getLookupDetail().getDataId().toString());
				}
			}

			if (person.getValidFromDate() != null)
				personVO.setEffectiveDatesFrom(DateFormat
						.convertDateToString(person.getValidFromDate()
								.toString()));

			if (person.getValidToDate() != null)
				personVO.setEffectiveDatesTo(DateFormat
						.convertDateToString(person.getValidToDate().toString()));

			if (person.getBirthDate() != null)
				personVO.setBirthDateStr(DateFormat.convertDateToString(person
						.getBirthDate().toString()));

			if (person.getPersonTypeAllocations() != null
					&& person.getPersonTypeAllocations().size() > 0) {
				String tempPersonTypes = "";
				String tempPersonIdTypes = "";

				for (PersonTypeAllocation personTypeAllocation : person
						.getPersonTypeAllocations()) {
					tempPersonTypes += personTypeAllocation.getLookupDetail()
							.getDisplayName() + ",";
					tempPersonIdTypes += personTypeAllocation.getLookupDetail()
							.getDataId() + ",";
				}
				personVO.setPersonTypes(tempPersonTypes);
				personVO.setPersonTypeIds(tempPersonIdTypes);
			}

			if (Character.toString(personVO.getPersonGroup()).equals("E")) {
				personVO.setPersonGroupCaption("Expatriate");
			} else if (Character.toString(personVO.getPersonGroup())
					.equals("L")) {
				personVO.setPersonGroupCaption("Local");
			} else if (Character.toString(personVO.getPersonGroup())
					.equals("A")) {
				personVO.setPersonGroupCaption("Arab");
			} else if (Character.toString(personVO.getPersonGroup())
					.equals("G")) {
				personVO.setPersonGroupCaption("Gulf");
			}

			if (personVO.getNationality() != null
					&& !personVO.getNationality().equals("")) {
				Country nationality = systemService.getCountryById(Long
						.parseLong(personVO.getNationality()));
				personVO.setNationalityCaption(nationality.getCountryName());
				personVO.setNationalityCaptionAR(AIOSCommons
						.bytesToUTFString(nationality.getCountryNameArabic()));
			}

			if (personVO.getGender().toString().equals("M")) {
				personVO.setGenderCaption("Male");
			} else if (personVO.getGender().toString().equals("F")) {
				personVO.setGenderCaption("Female");
			}

			if (Character.toString(personVO.getMaritalStatus()).equals("M")) {
				personVO.setMaritalStatusName("Married");
			} else if (Character.toString(personVO.getMaritalStatus()).equals(
					"S")) {
				personVO.setMaritalStatusName("Single");
			} else if (Character.toString(personVO.getMaritalStatus()).equals(
					"W")) {
				personVO.setMaritalStatusName("Widow");
			} else if (Character.toString(personVO.getMaritalStatus()).equals(
					"P")) {
				personVO.setMaritalStatusName("Separated");
			}

			if (personVO.getStatus() == (byte) 1) {
				personVO.setStatusCaption("Active");
			} else if (personVO.getStatus() == (byte) 0) {
				personVO.setStatusCaption("In-Active");
			}

			if (personVO.getAcademicQualificationses() != null
					&& personVO.getAcademicQualificationses().size() > 0) {
				List<PersonVO> qualifications = new ArrayList<PersonVO>();
				for (AcademicQualifications aq : personVO
						.getAcademicQualificationses()) {
					PersonVO qualification = new PersonVO();
					if (aq.getLookupDetail() != null)
						qualification.setDegree(aq.getLookupDetail()
								.getDisplayName());
					qualification.setMajorSubject(aq.getMajorSubject());
					qualification.setInstution(aq.getInstution());
					qualification.setCompletionYearDisplay(aq
							.getCompletionYear().toString());
					qualification.setResult(aq.getResult());
					qualification.setResultType(aq.getResultType());
					qualifications.add(qualification);
				}
				personVO.setAcademicQualificationsVO(qualifications);
			}

			if (personVO.getExperienceses() != null
					&& personVO.getExperienceses().size() > 0) {
				List<PersonVO> experiences = new ArrayList<PersonVO>();
				for (Experiences exp : personVO.getExperienceses()) {
					PersonVO experiences2 = new PersonVO();
					experiences2.setPreviousExperience(exp
							.getPreviousExperience());
					experiences2.setCurrentPost(exp.getCurrentPost());
					experiences2.setJobTitle(exp.getJobTitle());
					experiences2.setCompanyName(exp.getCompanyName());
					experiences2.setStartDateDisplay(exp.getStartDate()
							.toString());
					experiences2.setEndDateDisplay(exp.getEndDate().toString());
					experiences.add(experiences2);
				}
				personVO.setExperiencesVO(experiences);
			}

			if (personVO.getSkillses() != null
					&& personVO.getSkillses().size() > 0) {
				List<PersonVO> skillsVO = new ArrayList<PersonVO>();
				for (Skills skill : personVO.getSkillses()) {
					PersonVO sk = new PersonVO();
					sk.setSkillType(skill.getLookupDetail().getDisplayName());
					sk.setSkillLevel(skill.getSkillLevel());
					sk.setDescription(skill.getDescription());
					skillsVO.add(sk);
				}
				personVO.setSkillsVO(skillsVO);
			}

		}

		// Employee details
		if (person.getJobAssignments() != null) {
			personVO.setJobAssignments(person.getJobAssignments());
		}
		return personVO;

	}

	public static PersonVO convertIdentityToVO(PersonVO personVO,
			Identity identity) throws Exception {

		List<PersonVO> identityTypeList = identificationTypeList();
		personVO.setIdentityId(identity.getIdentityId());
		personVO.setIdentityType(identity.getIdentityType());
		for (PersonVO identityType : identityTypeList) {
			if (identityType.getIdentityType() == identity.getIdentityType())
				personVO.setIdentityTypeName(identityType.getIdentityTypeName());
		}
		personVO.setIdentityNumber(identity.getIdentityNumber());
		personVO.setIssuedPlace(identity.getIssuedPlace());
		if (identity.getExpireDate() != null) {
			personVO.setExpirationdate(DateFormat.convertDateToString(identity
					.getExpireDate().toString()));
			personVO.setDaysLeft(DateFormat.dayVariance(new Date(),
					identity.getExpireDate())
					+ "");
		}
		personVO.setDescription(identity.getDescription());
		return personVO;
	}

	public void moveProfilePicToDrive() {
		try {
			List<Person> persons = personService
					.getAllPersonWithOutImplementation();
			for (Person person : persons) {
				if (person.getProfilePic() != null) {
					String fileHashName = "";
					Map<String, byte[]> imageFileBytesMap = new HashMap<String, byte[]>();
					Properties confProps = (Properties) ServletActionContext
							.getServletContext().getAttribute("confProps");
					imageFileBytesMap.put(
							person.getFirstName() + "_"
									+ person.getPersonNumber() + ".PNG",
							person.getProfilePic());
					Map<String, FileVO> imageFilesMap = FileUtil
							.copyCachedFiles(imageFileBytesMap, confProps,
									null, "AIOSProfilePicture");

					FileVO vo = imageFilesMap.get(person.getFirstName() + "_"
							+ person.getPersonNumber() + ".PNG");
					fileHashName = vo.getHashedName();
					if (fileHashName != null)
						person.setProfilePic(fileHashName.getBytes());
				}
			}

			personService.savePersons(persons);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<PersonVO> identificationTypeList() throws Exception {
		List<PersonVO> identificationTypeList = new ArrayList<PersonVO>();
		PersonVO typeTo = null;
		typeTo = new PersonVO();
		typeTo.setIdentityTypeName("National ID");
		typeTo.setIdentityType(1);
		identificationTypeList.add(typeTo);

		typeTo = new PersonVO();
		typeTo.setIdentityTypeName("Labour ID");
		typeTo.setIdentityType(2);
		identificationTypeList.add(typeTo);

		typeTo = new PersonVO();
		typeTo.setIdentityTypeName("Passport");
		typeTo.setIdentityType(3);
		identificationTypeList.add(typeTo);

		typeTo = new PersonVO();
		typeTo.setIdentityTypeName("Insurance Card");
		typeTo.setIdentityType(4);
		identificationTypeList.add(typeTo);

		typeTo = new PersonVO();
		typeTo.setIdentityTypeName("Residence Visa");
		typeTo.setIdentityType(5);
		identificationTypeList.add(typeTo);

		typeTo = new PersonVO();
		typeTo.setIdentityTypeName("Driving License");
		typeTo.setIdentityType(6);
		identificationTypeList.add(typeTo);

		typeTo = new PersonVO();
		typeTo.setIdentityTypeName("Labour Contract");
		typeTo.setIdentityType(7);
		identificationTypeList.add(typeTo);

		typeTo = new PersonVO();
		typeTo.setIdentityTypeName("Company Contract");
		typeTo.setIdentityType(8);
		identificationTypeList.add(typeTo);

		typeTo = new PersonVO();
		typeTo.setIdentityTypeName("Employeement Visa");
		typeTo.setIdentityType(9);
		identificationTypeList.add(typeTo);

		return identificationTypeList;
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public PersonService getPersonService() {
		return personService;
	}

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

	public DocumentService getDocumentService() {
		return documentService;
	}

	public void setDocumentService(DocumentService documentService) {
		this.documentService = documentService;
	}

	public ImageService getImageService() {
		return imageService;
	}

	public void setImageService(ImageService imageService) {
		this.imageService = imageService;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public SupplierBL getSupplierBL() {
		return supplierBL;
	}

	public void setSupplierBL(SupplierBL supplierBL) {
		this.supplierBL = supplierBL;
	}

	public CustomerBL getCustomerBL() {
		return customerBL;
	}

	public void setCustomerBL(CustomerBL customerBL) {
		this.customerBL = customerBL;
	}

	public GLCombinationService getCombinationService() {
		return combinationService;
	}

	public void setCombinationService(GLCombinationService combinationService) {
		this.combinationService = combinationService;
	}

	public CombinationBL getCombinationBL() {
		return combinationBL;
	}

	public void setCombinationBL(CombinationBL combinationBL) {
		this.combinationBL = combinationBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public CandidateBL getCandidateBL() {
		return candidateBL;
	}

	public void setCandidateBL(CandidateBL candidateBL) {
		this.candidateBL = candidateBL;
	}

	public CommissionRuleBL getCommissionRuleBL() {
		return commissionRuleBL;
	}

	public void setCommissionRuleBL(CommissionRuleBL commissionRuleBL) {
		this.commissionRuleBL = commissionRuleBL;
	}

	public CompanyService getCompanyService() {
		return companyService;
	}

	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
