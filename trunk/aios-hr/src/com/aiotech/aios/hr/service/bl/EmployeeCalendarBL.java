package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.EmployeeCalendar;
import com.aiotech.aios.hr.domain.entity.EmployeeCalendarPeriod;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.vo.EmployeeCalendarVO;
import com.aiotech.aios.hr.service.EmployeeCalendarService;
import com.aiotech.aios.hr.service.JobService;
import com.aiotech.aios.hr.service.LocationService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class EmployeeCalendarBL {

	private EmployeeCalendarService employeeCalendarService;
	private LocationService locationService;
	private JobService jobService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public String saveCalendar(List<EmployeeCalendarPeriod> calendarPeriods,
			EmployeeCalendar employeeCalendar) {
		String returnMessage = "SUCCESS";
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			employeeCalendar
					.setIsApprove((byte) WorkflowConstants.Status.Published
							.getCode());
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (employeeCalendar != null
					&& employeeCalendar.getEmployeeCalendarId() != null
					&& employeeCalendar.getEmployeeCalendarId() > 0) {

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			if (employeeCalendar.getEmployeeCalendarId() != null
					&& employeeCalendar.getEmployeeCalendarId() > 0) {
				List<EmployeeCalendarPeriod> fromDbList = new ArrayList<EmployeeCalendarPeriod>(
						employeeCalendar.getEmployeeCalendarPeriods());
				List<EmployeeCalendarPeriod> deleteList = getSimilerPeriod(
						calendarPeriods, fromDbList);
				if (deleteList != null)
					employeeCalendarService
							.deleteEmployeeCalendarPeriods(deleteList);
			}
			employeeCalendarService.saveEmployeeCalendar(employeeCalendar);
			for (EmployeeCalendarPeriod period : calendarPeriods) {
				period.setEmployeeCalendar(employeeCalendar);
			}
			employeeCalendarService
					.saveEmployeeCalendarPeriods(calendarPeriods);

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					EmployeeCalendar.class.getSimpleName(),
					employeeCalendar.getEmployeeCalendarId(), user,
					workflowDetailVo);
		} catch (Exception e) {
			e.printStackTrace();
			returnMessage = "Failure";
		}
		return returnMessage;
	}

	public void deleteEmployeeCalendar(Long calendarId)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				EmployeeCalendar.class.getSimpleName(), calendarId,
				user, workflowDetailVo);
	}

	public void doEmployeeCalendarBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		EmployeeCalendar employeeCalendar = this.getEmployeeCalendarService()
				.getEmployeeCalendarInfo(recordId);
		List<EmployeeCalendarPeriod> employeeCalendarPeriods = new ArrayList<EmployeeCalendarPeriod>(
				employeeCalendar.getEmployeeCalendarPeriods());
		if (workflowDetailVO.isDeleteFlag()) {
			this.getEmployeeCalendarService().deleteEmployeeCalendarPeriods(
					employeeCalendarPeriods);
			this.getEmployeeCalendarService().deleteEmployeeCalendar(
					employeeCalendar);
		} else {
		}
	}

	public List<EmployeeCalendarPeriod> getSimilerPeriod(
			List<EmployeeCalendarPeriod> fromScreen,
			List<EmployeeCalendarPeriod> fromDB) {
		List<EmployeeCalendarPeriod> periodToDeleteOrInactive = new ArrayList<EmployeeCalendarPeriod>();

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		for (EmployeeCalendarPeriod period : fromScreen) {
			if (period.getEmployeeCalendarPeriodId() != null
					&& period.getEmployeeCalendarPeriodId() > 0)
				listOne.add(period.getEmployeeCalendarPeriodId());
		}
		for (EmployeeCalendarPeriod period : fromDB) {
			if (period.getEmployeeCalendarPeriodId() != null
					&& period.getEmployeeCalendarPeriodId() > 0)
				listTwo.add(period.getEmployeeCalendarPeriodId());
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);

		// Delete Process
		for (Long lng : different) {
			for (EmployeeCalendarPeriod period : fromDB) {
				if (lng != null
						&& period.getEmployeeCalendarPeriodId().equals(lng)) {
					periodToDeleteOrInactive.add(period);
				}
			}
		}
		return periodToDeleteOrInactive;
	}

	public List<EmployeeCalendarPeriod> getCalendarLineDetail(
			String calendarLineDetail) {
		List<EmployeeCalendarPeriod> calendars = new ArrayList<EmployeeCalendarPeriod>();
		if (calendarLineDetail != null && !calendarLineDetail.equals("")) {
			String[] recordArray = new String[calendarLineDetail.split("##").length];
			recordArray = calendarLineDetail.split("##");
			for (String record : recordArray) {
				EmployeeCalendarPeriod calendarPeriod = new EmployeeCalendarPeriod();
				String[] dataArray = new String[record.split("@").length];
				dataArray = record.split("@");
				if (!dataArray[0].equals("-1")
						&& !dataArray[0].trim().equals(""))
					calendarPeriod.setEmployeeCalendarPeriodId(Long
							.parseLong(dataArray[0]));
				if (!dataArray[1].equals("-1")
						&& !dataArray[1].trim().equals(""))
					calendarPeriod.setFromDate(DateFormat
							.convertStringToDate(dataArray[1]));
				if (!dataArray[2].equals("-1")
						&& !dataArray[2].trim().equals(""))
					calendarPeriod.setToDate(DateFormat
							.convertStringToDate(dataArray[2]));
				if (!dataArray[3].equals("-1")
						&& !dataArray[3].trim().equals(""))
					calendarPeriod.setHolidayType(Byte.parseByte(dataArray[3]));
				if (!dataArray[4].equals("-1")
						&& !dataArray[4].trim().equals(""))
					calendarPeriod.setDescription(dataArray[4]);

				calendars.add(calendarPeriod);
			}
		}
		return calendars;
	}

	public EmployeeCalendar getEmployeeCalendarInfo(
			JobAssignment jobAssignment,
			List<EmployeeCalendar> employeeCalendars) {
		EmployeeCalendar employeeCalendar = new EmployeeCalendar();
		for (EmployeeCalendar employeeCalendar2 : employeeCalendars) {
			if (employeeCalendar2.getJobAssignment() != null
					&& employeeCalendar2.getJobAssignment()
							.getJobAssignmentId()
							.equals(jobAssignment.getJobAssignmentId())) {
				employeeCalendar = employeeCalendar2;
				break;
			} else if (employeeCalendar2.getLocation() != null) {
				if (employeeCalendar2
						.getLocation()
						.getLocationId()
						.equals(jobAssignment.getCmpDeptLocation()
								.getLocation().getLocationId())) {
					employeeCalendar = employeeCalendar2;
				} else {
					Long tempLoc = jobAssignment.getCmpDeptLocation()
							.getLocation().getLocationId();
					String[] locGroup = null;
					if (employeeCalendar2.getLocationGroup() != null) {
						locGroup = employeeCalendar2.getLocationGroup().split(
								",");
						for (String loc : locGroup) {
							if (Long.parseLong(loc) == tempLoc) {
								employeeCalendar = employeeCalendar2;
								break;
							}
						}
					}
				}
			}
		}

		return employeeCalendar;
	}

	public List<EmployeeCalendarVO> getHolidayTypeList() {
		List<EmployeeCalendarVO> employeeCalendarVOs = new ArrayList<EmployeeCalendarVO>();
		EmployeeCalendarVO employeeCalendarVO = null;

		employeeCalendarVO = new EmployeeCalendarVO();
		employeeCalendarVO.setHolidayTypeId(1);
		employeeCalendarVO.setHolidayType("Public Holiday");
		employeeCalendarVOs.add(employeeCalendarVO);

		employeeCalendarVO = new EmployeeCalendarVO();
		employeeCalendarVO.setHolidayTypeId(2);
		employeeCalendarVO.setHolidayType("Work Off");
		employeeCalendarVOs.add(employeeCalendarVO);

		employeeCalendarVO = new EmployeeCalendarVO();
		employeeCalendarVO.setHolidayTypeId(3);
		employeeCalendarVO.setHolidayType("Religious Holiday");
		employeeCalendarVOs.add(employeeCalendarVO);

		employeeCalendarVO = new EmployeeCalendarVO();
		employeeCalendarVO.setHolidayTypeId(4);
		employeeCalendarVO.setHolidayType("Unofficial Holiday");
		employeeCalendarVOs.add(employeeCalendarVO);

		employeeCalendarVO = new EmployeeCalendarVO();
		employeeCalendarVO.setHolidayTypeId(5);
		employeeCalendarVO.setHolidayType("Other Holiday");
		employeeCalendarVOs.add(employeeCalendarVO);

		return employeeCalendarVOs;
	}

	public EmployeeCalendarService getEmployeeCalendarService() {
		return employeeCalendarService;
	}

	public void setEmployeeCalendarService(
			EmployeeCalendarService employeeCalendarService) {
		this.employeeCalendarService = employeeCalendarService;
	}

	public LocationService getLocationService() {
		return locationService;
	}

	public void setLocationService(LocationService locationService) {
		this.locationService = locationService;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

}
