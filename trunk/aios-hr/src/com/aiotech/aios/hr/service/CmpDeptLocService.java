package com.aiotech.aios.hr.service;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Department;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class CmpDeptLocService {

	private AIOTechGenericDAO<CmpDeptLoc> cmpDeptLocDAO;
	private AIOTechGenericDAO<Department> departmentDAO;

	public void saveCmpDeptLocation(CmpDeptLoc cmpDeptLocation)throws Exception{
		cmpDeptLocDAO.saveOrUpdate(cmpDeptLocation);
	}
	
	public void deleteSetup(CmpDeptLoc cmpDeptLocation)throws Exception{
		cmpDeptLocDAO.delete(cmpDeptLocation);
	}
	
	public void deleteSetupAll(List<CmpDeptLoc> cmpDeptLocations)throws Exception{
		cmpDeptLocDAO.deleteAll(cmpDeptLocations);
	}
	
	public List<CmpDeptLoc> getAllCmpDeptLocation(Company company) throws Exception{
		return cmpDeptLocDAO.findByNamedQuery("getAllCompanySetup", company);
	}
	
	public List<CmpDeptLoc> getSetupCDLBased(Long companyId,Long departmentId,Long locationId) throws Exception{
		return cmpDeptLocDAO.findByNamedQuery("getSetupCDLBased", companyId,departmentId,locationId);
	}
	
	public List<CmpDeptLoc> getSetupCLBased(Long companyId,Long locationId) throws Exception{
		return cmpDeptLocDAO.findByNamedQuery("getSetupCLBased", companyId,locationId);
	}
	
	public List<CmpDeptLoc> getSetupDepartmentBased(Long departmentId) throws Exception{
		return cmpDeptLocDAO.findByNamedQuery("getSetupDepartmentBased", departmentId);
	}
	
	public List<CmpDeptLoc> getSetupLocationBased(Long locationId) throws Exception{
		return cmpDeptLocDAO.findByNamedQuery("getSetupLocationBased", locationId);
	}
	
	public CmpDeptLoc getCompanySetup(Long setupId) throws Exception{
		return cmpDeptLocDAO.findById(setupId);
	}
	
	public CmpDeptLoc getBranchAndLocationInfo(Long branchLocationId) throws Exception{
		List<CmpDeptLoc> setups= cmpDeptLocDAO.findByNamedQuery("getBranchAndLocationInfo", branchLocationId);
		if(setups!=null && setups.size()>0)
			return setups.get(0);
		else
			return null;
	}
	
	public List<CmpDeptLoc> getSetupByCombinationId(Long combinationId) throws Exception{
		return cmpDeptLocDAO.findByNamedQuery("getSetupByCombinationId", combinationId);
	}
	
	//Get Location list
	public List<CmpDeptLoc> getAllLocation(Implementation implementation) {
		return cmpDeptLocDAO.findByNamedQuery("getAllLocation", implementation);
	}
	
	public List<Department> getCompanyBasedDepartment(Long companyId) {
		return departmentDAO.findByNamedQuery("getCompanyBasedDepartment", companyId);
	}
	
	public AIOTechGenericDAO<CmpDeptLoc> getCmpDeptLocDAO() {
		return cmpDeptLocDAO;
	}

	public void setCmpDeptLocDAO(
			AIOTechGenericDAO<CmpDeptLoc> cmpDeptLocDAO) {
		this.cmpDeptLocDAO = cmpDeptLocDAO;
	}

	public AIOTechGenericDAO<Department> getDepartmentDAO() {
		return departmentDAO;
	}

	public void setDepartmentDAO(AIOTechGenericDAO<Department> departmentDAO) {
		this.departmentDAO = departmentDAO;
	}

} 
