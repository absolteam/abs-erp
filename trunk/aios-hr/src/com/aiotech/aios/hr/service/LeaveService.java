package com.aiotech.aios.hr.service;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.hr.domain.entity.Leave;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class LeaveService {
public AIOTechGenericDAO<Leave> leaveDAO;
	
	public List<Leave> getLeaveList(Implementation implementation) {
		return leaveDAO.findByNamedQuery("getLeaveList", implementation,
				(byte)WorkflowConstants.Status.Published.getCode());
	}
	
	public List<Leave> getActiveLeaveList(Implementation implementation) {
		return leaveDAO.findByNamedQuery("getActiveLeaveList", implementation,
				(byte)WorkflowConstants.Status.Published.getCode());
	}
	
	public Leave getLeaveInfo(Long leaveId) {
		List<Leave> leaves=new ArrayList<Leave>();
		leaves=leaveDAO.findByNamedQuery("getLeaveInfo", leaveId);
		if(leaves!=null && leaves.size()>0)
			return leaves.get(0);
		else
			return null;
	}
	
	public void saveLeave(Leave leave){
		leaveDAO.saveOrUpdate(leave);
	}
	
	public void deleteLeave(Leave leave){
		leaveDAO.delete(leave);
	}

	public AIOTechGenericDAO<Leave> getLeaveDAO() {
		return leaveDAO;
	}

	public void setLeaveDAO(AIOTechGenericDAO<Leave> leaveDAO) {
		this.leaveDAO = leaveDAO;
	}

}
