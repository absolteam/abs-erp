package com.aiotech.aios.hr.service.bl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Candidate;
import com.aiotech.aios.hr.domain.entity.Interview;
import com.aiotech.aios.hr.domain.entity.InterviewProcess;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.QuestionBank;
import com.aiotech.aios.hr.domain.entity.vo.InterviewVO;
import com.aiotech.aios.hr.service.InterviewProcessService;
import com.aiotech.aios.hr.service.InterviewService;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.Role;
import com.aiotech.aios.workflow.domain.entity.Screen;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.sun.org.apache.xml.internal.utils.DefaultErrorHandler;

public class InterviewBL {
	private InterviewService interviewService;
	private InterviewProcessService interviewProcessService;
	private LookupMasterBL lookupMasterBL;
	private DocumentBL documentBL;
	private ImageBL imageBL;
	private DirectoryBL directoryBL;
	private LocationBL locationBL;
	private CandidateBL candidateBL;
	private JobAssignmentBL jobAssignmentBL;
	private QuestionBankBL questionBankBL;
	private AlertBL alertBL;
	private SystemBL systemBL;
	private Implementation implementation;

	@SuppressWarnings("unused")
	public List<InterviewVO> convertAllEntityToVO(List<Interview> interviews)
			throws Exception {
		List<InterviewVO> interviewVOs = null;
		if (interviews != null && interviews.size() > 0) {
			interviewVOs = new ArrayList<InterviewVO>();
			for (Interview interview : interviews) {
				interviewVOs.add(convertEntityIntoVO(interview));
			}
		}
		return interviewVOs;
	}

	public InterviewVO convertEntityIntoVO(Interview interview)
			throws Exception {
		InterviewVO interviewVO = new InterviewVO();
		Map<Integer, String> statues = this.status();
		Map<Integer, String> rounds = this.rounds();
		// Copy Interview class properties value into VO properties.
		BeanUtils.copyProperties(interviewVO, interview);
		if (interview.getInterviewTime() != null) {
			interviewVO.setInterviewDateView(DateFormat
					.convertDateToString(interview.getInterviewTime() + ""));
			interviewVO.setInterviewTimeView(DateFormat
					.convertDateToTimeOnly(interview.getInterviewTime()));
		}
		if (interview.getCreatedDate() != null)
			interviewVO.setCreatedDateView(DateFormat
					.convertDateToString(interview.getCreatedDate() + ""));
		interviewVO.setPositionReferenceNumber(interview.getOpenPosition()
				.getPositionReferenceNumber());
		interviewVO.setRoundView(rounds.get(Integer.valueOf(interview
				.getRound())));
		interviewVO.setStatusView(statues.get(Integer.valueOf(interview
				.getStatus())));
		interviewVO.setDesignationName(interview.getOpenPosition()
				.getDesignation().getDesignationName());
		interviewVO.setGradeName(interview.getOpenPosition().getDesignation()
				.getGrade().getGradeName());
		interviewVO.setQuestionType(interview.getQuestionBank()
				.getQuestionType());
		Map<Integer, String> patterns = questionBankBL.questionPatterns();
		interviewVO
				.setQuestionPatternView(patterns.get(Integer.valueOf(interview
						.getQuestionBank().getQuestionPattern() + "")));
		interviewVO.setRoom(interview.getLookupDetail().getDisplayName());
		interviewVO.setAddress((locationBL.getLocationFullAddress(interview
				.getLocation().getLocationId())).getFullAddress());
		String candidateIds = "";
		if (interview.getInterviewProcesses() != null
				&& interview.getInterviewProcesses().size() > 0) {
			for (InterviewProcess interviewProcess : interview
					.getInterviewProcesses())
				candidateIds += interviewProcess.getCandidate()
						.getCandidateId() + ",";

		}
		interviewVO.setCandidatesArray(candidateIds.replaceAll(" ,$", ""));

		return interviewVO;
	}

	public InterviewVO convertEntityIntoVO(InterviewProcess interviewProcess)
			throws Exception {
		InterviewVO interviewVO = new InterviewVO();
		Map<Integer, String> statues = this.status();
		Map<Integer, String> rounds = this.rounds();
		interviewVO.setInterviewProcess(interviewProcess);
		Interview interview = interviewProcess.getInterview();

		// Copy Interview class properties value into VO properties.
		BeanUtils.copyProperties(interviewVO, interview);
		if (interview.getInterviewTime() != null) {
			interviewVO.setInterviewDateView(DateFormat
					.convertDateToString(interview.getInterviewTime() + ""));
			interviewVO.setInterviewTimeView(DateFormat
					.convertDateToTimeOnly(interview.getInterviewTime()));
		}
		if (interview.getCreatedDate() != null)
			interviewVO.setCreatedDateView(DateFormat
					.convertDateToString(interview.getCreatedDate() + ""));
		interviewVO.setPositionReferenceNumber(interview.getOpenPosition()
				.getPositionReferenceNumber());
		interviewVO.setRoundView(rounds.get(Integer.valueOf(interview
				.getRound())));
		interviewVO.setStatusView(statues.get(Integer.valueOf(interview
				.getStatus())));
		interviewVO.setDesignationName(interview.getOpenPosition()
				.getDesignation().getDesignationName());
		interviewVO.setGradeName(interview.getOpenPosition().getDesignation()
				.getGrade().getGradeName());
		interviewVO.setQuestionType(interview.getQuestionBank()
				.getQuestionType());
		Map<Integer, String> patterns = questionBankBL.questionPatterns();
		interviewVO
				.setQuestionPatternView(patterns.get(Integer.valueOf(interview
						.getQuestionBank().getQuestionPattern() + "")));
		interviewVO.setRoom(interview.getLookupDetail().getDisplayName());
		interviewVO.setAddress((locationBL.getLocationFullAddress(interview
				.getLocation().getLocationId())).getFullAddress());

		interviewVO.setCandidateVO(candidateBL
				.convertEntityIntoVO(interviewProcess.getCandidate()));

		return interviewVO;
	}

	public void saveInterview(Interview interview, InterviewVO interviewVO)
			throws Exception {
		interviewService.saveInterview(interview);
		List<InterviewProcess> interviewProcesses = null;
		List<Alert> alerts = null;
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		Person person = new Person();
		person.setPersonId(user.getPersonId());

		// Get the screen Name to notify the interview schedule
		Screen screen = systemBL.getSystemService().getScreenBasedOnScreenName(
				"InterviewUpdateByInterviewers");

		if (interviewVO.getCandidatesArray() != null) {
			interviewProcesses = new ArrayList<InterviewProcess>();
			alerts = new ArrayList<Alert>();
			InterviewProcess interviewProcess = null;
			for (String string : interviewVO.getCandidatesArray().split(",")) {
				if (string != null && !string.equals("")) {
					interviewProcess = new InterviewProcess();
					Candidate candidate = new Candidate();
					candidate.setCandidateId(Long.valueOf(string));
					interviewProcess.setCandidate(candidate);
					interviewProcess.setCreatedDate(new Date());
					interviewProcess.setStatus(Byte.valueOf("1"));
					interviewProcess.setImplementation(getImplementation());
					interviewProcess.setIsApprove(Byte
							.valueOf(Constants.RealEstate.Status.NoDecession
									.getCode() + ""));
					interviewProcess.setPerson(person);
					interviewProcess.setCandidate(candidate);
					interviewProcess.setInterview(interview);
					interviewProcesses.add(interviewProcess);
				}
			}

			interviewService.saveInterviewProcesses(interviewProcesses);

			for (InterviewProcess interviewProcessTemp : interviewProcesses) {
				// ALert Mapping
				Alert alert = new Alert();
				alert.setRecordId(interviewProcessTemp.getInterviewProcessId());
				alerts.add(alert);
				// Update Candidate Status into scheduled
				candidateBL
						.updateCandidateStatus(interviewProcessTemp
								.getCandidate().getCandidateId(),
								(byte) Constants.HR.InterviewStatus.Scheduled
										.getCode());

			}

			// Alert list manipulation according to the interveiwers list
			if (alerts != null && alerts.size() > 0) {
				List<Alert> finalList = new ArrayList<Alert>();
				List<Role> roles = findInterviewersRole(interviewVO
						.getInterviewersArray());
				for (Alert alert : alerts) {
					for (Role role : roles) {
						Alert alertTemp = new Alert();
						alertTemp.setRecordId(alert.getRecordId());
						alertTemp.setTableName("InterviewProcess");
						alertTemp.setScreenId(screen.getScreenId());
						alertTemp.setRoleId(role.getRoleId());
						alertTemp.setIsActive(true);
						alertTemp.setMessage("Interview Sceduled at "
								+ interview.getInterviewTime());
						finalList.add(alertTemp);
					}
				}

				if (finalList != null && finalList.size() > 0)
					alertBL.commonSaveAllAlert(finalList);
				else
					throw new ArithmeticException();
			}
			;
		}
	}

	public List<Role> findInterviewersRole(String interviewers)
			throws Exception {
		List<Role> roles = null;
		if (interviewers != null && !interviewers.equals("")) {
			roles = new ArrayList<Role>();
			List<Person> persons = jobAssignmentBL.getJobBL()
					.getPersonService().getPersonsByAssignment(interviewers);
			String personString = "";
			for (Person person : persons) {
				personString += person.getPersonId() + ",";
			}
			personString = personString.replaceAll(" ,$", "");

			roles = systemBL.getSystemService().getRolesByPersons(personString);
		}

		return roles;
	}

	public void deleteInterview(Interview interview, InterviewVO interviewVO) {
		List<InterviewProcess> interviewProcesses = null;
		if (interview != null && interview.getInterviewProcesses() != null
				&& interview.getInterviewProcesses().size() > 0) {
			alertBL.getAlertService().deleteAlert(interviewVO.getAlerts());
			interviewProcesses = new ArrayList<InterviewProcess>(
					interview.getInterviewProcesses());
			interviewService.deleteInterviewProcesses(interviewProcesses);
		}
		interviewService.deleteInterview(interview);
	}

	public void saveInterviewProcess(InterviewProcess interviewProcess,
			Long alertId) {
		Alert alert;
		try {
			// set status as interviewed
			// Update Candidate Status into interviewed
			if (isFinalRoundCheck(interviewProcess)) {
				candidateBL.updateCandidateStatus(interviewProcess
						.getCandidate().getCandidateId(),
						(byte) Constants.HR.InterviewStatus.Interviewed
								.getCode());
				interviewProcess.getInterview().setStatus(
						(byte) Constants.HR.InterviewStatus.Interviewed
								.getCode());
			}else{
				interviewProcess.getInterview().setStatus(
						(byte) Constants.HR.InterviewStatus.Scheduled
								.getCode());
				interviewService.saveInterview(interviewProcess.getInterview());
			}
			interviewService.saveInterview(interviewProcess.getInterview());

			interviewService.saveInterviewProcess(interviewProcess);
			alert = alertBL.getAlertService().getAlertInfo(alertId);
			if (null != alert && !("").equals(alert)) {
				alert.setIsActive(false);
				alertBL.commonSaveAlert(alert);
			}

			saveDocuments(interviewProcess, true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public boolean isFinalRoundCheck(InterviewProcess interviewProcess) {
		boolean returnStatus = false;
		if (interviewProcess != null && interviewProcess.getInterview() != null
				&& interviewProcess.getInterview().getRound() != null
				&& interviewProcess.getInterview().getRound() == 100)
			returnStatus = true;

		return returnStatus;
	}

	public void saveDocuments(InterviewProcess interviewProcess,
			boolean editFlag) throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(interviewProcess,
				"uploadedDocs", editFlag);
		Map<String, String> dirs = docVO.getDirMap();

		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, interviewProcess.getInterviewProcessId(),
					"InterviewProcess", "uploadedDocs");
			docVO.setDirStruct(dirStucture);
		}

		documentBL.saveUploadDocuments(docVO);
	}

	public Map<Integer, String> status() {
		Map<Integer, String> types = new HashMap<Integer, String>();
		types.put(1, "Scheduled");
		types.put(2, "Holded");
		types.put(3, "Completed");
		return types;
	}

	public Map<Integer, String> rounds() {
		Map<Integer, String> types = new HashMap<Integer, String>();
		types.put(1, "First");
		types.put(2, "Second");
		types.put(3, "Third");
		types.put(4, "Fourth");
		types.put(5, "Fifth");
		types.put(6, "Sixth");
		types.put(7, "Seventh");
		types.put(8, "Eight");
		types.put(9, "Nineth");
		types.put(10, "Tenth");
		types.put(100, "Final");
		return types;
	}

	public Map<Integer, String> rattings() {
		Map<Integer, String> types = new HashMap<Integer, String>();
		types.put(Constants.HR.Rattings.Insufficient.getCode(), "Insufficient");
		types.put(Constants.HR.Rattings.Ok.getCode(), "Ok");
		types.put(Constants.HR.Rattings.Good.getCode(), "Good");
		types.put(Constants.HR.Rattings.VeryGood.getCode(), "Very Good");
		types.put(Constants.HR.Rattings.Excellent.getCode(), "Excellent");
		return types;
	}

	public InterviewVO calculateScoreAndRating(
			List<InterviewProcess> interviewProcesses, InterviewVO interviewVO) {

		if (interviewProcesses != null && interviewProcesses.size() > 0) {
			Integer rate = 0;
			Double score = 0.0;
			for (InterviewProcess interviewProcess : interviewProcesses) {
				if (interviewProcess.getRating() != null)
					rate += (int) interviewProcess.getRating();
				if (interviewProcess.getScore() != null)
					score += Double.valueOf(interviewProcess.getScore());
			}
			Integer average = Math.abs((rate / interviewProcesses.size()));
			DecimalFormat df = new DecimalFormat("#");
			interviewVO.setAverateRateing(df.format(average));
			interviewVO.setTotalScore(score + "");

		} else {
			interviewVO.setAverateRateing("0");
			interviewVO.setTotalScore("0");
		}

		return interviewVO;
	}

	public InterviewService getInterviewService() {
		return interviewService;
	}

	public void setInterviewService(InterviewService interviewService) {
		this.interviewService = interviewService;
	}

	public InterviewProcessService getInterviewProcessService() {
		return interviewProcessService;
	}

	public void setInterviewProcessService(
			InterviewProcessService interviewProcessService) {
		this.interviewProcessService = interviewProcessService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public LocationBL getLocationBL() {
		return locationBL;
	}

	public void setLocationBL(LocationBL locationBL) {
		this.locationBL = locationBL;
	}

	public CandidateBL getCandidateBL() {
		return candidateBL;
	}

	public void setCandidateBL(CandidateBL candidateBL) {
		this.candidateBL = candidateBL;
	}

	public JobAssignmentBL getJobAssignmentBL() {
		return jobAssignmentBL;
	}

	public void setJobAssignmentBL(JobAssignmentBL jobAssignmentBL) {
		this.jobAssignmentBL = jobAssignmentBL;
	}

	public QuestionBankBL getQuestionBankBL() {
		return questionBankBL;
	}

	public void setQuestionBankBL(QuestionBankBL questionBankBL) {
		this.questionBankBL = questionBankBL;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public SystemBL getSystemBL() {
		return systemBL;
	}

	public void setSystemBL(SystemBL systemBL) {
		this.systemBL = systemBL;
	}
}
