package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.WorkingShift;
import com.aiotech.aios.hr.domain.entity.vo.WorkingShiftVO;
import com.aiotech.aios.hr.service.WorkingShiftService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class WorkingShiftBL {
	private WorkingShiftService workingShiftService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	
	public void saveWorkingShift(WorkingShift workingShift) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
		.getSession();
		User user = (User) sessionObj.get("USER");
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (workingShift != null
				&& workingShift.getWorkingShiftId() != null
				&& workingShift.getWorkingShiftId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		workingShiftService.saveWorkingShift(workingShift);
		


		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
		WorkingShift.class.getSimpleName(),
		workingShift.getWorkingShiftId(), user,
		workflowDetailVo);
	}

	public void deleteWorkingShift(Long workingShiftId)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				WorkingShift.class.getSimpleName(), workingShiftId,
				user, workflowDetailVo);
	}
	
	public void doWorkingShiftBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		WorkingShift workingShift = workingShiftService
		.getWorkingShiftInfo(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			workingShiftService.deleteWorkingShift(workingShift);
		} else {
		}
	}

	public List<WorkingShiftVO> convertEntitiesTOVOs(
			List<WorkingShift> workingShifts) throws Exception {

		List<WorkingShiftVO> tosList = new ArrayList<WorkingShiftVO>();

		for (WorkingShift pers : workingShifts) {
			tosList.add(convertEntityToVO(pers));
		}

		return tosList;
	}

	public WorkingShiftVO convertEntityToVO(WorkingShift workingShift) throws Exception {
		WorkingShiftVO workingShiftVO = new WorkingShiftVO();
		BeanUtils.copyProperties(workingShiftVO, workingShift);
		workingShiftVO.setBreakHours(workingShift.getBreakHours());
		workingShiftVO.setBufferTime(workingShift.getBufferTime());
		workingShiftVO.setEndTime(workingShift.getEndTime());
		workingShiftVO.setImplementation(workingShift.getImplementation());
		workingShiftVO.setIrregularDay(workingShift.getIrregularDay());
		workingShiftVO.setIsActive(workingShift.getIsActive());
		workingShiftVO.setIsDefault(workingShift.getIsDefault());
		workingShiftVO.setJobShifts(workingShift.getJobShifts());
		workingShiftVO.setLateAttendance(workingShift.getLateAttendance());
		workingShiftVO.setSeviorLateAttendance(workingShift
				.getSeviorLateAttendance());
		workingShiftVO.setStartTime(workingShift.getStartTime());
		workingShiftVO.setWorkingShiftId(workingShift.getWorkingShiftId());
		workingShiftVO.setWorkingShiftName(workingShift.getWorkingShiftName());

		if (workingShift.getBreakHours() != null)
			workingShiftVO.setBreakHoursStr(DateFormat
					.convertTimeToString(workingShift.getBreakHours() + ""));

		if (workingShift.getBufferTime() != null)
			workingShiftVO.setBufferTimeStr(DateFormat
					.convertTimeToString(workingShift.getBufferTime() + ""));

		if (workingShift.getStartTime() != null)
			workingShiftVO.setStartTimeStr(DateFormat
					.convertTimeToString(workingShift.getStartTime() + ""));

		if (workingShift.getEndTime() != null)
			workingShiftVO.setEndTimeStr(DateFormat
					.convertTimeToString(workingShift.getEndTime() + ""));

		if (workingShift.getLateAttendance() != null)
			workingShiftVO
					.setLateAttendanceStr(DateFormat
							.convertTimeToString(workingShift
									.getLateAttendance() + ""));

		if (workingShift.getSeviorLateAttendance() != null)
			workingShiftVO.setSeviorLateAttendanceStr(DateFormat
					.convertTimeToString(workingShift.getSeviorLateAttendance()
							+ ""));

		if (workingShift.getIsActive() != null
				&& workingShift.getIsActive() == true) {
			workingShiftVO.setIsActiveStr("YES");

		} else {
			workingShiftVO.setIsActiveStr("NO");

		}
		if (workingShift.getIsDefault() != null
				&& workingShift.getIsDefault() == true) {
			workingShiftVO.setIsDefaultStr("YES");

		} else {
			workingShiftVO.setIsDefaultStr("NO");

		}

		if (workingShift.getBreakStart() != null)
			workingShiftVO.setBreakStartStr(DateFormat
					.convertTimeToString(workingShift.getBreakStart() + ""));

		if (workingShift.getBreakEnd() != null)
			workingShiftVO.setBreakEndStr(DateFormat
					.convertTimeToString(workingShift.getBreakEnd() + ""));

		return workingShiftVO;
	}

	public WorkingShiftService getWorkingShiftService() {
		return workingShiftService;
	}

	public void setWorkingShiftService(WorkingShiftService workingShiftService) {
		this.workingShiftService = workingShiftService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

}
