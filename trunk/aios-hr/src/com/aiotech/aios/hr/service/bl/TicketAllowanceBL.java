package com.aiotech.aios.hr.service.bl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.TicketAllowance;
import com.aiotech.aios.hr.domain.entity.vo.TicketAllowanceVO;
import com.aiotech.aios.hr.service.TicketAllowanceService;
import com.aiotech.aios.system.service.bl.LookupMasterBL;

public class TicketAllowanceBL {

	private LookupMasterBL lookupMasterBL;
	private TicketAllowanceService ticketAllowanceService;
	private PayPeriodBL payPeriodBL;

	public TicketAllowanceVO convertEntityToVO(TicketAllowance ticketAllowance) {
		TicketAllowanceVO ticketAllowanceVO = new TicketAllowanceVO();
		try {
			// Copy Entity class properties value into VO properties.
			BeanUtils.copyProperties(ticketAllowanceVO, ticketAllowance);
			if (ticketAllowance.getPurchaseDate() != null)
				ticketAllowanceVO.setPurchaseDateView(DateFormat
						.convertDateToString(ticketAllowance.getPurchaseDate()
								+ ""));
			ticketAllowanceVO.setFromDateView(DateFormat
					.convertDateToString(ticketAllowance.getFromDate() + ""));
			ticketAllowanceVO.setToDateView(DateFormat
					.convertDateToString(ticketAllowance.getToDate() + ""));

			if (ticketAllowance.getIsFinanceImpact() != null
					&& ticketAllowance.getIsFinanceImpact())
				ticketAllowanceVO.setIsFinanceView("YES");
			else
				ticketAllowanceVO.setIsFinanceView("NO");

			if (ticketAllowance.getLookupDetailByFromDestination() != null)
				ticketAllowanceVO.setFrom(ticketAllowance
						.getLookupDetailByFromDestination().getDisplayName());
			if (ticketAllowance.getLookupDetailByToDestination() != null)
				ticketAllowanceVO.setTo(ticketAllowance
						.getLookupDetailByToDestination().getDisplayName());

			if (ticketAllowance.getLookupDetailByAirline() != null)
				ticketAllowanceVO.setAirline(ticketAllowance
						.getLookupDetailByAirline().getDisplayName());

			if (ticketAllowance.getLookupDetailByAgency() != null)
				ticketAllowanceVO.setAgency(ticketAllowance
						.getLookupDetailByAgency().getDisplayName());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ticketAllowanceVO;
	}

	public String saveTicketAllowance(TicketAllowance ticketAllowance) {
		String returnStatus = "SUCCESS";
		try {

			ticketAllowanceService.saveTicketAllowance(ticketAllowance);

		} catch (Exception e) {
			returnStatus = "ERROR";
		}
		return returnStatus;
	}

	public String deleteTicketAllowance(TicketAllowance ticketAllowance) {
		String returnStatus = "SUCCESS";
		try {
			ticketAllowanceService.deleteTicketAllowance(ticketAllowance);
		} catch (Exception e) {
			returnStatus = "ERROR";
		}
		return returnStatus;
	}
	
	public Map<String, String> travelClass() {
		Map<String, String> types = new HashMap<String, String>();
		types.put("B", "Business");
		types.put("E", "Economy");
		types.put("C", "Coral Economy");
		types.put("O", "Other");
		return types;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public PayPeriodBL getPayPeriodBL() {
		return payPeriodBL;
	}

	public void setPayPeriodBL(PayPeriodBL payPeriodBL) {
		this.payPeriodBL = payPeriodBL;
	}

	public TicketAllowanceService getTicketAllowanceService() {
		return ticketAllowanceService;
	}

	public void setTicketAllowanceService(
			TicketAllowanceService ticketAllowanceService) {
		this.ticketAllowanceService = ticketAllowanceService;
	}
}
