package com.aiotech.aios.hr.service;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.CandidateOffer;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class CandidateOfferService {
	public AIOTechGenericDAO<CandidateOffer> candidateOfferDAO;
	
	public List<CandidateOffer> getCandidateOfferByStatus(
			Implementation implementation, Byte status) {
		return candidateOfferDAO.findByNamedQuery("getCandidateOfferByStatus",
				implementation, status);
	}
	
	public List<CandidateOffer> getCandidateOfferByStatuses(
			Implementation implementation, byte status1,byte status2) {
		return candidateOfferDAO.findByNamedQuery("getCandidateOfferByStatuses",
				implementation, status1,status2);
	}
	
	public CandidateOffer getCandiateOfferById(Long candidateOfferId) {
		List<CandidateOffer> candidateOffers = candidateOfferDAO
				.findByNamedQuery("getCandiateOfferById", candidateOfferId);
		if (candidateOffers != null && candidateOffers.size() > 0)
			return candidateOffers.get(0);
		else
			return null;
	}
	
	public void saveCandidateOffer(CandidateOffer candidateOffer){
		candidateOfferDAO.saveOrUpdate(candidateOffer);
	}
	public AIOTechGenericDAO<CandidateOffer> getCandidateOfferDAO() {
		return candidateOfferDAO;
	}

	public void setCandidateOfferDAO(
			AIOTechGenericDAO<CandidateOffer> candidateOfferDAO) {
		this.candidateOfferDAO = candidateOfferDAO;
	}
}
