package com.aiotech.aios.hr.service.bl;

import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.service.CmpDeptLocService;
import com.aiotech.aios.hr.service.CompanyService;
import com.aiotech.aios.hr.service.DepartmentService;
import com.aiotech.aios.hr.service.LocationService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;

public class CmpDeptLocBL {
	private LocationService locationService;
	private DepartmentService departmentService;
	private CompanyService companyService;
	private CmpDeptLocService cmpDeptLocService;
	private SystemService systemService;
	private LocationBL locationBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	
	public void doCmpDeptLocBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		CmpDeptLoc setup=cmpDeptLocService.getBranchAndLocationInfo(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			if(setup!=null){
				try{
					setup=cmpDeptLocService.getCompanySetup(setup.getCmpDeptLocId());
					cmpDeptLocService.deleteSetup(setup);//Save Department first
				}catch(org.hibernate.exception.ConstraintViolationException e){
					setup.setIsActive(false);
					try{
						cmpDeptLocService.saveCmpDeptLocation(setup);
					}catch (Exception ex) {
					}
				} catch (Exception e) {
					setup.setIsActive(false);
					try{
						cmpDeptLocService.saveCmpDeptLocation(setup);
					}catch (Exception ex) {

					}
				}
				
			}
		} else {
		}
	}
	public LocationService getLocationService() {
		return locationService;
	}
	public void setLocationService(LocationService locationService) {
		this.locationService = locationService;
	}
	public DepartmentService getDepartmentService() {
		return departmentService;
	}
	public void setDepartmentService(DepartmentService departmentService) {
		this.departmentService = departmentService;
	}
	public CompanyService getCompanyService() {
		return companyService;
	}
	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}
	public CmpDeptLocService getCmpDeptLocService() {
		return cmpDeptLocService;
	}
	public void setCmpDeptLocService(
			CmpDeptLocService cmpDeptLocService) {
		this.cmpDeptLocService = cmpDeptLocService;
	}
	public SystemService getSystemService() {
		return systemService;
	}
	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}
	public LocationBL getLocationBL() {
		return locationBL;
	}
	public void setLocationBL(LocationBL locationBL) {
		this.locationBL = locationBL;
	}
	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}
	public CommentBL getCommentBL() {
		return commentBL;
	}
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
