package com.aiotech.aios.hr.service.bl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.PayPeriod;
import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;
import com.aiotech.aios.hr.domain.entity.vo.PayPeriodVO;
import com.aiotech.aios.hr.service.PayPeriodService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class PayPeriodBL {
	private PayPeriodService payPeriodService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public PayPeriodVO convertEntityIntoVO(PayPeriod payPeriod)
			throws Exception {
		PayPeriodVO payPeriodVO = new PayPeriodVO();

		// Copy Interview class properties value into VO properties.
		BeanUtils.copyProperties(payPeriodVO, payPeriod);
		payPeriodVO.setStartDate(DateFormat.convertDateToString(payPeriod
				.getYearStartDate() + ""));
		payPeriodVO.setEndDate(DateFormat.convertDateToString(payPeriod
				.getYearEndDate() + ""));
		return payPeriodVO;
	}

	public String savePayPeriod(PayPeriod payPeriod,
			List<PayPeriodTransaction> payPeriodTransactions) {
		String returnStatus = "SUCCESS";
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			payPeriod.setIsApprove((byte) WorkflowConstants.Status.Published
					.getCode());
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (payPeriod != null && payPeriod.getPayPeriodId() != null
					&& payPeriod.getPayPeriodId() > 0) {

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			payPeriodService.savePayPeriod(payPeriod);
			payPeriodService.saveAllPayPeriodTransaciton(payPeriodTransactions);

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					PayPeriod.class.getSimpleName(),
					payPeriod.getPayPeriodId(), user, workflowDetailVo);
		} catch (Exception e) {
			returnStatus = "ERROR";
		}
		return returnStatus;
	}

	public String deletePayPeriod(PayPeriod payPeriod,
			List<PayPeriodTransaction> payPeriodTransactions) {
		String returnStatus = "SUCCESS";
		try {
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
							.getCode());
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					PayPeriod.class.getSimpleName(),
					payPeriod.getPayPeriodId(), user, workflowDetailVo);
		} catch (Exception e) {
			returnStatus = "ERROR";
		}
		return returnStatus;
	}

	public void doPayPeriodBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		PayPeriod payPeriod = payPeriodService.getPayPeriodById(recordId);
		List<PayPeriodTransaction> payPeriodTransactions = new ArrayList<PayPeriodTransaction>(
				payPeriod.getPayPeriodTransactions());
		if (workflowDetailVO.isDeleteFlag()) {
			payPeriodService
					.deleteAllPayPeriodTransaciton(payPeriodTransactions);
			payPeriodService.deletePayPeriod(payPeriod);
		} else {
		}
	}

	public List<PayPeriodVO> convertTransactionEntityIntoVO(
			List<PayPeriodTransaction> transactions) throws Exception {
		List<PayPeriodVO> payPeriodVOs = null;
		PayPeriodVO payPeriodVO = null;
		if (transactions != null && transactions.size() > 0) {
			payPeriodVOs = new ArrayList<PayPeriodVO>();
			for (PayPeriodTransaction payPeriodTransaction : transactions) {
				payPeriodVO = new PayPeriodVO();
				payPeriodVO.setPayPeriodTransactionId(payPeriodTransaction
						.getPayPeriodTransactionId());
				payPeriodVO.setStartDate(DateFormat
						.convertDateToString(payPeriodTransaction
								.getStartDate() + ""));
				payPeriodVO.setEndDate(DateFormat
						.convertDateToString(payPeriodTransaction.getEndDate()
								+ ""));
				payPeriodVO.setPayMonth(payPeriodTransaction.getPayMonth());
				payPeriodVO.setPayWeek(payPeriodTransaction.getPayWeek());
				if(payPeriodTransaction.getIsFreeze()!=null)
					payPeriodVO.setFreeze(payPeriodTransaction.getIsFreeze());
				payPeriodVOs.add(payPeriodVO);
			}
		}
		return payPeriodVOs;
	}

	public List<PayPeriodVO> generatePeriodTransaction(PayPeriodVO payPeriodVO) {

		List<PayPeriodVO> payPeriodTransactions = null;
		try {
			PayPeriodVO payPeriodTransaction = null;
			payPeriodTransactions = new ArrayList<PayPeriodVO>();
			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");

			DateTimeFormatter dtf = DateTimeFormat.forPattern("dd-MMM-yyyy");

			// Month calculations
			LocalDate date1 = new LocalDate(dtf.parseLocalDate(payPeriodVO
					.getStartDate()));
			LocalDate date2 = new LocalDate(dtf.parseLocalDate(payPeriodVO
					.getEndDate()));
			int i = 1;
			while (date1.isBefore(date2)) {
				payPeriodTransaction = new PayPeriodVO();
				payPeriodTransaction.setStartDate(df.format(date1.toDate()));
				if (payPeriodVO.getIsDefault()!=null && !payPeriodVO.getIsDefault()){
					payPeriodTransaction.setPayMonth(date1
							.toString("MMMM,yyyy"));
					date1 = date1.plus(Period.months(1));
				}else{
					date1 = date1.plus(Period.months(1));
					payPeriodTransaction.setPayMonth(date1
							.toString("MMMM,yyyy"));
				}
				payPeriodTransaction.setEndDate(df.format(date1.minusDays(1)
						.toDate()));
				payPeriodTransaction.setPayWeek(null);
				payPeriodTransaction.setOrder(i);
				payPeriodTransactions.add(payPeriodTransaction);

				i++;
			}

			// week calculations
			LocalDate startdate1 = new LocalDate(dtf.parseLocalDate(payPeriodVO
					.getStartDate()));
			LocalDate enddate2 = new LocalDate(dtf.parseLocalDate(payPeriodVO
					.getEndDate()));
			i = 1;
			while (startdate1.isBefore(enddate2)) {
				payPeriodTransaction = new PayPeriodVO();
				payPeriodTransaction
						.setStartDate(df.format(startdate1.toDate()));
				if (payPeriodVO.getIsDefault()!=null && !payPeriodVO.getIsDefault()){
					payPeriodTransaction.setPayWeek(startdate1
							.withYear(startdate1.getYear()).weekOfWeekyear()
							.get());
					startdate1 = startdate1.plus(Period.weeks(1));
				}else{
					startdate1 = startdate1.plus(Period.weeks(1));
					payPeriodTransaction.setPayWeek(startdate1
							.withYear(startdate1.getYear()).weekOfWeekyear()
							.get());
						
				}
				payPeriodTransaction.setEndDate(df.format(startdate1.minusDays(
						1).toDate()));
				payPeriodTransaction.setPayMonth(null);
				payPeriodTransaction.setOrder(i);
				payPeriodTransactions.add(payPeriodTransaction);
				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return payPeriodTransactions;
	}

	public PayPeriodTransaction findPayPeriod(PayPeriodVO payPeriodVO) {
		PayPeriodTransaction payPeriodTransaction = null;
		try {
			if (payPeriodVO != null) {
				if (payPeriodVO.getPayPeriodTransactionId() != null
						&& payPeriodVO.getPayPeriodTransactionId() > 0) {
					payPeriodTransaction = payPeriodService
							.getPayPeriodTransactionById(payPeriodVO
									.getPayPeriodTransactionId());
				} else {
					if (payPeriodVO.getPayPolicy() != null
							&& Constants.HR.PayPolicy.Monthly.getCode() == payPeriodVO
									.getPayPolicy())
						payPeriodVO.setMonthly(true);
					else if (payPeriodVO.getPayPolicy() != null
							&& Constants.HR.PayPolicy.Weekly.getCode() == payPeriodVO
									.getPayPolicy())
						payPeriodVO.setWeekly(true);
					else if (payPeriodVO.getPayPolicy() != null
							&& Constants.HR.PayPolicy.Yearly.getCode() == payPeriodVO
									.getPayPolicy()) {
						payPeriodVO.setYearly(true);

					}
					List<PayPeriodTransaction> payPeriodTransactions = payPeriodService
							.findPayPeriod(payPeriodVO);
					if (payPeriodTransactions != null
							&& payPeriodTransactions.size() > 0) {
						payPeriodTransaction = payPeriodTransactions.get(0);
					}
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return payPeriodTransaction;
	}

	public List<PayPeriodTransaction> findActivePayPeriods(
			PayPeriodVO payPeriodVO) {
		List<PayPeriodTransaction> payPeriodTransactions = null;
		try {
			if (payPeriodVO != null) {
				if (payPeriodVO.getPayPolicy() != null
						&& (byte) Constants.HR.PayPolicy.Monthly.getCode() == (byte) payPeriodVO
								.getPayPolicy())
					payPeriodVO.setMonthly(true);
				else if (payPeriodVO.getPayPolicy() != null
						&& (byte) Constants.HR.PayPolicy.Weekly.getCode() == (byte) payPeriodVO
								.getPayPolicy())
					payPeriodVO.setWeekly(true);
				else if (payPeriodVO.getPayPolicy() != null
						&& (byte) Constants.HR.PayPolicy.Yearly.getCode() == (byte) payPeriodVO
								.getPayPolicy()) {
					payPeriodVO.setYearly(true);

				}
				payPeriodTransactions = payPeriodService
						.findPayPeriod(payPeriodVO);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return payPeriodTransactions;
	}

	public PayPeriodService getPayPeriodService() {
		return payPeriodService;
	}

	public void setPayPeriodService(PayPeriodService payPeriodService) {
		this.payPeriodService = payPeriodService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
