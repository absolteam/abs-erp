package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.QuestionBank;
import com.aiotech.aios.hr.domain.entity.QuestionInfo;
import com.aiotech.aios.hr.domain.entity.vo.QuestionBankVO;
import com.aiotech.aios.hr.service.QuestionBankService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class QuestionBankBL {

	private QuestionBankService questionBankService;
	private LookupMasterBL lookupMasterBL;
	private DocumentBL documentBL;
	private ImageBL imageBL;
	private DirectoryBL directoryBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public Map<Integer, String> questionPatterns() {
		Map<Integer, String> types = new HashMap<Integer, String>();
		types.put(1, "Printable Questions");
		types.put(2, "Online Derivation");
		types.put(3, "Over Through Phone Call");
		return types;
	}

	@SuppressWarnings("unused")
	public List<QuestionBankVO> convertAllEntityToVO(
			List<QuestionBank> questionBanks) throws Exception {
		List<QuestionBankVO> questionBankVOs = null;
		if (questionBanks != null && questionBanks.size() > 0) {
			questionBankVOs = new ArrayList<QuestionBankVO>();
			for (QuestionBank questionBank : questionBanks) {
				questionBankVOs.add(convertEntityIntoVO(questionBank));
			}
		}
		return questionBankVOs;
	}

	public QuestionBankVO convertEntityIntoVO(QuestionBank questionBank)
			throws Exception {
		QuestionBankVO questionBankVO = new QuestionBankVO();
		// Copy QuestionBank class properties value into VO properties.
		BeanUtils.copyProperties(questionBankVO, questionBank);
		return questionBankVO;
	}

	public List<QuestionInfo> getQuestionInfoLineDetail(String childLineDetail) {
		List<QuestionInfo> questionInfos = new ArrayList<QuestionInfo>();
		if (childLineDetail != null && !childLineDetail.equals("")) {
			String[] recordArray = new String[childLineDetail.split("#%#").length];
			recordArray = childLineDetail.split("#%#");
			for (String record : recordArray) {
				QuestionInfo question = new QuestionInfo();
				String[] dataArray = new String[record.split("@%@").length];
				dataArray = record.split("@%@");
				if (!dataArray[0].equals("-1")
						&& !dataArray[0].trim().equals(""))
					question.setQuestionInfoId(Long.parseLong(dataArray[0]));
				if (!dataArray[1].trim().equals(""))
					question.setQuestionTitle(dataArray[1]);
				if (!dataArray[2].trim().equals(""))
					question.setQuestionContent(dataArray[2]);

				questionInfos.add(question);
			}
		}
		return questionInfos;
	}

	public void saveQuestionBank(QuestionBank questionBank,
			QuestionBankVO questionBankVO) {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			questionBank.setIsApprove((byte) WorkflowConstants.Status.Published
					.getCode());
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (questionBank != null
					&& questionBank.getQuestionBankId() != null
					&& questionBank.getQuestionBankId() > 0) {

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			if (questionBank != null
					&& questionBank.getQuestionBankId() != null
					&& questionBank.getQuestionBankId() > 0) {

				List<QuestionInfo> fromDbList = new ArrayList<QuestionInfo>(
						questionBankVO.getQuestionInfos());
				List<QuestionInfo> deleteList = findDeleteRecords(
						questionBankVO.getQuestionInfoList(), fromDbList);
				if (deleteList != null)
					questionBankService.deleteQuestionInfos(deleteList);

				if (questionBankVO.getQuestionInfoList() != null
						&& questionBankVO.getQuestionInfoList().size() > 0)
					for (QuestionInfo questionInfo : questionBankVO
							.getQuestionInfoList()) {
						questionInfo.setQuestionBank(questionBank);
					}

				questionBankService.saveQuestionBank(questionBank);
				questionBankService.saveQuestionInfos(questionBankVO
						.getQuestionInfoList());
				saveDocuments(questionBank, true);

			} else {
				questionBankService.saveQuestionBank(questionBank);
				saveDocuments(questionBank, false);
			}
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					QuestionBank.class.getSimpleName(),
					questionBank.getQuestionBankId(), user, workflowDetailVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteQuestionBank(QuestionBank questionBank) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				QuestionBank.class.getSimpleName(),
				questionBank.getQuestionBankId(), user, workflowDetailVo);
	}

	public void doQuestionBankBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		QuestionBank questionBank = this.getQuestionBankService()
				.getQuestionBankById(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			if (questionBank.getQuestionInfos() != null)
				questionBankService
						.deleteQuestionInfos(new ArrayList<QuestionInfo>(
								questionBank.getQuestionInfos()));

			questionBankService.deleteQuestionBank(questionBank);
		} else {
		}
	}

	public void saveDocuments(QuestionBank questionBank, boolean editFlag)
			throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(questionBank,
				"uploadedDocs", editFlag);
		Map<String, String> dirs = docVO.getDirMap();

		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, questionBank.getQuestionBankId(), "QuestionBank",
					"uploadedDocs");
			docVO.setDirStruct(dirStucture);
		}

		documentBL.saveUploadDocuments(docVO);
	}

	public List<QuestionInfo> findDeleteRecords(List<QuestionInfo> fromScreen,
			List<QuestionInfo> fromDB) {
		List<QuestionInfo> toDeleteOrInactive = new ArrayList<QuestionInfo>();

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		for (QuestionInfo info : fromScreen) {
			if (info.getQuestionInfoId() != null
					&& info.getQuestionInfoId() > 0)
				listOne.add(info.getQuestionInfoId());
		}
		for (QuestionInfo info : fromDB) {
			if (info.getQuestionInfoId() != null
					&& info.getQuestionInfoId() > 0)
				listTwo.add(info.getQuestionInfoId());
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);

		// Delete Process
		for (Long lng : different) {
			for (QuestionInfo info : fromDB) {
				if (lng != null && info.getQuestionInfoId().equals(lng)) {
					toDeleteOrInactive.add(info);
				}
			}
		}
		return toDeleteOrInactive;
	}

	public QuestionBankService getQuestionBankService() {
		return questionBankService;
	}

	public void setQuestionBankService(QuestionBankService questionBankService) {
		this.questionBankService = questionBankService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
