package com.aiotech.aios.hr.service;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.QuestionBank;
import com.aiotech.aios.hr.domain.entity.QuestionInfo;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class QuestionBankService {

	public AIOTechGenericDAO<QuestionBank> questionBankDAO;
	public AIOTechGenericDAO<QuestionInfo> questionInfoDAO;
	
	public List<QuestionBank> getAllQuestionBank(Implementation implementation){
		return questionBankDAO.findByNamedQuery("getAllQuestionBank",
				implementation,(byte)WorkflowConstants.Status.Published.getCode());
	}
	
	public List<QuestionBank> getAllActiveQuestionBank(Implementation implementation){
		return questionBankDAO.findByNamedQuery("getAllActiveQuestionBank",
				implementation);
	}
	
	public List<QuestionBank> getAllValidQuestionBank(Long designationId){
		return questionBankDAO.findByNamedQuery("getAllValidQuestionBank",
				designationId);
	}
	
	
	public QuestionBank getQuestionBankById(Long questionBankId) {
		List<QuestionBank> questionBanks = questionBankDAO.findByNamedQuery(
				"getQuestionBankById", questionBankId);
		if (questionBanks != null && questionBanks.size() > 0)
			return questionBanks.get(0);
		else
			return null;
	}
	
	public void saveQuestionBank(QuestionBank questionBank){
		questionBankDAO.saveOrUpdate(questionBank);
	}
	
	public void deleteQuestionBank(QuestionBank questionBank){
		questionBankDAO.delete(questionBank);
	}
	
	public void saveQuestionInfos(List<QuestionInfo> questionInfos){
		questionInfoDAO.saveOrUpdateAll(questionInfos);
	}
	
	public void deleteQuestionInfos(List<QuestionInfo> questionInfos){
		questionInfoDAO.deleteAll(questionInfos);
	}
	
	public AIOTechGenericDAO<QuestionInfo> getQuestionInfoDAO() {
		return questionInfoDAO;
	}
	public void setQuestionInfoDAO(AIOTechGenericDAO<QuestionInfo> questionInfoDAO) {
		this.questionInfoDAO = questionInfoDAO;
	}
	public AIOTechGenericDAO<QuestionBank> getQuestionBankDAO() {
		return questionBankDAO;
	}
	public void setQuestionBankDAO(AIOTechGenericDAO<QuestionBank> questionBankDAO) {
		this.questionBankDAO = questionBankDAO;
	}
}
