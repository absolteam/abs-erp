package com.aiotech.aios.hr.service.bl;

import java.util.Map;

import com.aiotech.aios.hr.domain.entity.PersonBank;
import com.aiotech.aios.hr.service.PersonBankService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class PersonBankBL {
	private PersonBankService personBankService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public void savePersonBank(PersonBank personBank) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		personBank.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (personBank != null && personBank.getPersonBankId() != null
				&& personBank.getPersonBankId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		personBankService.savePersonBank(personBank);

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				PersonBank.class.getSimpleName(), personBank.getPersonBankId(),
				user, workflowDetailVo);

	}

	public void deletePersonBank(Long personBankId) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				PersonBank.class.getSimpleName(), personBankId, user,
				workflowDetailVo);
	}

	public void doPersonBankBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		PersonBank personBank = personBankService.getPersonBankInfo(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			personBankService.deletePersonBank(personBank);
		} else {
		}
	}

	public PersonBankService getPersonBankService() {
		return personBankService;
	}

	public void setPersonBankService(PersonBankService personBankService) {
		this.personBankService = personBankService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

}
