package com.aiotech.aios.hr.service;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.OpenPosition;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class OpenPositionService {
	public AIOTechGenericDAO<OpenPosition> openPositionDAO;
	
	public List<OpenPosition> getAllOpenPosition(Implementation implementation){
		return openPositionDAO.findByNamedQuery("getAllOpenPosition",
				implementation);
	}
	
	public void saveOpenPosition(OpenPosition openPosition){
		openPositionDAO.saveOrUpdate(openPosition);
	}
	
	public OpenPosition getOpenPositionSameCombination(Long designationId,
			Long branchId) {
		List<OpenPosition> openPositions = openPositionDAO.findByNamedQuery(
				"getOpenPositionSameCombination", designationId,branchId);
		
		if(openPositions!=null && openPositions.size()>0)
			return openPositions.get(0);
		else
			return null;
	}
	
	public OpenPosition getOpenPositionById(Long openPositionId) {
		List<OpenPosition> openPositions = openPositionDAO.findByNamedQuery(
				"getOpenPositionById", openPositionId);
		
		if(openPositions!=null && openPositions.size()>0)
			return openPositions.get(0);
		else
			return null;
	}
	
	public void deleteOpenPosition(OpenPosition openPosition){
		openPositionDAO.delete(openPosition);
	}

	public AIOTechGenericDAO<OpenPosition> getOpenPositionDAO() {
		return openPositionDAO;
	}

	public void setOpenPositionDAO(AIOTechGenericDAO<OpenPosition> openPositionDAO) {
		this.openPositionDAO = openPositionDAO;
	}
}
