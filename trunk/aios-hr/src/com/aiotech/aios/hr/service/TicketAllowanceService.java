package com.aiotech.aios.hr.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.hr.domain.entity.TicketAllowance;
import com.aiotech.aios.hr.domain.entity.vo.TicketAllowanceVO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class TicketAllowanceService{
public AIOTechGenericDAO<TicketAllowance> ticketAllowanceDAO;

public List<TicketAllowance> getAllTicketAllowanceByCriteria(
			TicketAllowanceVO vO) {
		Criteria jobPayrollCriteria = ticketAllowanceDAO.createCriteria();

		jobPayrollCriteria.createAlias("jobAssignment", "ja");
		jobPayrollCriteria.createAlias("lookupDetailByToDestination", "destTo",
				CriteriaSpecification.LEFT_JOIN);
		jobPayrollCriteria.createAlias("lookupDetailByFromDestination",
				"fromDest", CriteriaSpecification.LEFT_JOIN);
		jobPayrollCriteria.createAlias("lookupDetailByAirline", "airline",
				CriteriaSpecification.LEFT_JOIN);
		jobPayrollCriteria.createAlias("lookupDetailByAgency", "agency",
				CriteriaSpecification.LEFT_JOIN);

		jobPayrollCriteria.createAlias("jobPayrollElement", "jpa");
		jobPayrollCriteria.createAlias("jpa.payrollElementByPayrollElementId",
				"pe");
		jobPayrollCriteria.createAlias("jpa.payrollElementByPercentageElement",
				"percentage", CriteriaSpecification.LEFT_JOIN);
		if (vO.getPayPeriodTransaction() != null
				&& vO.getPayPeriodTransaction().getStartDate() != null) {
			jobPayrollCriteria.add(Restrictions.conjunction().add(
					Restrictions.ge("fromDate", vO.getPayPeriodTransaction()
							.getStartDate())));
			jobPayrollCriteria.add(Restrictions.conjunction().add(
					Restrictions.le("toDate", vO.getPayPeriodTransaction()
							.getEndDate())));
		}

		if (vO.getJobAssignmentId() != null) {
			jobPayrollCriteria.add(Restrictions.eq("ja.jobAssignmentId",
					vO.getJobAssignmentId()));
		}

		if (vO.getJobPayrollElementId() != null) {
			jobPayrollCriteria.add(Restrictions.eq("jpa.jobPayrollElementId",
					vO.getJobPayrollElementId()));
		}

		return jobPayrollCriteria.list();
}

public List<TicketAllowance> getAllTicketAllowance(
		Implementation implementation) {
	return ticketAllowanceDAO.findByNamedQuery("getAllTicketAllowance",
			implementation);
}

public TicketAllowance getTicketAllowanceById(Long allowanceId) {
	List<TicketAllowance> allowances = ticketAllowanceDAO
			.findByNamedQuery("getTicketAllowanceById", allowanceId);
	if (allowances != null && allowances.size() > 0)
		return allowances.get(0);
	else
		return null;
}

public void saveTicketAllowance(TicketAllowance ticketAllowance) {
	ticketAllowanceDAO.saveOrUpdate(ticketAllowance);
}

public void deleteTicketAllowance(TicketAllowance ticketAllowance) {
	ticketAllowanceDAO.delete(ticketAllowance);
}

public AIOTechGenericDAO<TicketAllowance> getTicketAllowanceDAO() {
	return ticketAllowanceDAO;
}

public void setTicketAllowanceDAO(
		AIOTechGenericDAO<TicketAllowance> ticketAllowanceDAO) {
	this.ticketAllowanceDAO = ticketAllowanceDAO;
}


}
