package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Department;
import com.aiotech.aios.hr.domain.entity.Location;
import com.aiotech.aios.hr.service.CmpDeptLocationService;
import com.aiotech.aios.hr.service.CompanyService;
import com.aiotech.aios.hr.service.DepartmentService;
import com.aiotech.aios.hr.service.LocationService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class DepartmentBL {

	private LocationService locationService;
	private DepartmentService departmentService;
	private CompanyService companyService;
	private CmpDeptLocationService cmpDeptLocationService;
	private SystemService systemService;
	private LocationBL locationBL;
	private CmpDeptLocBL cmpDeptLocBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	
	public String saveCompanySetup(Company company,Department department,Location location,CmpDeptLoc setup) throws Exception{
		String returnMsg="SUCCESS";
		Map<String, Object> sessionObj = ActionContext.getContext()
		.getSession();
		User user = (User) sessionObj.get("USER");

		
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (setup != null
				&& setup.getCmpDeptLocId() != null
				&& setup.getCmpDeptLocId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		if(department!=null && department.getDepartmentId()!=null && department.getDepartmentId()>0){
			List<CmpDeptLoc> temCompanySetup=cmpDeptLocationService.getSetupCDLBased(
					company.getCompanyId(),location.getLocationId(),department.getDepartmentId());
			if(setup.getCombination()!=null){
				List<CmpDeptLoc> tempSetups=cmpDeptLocationService.getSetupByCombinationId(setup.getCombination().getCombinationId());
				if(tempSetups!=null && tempSetups.size()>0){
					returnMsg="Combination Setup(Account Code) is Already Exist to this Combination!!!";
					return returnMsg;
				}
			}
			if(temCompanySetup==null || temCompanySetup.size()==0){
				setup.setCompany(company);
				setup.setDepartment(department);
				setup.setLocation(location);
				setup.setIsActive(true);
				cmpDeptLocationService.saveCmpDeptLocation(setup);
			}else{
				returnMsg="Combination Setup(Departmen & Location) are Already Exist to this company!!!";
			}
		}else if(department!=null && department.getDepartmentId()==null){
			departmentService.saveDepartment(department);//Save Department first
			List<CmpDeptLoc> temCompanySetup=cmpDeptLocationService.getSetupCDLBased(
					company.getCompanyId(),location.getLocationId(),department.getDepartmentId());
			if(temCompanySetup==null || temCompanySetup.size()==0){
				setup.setCompany(company);
				setup.setDepartment(department);
				setup.setLocation(location);
				setup.setIsActive(true);
				cmpDeptLocationService.saveCmpDeptLocation(setup);
			}else{
				returnMsg="Combination Setup(Departmen & Location) are Already Exist to this company!!!";
			}
		}else{
			locationService.saveLocation(location);//Save Location First
			if(company!=null){
				List<CmpDeptLoc> temCompanySetup=cmpDeptLocationService.getSetupCLBased(
						company.getCompanyId(),location.getLocationId());
				if(temCompanySetup==null || temCompanySetup.size()==0){
					setup.setCompany(company);
					setup.setDepartment(department);
					setup.setLocation(location);
					setup.setIsActive(true);
					cmpDeptLocationService.saveCmpDeptLocation(setup);
				}else{
					returnMsg="Location Already Exist to this company!!!";
				}
			}
		}
		
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				CmpDeptLoc.class.getSimpleName(),
				setup.getCmpDeptLocId(), user,
				workflowDetailVo);
		return returnMsg;
	}
	
	public String saveDepartment(Department department) throws Exception{
		String returnMsg="";
		if(department!=null && department.getDepartmentId()!=null && department.getDepartmentId()>0){
			departmentService.saveDepartment(department);
		}
		return returnMsg;
	}
	public String saveLocation(Location location) throws Exception{
		String returnMsg="";
		locationService.saveLocation(location);
		return returnMsg;
	}
	
	
	public String deleteDepartment(Long departmentId){
		String returnMsg="SUCCESS";
		List<CmpDeptLoc> setups =new ArrayList<CmpDeptLoc>();
		Department department=new Department();
		if(departmentId!=null){
			try{
				department=departmentService.getDepartmentById(departmentId);

				setups =cmpDeptLocationService.getSetupDepartmentBased(departmentId);
				if(setups!=null)
					cmpDeptLocationService.deleteSetupAll(setups);
				departmentService.deleteDepartment(department);

			}catch(org.hibernate.exception.ConstraintViolationException e){
				try{
					for (CmpDeptLoc cmpDeptLocation : setups) {
						cmpDeptLocation.setIsActive(false);
						cmpDeptLocationService.saveCmpDeptLocation(cmpDeptLocation);
					}
				}catch (Exception ex) {
					returnMsg="WARNING";
				}
				
			} catch (Exception e) {
				try{
					for (CmpDeptLoc cmpDeptLocation : setups) {
						cmpDeptLocation.setIsActive(false);
						cmpDeptLocationService.saveCmpDeptLocation(cmpDeptLocation);
					}
				}catch (Exception ex) {
					returnMsg="WARNING";
				}
			}
			
		}
		return returnMsg;
	}
	
	public String deleteLocation(Long locationId) throws Exception{
		String returnMsg="SUCCESS";
		List<CmpDeptLoc> setups =null;
		Location location=null;
		if(locationId!=null){
			try{
				location=locationService.getLocationById(locationId);

				setups =cmpDeptLocationService.getSetupLocationBased(locationId);
				cmpDeptLocationService.deleteSetupAll(setups);//Save Department first
				locationService.deleteLocation(location);

			}catch(org.hibernate.exception.ConstraintViolationException e){
				try{
					for (CmpDeptLoc cmpDeptLocation : setups) {
						cmpDeptLocation.setIsActive(false);
						cmpDeptLocationService.saveCmpDeptLocation(cmpDeptLocation);
					}
				}catch (Exception ex) {
					returnMsg="Can not delete infromation already used in other palce!!!";
				}
			} catch (Exception e) {
				try{
					for (CmpDeptLoc cmpDeptLocation : setups) {
						cmpDeptLocation.setIsActive(false);
						cmpDeptLocationService.saveCmpDeptLocation(cmpDeptLocation);
					}
				
				}catch (Exception ex) {
					returnMsg="Can not delete infromation already used in other palce!!!";
				}
			}
			
		}
		return returnMsg;
	}

	
	public String deleteCompanySetup(CmpDeptLoc setup) throws Exception{
		String returnMsg="SUCCESS";
		/*if(setup!=null){
			try{
				setup=cmpDeptLocationService.getCompanySetup(setup.getCmpDeptLocId());
				cmpDeptLocationService.deleteSetup(setup);//Save Department first
			}catch(org.hibernate.exception.ConstraintViolationException e){
				setup.setIsActive(false);
				try{
					cmpDeptLocationService.saveCmpDeptLocation(setup);
				}catch (Exception ex) {
					returnMsg="Can not delete infromation already used in other palce!!!";
				}
			} catch (Exception e) {
				setup.setIsActive(false);
				try{
					cmpDeptLocationService.saveCmpDeptLocation(setup);
				}catch (Exception ex) {
					returnMsg="Can not delete infromation already used in other palce!!!";

				}
			}
			
		}*/
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				CmpDeptLoc.class.getSimpleName(), setup.getCmpDeptLocId(),
				user, workflowDetailVo);
		return returnMsg;
	}
	
	public List<Department> commonDepartmentList(Long companyId){
		List<Department> departments=new ArrayList<Department>();
		try {
			departments=cmpDeptLocationService.getCompanyBasedDepartment(companyId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return departments;
	}
	//Getter & Setter 
	public LocationService getLocationService() {
		return locationService;
	}
	public void setLocationService(LocationService locationService) {
		this.locationService = locationService;
	}
	public DepartmentService getDepartmentService() {
		return departmentService;
	}
	public void setDepartmentService(DepartmentService departmentService) {
		this.departmentService = departmentService;
	}
	public CompanyService getCompanyService() {
		return companyService;
	}
	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}
	public CmpDeptLocationService getCmpDeptLocationService() {
		return cmpDeptLocationService;
	}
	public void setCmpDeptLocationService(
			CmpDeptLocationService cmpDeptLocationService) {
		this.cmpDeptLocationService = cmpDeptLocationService;
	}
	public SystemService getSystemService() {
		return systemService;
	}
	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public LocationBL getLocationBL() {
		return locationBL;
	}

	public void setLocationBL(LocationBL locationBL) {
		this.locationBL = locationBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public CmpDeptLocBL getCmpDeptLocBL() {
		return cmpDeptLocBL;
	}

	public void setCmpDeptLocBL(CmpDeptLocBL cmpDeptLocBL) {
		this.cmpDeptLocBL = cmpDeptLocBL;
	}
	
}
