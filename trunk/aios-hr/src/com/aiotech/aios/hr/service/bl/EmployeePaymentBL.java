package com.aiotech.aios.hr.service.bl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Category;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.service.GLChartOfAccountService;
import com.aiotech.aios.accounts.service.GLCombinationService;
import com.aiotech.aios.accounts.service.bl.DirectPaymentBL;
import com.aiotech.aios.accounts.service.bl.TransactionBL;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.hr.domain.entity.EmployeeLoan;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.PayrollElement;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.EmployeePaymentVO;
import com.aiotech.aios.hr.service.EmployeeLoanService;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;

public class EmployeePaymentBL {

	private Implementation implementation;
	private DirectPaymentBL directPaymentBL;
	private PayrollElementBL payrollElementBL;
	private TransactionBL transactionBL;
	private GLChartOfAccountService accountService;
	private GLCombinationService combinationService;
	private PersonBL personBL;
	private EmployeeLoanService employeeLoanService;

	public void employeeDirectPayment(EmployeePaymentVO vo) {

		try {

			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			String paymentNumber = "";
			DirectPayment directPayment = null;

			List<Currency> currencyList = null;
			if (vo.getObject() != null) {
				currencyList = directPaymentBL.getTransactionBL()
						.getCurrencyService()
						.getAllCurrency(getImplementation());
				directPayment = new DirectPayment();

				paymentNumber = SystemBL.getReferenceStamp(
						DirectPayment.class.getName(), getImplementation());
				directPayment.setPersonByCreatedBy(person);
				directPayment.setPaymentNumber(paymentNumber);
				directPayment.setRecordId(vo.getRecordId());
				directPayment.setUseCase(vo.getUseCase());
				// Direct Payment details creation
				List<DirectPaymentDetail> directPayDetails = new ArrayList<DirectPaymentDetail>();
				if (vo.getUseCase().equalsIgnoreCase("EmployeeLoan")) {
					EmployeePaymentVO empVO = getEmployeeLoanTransaction(vo);
					directPayDetails.addAll(empVO.getDirectPayments());
					directPayment.setPersonByPersonId(empVO.getJobAssignment()
							.getPerson());
				}
				directPayment
						.setDirectPaymentDetails(new HashSet<DirectPaymentDetail>(
								directPayDetails));

			}
			for (Currency list : currencyList) {
				if (list.getDefaultCurrency())
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", list.getCurrencyId());
			}

			ServletActionContext.getRequest().setAttribute("DIRECT_PAYMENT",
					directPayment);
			ServletActionContext.getRequest().setAttribute(
					"DIRECT_PAYMENT_DETAIL",
					directPayment.getDirectPaymentDetails());

			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencyList);
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					directPaymentBL.getModeOfPayment());
			ServletActionContext.getRequest().setAttribute("PAYEE_TYPES",
					directPaymentBL.getReceiptSource());
		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	public EmployeePaymentVO getEmployeeLoanTransaction(
			EmployeePaymentVO employeePaymentVO) throws Exception {
		EmployeePaymentVO employeeVO = new EmployeePaymentVO();
		List<DirectPaymentDetail> directPayDetails = new ArrayList<DirectPaymentDetail>();
		EmployeeLoan employeeLoan = this.getEmployeeLoanService()
				.getEmployeeLoanDetail(employeePaymentVO.getRecordId());
		PayrollElement payrollElement = null;
		employeeVO.setJobAssignment(employeeLoan.getJobAssignment());
		if (employeeLoan.getFinanceType() == ((int) Constants.HR.LoanFinanceType.Loan
				.getCode())) {
			payrollElement = payrollElementBL.getPayrollElementService()
					.getPayrollElementByCode(getImplementation(), "LOAN");
		} else if (employeeLoan.getFinanceType() == ((int) Constants.HR.LoanFinanceType.Advance
				.getCode())) {

			payrollElement = payrollElementBL.getPayrollElementService()
					.getPayrollElementByCode(getImplementation(), "ADVANCE");
		}
		DirectPaymentDetail directPaymentDetail = new DirectPaymentDetail();
		directPaymentDetail.setAmount(employeeLoan.getSanctionedAmount());
		Combination depositcombination = new Combination();
		depositcombination.setCombinationId(payrollElement.getCombination()
				.getCombinationId());
		depositcombination = directPaymentBL
				.getTransactionBL()
				.getCombinationService()
				.getCombinationAllAccountDetail(
						depositcombination.getCombinationId());
		directPaymentDetail.setCombination(depositcombination);
		directPayDetails.add(directPaymentDetail);
		employeeVO.setDirectPayments(directPayDetails);
		return employeeVO;
	}

	public void paymentTransactionCreation(EmployeePaymentVO employeePaymentVO) {
		Double amount = 0.0;
		JobAssignment jobAssignment = null;
		List<TransactionDetail> transList = new ArrayList<TransactionDetail>();
		Transaction transaction = null;
		try {

			if (!employeePaymentVO.isDeleteFlag()) {
				amount = employeePaymentVO.getAmount();
				Category childCategory = transactionBL
						.getCategory(employeePaymentVO.getAccountCategory()
								+ " Liability");

				if (employeePaymentVO.getObject() != null) {
					Method method = employeePaymentVO.getObject().getClass()
							.getMethod("getJobAssignment");
					jobAssignment = (JobAssignment) method.invoke(
							employeePaymentVO.getObject(), null);
				}
				// -------------Payable Credit------------------------
				Combination personCombination = personBL
						.findCombinationInfo(jobAssignment.getPerson()
								.getFirstName()
								+ " "
								+ jobAssignment.getPerson().getLastName()
								+ "-"
								+ jobAssignment.getPerson().getPersonNumber()
								+ "-EMPLOYEE");

				String descriptionDebit = employeePaymentVO
						.getAccountCategory()
						+ " Debit for "
						+ jobAssignment.getPerson().getFirstName()
						+ " "
						+ jobAssignment.getPerson().getLastName();

				TransactionDetail transactionDetail = transactionBL
						.createTransactionDetail(amount,
								personCombination.getCombinationId(), true,
								descriptionDebit, null,null);

				transList.add(transactionDetail);

				String descriptionCredit = employeePaymentVO
						.getAccountCategory()
						+ " Credit for "
						+ jobAssignment.getPerson().getFirstName()
						+ " "
						+ jobAssignment.getPerson().getLastName();

				Combination elementCombination = null;
				if (employeePaymentVO.getPayrollElement().getCombination() != null
						&& employeePaymentVO.getPayrollElement()
								.getCombination().getCombinationId() != null)
					elementCombination = employeePaymentVO.getPayrollElement()
							.getCombination();
				else
					elementCombination = personBL
							.findCombinationInfo(employeePaymentVO
									.getPayrollElement().getElementName());

				transactionDetail = transactionBL.createTransactionDetail(
						amount, elementCombination.getCombinationId(), false,
						descriptionCredit, null,null);

				transList.add(transactionDetail);

				transaction = transactionBL
						.createTransaction(transactionBL.getCurrency()
								.getCurrencyId(),
								employeePaymentVO.getAccountCategory()
										+ " Issued to employee "
										+ jobAssignment.getPerson()
												.getFirstName()
										+ " "
										+ jobAssignment.getPerson()
												.getLastName(), new Date(),
								childCategory, employeePaymentVO.getRecordId(),
								employeePaymentVO.getUseCase());
				transactionBL.saveTransaction(transaction, transList);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void reverseTransaction(EmployeePaymentVO employeePaymentVO) {
		Double amount = 0.0;
		JobAssignment jobAssignment = null;
		List<TransactionDetail> transList = new ArrayList<TransactionDetail>();
		Transaction transaction = null;
		try {
			if (employeePaymentVO.isEditFlag())
				amount = employeePaymentVO.getOldAmount();
			else
				amount = employeePaymentVO.getAmount();

			Category childCategory = transactionBL
					.getCategory(employeePaymentVO.getAccountCategory()
							+ " Liability");

			if (employeePaymentVO.getObject() != null) {
				Method method = employeePaymentVO.getObject().getClass()
						.getMethod("getJobAssignment");
				jobAssignment = (JobAssignment) method.invoke(
						employeePaymentVO.getObject(), null);
			}
			// -------------Payable Debit------------------------
			Combination personCombination = personBL
					.findCombinationInfo(jobAssignment.getPerson()
							.getFirstName()
							+ " "
							+ jobAssignment.getPerson().getLastName()
							+ "-"
							+ jobAssignment.getPerson().getPersonNumber()
							+ "-EMPLOYEE");

			String descriptionDebit = employeePaymentVO.getAccountCategory()
					+ " Credit for " + jobAssignment.getPerson().getFirstName()
					+ " " + jobAssignment.getPerson().getLastName();

			TransactionDetail transactionDetail = transactionBL
					.createTransactionDetail(amount,
							personCombination.getCombinationId(), false,
							descriptionDebit, null,null);

			transList.add(transactionDetail);

			String descriptionCredit = employeePaymentVO.getAccountCategory()
					+ " Debit for " + jobAssignment.getPerson().getFirstName()
					+ " " + jobAssignment.getPerson().getLastName();

			Combination elementCombination = null;
			if (employeePaymentVO.getPayrollElement().getCombination() != null
					&& employeePaymentVO.getPayrollElement().getCombination()
							.getCombinationId() != null)
				elementCombination = employeePaymentVO.getPayrollElement()
						.getCombination();
			else
				elementCombination = personBL
						.findCombinationInfo(employeePaymentVO
								.getPayrollElement().getElementName());

			transactionDetail = transactionBL.createTransactionDetail(amount,
					elementCombination.getCombinationId(), true,
					descriptionCredit, null,null);

			transList.add(transactionDetail);

			transaction = transactionBL.createTransaction(transactionBL
					.getCurrency().getCurrencyId(),
					"Loan Liability for employee"
							+ jobAssignment.getPerson().getFirstName() + " "
							+ jobAssignment.getPerson().getLastName(),
					new Date(), childCategory, employeePaymentVO.getRecordId(),
					employeePaymentVO.getUseCase());
			transactionBL.saveTransaction(transaction, transList);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public PayrollElementBL getPayrollElementBL() {
		return payrollElementBL;
	}

	public void setPayrollElementBL(PayrollElementBL payrollElementBL) {
		this.payrollElementBL = payrollElementBL;
	}

	public DirectPaymentBL getDirectPaymentBL() {
		return directPaymentBL;
	}

	public void setDirectPaymentBL(DirectPaymentBL directPaymentBL) {
		this.directPaymentBL = directPaymentBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public GLChartOfAccountService getAccountService() {
		return accountService;
	}

	public void setAccountService(GLChartOfAccountService accountService) {
		this.accountService = accountService;
	}

	public GLCombinationService getCombinationService() {
		return combinationService;
	}

	public void setCombinationService(GLCombinationService combinationService) {
		this.combinationService = combinationService;
	}

	public PersonBL getPersonBL() {
		return personBL;
	}

	public void setPersonBL(PersonBL personBL) {
		this.personBL = personBL;
	}

	public EmployeeLoanService getEmployeeLoanService() {
		return employeeLoanService;
	}

	public void setEmployeeLoanService(EmployeeLoanService employeeLoanService) {
		this.employeeLoanService = employeeLoanService;
	}

}
