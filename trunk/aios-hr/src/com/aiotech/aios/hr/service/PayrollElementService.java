package com.aiotech.aios.hr.service;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.EosPolicy;
import com.aiotech.aios.hr.domain.entity.PayrollElement;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class PayrollElementService {
	public AIOTechGenericDAO<PayrollElement> payrollElementDAO;
	public AIOTechGenericDAO<EosPolicy> eosPolicyDAO;

	//Get Payroll Element list
	public List<PayrollElement> getPayrollElementList(Implementation implementation) {
		return payrollElementDAO.findByNamedQuery("getAllPayrollElement", implementation);
	}
	public PayrollElement getPayrollElementInfo(Long payrollElementId) {
		return payrollElementDAO.findById(payrollElementId);
	}
	
	public PayrollElement getPayrollElementByCode(Implementation implementation,String code) {
		List<PayrollElement> payrollElemetns = payrollElementDAO
				.findByNamedQuery("getPayrollElementByCode", implementation,code);
		if (payrollElemetns != null && payrollElemetns.size() > 0)
			return payrollElemetns.get(0);
		else
			return null;
	}
	
	public List<EosPolicy> getAllEosPolicy() {
		return eosPolicyDAO.getAll();
	}

	public void payrollElementSave(PayrollElement payrollElement){
		payrollElementDAO.saveOrUpdate(payrollElement);
	}
	
	public void payrollElementDelete(PayrollElement payrollElement){
		payrollElementDAO.delete(payrollElement);
	}
	
	public AIOTechGenericDAO<PayrollElement> getPayrollElementDAO() {
		return payrollElementDAO;
	}

	public void setPayrollElementDAO(
			AIOTechGenericDAO<PayrollElement> payrollElementDAO) {
		this.payrollElementDAO = payrollElementDAO;
	}
	public AIOTechGenericDAO<EosPolicy> getEosPolicyDAO() {
		return eosPolicyDAO;
	}
	public void setEosPolicyDAO(AIOTechGenericDAO<EosPolicy> eosPolicyDAO) {
		this.eosPolicyDAO = eosPolicyDAO;
	}
}
