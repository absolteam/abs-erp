package com.aiotech.aios.hr.service.bl;

import org.apache.commons.beanutils.BeanUtils;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.ParkingAllowance;
import com.aiotech.aios.hr.domain.entity.vo.ParkingAllowanceVO;
import com.aiotech.aios.hr.service.ParkingAllowanceService;
import com.aiotech.aios.system.service.bl.LookupMasterBL;

public class ParkingAllowanceBL {

	private LookupMasterBL lookupMasterBL;
	private ParkingAllowanceService parkingAllowanceService;
	private PayPeriodBL payPeriodBL;

	public ParkingAllowanceVO convertEntityToVO(
			ParkingAllowance parkingAllowance) {
		ParkingAllowanceVO parkingAllowanceVO = new ParkingAllowanceVO();
		try {
			// Copy Entity class properties value into VO properties.
			BeanUtils.copyProperties(parkingAllowanceVO, parkingAllowance);
			parkingAllowanceVO.setFromDateView(DateFormat
					.convertDateToString(parkingAllowance.getFromDate() + ""));
			parkingAllowanceVO.setToDateView(DateFormat
					.convertDateToString(parkingAllowance.getToDate() + ""));
			if (parkingAllowance.getPurchaseDate() != null)
				parkingAllowanceVO.setPurchaseDateView(DateFormat
						.convertDateToString(parkingAllowance.getPurchaseDate()
								+ ""));
			if (parkingAllowance.getIsFinanceImpact() != null
					&& parkingAllowance.getIsFinanceImpact())
				parkingAllowanceVO.setIsFinanceView("YES");
			else
				parkingAllowanceVO.setIsFinanceView("NO");

			if (parkingAllowance.getLookupDetailByCardCompany() != null)
				parkingAllowanceVO.setCardCompany(parkingAllowance
						.getLookupDetailByCardCompany().getDisplayName());
			if (parkingAllowance.getLookupDetailByCardType() != null)
				parkingAllowanceVO.setCardType(parkingAllowance
						.getLookupDetailByCardType().getDisplayName());

			if (parkingAllowance.getLookupDetailByParkingType() != null)
				parkingAllowanceVO.setParkingType(parkingAllowance
						.getLookupDetailByParkingType().getDisplayName());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return parkingAllowanceVO;
	}

	public String saveParkingAllowance(ParkingAllowance parkingAllowance) {
		String returnStatus = "SUCCESS";
		try {

			parkingAllowanceService.saveParkingAllowance(parkingAllowance);

		} catch (Exception e) {
			returnStatus = "ERROR";
		}
		return returnStatus;
	}

	public String deleteParkingAllowance(ParkingAllowance parkingAllowance) {
		String returnStatus = "SUCCESS";
		try {

			parkingAllowanceService.deleteParkingAllowance(parkingAllowance);
		} catch (Exception e) {
			returnStatus = "ERROR";
		}
		return returnStatus;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public PayPeriodBL getPayPeriodBL() {
		return payPeriodBL;
	}

	public void setPayPeriodBL(PayPeriodBL payPeriodBL) {
		this.payPeriodBL = payPeriodBL;
	}

	public ParkingAllowanceService getParkingAllowanceService() {
		return parkingAllowanceService;
	}

	public void setParkingAllowanceService(
			ParkingAllowanceService parkingAllowanceService) {
		this.parkingAllowanceService = parkingAllowanceService;
	}
}
