package com.aiotech.aios.hr.service.bl;

import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.FuelAllowance;
import com.aiotech.aios.hr.domain.entity.MileageLog;
import com.aiotech.aios.hr.domain.entity.vo.FuelAllowanceVO;
import com.aiotech.aios.hr.service.FuelAllowanceService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class FuelAllowanceBL {

	private LookupMasterBL lookupMasterBL;
	private FuelAllowanceService fuelAllowanceService;
	private PayPeriodBL payPeriodBL;
	private MileageLogBL mileageLogBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public FuelAllowanceVO convertEntityToVO(FuelAllowance allowance) {
		FuelAllowanceVO allowanceVO = new FuelAllowanceVO();
		try {
			// Copy Entity class properties value into VO properties.
			BeanUtils.copyProperties(allowanceVO, allowance);
			allowanceVO.setFromDateView(DateFormat
					.convertDateToString(allowance.getFromDate() + ""));
			allowanceVO.setToDateView(DateFormat.convertDateToString(allowance
					.getToDate() + ""));
			if (allowance.getIsFinanceImpact() != null
					&& allowance.getIsFinanceImpact())
				allowanceVO.setIsFinanceView("YES");
			else
				allowanceVO.setIsFinanceView("NO");

			if (allowance.getLookupDetailByVehicleNumber() != null)
				allowanceVO.setVehicleNumber(allowance
						.getLookupDetailByVehicleNumber().getDisplayName());

			if (allowance.getLookupDetailByCardType() != null)
				allowanceVO.setCardType(allowance.getLookupDetailByCardType()
						.getDisplayName());
			if (allowance.getLookupDetailByFuelCompany() != null)
				allowanceVO.setFuelCompany(allowance
						.getLookupDetailByFuelCompany().getDisplayName());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return allowanceVO;
	}

	public String saveFuelAllowance(FuelAllowance allowance) {
		String returnStatus = "SUCCESS";
		try {

			fuelAllowanceService.saveFuelAllowance(allowance);

		} catch (Exception e) {
			returnStatus = "ERROR";
		}
		return returnStatus;
	}

	public String saveMileageLog(MileageLog mileageLog) {
		String returnStatus = "SUCCESS";
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			mileageLog.setIsApprove((byte) WorkflowConstants.Status.Published
					.getCode());
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (mileageLog != null && mileageLog.getMileageLogId() != null
					&& mileageLog.getMileageLogId() > 0) {

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			fuelAllowanceService.saveMileageLog(mileageLog);

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					MileageLog.class.getSimpleName(),
					mileageLog.getMileageLogId(), user, workflowDetailVo);

		} catch (Exception e) {
			returnStatus = "ERROR";
		}
		return returnStatus;
	}

	public String deleteMileageLog(MileageLog mileageLog) {
		String returnStatus = "SUCCESS";
		try {

			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
							.getCode());
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					MileageLog.class.getSimpleName(),
					mileageLog.getMileageLogId(), user, workflowDetailVo);

		} catch (Exception e) {
			returnStatus = "ERROR";
		}
		return returnStatus;
	}

	public String deleteFuelAllowance(FuelAllowance allowance) {
		String returnStatus = "SUCCESS";
		try {
			fuelAllowanceService.deleteFuelAllowance(allowance);
		} catch (Exception e) {
			returnStatus = "ERROR";
		}
		return returnStatus;
	}

	public FuelAllowanceVO convertEntityToVO(MileageLog mileageLog) {
		FuelAllowanceVO allowanceVO = new FuelAllowanceVO();
		try {
			// Copy Entity class properties value into VO properties.
			allowanceVO.setMileageLog(mileageLog);
			allowanceVO.setMileageLogId(mileageLog.getMileageLogId());
			if (mileageLog.getLookupDetailByVehicleNumber() != null)
				allowanceVO.setVehicleNumber(mileageLog
						.getLookupDetailByVehicleNumber().getDisplayName());

			if (mileageLog.getLookupDetailBySource() != null)
				allowanceVO.setSource(mileageLog.getLookupDetailBySource()
						.getDisplayName());

			if (mileageLog.getLookupDetailByDestination() != null)
				allowanceVO.setDestination(mileageLog
						.getLookupDetailByDestination().getDisplayName());

			allowanceVO.setDriver(mileageLog.getJobAssignment().getPerson()
					.getFirstName()
					+ " "
					+ mileageLog.getJobAssignment().getPerson().getLastName());
			allowanceVO.setStart(mileageLog.getStart().toString());
			allowanceVO.setEnd(mileageLog.getEnd().toString());
			allowanceVO
					.setMileage((mileageLog.getEnd() - mileageLog.getStart())
							+ "");

			allowanceVO.setDate(DateFormat.convertDateToString(mileageLog
					.getDate().toString()));

			allowanceVO.setTime(DateFormat.convertTimeToString(mileageLog
					.getTime().toString()));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return allowanceVO;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public PayPeriodBL getPayPeriodBL() {
		return payPeriodBL;
	}

	public void setPayPeriodBL(PayPeriodBL payPeriodBL) {
		this.payPeriodBL = payPeriodBL;
	}

	public FuelAllowanceService getFuelAllowanceService() {
		return fuelAllowanceService;
	}

	public void setFuelAllowanceService(
			FuelAllowanceService fuelAllowanceService) {
		this.fuelAllowanceService = fuelAllowanceService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public MileageLogBL getMileageLogBL() {
		return mileageLogBL;
	}

	public void setMileageLogBL(MileageLogBL mileageLogBL) {
		this.mileageLogBL = mileageLogBL;
	}

}
