package com.aiotech.aios.hr.service.bl;

import java.util.Map;

import com.aiotech.aios.accounts.service.GLChartOfAccountService;
import com.aiotech.aios.accounts.service.GLCombinationService;
import com.aiotech.aios.hr.domain.entity.PayrollElement;
import com.aiotech.aios.hr.service.JobService;
import com.aiotech.aios.hr.service.PayrollElementService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class PayrollElementBL {

	private PayrollElementService payrollElementService;
	private JobService jobService;
	private GLChartOfAccountService accountService;
	private GLCombinationService combinationService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	public void savePayrollElement(PayrollElement payrollElement) throws Exception{
		Map<String, Object> sessionObj = ActionContext.getContext()
		.getSession();
		User user = (User) sessionObj.get("USER");

		payrollElement.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (payrollElement != null
				&& payrollElement.getPayrollElementId() != null
				&& payrollElement.getPayrollElementId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		this.getPayrollElementService().payrollElementSave(payrollElement);
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				PayrollElement.class.getSimpleName(),
				payrollElement.getPayrollElementId(), user,
				workflowDetailVo);
	}
	
	public void doPayrollElementBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		PayrollElement payrollElement = this.getPayrollElementService().getPayrollElementInfo(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			this.getPayrollElementService().payrollElementDelete(payrollElement);
		} else {
		}
	}
	
	public PayrollElementService getPayrollElementService() {
		return payrollElementService;
	}
	public void setPayrollElementService(PayrollElementService payrollElementService) {
		this.payrollElementService = payrollElementService;
	}
	public JobService getJobService() {
		return jobService;
	}
	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}
	public GLCombinationService getCombinationService() {
		return combinationService;
	}
	public void setCombinationService(GLCombinationService combinationService) {
		this.combinationService = combinationService;
	}
	public GLChartOfAccountService getAccountService() {
		return accountService;
	}
	public void setAccountService(GLChartOfAccountService accountService) {
		this.accountService = accountService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

}
