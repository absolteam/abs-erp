package com.aiotech.aios.hr.service.bl;

import org.apache.commons.beanutils.BeanUtils;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.PayByCount;
import com.aiotech.aios.hr.domain.entity.vo.PayByCountVO;
import com.aiotech.aios.hr.service.PayByCountService;
import com.aiotech.aios.system.service.bl.LookupMasterBL;

public class PayByCountBL {

	private LookupMasterBL lookupMasterBL;
	private PayByCountService payByCountService;
	private PayPeriodBL payPeriodBL;

	public PayByCountVO convertEntityToVO(PayByCount payByCount) {
		PayByCountVO payByCountVO = new PayByCountVO();
		try {
			// Copy Entity class properties value into VO properties.
			BeanUtils.copyProperties(payByCountVO, payByCount);

			payByCountVO.setFromDateView(DateFormat
					.convertDateToString(payByCount.getFromDate() + ""));
			payByCountVO.setToDateView(DateFormat
					.convertDateToString(payByCount.getToDate() + ""));

			/*
			 * if (payByCount.getIsFinanceImpact() != null &&
			 * payByCount.getIsFinanceImpact())
			 * payByCountVO.setIsFinanceView("YES"); else
			 * payByCountVO.setIsFinanceView("NO");
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}
		return payByCountVO;
	}

	public String savePayByCount(PayByCount payByCount) {
		String returnStatus = "SUCCESS";
		try {

			payByCountService.savePayByCount(payByCount);

		} catch (Exception e) {
			returnStatus = "ERROR";
		}
		return returnStatus;
	}

	public String deletePayByCount(PayByCount payByCount) {
		String returnStatus = "SUCCESS";
		try {
			payByCountService.deletePayByCount(payByCount);
		} catch (Exception e) {
			returnStatus = "ERROR";
		}
		return returnStatus;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public PayPeriodBL getPayPeriodBL() {
		return payPeriodBL;
	}

	public void setPayPeriodBL(PayPeriodBL payPeriodBL) {
		this.payPeriodBL = payPeriodBL;
	}

	public PayByCountService getPayByCountService() {
		return payByCountService;
	}

	public void setPayByCountService(PayByCountService payByCountService) {
		this.payByCountService = payByCountService;
	}
}
