package com.aiotech.aios.hr.service;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.hr.domain.entity.WorkingShift;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class WorkingShiftService {
	public AIOTechGenericDAO<WorkingShift> workingShiftDAO;
	
	public List<WorkingShift> getWorkingShiftList(Implementation implementation) {
		return workingShiftDAO.findByNamedQuery("getWorkingShiftList", implementation);
	}
	
	public WorkingShift getWorkingShiftInfo(Long workingShiftId) {
		List<WorkingShift> workingShifts=new ArrayList<WorkingShift>();
		workingShifts=workingShiftDAO.findByNamedQuery("getWorkingShiftInfo", workingShiftId);
		if(workingShifts!=null && workingShifts.size()>0)
			return workingShifts.get(0);
		else
			return null;
	}
	
	public void saveWorkingShift(WorkingShift workingShift){
		workingShiftDAO.saveOrUpdate(workingShift);
	}
	
	public void deleteWorkingShift(WorkingShift workingShift){
		workingShiftDAO.delete(workingShift);
	}

	public AIOTechGenericDAO<WorkingShift> getWorkingShiftDAO() {
		return workingShiftDAO;
	}

	public void setWorkingShiftDAO(AIOTechGenericDAO<WorkingShift> workingShiftDAO) {
		this.workingShiftDAO = workingShiftDAO;
	}
}
