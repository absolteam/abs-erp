package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.hr.domain.entity.LeavePolicy;
import com.aiotech.aios.hr.service.LeavePolicyService;
import com.aiotech.aios.system.domain.entity.Implementation;

public class LeavePolicyBL {
	private LeavePolicyService leavePolicyService;
	private Implementation implementation;

	
	public String leavePoliciesSave(List<LeavePolicy> leavePolicies,
			List<LeavePolicy> deleteLeavePolicies) throws Exception {
		String returnMessage = "SUCCESS";
		if (null != deleteLeavePolicies && deleteLeavePolicies.size() > 0) {
			leavePolicyService.deleteLeavePolicies(deleteLeavePolicies);
		}
		if (leavePolicies != null) {
			leavePolicyService.saveLeavePolicies(leavePolicies);
		}
		return returnMessage;
	}
	
	public List<LeavePolicy> getJobLeaveSimiler(List<LeavePolicy> leavePolicys,
			List<LeavePolicy> leavePolicyEdits){
        List<LeavePolicy> leavePolicyUpdates=new ArrayList<LeavePolicy>();

		Collection<Long> listOne =new ArrayList<Long>();
		Collection<Long> listTwo =new ArrayList<Long>();
		for(LeavePolicy leavePolicy:leavePolicys){
			if(leavePolicy.getLeavePolicyId()!=null && leavePolicy.getLeavePolicyId()>0)
				listOne.add(leavePolicy.getLeavePolicyId());
		}
		for(LeavePolicy leavePolicy:leavePolicyEdits){
			if(leavePolicy.getLeavePolicyId()!=null && leavePolicy.getLeavePolicyId()>0)
				listTwo.add(leavePolicy.getLeavePolicyId());
		}
		Collection<Long> similar = new HashSet<Long>( listOne );
        Collection<Long> different = new HashSet<Long>();
        different.addAll( listOne );
        different.addAll( listTwo );
        similar.retainAll( listTwo );
        different.removeAll( similar );
        
      //Delete Process
        for(Long lng:different){
	        for(LeavePolicy leavePolicy:leavePolicyEdits){
				if(lng!=null && leavePolicy.getLeavePolicyId().equals(lng)){
					leavePolicyUpdates.add(leavePolicy);
				}
			}
        }
        return leavePolicyUpdates;
	}
	
	public List<LeavePolicy> getLeaveLineDetail(String leaveLineDetail){
		List<LeavePolicy> leavePolicies=new ArrayList<LeavePolicy>();
		if(leaveLineDetail!=null && !leaveLineDetail.equals("")){
			String[] recordArray=new String[(leaveLineDetail.split("##")).length];
			recordArray=leaveLineDetail.split("##");
			for (String record : recordArray) {
				LeavePolicy leavePolicy=new LeavePolicy();
				String[] dataArray=new String[(record.split("@@")).length];
				dataArray=record.split("@@");
				if(!dataArray[0].equals("-1") && !dataArray[0].trim().equals(""))
					leavePolicy.setLeavePolicyId(Long.parseLong(dataArray[0]));
				if(!dataArray[1].equals("-1") && !dataArray[1].trim().equals(""))
					leavePolicy.setLeavePolicyName(dataArray[1]);
				
				if(dataArray[2].equals("true") || dataArray[2].trim().equals("true"))
					leavePolicy.setIsWeekendExcluded(true);
				else
					leavePolicy.setIsWeekendExcluded(false);
				
				if(dataArray[3].equals("true") || dataArray[3].trim().equals("true"))
					leavePolicy.setIsHolidayExcluded(true);
				else
					leavePolicy.setIsHolidayExcluded(false);
				
				if(dataArray[4].equals("true") || dataArray[4].trim().equals("true"))
					leavePolicy.setIsDefalut(true);
				else
					leavePolicy.setIsDefalut(false);
				
				leavePolicy.setImplementation(getImplementation());
				leavePolicies.add(leavePolicy);
			}
		}
		return leavePolicies;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}
	public LeavePolicyService getLeavePolicyService() {
		return leavePolicyService;
	}

	public void setLeavePolicyService(LeavePolicyService leavePolicyService) {
		this.leavePolicyService = leavePolicyService;
	}

}
