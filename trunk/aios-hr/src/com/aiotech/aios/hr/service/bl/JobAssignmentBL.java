package com.aiotech.aios.hr.service.bl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.dao.DataIntegrityViolationException;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Designation;
import com.aiotech.aios.hr.domain.entity.EmployeeCalendar;
import com.aiotech.aios.hr.domain.entity.EmployeeCalendarPeriod;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.domain.entity.Job;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobLeave;
import com.aiotech.aios.hr.domain.entity.JobPayrollElement;
import com.aiotech.aios.hr.domain.entity.JobShift;
import com.aiotech.aios.hr.domain.entity.Leave;
import com.aiotech.aios.hr.domain.entity.LeaveProcess;
import com.aiotech.aios.hr.domain.entity.LeaveReconciliation;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.PersonTypeAllocation;
import com.aiotech.aios.hr.domain.entity.vo.JobAssignmentVO;
import com.aiotech.aios.hr.domain.entity.vo.JobShiftVO;
import com.aiotech.aios.hr.domain.entity.vo.LeaveReconciliationVO;
import com.aiotech.aios.hr.domain.entity.vo.PersonVO;
import com.aiotech.aios.hr.service.EmployeeCalendarService;
import com.aiotech.aios.hr.service.JobAssignmentService;
import com.aiotech.aios.hr.service.LeaveProcessService;
import com.aiotech.aios.hr.service.PayrollElementService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class JobAssignmentBL {

	private JobAssignmentService jobAssignmentService;
	private JobBL jobBL;
	private Implementation implementation;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	private DesignationBL designationBL;
	private CmpDeptLocBL cmpDeptLocBL;
	private PersonBL personBL;
	private LookupMasterBL lookupMasterBL;
	private LeaveProcessService leaveProcessService;
	private EmployeeCalendarService employeeCalendarService;
	private PayrollElementService payrollElementService;

	public String saveJobAssignment(JobAssignment jobAssignment,
			List<JobAssignment> jobAssignmentsTemp) throws Exception {
		String returnMessage = "SUCCESS";
		if (jobAssignmentsTemp != null && jobAssignmentsTemp.size() > 0) {

			for (JobAssignment tempJobAssign : jobAssignmentsTemp) {
				if (null != jobAssignment.getJobAssignmentId()
						&& !jobAssignment.getJobAssignmentId().equals(
								tempJobAssign.getJobAssignmentId())
						&& tempJobAssign.getIsActive()
						&& tempJobAssign
								.getPerson()
								.getPersonId()
								.equals(jobAssignment.getPerson().getPersonId())) {

					return "Employee has Active Job Assignment already with this same combination!!!";
				} else if (tempJobAssign.getIsActive()
						&& null == jobAssignment.getJobAssignmentId()
						&& tempJobAssign
								.getPerson()
								.getPersonId()
								.equals(jobAssignment.getPerson().getPersonId())) {
					return "Employee has Active Job Assignment already with this same combination!!!";
				}
			}
		}

		String transactionReference = null;
		if (jobAssignment.getTransactionReference() == null) {
			DateTime date = DateTime.now();
			DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyyMMddHHmm");
			transactionReference = date.toString(fmt);
			jobAssignment.setTransactionReference(transactionReference);
		} else {
			transactionReference = jobAssignment.getTransactionReference();
		}

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		jobAssignment.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (jobAssignment != null && jobAssignment.getJobAssignmentId() != null
				&& jobAssignment.getJobAssignmentId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}

		jobAssignmentService.saveJobAssignment(jobAssignment);

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				JobAssignment.class.getSimpleName(),
				jobAssignment.getJobAssignmentId(), user, workflowDetailVo);

		HttpSession session = ServletActionContext.getRequest().getSession();
		Boolean isRejoingEnabled = session.getAttribute("rejoining_adjustment") != null ? ((String) session
				.getAttribute("rejoining_adjustment")).equalsIgnoreCase("true")
				: false;

		if (isRejoingEnabled && jobAssignment.getRejoiningDate() != null) {
			List<LeaveProcess> leaveProcessesToDelete = leaveProcessService
					.getLeaveProcessByTransactionRefrence(jobAssignment
							.getTransactionReference());
			if (leaveProcessesToDelete != null
					&& leaveProcessesToDelete.size() > 0) {
				leaveProcessService
						.deleteAllLeaveProcess(leaveProcessesToDelete);
			}

			Double perMonthDays = 0.0;
			double acutalDays = 0.0;
			LocalDate startDate = null;
			LocalDate endDate = null;
			Map<String, Leave> leaveMap = new HashMap<String, Leave>();
			LeaveProcess leaveProcessEdit = null;
			List<JobLeave> jobLeaves = jobBL.getJobService()
					.getAllJobLeaveByPerson(jobAssignment.getPerson());
			JobLeave forUnpaidAutoEntry = null;
			for (JobLeave jobLeave : jobLeaves) {
				leaveMap.put(jobLeave.getLeave().getLookupDetail()
						.getAccessCode(), jobLeave.getLeave());
				if (jobLeave.getLeave().getIsPaid() != null
						&& !jobLeave.getLeave().getIsPaid())
					forUnpaidAutoEntry = jobLeave;
			}

			List<LeaveProcess> leaveProcesses = leaveProcessService
					.getAllLeaveProcessBasedOnPerson(jobAssignment.getPerson());

			// Order by descending to get the first record in the next loop
			Collections.sort(leaveProcesses, new Comparator<LeaveProcess>() {
				public int compare(LeaveProcess m1, LeaveProcess m2) {
					return m2.getFromDate().compareTo(m1.getFromDate());
				}
			});

			// Filter the recent leave request which has hold option until
			// rejoining
			boolean rejoiningEarly = false;
			Double earlierReturnDays = null;
			List<LeaveProcess> leaveProcessListToManipulateLateJoining = new ArrayList<LeaveProcess>();
			for (LeaveProcess leaveProcess : leaveProcesses) {
				if (leaveProcess != null
						&& leaveProcess.getHoldUntilRejoining() != null
						&& leaveProcess.getHoldUntilRejoining()) {
					startDate = new LocalDate(leaveProcess.getFromDate());
					endDate = new LocalDate(jobAssignment.getRejoiningDate());
					if (jobAssignment.getRejoiningDate().before(
							leaveProcess.getToDate())) {
						Days earlDay = Days.daysBetween(endDate, new LocalDate(leaveProcess.getToDate()));
						earlierReturnDays = (double) earlDay.getDays()+1;
						Days d = Days.daysBetween(
								new LocalDate(leaveProcess.getFromDate()),
								endDate);
						leaveProcess.setDays((double) d.getDays()+1);
						leaveProcess
								.setToDate(jobAssignment.getRejoiningDate());
						leaveProcess.setEarlierReturnDays(earlierReturnDays);
						rejoiningEarly = true;
					} else {
						leaveProcessListToManipulateLateJoining
								.add(leaveProcess);
					}
					leaveProcessEdit = leaveProcess;
					break;
				}

			}

			// Calculate the service deduction and save the service cutoff days
			if (leaveProcessEdit !=null && leaveProcessEdit.getHoldUntilRejoining() != null
					&& leaveProcessEdit.getHoldUntilRejoining()
					&& leaveMap.containsKey("AL")) {
				Leave leave = leaveMap.get("AL");
				perMonthDays = leave.getDefaultDays() / 12;
				startDate = startDate.minusDays(1);
				Days d = Days.daysBetween(startDate, endDate);
				acutalDays = (double) d.getDays();
				Double days = acutalDays / 30;
				leaveProcessEdit.setServiceCutoffDays(days * perMonthDays);
				leaveProcessService.saveLeaveProcess(leaveProcessEdit);
			}

			Map<Date, Date> dateMapFromLeave = new HashMap<Date, Date>();
			for (LeaveProcess leaveProcess : leaveProcessListToManipulateLateJoining) {
				LocalDate startDate1 = new LocalDate(leaveProcess.getFromDate());
				LocalDate endDate1 = new LocalDate(leaveProcess.getToDate());
				endDate1 = endDate1.plusDays(1);
				while (startDate1.isBefore(endDate1)) {
					if (!dateMapFromLeave.containsKey(startDate1.toDate())) {
						dateMapFromLeave.put(startDate1.toDate(),
								startDate1.toDate());
					}
					startDate1 = startDate1.plusDays(1);

				}
			}

			if (!rejoiningEarly && leaveProcessEdit !=null) {
				// Set back to normal date. Before it was minus 1 day for
				// calculation
				startDate = startDate.plusDays(1);
				Map<Date, Date> dateMapFromRejoining = new HashMap<Date, Date>();
				endDate = endDate.minusDays(1);
				while (startDate.isBefore(endDate)) {
					startDate = startDate.plusDays(1);
					if (!dateMapFromLeave.containsKey(startDate.toDate())) {
						dateMapFromRejoining.put(startDate.toDate(),
								startDate.toDate());
					}

				}

				if (dateMapFromRejoining.size() > 0) {
					if (forUnpaidAutoEntry == null) {
						throw new DataIntegrityViolationException(
								"ASIOERR6666_Atleast one unpaid leave must be configured for late joining unpaid adjustment");
					}

					List<Date> dates = new ArrayList<Date>(
							dateMapFromRejoining.values());
					Collections.sort(dates, new Comparator<Date>() {
						public int compare(Date m1, Date m2) {
							return m1.compareTo(m2);
						}
					});
					LeaveProcess leaveProcessToUnPaid = new LeaveProcess();
					leaveProcessToUnPaid.setFromDate(dates.get(0));
					leaveProcessToUnPaid.setToDate(dates.get(dates.size() - 1));
					leaveProcessToUnPaid.setJobAssignment(jobAssignment);
					leaveProcessToUnPaid.setJobLeave(forUnpaidAutoEntry);
					leaveProcessToUnPaid.setImplementation(getImplementation());
					leaveProcessToUnPaid
							.setIsApprove((byte) WorkflowConstants.Status.Publish
									.getCode());
					leaveProcessToUnPaid.setHalfDay(false);
					leaveProcessToUnPaid
							.setStatus((byte) Constants.HR.LeaveStatus.Taken
									.getCode());
					leaveProcessToUnPaid.setCreatedDate(new Date());
					Person person = new Person();
					person.setPersonId(user.getPersonId());
					leaveProcessToUnPaid.setPerson(person);
					leaveProcessToUnPaid
							.setTransactionReference(transactionReference);
					leaveProcessToUnPaid.setDays((double) dateMapFromRejoining
							.size());
					leaveProcessToUnPaid
							.setReason("Leave information has been automatically created based on the employee late rejoining infromation");
					leaveProcessService.saveLeaveProcess(leaveProcessToUnPaid);
				}
			} else if (rejoiningEarly && leaveProcessEdit !=null) {
				
			}

		}

		return returnMessage;
	}

	public void deleteJobAssignment(JobAssignment jobAssignment)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				JobAssignment.class.getSimpleName(),
				jobAssignment.getJobAssignmentId(), user, workflowDetailVo);
	}

	public void doJobAssignmentBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		JobAssignment jobAssignment = jobAssignmentService
				.getJobAssignmentInfo(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			try {
				jobAssignmentService.deleteJobAssignment(jobAssignment);
			} catch (Exception e) {
				jobAssignment.setIsActive(false);
				jobAssignmentService.saveJobAssignment(jobAssignment);
			}
		} else {
		}
	}

	public List<JobAssignmentVO> convertEntitiesTOVOs(
			List<JobAssignment> jobAssignments) throws Exception {

		List<JobAssignmentVO> tosList = new ArrayList<JobAssignmentVO>();

		for (JobAssignment pers : jobAssignments) {
			tosList.add(convertEntityToVO(pers));
		}

		return tosList;
	}

	public JobAssignmentVO convertEntityToVOWithJob(JobAssignment jobAssignment) {
		JobAssignmentVO jobAssignmentVO = new JobAssignmentVO();
		// jobAssignmentVO = (JobAssignmentVO) jobAssignment;
		jobAssignmentVO.setJobAssignmentId(jobAssignment.getJobAssignmentId());
		jobAssignmentVO.setJobAssignmentNumber(jobAssignment
				.getJobAssignmentNumber());
		jobAssignmentVO.setJob(jobAssignment.getJob());
		jobAssignmentVO.setPerson(jobAssignment.getPerson());
		jobAssignmentVO.setCmpDeptLocation(jobAssignment.getCmpDeptLocation());
		jobAssignmentVO.setDesignation(jobAssignment.getDesignation());
		jobAssignmentVO.setSwipeId(jobAssignment.getSwipeId());
		jobAssignmentVO.setIsLocalPay(jobAssignment.getIsLocalPay());
		jobAssignmentVO.setIsApprove(jobAssignment.getIsApprove());
		jobAssignmentVO.setIsActive(jobAssignment.getIsActive());
		jobAssignmentVO.setIsPrimary(jobAssignment.getIsPrimary());
		jobAssignmentVO.setEffectiveDate(jobAssignment.getEffectiveDate());
		jobAssignmentVO.setEndDate(jobAssignment.getEndDate());
		jobAssignmentVO.setPersonBank(jobAssignment.getPersonBank());
		jobAssignmentVO.setPayMode(jobAssignment.getPayMode());
		jobAssignmentVO.setPayPolicy(jobAssignment.getPayPolicy());

		jobAssignmentVO.setJobName(jobAssignment.getJob().getJobName() + " ["
				+ jobAssignment.getJob().getJobNumber() + "]");
		jobAssignmentVO.setJobVO(jobBL.convertEntityToVO(jobBL.getJobService()
				.getJobInfo(jobAssignment.getJob().getJobId())));

		jobAssignmentVO.setDepartmentLocationName(jobAssignment
				.getCmpDeptLocation().getCompany().getCompanyName()
				+ ">>"
				+ jobAssignment.getCmpDeptLocation().getDepartment()
						.getDepartmentName()
				+ ">>"
				+ jobAssignment.getCmpDeptLocation().getLocation()
						.getLocationName());

		jobAssignmentVO.setPersonName(jobAssignment.getPerson().getFirstName()
				+ " " + jobAssignment.getPerson().getLastName() + " ["
				+ jobAssignment.getPerson().getPersonNumber() + "]");

		jobAssignmentVO.setEmail(jobAssignment.getPerson().getEmail());
		jobAssignmentVO.setMobile(jobAssignment.getPerson().getMobile());
		jobAssignmentVO.setDesignationName(jobAssignment.getDesignation()
				.getDesignationName());
		jobAssignmentVO.setDesignationNameAR(AIOSCommons
				.bytesToUTFString(jobAssignment.getDesignation()
						.getDesignationNameArabic()));

		if (jobAssignment.getEffectiveDate() != null)
			jobAssignmentVO
					.setEffectiveDateStr(DateFormat
							.convertDateToString(jobAssignment
									.getEffectiveDate() + ""));

		if (jobAssignment.getEndDate() != null)
			jobAssignmentVO.setEndDateStr(DateFormat
					.convertDateToString(jobAssignment.getEndDate() + ""));

		if (jobAssignment.getIsActive() != null
				&& jobAssignment.getIsActive() == true) {
			jobAssignmentVO.setIsActiveStr("YES");

		} else {
			jobAssignmentVO.setIsActiveStr("NO");
		}

		if (jobAssignment.getIsLocalPay() != null
				&& jobAssignment.getIsLocalPay() == true) {
			jobAssignmentVO.setIsLocalPayStr("YES");

		} else {
			jobAssignmentVO.setIsLocalPayStr("NO");
		}

		if (jobAssignment.getIsPrimary() != null
				&& jobAssignment.getIsPrimary() == true) {
			jobAssignmentVO.setIsPrimaryStr("YES");

		} else {
			jobAssignmentVO.setIsPrimaryStr("NO");
		}

		if (jobAssignment.getPersonBank() != null)
			jobAssignmentVO.setBankNameAndNumber(jobAssignment.getPersonBank()
					.getAccountNumber()
					+ " ["
					+ jobAssignment.getPersonBank().getBankName() + "]");
		else
			jobAssignmentVO.setBankNameAndNumber("");

		if (jobAssignment.getIsApprove() != null
				&& jobAssignment.getIsApprove() == 3) {
			jobAssignmentVO.setIsApproveStr("APPROVAL PENDING");
		} else if (jobAssignment.getIsApprove() != null
				&& jobAssignment.getIsApprove() == 1) {
			jobAssignmentVO.setIsApproveStr("APPROVED");
		} else if (jobAssignment.getIsApprove() != null
				&& jobAssignment.getIsApprove() == 4) {
			jobAssignmentVO.setIsApproveStr("APPROVED");
		} else if (jobAssignment.getIsApprove() != null
				&& jobAssignment.getIsApprove() == 2) {
			jobAssignmentVO.setIsApproveStr("CANCELLED");
		} else {
			jobAssignmentVO.setIsApproveStr("-NA-");
		}

		if (jobAssignment.getCmpDeptLocation() != null
				&& jobAssignment.getCmpDeptLocation().getDepartment() != null) {
			jobAssignmentVO.setDepartmentName(jobAssignment
					.getCmpDeptLocation().getDepartment().getDepartmentName());
		}

		if (jobAssignment.getCmpDeptLocation() != null
				&& jobAssignment.getCmpDeptLocation().getCompany() != null) {
			jobAssignmentVO.setCompanyName(jobAssignment.getCmpDeptLocation()
					.getCompany().getCompanyName());
		}

		if (jobAssignment.getCmpDeptLocation() != null
				&& jobAssignment.getCmpDeptLocation().getLocation() != null) {
			jobAssignmentVO.setLocationName(jobAssignment.getCmpDeptLocation()
					.getLocation().getLocationName());
		}

		if (jobAssignment.getLeaveReconciliations() != null
				&& jobAssignment.getLeaveReconciliations().size() > 0) {
			jobAssignmentVO.setLeaveReconciliations(jobAssignment
					.getLeaveReconciliations());
			jobAssignmentVO
					.setLeaveReconciliationVOs(convertLRListtoVOs(new ArrayList<LeaveReconciliation>(
							jobAssignment.getLeaveReconciliations())));
		}
		return jobAssignmentVO;
	}

	public JobAssignmentVO convertEntityToVO(JobAssignment jobAssignment) {
		JobAssignmentVO jobAssignmentVO = new JobAssignmentVO();
		// jobAssignmentVO = (JobAssignmentVO) jobAssignment;
		jobAssignmentVO.setJobAssignmentId(jobAssignment.getJobAssignmentId());
		jobAssignmentVO.setJobAssignmentNumber(jobAssignment
				.getJobAssignmentNumber());
		jobAssignmentVO.setJob(jobAssignment.getJob());
		jobAssignmentVO.setPerson(jobAssignment.getPerson());
		jobAssignmentVO.setCmpDeptLocation(jobAssignment.getCmpDeptLocation());
		jobAssignmentVO.setDesignation(jobAssignment.getDesignation());
		jobAssignmentVO.setSwipeId(jobAssignment.getSwipeId());
		jobAssignmentVO.setIsLocalPay(jobAssignment.getIsLocalPay());
		jobAssignmentVO.setIsApprove(jobAssignment.getIsApprove());
		jobAssignmentVO.setIsActive(jobAssignment.getIsActive());
		jobAssignmentVO.setIsPrimary(jobAssignment.getIsPrimary());
		jobAssignmentVO.setEffectiveDate(jobAssignment.getEffectiveDate());
		jobAssignmentVO.setEndDate(jobAssignment.getEndDate());
		jobAssignmentVO.setPersonBank(jobAssignment.getPersonBank());
		jobAssignmentVO.setPayMode(jobAssignment.getPayMode());
		jobAssignmentVO.setPayPolicy(jobAssignment.getPayPolicy());

		jobAssignmentVO.setJobName(jobAssignment.getJob().getJobName() + " ["
				+ jobAssignment.getJob().getJobNumber() + "]");

		jobAssignmentVO.setDepartmentLocationName(jobAssignment
				.getCmpDeptLocation().getCompany().getCompanyName()
				+ ">>"
				+ jobAssignment.getCmpDeptLocation().getDepartment()
						.getDepartmentName()
				+ ">>"
				+ jobAssignment.getCmpDeptLocation().getLocation()
						.getLocationName());

		jobAssignmentVO.setPersonName(jobAssignment.getPerson().getFirstName()
				+ " " + jobAssignment.getPerson().getLastName() + " ["
				+ jobAssignment.getPerson().getPersonNumber() + "]");

		jobAssignmentVO.setEmail(jobAssignment.getPerson().getEmail());
		jobAssignmentVO.setMobile(jobAssignment.getPerson().getMobile());
		jobAssignmentVO.setDesignationName(jobAssignment.getDesignation()
				.getDesignationName());
		jobAssignmentVO.setDesignationNameAR(AIOSCommons
				.bytesToUTFString(jobAssignment.getDesignation()
						.getDesignationNameArabic()));

		if (jobAssignment.getEffectiveDate() != null)
			jobAssignmentVO
					.setEffectiveDateStr(DateFormat
							.convertDateToString(jobAssignment
									.getEffectiveDate() + ""));

		if (jobAssignment.getEndDate() != null)
			jobAssignmentVO.setEndDateStr(DateFormat
					.convertDateToString(jobAssignment.getEndDate() + ""));

		if (jobAssignment.getRejoiningDate() != null)
			jobAssignmentVO
					.setRejoiningDateStr(DateFormat
							.convertDateToString(jobAssignment
									.getRejoiningDate() + ""));

		if (jobAssignment.getIsActive() != null
				&& jobAssignment.getIsActive() == true) {
			jobAssignmentVO.setIsActiveStr("YES");

		} else {
			jobAssignmentVO.setIsActiveStr("NO");
		}

		if (jobAssignment.getIsLocalPay() != null
				&& jobAssignment.getIsLocalPay() == true) {
			jobAssignmentVO.setIsLocalPayStr("YES");

		} else {
			jobAssignmentVO.setIsLocalPayStr("NO");
		}

		if (jobAssignment.getIsPrimary() != null
				&& jobAssignment.getIsPrimary() == true) {
			jobAssignmentVO.setIsPrimaryStr("YES");

		} else {
			jobAssignmentVO.setIsPrimaryStr("NO");
		}

		if (jobAssignment.getPersonBank() != null)
			jobAssignmentVO.setBankNameAndNumber(jobAssignment.getPersonBank()
					.getAccountNumber()
					+ " ["
					+ jobAssignment.getPersonBank().getBankName() + "]");
		else
			jobAssignmentVO.setBankNameAndNumber("");

		if (jobAssignment.getIsApprove() != null
				&& jobAssignment.getIsApprove() == 3) {
			jobAssignmentVO.setIsApproveStr("APPROVAL PENDING");
		} else if (jobAssignment.getIsApprove() != null
				&& jobAssignment.getIsApprove() == 1) {
			jobAssignmentVO.setIsApproveStr("APPROVED");
		} else if (jobAssignment.getIsApprove() != null
				&& jobAssignment.getIsApprove() == 4) {
			jobAssignmentVO.setIsApproveStr("APPROVED");
		} else if (jobAssignment.getIsApprove() != null
				&& jobAssignment.getIsApprove() == 2) {
			jobAssignmentVO.setIsApproveStr("CANCELLED");
		} else {
			jobAssignmentVO.setIsApproveStr("-NA-");
		}

		if (jobAssignment.getCmpDeptLocation() != null
				&& jobAssignment.getCmpDeptLocation().getDepartment() != null) {
			jobAssignmentVO.setDepartmentName(jobAssignment
					.getCmpDeptLocation().getDepartment().getDepartmentName());
		}

		if (jobAssignment.getCmpDeptLocation() != null
				&& jobAssignment.getCmpDeptLocation().getCompany() != null) {
			jobAssignmentVO.setCompanyName(jobAssignment.getCmpDeptLocation()
					.getCompany().getCompanyName());
		}

		if (jobAssignment.getCmpDeptLocation() != null
				&& jobAssignment.getCmpDeptLocation().getLocation() != null) {
			jobAssignmentVO.setLocationName(jobAssignment.getCmpDeptLocation()
					.getLocation().getLocationName());
		}

		if (jobAssignment.getLeaveReconciliations() != null
				&& jobAssignment.getLeaveReconciliations().size() > 0) {
			jobAssignmentVO.setLeaveReconciliations(jobAssignment
					.getLeaveReconciliations());
			jobAssignmentVO
					.setLeaveReconciliationVOs(convertLRListtoVOs(new ArrayList<LeaveReconciliation>(
							jobAssignment.getLeaveReconciliations())));
		}
		
		jobAssignmentVO.setFreeze(jobAssignment.getFreeze());

		jobAssignmentVO.setLookupKey(jobAssignment.getLookupKey());
		return jobAssignmentVO;
	}

	private List<LeaveReconciliationVO> convertLRListtoVOs(
			List<LeaveReconciliation> leaveReconciliations) {
		List<LeaveReconciliationVO> leaveReconciliationVOs = new ArrayList<LeaveReconciliationVO>();
		for (LeaveReconciliation lrc : leaveReconciliations) {
			LeaveReconciliationVO lrcVO = new LeaveReconciliationVO();
			lrcVO.setCarryForwardedCount(lrc.getCarryForwardedCount());
			lrcVO.setCreatedDate(lrc.getCreatedDate());
			lrcVO.setEligibleDays(lrc.getEligibleDays());
			lrcVO.setEndDate(lrc.getEndDate());
			lrcVO.setProvidedDays(lrc.getProvidedDays());
			lrcVO.setTakendDays(lrc.getTakendDays());
			lrcVO.setAvaiableDays(lrc.getEligibleDays() - lrc.getTakendDays());
			lrcVO.setLeaveType(lrc.getLeave().getLookupDetail()
					.getDisplayName());
			leaveReconciliationVOs.add(lrcVO);
		}

		return leaveReconciliationVOs;
	}

	public JobAssignmentService getJobAssignmentService() {
		return jobAssignmentService;
	}

	public void processEmployeeBulkEntries(String fileName) throws Exception {
		List<Job> jobs = jobBL.getJobService().getAllJobWithoutStatus(
				getImplementation());
		Map<String, Job> jobMap = new HashMap<String, Job>();
		for (Job job : jobs) {
			jobMap.put(job.getJobNumber().trim().toUpperCase(), job);
		}

		List<Designation> designations = designationBL.getDesignationService()
				.getAllDesignation(getImplementation());
		Map<String, Designation> designationMap = new HashMap<String, Designation>();
		for (Designation object : designations) {
			designationMap.put(
					object.getDesignationName().trim().toUpperCase(), object);
		}

		List<CmpDeptLoc> departments = cmpDeptLocBL.getCmpDeptLocService()
				.getAllLocation(getImplementation());
		Map<String, CmpDeptLoc> departmentsMap = new HashMap<String, CmpDeptLoc>();
		for (CmpDeptLoc object : departments) {
			departmentsMap.put(object.getDepartment().getDepartmentName()
					.trim().toUpperCase(), object);
		}

		List<String> rowList = fileReader(fileName);

		List<Person> persons = new ArrayList<Person>();
		Person person = null;
		JobAssignment jobAssignment = null;
		List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();
		List<PersonTypeAllocation> personTypeAllocationList = new ArrayList<PersonTypeAllocation>();
		List<LookupDetail> personTypeList = jobBL.getLookupMasterBL()
				.getActiveLookupDetails("PERSON_TYPE", true);
		Map<Integer, LookupDetail> map = new HashMap<Integer, LookupDetail>();
		for (LookupDetail el : personTypeList) {
			map.put(el.getDataId(), el);
		}
		PersonTypeAllocation personTypeAllocation = new PersonTypeAllocation();
		personTypeAllocation.setLookupDetail(map.get(Integer.valueOf(1)));
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, cal.get(Calendar.YEAR));
		cal.set(Calendar.DAY_OF_YEAR, 1);
		Date start = cal.getTime();
		for (String string : rowList) {

			String[] detailString = splitValues(string, ",");

			person = new Person();
			person.setPersonNumber(detailString[0].trim());
			person.setFirstName(detailString[1].trim());
			person.setLastName("");
			person.setMobile(detailString[0].trim());
			person.setImplementation(getImplementation());
			person.setValidFromDate(start);
			person.setStatus((byte) 1);
			person.setIsApprove((byte) WorkflowConstants.Status.Published
					.getCode());
			personTypeAllocation = new PersonTypeAllocation();
			personTypeAllocation.setPerson(person);
			personTypeAllocation.setLookupDetail(map.get(Integer.valueOf(1)));
			personTypeAllocationList.add(personTypeAllocation);
			persons.add(person);

			jobAssignment = new JobAssignment();
			jobAssignment.setJobAssignmentNumber(detailString[0].trim());
			jobAssignment.setPerson(person);
			jobAssignment.setIsActive(true);
			jobAssignment
					.setIsApprove((byte) WorkflowConstants.Status.Published
							.getCode());
			jobAssignment.setImplementation(getImplementation());
			jobAssignment.setIsPrimary(true);
			jobAssignment.setIsLocalPay(true);

			jobAssignment.setEffectiveDate(start);
			jobAssignment.setPayPolicy((byte) Constants.HR.PayPolicy.Monthly
					.getCode());
			jobAssignment.setCmpDeptLocation(departmentsMap.get(detailString[2]
					.trim().toUpperCase()));
			jobAssignment.setDesignation(designationMap.get(detailString[3]
					.trim().toUpperCase()));
			jobAssignment.setJob(jobMap.get(detailString[4].trim()
					.toUpperCase()));
			System.out.println(detailString[0].trim() + "----"
					+ detailString[1].trim() + "----" + detailString[2].trim()
					+ "----" + detailString[3].trim() + "----"
					+ detailString[4].trim());
			System.out.println("Designation Id"
					+ jobAssignment.getDesignation().getDesignationId());
			jobAssignments.add(jobAssignment);

		}
		jobBL.getPersonService().savePersons(persons);
		jobBL.getPersonService().savePersonTypes(personTypeAllocationList);
		jobAssignmentService.saveJobAssignmentAll(jobAssignments);
	}

	public void processEmployeeBulkUpdates(String fileName) throws Exception {

		List<Person> persons = new ArrayList<Person>();
		Person person = null;
		List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();
		List<JobAssignment> jobAssignmentsTemp = jobAssignmentService
				.getAllJobAssignment(getImplementation());
		Map<String, JobAssignment> jobAssignmentMap = new HashMap<String, JobAssignment>();
		for (JobAssignment job : jobAssignmentsTemp) {
			jobAssignmentMap.put(job.getJobAssignmentNumber().trim()
					.toUpperCase(), job);
		}

		List<String> rowList = fileReader(fileName);

		List<LookupDetail> accomodationOptions = jobBL.getLookupMasterBL()
				.getActiveLookupDetails("ACCOMODATION_OPTIONS", true);
		Map<String, LookupDetail> map = new HashMap<String, LookupDetail>();
		for (LookupDetail el : accomodationOptions) {
			map.put(el.getAccessCode(), el);
		}

		for (String string : rowList) {

			String[] detailString = splitValues(string, ",");
			JobAssignment jobAssignment = jobAssignmentMap.get(detailString[0]
					.trim());
			person = jobAssignment.getPerson();
			person.setFirstNameArabic(AIOSCommons
					.stringToUTFBytes(detailString[1].trim()));
			jobAssignment.setLookupKey(detailString[2].trim());
			persons.add(person);
			jobAssignments.add(jobAssignment);

		}
		jobBL.getPersonService().savePersons(persons);
		jobAssignmentService.saveJobAssignmentAll(jobAssignments);
	}

	public JobAssignmentVO getEmployeeFullDetails(Long jobAssignmentId) {
		JobAssignmentVO jobAssignmentVO = null;
		try {
			JobAssignment jobAssignment = jobAssignmentService
					.getJobAssignmentInfo(jobAssignmentId);
			jobAssignmentVO = new JobAssignmentVO();
			// Employee designation details
			jobAssignmentVO = convertEntityToVO(jobAssignment);
			Person person = personBL.getPersonService().getPersonDetails(
					jobAssignment.getPerson().getPersonId());
			// Person name & nationality
			PersonVO personVO = personBL.personConverterFromEntityTOVO(person);
			jobAssignmentVO.setPersonVO(personVO);

			// Passport details
			List<Identity> identities = jobBL.getPersonService()
					.getAllIdentity(jobAssignment.getPerson());
			for (Identity identity : identities) {
				if ((int) identity.getIdentityType() == (int) 3)
					jobAssignmentVO.setPassportNumber(identity
							.getIdentityNumber());
			}

			// Cross Salary
			List<JobPayrollElement> payrollElements = jobBL.getJobService()
					.getAllJobPayrollElement(jobAssignment.getJob());
			Double corssSalary = 0.0;
			String salaryDetails = "";
			String salaryDetailsAR = "";
			int count = 1;
			String name = "";
			for (JobPayrollElement jobPayrollElement : payrollElements) {

				name = AIOSCommons.bytesToUTFString(jobPayrollElement
						.getPayrollElementByPayrollElementId()
						.getElementNameArabic());

				corssSalary += jobPayrollElement.getAmount();
				if (count == 1) {
					salaryDetails += jobPayrollElement
							.getPayrollElementByPayrollElementId()
							.getElementName()
							+ " of AED " + jobPayrollElement.getAmount();

					salaryDetailsAR += "  من درهم إماراتي  "
							+ jobPayrollElement.getAmount() + " " + name;
				} else {
					salaryDetails += " ,"
							+ jobPayrollElement
									.getPayrollElementByPayrollElementId()
									.getElementName() + " of AED "
							+ jobPayrollElement.getAmount();

					salaryDetailsAR += "  من درهم إماراتي  "
							+ jobPayrollElement.getAmount() + " " + name;
				}
				count++;
			}
			jobAssignmentVO.setSalaryDetails(salaryDetails);
			jobAssignmentVO.setSalaryDetailsAR(salaryDetailsAR);
			jobAssignmentVO.setCrossSalary(corssSalary);

			// Set Accommodation Details
			List<LookupDetail> accomodationOptions = jobBL.getLookupMasterBL()
					.getActiveLookupDetails("ACCOMODATION_OPTIONS", true);
			Map<String, LookupDetail> map = new HashMap<String, LookupDetail>();
			for (LookupDetail el : accomodationOptions) {
				map.put(el.getAccessCode(), el);
			}
			LookupDetail detail = map.get(jobAssignment.getLookupKey());

			if (detail != null) {
				jobAssignmentVO.setAccomodation(detail.getDisplayName());
				jobAssignmentVO.setAccomodationAR(AIOSCommons
						.bytesToUTFString(detail.getDisplayNameArabic()));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return jobAssignmentVO;
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public List<String> fileReader(String fileName) {
		File file = new File(fileName);
		List<String> files = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			boolean exists = file.exists();
			if (exists) {
				for (String line1 = br.readLine(); line1 != null; line1 = br
						.readLine()) {
					files.add(line1);
				}
			}
			return files;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<JobShiftVO> fetchLocationBasedShifts(Long locationId,
			Date fromDate, Date toDate) {
		List<JobShiftVO> jobShiftsVO = new ArrayList<JobShiftVO>();
		try {

			// By Assignments
			List<JobShift> jobShifts = jobBL.getJobService()
					.getAllJobShiftWithLocationAndDateByAssignment(locationId);

			jobShiftsVO.addAll(convertAllShiftEntitiesToVo(jobShifts));
			List<JobShiftVO> jobShiftsVOTemp = new ArrayList<JobShiftVO>(
					jobShiftsVO);

			LocalDate startDate = new LocalDate(fromDate);
			LocalDate endDate = new LocalDate(toDate);
			for (JobShiftVO jobShiftVO : jobShiftsVOTemp) {
				if (!checkDate(startDate.toDate(), endDate.toDate(),
						jobShiftVO.getDate()))
					jobShiftsVO.remove(jobShiftVO);

			}

			Collections.sort(jobShiftsVO, new Comparator<JobShiftVO>() {
				public int compare(JobShiftVO m1, JobShiftVO m2) {
					return m1.getEmployeeName().compareTo(m2.getEmployeeName());
				}
			});

			Collections.sort(jobShiftsVO, new Comparator<JobShiftVO>() {
				public int compare(JobShiftVO m1, JobShiftVO m2) {
					return m1.getDate().compareTo(m2.getDate());
				}
			});

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return jobShiftsVO;
	}

	public List<JobShiftVO> fetchAssignmentBasedShifts(
			List<JobAssignment> jobAssignments, Date fromDate, Date toDate) {
		List<JobShiftVO> jobShiftsVOFinal = new ArrayList<JobShiftVO>();
		try {

			JobShiftVO vo = null;
			LocalDate startDate = new LocalDate(fromDate);
			LocalDate endDate = new LocalDate(toDate);
			Map<Date, String> periodDateMap = new HashMap<Date, String>();
			endDate = endDate.plusDays(1);
			while (startDate.isBefore(endDate)) {
				periodDateMap.put(startDate.toDate(), DateTimeFormat
						.forPattern("dd-MMM-yyyy").print(startDate));
				startDate = startDate.plusDays(1);
			}

			for (JobAssignment ja : jobAssignments) {
				/*
				 * // By Jobs List<JobShift> jobShifts = jobBL.getJobService()
				 * .getAllJobShiftByJobAndJobAssignment(
				 * ja.getJobAssignmentId());
				 * jobShiftsVO.addAll(convertAllShiftEntitiesToVo(jobShifts));
				 */

				// By Assignments
				List<JobShift> jobShifts = jobBL.getJobService()
						.getAllJobShiftByJobAssignment(ja.getJobAssignmentId());

				List<JobShiftVO> jobShiftsVO = convertAllShiftEntitiesToVo(ja,
						jobShifts, periodDateMap);

				Map<Date, JobShiftVO> jobShitVoMap = new HashMap<Date, JobShiftVO>();
				for (JobShiftVO jobShift : jobShiftsVO) {
					if (periodDateMap.containsKey(jobShift.getDate())) {
						jobShitVoMap.put(jobShift.getDate(), jobShift);
					}
				}

				for (EmployeeCalendar employeeCalendar : ja
						.getEmployeeCalendars()) {

					for (EmployeeCalendarPeriod calPeriod : employeeCalendar
							.getEmployeeCalendarPeriods()) {
						if (calPeriod.getFromDate() == null
								|| calPeriod.getToDate() == null)
							continue;

						LocalDate startDate1 = new LocalDate(
								calPeriod.getFromDate());
						LocalDate endDate1 = new LocalDate(
								calPeriod.getToDate());
						endDate1 = endDate1.plusDays(1);
						while (startDate1.isBefore(endDate1)) {
							if (periodDateMap.containsKey(startDate1.toDate())) {
								vo = jobShitVoMap.get(startDate1.toDate());
								jobShiftsVO.remove(vo);
								vo = new JobShiftVO();
								vo.setEmployeeName(ja.getPerson()
										.getFirstName()
										+ " "
										+ ja.getPerson().getLastName());
								vo.setJobAssignment(ja);
								vo.setDateView(DateTimeFormat.forPattern(
										"dd-MMM-yyyy").print(startDate1));
								vo.setDate(startDate1.toDate());
								vo.setDayName(DateTimeFormat.forPattern("EEEE")
										.print(startDate1));
								vo.setOff(true);
								vo.setOffType("OFF");
								jobShiftsVO.add(vo);
								jobShitVoMap.put(startDate1.toDate(), vo);
							}
							startDate1 = startDate1.plusDays(1);
						}
					}
				}

				LocalDate dt = null;
				for (Map.Entry<Date, String> entry : periodDateMap.entrySet()) {
					if (!jobShitVoMap.containsKey(entry.getKey())) {
						vo = new JobShiftVO();
						dt = new LocalDate(entry.getKey());
						vo.setEmployeeName(ja.getPerson().getFirstName() + " "
								+ ja.getPerson().getLastName());
						vo.setJobAssignment(ja);
						vo.setDateView(DateTimeFormat.forPattern("dd-MMM-yyyy")
								.print(dt));
						vo.setDate(dt.toDate());
						vo.setDayName(DateTimeFormat.forPattern("EEEE").print(
								dt));
						vo.setOff(false);
						vo.setOffType("-NA-");
						jobShiftsVO.add(vo);
						jobShitVoMap.put(dt.toDate(), vo);
					}
				}
				jobShiftsVOFinal.addAll(jobShiftsVO);
			}

			Collections.sort(jobShiftsVOFinal, new Comparator<JobShiftVO>() {
				public int compare(JobShiftVO m1, JobShiftVO m2) {
					return m1.getDate().compareTo(m2.getDate());
				}
			});

			Collections.sort(jobShiftsVOFinal, new Comparator<JobShiftVO>() {
				public int compare(JobShiftVO m1, JobShiftVO m2) {
					return m1.getEmployeeName().compareTo(m2.getEmployeeName());
				}
			});

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return jobShiftsVOFinal;
	}

	public Boolean checkDate(Date startDate, Date endDate, Date checkDate) {
		/*
		 * Interval interval = new Interval(new
		 * DateTime(startDate).minusDays(1), new DateTime(
		 * endDate).plusDays(1));
		 */
		if (startDate.compareTo(checkDate) == 0
				&& checkDate.compareTo(endDate) == 0)
			return true;
		return false;
	}

	public List<JobShiftVO> convertAllShiftEntitiesToVo(JobAssignment ja,
			List<JobShift> jobShifts, Map<Date, String> periodMaps) {
		List<JobShiftVO> jobShiftsVO = new ArrayList<JobShiftVO>();
		try {
			JobShiftVO vo = null;
			for (JobShift jobShift : jobShifts) {
				if (jobShift.getEffectiveStartDate() == null
						|| jobShift.getEffectiveEndDate() == null)
					continue;

				LocalDate startDate = new LocalDate(
						jobShift.getEffectiveStartDate());
				LocalDate endDate = new LocalDate(
						jobShift.getEffectiveEndDate());
				endDate = endDate.plusDays(1);
				while (startDate.isBefore(endDate)) {
					if (periodMaps.containsKey(startDate.toDate())) {
						vo = new JobShiftVO();
						BeanUtils.copyProperties(vo, jobShift);
						vo.setEmployeeName(ja.getPerson().getFirstName() + " "
								+ ja.getPerson().getLastName());
						if (jobShift.getWorkingShift().getCmpDeptLocation() != null) {
							vo.setLocationName(jobShift.getWorkingShift()
									.getCmpDeptLocation().getLocation()
									.getLocationName());
						} else {
							vo.setLocationName("");
						}
						vo.setStartTime(DateFormat.convertTimeToString(jobShift
								.getWorkingShift().getStartTime() + ""));
						vo.setEndTime(DateFormat.convertTimeToString(jobShift
								.getWorkingShift().getEndTime() + ""));
						vo.setDateView(DateTimeFormat.forPattern("dd-MMM-yyyy")
								.print(startDate));
						vo.setDate(startDate.toDate());
						vo.setDayName(DateTimeFormat.forPattern("EEEE").print(
								startDate));
						LocalTime tim = new LocalTime(jobShift
								.getWorkingShift().getStartTime());
						if (tim.hourOfDay().get() > 12)
							vo.setShiftShortName("PM");
						else
							vo.setShiftShortName("AM");
						jobShiftsVO.add(vo);

					}
					startDate = startDate.plusDays(1);

				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return jobShiftsVO;
	}

	public List<JobShiftVO> convertAllShiftEntitiesToVo(List<JobShift> jobShifts) {
		List<JobShiftVO> jobShiftsVO = new ArrayList<JobShiftVO>();
		try {
			JobShiftVO vo = null;
			for (JobShift jobShift : jobShifts) {
				if (jobShift.getEffectiveStartDate() == null
						|| jobShift.getEffectiveEndDate() == null)
					continue;

				if (jobShift.getJob() != null) {
					for (JobAssignment ja : jobShift.getJob()
							.getJobAssignments()) {
						LocalDate startDate = new LocalDate(
								jobShift.getEffectiveStartDate());
						LocalDate endDate = new LocalDate(
								jobShift.getEffectiveEndDate());
						endDate = endDate.plusDays(1);
						while (startDate.isBefore(endDate)) {
							vo = new JobShiftVO();
							BeanUtils.copyProperties(vo, jobShift);
							vo.setEmployeeName(ja.getPerson().getFirstName()
									+ " " + ja.getPerson().getLastName());
							vo.setLocationName(jobShift.getWorkingShift()
									.getCmpDeptLocation().getLocation()
									.getLocationName());
							vo.setStartTime(DateFormat
									.convertTimeToString(jobShift
											.getWorkingShift().getStartTime()
											+ ""));
							vo.setEndTime(DateFormat
									.convertTimeToString(jobShift
											.getWorkingShift().getEndTime()
											+ ""));
							vo.setDateView(startDate.toString());
							vo.setDate(startDate.toDate());
							vo.setDayName(DateTimeFormat.forPattern("EEEE")
									.print(startDate));
							LocalTime tim = new LocalTime(jobShift
									.getWorkingShift().getStartTime());
							if (tim.hourOfDay().get() > 12)
								vo.setShiftShortName("PM");
							else
								vo.setShiftShortName("AM");
							startDate = startDate.plusDays(1);
							jobShiftsVO.add(vo);
						}

					}
				} else {
					LocalDate startDate = new LocalDate(
							jobShift.getEffectiveStartDate());
					LocalDate endDate = new LocalDate(
							jobShift.getEffectiveEndDate());
					endDate = endDate.plusDays(1);
					while (startDate.isBefore(endDate)) {
						vo = new JobShiftVO();
						BeanUtils.copyProperties(vo, jobShift);
						vo.setEmployeeName(jobShift.getJobAssignment()
								.getPerson().getFirstName()
								+ " "
								+ jobShift.getJobAssignment().getPerson()
										.getLastName());
						if (jobShift.getWorkingShift().getCmpDeptLocation() != null) {
							vo.setLocationName(jobShift.getWorkingShift()
									.getCmpDeptLocation().getLocation()
									.getLocationName());
						} else {
							vo.setLocationName("");
						}
						vo.setStartTime(DateFormat.convertTimeToString(jobShift
								.getWorkingShift().getStartTime() + ""));
						vo.setEndTime(DateFormat.convertTimeToString(jobShift
								.getWorkingShift().getEndTime() + ""));
						vo.setDateView(startDate.toString());
						vo.setDate(startDate.toDate());
						vo.setDayName(DateTimeFormat.forPattern("EEEE").print(
								startDate));
						LocalTime tim = new LocalTime(jobShift
								.getWorkingShift().getStartTime());
						if (tim.hourOfDay().get() > 12)
							vo.setShiftShortName("PM");
						else
							vo.setShiftShortName("AM");
						startDate = startDate.plusDays(1);
						jobShiftsVO.add(vo);
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return jobShiftsVO;
	}

	public void setJobAssignmentService(
			JobAssignmentService jobAssignmentService) {
		this.jobAssignmentService = jobAssignmentService;
	}

	public JobBL getJobBL() {
		return jobBL;
	}

	public void setJobBL(JobBL jobBL) {
		this.jobBL = jobBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public DesignationBL getDesignationBL() {
		return designationBL;
	}

	public void setDesignationBL(DesignationBL designationBL) {
		this.designationBL = designationBL;
	}

	public CmpDeptLocBL getCmpDeptLocBL() {
		return cmpDeptLocBL;
	}

	public void setCmpDeptLocBL(CmpDeptLocBL cmpDeptLocBL) {
		this.cmpDeptLocBL = cmpDeptLocBL;
	}

	public PersonBL getPersonBL() {
		return personBL;
	}

	public void setPersonBL(PersonBL personBL) {
		this.personBL = personBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public LeaveProcessService getLeaveProcessService() {
		return leaveProcessService;
	}

	public void setLeaveProcessService(LeaveProcessService leaveProcessService) {
		this.leaveProcessService = leaveProcessService;
	}

	public EmployeeCalendarService getEmployeeCalendarService() {
		return employeeCalendarService;
	}

	public void setEmployeeCalendarService(
			EmployeeCalendarService employeeCalendarService) {
		this.employeeCalendarService = employeeCalendarService;
	}

	public PayrollElementService getPayrollElementService() {
		return payrollElementService;
	}

	public void setPayrollElementService(
			PayrollElementService payrollElementService) {
		this.payrollElementService = payrollElementService;
	}

}
