package com.aiotech.aios.hr.service.bl;

import com.aiotech.aios.hr.domain.entity.MileageLog;
import com.aiotech.aios.hr.service.FuelAllowanceService;
import com.aiotech.aios.hr.service.MileageLogService;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;

public class MileageLogBL {
	private MileageLogService mileageLogService;
	private FuelAllowanceService fuelAllowanceService;

	public void doMileageLogBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		MileageLog mileageLog = fuelAllowanceService
				.getMileageLogById(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			fuelAllowanceService.deleteMileageLog(mileageLog);
		} else {
		}
	}

	public MileageLogService getMileageLogService() {
		return mileageLogService;
	}

	public void setMileageLogService(MileageLogService mileageLogService) {
		this.mileageLogService = mileageLogService;
	}

	
	public FuelAllowanceService getFuelAllowanceService() {
		return fuelAllowanceService;
	}

	public void setFuelAllowanceService(
			FuelAllowanceService fuelAllowanceService) {
		this.fuelAllowanceService = fuelAllowanceService;
	}

}
