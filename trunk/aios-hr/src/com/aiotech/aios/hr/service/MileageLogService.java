package com.aiotech.aios.hr.service;

import com.aiotech.aios.hr.domain.entity.MileageLog;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class MileageLogService {
	public AIOTechGenericDAO<MileageLog> mileageLogDAO;

	public AIOTechGenericDAO<MileageLog> getMileageLogDAO() {
		return mileageLogDAO;
	}

	public void setMileageLogDAO(AIOTechGenericDAO<MileageLog> mileageLogDAO) {
		this.mileageLogDAO = mileageLogDAO;
	}

}
