package com.aiotech.aios.hr.service;

import com.aiotech.aios.hr.domain.entity.InterviewProcess;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class InterviewProcessService {

	public AIOTechGenericDAO<InterviewProcess> interviewProcessDAO;

	public AIOTechGenericDAO<InterviewProcess> getInterviewProcessDAO() {
		return interviewProcessDAO;
	}

	public void setInterviewProcessDAO(
			AIOTechGenericDAO<InterviewProcess> interviewProcessDAO) {
		this.interviewProcessDAO = interviewProcessDAO;
	}
}
