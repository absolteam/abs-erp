package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.IdentityAvailability;
import com.aiotech.aios.hr.domain.entity.IdentityAvailabilityMaster;
import com.aiotech.aios.hr.domain.entity.vo.IdentityAvailabilityMasterVO;
import com.aiotech.aios.hr.domain.entity.vo.IdentityAvailabilityVO;
import com.aiotech.aios.hr.service.IdentityAvailabilityMasterService;
import com.aiotech.aios.hr.service.IdentityAvailabilityService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class IdentityAvailabilityMasterBL {
	private IdentityAvailabilityService identityAvailabilityService;
	private CommentBL commentBL;
	private SystemService systemService;
	private AlertBL alertBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private IdentityAvailabilityMasterService identityAvailabilityMasterService;
	private Implementation implementation;

	public void saveIdentityAvailability(
			IdentityAvailabilityMaster identityAvailabilityMaster,
			List<IdentityAvailability> identityAvailabilities,
			List<IdentityAvailability> deleteIdentityAvailabilities,
			IdentityAvailabilityVO iaVO) {
		try {
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (identityAvailabilityMaster != null
					&& identityAvailabilityMaster
							.getIdentityAvailabilityMasterId() != null
					&& identityAvailabilityMaster
							.getIdentityAvailabilityMasterId() > 0) {

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());

			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());

				identityAvailabilityMaster.setReferenceNumber(SystemBL.getReferenceStamp(
						IdentityAvailabilityMaster.class.getName(),
						this.getImplementation()));
			}
			workflowDetailVo.setRejectAlertId(iaVO.getAlertId());

			if (deleteIdentityAvailabilities != null
					&& deleteIdentityAvailabilities.size() > 0)
				identityAvailabilityMasterService
						.deleteIdentityAvailabilities(deleteIdentityAvailabilities);
			identityAvailabilityMasterService
					.saveIdentityAvailabilityMaster(identityAvailabilityMaster);

			for (IdentityAvailability identityAvailability : identityAvailabilities) {
				identityAvailability
						.setIdentityAvailabilityMaster(identityAvailabilityMaster);
			}
			identityAvailabilityMasterService
					.saveIdentityAvailabilities(identityAvailabilities);

			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					IdentityAvailabilityMaster.class.getSimpleName(),
					identityAvailabilityMaster
							.getIdentityAvailabilityMasterId(), user,
					workflowDetailVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void deleteIdentityAvailability(Long identityAvailabilityId,
			Long messageId) throws Exception {

		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				IdentityAvailability.class.getSimpleName(),
				identityAvailabilityId, user, workflowDetailVo);
	}

	public void deleteIdentityAvailability(Long identityAvailabilityId)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				IdentityAvailabilityMaster.class.getSimpleName(),
				identityAvailabilityId, user, workflowDetailVo);
	}

	public void doIdentityAvailabilityMasterBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		IdentityAvailabilityMaster identityAvailabilityMaster = identityAvailabilityMasterService
				.getIdentityAvailabilityMasterInfo(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			
			identityAvailabilityMasterService
					.deleteIdentityAvailabilities(new ArrayList<IdentityAvailability>(
							identityAvailabilityMaster
									.getIdentityAvailabilities()));
			identityAvailabilityMasterService
			.deleteIdentityAvailabilityMaster(identityAvailabilityMaster);
		} else {
		}
	}

	public List<IdentityAvailabilityMasterVO> convertEntitiesTOVOs(
			List<IdentityAvailabilityMaster> identityAvailabilities)
			throws Exception {

		List<IdentityAvailabilityMasterVO> tosList = new ArrayList<IdentityAvailabilityMasterVO>();

		for (IdentityAvailabilityMaster pers : identityAvailabilities) {
			tosList.add(convertEntityToVO(pers));
		}

		return tosList;
	}

	public IdentityAvailabilityMasterVO convertEntityToVO(
			IdentityAvailabilityMaster identityAvailability) throws Exception {
		IdentityAvailabilityMasterVO identityAvailabilityVO = new IdentityAvailabilityMasterVO();
		BeanUtils.copyProperties(identityAvailabilityVO, identityAvailability);
		if (identityAvailability.getHandoverDate() != null)
			identityAvailabilityVO.setHandoverDateStr(DateFormat
					.convertDateToString(identityAvailability.getHandoverDate()
							+ ""));
		identityAvailabilityVO.setDepartment(identityAvailability
				.getDepartment());
		identityAvailabilityVO.setPurpose(identityAvailability.getPurpose());
		identityAvailabilityVO.setDescription(identityAvailability
				.getDescription());
		identityAvailabilityVO.setReferenceNumber(identityAvailability
				.getReferenceNumber());

		if (identityAvailability.getStatus() != null
				&& (byte) identityAvailability.getStatus() == (byte) 1)
			identityAvailabilityVO.setStatusStr("Employer to Employee");
		else if (identityAvailability.getStatus() != null
				&& (byte) identityAvailability.getStatus() == (byte) 2)
			identityAvailabilityVO.setStatusStr("Employer to Employee");
		else
			identityAvailabilityVO.setStatusStr("Others");

		if (identityAvailability.getCreatedDate() != null)
			identityAvailabilityVO.setCreatedDateStr(DateFormat
					.convertDateToString(identityAvailability.getCreatedDate()
							+ ""));

		if (identityAvailability.getPersonByHandoverTo() != null)
			identityAvailabilityVO.setPersonName(identityAvailability
					.getPersonByHandoverTo().getFirstName()
					+ " "
					+ identityAvailability.getPersonByHandoverTo()
							.getLastName());
		else
			identityAvailabilityVO
					.setPersonName(identityAvailability.getName());

		if (identityAvailability.getPersonByCreatedBy() != null)
			identityAvailabilityVO
					.setCreatedPerson(identityAvailability
							.getPersonByCreatedBy().getFirstName()
							+ " "
							+ identityAvailability.getPersonByCreatedBy()
									.getLastName());
		else
			identityAvailabilityVO.setCreatedPerson("");

		identityAvailabilityVO
				.setIdentityAvailabilityVOs(convertChildEntitiesTOVOs(new ArrayList<IdentityAvailability>(
						identityAvailability.getIdentityAvailabilities())));

		return identityAvailabilityVO;
	}

	public List<IdentityAvailabilityVO> convertChildEntitiesTOVOs(
			List<IdentityAvailability> identityAvailabilities) throws Exception {

		List<IdentityAvailabilityVO> tosList = new ArrayList<IdentityAvailabilityVO>();

		for (IdentityAvailability pers : identityAvailabilities) {
			tosList.add(convertChildEntityToVO(pers));
		}

		return tosList;
	}

	public IdentityAvailabilityVO convertChildEntityToVO(
			IdentityAvailability identityAvailability) {
		IdentityAvailabilityVO identityAvailabilityVO = new IdentityAvailabilityVO();

		identityAvailabilityVO.setIdentityAvailabilityId(identityAvailability
				.getIdentityAvailabilityId());
		identityAvailabilityVO.setIdentity(identityAvailability.getIdentity());
		identityAvailabilityVO.setActualReturnDate(identityAvailability
				.getActualReturnDate());
		identityAvailabilityVO.setHandoverDate(identityAvailability
				.getHandoverDate());

		identityAvailabilityVO.setExpectedReturnDate(identityAvailability
				.getExpectedReturnDate());
		identityAvailabilityVO.setImplementation(identityAvailability
				.getImplementation());

		identityAvailabilityVO.setPurpose(identityAvailability.getPurpose());

		if (identityAvailability.getIdentity().getLookupDetail() != null)
			identityAvailabilityVO.setIdentityType(identityAvailability
					.getIdentity().getLookupDetail().getDisplayName());

		if (identityAvailability.getIdentity() != null) {
			if (identityAvailability.getIdentity().getPerson() != null)
				identityAvailabilityVO.setIdentityName(identityAvailability
						.getIdentity().getPerson().getFirstName()
						+ " "
						+ identityAvailability.getIdentity().getPerson()
								.getLastName()
						+ "["
						+ identityAvailability.getIdentity().getPerson()
								.getPersonNumber() + "]");
			else
				identityAvailabilityVO.setIdentityName(identityAvailability
						.getIdentity().getCompany().getCompanyName()
						+ "");
		} else {
			identityAvailabilityVO.setIdentityName(identityAvailability
					.getDocumentName());
		}

		if (identityAvailability.getHandoverDate() != null)
			identityAvailabilityVO.setHandoverDateStr(DateFormat
					.convertDateToString(identityAvailability.getHandoverDate()
							+ ""));

		if (identityAvailability.getExpectedReturnDate() != null)
			identityAvailabilityVO.setExpectedReturnDateStr(DateFormat
					.convertDateToString(identityAvailability
							.getExpectedReturnDate() + ""));

		if (identityAvailability.getActualReturnDate() != null)
			identityAvailabilityVO.setActualReturnDateStr(DateFormat
					.convertDateToString(identityAvailability
							.getActualReturnDate() + ""));

		return identityAvailabilityVO;
	}

	public IdentityAvailabilityService getIdentityAvailabilityService() {
		return identityAvailabilityService;
	}

	public void setIdentityAvailabilityService(
			IdentityAvailabilityService identityAvailabilityService) {
		this.identityAvailabilityService = identityAvailabilityService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public IdentityAvailabilityMasterService getIdentityAvailabilityMasterService() {
		return identityAvailabilityMasterService;
	}

	public void setIdentityAvailabilityMasterService(
			IdentityAvailabilityMasterService identityAvailabilityMasterService) {
		this.identityAvailabilityMasterService = identityAvailabilityMasterService;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

}
