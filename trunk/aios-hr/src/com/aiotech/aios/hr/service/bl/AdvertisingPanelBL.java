package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.AdvertisingPanel;
import com.aiotech.aios.hr.domain.entity.vo.AdvertisingPanelVO;
import com.aiotech.aios.hr.service.AdvertisingPanelService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class AdvertisingPanelBL {
	private AdvertisingPanelService advertisingPanelService;
	private LocationBL locationBL;
	private LookupMasterBL lookupMasterBL;
	private Implementation implementation;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public String getRefereceNum() {
		String referenceStamp = SystemBL.getReferenceStamp(
				AdvertisingPanel.class.getName(), getImplementation());
		return referenceStamp;
	}

	public String saveReferenceNum() {
		String referenceStamp = SystemBL.saveReferenceStamp(
				AdvertisingPanel.class.getName(), getImplementation());
		return referenceStamp;
	}

	public AdvertisingPanelService getAdvertisingPanelService() {
		return advertisingPanelService;
	}

	public void setAdvertisingPanelService(
			AdvertisingPanelService advertisingPanelService) {
		this.advertisingPanelService = advertisingPanelService;
	}

	@SuppressWarnings("unused")
	public List<AdvertisingPanelVO> convertAllEntityToVO(
			List<AdvertisingPanel> advertisingPanels) throws Exception {
		List<AdvertisingPanelVO> advertisingPanelVOs = null;
		if (advertisingPanelVOs != null && advertisingPanelVOs.size() > 0) {
			advertisingPanelVOs = new ArrayList<AdvertisingPanelVO>();
			for (AdvertisingPanel advertisingPanel : advertisingPanels) {
				advertisingPanelVOs.add(convertEntityIntoVO(advertisingPanel));
			}
		}
		return advertisingPanelVOs;
	}

	public AdvertisingPanelVO convertEntityIntoVO(
			AdvertisingPanel advertisingPanel) throws Exception {
		AdvertisingPanelVO advertisingPanelVO = new AdvertisingPanelVO();
		// Copy EmployeeLoan class properties value into VO properties.
		BeanUtils.copyProperties(advertisingPanelVO, advertisingPanel);
		if (advertisingPanel.getPostedDate() != null)
			advertisingPanelVO
					.setPostedDateDisplay(DateFormat
							.convertDateToString(advertisingPanel
									.getPostedDate() + ""));

		return advertisingPanelVO;
	}

	public void saveAdvertisingPanel(AdvertisingPanel advertisingPanel)
			throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		advertisingPanel.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (advertisingPanel != null
				&& advertisingPanel.getAdvertisingPanelId() != null
				&& advertisingPanel.getAdvertisingPanelId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		advertisingPanelService.saveAdvertisingPanel(advertisingPanel);
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				AdvertisingPanel.class.getSimpleName(),
				advertisingPanel.getAdvertisingPanelId(), user,
				workflowDetailVo);
	}

	public void deleteAdvertisingPanel(AdvertisingPanel advertisingPanel)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				AdvertisingPanel.class.getSimpleName(),
				advertisingPanel.getAdvertisingPanelId(), user,
				workflowDetailVo);
	}

	public void doAdvertisingPanelBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		AdvertisingPanel advertisingPanel = this.getAdvertisingPanelService()
				.getAdvertisingPanelById(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			this.getAdvertisingPanelService().deleteAdvertisingPanel(
					advertisingPanel);
		} else {
		}
	}

	public Map<Integer, String> statusType() {
		Map<Integer, String> types = new HashMap<Integer, String>();
		types.put(1, "Posted");
		types.put(2, "Pending");
		types.put(3, "Closed");
		types.put(4, "Suspended");
		return types;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public LocationBL getLocationBL() {
		return locationBL;
	}

	public void setLocationBL(LocationBL locationBL) {
		this.locationBL = locationBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
