package com.aiotech.aios.hr.service.bl;

import java.util.HashMap;
import java.util.Map;

import com.aiotech.aios.hr.domain.entity.Grade;
import com.aiotech.aios.hr.service.GradeService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class GradeBL {
	
	private GradeService gradeService;
	private AttendancePolicyBL attendancePolicyBL;
	private LeavePolicyBL leavePolicyBL;
	private LookupMasterBL lookupMasterBL;
	private PayPeriodBL payPeriodBL;
	private JobAssignmentBL jobAssignmentBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	public void saveGrade(Grade grade) throws Exception{
		/*Grade toEdit=null;
		if(grade.getGradeId()!=null && grade.getGradeId()>0){
			toEdit=new Grade();
			toEdit=gradeService.getGradeInfo(grade.getGradeId());
		}*/
		Map<String, Object> sessionObj = ActionContext.getContext()
		.getSession();
		User user = (User) sessionObj.get("USER");

		grade.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (grade != null
				&& grade.getGradeId() != null
				&& grade.getGradeId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		gradeService.saveGrade(grade);
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Grade.class.getSimpleName(),
				grade.getGradeId(), user,
				workflowDetailVo);
	}
	

	
	public void deleteGrade(Long gradeId) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Grade.class.getSimpleName(), gradeId,
				user, workflowDetailVo);
		}
		
	public void doGradeBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		Grade grade=gradeService.getGradeInfo(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			gradeService.deleteGrade(grade);
		} else {
		}
	}

	public Map<String, String> evaluationPeriod() {
		Map<String, String> types = new HashMap<String, String>();
		types.put("Y", "Yearly");
		types.put("M", "Monthly");
		types.put("W", "Weekly");
		types.put("D", "Daily");
		return types;
	}
	
	public Map<String, String> ticketClass() {
		Map<String, String> types = new HashMap<String, String>();
		types.put("E", "Economy");
		types.put("B", "Business");
		types.put("C", "Coral Economy");
		types.put("O", "Other");
		return types;
	}
	
	public Grade findGradeByJobAssignmentId(Long jobAssignmentId){
		return gradeService.findGradeByJobAssignmentId(jobAssignmentId);
	}

	public GradeService getGradeService() {
		return gradeService;
	}

	public void setGradeService(GradeService gradeService) {
		this.gradeService = gradeService;
	}

	public AttendancePolicyBL getAttendancePolicyBL() {
		return attendancePolicyBL;
	}

	public void setAttendancePolicyBL(AttendancePolicyBL attendancePolicyBL) {
		this.attendancePolicyBL = attendancePolicyBL;
	}

	public LeavePolicyBL getLeavePolicyBL() {
		return leavePolicyBL;
	}

	public void setLeavePolicyBL(LeavePolicyBL leavePolicyBL) {
		this.leavePolicyBL = leavePolicyBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public PayPeriodBL getPayPeriodBL() {
		return payPeriodBL;
	}

	public void setPayPeriodBL(PayPeriodBL payPeriodBL) {
		this.payPeriodBL = payPeriodBL;
	}

	public JobAssignmentBL getJobAssignmentBL() {
		return jobAssignmentBL;
	}

	public void setJobAssignmentBL(JobAssignmentBL jobAssignmentBL) {
		this.jobAssignmentBL = jobAssignmentBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

}
