package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.OpenPosition;
import com.aiotech.aios.hr.domain.entity.RecruitmentSource;
import com.aiotech.aios.hr.domain.entity.vo.RecruitmentSourceVO;
import com.aiotech.aios.hr.service.RecruitmentSourceService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class RecruitmentSourceBL {

	private RecruitmentSourceService recruitmentSourceService;
	private LocationBL locationBL;
	private LookupMasterBL lookupMasterBL;
	private DocumentBL documentBL;
	private DirectoryBL directoryBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	@SuppressWarnings("unused")
	public List<RecruitmentSourceVO> convertAllEntityToVO(
			List<RecruitmentSource> recruitmentSources) throws Exception {
		List<RecruitmentSourceVO> recruitmentSourceVOs = null;
		if (recruitmentSourceVOs != null && recruitmentSourceVOs.size() > 0) {
			recruitmentSourceVOs = new ArrayList<RecruitmentSourceVO>();
			for (RecruitmentSource recruitmentSource : recruitmentSources) {
				recruitmentSourceVOs
						.add(convertEntityIntoVO(recruitmentSource));
			}
		}
		return recruitmentSourceVOs;
	}

	public RecruitmentSourceVO convertEntityIntoVO(
			RecruitmentSource recruitmentSource) throws Exception {
		RecruitmentSourceVO recruitmentSourceVO = new RecruitmentSourceVO();
		// Copy EmployeeLoan class properties value into VO properties.
		BeanUtils.copyProperties(recruitmentSourceVO, recruitmentSource);
		recruitmentSourceVO.setCreatedDateDisplay(DateFormat
				.convertDateToString(recruitmentSource.getCreatedDate() + ""));
		if (recruitmentSource.getLocation() != null)
			recruitmentSourceVO.setFullAddress((locationBL
					.getLocationFullAddress(recruitmentSource.getLocation()
							.getLocationId()).getFullAddress()));
		return recruitmentSourceVO;
	}

	public void saveDocuments(RecruitmentSource recruitmentSource,
			boolean editFlag) throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(recruitmentSource,
				"uploadedDocs", editFlag);
		Map<String, String> dirs = docVO.getDirMap();

		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, recruitmentSource.getRecruitmentSourceId(),
					"RecruitmentSource", "uploadedDocs");
			docVO.setDirStruct(dirStucture);
		}

		documentBL.saveUploadDocuments(docVO);
	}

	public void saveRecruitementSource(RecruitmentSource recruitmentSource) {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			recruitmentSource
					.setIsApprove((byte) WorkflowConstants.Status.Published
							.getCode());
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (recruitmentSource != null
					&& recruitmentSource.getRecruitmentSourceId() != null
					&& recruitmentSource.getRecruitmentSourceId() > 0) {

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			if (recruitmentSource != null
					&& recruitmentSource.getRecruitmentSourceId() != null
					&& recruitmentSource.getRecruitmentSourceId() > 0) {

				recruitmentSourceService
						.saveRecruitmentSource(recruitmentSource);
				saveDocuments(recruitmentSource, true);

			} else {
				recruitmentSourceService
						.saveRecruitmentSource(recruitmentSource);
				saveDocuments(recruitmentSource, false);
			}
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					RecruitmentSource.class.getSimpleName(),
					recruitmentSource.getRecruitmentSourceId(), user,
					workflowDetailVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteRecruitmentSource(RecruitmentSource recruitmentSource)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				RecruitmentSource.class.getSimpleName(),
				recruitmentSource.getRecruitmentSourceId(), user,
				workflowDetailVo);
	}

	public void doRecruitmentSourceBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		RecruitmentSource recruitmentSource = this
				.getRecruitmentSourceService().getRecruitmentSourceById(
						recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			recruitmentSourceService.deleteRecruitmentSource(recruitmentSource);
		} else {
		}
	}

	public RecruitmentSourceService getRecruitmentSourceService() {
		return recruitmentSourceService;
	}

	public void setRecruitmentSourceService(
			RecruitmentSourceService recruitmentSourceService) {
		this.recruitmentSourceService = recruitmentSourceService;
	}

	public LocationBL getLocationBL() {
		return locationBL;
	}

	public void setLocationBL(LocationBL locationBL) {
		this.locationBL = locationBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
