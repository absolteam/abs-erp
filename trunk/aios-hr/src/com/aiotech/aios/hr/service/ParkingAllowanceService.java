package com.aiotech.aios.hr.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.hr.domain.entity.ParkingAllowance;
import com.aiotech.aios.hr.domain.entity.vo.ParkingAllowanceVO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ParkingAllowanceService {
	public AIOTechGenericDAO<ParkingAllowance> parkingAllowanceDAO;

	public List<ParkingAllowance> getAllParkingAllowanceByCriteria(
			ParkingAllowanceVO vO) {
		Criteria jobPayrollCriteria = parkingAllowanceDAO.createCriteria();

		jobPayrollCriteria.createAlias("jobAssignment", "ja");
		jobPayrollCriteria.createAlias("lookupDetailByParkingType",
				"parkingType", CriteriaSpecification.LEFT_JOIN);
		jobPayrollCriteria.createAlias("lookupDetailByCardCompany", "company",
				CriteriaSpecification.LEFT_JOIN);
		jobPayrollCriteria.createAlias("lookupDetailByCardType", "cardType",
				CriteriaSpecification.LEFT_JOIN);
		jobPayrollCriteria.createAlias("jobPayrollElement", "jpa");
		jobPayrollCriteria.createAlias("jpa.payrollElementByPayrollElementId",
				"pe");
		jobPayrollCriteria.createAlias("jpa.payrollElementByPercentageElement",
				"percentage", CriteriaSpecification.LEFT_JOIN);
		if (vO.getPayPeriodTransaction() != null
				&& vO.getPayPeriodTransaction().getStartDate() != null) {
			jobPayrollCriteria.add(Restrictions.conjunction().add(
					Restrictions.ge("fromDate", vO.getPayPeriodTransaction()
							.getStartDate())));
			jobPayrollCriteria.add(Restrictions.conjunction().add(
					Restrictions.le("toDate", vO.getPayPeriodTransaction()
							.getEndDate())));
		}

		if (vO.getJobAssignmentId() != null) {
			jobPayrollCriteria.add(Restrictions.eq("ja.jobAssignmentId",
					vO.getJobAssignmentId()));
		}

		if (vO.getJobPayrollElementId() != null) {
			jobPayrollCriteria.add(Restrictions.eq("jpa.jobPayrollElementId",
					vO.getJobPayrollElementId()));
		}

		return jobPayrollCriteria.list();
	}

	public List<ParkingAllowance> getAllParkingAllowance(
			Implementation implementation) {
		return parkingAllowanceDAO.findByNamedQuery("getAllParkingAllowance",
				implementation);
	}

	public ParkingAllowance getParkingAllowanceById(Long allowanceId) {
		List<ParkingAllowance> parkingAllowances = parkingAllowanceDAO
				.findByNamedQuery("getParkingAllowanceById", allowanceId);
		if (parkingAllowances != null && parkingAllowances.size() > 0)
			return parkingAllowances.get(0);
		else
			return null;
	}

	public void saveParkingAllowance(ParkingAllowance parkingAllowance) {
		parkingAllowanceDAO.saveOrUpdate(parkingAllowance);
	}

	public void deleteParkingAllowance(ParkingAllowance parkingAllowance) {
		parkingAllowanceDAO.delete(parkingAllowance);
	}

	public AIOTechGenericDAO<ParkingAllowance> getParkingAllowanceDAO() {
		return parkingAllowanceDAO;
	}

	public void setParkingAllowanceDAO(
			AIOTechGenericDAO<ParkingAllowance> parkingAllowanceDAO) {
		this.parkingAllowanceDAO = parkingAllowanceDAO;
	}

}