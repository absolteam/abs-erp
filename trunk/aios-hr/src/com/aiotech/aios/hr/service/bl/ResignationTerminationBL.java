package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.joda.time.Years;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.EosPolicy;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobPayrollElement;
import com.aiotech.aios.hr.domain.entity.Payroll;
import com.aiotech.aios.hr.domain.entity.PayrollElement;
import com.aiotech.aios.hr.domain.entity.PayrollTransaction;
import com.aiotech.aios.hr.domain.entity.ResignationTermination;
import com.aiotech.aios.hr.domain.entity.vo.JobPayrollElementVO;
import com.aiotech.aios.hr.domain.entity.vo.PayrollTransactionVO;
import com.aiotech.aios.hr.domain.entity.vo.PayrollVO;
import com.aiotech.aios.hr.domain.entity.vo.ResignationTerminationVO;
import com.aiotech.aios.hr.service.ResignationTerminationService;
import com.aiotech.aios.hr.to.LeaveProcessTO;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class ResignationTerminationBL {

	private ResignationTerminationService resignationTerminationService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private JobAssignmentBL jobAssignmentBL;
	private PayrollBL payrollBL;

	private Implementation implementation;
	private CommentBL commentBL;

	public ResignationTerminationVO convertEntityToVO(
			ResignationTermination resignationTermination) {
		ResignationTerminationVO resignationVO = new ResignationTerminationVO();
		try {
			// Copy Entity class properties value into VO properties.
			BeanUtils.copyProperties(resignationVO, resignationTermination);

			if (resignationTermination.getEffectiveDate() != null)
				resignationVO.setEffectiveDateView(DateFormat
						.convertDateToString(resignationTermination
								.getEffectiveDate() + ""));

			if (resignationTermination.getRequestedDate() != null)
				resignationVO.setCreatedDateView(DateFormat
						.convertDateToString(resignationTermination
								.getCreatedDate() + ""));

			JobAssignment jobAssigment = jobAssignmentBL
					.getJobAssignmentService().getJobAssignmentInfo(
							resignationTermination.getJobAssignment()
									.getJobAssignmentId());

			resignationVO.setPersonNumber(jobAssigment.getPerson()
					.getPersonNumber());
			resignationVO.setPersonName(jobAssigment.getPerson().getFirstName()
					+ " " + jobAssigment.getPerson().getLastName());
			resignationVO.setCompanyName(jobAssigment.getCmpDeptLocation()
					.getCompany().getCompanyName());
			resignationVO.setDesignationName(jobAssigment.getDesignation()
					.getDesignationName());
			resignationVO.setDepartmentName(jobAssigment.getCmpDeptLocation()
					.getDepartment().getDepartmentName());
			resignationVO.setLocationName(jobAssigment.getCmpDeptLocation()
					.getLocation().getLocationName());
			Map<Byte, String> maps = resignationType();
			resignationVO
					.setTypeView(maps.get(resignationTermination.getType()));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return resignationVO;
	}

	public String saveResignationTermination(
			ResignationTermination resignationTermination, Payroll payroll,
			List<PayrollTransaction> payrollTransactions, Payroll oldPayroll) {
		String returnStatus = "SUCCESS";
		try {
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (resignationTermination != null
					&& resignationTermination.getResignationTerminationId() != null
					&& resignationTermination.getResignationTerminationId() > 0) {

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());

				payrollBL.getPayrollService().deletePayroll(
						oldPayroll,
						new ArrayList<PayrollTransaction>(oldPayroll
								.getPayrollTransactions()));
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			resignationTerminationService
					.saveResignationTermination(resignationTermination);

			// Save Pay Transactions
			payroll.setRecordId(resignationTermination
					.getResignationTerminationId());
			payrollBL.getPayrollService().savePayroll(payroll);
			payrollBL.getPayrollService().savePayrollTransaction(
					payrollTransactions);

			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					"ResignationTermination",
					resignationTermination.getResignationTerminationId(), user,
					workflowDetailVo);

		} catch (Exception e) {
			e.printStackTrace();
			returnStatus = "ERROR";
		}
		return returnStatus;
	}

	public void autoDisableTheJobAssignment(Implementation imp) {
		try {
			List<Implementation> implementations = new ArrayList<Implementation>();
			if (imp != null) {
				implementations.add(imp);
			} else {
				implementations = jobAssignmentBL.getJobBL().getSystemService()
				.getAllImplementation();
			}
			for (Implementation implementation : implementations) {
				List<ResignationTermination> resigns = resignationTerminationService
						.getAllResignationTerminationToActivate(implementation);
				JobAssignment jobAssignment = null;
				if (resigns != null && resigns.size() > 0) {
					for (ResignationTermination resignationTermination : resigns) {
						LocalDate effectiveDate = new LocalDate(
								resignationTermination.getEffectiveDate());
						LocalDate currentDate = new LocalDate();
						if (effectiveDate.equals(currentDate)
								|| effectiveDate.isBefore(currentDate)) {
							jobAssignment = resignationTermination
									.getJobAssignment();

							jobAssignment.setIsActive(false);
							jobAssignment.setIsPrimary(false);
							jobAssignment.setEndDate(effectiveDate.minusDays(1)
									.toDate());
							jobAssignmentBL.getJobAssignmentService()
									.saveJobAssignment(jobAssignment);

							resignationTermination
									.setStatus(Constants.HR.ResignationStatus.InActivated
											.getCode());
							resignationTerminationService
									.saveResignationTermination(resignationTermination);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteResignationTermination(
			ResignationTermination resignationTermination) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				ResignationTermination.class.getSimpleName(),
				resignationTermination.getResignationTerminationId(), user,
				workflowDetailVo);
	}

	public void doResignationTerminationBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		ResignationTermination resignationTermination = resignationTerminationService
				.getResignationTerminationById(recordId);
		if (workflowDetailVO.isDeleteFlag()) {

			Payroll oldPayroll = this
					.getPayrollBL()
					.getPayrollService()
					.getPayrollDetailByUseCase(
							resignationTermination
									.getResignationTerminationId(),
							resignationTermination.getClass().getSimpleName());

			resignationTerminationService
					.deleteResignationTermination(resignationTermination);
		} else {
			//autoDisableTheJobAssignment(getImplementation());
		}
	}

	public ResignationTerminationVO findServicePeriod(
			ResignationTerminationVO resignationVO) {
		try {

			JobAssignment jobAssignment = jobAssignmentBL
					.getJobAssignmentService().getJobAssignmentInfo(
							resignationVO.getJobAssignmentId());
			JobPayrollElementVO jobPayrollElementVO = new JobPayrollElementVO();
			jobPayrollElementVO.setJobAssignmentId(jobAssignment
					.getJobAssignmentId());
			List<JobPayrollElement> jobPayrollElements = jobAssignmentBL
					.getJobBL().getJobService()
					.getJobPayrollByJobAssignment(jobPayrollElementVO);
			Double basicAmount = 0.0;
			for (JobPayrollElement jobPayrollElement : jobPayrollElements) {
				if (jobPayrollElement.getPayrollElementByPayrollElementId()
						.getElementCode().equalsIgnoreCase("BASIC")) {
					basicAmount = jobPayrollElement.getAmount();
					break;
				}
			}
			if (jobAssignment != null) {
				resignationVO.setFromDate(jobAssignment.getPerson()
						.getValidFromDate());
				Integer noticeDays = 0;
				if ((byte) resignationVO.getType() == (byte) Constants.HR.ResignationActions.NoticeByEmployer
						.getCode())
					noticeDays = jobAssignment.getDesignation().getGrade()
							.getNoticeDaysCompany();
				else if ((byte) resignationVO.getType() == (byte) Constants.HR.ResignationActions.NoticeByEmployee
						.getCode())
					noticeDays = jobAssignment.getDesignation().getGrade()
							.getNoticeDaysEmployee();

				resignationVO.setFromDateView(DateFormat
						.convertDateToString(resignationVO.getFromDate()
								.toString()));
				resignationVO.setToDate(DateFormat
						.convertStringToDate(resignationVO
								.getEffectiveDateView()));

				Calendar c = Calendar.getInstance();
				c.setTime(resignationVO.getToDate());
				Date date = c.getTime();
				resignationVO.setToDate(date);
				resignationVO.setToDateView(DateFormat
						.convertSystemDateToString(date));
				resignationVO = findNumberOfYears(resignationVO);

				resignationVO = findEosPolicy(resignationVO);
				int graduitydays = (int) Math.abs(resignationVO.getEosPolicy()
						.getPayDays() * resignationVO.getTotalYears());
				resignationVO.setGratuityDays(graduitydays);

				resignationVO.setNoticeDays(noticeDays);
				double perDayBasic = (basicAmount * 12) / 365;
				resignationVO.setPerDayBasic(perDayBasic);
				resignationVO.setBasicAmount(AIOSCommons
						.formatAmountToDouble(basicAmount));
				resignationVO.setTotalAmount(perDayBasic
						* resignationVO.getEosPolicy().getPayDays()
						* resignationVO.getTotalYears());
				resignationVO.setTotalAmountView(AIOSCommons
						.roundTwoDecimals(resignationVO.getTotalAmount()));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return resignationVO;
	}

	public ResignationTerminationVO findNumberOfYears(
			ResignationTerminationVO resignationVO) {
		DateTime startDate = new DateTime(resignationVO.getFromDate());
		DateTime endDate = new DateTime(resignationVO.getToDate());

		int years = Years.yearsBetween(startDate, endDate).getYears(); // 1
		int months = Months.monthsBetween(startDate, endDate).getMonths();
		int days = Days.daysBetween(startDate, endDate).getDays();
		int remainingMonths = months % 12;
		resignationVO.setTotalMonths(months);
		resignationVO.setTotalYears(Double.valueOf(years + "."
				+ remainingMonths));
		resignationVO.setTotalDays(days);
		return resignationVO;
	}

	public ResignationTerminationVO findEosPolicy(
			ResignationTerminationVO resignationVO) throws Exception {
		resignationVO.setImplementation(getImplementation());
		List<EosPolicy> eosPolicies = resignationTerminationService
				.findEosPolicy(resignationVO);

		if (eosPolicies != null && eosPolicies.size() > 0) {
			Collections.sort(eosPolicies, new Comparator<EosPolicy>() {
				public int compare(EosPolicy m1, EosPolicy m2) {
					return m2.getServiceYears().compareTo(m1.getServiceYears());
				}
			});
			for (EosPolicy eosPolicy : eosPolicies) {
				if (resignationVO.getTotalYears() >= eosPolicy
						.getServiceYears()) {
					resignationVO.setEosPolicy(eosPolicy);
					resignationVO.setTypeView(Constants.HR.ResignationType.get(
							eosPolicy.getType()).name());
					break;
				}
			}

		}
		return resignationVO;
	}

	public PayrollVO processFinalSettlement(ResignationTerminationVO vo)
			throws Exception {
		Payroll payroll = null;
		PayrollVO payrollVO = null;
		if (vo.getResignationTerminationId() != null
				&& vo.getResignationTerminationId() > 0) {

			payroll = this
					.getPayrollBL()
					.getPayrollService()
					.getPayrollDetailByUseCase(
							vo.getResignationTerminationId(),
							ResignationTermination.class.getSimpleName());
			if (payroll != null)
				payrollVO = this.getPayrollBL().payrollToVOConverter(payroll);

			return payrollVO;
		}

		JobAssignment jobAssignment = jobAssignmentBL.getJobAssignmentService()
				.getJobAssignmentInfo(vo.getJobAssignmentId());
		// Pending Salary
		payrollVO = new PayrollVO();
		PayrollTransactionVO payrollTransaction = null;
		List<PayrollTransactionVO> payrollTransactionVOs = new ArrayList<PayrollTransactionVO>();
		PayrollElement encashElement = null;
		payrollTransactionVOs = recursivePayroll(vo, payrollVO,
				payrollTransactionVOs);

		/*
		 * for (PayrollTransaction temp : payrollVO.getPayrollTransactions()) {
		 * payrollTransaction = new PayrollTransactionVO();
		 * BeanUtils.copyProperties(payrollTransaction, temp);
		 * payrollTransactionVOs.add(payrollTransaction); }
		 */

		// Get Leave details
		List<LeaveProcessTO> leaves = payrollBL.getLeaveProcessBL()
				.findLeaveAvailability(jobAssignment, vo.getEffectiveDate());

		JobPayrollElementVO jobPayrollElementVO = new JobPayrollElementVO();
		jobPayrollElementVO.setJobAssignmentId(jobAssignment
				.getJobAssignmentId());
		List<JobPayrollElement> jobPayrollElements = payrollBL
				.getLeaveProcessBL().getJobService()
				.getJobPayrollByJobAssignment(jobPayrollElementVO);
		Double basicAmount = 0.0;
		double perDayBasic = 0.0;
		double totalAmount = 0.0;

		for (JobPayrollElement jobPayrollElement : jobPayrollElements) {
			if (jobPayrollElement.getPayrollElementByPayrollElementId()
					.getElementCode().equalsIgnoreCase("BASIC")) {
				basicAmount = jobPayrollElement.getAmount();
				encashElement = jobPayrollElement
						.getPayrollElementByPayrollElementId();
				break;
			}
		}
		encashElement = payrollBL.getPayrollElementBL()
				.getPayrollElementService()
				.getPayrollElementByCode(getImplementation(), "ENCASH");
		for (LeaveProcessTO leaveProcessTO : leaves) {

			if (leaveProcessTO.isEncash()) {
				double availableDays = Double.valueOf(leaveProcessTO
						.getAvailableDays());

				perDayBasic = (basicAmount * 12) / 360;
				totalAmount = perDayBasic * availableDays;
				payrollTransaction = new PayrollTransactionVO();
				payrollTransaction.setAmount(AIOSCommons
						.roundDecimals(totalAmount));
				payrollTransaction.setCalculatedAmount(AIOSCommons
						.roundDecimals(totalAmount));
				payrollTransaction.setPayrollElement(encashElement);
				payrollTransaction
						.setStatus(Constants.HR.PayTransactionStatus.New
								.getCode());
				payrollTransaction.setNote("Leave Balance("
						+ leaveProcessTO.getLeaveType() + ") days ("
						+ availableDays + ")");
				payrollTransaction.setLeaveAvailableDays(availableDays);
				payrollTransaction.setLeaveType(leaveProcessTO.getLeaveType());
				payrollTransactionVOs.add(payrollTransaction);
			}

		}
		payrollVO.setPayrollTransactionVOs(payrollTransactionVOs);
		return payrollVO;
	}

	public List<PayrollTransactionVO> recursivePayroll(
			ResignationTerminationVO vo, PayrollVO payrollVO,
			List<PayrollTransactionVO> payrollTransactionVOs) throws Exception {
		payrollVO = payrollBL.getPendingSalaryForFinalSettlement(vo);

		PayrollElement encashElement = payrollBL.getPayrollElementBL()
				.getPayrollElementService()
				.getPayrollElementByCode(getImplementation(), "SALARY");
		PayrollTransactionVO payrollTransaction = new PayrollTransactionVO();
		payrollTransaction.setAmount(AIOSCommons.roundDecimals(Double
				.valueOf(payrollVO.getPayroll().getNetAmount())));
		payrollTransaction.setCalculatedAmount(AIOSCommons.roundDecimals(Double
				.valueOf(payrollVO.getPayroll().getNetAmount())));
		payrollTransaction.setPayMonth(payrollVO.getPayPeriodTransaction()
				.getPayMonth());
		payrollTransaction.setFromDate(DateFormat.convertDateToString(payrollVO
				.getPayPeriodTransaction().getStartDate() + ""));
		payrollTransaction.setToDate(DateFormat.convertDateToString(payrollVO
				.getPayPeriodTransaction().getEndDate() + ""));
		payrollTransaction.setPayrollElement(encashElement);
		payrollTransaction.setStatus(Constants.HR.PayTransactionStatus.New
				.getCode());
		payrollTransaction.setNote("Salary for the month of("
				+ payrollVO.getPayPeriodTransaction().getPayMonth()
				+ ") days (" + payrollVO.getPayroll().getNumberOfDays() + ")");
		payrollTransactionVOs.add(payrollTransaction);
		LocalDate cutOff = new LocalDate(vo.getEffectiveDate());
		LocalDate periodEnd = new LocalDate(payrollVO.getPayPeriodTransaction()
				.getEndDate());
		if (cutOff.isAfter(periodEnd)) {
			vo.setPayroll(payrollVO.getPayroll());
			payrollVO = new PayrollVO();
			recursivePayroll(vo, payrollVO, payrollTransactionVOs);
		}

		return payrollTransactionVOs;
	}

	public Map<Byte, String> resignationType() {
		Map<Byte, String> types = new HashMap<Byte, String>();
		types.put(Constants.HR.ResignationType.Resignation.getCode(),
				Constants.HR.ResignationType.Resignation.name());
		types.put(Constants.HR.ResignationType.Termination.getCode(),
				Constants.HR.ResignationType.Termination.name());
		types.put(
				Constants.HR.ResignationType.EmployeeTransfer.getCode(),
				Constants.HR.ResignationType.EmployeeTransfer.name()
						.replaceAll("\\d+", "")
						.replaceAll("(.)([A-Z])", "$1 $2"));

		return types;
	}

	public Map<Byte, String> actions() {
		Map<Byte, String> types = new HashMap<Byte, String>();
		types.put(
				Constants.HR.ResignationActions.NoticeByEmployer.getCode(),
				Constants.HR.ResignationActions.NoticeByEmployer.name()
						.replaceAll("\\d+", "")
						.replaceAll("(.)([A-Z])", "$1 $2"));
		types.put(
				Constants.HR.ResignationActions.NoticeByEmployee.getCode(),
				Constants.HR.ResignationActions.NoticeByEmployee.name()
						.replaceAll("\\d+", "")
						.replaceAll("(.)([A-Z])", "$1 $2"));
		types.put(
				Constants.HR.ResignationActions.NoAction.getCode(),
				Constants.HR.ResignationActions.NoAction.name()
						.replaceAll("\\d+", "")
						.replaceAll("(.)([A-Z])", "$1 $2"));

		return types;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public JobAssignmentBL getJobAssignmentBL() {
		return jobAssignmentBL;
	}

	public void setJobAssignmentBL(JobAssignmentBL jobAssignmentBL) {
		this.jobAssignmentBL = jobAssignmentBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public ResignationTerminationService getResignationTerminationService() {
		return resignationTerminationService;
	}

	public void setResignationTerminationService(
			ResignationTerminationService resignationTerminationService) {
		this.resignationTerminationService = resignationTerminationService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public PayrollBL getPayrollBL() {
		return payrollBL;
	}

	public void setPayrollBL(PayrollBL payrollBL) {
		this.payrollBL = payrollBL;
	}

}
