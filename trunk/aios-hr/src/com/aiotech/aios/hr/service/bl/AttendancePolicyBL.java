package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.vo.AttendancePolicyVO;
import com.aiotech.aios.hr.service.AttendancePolicyService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class AttendancePolicyBL {
	private AttendancePolicyService attendancePolicyService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public void saveAttendancePolicy(AttendancePolicy attendancePolicy)
			throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
		.getSession();
		User user = (User) sessionObj.get("USER");

		attendancePolicy.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (attendancePolicy != null
				&& attendancePolicy.getAttendancePolicyId() != null
				&& attendancePolicy.getAttendancePolicyId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		attendancePolicyService.saveAttendancePolicy(attendancePolicy);
		
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				AttendancePolicy.class.getSimpleName(),
				attendancePolicy.getAttendancePolicyId(), user,
				workflowDetailVo);

	}

	public void deleteAttendancePolicy(Long attendancePolicyId)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				AttendancePolicy.class.getSimpleName(), attendancePolicyId,
				user, workflowDetailVo);
	}

	public void doAttendancePolicyBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		AttendancePolicy attendancePolicy = attendancePolicyService
				.getAttendancePolicyInfo(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			attendancePolicyService.deleteAttendancePolicy(attendancePolicy);
		} else {
		}
	}

	public List<AttendancePolicyVO> convertEntitiesTOVOs(
			List<AttendancePolicy> attendancePolicies) throws Exception {

		List<AttendancePolicyVO> tosList = new ArrayList<AttendancePolicyVO>();

		for (AttendancePolicy pers : attendancePolicies) {
			tosList.add(convertEntityToVO(pers));
		}

		return tosList;
	}

	public AttendancePolicyVO convertEntityToVO(
			AttendancePolicy attendancePolicy) {
		AttendancePolicyVO attendancePolicyVO = new AttendancePolicyVO();
		attendancePolicyVO.setAttendancePolicyId(attendancePolicy
				.getAttendancePolicyId());
		attendancePolicyVO.setPolicyName(attendancePolicy.getPolicyName() + "");
		if (attendancePolicy.getProcessType() != null
				&& attendancePolicy.getProcessType().equals("FLS"))
			attendancePolicyVO.setProcessType("First In & Last Out Swipe Only");
		else
			attendancePolicyVO.setProcessType("All Swipe");

		if (attendancePolicy.getAllowOt() != null
				&& attendancePolicy.getAllowOt() == true
				&& attendancePolicy
				.getOtHoursForFullday()!=null
				&& attendancePolicy
				.getOtHoursForFullday()!=null) {
			attendancePolicyVO.setAllowOtStr("YES");
			attendancePolicyVO.setOtHoursForHalfdayStr(DateFormat
					.convertTimeToString(attendancePolicy
							.getOtHoursForHalfday() + ""));
			attendancePolicyVO.setOtHoursForFulldayStr(DateFormat
					.convertTimeToString(attendancePolicy
							.getOtHoursForFullday() + ""));
		} else {
			attendancePolicyVO.setAllowOtStr("NO");
			attendancePolicyVO.setOtHoursForHalfdayStr("");
			attendancePolicyVO.setOtHoursForFulldayStr("");
		}
		if (attendancePolicy.getAllowCompoff() != null
				&& attendancePolicy.getAllowCompoff() == true) {
			attendancePolicyVO.setAllowCompoffStr("YES");
			attendancePolicyVO.setCompoffHoursForHalfdayStr(DateFormat
					.convertTimeToString(attendancePolicy
							.getCompoffHoursForHalfday() + ""));
			attendancePolicyVO.setCompoffHoursForFulldayStr(DateFormat
					.convertTimeToString(attendancePolicy
							.getCompoffHoursForFullday() + ""));
		} else {
			attendancePolicyVO.setAllowCompoffStr("NO");
			attendancePolicyVO.setCompoffHoursForHalfdayStr("");
			attendancePolicyVO.setCompoffHoursForFulldayStr("");
		}
		if(attendancePolicy
						.getCompoffHoursForHalfday()!=null)
		attendancePolicyVO.setLopHoursForHalfdayStr(DateFormat
				.convertTimeToString(attendancePolicy
						.getCompoffHoursForHalfday() + ""));
		if(attendancePolicy
						.getCompoffHoursForFullday()!=null)
		attendancePolicyVO.setLopHoursForFulldayStr(DateFormat
				.convertTimeToString(attendancePolicy
						.getCompoffHoursForFullday() + ""));

		attendancePolicyVO.setAttendancePolicy(attendancePolicy);
		return attendancePolicyVO;
	}

	public AttendancePolicyService getAttendancePolicyService() {
		return attendancePolicyService;
	}

	public void setAttendancePolicyService(
			AttendancePolicyService attendancePolicyService) {
		this.attendancePolicyService = attendancePolicyService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

}
