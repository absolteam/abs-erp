package com.aiotech.aios.hr.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.domain.entity.IdentityAvailability;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class IdentityAvailabilityService {
	public AIOTechGenericDAO<IdentityAvailability> identityAvailabilityDAO;
	public AIOTechGenericDAO<Identity> identityDAO;

	public List<IdentityAvailability> getIdentityAvailabilityList(
			Implementation implementation) {

		return identityAvailabilityDAO.findByNamedQuery(
				"getIdentityAvailabilityList", implementation);
	}

	public List<Identity> getIdentityList(Implementation implementation) {

		return identityDAO.findByNamedQuery("getIdentityList", implementation,
				implementation);
	}

	public List<IdentityAvailability> getIdentityAvailabilitiesListByDifferentValues(
			Long personId, Long companyId, Date fromDate, Date toDate,
			Boolean isActive, Implementation implementation) throws Exception {

		Criteria identityAvailabilityCriteria = identityAvailabilityDAO
				.createCriteria();

		if (fromDate != null && toDate != null) {
			identityAvailabilityCriteria.add(Restrictions.between("handoverDate", fromDate,
					toDate));
		}

		if (isActive != null) {
			identityAvailabilityCriteria.add(Restrictions.eq("isActive", isActive));
		}
		
		identityAvailabilityCriteria.setFetchMode("person", FetchMode.JOIN);

		Criteria identityCriteria = identityAvailabilityCriteria
				.createCriteria("identity");

		if (implementation != null) {

			identityCriteria.createAlias("company", "c",
					CriteriaSpecification.LEFT_JOIN);

			identityCriteria.createAlias("person", "p",
					CriteriaSpecification.LEFT_JOIN);

			Disjunction disjunction = Restrictions.disjunction();
			disjunction
					.add(Restrictions.eq("c.implementation", implementation));
			disjunction
					.add(Restrictions.eq("p.implementation", implementation));

			identityCriteria.add(disjunction);

			if (personId != null) {
				identityCriteria.add(Restrictions.eq("p.personId", personId));
			}
			if (companyId != null) {
				identityCriteria.add(Restrictions.eq("c.companyId", companyId));
			}

		} else {
			identityCriteria.setFetchMode("company", FetchMode.JOIN);
			identityCriteria.setFetchMode("person", FetchMode.JOIN);
		}

		identityCriteria.createCriteria("lookupDetail");

		return identityAvailabilityCriteria.list();

	}

	public List<Identity> getIdentityListByDifferentValues(Long personId,
			Long companyId, Date fromDate, Date toDate, Boolean isActive,
			Implementation implementation) throws Exception {

		Criteria identityCriteria = identityDAO.createCriteria();

		if (fromDate != null && toDate != null) {
			identityCriteria.add(Restrictions.between("expireDate", fromDate,
					toDate));
		}

		if (isActive != null) {
			identityCriteria.add(Restrictions.eq("isActive", isActive));
		}

		if (implementation != null) {

			identityCriteria.createAlias("company", "c",
					CriteriaSpecification.LEFT_JOIN);

			identityCriteria.createAlias("person", "p",
					CriteriaSpecification.LEFT_JOIN);

			Disjunction disjunction = Restrictions.disjunction();
			disjunction
					.add(Restrictions.eq("c.implementation", implementation));
			disjunction
					.add(Restrictions.eq("p.implementation", implementation));

			identityCriteria.add(disjunction);

			if (personId != null) {
				identityCriteria.add(Restrictions.eq("p.personId", personId));
			}
			if (companyId != null) {
				identityCriteria.add(Restrictions.eq("c.companyId", companyId));
			}

		} else {
			identityCriteria.setFetchMode("company", FetchMode.JOIN);
			identityCriteria.setFetchMode("person", FetchMode.JOIN);
		}

		identityCriteria.createCriteria("lookupDetail");

		return identityCriteria.list();

	}

	public IdentityAvailability getIdentityAvailabilityInfo(
			Long identityAvailabilityId) {
		List<IdentityAvailability> identityAvailabilities = identityAvailabilityDAO
				.findByNamedQuery("getIdentityAvailabilityInfo",
						identityAvailabilityId);
		if (identityAvailabilities != null && identityAvailabilities.size() > 0)
			return identityAvailabilities.get(0);
		else
			return null;
	}

	public List<IdentityAvailability> getIdentityExpectedReturn(
			Date currentDate, Date date) {
		return identityAvailabilityDAO.findByNamedQuery(
				"getIdentityExpectedReturn", currentDate, date);
	}

	public void saveIdentityAvailability(
			IdentityAvailability identityAvailability) {
		identityAvailabilityDAO.saveOrUpdate(identityAvailability);
	}

	public void deleteIdentityAvailability(
			IdentityAvailability identityAvailability) {
		identityAvailabilityDAO.delete(identityAvailability);
	}

	public AIOTechGenericDAO<IdentityAvailability> getIdentityAvailabilityDAO() {
		return identityAvailabilityDAO;
	}

	public void setIdentityAvailabilityDAO(
			AIOTechGenericDAO<IdentityAvailability> identityAvailabilityDAO) {
		this.identityAvailabilityDAO = identityAvailabilityDAO;
	}

	public AIOTechGenericDAO<Identity> getIdentityDAO() {
		return identityDAO;
	}

	public void setIdentityDAO(AIOTechGenericDAO<Identity> identityDAO) {
		this.identityDAO = identityDAO;
	}

}
