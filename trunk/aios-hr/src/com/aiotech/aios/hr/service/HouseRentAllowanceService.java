package com.aiotech.aios.hr.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.hr.domain.entity.HouseRentAllowance;
import com.aiotech.aios.hr.domain.entity.HraContractPayment;
import com.aiotech.aios.hr.domain.entity.vo.HouseRentAllowanceVO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class HouseRentAllowanceService {
	public AIOTechGenericDAO<HouseRentAllowance> houseRentAllowanceDAO;
	public AIOTechGenericDAO<HraContractPayment> hraContractPaymentDAO;

	public List<HouseRentAllowance> getAllHouseRentAllowanceByCriteria(
			HouseRentAllowanceVO houseVO) {
		Criteria jobPayrollCriteria = houseRentAllowanceDAO.createCriteria();
		jobPayrollCriteria.createAlias("jobAssignment", "ja");
		jobPayrollCriteria.createAlias("lookupDetail", "ld",
				CriteriaSpecification.LEFT_JOIN);
		jobPayrollCriteria.createAlias("jobPayrollElement", "jpa");
		jobPayrollCriteria.createAlias("jpa.payrollElementByPayrollElementId",
				"pe");
		jobPayrollCriteria.createAlias("jpa.payrollElementByPercentageElement",
				"percentage", CriteriaSpecification.LEFT_JOIN);

		if (houseVO.getPayPeriodTransaction() != null
				&& houseVO.getPayPeriodTransaction().getStartDate() != null) {
			jobPayrollCriteria.add(Restrictions.conjunction().add(
					Restrictions.ge("fromDate", houseVO
							.getPayPeriodTransaction().getStartDate())));
			jobPayrollCriteria.add(Restrictions.conjunction().add(
					Restrictions.le("toDate", houseVO.getPayPeriodTransaction()
							.getEndDate())));
		}

		if (houseVO.getJobAssignmentId() != null) {
			jobPayrollCriteria.add(Restrictions.eq("ja.jobAssignmentId",
					houseVO.getJobAssignmentId()));
		}

		if (houseVO.getJobPayrollElementId() != null) {
			jobPayrollCriteria.add(Restrictions.eq("jpa.jobPayrollElementId",
					houseVO.getJobPayrollElementId()));
		}

		return jobPayrollCriteria.list();
	}

	public List<HouseRentAllowance> getAllHouseRentAllowance(
			Implementation implementation) {
		return houseRentAllowanceDAO.findByNamedQuery(
				"getAllHouseRentAllowance", implementation);
	}

	public HouseRentAllowance getHouseRentAllowanceById(
			Long houseRentAllowanceId) {
		List<HouseRentAllowance> payPeriods = houseRentAllowanceDAO
				.findByNamedQuery("getHouseRentAllowanceById",
						houseRentAllowanceId);
		if (payPeriods != null && payPeriods.size() > 0)
			return payPeriods.get(0);
		else
			return null;
	}

	public void saveHouseRentAllowance(HouseRentAllowance houseRentAllowance) {
		houseRentAllowanceDAO.saveOrUpdate(houseRentAllowance);
	}

	public void saveAllHraContractPayment(
			List<HraContractPayment> contractPayments) {
		hraContractPaymentDAO.saveOrUpdateAll(contractPayments);
	}

	public void deleteHouseRentAllowance(HouseRentAllowance houseRentAllowance) {
		houseRentAllowanceDAO.delete(houseRentAllowance);
	}

	public void deleteAllHraContractPayment(
			List<HraContractPayment> contractPayments) {
		hraContractPaymentDAO.deleteAll(contractPayments);
	}

	public AIOTechGenericDAO<HraContractPayment> getHraContractPaymentDAO() {
		return hraContractPaymentDAO;
	}

	public void setHraContractPaymentDAO(
			AIOTechGenericDAO<HraContractPayment> hraContractPaymentDAO) {
		this.hraContractPaymentDAO = hraContractPaymentDAO;
	}

	public AIOTechGenericDAO<HouseRentAllowance> getHouseRentAllowanceDAO() {
		return houseRentAllowanceDAO;
	}

	public void setHouseRentAllowanceDAO(
			AIOTechGenericDAO<HouseRentAllowance> houseRentAllowanceDAO) {
		this.houseRentAllowanceDAO = houseRentAllowanceDAO;
	}
}
