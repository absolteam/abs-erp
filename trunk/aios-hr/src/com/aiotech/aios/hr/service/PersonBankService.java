package com.aiotech.aios.hr.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.hr.domain.entity.PersonBank;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class PersonBankService {
	public AIOTechGenericDAO<PersonBank> personBankDAO;

	public List<PersonBank> getPersonBanksByPerson(Long personId) {
		return personBankDAO.findByNamedQuery("getPersonBanksByPerson",
				personId);
	}

	public List<PersonBank> getAllPersonBank(Implementation implementation) {
		return personBankDAO.findByNamedQuery("getAllPersonBank",
				implementation,
				(byte)WorkflowConstants.Status.Published.getCode());
	}

	public PersonBank getPersonBankInfo(Long personBankId) {
		List<PersonBank> personBanks = personBankDAO.findByNamedQuery(
				"getPersonBankInfo", personBankId);
		if (personBanks != null && personBanks.size() > 0)
			return personBanks.get(0);
		else
			return null;
	}

	public List<PersonBank> getAllOwnerPersonBanks(Implementation implementation) {
		return personBankDAO.findByNamedQuery("getAllOwnerPersonBanks",
				implementation);
	}

	public List<PersonBank> getPersonBanksByCompanyTypePersonTypeAndStatus(
			Long personType, Integer companyType, Boolean status,
			Implementation implementation) throws Exception {

		Criteria personBankCriteria = personBankDAO.createCriteria();

		if (personType != null) {
			Criteria personCriteria = personBankCriteria
					.createCriteria("person");
			
			personCriteria.createAlias("personTypeAllocations", "pta",
					CriteriaSpecification.LEFT_JOIN);
			personCriteria.createAlias("pta.lookupDetail", "ld",
					CriteriaSpecification.LEFT_JOIN);

			personCriteria.add(Restrictions.eq("ld.lookupDetailId",
					personType));
		} else {
			personBankCriteria.setFetchMode("person", FetchMode.EAGER);
		}

		if (companyType != null) {
			Criteria companyCriteria = personBankCriteria
					.createCriteria("company");
			companyCriteria.add(Restrictions.eq("companyType", companyType));
		} else {
			personBankCriteria.setFetchMode("company", FetchMode.EAGER);
		}

		if (status != null) {
			personBankCriteria.add(Restrictions.eq("isActive", status));
		}

		if (implementation != null) {
			personBankCriteria.add(Restrictions.eq("implementation",
					implementation));
		}

		return personBankCriteria.list();

	}

	public void savePersonBank(PersonBank personBank) {
		personBankDAO.saveOrUpdate(personBank);
	}

	public void deletePersonBank(PersonBank personBank) {
		personBankDAO.delete(personBank);
	}

	public AIOTechGenericDAO<PersonBank> getPersonBankDAO() {
		return personBankDAO;
	}

	public void setPersonBankDAO(AIOTechGenericDAO<PersonBank> personBankDAO) {
		this.personBankDAO = personBankDAO;
	}

}
