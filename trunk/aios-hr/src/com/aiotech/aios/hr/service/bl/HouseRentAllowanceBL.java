package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.HouseRentAllowance;
import com.aiotech.aios.hr.domain.entity.HraContractPayment;
import com.aiotech.aios.hr.domain.entity.vo.HouseRentAllowanceVO;
import com.aiotech.aios.hr.service.HouseRentAllowanceService;
import com.aiotech.aios.system.service.bl.LookupMasterBL;

public class HouseRentAllowanceBL {

	private LookupMasterBL lookupMasterBL;
	private HouseRentAllowanceService houseRentAllowanceService;
	private PayPeriodBL payPeriodBL;
	
	
	public HouseRentAllowanceVO convertEntityToVO(
			HouseRentAllowance houseRentAllowance) {
		HouseRentAllowanceVO hraVO = new HouseRentAllowanceVO();
		try {
			// Copy Entity class properties value into VO properties.
			BeanUtils.copyProperties(hraVO, houseRentAllowance);
			hraVO.setFromDateView(DateFormat
					.convertDateToString(houseRentAllowance.getFromDate() + ""));
			hraVO.setToDateView(DateFormat
					.convertDateToString(houseRentAllowance.getToDate() + ""));
			if (houseRentAllowance.getIsFinanceImpact() != null
					&& houseRentAllowance.getIsFinanceImpact())
				hraVO.setIsFinanceView("YES");
			else
				hraVO.setIsFinanceView("NO");
			
			hraVO.setPropertyName(houseRentAllowance.getLookupDetail().getDisplayName());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return hraVO;
	}
	
	public String saveHouseRentAllowance(HouseRentAllowance houseRentAllowance,
			List<HraContractPayment> contractPayments) {
		String returnStatus = "SUCCESS";
		try {
			if (houseRentAllowance.getHouseRentAllowanceId() != null
					&& houseRentAllowance.getHouseRentAllowanceId() > 0) {
				List<HraContractPayment> tempHraPayments = new ArrayList<HraContractPayment>(
						houseRentAllowance.getHraContractPayments());
				List<HraContractPayment> deleteContractPayments = getPaymentsToDelete(
						contractPayments, tempHraPayments);

				if (deleteContractPayments != null
						&& deleteContractPayments.size() > 0)
					houseRentAllowanceService
							.deleteAllHraContractPayment(deleteContractPayments);
			}

			houseRentAllowanceService
					.saveHouseRentAllowance(houseRentAllowance);
			for (HraContractPayment hraContractPayment : contractPayments) {
				hraContractPayment.setHouseRentAllowance(houseRentAllowance);
			}
			houseRentAllowanceService
					.saveAllHraContractPayment(contractPayments);
		} catch (Exception e) {
			returnStatus = "ERROR";
		}
		return returnStatus;
	}
	
	public List<HraContractPayment> getPaymentsToDelete(
			List<HraContractPayment> payments,
			List<HraContractPayment> editPayments) {
		List<HraContractPayment> deletePayments = new ArrayList<HraContractPayment>();

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		for (HraContractPayment payment1 : payments) {
			if (payment1.getHraContractPaymentId() != null
					&& payment1.getHraContractPaymentId() > 0)
				listOne.add(payment1.getHraContractPaymentId());
		}
		for (HraContractPayment payment2 : editPayments) {
			if (payment2.getHraContractPaymentId() != null
					&& payment2.getHraContractPaymentId() > 0)
				listTwo.add(payment2.getHraContractPaymentId());
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);

		// Delete Process
		for (Long lng : different) {
			for (HraContractPayment payment3 : editPayments) {
				if (lng != null
						&& payment3.getHraContractPaymentId().equals(
								lng)) {
					deletePayments.add(payment3);
				}
			}
		}
		return deletePayments;
	}


	public String deleteHouseRentAllowance(HouseRentAllowance houseRentAllowance,
			List<HraContractPayment> contractPayments) {
		String returnStatus = "SUCCESS";
		try {
			houseRentAllowanceService
					.deleteAllHraContractPayment(contractPayments);
			houseRentAllowanceService.deleteHouseRentAllowance(houseRentAllowance);
		} catch (Exception e) {
			returnStatus = "ERROR";
		}
		return returnStatus;
	}
	
	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public HouseRentAllowanceService getHouseRentAllowanceService() {
		return houseRentAllowanceService;
	}

	public void setHouseRentAllowanceService(
			HouseRentAllowanceService houseRentAllowanceService) {
		this.houseRentAllowanceService = houseRentAllowanceService;
	}

	public PayPeriodBL getPayPeriodBL() {
		return payPeriodBL;
	}

	public void setPayPeriodBL(PayPeriodBL payPeriodBL) {
		this.payPeriodBL = payPeriodBL;
	}


}
