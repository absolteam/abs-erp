package com.aiotech.aios.hr.service.bl;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.Category;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.service.GLChartOfAccountService;
import com.aiotech.aios.accounts.service.GLCombinationService;
import com.aiotech.aios.accounts.service.bl.DirectPaymentBL;
import com.aiotech.aios.accounts.service.bl.TransactionBL;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Attendance;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobPayrollElement;
import com.aiotech.aios.hr.domain.entity.LeaveProcess;
import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;
import com.aiotech.aios.hr.domain.entity.Payroll;
import com.aiotech.aios.hr.domain.entity.PayrollElement;
import com.aiotech.aios.hr.domain.entity.PayrollTransaction;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.JobPayrollElementVO;
import com.aiotech.aios.hr.domain.entity.vo.PayPeriodVO;
import com.aiotech.aios.hr.domain.entity.vo.PayrollTransactionVO;
import com.aiotech.aios.hr.domain.entity.vo.PayrollVO;
import com.aiotech.aios.hr.domain.entity.vo.ResignationTerminationVO;
import com.aiotech.aios.hr.service.PayrollService;
import com.aiotech.aios.hr.to.PayrollTO;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.common.WorkflowBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class PayrollBL {

	private PayrollService payrollService;
	private SystemService systemService;
	private AttendanceBL attendanceBL;
	private CommentBL commentBL;
	private LeaveProcessBL leaveProcessBL;
	private JobBL jobBL;
	private JobAssignmentBL jobAssignmentBL;
	private Implementation implementation;
	private CompanyBL companyBL;
	private PersonBL personBL;
	private TransactionBL transactionBL;
	private GLChartOfAccountService accountService;
	private GLCombinationService combinationService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private WorkflowBL workflowBL;
	private PayPeriodBL payPeriodBL;
	private EmployeeLoanBL employeeLoanBL;
	private DirectPaymentBL directPaymentBL;
	private PayrollElementBL payrollElementBL;

	public List<PayrollVO> payrollIntoVOs(List<Payroll> payrolls) {
		try {
			List<PayrollVO> payrollVOs = null;
			if (payrolls != null) {
				payrollVOs = new ArrayList<PayrollVO>();
				PayrollVO payrollVO = null;
				for (Payroll payroll : payrolls) {
					payrollVO = new PayrollVO();
					payrollVO = payrollToPayrollVOConverterPrint(payroll);
					payrollVOs.add(payrollVO);
				}
			}
			return payrollVOs;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public PayrollVO payrollToPayrollVOConverterPrint(Payroll payroll)
			throws Exception {
		PayrollVO payrollVO = new PayrollVO();

		payrollVO.setEmployeeName(payroll.getJobAssignment().getPerson()
				.getFirstName()
				+ " " + payroll.getJobAssignment().getPerson().getLastName());
		payrollVO.setDesignation(payroll.getJobAssignment().getDesignation()
				.getDesignationName());

		payrollVO.setCompanyName(payroll.getJobAssignment()
				.getCmpDeptLocation().getCompany().getCompanyName());
		// Period Start & End
		payrollVO.setPeriodStart(DateFormat.convertDateToString(payroll
				.getStartDate().toString()));
		payrollVO.setPeriodEnd(DateFormat.convertDateToString(payroll
				.getEndDate().toString()));
		payrollVO.setNumberOfDays(payroll.getNumberOfDays());

		payrollVO.setDepartmentName(payroll.getJobAssignment()
				.getCmpDeptLocation().getDepartment().getDepartmentName());

		// LOP Days
		if (payroll.getLopDays() != null
				&& !payroll.getLopDays().trim().equals(""))
			payrollVO.setLopDays(AIOSCommons.roundLeaveDays(payroll
					.getLopDays()));
		else
			payrollVO.setLopDays("0");

		Double otherEarning = 0.0;
		Double otherDeduction = 0.0;
		Double grossPay = 0.0;
		Double ot = 0.0;
		Double aleave = 0.0;
		String description = null;
		List<JobPayrollElement> jobPayrollElements = new ArrayList<JobPayrollElement>();
		if (payroll.getPayrollTransactions() != null)
			for (PayrollTransaction payrollTransaction : payroll
					.getPayrollTransactions()) {
				if (payrollTransaction.getNote() != null
						&& !payrollTransaction.getNote().trim()
								.equalsIgnoreCase("")) {
					description = (description == null ? "" : description + "<br>")
							+ payrollTransaction.getNote();
				}
				if (payrollTransaction.getPayrollElement().getElementCode()
						.equalsIgnoreCase("BASIC"))
					payrollVO.setBasic(AIOSCommons
							.formatAmount(payrollTransaction.getAmount()));
				else if (payrollTransaction.getPayrollElement()
						.getElementCode().equalsIgnoreCase("OT")) {
					payrollVO.setOverTime(AIOSCommons.formatAmount(ot
							+ payrollTransaction.getAmount()));
					ot += ot;
				} else if (payrollTransaction.getPayrollElement()
						.getElementCode().equalsIgnoreCase("ALEAVE")) {
					payrollVO.setAnnualLeave(AIOSCommons.formatAmount(aleave
							+ payrollTransaction.getAmount()));
					aleave += aleave;
				}

				if (payrollTransaction.getPayrollElement().getElementNature()
						.equalsIgnoreCase("EARNING"))
					otherEarning = otherEarning
							+ payrollTransaction.getAmount();
				else if (payrollTransaction.getPayrollElement()
						.getElementNature().equalsIgnoreCase("DEDUCTION"))
					otherDeduction = otherDeduction
							+ payrollTransaction.getAmount();

				for (JobPayrollElement jobpayrollelement : payroll
						.getJobAssignment().getJob().getJobPayrollElements()) {
					if (!jobpayrollelement.getIsActive())
						continue;

					payrollVO.getPayrollCodeMaps().put(
							jobpayrollelement
									.getPayrollElementByPayrollElementId()
									.getElementCode(),
							jobpayrollelement
									.getPayrollElementByPayrollElementId()
									.getElementName());
					if (jobpayrollelement
							.getPayrollElementByPayrollElementId()
							.getPayrollElementId()
							.equals(payrollTransaction.getPayrollElement()
									.getPayrollElementId())) {
						grossPay = grossPay + payrollTransaction.getAmount();
						jobPayrollElements.add(jobpayrollelement);
					}
				}
			}
		payrollVO.setDescription(description);
		payrollVO.setJobPayrollElements(jobPayrollElements);
		payrollVO.setGrossPay(AIOSCommons.formatAmount(grossPay));
		payrollVO.setOtherEarning(AIOSCommons.roundTwoDecimals(otherEarning));
		payrollVO.setOtherDeduction(AIOSCommons
				.roundTwoDecimals(otherDeduction));
		payrollVO.setJobAssignment(payroll.getJobAssignment());
		if (payroll.getNetAmount() != null && payroll.getNetAmount() > 0)
			payrollVO.setNetAmount(Double.parseDouble(AIOSCommons
					.roundTwoDecimals(payroll.getNetAmount())));
		payrollVO.setNetPay(AIOSCommons.formatAmount(Math.round(payroll
				.getNetAmount())));
		payrollVO.setPaymentMode(payroll.getJobAssignment().getPayMode());
		payrollVO.setPayMonth(payroll.getPayMonth());
		payrollVO.setNetAmountStr(AIOSCommons.roundTwoDecimals(payroll
				.getNetAmount()));

		payrollVO.setStatus(payroll.getStatus());
		payrollVO.setPayStatus(Constants.HR.PayStatus.get(payroll.getStatus())
				.name());
		payrollVO.setPayrollId(payroll.getPayrollId());
		payrollVO.setJobAssignment(payroll.getJobAssignment());
		payrollVO.setPayroll(payroll);
		if (payroll.getCreatedDate() != null)
			payrollVO.setCreatedDateStr(DateFormat.convertDateToString(payroll
					.getCreatedDate().toString()));
		return payrollVO;

	}

	public PayrollVO personIntoPayrollViewFormat(PayrollVO payrollVO)
			throws Exception {
		PayrollVO returnPayrollVO = new PayrollVO();
		returnPayrollVO.setJobAssignment(payrollVO.getJobAssignment());
		returnPayrollVO.setEmployeeName(payrollVO.getJobAssignment()
				.getPerson().getFirstName()
				+ " "
				+ payrollVO.getJobAssignment().getPerson().getLastName()
				+ "-"
				+ payrollVO.getJobAssignment().getPerson().getPersonNumber());
		returnPayrollVO.setDesignation(payrollVO.getJobAssignment()
				.getDesignation().getDesignationName());

		returnPayrollVO.setCompanyName(payrollVO.getJobAssignment()
				.getCmpDeptLocation().getCompany().getCompanyName());
		// Period Start & End
		if (payrollVO.getPayPeriodTransaction() != null) {
			returnPayrollVO.setPeriodStart(DateFormat
					.convertDateToString(payrollVO.getPayPeriodTransaction()
							.getStartDate().toString()));
			returnPayrollVO.setPeriodEnd(DateFormat
					.convertDateToString(payrollVO.getPayPeriodTransaction()
							.getEndDate().toString()));
			returnPayrollVO.setPayMonth(payrollVO.getPayPeriodTransaction()
					.getPayMonth());
		}
		returnPayrollVO.setPaymentMode(payrollVO.getJobAssignment()
				.getPayMode());

		returnPayrollVO.setStatus(Constants.HR.PayStatus.Pending.getCode());
		returnPayrollVO.setPayStatus(Constants.HR.PayStatus.get(
				returnPayrollVO.getStatus()).name());
		returnPayrollVO.setGrossPay("0.0");
		returnPayrollVO.setOtherEarning("0.0");
		returnPayrollVO.setOtherDeduction("0.0");
		returnPayrollVO.setNetAmount(0.0);
		returnPayrollVO.setNetPay("0.0");

		return returnPayrollVO;

	}

	public Payroll payrollExecution(PayrollVO payrollVO) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		HttpSession session = ServletActionContext.getRequest().getSession();
		User user = (User) sessionObj.get("USER");
		PayrollVO copyPayrollVO = new PayrollVO();
		BeanUtils.copyProperties(copyPayrollVO, payrollVO);
		for (JobAssignment jobAssigment : payrollVO.getJobAssignmentList()) {

			if (jobAssigment.getFreeze() != null && jobAssigment.getFreeze()) {
				createEmptyPayrollForEmployee(jobAssigment, payrollVO, "FREEZE");
				continue;
			}

			BeanUtils.copyProperties(payrollVO, copyPayrollVO);
			if (payrollVO.getPayMonth() != null
					&& !payrollVO.getPayMonth().equals("")) {
				deletePayrollRecursively(jobAssigment.getPerson(),
						payrollVO.getPayMonth());
			}

			if (jobAssigment.getResignationTerminations() != null
					&& jobAssigment.getResignationTerminations().size() > 0) {
				createEmptyPayrollForEmployee(jobAssigment, payrollVO,
						"SERVICEOVER");
				continue;
			}

			payrollVO.setJobAssignment(jobAssigment);
			Payroll payroll = new Payroll();
			payrollVO.setPayroll(payroll);
			/*
			 * Get all pay period transactions Input: payPolicy,jobAssignmentId
			 * Output: payPeriodTransactions
			 */
			PayPeriodVO payPeriodVO = new PayPeriodVO();
			payPeriodVO.setPayPolicy(payrollVO.getPayPolicy());
			payPeriodVO.setJobAssignmentId(jobAssigment.getJobAssignmentId());
			/*
			 * Get previous pay information Input: person Output:
			 * previousPayroll
			 */
			Payroll previousPayroll = payrollService
					.getPayrollBasedonPerson(jobAssigment.getPerson());

			List<PayPeriodTransaction> payPeriodTransactions = payPeriodBL
					.findActivePayPeriods(payPeriodVO);

			if (previousPayroll == null
					|| previousPayroll.getPayrollId() == null)
				payPeriodVO.setTransactionDate(jobAssigment.getEffectiveDate());

			payrollVO.setPayPeriodVO(payPeriodVO);

			/*
			 * Find current and previous payPeriod Input:
			 * previousPayroll,payPeriodTransactions Output:
			 * previousPeriod,payPeriodTransaction
			 */
			payrollVO.setPreviousPayroll(previousPayroll);
			payrollVO.setPayPeriodTransactions(payPeriodTransactions);
			payrollVO = findCurrentAndPreviousPayPeriod(payrollVO);

			/*
			 * Find actual pay period Input:
			 * previousPayroll,jobAssignment,payPeriodTransaction Output:
			 * payroll.startDate,payroll.endDate
			 */
			if (payrollVO.getPayPeriodTransaction() != null)
				payrollVO = findPayActualPeriod(payrollVO);
			else {
				continue;
			}

			// Set the active jobPayrollElments against the job assignment
			JobPayrollElementVO jobPayrollElementVO = new JobPayrollElementVO();
			jobPayrollElementVO.setJobAssignmentId(jobAssigment
					.getJobAssignmentId());
			List<JobPayrollElement> jobPayrollElements = jobBL.getJobService()
					.getJobPayrollByJobAssignment(jobPayrollElementVO);
			// Sort the calculation order by calculationType
			Collections.sort(jobPayrollElements,
					new Comparator<JobPayrollElement>() {
						public int compare(JobPayrollElement m1,
								JobPayrollElement m2) {
							return m1.getCalculationType().compareTo(
									m2.getCalculationType());
						}
					});
			payrollVO.setJobPayrollElements(jobPayrollElements);
			Map<String, JobPayrollElement> jobPayrollElmentMaps = new HashMap<String, JobPayrollElement>();
			for (JobPayrollElement jobPayrollElement : jobPayrollElements) {
				jobPayrollElmentMaps.put(
						jobPayrollElement.getPayrollElementByPayrollElementId()
								.getElementCode(), jobPayrollElement);
			}
			payrollVO.setJobPayrollElementsMap(jobPayrollElmentMaps);
			// Initialise the pay transaction list
			Set<PayrollTransaction> payrollTransactions = new HashSet<PayrollTransaction>(
					0);
			payrollVO.setPayrollTransactions(payrollTransactions);
			/*
			 * Process Pay Transactions Input:
			 * jobPayrollElements,jobPayrollElmentMaps,jobAssignment Output:
			 * payrollTransactions;
			 */
			payrollVO = getAllPayTransactions(payrollVO);

			payrollVO = getPerDayAndPerHourSalary(payrollVO);

			/*
			 * Get Loss of pay occurrence from leave process Input:
			 * jobPayrollElements,jobPayrollElmentMaps,jobAssignment Output:
			 * payrollTransactions;
			 */
			payrollVO = getLossOfPayFromLeaveProcess(payrollVO);

			/*
			 * Get Loss of pay and over time occurrence from regular attendance
			 * Input: jobPayrollElements,jobPayrollElmentMaps,jobAssignment
			 * Output: payrollTransactions;
			 */
			payrollVO = getLossOfPayFromAttendance(payrollVO);

			payrollVO = getLossOfPayFromNewJoinies(payrollVO);

			// Find the salary is exceeding the actual monthly salary
			Double actualPayDays = DateFormat.dayDifference(payrollVO
					.getPayPeriodTransaction().getStartDate(), payrollVO
					.getPayPeriodTransaction().getEndDate()) + 1;
			Double acutalLopDays = (payrollVO.getPayroll().getLopDays() != null ? Double
					.valueOf(payrollVO.getPayroll().getLopDays()) : 0.0);
			if (actualPayDays <= acutalLopDays) {
				Double netAmount = 0.0;
				Double netAmountEarning = 0.0;
				Double netAmountDeduction = 0.0;
				for (PayrollTransaction payrollTransaction : payrollVO
						.getPayrollTransactions()) {
					if (payrollTransaction.getPayrollElement()
							.getElementNature() != null
							&& payrollTransaction.getPayrollElement()
									.getElementNature().equals("EARNING")) {
						netAmountEarning += payrollTransaction.getAmount();
					} else if (payrollTransaction.getPayrollElement()
							.getElementNature() != null
							&& payrollTransaction.getPayrollElement()
									.getElementNature().equals("DEDUCTION")) {
						netAmountDeduction += payrollTransaction.getAmount();
					}
				}
				netAmount = ((netAmountEarning - netAmountDeduction));

				if (netAmountEarning > netAmountDeduction) {
					// Paid From Leave Settlement
					PayrollElement payrollElement = payrollElementBL
							.getPayrollElementService()
							.getPayrollElementByCode(getImplementation(),
									"UNBALANCED-DEDUCTION");

					PayrollTransaction payrollTransaction = new PayrollTransaction();
					payrollTransaction.setCreatedDate(new Date());
					payrollTransaction.setImplementation(getImplementation());
					payrollTransaction.setPayrollElement(payrollElement);
					payrollTransaction.setCalculatedAmount(Math.abs(netAmount));
					payrollTransaction.setAmount(Math.abs(netAmount));
					payrollTransaction.setNote("Settlement Adjustment");
					payrollTransaction
							.setStatus(Constants.HR.PayTransactionStatus.New
									.getCode());
					payrollVO.getPayrollTransactions().add(payrollTransaction);
				} else if (netAmountEarning < netAmountDeduction) {
					// Paid From Leave Settlement
					PayrollElement payrollElement = payrollElementBL
							.getPayrollElementService()
							.getPayrollElementByCode(getImplementation(),
									"UNBALANCED-EARNING");

					PayrollTransaction payrollTransaction = new PayrollTransaction();
					payrollTransaction.setCreatedDate(new Date());
					payrollTransaction.setImplementation(getImplementation());
					payrollTransaction.setPayrollElement(payrollElement);
					payrollTransaction.setCalculatedAmount(Math.abs(netAmount));
					payrollTransaction.setAmount(Math.abs(netAmount));
					payrollTransaction.setNote("Settlement Adjustment");
					payrollTransaction
							.setStatus(Constants.HR.PayTransactionStatus.New
									.getCode());
					payrollVO.getPayrollTransactions().add(payrollTransaction);
				}
			}

			/*
			 * Get Loan & Advance Deduction from employee benefit Input:
			 * jobPayrollElements,jobPayrollElmentMaps,jobAssignment Output:
			 * payrollTransactions;
			 */
			payrollVO = getLoanAndAdvanceDeduction(payrollVO);

			// Payroll Master process
			payrollVO.getPayroll().setPerson(jobAssigment.getPerson());
			payrollVO.getPayroll().setImplementation(getImplementation());
			payrollVO.getPayroll().setJobAssignment(jobAssigment);
			payrollVO.getPayroll().setStatus(
					Constants.HR.PayStatus.InProcess.getCode());

			payrollVO.getPayroll().setIsApprove(
					Byte.parseByte(WorkflowConstants.Status.Published.getCode()
							+ ""));
			payrollVO.getPayroll().setCreatedDate(new Date());
			Double netAmount = 0.0;
			Double netAmountEarning = 0.0;
			Double netAmountDeduction = 0.0;
			for (PayrollTransaction payrollTransaction : payrollTransactions) {
				if (payrollTransaction.getPayrollElement().getElementNature() != null
						&& payrollTransaction.getPayrollElement()
								.getElementNature().equals("EARNING")) {
					netAmountEarning += payrollTransaction.getAmount();
				} else if (payrollTransaction.getPayrollElement()
						.getElementNature() != null
						&& payrollTransaction.getPayrollElement()
								.getElementNature().equals("DEDUCTION")) {
					netAmountDeduction += payrollTransaction.getAmount();
				}
			}
			netAmount = ((netAmountEarning - netAmountDeduction));
			payrollVO.getPayroll().setNetAmount(netAmount);
			payrollVO.getPayroll().setIsLedgerTransaction(true);
			payrollVO.getPayroll().setIsSalary(true);
			payrollVO
					.getPayroll()
					.setPayMonth(
							payrollVO.getPayPeriodTransaction().getPayMonth() != null ? payrollVO
									.getPayPeriodTransaction().getPayMonth()
									: payrollVO.getPayPeriodTransaction()
											.getPayWeek() + " Week");
			// Save /Update salary
			payrollVO.getPayroll().setPayPeriodTransaction(
					payrollVO.getPayPeriodTransaction());
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (payrollVO.getPayroll() != null
					&& payrollVO.getPayroll().getPayrollId() != null
					&& payrollVO.getPayroll().getPayrollId() > 0) {

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			payrollService.savePayroll(payrollVO.getPayroll());

			for (PayrollTransaction payrollTransaction : payrollTransactions) {
				payrollTransaction.setCalculatedAmount(payrollTransaction
						.getAmount());
				payrollTransaction.setPayroll(payrollVO.getPayroll());
			}

			payrollService
					.savePayrollTransaction(new ArrayList<PayrollTransaction>(
							payrollVO.getPayrollTransactions()));

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					Payroll.class.getSimpleName(), payrollVO.getPayroll()
							.getPayrollId(), user, workflowDetailVo);

		}

		return null;
	}

	public void createEmptyPayrollForEmployee(JobAssignment jobAssigment,
			PayrollVO payrollVO, String message) {
		PayPeriodVO payPeriodVO = new PayPeriodVO();
		payPeriodVO.setPayPolicy(payrollVO.getPayPolicy());
		payPeriodVO.setJobAssignmentId(jobAssigment.getJobAssignmentId());
		/*
		 * Get previous pay information Input: person Output: previousPayroll
		 */
		Payroll previousPayroll = payrollService
				.getPayrollBasedonPerson(jobAssigment.getPerson());

		List<PayPeriodTransaction> payPeriodTransactions = payPeriodBL
				.findActivePayPeriods(payPeriodVO);

		if (previousPayroll == null || previousPayroll.getPayrollId() == null)
			payPeriodVO.setTransactionDate(jobAssigment.getEffectiveDate());

		payrollVO.setPayPeriodVO(payPeriodVO);

		/*
		 * Find current and previous payPeriod Input:
		 * previousPayroll,payPeriodTransactions Output:
		 * previousPeriod,payPeriodTransaction
		 */
		payrollVO.setPreviousPayroll(previousPayroll);
		payrollVO.setPayPeriodTransactions(payPeriodTransactions);
		payrollVO = findCurrentAndPreviousPayPeriod(payrollVO);

		/*
		 * Find actual pay period Input:
		 * previousPayroll,jobAssignment,payPeriodTransaction Output:
		 * payroll.startDate,payroll.endDate
		 */
		if (payrollVO.getPayPeriodTransaction() != null)
			payrollVO = findPayActualPeriod(payrollVO);

		/*
		 * Find current and previous payPeriod Input:
		 * previousPayroll,payPeriodTransactions Output:
		 * previousPeriod,payPeriodTransaction
		 */
		payrollVO.setPreviousPayroll(previousPayroll);
		payrollVO.setPayPeriodTransactions(payPeriodTransactions);
		payrollVO = findCurrentAndPreviousPayPeriod(payrollVO);
		Payroll deletePayroll = payrollService.getPayrollBasedonPersonAndMonth(
				jobAssigment.getPerson(), payrollVO.getPayMonth());
		if (deletePayroll != null) {

		}
		Payroll payroll = new Payroll();
		payroll.setNetAmount(0.0);
		payroll.setIsLedgerTransaction(true);
		payroll.setIsSalary(true);
		DateTimeFormatter dtf = DateTimeFormat.forPattern("MMMM,yyyy");
		LocalDate monthDt = new LocalDate(dtf.parseLocalDate(payrollVO
				.getPayMonth()));
		LocalDate date1 = new LocalDate(monthDt.dayOfMonth().withMinimumValue());
		LocalDate date2 = new LocalDate(monthDt.dayOfMonth().withMaximumValue());
		payroll.setPayMonth(payrollVO.getPayMonth());
		payroll.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		payroll.setStatus(Constants.HR.PayStatus.Holded.getCode());
		payroll.setStartDate(date1.toDate());
		payroll.setEndDate(date2.toDate());
		payroll.setCreatedDate(new Date());
		payroll.setJobAssignment(jobAssigment);
		payroll.setPerson(jobAssigment.getPerson());
		payroll.setLopDays("0.0");
		payroll.setImplementation(getImplementation());
		payrollService.savePayroll(payroll);
	}

	public PayrollVO getPendingSalaryForFinalSettlement(
			ResignationTerminationVO vo) throws Exception {
		PayrollVO payrollVO = null;
		if (vo.getJobAssignmentId() != null) {
			payrollVO = new PayrollVO();
			JobAssignment jobAssigment = jobAssignmentBL
					.getJobAssignmentService().getJobAssignmentInfo(
							vo.getJobAssignmentId());

			payrollVO.setJobAssignment(jobAssigment);
			Payroll payroll = new Payroll();
			payrollVO.setPayroll(payroll);
			payrollVO.setCutOffDate(vo.getEffectiveDate());
			payrollVO.setCutOffFromEnd(true);

			/*
			 * Get all pay period transactions Input: payPolicy,jobAssignmentId
			 * Output: payPeriodTransactions
			 */
			PayPeriodVO payPeriodVO = new PayPeriodVO();
			payPeriodVO.setPayPolicy(jobAssigment.getPayPolicy());
			payPeriodVO.setJobAssignmentId(jobAssigment.getJobAssignmentId());
			/*
			 * Get previous pay information Input: person Output:
			 * previousPayroll
			 */
			Payroll previousPayroll = null;
			if (vo.getPayroll() == null)
				previousPayroll = payrollService
						.getPayrollBasedonPerson(jobAssigment.getPerson());
			else
				previousPayroll = vo.getPayroll();

			List<PayPeriodTransaction> payPeriodTransactions = payPeriodBL
					.findActivePayPeriods(payPeriodVO);

			if (previousPayroll == null)
				payPeriodVO.setTransactionDate(jobAssigment.getEffectiveDate());

			payrollVO.setPayPeriodVO(payPeriodVO);

			/*
			 * Find current and previous payPeriod Input:
			 * previousPayroll,payPeriodTransactions Output:
			 * previousPeriod,payPeriodTransaction
			 */
			payrollVO.setPreviousPayroll(previousPayroll);
			payrollVO.setPayPeriodTransactions(payPeriodTransactions);
			payrollVO = findCurrentAndPreviousPayPeriod(payrollVO);

			/*
			 * Find actual pay period Input:
			 * previousPayroll,jobAssignment,payPeriodTransaction Output:
			 * payroll.startDate,payroll.endDate
			 */
			payrollVO = findPayActualPeriod(payrollVO);

			// Set the active jobPayrollElments against the job assignment
			JobPayrollElementVO jobPayrollElementVO = new JobPayrollElementVO();
			jobPayrollElementVO.setJobAssignmentId(jobAssigment
					.getJobAssignmentId());
			List<JobPayrollElement> jobPayrollElements = jobBL.getJobService()
					.getJobPayrollByJobAssignment(jobPayrollElementVO);
			// Sort the calculation order by calculationType
			Collections.sort(jobPayrollElements,
					new Comparator<JobPayrollElement>() {
						public int compare(JobPayrollElement m1,
								JobPayrollElement m2) {
							return m1.getCalculationType().compareTo(
									m2.getCalculationType());
						}
					});
			payrollVO.setJobPayrollElements(jobPayrollElements);
			Map<String, JobPayrollElement> jobPayrollElmentMaps = new HashMap<String, JobPayrollElement>();
			for (JobPayrollElement jobPayrollElement : jobPayrollElements) {
				jobPayrollElmentMaps.put(
						jobPayrollElement.getPayrollElementByPayrollElementId()
								.getElementCode(), jobPayrollElement);
			}
			payrollVO.setJobPayrollElementsMap(jobPayrollElmentMaps);
			// Initialise the pay transaction list
			Set<PayrollTransaction> payrollTransactions = new HashSet<PayrollTransaction>(
					0);
			payrollVO.setPayrollTransactions(payrollTransactions);
			/*
			 * Process Pay Transactions Input:
			 * jobPayrollElements,jobPayrollElmentMaps,jobAssignment Output:
			 * payrollTransactions;
			 */
			payrollVO = getAllPayTransactions(payrollVO);

			payrollVO = getPerDayAndPerHourSalary(payrollVO);

			/*
			 * Get Loss of pay occurrence from leave process Input:
			 * jobPayrollElements,jobPayrollElmentMaps,jobAssignment Output:
			 * payrollTransactions;
			 */
			payrollVO = getLossOfPayFromLeaveProcess(payrollVO);

			/*
			 * Get Loss of pay and over time occurrence from regular attendance
			 * Input: jobPayrollElements,jobPayrollElmentMaps,jobAssignment
			 * Output: payrollTransactions;
			 */
			payrollVO = getLossOfPayFromAttendance(payrollVO);

			payrollVO = getLossOfPayFromNewJoinies(payrollVO);

			/*
			 * Get Loan & Advance Deduction from employee benefit Input:
			 * jobPayrollElements,jobPayrollElmentMaps,jobAssignment Output:
			 * payrollTransactions;
			 */
			payrollVO = getLoanAndAdvanceDeduction(payrollVO);

			// Payroll Master process
			payrollVO.getPayroll().setPerson(jobAssigment.getPerson());
			payrollVO.getPayroll().setImplementation(getImplementation());
			payrollVO.getPayroll().setJobAssignment(jobAssigment);
			payrollVO.getPayroll().setStatus(
					Constants.HR.PayStatus.InProcess.getCode());

			payrollVO.getPayroll().setIsApprove(
					Byte.parseByte(WorkflowConstants.Status.Published.getCode()
							+ ""));
			payrollVO.getPayroll().setCreatedDate(new Date());
			Double netAmount = 0.0;
			Double netAmountEarning = 0.0;
			Double netAmountDeduction = 0.0;
			for (PayrollTransaction payrollTransaction : payrollVO
					.getPayrollTransactions()) {
				if (payrollTransaction.getPayrollElement().getElementNature() != null
						&& payrollTransaction.getPayrollElement()
								.getElementNature().equals("EARNING")) {
					netAmountEarning += payrollTransaction.getAmount();
				} else if (payrollTransaction.getPayrollElement()
						.getElementNature() != null
						&& payrollTransaction.getPayrollElement()
								.getElementNature().equals("DEDUCTION")) {
					netAmountDeduction += payrollTransaction.getAmount();
				}
			}
			netAmount = ((netAmountEarning - netAmountDeduction));
			payrollVO.getPayroll().setNetAmount(netAmount);
			payrollVO.getPayroll().setIsLedgerTransaction(false);
			payrollVO.getPayroll().setIsSalary(false);
			payrollVO
					.getPayroll()
					.setPayMonth(
							payrollVO.getPayPeriodTransaction().getPayMonth() != null ? payrollVO
									.getPayPeriodTransaction().getPayMonth()
									: payrollVO.getPayPeriodTransaction()
											.getPayWeek() + " Week");
			// Save /Update salary
			payrollVO.getPayroll().setPayPeriodTransaction(
					payrollVO.getPayPeriodTransaction());

		}
		return payrollVO;
	}

	public PayrollTO getPayrollInfoForApproval(Long payrollId) throws Exception {
		PayrollTO payrollTO = new PayrollTO();
		List<PayrollTO> payrollTransactions = new ArrayList<PayrollTO>();
		List<PayrollTO> payrollDeductions = new ArrayList<PayrollTO>();
		Payroll payroll = payrollService.getPayrollDetail(payrollId);
		if (payroll != null) {
			Double lopCount = 0.0;
			payrollTO.setPersonName(payroll.getPerson().getFirstName() + " "
					+ payroll.getPerson().getLastName());
			payrollTO.setPersonNumber(payroll.getPerson().getPersonNumber());
			payrollTO.setCompanyName(payroll.getJobAssignment()
					.getCmpDeptLocation().getCompany().getCompanyName()
					.toUpperCase());
			payrollTO.setDepartmentName(payroll.getJobAssignment()
					.getCmpDeptLocation().getDepartment().getDepartmentName());
			payrollTO.setLocationName(payroll.getJobAssignment()
					.getCmpDeptLocation().getLocation().getLocationName());
			payrollTO.setFromDate(DateFormat.convertDateToString(payroll
					.getStartDate().toString()));
			payrollTO.setDesignationName(payroll.getJobAssignment()
					.getDesignation().getDesignationName());
			payrollTO.setToDate(DateFormat.convertDateToString(payroll
					.getEndDate().toString()));
			payrollTO.setCreatedDate(DateFormat.convertDateToString(payroll
					.getCreatedDate().toString()));
			payrollTO.setCreatedDateStr(DateFormat.convertDateToString(payroll
					.getCreatedDate().toString()));
			payrollTO.setNoOfDays(payroll.getNumberOfDays() + "");
			payrollTO.setLopDays(payroll.getLopDays() + "");
			payrollTO.setNetAmount(AIOSCommons.formatAmount(payroll
					.getNetAmount()));
			if (payroll.getJobAssignment().getPayMode() != null
					&& !payroll.getJobAssignment().getPayMode().equals(""))
				payrollTO.setPaymentMode(payroll.getJobAssignment()
						.getPayMode());
			else
				payrollTO.setPaymentMode("OTHER");

			// Code of pay month
			payrollTO.setPayMonth(payroll.getPayMonth());

			Double earningTotal = 0.0;
			Double deductionTotal = 0.0;
			List<Object> pts = payrollService
					.getPayrollTransactionList(payrollId);
			int i = 0;
			for (Object obj : pts) {
				PayrollTO payrollTransTO = new PayrollTO();

				Object[] objs = (Object[]) obj;
				Double amountTemp = (Double) objs[1];
				PayrollTransaction payrollElement = (PayrollTransaction) objs[2];

				payrollTransTO.setElementName(payrollElement
						.getPayrollElement().getElementName());
				payrollTransTO.setElementType(payrollElement
						.getPayrollElement().getElementType());
				payrollTransTO.setElementNature(payrollElement
						.getPayrollElement().getElementNature());
				if (payrollElement.getPayrollElement().getElementNature()
						.equals("EARNING")) {
					payrollTransTO.setEarningAmount(AIOSCommons
							.formatAmount(amountTemp));
					earningTotal += amountTemp;
					payrollTransactions.add(payrollTransTO);

					if (payrollElement.getPayrollElement().getElementCode()
							.equals("BASIC"))
						payrollTO.setBasicAmount(AIOSCommons
								.formatAmount(amountTemp));

				} else if (payrollElement.getPayrollElement()
						.getElementNature().equals("DEDUCTION")) {
					payrollTransTO.setDeductionAmount(AIOSCommons
							.formatAmount(amountTemp));
					deductionTotal += amountTemp;
					payrollDeductions.add(payrollTransTO);
				}
				i++;
			}

			// LOP Days count
			List<PayrollTransaction> pLopt = payrollService
					.getLOPPayrollTransaction(payrollId);
			if (pLopt != null && pLopt.size() > 0) {
				for (PayrollTransaction payrollLOPTransaction : pLopt) {
					Double numberOfDay = 0.0;
					if (payrollLOPTransaction.getUseCase() != null
							&& payrollLOPTransaction.getUseCase().equals(
									"Attendance")) {
						Attendance attendance = attendanceBL
								.getAttendanceService()
								.getAttendanceInfoFindById(
										payrollLOPTransaction.getRecordId());
						PayrollTO payTO = new PayrollTO();
						if (attendance.getAttendanceDate() != null) {
							payTO.setFromDate(DateFormat
									.convertDateToString(attendance
											.getAttendanceDate().toString()));
							payTO.setToDate(DateFormat
									.convertDateToString(attendance
											.getAttendanceDate().toString()));
						}
						numberOfDay = 1.0;
						if (attendance.getHalfDay() != null
								&& attendance.getHalfDay() == true) {
							numberOfDay = 0.5;
						}

					} else if (payrollLOPTransaction.getUseCase() != null
							&& payrollLOPTransaction.getUseCase().equals(
									"LeaveProcess")) {
						LeaveProcess leaveProcess = leaveProcessBL
								.getLeaveProcessService().getLeaveProcess(
										payrollLOPTransaction.getRecordId());
						PayrollTO payTO = new PayrollTO();
						payTO.setFromDate(DateFormat
								.convertDateToString(payrollLOPTransaction
										.getStartDate().toString()));
						payTO.setToDate(DateFormat
								.convertDateToString(payrollLOPTransaction
										.getEndDate().toString()));
						numberOfDay = 1.0;
						if (leaveProcess.getHalfDay() != null
								&& leaveProcess.getHalfDay() == true) {
							numberOfDay = 0.5;
						}
						payTO.setNoOfDays(numberOfDay + "");

					}
					lopCount += numberOfDay;
				}
			}
			lopCount = payroll.getNumberOfDays() - lopCount;
			payrollTO.setDaysWorked(lopCount + "");
			payrollTO.setCurrency("AED");
			payrollTO.setSalaryType("MONTHLY");
			payrollTO.setNetAmountInWords(AIOSCommons
					.convertAmountToWords(payroll.getNetAmount()));
			payrollTO.setEarningTotal(AIOSCommons.formatAmount(earningTotal));
			payrollTO.setDeductionTotal(AIOSCommons
					.formatAmount(deductionTotal));
			payrollTO.setPayrollTransactions(payrollTransactions);
			payrollTO.setPayrollDeductions(payrollDeductions);
		}
		return payrollTO;
	}

	public List<PayrollTO> getLOPDetails(Long payrollId) {
		List<PayrollTO> lopList = new ArrayList<PayrollTO>();
		List<PayrollTransaction> pts = payrollService
				.getLOPPayrollTransaction(payrollId);
		if (pts != null && pts.size() > 0) {
			for (PayrollTransaction payrollTransaction : pts) {
				if (payrollTransaction.getUseCase() != null
						&& payrollTransaction.getUseCase().equals("Attendance")) {
					Attendance attendance = attendanceBL.getAttendanceService()
							.getAttendanceInfoFindById(
									payrollTransaction.getRecordId());
					PayrollTO payTO = new PayrollTO();
					if (attendance.getAttendanceDate() != null) {
						payTO.setFromDate(DateFormat
								.convertDateToString(attendance
										.getAttendanceDate().toString()));
						payTO.setToDate(DateFormat
								.convertDateToString(attendance
										.getAttendanceDate().toString()));
						payTO.setDateStart(attendance.getAttendanceDate());
						payTO.setDateEnd(attendance.getAttendanceDate());
					}
					Double numberOfDay = 1.0;
					if (attendance.getHalfDay() != null
							&& attendance.getHalfDay() == true) {
						numberOfDay = 0.5;
					}
					payTO.setNoOfDays(numberOfDay.toString());
					payTO.setAmount(AIOSCommons.formatAmount(payrollTransaction
							.getAmount()));
					payTO.setDetails("Loss of pay for Attendance Failure/Deviation");
					lopList.add(payTO);
				} else if (payrollTransaction.getUseCase() != null
						&& payrollTransaction.getUseCase().equals(
								"LeaveProcess")) {
					PayrollTO payTO = new PayrollTO();
					payTO.setFromDate(DateFormat
							.convertDateToString(payrollTransaction
									.getStartDate().toString()));
					payTO.setToDate(DateFormat
							.convertDateToString(payrollTransaction
									.getEndDate().toString()));
					payTO.setDateStart(payrollTransaction.getStartDate());
					payTO.setDateEnd(payrollTransaction.getEndDate());
					DateTime from = new DateTime(
							payrollTransaction.getStartDate());
					DateTime to = new DateTime(payrollTransaction.getEndDate());
					Interval interval = new Interval(from.getMillis(),
							to.getMillis());
					// than get the duration
					Duration duration = interval.toDuration();
					payTO.setNoOfDays((duration.getStandardDays() + 1) + "");
					payTO.setAmount(AIOSCommons.formatAmount(payrollTransaction
							.getAmount()));
					payTO.setDetails("UnPaid Leave");

					lopList.add(payTO);
				}
			}
		}

		return lopList;
	}

	// OT
	public List<PayrollTO> getOTDetails(Long payrollId) {
		List<PayrollTO> lopList = new ArrayList<PayrollTO>();
		List<PayrollTransaction> pts = payrollService
				.getOTPayrollTransaction(payrollId);
		if (pts != null && pts.size() > 0) {
			for (PayrollTransaction payrollTransaction : pts) {
				if (payrollTransaction.getUseCase() != null
						&& payrollTransaction.getUseCase().equals("Attendance")) {
					Attendance attendance = attendanceBL.getAttendanceService()
							.getAttendanceInfoFindById(
									payrollTransaction.getRecordId());
					PayrollTO payTO = new PayrollTO();
					payTO.setFromDate(DateFormat.convertDateToString(attendance
							.getAttendanceDate().toString()));
					payTO.setToDate(DateFormat.convertDateToString(attendance
							.getAttendanceDate().toString()));
					payTO.setDateStart(attendance.getAttendanceDate());
					payTO.setDateEnd(attendance.getAttendanceDate());
					Double numberOfDay = 1.0;
					if (attendance.getHalfDay() != null
							&& attendance.getHalfDay() == true) {
						numberOfDay = 0.5;
					}
					payTO.setNoOfDays(numberOfDay.toString());
					payTO.setAmount(AIOSCommons.formatAmount(payrollTransaction
							.getAmount()));
					payTO.setDetails("Over Time for working more time");
					lopList.add(payTO);
				}
			}
		}

		return lopList;
	}

	public void savePayroll(Payroll payroll,
			List<PayrollTransaction> payrollTransactions) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (payroll != null && payroll.getPayrollId() != null
				&& payroll.getPayrollId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		payrollService.savePayroll(payroll);

		if (payroll.getRelativeId() == null) {
			payroll.setRelativeId(payroll.getPayrollId());
			payrollService.savePayroll(payroll);
		}

		List<PayrollTransaction> deletedTransactions = getDeletedPayrollTransaction(
				payrollTransactions,
				new ArrayList<PayrollTransaction>(payroll
						.getPayrollTransactions()));

		for (PayrollTransaction payrollTransaction : payrollTransactions) {
			if (payrollTransaction.getPayrollTransactionId() == null) {
				payrollTransaction.setPayroll(payroll);
			}
		}
		if (deletedTransactions != null) {
			// payrollTransactions.addAll(deletedTransactions);
			payrollService.deletePayrollTransaction(deletedTransactions);
		}

		payrollService.savePayrollTransaction(payrollTransactions);

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Payroll.class.getSimpleName(), payroll.getPayrollId(), user,
				workflowDetailVo);

	}

	public void directSavePayroll(Payroll payroll,
			List<PayrollTransaction> payrollTransactions) throws Exception {

		payrollService.savePayroll(payroll);

		for (PayrollTransaction payrollTransaction : payrollTransactions) {
			if (payrollTransaction.getPayrollTransactionId() == null) {
				payrollTransaction.setPayroll(payroll);
			}
		}

		payrollService.savePayrollTransaction(payrollTransactions);

	}

	public List<PayrollTransaction> getDeletedPayrollTransaction(
			List<PayrollTransaction> payrollTransactions,
			List<PayrollTransaction> payrollTransactionEdits) {
		List<PayrollTransaction> payrollTransactionDeletes = new ArrayList<PayrollTransaction>();

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		for (PayrollTransaction payrollTransaction1 : payrollTransactions) {
			if (payrollTransaction1.getPayrollTransactionId() != null
					&& payrollTransaction1.getPayrollTransactionId() > 0)
				listOne.add(payrollTransaction1.getPayrollTransactionId());
		}
		for (PayrollTransaction payrollTransaction2 : payrollTransactionEdits) {
			if (payrollTransaction2.getPayrollTransactionId() != null
					&& payrollTransaction2.getPayrollTransactionId() > 0)
				listTwo.add(payrollTransaction2.getPayrollTransactionId());
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);

		// Delete Process
		for (Long lng : different) {
			for (PayrollTransaction payrollTransaction3 : payrollTransactionEdits) {
				if (lng != null
						&& payrollTransaction3.getPayrollTransactionId()
								.equals(lng)) {
					payrollTransaction3
							.setStatus(Constants.HR.PayTransactionStatus.Suspended
									.getCode());
					payrollTransactionDeletes.add(payrollTransaction3);
				}
			}
		}
		return payrollTransactionDeletes;
	}

	public List<PayrollVO> wpsExecution(List<JobAssignment> jobAssignmentList,
			String payMonth) throws Exception {

		List<PayrollVO> payrollVOS = new ArrayList<PayrollVO>();
		for (JobAssignment jobAssignment : jobAssignmentList) {

			Payroll tempPayroll = payrollService.getAllPayrollToWPS(
					jobAssignment, payMonth);
			if (tempPayroll != null) {
				PayrollVO payrollVO = payrollToPayrollVOConverter(tempPayroll);
				payrollVOS.add(payrollVO);
			}
		}

		return payrollVOS;
	}

	public PayrollVO payrollToPayrollVOConverter(Payroll payroll)
			throws Exception {
		PayrollVO payrollVO = new PayrollVO();
		payrollVO.setRecordType("EDR");
		// Labour Card
		if (payroll.getJobAssignment() != null
				&& payroll.getJobAssignment().getPerson().getIdentities() != null) {
			for (Identity identity : payroll.getJobAssignment().getPerson()
					.getIdentities()) {
				if (identity.getIdentityType().equals(2))
					payrollVO.setLabourCardId(identity.getIdentityNumber()
							.trim());
			}
		}
		// Routing & Iban Number
		if (payroll.getJobAssignment().getPersonBank() != null
				&& payroll.getJobAssignment().getPersonBank().getRoutingCode() != null) {
			payrollVO.setRoutingCode(payroll.getJobAssignment().getPersonBank()
					.getRoutingCode().trim());
		}
		if (payroll.getJobAssignment().getPersonBank() != null
				&& payroll.getJobAssignment().getPersonBank()
						.getAccountNumber() != null) {
			payrollVO.setIbanNumber(payroll.getJobAssignment().getPersonBank()
					.getIban().trim());
		}

		payrollVO.setStartDate(payroll.getStartDate());
		payrollVO.setEndDate(payroll.getEndDate());
		// Period Start & End
		SimpleDateFormat formatter = new SimpleDateFormat("MMM,yyyy");
		Date tempDate = formatter.parse(payroll.getPayMonth());
		Date start = payroll.getStartDate();
		Date end = payroll.getEndDate();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(tempDate);
		calendar.set(Calendar.DAY_OF_MONTH,
				calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		payroll.setStartDate(calendar.getTime());

		calendar.set(Calendar.DAY_OF_MONTH,
				calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		payroll.setEndDate(calendar.getTime());

		SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd");
		payrollVO.setPeriodStart(outFormat.format(start));
		payrollVO.setPeriodEnd(outFormat.format(end));

		payrollVO.setNumberOfDays(payroll.getNumberOfDays());

		payrollVO.setVariableSalary("0.00");
		payrollVO.setNetAmountStr(AIOSCommons.roundTwoDecimalsWPS(payroll
				.getNetAmount()));
		payrollVO.setNetAmount(Double.parseDouble(AIOSCommons
				.roundTwoDecimalsWPS(payroll.getNetAmount())));
		// LOP Days
		if (payroll.getLopDays() != null
				&& !payroll.getLopDays().trim().equals(""))
			payrollVO.setLopDays(AIOSCommons.roundLeaveDays(payroll
					.getLopDays()));
		else
			payrollVO.setLopDays("0");

		payrollVO.setEmployeeName(payroll.getJobAssignment().getPerson()
				.getFirstName()
				+ " "
				+ payroll.getJobAssignment().getPerson().getLastName()
				+ "-"
				+ payroll.getJobAssignment().getPerson().getPersonNumber());

		payrollVO.setJobAssignment(payroll.getJobAssignment());

		payrollVO.setPayMonth(payroll.getPayMonth());

		payrollVO.setStatus(payroll.getStatus());

		payrollVO.setPayrollId(payroll.getPayrollId());

		payrollVO.setPayroll(payroll);

		return payrollVO;

	}

	public PayrollVO payrollToVOConverter(Payroll payroll) throws Exception {
		PayrollVO payrollVO = new PayrollVO();

		payrollVO.setJobAssignment(payroll.getJobAssignment());

		payrollVO.setPayMonth(payroll.getPayMonth());

		payrollVO.setStatus(payroll.getStatus());

		payrollVO.setPayrollId(payroll.getPayrollId());

		payrollVO
				.setPayrollTransactionVOs(payrollTransactionToPayrollTransactionVOConverter(new ArrayList<PayrollTransaction>(
						payroll.getPayrollTransactions())));
		payrollVO.setPayroll(payroll);

		return payrollVO;

	}

	public List<PayrollTransactionVO> payrollTransactionToPayrollTransactionVOConverter(
			List<PayrollTransaction> transactions) throws Exception {
		List<PayrollTransactionVO> transactionVOs = new ArrayList<PayrollTransactionVO>();

		PayrollTransactionVO vo = null;
		for (PayrollTransaction payTransaction : transactions) {
			vo = new PayrollTransactionVO();
			BeanUtils.copyProperties(vo, payTransaction);
			if (payTransaction.getStartDate() != null)
				vo.setFromDate(DateFormat.convertDateToString(payTransaction
						.getStartDate() + ""));
			if (payTransaction.getEndDate() != null)
				vo.setToDate(DateFormat.convertDateToString(payTransaction
						.getEndDate() + ""));
			vo.setElementName(payTransaction.getPayrollElement()
					.getElementName());
			vo.setElementNature(payTransaction.getPayrollElement()
					.getElementNature());
			vo.setCalculatedAmount(payTransaction.getAmount());
			if (vo.getNote() == null)
				vo.setNote("");

			transactionVOs.add(vo);
		}

		return transactionVOs;
	}

	public String updatePayStatus(Long accountId, List<Payroll> payrolls,
			Double exchangeRate) {
		String returnMessage = "SUCCESS";
		try {
			List<Payroll> payrollExsistList = this.getPayrollService()
					.getAllPayroll(getImplementation());
			List<PayrollVO> payrollNewListVO = new ArrayList<PayrollVO>();
			for (Payroll payrollActual : payrollExsistList) {
				for (Payroll payrollSelected : payrolls) {
					if (payrollSelected.getPayrollId().equals(
							payrollActual.getPayrollId())) {
						PayrollVO payrollVo = new PayrollVO();
						payrollVo.setPayroll(payrollActual);
						payrollVo.setJobAssignment(payrollActual
								.getJobAssignment());
						payrollVo.setPayStatusId(payrollSelected.getStatus());
						payrollNewListVO.add(payrollVo);
					}
				}
			}

			// ***********************Payroll
			// Transaction***************************************************

			// Remove the payroll which has completed transaction already
			List<Payroll> payTransList = new ArrayList<Payroll>();
			for (PayrollVO payrollVO : payrollNewListVO) {
				if (payrollVO.getPayStatusId() == Constants.HR.PayStatus.Commit
						.getCode()
						&& payrollVO.getPayroll().getStatus() != Constants.HR.PayStatus.Commit
								.getCode()
						&& (payrollVO.getPayroll().getJobAssignment()
								.getIsLocalPay() == null || payrollVO
								.getPayroll().getJobAssignment()
								.getIsLocalPay() == false)
						&& exchangeRate != null && exchangeRate != 0.0) {
					Double localMoney = 1 / exchangeRate;
					for (PayrollTransaction payTransaction : payrollVO
							.getPayroll().getPayrollTransactions()) {
						payTransaction.setAmount(payTransaction.getAmount()
								/ localMoney);
					}
					payrollVO.getPayroll().setNetAmount(
							payrollVO.getPayroll().getNetAmount() / localMoney);
					payTransList.add(payrollVO.getPayroll());
				} else if (payrollVO.getPayStatusId() == Constants.HR.PayStatus.Commit
						.getCode()
						&& payrollVO.getPayroll().getStatus() != Constants.HR.PayStatus.Commit
								.getCode()) {
					payTransList.add(payrollVO.getPayroll());
				}
			}
			List<TransactionDetail> masterList = null;
			List<TransactionDetail> transList = null;
			Category childCategory = null;
			Category masterCategory = null;
			boolean isCrditSide = false;
			Transaction transaction = null;
			TransactionDetail transactionDetail = null;
			for (Payroll payrollActual : payTransList) {

				childCategory = transactionBL.getCategory("Salary Expense");
				masterCategory = transactionBL.getCategory("Salary Payable");
				masterList = new ArrayList<TransactionDetail>();
				transList = new ArrayList<TransactionDetail>();
				for (PayrollTransaction payrollTransaction : payrollActual
						.getPayrollTransactions()) {
					isCrditSide = false;
					// Skip the accounts transaction which is paid already by
					// other mode.
					if ((byte) payrollTransaction.getStatus() != (byte) Constants.HR.PayTransactionStatus.New
							.getCode()
							|| payrollTransaction.getAmount() == null
							|| payrollTransaction.getAmount() <= 0)
						continue;

					transactionDetail = new TransactionDetail();

					payrollTransaction
							.getPayrollElement()
							.setCombination(
									combinationService
											.getCombinationAllAccountDetail(payrollTransaction
													.getPayrollElement()
													.getCombination()
													.getCombinationId()));
					// Find to credit / debit
					if (((int) payrollTransaction.getPayrollElement()
							.getCombination().getAccountByNaturalAccountId()
							.getAccountType() == (int) Accounts.AccountType.Revenue
							.getCode())
							|| ((int) payrollTransaction.getPayrollElement()
									.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountType() == (int) Accounts.AccountType.Assets
									.getCode()))
						isCrditSide = true;

					if (!isCrditSide) {
						// -----------------Salary Expense
						// Debit------------------------------

						String descriptionDebit = "Salary Expense debit for "
								+ payrollActual.getJobAssignment().getPerson()
										.getFirstName()
								+ " "
								+ payrollActual.getJobAssignment().getPerson()
										.getLastName()
								+ " of "
								+ payrollTransaction.getPayrollElement()
										.getElementName();

						transactionDetail = this.getTransactionBL()
								.createTransactionDetail(
										payrollTransaction.getAmount(),
										payrollTransaction.getPayrollElement()
												.getCombination()
												.getCombinationId(), true,
										descriptionDebit, null, null);

						transList.add(transactionDetail);

					} else {
						// --------------Salary Payable
						// Credit--------------------------------
						// Description
						String descriptionCredit = "Salary Payable credit for "
								+ payrollActual.getJobAssignment().getPerson()
										.getFirstName()
								+ " "
								+ payrollActual.getJobAssignment().getPerson()
										.getLastName()
								+ " of "
								+ payrollTransaction.getPayrollElement()
										.getElementName();

						transactionDetail = this.getTransactionBL()
								.createTransactionDetail(
										payrollTransaction.getAmount(),
										payrollTransaction.getPayrollElement()
												.getCombination()
												.getCombinationId(), false,
										descriptionCredit, null, null);

						transList.add(transactionDetail);
					}

				}
				// Find Net amount
				PayrollVO payVO = calculateNetAmount(transList);

				// -------------Payable Credit------------------------
				Combination personCombination = personBL
						.findCombinationInfo(payrollActual.getJobAssignment()
								.getPerson().getFirstName()
								+ " "
								+ payrollActual.getJobAssignment().getPerson()
										.getLastName()
								+ "-"
								+ payrollActual.getJobAssignment().getPerson()
										.getPersonNumber() + "-EMPLOYEE");

				String descriptionDebit = "Salary Payable Credit for "
						+ payrollActual.getJobAssignment().getPerson()
								.getFirstName()
						+ " "
						+ payrollActual.getJobAssignment().getPerson()
								.getLastName();

				transactionDetail = this.getTransactionBL()
						.createTransactionDetail(
								Math.abs(payVO.getNetAmount()),
								personCombination.getCombinationId(), false,
								descriptionDebit, null, null);

				transList.add(transactionDetail);

				transaction = this.getTransactionBL().createTransaction(
						this.getTransactionBL().getCurrency().getCurrencyId(),
						"SALARY EXPENSE FOR EMPLOYEE "
								+ payrollActual.getJobAssignment().getPerson()
										.getFirstName()
								+ " "
								+ payrollActual.getJobAssignment().getPerson()
										.getLastName(), new Date(),
						childCategory, payrollActual.getPayrollId(), "Payroll");
				this.getTransactionBL().saveTransaction(transaction, transList);

				// **********************************************Payroll Master
				// Entry
				// starts*****************************************************************
				// -------------Payable Debit------------------------
				descriptionDebit = "Salary Payable Debit for "
						+ payrollActual.getJobAssignment().getPerson()
								.getFirstName()
						+ " "
						+ payrollActual.getJobAssignment().getPerson()
								.getLastName();

				transactionDetail = this.getTransactionBL()
						.createTransactionDetail(payrollActual.getNetAmount(),
								personCombination.getCombinationId(), true,
								descriptionDebit, null, null);

				masterList.add(transactionDetail);

				// ---------------------Bank Credit-----------------------------
				Combination bankCombination = null;
				List<BankAccount> bankAccounts = null;
				if (accountId != null) {
					bankCombination = combinationService
							.getCombinationAllAccountByBankAccount(accountId);
					bankAccounts = new ArrayList<BankAccount>(
							bankCombination.getBankAccountsForCombinationId());
				}

				String descriptionCredit = "Bank credit for bank account : "
						+ bankAccounts.get(0).getAccountNumber()
						+ " employee : "
						+ payrollActual.getJobAssignment().getPerson()
								.getFirstName()
						+ " "
						+ payrollActual.getJobAssignment().getPerson()
								.getLastName();

				transactionDetail = this.getTransactionBL()
						.createTransactionDetail(payrollActual.getNetAmount(),
								bankCombination.getCombinationId(), false,
								descriptionCredit, null, null);

				masterList.add(transactionDetail);

				transaction = this.getTransactionBL()
						.createTransaction(
								this.getTransactionBL().getCurrency()
										.getCurrencyId(),
								"SALARY PAYABLE FOR EMPLOYEE "
										+ payrollActual.getJobAssignment()
												.getPerson().getFirstName()
										+ " "
										+ payrollActual.getJobAssignment()
												.getPerson().getLastName(),
								new Date(), masterCategory,
								payrollActual.getPayrollId(), "Payroll");
				this.getTransactionBL()
						.saveTransaction(transaction, masterList);
			}

			// ********************************Payroll Update
			// section***********************************

			// Remove the payroll which has completed transaction already
			List<Payroll> payUpdateList = new ArrayList<Payroll>();
			for (PayrollVO payrollVO : payrollNewListVO) {
				payrollVO.getPayroll().setStatus(payrollVO.getPayStatusId());
				payUpdateList.add(payrollVO.getPayroll());
			}
			payrollService.saveAllPayroll(payUpdateList);

		} catch (Exception e) {
			e.printStackTrace();
			returnMessage = "Can not update the payroll";
		}
		return returnMessage;
	}

	public PayrollVO calculateNetAmount(List<TransactionDetail> transList) {
		PayrollVO payVO = null;
		if (transList != null && transList.size() > 0) {
			payVO = new PayrollVO();
			Double totalCrdit = 0.0;
			Double totalDebit = 0.0;
			for (TransactionDetail transactionDetail : transList) {
				if (transactionDetail.getIsDebit())
					totalDebit += transactionDetail.getAmount();
				else
					totalCrdit += transactionDetail.getAmount();
			}
			payVO.setNetAmount(totalDebit - totalCrdit);
		}
		return payVO;
	}

	public Combination getCombinationDetails(Long combinationId)
			throws Exception {
		Combination combination = this.getTransactionBL()
				.getCombinationService()
				.getCombination(combinationId.intValue());
		return combination;
	}

	public Combination getCombinationByAccount(Long accountId) throws Exception {
		Combination combination = this.getTransactionBL()
				.getCombinationService().getCombinationByAccount(accountId);
		return combination;
	}

	public List<PayrollVO> getPayrollUpdate() throws Exception {
		PayrollVO globalPayrollVO = new PayrollVO();
		List<PayrollVO> payrollVOS = new ArrayList<PayrollVO>();
		List<JobAssignment> jobAssignments = jobAssignmentBL
				.getJobAssignmentService().getAllActiveJobAssignment(
						getImplementation());
		PayPeriodTransaction payPeriodTransaction = null;
		PayPeriodVO payPeriodVO = null;
		PayrollVO payrollVO = null;

		if (jobAssignments != null && jobAssignments.size() > 0) {
			for (JobAssignment jobAssignment : jobAssignments) {
				globalPayrollVO.setJobAssignment(jobAssignment);
				globalPayrollVO = findPayPeriodOfLastMonthPay(globalPayrollVO);
				PayPeriodTransaction ppt = globalPayrollVO.getPreviousPeriod() != null ? globalPayrollVO
						.getPreviousPeriod() : globalPayrollVO
						.getPayPeriodTransaction();
				List<Payroll> tempPayrolls = payrollService
						.getPayrollBasedonSameCombination(
								jobAssignment.getPerson(),
								ppt.getPayPeriodTransactionId());
				if (tempPayrolls != null && tempPayrolls.size() > 0) {
					payrollVO = new PayrollVO();
					payrollVO = payrollToPayrollVOConverterPrint(tempPayrolls
							.get(0));
					payrollVOS.add(payrollVO);
				} else {
					globalPayrollVO.setJobAssignment(jobAssignment);
					payrollVO = personIntoPayrollViewFormat(globalPayrollVO);
					payrollVOS.add(payrollVO);
				}
			}
		}
		return payrollVOS;
	}

	public List<PayrollVO> getPayrollToReportPrint(String payMonth)
			throws Exception {
		List<PayrollVO> payrollVOS = new ArrayList<PayrollVO>();
		PayrollVO payrollVO = null;

		List<Payroll> tempPayrolls = payrollService
				.getAllPayrollBasedonPayMonth(getImplementation(), payMonth);
		Collections.sort(tempPayrolls, new Comparator<Payroll>() {
			public int compare(Payroll m1, Payroll m2) {
				if (m1.getJobAssignment().getPayMode() != null
						&& m2.getJobAssignment().getPayMode() != null)
					return m1.getJobAssignment().getPayMode()
							.compareTo(m2.getJobAssignment().getPayMode());
				else
					return 0;
			}
		});
		if (tempPayrolls != null && tempPayrolls.size() > 0) {
			for (Payroll payroll : tempPayrolls) {
				if ((payroll.getJobAssignment().getEndDate() != null ? (payroll
						.getPayPeriodTransaction().getEndDate().after(payroll
						.getJobAssignment().getEndDate())) : false)) {
					continue;
				}
				payrollVO = new PayrollVO();
				payrollVO = payrollToPayrollVOConverterPrint(payroll);
				payrollVOS.add(payrollVO);
			}
		}
		return payrollVOS;
	}

	public PayrollVO findPayPeriodOfLastMonthPay(PayrollVO payrollVO) {
		try {
			/*
			 * Get all pay period transactions Input: payPolicy,jobAssignmentId
			 * Output: payPeriodTransactions
			 */
			PayPeriodVO payPeriodVO = new PayPeriodVO();
			payPeriodVO.setPayPolicy(payrollVO.getPayPolicy());
			payPeriodVO.setJobAssignmentId(payrollVO.getJobAssignment()
					.getJobAssignmentId());
			List<PayPeriodTransaction> payPeriodTransactions = payPeriodBL
					.findActivePayPeriods(payPeriodVO);

			Payroll previousPayroll = payrollService
					.getPayrollBasedonPerson(payrollVO.getJobAssignment()
							.getPerson());

			/*
			 * Find current and previous payPeriod Input:
			 * previousPayroll,payPeriodTransactions Output:
			 * previousPeriod,payPeriodTransaction
			 */
			payrollVO.setPreviousPayroll(previousPayroll);
			payrollVO.setPayPeriodTransactions(payPeriodTransactions);
			payrollVO = findCurrentAndPreviousPayPeriod(payrollVO);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return payrollVO;
	}

	public List<JobPayrollElementVO> jobPayrollElements(
			JobPayrollElementVO jobPayVO) {
		List<JobPayrollElementVO> payElementVOs = null;

		try {
			Map<String, String> actions = payActionInformation();
			jobPayVO.setUseCalculationType(true);
			List<JobPayrollElement> jobPayrollElements = jobBL.getJobService()
					.getJobPayrollByJobAssignment(jobPayVO);
			if (jobPayrollElements != null && jobPayrollElements.size() > 0) {
				payElementVOs = new ArrayList<JobPayrollElementVO>();
				for (JobPayrollElement jobPayrollElement : jobPayrollElements) {
					JobPayrollElementVO jobPayrollElementVO = convertEntityIntoVO(jobPayrollElement);
					jobPayrollElementVO.setJobAssignmentId(jobPayVO
							.getJobAssignmentId());
					if ((byte) jobPayrollElement.getCalculationType() == Constants.HR.CalculationType.Count
							.getCode()) {
						jobPayrollElementVO.setAllowanceUrl("pay_by_count");
					} else if (actions.containsKey(jobPayrollElement
							.getPayrollElementByPayrollElementId()
							.getElementCode())) {
						jobPayrollElementVO.setAllowanceUrl(actions
								.get(jobPayrollElement
										.getPayrollElementByPayrollElementId()
										.getElementCode()));
					} else {
						jobPayrollElementVO.setAllowanceUrl("allowance");
					}
					payElementVOs.add(jobPayrollElementVO);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return payElementVOs;
	}

	public List<JobPayrollElementVO> getBasicPayrollElements(
			JobPayrollElementVO jobPayVO) {
		List<JobPayrollElementVO> payElementVOs = null;

		try {
			List<JobPayrollElement> jobPayrollElements = jobBL.getJobService()
					.getJobPayrollByJobAssignment(jobPayVO);
			if (jobPayrollElements != null && jobPayrollElements.size() > 0) {
				payElementVOs = new ArrayList<JobPayrollElementVO>();
				for (JobPayrollElement jobPayrollElement : jobPayrollElements) {
					if (jobPayrollElement.getPayrollElementByPayrollElementId()
							.getElementCode().equalsIgnoreCase("BASIC")) {
						JobPayrollElementVO jobPayrollElementVO = convertEntityIntoVO(jobPayrollElement);
						jobPayrollElementVO.setJobAssignmentId(jobPayVO
								.getJobAssignmentId());
						payElementVOs.add(jobPayrollElementVO);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return payElementVOs;
	}

	public JobPayrollElementVO convertEntityIntoVO(
			JobPayrollElement jobPayrollElement) {
		JobPayrollElementVO jobPayrollElementVO = new JobPayrollElementVO();
		try {
			BeanUtils.copyProperties(jobPayrollElementVO, jobPayrollElement);
			jobPayrollElementVO.setElementName(jobPayrollElement
					.getPayrollElementByPayrollElementId().getElementName());
			jobPayrollElementVO.setAllowedMaixmumAmount(AIOSCommons
					.formatAmount(jobPayrollElement.getAmount()));

			if ((byte) Constants.HR.CalculationType.get(
					jobPayrollElement.getCalculationType()).getCode() == (byte) Constants.HR.CalculationType.Count
					.getCode()) {
				jobPayrollElementVO
						.setCalculationTypeView(Constants.HR.CalculationType
								.get(jobPayrollElement.getCalculationType())
								.name()
								+ "(" + jobPayrollElement.getCount() + ")");
			} else {
				jobPayrollElementVO
						.setCalculationTypeView(Constants.HR.CalculationType
								.get(jobPayrollElement.getCalculationType())
								.name());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return jobPayrollElementVO;
	}

	public JobPayrollElementVO getJobPayrollInfomration(
			Long jobPayrollElementId, List<Object> objects,
			PayPeriodTransaction payPeriodTransaction) {
		JobPayrollElementVO jobPayrollElementVO = null;
		try {
			JobPayrollElement jobPayrollElement = jobBL.getJobService()
					.getJobPayrollElementById(jobPayrollElementId);
			jobPayrollElementVO = convertEntityIntoVO(jobPayrollElement);
			jobPayrollElementVO.setPeriodStartDate(DateFormat
					.convertDateToString(payPeriodTransaction.getStartDate()
							.toString()));
			jobPayrollElementVO.setPeriodEndDate(DateFormat
					.convertDateToString(payPeriodTransaction.getEndDate()
							.toString()));
			Double tempAmount = 0.0;
			for (Object object : objects) {
				if (object != null) {
					Method method = object.getClass().getMethod("getAmount");
					tempAmount += (Double) method.invoke(object, null);
				}
			}
			jobPayrollElementVO.setAvailableAmount(AIOSCommons
					.formatAmountIncludingNegative((jobPayrollElementVO
							.getAmount() - tempAmount)));
		} catch (Exception e) {

		}

		return jobPayrollElementVO;
	}

	public Map<String, String> payActionInformation() {
		Map<String, String> types = new HashMap<String, String>();
		types.put("PHONE", "phone_allowance");
		types.put("FUEL", "fuel_allowance");
		types.put("HRA", "house_rent_allowance");
		types.put("PARKING", "parking_allowance");
		types.put("TICKET", "ticket_allowance");
		return types;
	}

	public Map<String, String> payUseCaseInformation() {
		Map<String, String> types = new HashMap<String, String>();
		types.put("PHONE", "PhoneAllowance");
		types.put("FUEL", "FuelAllowance");
		types.put("HRA", "HouseRentAllowance");
		types.put("PARKING", "ParkingAllowance");
		types.put("TICKET", "TicketAllowance");
		types.put("OTHER", "Allowance");
		return types;
	}

	/*
	 * Find current and previous payPeriod input:
	 * previousPayroll,payPeriodTransactions output:
	 * previousPeriod,payPeriodTransaction
	 */
	public PayrollVO findCurrentAndPreviousPayPeriod(PayrollVO payrollVO) {
		Map<Long, PayPeriodTransaction> mapTransactions = new HashMap<Long, PayPeriodTransaction>();
		Map<Integer, PayPeriodTransaction> mapOrderTransactions = new HashMap<Integer, PayPeriodTransaction>();
		for (PayPeriodTransaction payPeriodTransaciton : payrollVO
				.getPayPeriodTransactions()) {

			mapTransactions.put(
					payPeriodTransaciton.getPayPeriodTransactionId(),
					payPeriodTransaciton);
			// Set with order
			mapOrderTransactions.put(payPeriodTransaciton.getRecordOrder(),
					payPeriodTransaciton);
		}
		if (payrollVO.getPreviousPayroll() != null) {
			payrollVO.setPreviousPeriod(payrollVO.getPreviousPayroll()
					.getPayPeriodTransaction());

			// Find the period is in current active set or some old period set
			if (payrollVO.getPreviousPeriod() != null
					&& mapTransactions.containsKey(payrollVO
							.getPreviousPeriod().getPayPeriodTransactionId())) {
				payrollVO
						.setPayPeriodTransaction(mapOrderTransactions
								.get(payrollVO.getPreviousPeriod()
										.getRecordOrder() + 1));
			} else {
				payrollVO.getPayPeriodVO().setTransactionDate(
						payrollVO.getJobAssignment().getEffectiveDate());
				payrollVO.setPayPeriodTransaction(payPeriodBL
						.findPayPeriod(payrollVO.getPayPeriodVO()));
			}
		} else {
			payrollVO.setPayPeriodTransaction(payPeriodBL
					.findPayPeriod(payrollVO.getPayPeriodVO()));
			// payrollVO.setPayPeriodTransaction(mapOrderTransactions.get(1));
		}
		if (payrollVO.getPayMonth() != null
				&& !payrollVO.getPayMonth().equals(
						payrollVO.getPayPeriodTransaction().getPayMonth()))
			payrollVO.setPayPeriodTransaction(null);

		if (payrollVO.getPayPeriodTransaction() != null
				&& (payrollVO.getJobAssignment().getEndDate() != null ? (payrollVO
						.getPayPeriodTransaction().getEndDate().after(payrollVO
						.getJobAssignment().getEndDate())) : false)) {
			payrollVO.setPayPeriodTransaction(null);
		}

		return payrollVO;
	}

	public PayrollVO findPayActualPeriod(PayrollVO payrollVO) {
		if (payrollVO.getPreviousPayroll() != null
				&& payrollVO.getPreviousPayroll().getEndDate() != null) {
			payrollVO.getPayroll().setStartDate(
					payrollVO.getPayPeriodTransaction().getStartDate());
		} else {
			payrollVO.getPayroll().setStartDate(
					payrollVO.getJobAssignment().getEffectiveDate());
		}
		// current date minus one day if cutoff date is empty
		LocalDate currentEnd = new LocalDate(new Date());
		currentEnd = currentEnd.minusDays(1);
		LocalDate periodEnd = new LocalDate(payrollVO.getPayPeriodTransaction()
				.getEndDate());
		if (periodEnd.isBefore(currentEnd)) {
			payrollVO.getPayroll().setEndDate(periodEnd.toDate());
		} else {
			payrollVO.getPayroll().setEndDate(periodEnd.toDate());

			if (payrollVO.getCutOffDate() != null
					&& payrollVO.isCutOffFromEnd()) {
				payrollVO.getPayroll().setEndDate(payrollVO.getCutOffDate());
			}
		}
		int days = Days
				.daysBetween(
						new LocalDate(payrollVO.getPayPeriodTransaction()
								.getStartDate()),
						new LocalDate(payrollVO.getPayPeriodTransaction()
								.getEndDate())).getDays();

		payrollVO.getPayroll().setNumberOfDays(days + 1);
		return payrollVO;
	}

	public PayrollVO getAllPayTransactions(PayrollVO payrollVO) {
		try {
			PayrollTransaction payrollTransaction = null;
			List<PayrollTransaction> payrollTransactions = null;
			Map<String, String> actions = payActionInformation();
			Map<String, String> useCases = payUseCaseInformation();
			Double totalGrossSalary = 0.0;
			for (JobPayrollElement jobPayrollElement : payrollVO
					.getJobPayrollElements()) {

				if ((byte) jobPayrollElement.getPayType() != (byte) Constants.HR.PayMode.Salary
						.getCode())
					continue;

				if ((byte) jobPayrollElement.getCalculationType() == (byte) Constants.HR.CalculationType.Variance
						.getCode()
						|| (byte) jobPayrollElement.getCalculationType() == (byte) Constants.HR.CalculationType.Count
								.getCode()) {
					if ((byte) jobPayrollElement.getCalculationType() == (byte) Constants.HR.CalculationType.Count
							.getCode()) {
						payrollVO.setUseCase("PayByCount");
					} else if (actions.containsKey(jobPayrollElement
							.getPayrollElementByPayrollElementId()
							.getElementCode())) {
						payrollVO.setUseCase(useCases.get(jobPayrollElement
								.getPayrollElementByPayrollElementId()
								.getElementCode()));
					} else {
						payrollVO.setUseCase(useCases.get("OTHER"));
					}
					payrollTransactions = new ArrayList<PayrollTransaction>();
					payrollTransactions = this.getAllowanceList(payrollVO);
				} else {
					if ((byte) jobPayrollElement.getCalculationType() == (byte) Constants.HR.CalculationType.Fixed
							.getCode()) {
						payrollTransaction = new PayrollTransaction();
						payrollTransactions = new ArrayList<PayrollTransaction>();
						payrollTransaction.setAmount(jobPayrollElement
								.getAmount());
						payrollTransaction
								.setStatus(Constants.HR.PayTransactionStatus.New
										.getCode());
						payrollTransactions.add(payrollTransaction);
					} else if ((byte) jobPayrollElement.getCalculationType() == (byte) Constants.HR.CalculationType.Percentage
							.getCode()) {
						Double amount = 0.0;
						amount = findPercentageAmount(jobPayrollElement,
								payrollVO);
						payrollTransaction = new PayrollTransaction();
						payrollTransactions = new ArrayList<PayrollTransaction>();
						payrollTransaction.setAmount(amount);
						payrollTransaction
								.setStatus(Constants.HR.PayTransactionStatus.New
										.getCode());
						payrollTransactions.add(payrollTransaction);
					}
				}

				// Set payroll transaction other parameter in general way
				if (payrollTransactions != null
						&& payrollTransactions.size() > 0) {
					payrollVO.getPayrollTransactions().addAll(
							setPayrollTransactionOtherParam(
									payrollTransactions, jobPayrollElement,
									payrollVO));

					for (PayrollTransaction payrollTransaction2 : payrollTransactions) {
						totalGrossSalary += payrollTransaction2.getAmount();
					}
				}
			}

			if (payrollVO.getJobAssignment().getJobAssignmentId() != null
					&& payrollVO.getPayPeriodTransaction() != null) {
				Payroll payroll = payrollService.getPayrollDetailByUseCase(
						payrollVO.getJobAssignment().getJobAssignmentId(),
						"PayPeriod"
								+ payrollVO.getPayPeriodTransaction()
										.getPayPeriodTransactionId());
				if (payroll != null) {
					for (PayrollTransaction payrollTransaction2 : payroll
							.getPayrollTransactions()) {
						payrollTransaction = new PayrollTransaction();
						BeanUtils.copyProperties(payrollTransaction,
								payrollTransaction2);
						payrollTransaction.setPayrollTransactionId(null);
						payrollVO.getPayrollTransactions().add(
								payrollTransaction);
					}
				}
			}
			payrollVO.setTotalGross(totalGrossSalary.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return payrollVO;
	}

	public List<PayrollTransaction> setPayrollTransactionOtherParam(
			List<PayrollTransaction> transaction,
			JobPayrollElement jobPayrollElement, PayrollVO payrollVO) {
		if (transaction != null && transaction.size() > 0)
			for (PayrollTransaction payrollTransaction : transaction) {
				payrollTransaction.setCreatedDate(new Date());
				payrollTransaction.setImplementation(getImplementation());
				payrollTransaction.setPayrollElement(jobPayrollElement
						.getPayrollElementByPayrollElementId());

				payrollTransaction.setPerson(payrollVO.getJobAssignment()
						.getPerson());
				payrollTransaction.setJobPayrollElement(jobPayrollElement);
			}
		return transaction;
	}

	public Double findPercentageAmount(JobPayrollElement jobPayrollElement,
			PayrollVO payrollVO) {
		Double returnAmount = 0.0;
		for (PayrollTransaction payTrans : payrollVO.getPayrollTransactions()) {
			if ((long) jobPayrollElement.getPayrollElementByPercentageElement()
					.getPayrollElementId() == (long) payTrans
					.getPayrollElement().getPayrollElementId()) {
				returnAmount += payTrans.getAmount();
			}
		}
		if (returnAmount > 0.0) {
			returnAmount = (returnAmount * jobPayrollElement.getAmount()) / 100;
		}
		return returnAmount;
	}

	public PayrollVO getPerDayAndPerHourSalary(PayrollVO payrollVO) {
		Double totalGrossSalary = (payrollVO.getTotalGross() != null ? Double
				.parseDouble(payrollVO.getTotalGross()) : 0.0);
		Double perDayRate = 0.0;
		if ((byte) payrollVO.getJobAssignment().getPayPolicy() == (byte) Constants.HR.PayPolicy.Monthly
				.getCode()) {
			perDayRate = totalGrossSalary
					/ payrollVO.getPayroll().getNumberOfDays();
		} else if ((byte) payrollVO.getJobAssignment().getPayPolicy() == (byte) Constants.HR.PayPolicy.Weekly
				.getCode()) {
			perDayRate = totalGrossSalary * 52 / 360;
		} else if ((byte) payrollVO.getJobAssignment().getPayPolicy() == (byte) Constants.HR.PayPolicy.Daily
				.getCode()) {
			perDayRate = totalGrossSalary;
		}
		payrollVO.setPerDaySalary(perDayRate);
		return payrollVO;
	}

	public PayrollVO getLossOfPayFromLeaveProcess(PayrollVO payrollVO) {

		// Loss of Pay checking with leave process information
		List<LeaveProcess> leaveProcesses = leaveProcessBL
				.getLeaveProcessService().getLossOfPays(
						payrollVO.getJobAssignment().getPerson());

		PayrollElement payrollElementLOP = payrollElementBL
				.getPayrollElementService().getPayrollElementByCode(
						getImplementation(), "LOP");
		for (LeaveProcess leaveProcess : leaveProcesses) {
			boolean flag = false;
			Double numberOfDays = 1.0;
			Double lopDays = 0.0;
			Double lop = 0.0;
			Double totalLopdays = 0.0;
			Double totalLopAmount = 0.0;
			if (leaveProcess.getJobLeave().getLeave().getLookupDetail()
					.getAccessCode().equals("AL")
					&& leaveProcess.getSettleDues() != null
					&& leaveProcess.getSettleDues()) {
				flag = true;
				payrollElementLOP = payrollElementBL.getPayrollElementService()
						.getPayrollElementByCode(getImplementation(),
								"OTHERDED");
				totalLopAmount = leaveProcess.getTotalAmount();
				totalLopdays = leaveProcess.getDays();

			}
			if (leaveProcess.getJobLeave().getLeave().getIsPaid() == null
					|| !leaveProcess.getJobLeave().getLeave().getIsPaid()
					|| (leaveProcess.getEncash() != null && !leaveProcess
							.getEncash()))
				flag = true;

			if (!flag)
				continue;

			Date actualFromDate = findActualStartDate(leaveProcess,
					payrollVO.getPayroll());
			Date actualToDate = findActualEndDate(leaveProcess,
					payrollVO.getPayroll());
			boolean returnStatus = isBetweenInclusive(new LocalDate(payrollVO
					.getPayroll().getStartDate()), new LocalDate(payrollVO
					.getPayroll().getEndDate()), new LocalDate(actualFromDate));
			boolean returnStatus1 = isBetweenInclusive(new LocalDate(payrollVO
					.getPayroll().getStartDate()), new LocalDate(payrollVO
					.getPayroll().getEndDate()), new LocalDate(actualToDate));
			if (!returnStatus || !returnStatus1)
				continue;

			numberOfDays = numberOfDays
					+ DateFormat.dayDifference(actualFromDate, actualToDate);
			if (leaveProcess.getHalfDay() != null
					&& leaveProcess.getHalfDay() == true) {
				numberOfDays = numberOfDays - 0.5;

			}
			lopDays = numberOfDays;
			if (totalLopAmount == 0.0) {
				lop = (payrollVO.getPerDaySalary() * numberOfDays);
			} else {
				lop = (totalLopAmount / totalLopdays) * numberOfDays;
				// lop = (payrollVO.getPerDaySalary() * numberOfDays);
			}

			PayrollTransaction payrollTransaction = new PayrollTransaction();
			payrollTransaction.setAmount(lop);
			payrollTransaction.setPayrollElement(payrollElementLOP);
			payrollTransaction.setStartDate(actualFromDate);
			payrollTransaction.setEndDate(actualToDate);
			payrollTransaction.setRecordId(leaveProcess.getLeaveProcessId());
			payrollTransaction.setUseCase("LeaveProcess");
			payrollTransaction.setStatus(Constants.HR.PayTransactionStatus.New
					.getCode());
			payrollTransaction
					.setNote("Leave Due settled / UnPaid deduction for("
							+ lopDays + ") days");

			payrollVO.getPayrollTransactions().add(payrollTransaction);

			lopDays = Double
					.parseDouble((payrollVO.getPayroll().getLopDays() != null ? payrollVO
							.getPayroll().getLopDays() : "0.0"))
					+ numberOfDays;
			payrollVO.getPayroll().setLopDays(lopDays + "");
			payrollVO.getPayroll().setNumberOfDays(
					payrollVO.getPayroll().getNumberOfDays()
							- (int) Math.ceil(numberOfDays));

		}

		// Paid From Leave Settlement
		PayrollElement payrollElement = payrollElementBL
				.getPayrollElementService().getPayrollElementByCode(
						getImplementation(), "OTHERDED");
		List<PayrollTransaction> payrollTransactions = payrollService
				.getPayTransactionByPayPeriodId(payrollVO.getJobAssignment(),
						payrollVO.getPayPeriodTransaction()
								.getPayPeriodTransactionId());
		if (payrollTransactions != null && payrollTransactions.size() > 0) {
			PayrollTransaction temp = null;
			for (PayrollTransaction payrollTransaction : payrollTransactions) {
				temp = new PayrollTransaction();
				try {
					BeanUtils.copyProperties(temp, payrollTransaction);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (payrollTransaction.getNumberOfDays() != null) {
					Double lopDays = 0.0;
					if (payrollVO.getPayroll().getLopDays() != null)
						lopDays = Double.valueOf(payrollVO.getPayroll()
								.getLopDays())
								+ (payrollTransaction.getNumberOfDays() != null ? payrollTransaction
										.getNumberOfDays() : 0.0);

					payrollVO.getPayroll().setLopDays(lopDays + "");
					payrollVO
							.getPayroll()
							.setNumberOfDays(
									payrollVO.getPayroll().getNumberOfDays()
											- (int) Math.ceil((payrollTransaction
													.getNumberOfDays() != null ? payrollTransaction
													.getNumberOfDays() : 0.0)));
				}
				temp.setPayrollTransactionId(null);
				temp.setPayrollElement(payrollElement);
				payrollVO.getPayrollTransactions().add(temp);
			}
		}
		return payrollVO;
	}

	boolean isBetweenInclusive(LocalDate start, LocalDate end, LocalDate target) {
		return !target.isBefore(start) && !target.isAfter(end);
	}

	public Date findActualStartDate(LeaveProcess leaveProcess, Payroll payroll) {
		LocalDate fromLeave = new LocalDate(leaveProcess.getFromDate());
		LocalDate toLeave = new LocalDate(leaveProcess.getToDate());
		LocalDate fromPayPeriod = new LocalDate(payroll.getStartDate());
		LocalDate startDt = null;
		boolean flag = (fromLeave.isAfter(fromPayPeriod) || fromLeave
				.equals(fromPayPeriod));
		boolean flag1 = (toLeave.isBefore(fromPayPeriod) || toLeave
				.equals(fromPayPeriod));
		if (flag && flag1) {
			startDt = fromPayPeriod;
		} else if (!flag && !flag1) {
			startDt = fromPayPeriod;
		} else {
			startDt = fromLeave;
		}

		return startDt.toDate();

	}

	public Date findActualEndDate(LeaveProcess leaveProcess, Payroll payroll) {
		LocalDate fromLeave = new LocalDate(leaveProcess.getFromDate());
		LocalDate toLeave = new LocalDate(leaveProcess.getToDate());
		LocalDate toPayPeriod = new LocalDate(payroll.getEndDate());
		LocalDate endDt = null;

		if ((toLeave.isAfter(toPayPeriod) || toLeave.equals(toPayPeriod))
				&& (fromLeave.isBefore(toPayPeriod) || fromLeave
						.equals(toPayPeriod))) {
			endDt = toPayPeriod;
		} else {
			endDt = toLeave;
		}

		return endDt.toDate();
	}

	public PayrollVO getLossOfPayFromAttendance(PayrollVO payrollVO) {
		// Attendance LOP process or overtime process
		List<Attendance> attendances = attendanceBL.getAttendanceService()
				.getAllAttendanceDeviationForSalary(
						payrollVO.getJobAssignment().getPerson(),
						payrollVO.getPayroll().getStartDate(),
						payrollVO.getPayroll().getEndDate());

		PayrollElement payrollElementLOP = payrollElementBL
				.getPayrollElementService().getPayrollElementByCode(
						getImplementation(), "LOP");
		PayrollElement payrollElementOT = payrollElementBL
				.getPayrollElementService().getPayrollElementByCode(
						getImplementation(), "OT");
		if (payrollVO.getPayroll().getLopDays() == null
				|| payrollVO.getPayroll().getLopDays().equals(""))
			payrollVO.getPayroll().setLopDays("0.0");

		for (Attendance attendance : attendances) {
			Double amount = 0.0;
			Double numberOfDay = 1.0;
			// Per Hour Salary calculation
			Double perDayHours = 0.0;
			Double perHourRate = 0.0;
			if (attendance.getIsAbsent()) {
				amount = (payrollVO.getPerDaySalary() * numberOfDay);
				payrollVO.getPayroll().setNumberOfDays(
						payrollVO.getPayroll().getNumberOfDays()
								- (int) Math.ceil(numberOfDay));
				payrollVO.getPayroll()
						.setLopDays(
								(Double.valueOf(payrollVO.getPayroll()
										.getLopDays()) + numberOfDay) + "");

				PayrollTransaction payrollTransaction = new PayrollTransaction();
				payrollTransaction.setAmount(amount);
				payrollTransaction.setPayrollElement(payrollElementLOP);
				payrollTransaction.setRecordId(attendance.getAttendanceId());
				payrollTransaction.setUseCase("Attendance");
				payrollTransaction.setStartDate(attendance.getAttendanceDate());
				payrollTransaction.setEndDate(attendance.getAttendanceDate());
				payrollTransaction.setNumberOfDays(numberOfDay);
				payrollTransaction
						.setStatus(Constants.HR.PayTransactionStatus.New
								.getCode());
				payrollVO.getPayrollTransactions().add(payrollTransaction);
			} else {
				perDayHours = (attendance.getShiftHours() != null ? attendance
						.getShiftHours() : Double.valueOf(attendance
						.getTotalHours()));

				if (perDayHours > 0)
					perHourRate = payrollVO.getPerDaySalary() / perDayHours;

				if (attendance.getDeviationHours() == null) {
					if (attendance.getHalfDay() != null
							&& attendance.getHalfDay() == true) {
						numberOfDay = 0.5;
					}
					amount = (payrollVO.getPerDaySalary() * numberOfDay);
				} else {
					amount = (attendance.getDeviationHours()) * perHourRate;
				}
				// Find Attendance Calculation part
				amount = (amount * attendance.getCalcultionMethod()) / 100;

				if (attendance.getDeductionType() != null
						&& attendance.getDeductionType() == 2) {// Lop

					PayrollTransaction payrollTransaction = new PayrollTransaction();
					payrollTransaction.setAmount(amount);
					payrollTransaction.setPayrollElement(payrollElementLOP);
					payrollTransaction
							.setRecordId(attendance.getAttendanceId());
					payrollTransaction.setUseCase("Attendance");
					payrollTransaction
							.setStatus(Constants.HR.PayTransactionStatus.New
									.getCode());
					payrollVO.getPayrollTransactions().add(payrollTransaction);

				} else if (attendance.getDeductionType() != null
						&& attendance.getDeductionType() == 3) {// OverTime

					PayrollTransaction payrollTransaction = new PayrollTransaction();
					payrollTransaction.setAmount(amount);
					payrollTransaction.setPayrollElement(payrollElementOT);
					payrollTransaction
							.setRecordId(attendance.getAttendanceId());
					payrollTransaction.setUseCase("Attendance");
					payrollTransaction
							.setStatus(Constants.HR.PayTransactionStatus.New
									.getCode());
					payrollVO.getPayrollTransactions().add(payrollTransaction);
				}
			}

		}
		return payrollVO;
	}

	public PayrollVO getLossOfPayFromNewJoinies(PayrollVO payrollVO) {

		// Loss of Pay checking with leave process information

		PayrollElement payrollElement = payrollElementBL
				.getPayrollElementService().getPayrollElementByCode(
						getImplementation(), "LOP");
		LocalDate date = new LocalDate(payrollVO.getCutOffDate());
		LocalDate end = new LocalDate(payrollVO.getPayPeriodTransaction()
				.getEndDate());

		/*
		 * if (payrollVO.getCutOffDate() != null && payrollElement != null &&
		 * payrollVO != null && date.isBefore(end)) { Double lopDays = 0.0;
		 * 
		 * while (end.isAfter(date)) { lopDays = lopDays + 1; date =
		 * date.plusDays(1); } Double lop = (payrollVO.getPerDaySalary() *
		 * lopDays);
		 * 
		 * PayrollTransaction payrollTransaction = new PayrollTransaction();
		 * payrollTransaction.setAmount(lop);
		 * payrollTransaction.setPayrollElement(payrollElement);
		 * payrollTransaction.setStatus(Constants.HR.PayTransactionStatus.New
		 * .getCode());
		 * payrollVO.getPayrollTransactions().add(payrollTransaction);
		 * 
		 * lopDays = Double.parseDouble(payrollVO.getPayroll().getLopDays()) +
		 * lopDays; payrollVO.getPayroll().setLopDays(lopDays + "");
		 * payrollVO.getPayroll().setNumberOfDays(
		 * payrollVO.getPayroll().getNumberOfDays() - (int) Math.ceil(lopDays));
		 * 
		 * }
		 */
		date = new LocalDate(payrollVO.getPayroll().getStartDate());
		end = new LocalDate(payrollVO.getPayPeriodTransaction().getStartDate());
		if (payrollElement != null && payrollVO != null && date.isAfter(end)) {
			Double days = 0.0;
			Double lopDays = 0.0;
			while (end.isBefore(date)) {
				lopDays = lopDays + 1;
				end = end.plusDays(1);
			}
			Double lop = (payrollVO.getPerDaySalary() * lopDays);

			PayrollTransaction payrollTransaction = new PayrollTransaction();
			payrollTransaction.setAmount(lop);
			payrollTransaction.setPayrollElement(payrollElement);
			payrollTransaction.setStatus(Constants.HR.PayTransactionStatus.New
					.getCode());
			payrollVO.getPayrollTransactions().add(payrollTransaction);

			days = Double.parseDouble(payrollVO.getPayroll().getLopDays())
					+ lopDays;
			payrollVO.getPayroll().setLopDays(days + "");
			payrollVO.getPayroll().setNumberOfDays(
					payrollVO.getPayroll().getNumberOfDays()
							- (int) Math.ceil(lopDays));

		}
		return payrollVO;
	}

	public PayrollVO getLoanAndAdvanceDeduction(PayrollVO payrollVO) {
		List<PayrollTransaction> payrollTransactions = employeeLoanBL
				.getRepaymentList(payrollVO);
		if (payrollTransactions != null && payrollTransactions.size() > 0) {
			payrollVO.getPayrollTransactions().addAll(payrollTransactions);
		}

		return payrollVO;
	}

	public List<PayrollTransaction> getAllowanceList(PayrollVO payrollVO)
			throws Exception {
		List<PayrollTransaction> payrollTransactions = null;
		PayrollTransaction payrollTransaction = null;
		Double amout = 0.0;
		Long recordId = null;
		Boolean financeImpact = true;
		try {
			List<Object> objects = payrollService.getAllowanceList(payrollVO);
			if (objects != null && objects.size() > 0) {
				payrollTransactions = new ArrayList<PayrollTransaction>();
				for (Object object : objects) {
					amout = 0.0;
					payrollTransaction = new PayrollTransaction();
					Method method = object.getClass().getMethod("getAmount");
					amout = (Double) method.invoke(object, null);
					method = object.getClass().getMethod(
							"get" + payrollVO.getUseCase() + "Id");
					recordId = (Long) method.invoke(object, null);

					method = object.getClass().getMethod("getIsFinanceImpact");
					financeImpact = (Boolean) method.invoke(object, null);

					payrollTransaction.setAmount(amout);
					if (financeImpact)
						payrollTransaction
								.setStatus(Constants.HR.PayTransactionStatus.New
										.getCode());
					else
						payrollTransaction
								.setStatus(Constants.HR.PayTransactionStatus.LedgerPosted
										.getCode());

					payrollTransaction.setRecordId(recordId);
					payrollTransaction.setUseCase(payrollVO.getUseCase());
					payrollTransactions.add(payrollTransaction);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return payrollTransactions;
	}

	public void getPayrollVerification(Long payrollId) {

		try {
			Payroll pay = null;
			List<JobPayrollElement> jobPayrollElements = null;
			List<JobPayrollElementVO> jobPayrollElementVos = null;
			Map<Long, JobPayrollElementVO> jobPayrollElementVoMaps = null;
			if (payrollId != null && payrollId > 0) {
				pay = new Payroll();
				pay = this.getPayrollService().getPayrollDetail(payrollId);
				JobAssignment jobAssignment = this
						.getJobAssignmentBL()
						.getJobAssignmentService()
						.getJobAssignmentInfo(
								pay.getJobAssignment().getJobAssignmentId());
				jobPayrollElements = this.getJobBL().getJobService()
						.getAllJobPayrollElement(jobAssignment.getJob());

				jobPayrollElementVos = new ArrayList<JobPayrollElementVO>();
				jobPayrollElementVoMaps = new HashMap<Long, JobPayrollElementVO>();
				JobPayrollElementVO jobPayrollElementVO = null;
				// Get all the actual pay transaction information
				for (PayrollTransaction payrollTransaction : pay
						.getPayrollTransactions()) {

					if (payrollTransaction.getPayrollElement() == null
							|| payrollTransaction.getPayrollElement()
									.getPayrollElementId() == null)
						continue;

					jobPayrollElementVO = new JobPayrollElementVO();
					jobPayrollElementVO.setPayrollElementId(payrollTransaction
							.getPayrollElement().getPayrollElementId());
					if (jobPayrollElementVoMaps.containsKey(payrollTransaction
							.getPayrollElement().getPayrollElementId())) {
						jobPayrollElementVO = jobPayrollElementVoMaps
								.get(payrollTransaction.getPayrollElement()
										.getPayrollElementId());
						jobPayrollElementVO
								.setConsolidatedAmount(payrollTransaction
										.getAmount()
										+ jobPayrollElementVO
												.getConsolidatedAmount());
						jobPayrollElementVoMaps.put(payrollTransaction
								.getPayrollElement().getPayrollElementId(),
								jobPayrollElementVO);
					} else {
						jobPayrollElementVO
								.setConsolidatedAmount(0.0 + payrollTransaction
										.getAmount());
						jobPayrollElementVoMaps.put(payrollTransaction
								.getPayrollElement().getPayrollElementId(),
								jobPayrollElementVO);
					}
				}

				for (JobPayrollElement jobPayrollElement : new ArrayList<JobPayrollElement>(
						jobPayrollElements)) {
					jobPayrollElementVO = new JobPayrollElementVO();
					jobPayrollElementVO = this
							.convertEntityIntoVO(jobPayrollElement);

					if (jobPayrollElementVoMaps.containsKey(jobPayrollElementVO
							.getPayrollElementByPayrollElementId()
							.getPayrollElementId())) {
						JobPayrollElementVO jobpayelevo = jobPayrollElementVoMaps
								.get(jobPayrollElementVO
										.getPayrollElementByPayrollElementId()
										.getPayrollElementId());
						jobPayrollElementVO.setConsolidatedAmount(jobpayelevo
								.getConsolidatedAmount());
					}
					if (pay.getIsSalary()) {
						if ((byte) jobPayrollElement.getPayType() == (byte) Constants.HR.PayMode.Salary
								.getCode())
							jobPayrollElementVos.add(jobPayrollElementVO);
						else
							jobPayrollElements.remove(jobPayrollElement);

					} else {
						jobPayrollElementVos.add(jobPayrollElementVO);
					}

				}

			}
			List<PayrollElement> payrollElements = this.getPayrollService()
					.getAllPayrollElement(getImplementation());

			ServletActionContext.getRequest().setAttribute(
					"JOB_PAYROLL_ELEMENTS", jobPayrollElementVos);
			ServletActionContext.getRequest().setAttribute("PAYROLL_ELEMENTS",
					payrollElements);
			ServletActionContext.getRequest().setAttribute("PAYROLL_PROCESS",
					pay);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deletePyarollBulk(String payrols) throws Exception {
		String[] leaveIdArr = payrols.split(",");
		for (String string : leaveIdArr) {
			deletePayroll(Long.valueOf(string));
		}
	}

	public void deleteAllPyarollByMonth(String payMonth) throws Exception {
		List<Payroll> deletePayrolls = this.getPayrollService()
				.getAllPayrollBasedonPayMonth(getImplementation(), payMonth);
		if (deletePayrolls != null) {
			for (Payroll payroll : deletePayrolls) {
				payrollService.deletePayroll(
						payroll,
						new ArrayList<PayrollTransaction>(payroll
								.getPayrollTransactions()));
			}

		}
	}

	public void deletePayroll(Long payrollId) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Payroll.class.getSimpleName(), payrollId, user,
				workflowDetailVo);
	}

	public void doPayrollBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		Payroll payroll = payrollService.getPayrollInfoPlain(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			payrollService.deletePayroll(
					payroll,
					new ArrayList<PayrollTransaction>(payroll
							.getPayrollTransactions()));
		} else {
		}
	}

	public Map<Byte, String> payrollStaus() {
		Map<Byte, String> types = new HashMap<Byte, String>();
		types.put(Constants.HR.PayStatus.InProcess.getCode(),
				Constants.HR.PayStatus.InProcess.name());
		types.put(Constants.HR.PayStatus.Deposited.getCode(),
				Constants.HR.PayStatus.Deposited.name());
		types.put(Constants.HR.PayStatus.Cancelled.getCode(),
				Constants.HR.PayStatus.Cancelled.name());
		types.put(Constants.HR.PayStatus.Holded.getCode(),
				Constants.HR.PayStatus.Holded.name());
		types.put(Constants.HR.PayStatus.Pending.getCode(),
				Constants.HR.PayStatus.Pending.name());
		types.put(Constants.HR.PayStatus.InProcess.getCode(),
				Constants.HR.PayStatus.InProcess.name());
		return types;
	}

	public Map<Byte, String> payTransactionStaus() {
		Map<Byte, String> types = new HashMap<Byte, String>();
		types.put(Constants.HR.PayTransactionStatus.New.getCode(),
				Constants.HR.PayTransactionStatus.New.name());
		types.put(Constants.HR.PayTransactionStatus.LedgerPosted.getCode(),
				Constants.HR.PayTransactionStatus.LedgerPosted.name());
		types.put(Constants.HR.PayTransactionStatus.Suspended.getCode(),
				Constants.HR.PayTransactionStatus.Suspended.name());
		return types;
	}

	public PayrollVO periodBasedTransaction(PayrollVO vo) throws Exception {
		Payroll payroll = null;
		PayrollVO payrollVO = null;
		if (vo.getJobAssignmentId() != null && vo.getJobAssignmentId() > 0) {

			payroll = this.getPayrollService().getPayrollDetailByUseCase(
					vo.getJobAssignmentId(), "PayPeriod" + vo.getPayPeriodId());
			if (payroll != null)
				payrollVO = this.payrollToVOConverter(payroll);

		}
		return payrollVO;
	}

	public void deletePayrollRecursively(Person person, String payMonth) {
		Payroll deletePayroll = payrollService.getPayrollBasedonPersonAndMonth(
				person, payMonth);
		if (deletePayroll != null) {
			payrollService.deletePayroll(
					deletePayroll,
					new ArrayList<PayrollTransaction>(deletePayroll
							.getPayrollTransactions()));
			deletePayrollRecursively(person, payMonth);
		}
	}

	// Getter setter
	public PayrollService getPayrollService() {
		return payrollService;
	}

	public void setPayrollService(PayrollService payrollService) {
		this.payrollService = payrollService;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public AttendanceBL getAttendanceBL() {
		return attendanceBL;
	}

	public void setAttendanceBL(AttendanceBL attendanceBL) {
		this.attendanceBL = attendanceBL;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public LeaveProcessBL getLeaveProcessBL() {
		return leaveProcessBL;
	}

	public void setLeaveProcessBL(LeaveProcessBL leaveProcessBL) {
		this.leaveProcessBL = leaveProcessBL;
	}

	public JobBL getJobBL() {
		return jobBL;
	}

	public void setJobBL(JobBL jobBL) {
		this.jobBL = jobBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public CompanyBL getCompanyBL() {
		return companyBL;
	}

	public void setCompanyBL(CompanyBL companyBL) {
		this.companyBL = companyBL;
	}

	public PersonBL getPersonBL() {
		return personBL;
	}

	public void setPersonBL(PersonBL personBL) {
		this.personBL = personBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public GLChartOfAccountService getAccountService() {
		return accountService;
	}

	public void setAccountService(GLChartOfAccountService accountService) {
		this.accountService = accountService;
	}

	public GLCombinationService getCombinationService() {
		return combinationService;
	}

	public void setCombinationService(GLCombinationService combinationService) {
		this.combinationService = combinationService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public WorkflowBL getWorkflowBL() {
		return workflowBL;
	}

	public void setWorkflowBL(WorkflowBL workflowBL) {
		this.workflowBL = workflowBL;
	}

	public JobAssignmentBL getJobAssignmentBL() {
		return jobAssignmentBL;
	}

	public void setJobAssignmentBL(JobAssignmentBL jobAssignmentBL) {
		this.jobAssignmentBL = jobAssignmentBL;
	}

	public PayPeriodBL getPayPeriodBL() {
		return payPeriodBL;
	}

	public void setPayPeriodBL(PayPeriodBL payPeriodBL) {
		this.payPeriodBL = payPeriodBL;
	}

	public EmployeeLoanBL getEmployeeLoanBL() {
		return employeeLoanBL;
	}

	public void setEmployeeLoanBL(EmployeeLoanBL employeeLoanBL) {
		this.employeeLoanBL = employeeLoanBL;
	}

	public DirectPaymentBL getDirectPaymentBL() {
		return directPaymentBL;
	}

	public void setDirectPaymentBL(DirectPaymentBL directPaymentBL) {
		this.directPaymentBL = directPaymentBL;
	}

	public PayrollElementBL getPayrollElementBL() {
		return payrollElementBL;
	}

	public void setPayrollElementBL(PayrollElementBL payrollElementBL) {
		this.payrollElementBL = payrollElementBL;
	}

}
