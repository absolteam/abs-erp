package com.aiotech.aios.hr.service;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.Location;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class LocationService {

	private AIOTechGenericDAO<Location> locationDAO;
	
	public List<Location> getAllLocation(Implementation implementation) throws Exception{
		return locationDAO.findByNamedQuery("getAllLocations", implementation);
	}

	public List<Location> getAllLocationBasedOnCompany(Long companyId) throws Exception{
		return locationDAO.findByNamedQuery("getAllLocationBasedOnCompany", companyId);
	}
	
	public List<Location> getAllLocationList(Implementation implementation) throws Exception{
		return locationDAO.findByNamedQuery("getAllLocationList", implementation);
	}
	
	public Location getLocationDetail(Long locationId) throws Exception{
		List<Location> locations= locationDAO.findByNamedQuery("getLocationDetail", locationId);
		if(locations!=null && locations.size()>0){
			return locations.get(0);
		}else{
			return null;
		}
	}
	
	public Location getLocationById(Long locationId) throws Exception{
		return locationDAO.findById(locationId);
	}
	
	public void saveLocation(Location location)throws Exception{
		locationDAO.saveOrUpdate(location);
	}
	public void deleteLocation(Location location)throws Exception{
		locationDAO.delete(location);
	}
	public AIOTechGenericDAO<Location> getLocationDAO() {
		return locationDAO;
	}

	public void setLocationDAO(AIOTechGenericDAO<Location> locationDAO) {
		this.locationDAO = locationDAO;
	}
}
