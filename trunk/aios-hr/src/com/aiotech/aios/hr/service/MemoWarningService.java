package com.aiotech.aios.hr.service;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.MemoWarning;
import com.aiotech.aios.hr.domain.entity.MemoWarningPerson;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class MemoWarningService {
	public AIOTechGenericDAO<MemoWarning> memoWarningDAO;
	public AIOTechGenericDAO<MemoWarningPerson> memoWarningPersonDAO;
	
	public List<MemoWarning> getMemoWarningList(Implementation implementation) {
		return memoWarningDAO.findByNamedQuery("getMemoWarningList", implementation);
	}
	
	public MemoWarning getMemoWarningInfo(Long memoWarningId) {
		List<MemoWarning> memoWarnings=memoWarningDAO.findByNamedQuery("getMemoWarningInfo", memoWarningId);
		if(memoWarnings!=null && memoWarnings.size()>0)
			return memoWarnings.get(0);
		else
			return null;
	}
	
	public void saveMemoWarning(MemoWarning memoWarning){
		memoWarningDAO.saveOrUpdate(memoWarning);
	}
	
	public void saveMemoWarningPersons(List<MemoWarningPerson> memoWarningPersons){
		memoWarningPersonDAO.saveOrUpdateAll(memoWarningPersons);
	}
	
	public void deleteMemoWarningPersons(List<MemoWarningPerson> memoWarningPersons){
		memoWarningPersonDAO.deleteAll(memoWarningPersons);
	}
	
	public void deleteMemoWarning(MemoWarning memoWarning){
		memoWarningDAO.delete(memoWarning);
	}
	
	
	
	
	public AIOTechGenericDAO<MemoWarning> getMemoWarningDAO() {
		return memoWarningDAO;
	}
	public void setMemoWarningDAO(AIOTechGenericDAO<MemoWarning> memoWarningDAO) {
		this.memoWarningDAO = memoWarningDAO;
	}
	public AIOTechGenericDAO<MemoWarningPerson> getMemoWarningPersonDAO() {
		return memoWarningPersonDAO;
	}
	public void setMemoWarningPersonDAO(
			AIOTechGenericDAO<MemoWarningPerson> memoWarningPersonDAO) {
		this.memoWarningPersonDAO = memoWarningPersonDAO;
	}
}
