package com.aiotech.aios.hr.service;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.hr.domain.entity.Designation;
import com.aiotech.aios.hr.domain.entity.Grade;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class DesignationService {

	private AIOTechGenericDAO<Designation> designationDAO;
	private AIOTechGenericDAO<Grade> gradeDAO;

	public List<Designation> getAllDesignation(Implementation implementation) throws Exception{
		return designationDAO.findByNamedQuery("getAllDesignation", implementation,
				(byte)WorkflowConstants.Status.Published.getCode());
	}
	
	public List<Designation> getAllCompanyDesignation(Long companyId) throws Exception{
		return designationDAO.findByNamedQuery("getAllCompanyDesignation", companyId
				,
				(byte)WorkflowConstants.Status.Published.getCode());
	}
	
	public List<Grade> getAllGrade(Implementation implementation) throws Exception{
		return gradeDAO.findByNamedQuery("getAllGrade", implementation);
	}
	
	
	public Designation getDesignationInfo(Long designationId) throws Exception{
		List<Designation> designations=new ArrayList<Designation>();
		designations=designationDAO.findByNamedQuery("getDesignationInfo", designationId);
		if(designations!=null && designations.size()>0)
			return designations.get(0);
		else
			return null;
	}
	
	public void saveDesignation(Designation designation){
		designationDAO.saveOrUpdate(designation);
	}
	
	public void deleteDesignation(Designation designation){
		designationDAO.delete(designation);
	}
	
	
	
	public AIOTechGenericDAO<Designation> getDesignationDAO() {
		return designationDAO;
	}

	public void setDesignationDAO(AIOTechGenericDAO<Designation> designationDAO) {
		this.designationDAO = designationDAO;
	}

	public AIOTechGenericDAO<Grade> getGradeDAO() {
		return gradeDAO;
	}

	public void setGradeDAO(AIOTechGenericDAO<Grade> gradeDAO) {
		this.gradeDAO = gradeDAO;
	}
}
