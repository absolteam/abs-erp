package com.aiotech.aios.hr.service;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.Candidate;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class CandidateService {
	public AIOTechGenericDAO<Candidate> candidateDAO;
	
	public List<Candidate> getAllCandidate(Implementation implementation){
		return candidateDAO.findByNamedQuery("getAllCandidate",
				implementation,
				(byte)WorkflowConstants.Status.Published.getCode());
	}
	
	public List<Candidate> getCandidateByStatus(Implementation implementation,Byte status){
		return candidateDAO.findByNamedQuery("getCandidateByStatus",
				implementation,status);
	}
	
	public Candidate getCandiateById(Long candidateId) {
		List<Candidate> candidates = candidateDAO.findByNamedQuery(
				"getCandiateById", candidateId);
		if (candidates != null && candidates.size() > 0)
			return candidates.get(0);
		else
			return null;
	}
	
	public void saveCandidate(Candidate candidate){
		candidateDAO.saveOrUpdate(candidate);
	}
	
	public void deleteCandidate(Candidate candidate){
		candidateDAO.delete(candidate);
	}

	public AIOTechGenericDAO<Candidate> getCandidateDAO() {
		return candidateDAO;
	}

	public void setCandidateDAO(AIOTechGenericDAO<Candidate> candidateDAO) {
		this.candidateDAO = candidateDAO;
	}
	
}
