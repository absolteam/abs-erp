package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.CompanyTypeAllocation;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.domain.entity.Job;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobLeave;
import com.aiotech.aios.hr.domain.entity.JobPayrollElement;
import com.aiotech.aios.hr.domain.entity.JobShift;
import com.aiotech.aios.hr.domain.entity.Leave;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.PersonTypeAllocation;
import com.aiotech.aios.hr.domain.entity.vo.JobVO;
import com.aiotech.aios.hr.service.CompanyService;
import com.aiotech.aios.hr.service.JobService;
import com.aiotech.aios.hr.service.PersonService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.domain.entity.LookupMaster;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class JobBL {

	private JobService jobService;
	private PersonService personService;
	private CompanyService companyService;
	private SystemService systemService;
	private WorkingShiftBL workingShiftBL;
	private Implementation implementation;
	private LookupMasterBL lookupMasterBL;
	private LeaveBL leaveBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public void jobSave(Job job, List<JobPayrollElement> jobPayrollElements,
			List<JobLeave> jobLeavs, List<JobShift> jobShifts,
			List<JobAssignment> jobAssignments) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		job.setIsApprove((byte) WorkflowConstants.Status.Published.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (job != null && job.getJobId() != null && job.getJobId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		if (job.getJobId() != null && job.getJobId() != 0) {
			// Job tempJob=jobService.getJobInfo(job.getJobId());
			List<JobPayrollElement> tempjobPayrollElements = new ArrayList<JobPayrollElement>(
					job.getJobPayrollElements());
			List<JobLeave> tempjobLeavs = new ArrayList<JobLeave>(
					job.getJobLeaves());
			List<JobShift> tempjobShifts = new ArrayList<JobShift>(
					job.getJobShifts());
			// List<JobAssignment> tempjobAssignments=new
			// ArrayList<JobAssignment>(job.getJobAssignments());

			List<JobPayrollElement> updateJobPayrollElements = getJobPayrollElementSimiler(
					jobPayrollElements, tempjobPayrollElements);
			if (updateJobPayrollElements != null
					&& updateJobPayrollElements.size() > 0)
				jobService.saveJobAllowanceAll(updateJobPayrollElements);

			List<JobLeave> updateJobLeaves = getJobLeaveSimiler(jobLeavs,
					tempjobLeavs);
			if (updateJobLeaves != null && updateJobLeaves.size() > 0)
				jobService.saveJobLeaveAll(updateJobLeaves);

			List<JobShift> updateJobShifts = getJobShiftSimiler(jobShifts,
					tempjobShifts);
			if (updateJobShifts != null && updateJobShifts.size() > 0)
				jobService.saveJobShiftAll(updateJobShifts);

			/*
			 * List<JobAssignment>
			 * updateJobAssignments=getJobAssignmentSimiler(jobAssignments
			 * ,tempjobAssignments); if(updateJobAssignments!=null &&
			 * updateJobAssignments.size()>0)
			 * jobService.saveJobAssignmentAll(updateJobAssignments);
			 */

		}

		jobService.saveJob(job);

		if (jobPayrollElements != null && jobPayrollElements.size() > 0) {
			for (JobPayrollElement jobPayrollElement : jobPayrollElements) {
				jobPayrollElement.setJob(job);
			}
			jobService.saveJobAllowanceAll(jobPayrollElements);

		}
		if (jobLeavs != null && jobLeavs.size() > 0) {
			for (JobLeave jobLeav : jobLeavs) {
				jobLeav.setJob(job);
			}
			jobService.saveJobLeaveAll(jobLeavs);

		}

		if (jobShifts != null && jobShifts.size() > 0) {
			for (JobShift jobShift : jobShifts) {
				jobShift.setJob(job);
			}
			jobService.saveJobShiftAll(jobShifts);

		}

		/*
		 * if(jobAssignments!=null && jobAssignments.size()>0){ for
		 * (JobAssignment jobAssignment : jobAssignments) {
		 * jobAssignment.setJob(job); }
		 * jobService.saveJobAssignmentAll(jobAssignments);
		 * 
		 * }
		 */

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Job.class.getSimpleName(), job.getJobId(), user,
				workflowDetailVo);
	}

	public Job jobRemoveInactiveRec(Job job) {
		List<JobAssignment> tempjobAssignments = new ArrayList<JobAssignment>(
				job.getJobAssignments());
		List<JobLeave> tempjobLeavs = new ArrayList<JobLeave>(
				job.getJobLeaves());
		List<JobShift> tempjobShifts = new ArrayList<JobShift>(
				job.getJobShifts());
		List<JobPayrollElement> tempjobPayrollElements = new ArrayList<JobPayrollElement>(
				job.getJobPayrollElements());

		for (JobAssignment jobAssignment : tempjobAssignments) {
			if (jobAssignment.getIsActive() == false
					|| jobAssignment.getPerson().getStatus() != 1)
				job.getJobAssignments().remove(jobAssignment);
		}
		for (JobLeave jobLeave : tempjobLeavs) {
			if (jobLeave.getIsActive() == false)
				job.getJobLeaves().remove(jobLeave);
		}
		for (JobShift jobShift : tempjobShifts) {
			if (jobShift.getIsActive() == false)
				job.getJobShifts().remove(jobShift);
		}
		for (JobPayrollElement jobPayrollElement : tempjobPayrollElements) {
			if (jobPayrollElement.getIsActive() == false)
				job.getJobPayrollElements().remove(jobPayrollElement);
		}

		return job;
	}

	public Job jobRemoveInactiveRecForShift(Job job) {
		List<JobAssignment> tempjobAssignments = new ArrayList<JobAssignment>(
				job.getJobAssignments());
		List<JobShift> tempjobShifts = new ArrayList<JobShift>(
				job.getJobShifts());

		for (JobAssignment jobAssignment : tempjobAssignments) {
			if (jobAssignment.getIsActive() == false
					|| jobAssignment.getPerson().getStatus() != 1)
				job.getJobAssignments().remove(jobAssignment);
		}

		for (JobShift jobShift : tempjobShifts) {
			if (jobShift.getIsActive() == false)
				job.getJobShifts().remove(jobShift);
		}

		return job;
	}

	public List<JobPayrollElement> getJobPayrollElementSimiler(
			List<JobPayrollElement> jobPayrollElements,
			List<JobPayrollElement> jobPayrollElementEdits) {
		List<JobPayrollElement> jobPayrollElementUpdates = new ArrayList<JobPayrollElement>();

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		for (JobPayrollElement jobPayrollElement1 : jobPayrollElements) {
			if (jobPayrollElement1.getJobPayrollElementId() != null
					&& jobPayrollElement1.getJobPayrollElementId() > 0)
				listOne.add(jobPayrollElement1.getJobPayrollElementId());
		}
		for (JobPayrollElement jobPayrollElement2 : jobPayrollElementEdits) {
			if (jobPayrollElement2.getJobPayrollElementId() != null
					&& jobPayrollElement2.getJobPayrollElementId() > 0)
				listTwo.add(jobPayrollElement2.getJobPayrollElementId());
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);

		// Delete Process
		for (Long lng : different) {
			for (JobPayrollElement jobPayrollElement3 : jobPayrollElementEdits) {
				if (lng != null
						&& jobPayrollElement3.getJobPayrollElementId().equals(
								lng)) {
					jobPayrollElement3.setIsActive(false);
					jobPayrollElementUpdates.add(jobPayrollElement3);
				}
			}
		}
		return jobPayrollElementUpdates;
	}

	public List<JobLeave> getJobLeaveSimiler(List<JobLeave> jobLeaves,
			List<JobLeave> jobLeaveEdits) {
		List<JobLeave> jobLeaveUpdates = new ArrayList<JobLeave>();

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		for (JobLeave jobLeave : jobLeaves) {
			if (jobLeave.getJobLeaveId() != null
					&& jobLeave.getJobLeaveId() > 0)
				listOne.add(jobLeave.getJobLeaveId());
		}
		for (JobLeave jobLeave : jobLeaveEdits) {
			if (jobLeave.getJobLeaveId() != null
					&& jobLeave.getJobLeaveId() > 0)
				listTwo.add(jobLeave.getJobLeaveId());
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);

		// Delete Process
		for (Long lng : different) {
			for (JobLeave jobLeave : jobLeaveEdits) {
				if (lng != null && jobLeave.getJobLeaveId().equals(lng)) {
					jobLeave.setIsActive(false);
					jobLeaveUpdates.add(jobLeave);
				}
			}
		}
		return jobLeaveUpdates;
	}

	public List<JobShift> getJobShiftSimiler(List<JobShift> jobShifts,
			List<JobShift> jobShiftEdits) {
		List<JobShift> jobShiftUpdates = new ArrayList<JobShift>();

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		for (JobShift jobShift : jobShifts) {
			if (jobShift.getJobShiftId() != null
					&& jobShift.getJobShiftId() > 0)
				listOne.add(jobShift.getJobShiftId());
		}
		for (JobShift jobShift : jobShiftEdits) {
			if (jobShift.getJobShiftId() != null
					&& jobShift.getJobShiftId() > 0)
				listTwo.add(jobShift.getJobShiftId());
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);

		// Delete Process
		for (Long lng : different) {
			for (JobShift jobShift : jobShiftEdits) {
				if (lng != null && jobShift.getJobShiftId().equals(lng)) {
					jobShift.setIsActive(false);
					jobShiftUpdates.add(jobShift);
				}
			}
		}
		return jobShiftUpdates;
	}

	/*
	 * public List<JobAssignment> getJobAssignmentSimiler(List<JobAssignment>
	 * jobAssignments, List<JobAssignment> jobAssignmentEdits){
	 * List<JobAssignment> jobAssignmentUpdates=new ArrayList<JobAssignment>();
	 * 
	 * Collection<Long> listOne =new ArrayList<Long>(); Collection<Long> listTwo
	 * =new ArrayList<Long>(); for(JobAssignment jobAssignment:jobAssignments){
	 * if(jobAssignment.getJobAssignmentId()!=null &&
	 * jobAssignment.getJobAssignmentId()>0)
	 * listOne.add(jobAssignment.getJobAssignmentId()); } for(JobAssignment
	 * jobAssignment:jobAssignmentEdits){
	 * if(jobAssignment.getJobAssignmentId()!=null &&
	 * jobAssignment.getJobAssignmentId()>0)
	 * listTwo.add(jobAssignment.getJobAssignmentId()); } Collection<Long>
	 * similar = new HashSet<Long>( listOne ); Collection<Long> different = new
	 * HashSet<Long>(); different.addAll( listOne ); different.addAll( listTwo
	 * ); similar.retainAll( listTwo ); different.removeAll( similar );
	 * 
	 * //Delete Process for(Long lng:different){ for(JobAssignment
	 * jobAssignment:jobAssignmentEdits){ if(lng!=null &&
	 * jobAssignment.getJobAssignmentId().equals(lng)){
	 * jobAssignment.setIsActive(false);
	 * jobAssignmentUpdates.add(jobAssignment); } } } return
	 * jobAssignmentUpdates; }
	 */

	public boolean deleteJob(Job job) throws Exception {
		boolean returnFlag = true;
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Job.class.getSimpleName(), job.getJobId(), user,
				workflowDetailVo);
		return returnFlag;
	}

	public void doJobBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		Job job = jobService.getJobForDelete(recordId);
		List<JobPayrollElement> jobPayrollElements = new ArrayList<JobPayrollElement>(
				job.getJobPayrollElements());
		List<JobLeave> jobLeaves = new ArrayList<JobLeave>(job.getJobLeaves());
		List<JobShift> jobShifts = new ArrayList<JobShift>(job.getJobShifts());
		List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>(
				job.getJobAssignments());
		if (workflowDetailVO.isDeleteFlag()) {
			try {
				jobService.deleteJobAllowanceAll(jobPayrollElements);
				jobService.deleteJobLeaveAll(jobLeaves);
				jobService.deleteJobShiftAll(jobShifts);
				jobService.deleteJobAssignemntAll(jobAssignments);
				jobService.deleteJob(job);
			} catch (Exception e) {
				for (JobPayrollElement jobPayrollElement : jobPayrollElements) {
					jobPayrollElement.setIsActive(false);
				}
				jobService.saveJobAllowanceAll(jobPayrollElements);

				for (JobLeave jobLeave : jobLeaves) {
					jobLeave.setIsActive(false);
				}
				jobService.saveJobLeaveAll(jobLeaves);

				for (JobShift jobShift : jobShifts) {
					jobShift.setIsActive(false);
				}
				jobService.saveJobShiftAll(jobShifts);

				for (JobAssignment jobAssignment : jobAssignments) {
					jobAssignment.setIsActive(false);
				}
				jobService.saveJobAssignmentAll(jobAssignments);

				job.setStatus(Byte.parseByte("0"));
				jobService.saveJob(job);
			}
		} else {
		}
	}

	public List<JobVO> convertEntitiesTOVOs(List<Job> jobs) throws Exception {

		List<JobVO> tosList = new ArrayList<JobVO>();

		for (Job pers : jobs) {
			tosList.add(convertEntityToVO(pers));
		}

		return tosList;
	}

	public JobVO convertEntityToVO(Job job) {
		JobVO jobVO = new JobVO();
		try {
			// Copy EmployeeLoan class properties value into VO properties.
			BeanUtils.copyProperties(jobVO, job);
			jobVO.setJobId(job.getJobId());
			jobVO.setJobName(job.getJobName());
			jobVO.setJobNumber(job.getJobNumber());
			String pays = "";
			if (job.getJobPayrollElements() != null) {
				for (JobPayrollElement jobPayrollElement : job
						.getJobPayrollElements()) {
					if (jobPayrollElement.getIsActive())
						pays += jobPayrollElement
								.getPayrollElementByPayrollElementId()
								.getElementName()
								+ " ["
								+ jobPayrollElement.getAmount()
								+ "]"
								+ ", ";
				}
				jobVO.setPayList(pays);
			}
			String leaves = "";
			if (job.getJobLeaves() != null) {
				for (JobLeave jobLeave : job.getJobLeaves()) {
					if (jobLeave.getIsActive())
						leaves += jobLeave.getLeave().getDisplayName()
								+ " ["
								+ jobLeave.getLeave().getDefaultDays()
								+ "]" + ", ";
				}
				jobVO.setLeaveList(leaves);
			}
			String shifts = "";
			if (job.getJobShifts() != null) {
				for (JobShift jobShift : job.getJobShifts()) {
					if (jobShift.getIsActive())
						shifts += jobShift.getWorkingShift()
								.getWorkingShiftName()
								+ "("
								+ jobShift.getWorkingShift().getStartTime()
								+ "-"
								+ jobShift.getWorkingShift().getEndTime()
								+ "), ";
				}
				jobVO.setShiftList(shifts);
			}

			String assignments = "";
			if (job.getJobAssignments() != null) {
				for (JobAssignment jobAssignment : job.getJobAssignments()) {
					if (jobAssignment.getIsActive())
						assignments += jobAssignment.getPerson().getFirstName()
								+ " " + jobAssignment.getPerson().getLastName()
								+ " [" + jobAssignment.getJobAssignmentNumber()
								+ "]" + ", ";
				}
				jobVO.setAssignmentList(assignments);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jobVO;
	}

	public void hrBuildUpdateScript() {
		try {
			List<Implementation> implementations = new ArrayList<Implementation>();
			implementations = systemService.getAllImplementation();
			for (Implementation implem : implementations) {
				int number = 1000;
				List<Job> jobs = new ArrayList<Job>();
				jobs = jobService.getAllJobWithoutStatus(implem);
				for (Job job : jobs) {
					List<JobAssignment> jobAssignmnets = new ArrayList<JobAssignment>(
							job.getJobAssignments());
					for (JobAssignment jobAssignment : jobAssignmnets) {
						number += 1;
						jobAssignment.setJobAssignmentNumber(number + "");
					}
				}

				// Lookup master & child Update
				List<LookupMaster> activeLookupDetails = lookupMasterBL
						.getLookupMasterService().getLookupInformation(
								"COMPONENT_FEATURE", implem);

				// If there is lookup information exist already then skip the
				// insert process
				if (activeLookupDetails == null
						|| activeLookupDetails.size() == 0) {
					List<LookupMaster> lookupMaster = new ArrayList<LookupMaster>();
					Implementation implementation = new Implementation();
					implementation.setImplementationId(Long.valueOf("6"));
					lookupMaster = lookupMasterBL.getLookupMasterService()
							.getLookupListForImplementation(implementation);
					for (LookupMaster lookupMaster2 : lookupMaster) {
						LookupMaster updateMaster = new LookupMaster();
						updateMaster.setAccessCode(lookupMaster2
								.getAccessCode());
						updateMaster.setDisplayName(lookupMaster2
								.getDisplayName());
						updateMaster.setIsActive(lookupMaster2.getIsActive());
						updateMaster.setIsSystem(lookupMaster2.getIsSystem());
						updateMaster.setImplementation(implem);

						List<LookupDetail> lookupdetails = new ArrayList<LookupDetail>();

						for (LookupDetail lookDetail : lookupMaster2
								.getLookupDetails()) {
							LookupDetail templookupDetail = new LookupDetail();
							templookupDetail.setAccessCode(lookDetail
									.getAccessCode());
							templookupDetail.setDataId(lookDetail.getDataId());
							templookupDetail.setDisplayName(lookDetail
									.getDisplayName());
							templookupDetail.setDisplayNameArabic(lookDetail
									.getDisplayNameArabic());
							templookupDetail.setIsActive(lookDetail
									.getIsActive());
							templookupDetail.setIsSystem(lookDetail
									.getIsSystem());

							lookupdetails.add(templookupDetail);
						}

						lookupMasterBL.getLookupMasterService()
								.saveLookupInformation(updateMaster,
										lookupdetails);

					}
				}

				List<LookupMaster> activeLookupDetailsPerson = lookupMasterBL
						.getLookupMasterService().getLookupInformation(
								"PERSON_TYPE", implem);
				List<LookupMaster> activeLookupDetailsComp = lookupMasterBL
						.getLookupMasterService().getLookupInformation(
								"COMPANY_TYPE", implem);
				// Person Type Update into
				List<Person> persons = personService
						.getAllPersonWithoutStatus(implem);
				List<PersonTypeAllocation> personTypeAllocations = new ArrayList<PersonTypeAllocation>();
				for (Person person : persons) {
					if (person.getPersonType() != null) {
						PersonTypeAllocation personTypeAllocation = new PersonTypeAllocation();
						LookupDetail tempLookupDetail = getTypeLookup(
								activeLookupDetailsPerson, person
										.getPersonType().getPersonTypeId());
						if (tempLookupDetail != null) {
							personTypeAllocation.setPerson(person);
							personTypeAllocation
									.setLookupDetail(tempLookupDetail);
							personTypeAllocations.add(personTypeAllocation);
							person.setPersonType(null);
						}
					}
				}
				personService.savePersonTypes(personTypeAllocations);

				// Company Type Update into
				List<Company> companies = companyService.getAllCompany(implem);
				List<CompanyTypeAllocation> companyTypeAllocations = new ArrayList<CompanyTypeAllocation>();
				for (Company company : companies) {
					if (company.getCompanyType() != null) {
						CompanyTypeAllocation companyTypeAllocation = new CompanyTypeAllocation();
						LookupDetail tempLookupDetail = getTypeLookup(
								activeLookupDetailsComp,
								company.getCompanyType());
						if (tempLookupDetail != null) {
							companyTypeAllocation.setCompany(company);
							companyTypeAllocation
									.setLookupDetail(tempLookupDetail);
							companyTypeAllocations.add(companyTypeAllocation);
							company.setCompanyType(null);
						}
					}
				}
				companyService.saveCompanyTypes(companyTypeAllocations);

				// Identity Type Update by Lookup Table Information on Identity
				// Table
				List<LookupDetail> activeLookupDetailsIdentityType = lookupMasterBL
						.getActiveLookupDetails("PERSON_IDENTITY_LIST", false);
				List<Identity> identities = personService
						.getAllIdentityOnlyByImplementation(implem);
				Map<Integer, LookupDetail> lookupDetailMap = new HashMap<Integer, LookupDetail>();
				for (LookupDetail lookupDetail : activeLookupDetailsIdentityType) {
					lookupDetailMap.put(lookupDetail.getDataId(), lookupDetail);
				}
				for (Identity identity : identities) {
					if (identity.getIdentityType() != null)
						identity.setLookupDetail(lookupDetailMap.get(identity
								.getIdentityType()));
				}

				// Leave Type Update by Lookup Table Information on Leave
				// Table
				List<LookupDetail> lookupDetailLeaveUpdate = lookupMasterBL
						.getActiveLookupDetails("LEAVE_TYPE_LIST", false);
				List<Leave> leaves = leaveBL.getLeaveService()
						.getActiveLeaveList(implem);
				Map<String, LookupDetail> lookupDetailLeaveMap = new HashMap<String, LookupDetail>();
				for (LookupDetail lookupDetail : lookupDetailLeaveUpdate) {
					lookupDetailLeaveMap.put(lookupDetail.getAccessCode(),
							lookupDetail);
				}
				for (Leave leave : leaves) {
					if (leave.getLeaveCode() != null)
						leave.setLookupDetail(lookupDetailLeaveMap.get(leave
								.getLeaveCode()));
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public LookupDetail getTypeLookup(List<LookupMaster> activeLookupDetails,
			Integer id) {
		LookupDetail lookupDetail = null;
		if (activeLookupDetails != null && activeLookupDetails.size() > 0) {
			List<LookupDetail> lookupDetails = new ArrayList<LookupDetail>(
					activeLookupDetails.get(0).getLookupDetails());
			for (LookupDetail templookupDetail : lookupDetails) {
				if (templookupDetail.getDataId().equals(id)) {
					lookupDetail = new LookupDetail();
					lookupDetail.setLookupDetailId(templookupDetail
							.getLookupDetailId());
				}
			}
		}
		return lookupDetail;
	}

	public Map<Byte, String> payPolicy() {
		Map<Byte, String> types = new HashMap<Byte, String>();
		types.put(Constants.HR.PayPolicy.Monthly.getCode(),
				Constants.HR.PayPolicy.Monthly.name());
		types.put(Constants.HR.PayPolicy.Weekly.getCode(),
				Constants.HR.PayPolicy.Weekly.name());
		types.put(Constants.HR.PayPolicy.Daily.getCode(),
				Constants.HR.PayPolicy.Daily.name());
		types.put(Constants.HR.PayPolicy.Yearly.getCode(),
				Constants.HR.PayPolicy.Yearly.name());

		return types;
	}

	public Map<Byte, String> payMode() {
		Map<Byte, String> types = new HashMap<Byte, String>();
		types.put(Constants.HR.PayMode.Salary.getCode(),
				Constants.HR.PayMode.Salary.name());
		types.put(Constants.HR.PayMode.Cash.getCode(),
				Constants.HR.PayMode.Cash.name());
		types.put(Constants.HR.PayMode.Transfer.getCode(),
				Constants.HR.PayMode.Transfer.name());
		types.put(Constants.HR.PayMode.Other.getCode(),
				Constants.HR.PayMode.Other.name());
		return types;
	}

	public Map<Byte, String> calculationType() {
		Map<Byte, String> types = new HashMap<Byte, String>();
		types.put(Constants.HR.CalculationType.Fixed.getCode(),
				Constants.HR.CalculationType.Fixed.name());
		types.put(Constants.HR.CalculationType.Percentage.getCode(),
				Constants.HR.CalculationType.Percentage.name());
		types.put(Constants.HR.CalculationType.Count.getCode(),
				Constants.HR.CalculationType.Count.name());
		types.put(Constants.HR.CalculationType.WorkDay.getCode(),
				Constants.HR.CalculationType.WorkDay.name());
		types.put(Constants.HR.CalculationType.Variance.getCode(),
				Constants.HR.CalculationType.Variance.name());
		return types;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public PersonService getPersonService() {
		return personService;
	}

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

	public CompanyService getCompanyService() {
		return companyService;
	}

	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public WorkingShiftBL getWorkingShiftBL() {
		return workingShiftBL;
	}

	public void setWorkingShiftBL(WorkingShiftBL workingShiftBL) {
		this.workingShiftBL = workingShiftBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public LeaveBL getLeaveBL() {
		return leaveBL;
	}

	public void setLeaveBL(LeaveBL leaveBL) {
		this.leaveBL = leaveBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
