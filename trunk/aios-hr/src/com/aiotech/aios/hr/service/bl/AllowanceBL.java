package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Allowance;
import com.aiotech.aios.hr.domain.entity.AllowanceTransaction;
import com.aiotech.aios.hr.domain.entity.vo.AllowanceVO;
import com.aiotech.aios.hr.service.AllowanceService;
import com.aiotech.aios.system.service.bl.LookupMasterBL;

public class AllowanceBL {

	private LookupMasterBL lookupMasterBL;
	private AllowanceService allowanceService;
	private PayPeriodBL payPeriodBL;
	
	
	public AllowanceVO convertEntityToVO(
			Allowance allowance) {
		AllowanceVO allowanceVO = new AllowanceVO();
		try {
			// Copy Entity class properties value into VO properties.
			BeanUtils.copyProperties(allowanceVO, allowance);
			allowanceVO.setFromDateView(DateFormat
					.convertDateToString(allowance.getFromDate() + ""));
			allowanceVO.setToDateView(DateFormat
					.convertDateToString(allowance.getToDate() + ""));
			if (allowance.getIsFinanceImpact() != null
					&& allowance.getIsFinanceImpact())
				allowanceVO.setIsFinanceView("YES");
			else
				allowanceVO.setIsFinanceView("NO");
			
			if(allowance.getLookupDetailByAllowanceType()!=null)
				allowanceVO.setAllowanceType(allowance.getLookupDetailByAllowanceType().getDisplayName());
			if(allowance.getLookupDetailByAllowanceSubType()!=null)
				allowanceVO.setAllowanceSubType(allowance.getLookupDetailByAllowanceSubType().getDisplayName());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return allowanceVO;
	}
	
	public String saveAllowance(Allowance allowance,
			List<AllowanceTransaction> allowanceTransactions) {
		String returnStatus = "SUCCESS";
		try {
			if (allowance.getAllowanceId() != null
					&& allowance.getAllowanceId() > 0) {
				List<AllowanceTransaction> tempTransactions = new ArrayList<AllowanceTransaction>(
						allowance.getAllowanceTransactions());
				List<AllowanceTransaction> deleteTransactions = getPaymentsToDelete(
						allowanceTransactions, tempTransactions);

				if (deleteTransactions != null
						&& deleteTransactions.size() > 0)
					allowanceService
							.deleteAllAllowanceTransaction(deleteTransactions);
			}

			allowanceService
					.saveAllowance(allowance);
			for (AllowanceTransaction allowanceTransaction : allowanceTransactions) {
				allowanceTransaction.setAllowance(allowance);
			}
			allowanceService
					.saveAllAllowanceTransaction(allowanceTransactions);
		} catch (Exception e) {
			returnStatus = "ERROR";
		}
		return returnStatus;
	}
	
	public List<AllowanceTransaction> getPaymentsToDelete(
			List<AllowanceTransaction> payments,
			List<AllowanceTransaction> editPayments) {
		List<AllowanceTransaction> deletePayments = new ArrayList<AllowanceTransaction>();

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		for (AllowanceTransaction payment1 : payments) {
			if (payment1.getAllowanceTransactionId() != null
					&& payment1.getAllowanceTransactionId() > 0)
				listOne.add(payment1.getAllowanceTransactionId());
		}
		for (AllowanceTransaction payment2 : editPayments) {
			if (payment2.getAllowanceTransactionId() != null
					&& payment2.getAllowanceTransactionId() > 0)
				listTwo.add(payment2.getAllowanceTransactionId());
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);

		// Delete Process
		for (Long lng : different) {
			for (AllowanceTransaction payment3 : editPayments) {
				if (lng != null
						&& payment3.getAllowanceTransactionId().equals(
								lng)) {
					deletePayments.add(payment3);
				}
			}
		}
		return deletePayments;
	}


	public String deleteAllowance(Allowance allowance,
			List<AllowanceTransaction> allowanceTransactions) {
		String returnStatus = "SUCCESS";
		try {
			allowanceService
					.deleteAllAllowanceTransaction(allowanceTransactions);
			allowanceService.deleteAllowance(allowance);
		} catch (Exception e) {
			returnStatus = "ERROR";
		}
		return returnStatus;
	}
	
	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}


	public PayPeriodBL getPayPeriodBL() {
		return payPeriodBL;
	}

	public void setPayPeriodBL(PayPeriodBL payPeriodBL) {
		this.payPeriodBL = payPeriodBL;
	}

	public AllowanceService getAllowanceService() {
		return allowanceService;
	}

	public void setAllowanceService(AllowanceService allowanceService) {
		this.allowanceService = allowanceService;
	}


}
