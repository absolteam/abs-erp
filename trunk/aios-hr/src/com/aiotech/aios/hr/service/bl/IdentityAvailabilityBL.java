package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.IdentityAvailability;
import com.aiotech.aios.hr.domain.entity.vo.IdentityAvailabilityVO;
import com.aiotech.aios.hr.service.IdentityAvailabilityService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class IdentityAvailabilityBL {
	private IdentityAvailabilityService identityAvailabilityService;
	private CommentBL commentBL;
	private SystemService systemService;
	private AlertBL alertBL;
	private WorkflowEnterpriseService workflowEnterpriseService;

	public void saveIdentityAvailability(
			IdentityAvailability identityAvailability,
			IdentityAvailabilityVO iaVO) {
		try {
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (identityAvailability != null && identityAvailability.getIdentityAvailabilityId() != null
					&& identityAvailability.getIdentityAvailabilityId()> 0) {

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			workflowDetailVo.setRejectAlertId(iaVO.getAlertId());
			identityAvailabilityService
					.saveIdentityAvailability(identityAvailability);
			/*if (iaVO != null && iaVO.getAlertId() != null
					&& iaVO.getAlertId() > 0) {
				alertBL.alertCommonUpdate(iaVO.getAlertId());
			}
*/
			Map<String, Object> sessionObj = ActionContext.getContext()
			.getSession();
			User user = (User) sessionObj.get("USER");
			
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					IdentityAvailability.class.getSimpleName(),
					identityAvailability.getIdentityAvailabilityId(), user,
					workflowDetailVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void deleteIdentityAvailability(Long identityAvailabilityId,Long messageId) throws Exception {
		
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				IdentityAvailability.class.getSimpleName(), identityAvailabilityId,
				user, workflowDetailVo);
	}
	
	
	public void deleteIdentityAvailability(Long identityAvailabilityId) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				IdentityAvailability.class.getSimpleName(), identityAvailabilityId,
				user, workflowDetailVo);
	}

	public void doIdentityAvailabilityBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		IdentityAvailability identityAvailability = identityAvailabilityService
		.getIdentityAvailabilityInfo(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			identityAvailabilityService
			.deleteIdentityAvailability(identityAvailability);
		} else {
		}
	}
	

	public List<IdentityAvailabilityVO> convertEntitiesTOVOs(
			List<IdentityAvailability> identityAvailabilities) throws Exception {

		List<IdentityAvailabilityVO> tosList = new ArrayList<IdentityAvailabilityVO>();

		for (IdentityAvailability pers : identityAvailabilities) {
			tosList.add(convertEntityToVO(pers));
		}

		return tosList;
	}

	public IdentityAvailabilityVO convertEntityToVO(
			IdentityAvailability identityAvailability) {
		IdentityAvailabilityVO identityAvailabilityVO = new IdentityAvailabilityVO();

		identityAvailabilityVO.setIdentityAvailabilityId(identityAvailability
				.getIdentityAvailabilityId());
		identityAvailabilityVO.setIdentity(identityAvailability.getIdentity());
		identityAvailabilityVO.setActualReturnDate(identityAvailability
				.getActualReturnDate());
		identityAvailabilityVO.setHandoverDate(identityAvailability
				.getHandoverDate());
	
		identityAvailabilityVO.setExpectedReturnDate(identityAvailability
				.getExpectedReturnDate());
		identityAvailabilityVO.setImplementation(identityAvailability
				.getImplementation());
		
		identityAvailabilityVO.setPurpose(identityAvailability.getPurpose());

		if (identityAvailability.getIdentity().getLookupDetail() != null)
			identityAvailabilityVO.setIdentityType(identityAvailability
					.getIdentity().getLookupDetail().getDisplayName());

		if (identityAvailability.getIdentity().getPerson() != null)
			identityAvailabilityVO.setIdentityName(identityAvailability
					.getIdentity().getPerson().getFirstName()
					+ " "
					+ identityAvailability.getIdentity().getPerson()
							.getLastName()
					+ "["
					+ identityAvailability.getIdentity().getPerson()
							.getPersonNumber() + "]");
		else
			identityAvailabilityVO.setIdentityName(identityAvailability
					.getIdentity().getCompany().getCompanyName()
					+ "");

	
		if (identityAvailability.getHandoverDate() != null)
			identityAvailabilityVO.setHandoverDateStr(DateFormat
					.convertDateToString(identityAvailability.getHandoverDate()
							+ ""));

		if (identityAvailability.getExpectedReturnDate() != null)
			identityAvailabilityVO.setExpectedReturnDateStr(DateFormat
					.convertDateToString(identityAvailability
							.getExpectedReturnDate() + ""));

		if (identityAvailability.getActualReturnDate() != null)
			identityAvailabilityVO.setActualReturnDateStr(DateFormat
					.convertDateToString(identityAvailability
							.getActualReturnDate() + ""));

			
		return identityAvailabilityVO;
	}

	

	public IdentityAvailabilityService getIdentityAvailabilityService() {
		return identityAvailabilityService;
	}

	public void setIdentityAvailabilityService(
			IdentityAvailabilityService identityAvailabilityService) {
		this.identityAvailabilityService = identityAvailabilityService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

}
