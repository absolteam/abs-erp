package com.aiotech.aios.hr.service;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Department;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class CmpDeptLocationService {

	private AIOTechGenericDAO<CmpDeptLoc> cmpDeptLocationDAO;
	private AIOTechGenericDAO<Department> departmentDAO;

	public void saveCmpDeptLocation(CmpDeptLoc cmpDeptLocation)throws Exception{
		cmpDeptLocationDAO.saveOrUpdate(cmpDeptLocation);
	}
	
	public void deleteSetup(CmpDeptLoc cmpDeptLocation)throws Exception{
		cmpDeptLocationDAO.delete(cmpDeptLocation);
	}
	
	public void deleteSetupAll(List<CmpDeptLoc> cmpDeptLocations)throws Exception{
		cmpDeptLocationDAO.deleteAll(cmpDeptLocations);
	}
	
	public List<CmpDeptLoc> getAllCmpDeptLocation(Company company) throws Exception{
		return cmpDeptLocationDAO.findByNamedQuery("getAllCompanySetup", company);
	}
	
	public List<CmpDeptLoc> getSetupCDLBased(Long companyId,Long departmentId,Long locationId) throws Exception{
		return cmpDeptLocationDAO.findByNamedQuery("getSetupCDLBased", companyId,departmentId,locationId);
	}
	
	public List<CmpDeptLoc> getSetupCLBased(Long companyId,Long locationId) throws Exception{
		return cmpDeptLocationDAO.findByNamedQuery("getSetupCLBased", companyId,locationId);
	}
	
	public List<CmpDeptLoc> getSetupDepartmentBased(Long departmentId) throws Exception{
		return cmpDeptLocationDAO.findByNamedQuery("getSetupDepartmentBased", departmentId);
	}
	
	public List<CmpDeptLoc> getSetupLocationBased(Long locationId) throws Exception{
		return cmpDeptLocationDAO.findByNamedQuery("getSetupLocationBased", locationId);
	}
	
	public CmpDeptLoc getCompanySetup(Long setupId) throws Exception{
		return cmpDeptLocationDAO.findById(setupId);
	}
	
	public List<CmpDeptLoc> getSetupByCombinationId(Long combinationId) throws Exception{
		return cmpDeptLocationDAO.findByNamedQuery("getSetupByCombinationId", combinationId);
	}
	
	//Get Location list
	public List<CmpDeptLoc> getAllLocation(Implementation implementation) {
		return cmpDeptLocationDAO.findByNamedQuery("getAllLocation", implementation);
	}
	
	public List<Department> getCompanyBasedDepartment(Long companyId) {
		return departmentDAO.findByNamedQuery("getCompanyBasedDepartment", companyId);
	}
	
	public AIOTechGenericDAO<CmpDeptLoc> getCmpDeptLocationDAO() {
		return cmpDeptLocationDAO;
	}

	public void setCmpDeptLocationDAO(
			AIOTechGenericDAO<CmpDeptLoc> cmpDeptLocationDAO) {
		this.cmpDeptLocationDAO = cmpDeptLocationDAO;
	}

	public AIOTechGenericDAO<Department> getDepartmentDAO() {
		return departmentDAO;
	}

	public void setDepartmentDAO(AIOTechGenericDAO<Department> departmentDAO) {
		this.departmentDAO = departmentDAO;
	}
	
	
} 
