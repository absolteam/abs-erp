package com.aiotech.aios.hr.service;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.AdvertisingPanel;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AdvertisingPanelService {
	public AIOTechGenericDAO<AdvertisingPanel> advertisingPanelDAO;

	public List<AdvertisingPanel> getAllAdvertisingList(
			Implementation implementation) {
		return advertisingPanelDAO.findByNamedQuery("getAllAdvertisingList",
				implementation,(byte)WorkflowConstants.Status.Published.getCode());
	}
	
	public List<AdvertisingPanel> getAllAdvertisingListWithPosition(
			Long openPositionId,Long recrutitmentSourceId) {
		return advertisingPanelDAO.findByNamedQuery("getAllAdvertisingListWithPosition",
				openPositionId,recrutitmentSourceId);
	}
	

	public AdvertisingPanel getAdvertisingPanelById(Long advertisingPanelId) {
		List<AdvertisingPanel> advertisingPanels = advertisingPanelDAO
				.findByNamedQuery("getAdvertisingPanelById", advertisingPanelId);
		if (advertisingPanels != null && advertisingPanels.size() > 0)
			return advertisingPanels.get(0);
		else
			return null;
	}

	public void saveAdvertisingPanel(AdvertisingPanel advertisingPanel){
		advertisingPanelDAO.saveOrUpdate(advertisingPanel);
	}
	
	public void deleteAdvertisingPanel(AdvertisingPanel advertisingPanel){
		advertisingPanelDAO.delete(advertisingPanel);
	}
	public AIOTechGenericDAO<AdvertisingPanel> getAdvertisingPanelDAO() {
		return advertisingPanelDAO;
	}

	public void setAdvertisingPanelDAO(
			AIOTechGenericDAO<AdvertisingPanel> advertisingPanelDAO) {
		this.advertisingPanelDAO = advertisingPanelDAO;
	}
}
