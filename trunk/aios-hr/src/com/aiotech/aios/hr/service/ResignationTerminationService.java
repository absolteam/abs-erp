package com.aiotech.aios.hr.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.hr.domain.entity.EosPolicy;
import com.aiotech.aios.hr.domain.entity.ResignationTermination;
import com.aiotech.aios.hr.domain.entity.vo.ResignationTerminationVO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ResignationTerminationService {
	public AIOTechGenericDAO<ResignationTermination> resignationTerminationDAO;
	public AIOTechGenericDAO<EosPolicy> eosPolicyDAO;

	public List<ResignationTermination> getAllResignationTermination(
			Implementation implementation) {
		return resignationTerminationDAO.findByNamedQuery(
				"getAllResignationTermination", implementation);
	}

	public List<EosPolicy> findEosPolicy(ResignationTerminationVO vo)
			throws Exception {
		Criteria criteria = eosPolicyDAO.createCriteria();

		//criteria.createAlias("grade", "grd",CriteriaSpecification.LEFT_JOIN);
		//criteria.createAlias("implementation", "imp",CriteriaSpecification.LEFT_JOIN);

		criteria.add(Restrictions.eq("type", vo.getType()));

		/*criteria.add(Restrictions.eq("imp.implementationId", vo
				.getImplementation().getImplementationId()));*/
		criteria.add(Restrictions.eq("isActive", true));
		return criteria.list();

	}

	public ResignationTermination getResignationTerminationById(Long id) {
		List<ResignationTermination> list = resignationTerminationDAO
				.findByNamedQuery("getResignationTerminationById", id);
		if (list != null && list.size() > 0)
			return list.get(0);
		else
			return null;
	}

	public List<ResignationTermination> getAllResignationTerminationToActivate(
			Implementation implementation) {
		return resignationTerminationDAO.findByNamedQuery(
				"getAllResignationTerminationToActivate", implementation);
	}

	public void saveResignationTermination(
			ResignationTermination resignationTermination) {
		resignationTerminationDAO.saveOrUpdate(resignationTermination);
	}

	public void deleteResignationTermination(
			ResignationTermination resignationTermination) {
		resignationTerminationDAO.delete(resignationTermination);
	}

	public AIOTechGenericDAO<ResignationTermination> getResignationTerminationDAO() {
		return resignationTerminationDAO;
	}

	public void setResignationTerminationDAO(
			AIOTechGenericDAO<ResignationTermination> resignationTerminationDAO) {
		this.resignationTerminationDAO = resignationTerminationDAO;
	}

	public AIOTechGenericDAO<EosPolicy> getEosPolicyDAO() {
		return eosPolicyDAO;
	}

	public void setEosPolicyDAO(AIOTechGenericDAO<EosPolicy> eosPolicyDAO) {
		this.eosPolicyDAO = eosPolicyDAO;
	}

}
