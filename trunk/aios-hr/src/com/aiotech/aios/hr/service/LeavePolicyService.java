package com.aiotech.aios.hr.service;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.LeavePolicy;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class LeavePolicyService {
	public AIOTechGenericDAO<LeavePolicy> leavePolicyDAO;
	
	public List<LeavePolicy> getLeavePolicyList(Implementation implementation) {
		return leavePolicyDAO.findByNamedQuery("getLeavePolicyList", implementation);
	}
	
	public void deleteLeavePolicies(List<LeavePolicy> deleteLeavePolicies)
			throws Exception {
		leavePolicyDAO.deleteAll(deleteLeavePolicies);
	}
	
	public void saveLeavePolicies(List<LeavePolicy> leavePolicies){
		leavePolicyDAO.saveOrUpdateAll(leavePolicies);
	}
	public AIOTechGenericDAO<LeavePolicy> getLeavePolicyDAO() {
		return leavePolicyDAO;
	}
	public void setLeavePolicyDAO(AIOTechGenericDAO<LeavePolicy> leavePolicyDAO) {
		this.leavePolicyDAO = leavePolicyDAO;
	} 

}
