package com.aiotech.aios.hr.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.CompanyTypeAllocation;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class CompanyService {

	public AIOTechGenericDAO<Company> companyDAO;
	public AIOTechGenericDAO<CompanyTypeAllocation> companyTypeAllocationDAO;
	public AIOTechGenericDAO<Identity> identityDAO;

	public AIOTechGenericDAO<Company> getCompanyDAO() {
		return companyDAO;
	}

	public void setCompanyDAO(AIOTechGenericDAO<Company> companyDAO) {
		this.companyDAO = companyDAO;
	}

	public Company getCompanyById(Long id) {
		return companyDAO.findById(id);
	}

	@SuppressWarnings("unchecked")
	public Company getCompanyByCustomerId(Long customerId, Session session)
			throws Exception {
		String getCompanyByCustomerId = "SELECT Distinct(c) FROM Company c "
				+ " WHERE c.companyId=(SELECT cst.company.companyId FROM Customer cst WHERE cst.customerId = ?)";
		List<Company> company = session.createQuery(getCompanyByCustomerId)
				.setLong(0, customerId).list();
		return company.get(0);
	}

	public Company getCompanyInfo(Long id) {
		List<Company> companys = new ArrayList<Company>();
		companys = companyDAO.findByNamedQuery("getCompanyInfo", id);
		if (companys != null && companys.size() > 0)
			return companys.get(0);
		else
			return null;
	}

	public List<Company> getAllCompany(Implementation implementation) {
		return companyDAO.findByNamedQuery("getAllCompanys", implementation,
				(byte)WorkflowConstants.Status.Published.getCode());
	}

	public Company getCompanyByCustomerId(long customerId) {
		return companyDAO
				.findByNamedQuery("getCompanyByCustomerId", customerId).get(0);
	}

	public List<Company> getAllSupplierCompanies(Implementation implementation) {
		return companyDAO.findByNamedQuery("getAllSupplierCompanies",
				implementation);
	}

	public List<Company> getOnlyCompanies(Implementation implementation) {
		return companyDAO.findByNamedQuery("getOnlyCompanies", implementation);
	}

	public void saveCompany(Company company, List<Identity> identitys) {
		companyDAO.saveOrUpdate(company);
		if (identitys != null) {
			for (Identity identity : identitys) {
				identity.setCompany(company);
			}
			identityDAO.saveOrUpdateAll(identitys);
		}

	}
	
	public void saveCompany(List<Company> company) {
		companyDAO.saveOrUpdateAll(company); 
	}

	public void updateCompany(Company company) {
		companyDAO.saveOrUpdate(company);
	}

	public void updateCompany(Company company, Session session) {
		session.saveOrUpdate(company);
	}

	public void deleteCompany(Company company) {
		companyDAO.delete(company);
	}

	public void saveCompanyTypes(List<CompanyTypeAllocation> companytypes)
			throws Exception {
		companyTypeAllocationDAO.saveOrUpdateAll(companytypes);
	}

	public void deleteCompanyTypes(List<CompanyTypeAllocation> companytypes)
			throws Exception {
		companyTypeAllocationDAO.deleteAll(companytypes);
	}

	public void saveCompanyType(CompanyTypeAllocation companytype)
			throws Exception {
		companyTypeAllocationDAO.saveOrUpdate(companytype);
	}

	public void deleteCompanyType(CompanyTypeAllocation companytype)
			throws Exception {
		companyTypeAllocationDAO.delete(companytype);
	}

	public List<CompanyTypeAllocation> getAllCompanyTypeAllocation(
			Company company) throws Exception {
		return companyTypeAllocationDAO.findByNamedQuery(
				"getAllCompanyTypeAllocation", company);
	}

	public void deleteIdentityAll(List<Identity> identitys) throws Exception {
		identityDAO.deleteAll(identitys);
	}

	public AIOTechGenericDAO<CompanyTypeAllocation> getCompanyTypeAllocationDAO() {
		return companyTypeAllocationDAO;
	}

	public void setCompanyTypeAllocationDAO(
			AIOTechGenericDAO<CompanyTypeAllocation> companyTypeAllocationDAO) {
		this.companyTypeAllocationDAO = companyTypeAllocationDAO;
	}

	public AIOTechGenericDAO<Identity> getIdentityDAO() {
		return identityDAO;
	}

	public void setIdentityDAO(AIOTechGenericDAO<Identity> identityDAO) {
		this.identityDAO = identityDAO;
	}
}
