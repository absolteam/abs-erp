package com.aiotech.aios.hr.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Department;
import com.aiotech.aios.hr.domain.entity.Designation;
import com.aiotech.aios.hr.domain.entity.Grade;
import com.aiotech.aios.hr.domain.entity.Job;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobLeave;
import com.aiotech.aios.hr.domain.entity.JobPayrollElement;
import com.aiotech.aios.hr.domain.entity.JobShift;
import com.aiotech.aios.hr.domain.entity.Leave;
import com.aiotech.aios.hr.domain.entity.LeaveReconciliation;
import com.aiotech.aios.hr.domain.entity.Location;
import com.aiotech.aios.hr.domain.entity.PayrollElement;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.WorkingShift;
import com.aiotech.aios.hr.domain.entity.vo.JobPayrollElementVO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class JobService {

	public AIOTechGenericDAO<CmpDeptLoc> cmpDeptLocationDAO;
	public AIOTechGenericDAO<Location> locationDAO;
	public AIOTechGenericDAO<Department> departmentDAO;
	public AIOTechGenericDAO<Grade> gradeDAO;
	public AIOTechGenericDAO<Job> jobDAO;
	public AIOTechGenericDAO<Designation> designationDAO;
	public AIOTechGenericDAO<WorkingShift> workingShiftDAO;
	public AIOTechGenericDAO<JobShift> jobShiftDAO;
	public AIOTechGenericDAO<Leave> leaveDAO;
	public AIOTechGenericDAO<JobLeave> jobLeaveDAO;
	public AIOTechGenericDAO<JobAssignment> jobAssignmentDAO;

	public AIOTechGenericDAO<PayrollElement> payrollElementDAO;
	public AIOTechGenericDAO<JobPayrollElement> jobPayrollElementDAO;

	// Get Job list
	public List<Job> getAllJob(Implementation implementation) {
		return jobDAO.findByNamedQuery("getAllJob", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Get Job list
	public List<Job> getAllJobWithoutStatus(Implementation implementation) {
		return jobDAO
				.findByNamedQuery("getAllJobWithoutStatus", implementation);
	}

	// Get Job list
	public List<Job> getAllJobBasedOnCompany(Long companyId) {
		return jobDAO.findByNamedQuery("getAllJobBasedOnCompany", companyId);
	}

	// Get JobAssignment list
	public List<JobAssignment> getAllJobAssignmentBasedOnCompanyWPS(
			Long companyId) {
		return jobAssignmentDAO.findByNamedQuery(
				"getAllJobAssignmentBasedOnCompanyWPS", companyId);
	}

	// Get JobAssignment list
	public List<JobAssignment> getAllJobAssignmentBasedOnCompany(Long companyId) {
		return jobAssignmentDAO.findByNamedQuery(
				"getAllJobAssignmentBasedOnCompany", companyId);
	}

	// Get Job list
	public Job getJobInfo(Long jobId) {
		return jobDAO.findByNamedQuery("getJob", jobId).get(0);
	}

	// Get Job list
	public Job getJobForDelete(Long jobId) {
		return jobDAO.findByNamedQuery("getJobForDelete", jobId).get(0);
	}

	// Get PayrollElement list
	public List<PayrollElement> getAllPayrollElement(
			Implementation implementation) {
		return payrollElementDAO.findByNamedQuery("getAllPayrollElement",
				implementation);
	}

	// Get Designation list
	public List<Designation> getAllDesignation(Implementation implementation) {
		return designationDAO.findByNamedQuery("getAllDesignation",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Get Location list
	public List<CmpDeptLoc> getAllLocation(Implementation implementation) {
		return cmpDeptLocationDAO.findByNamedQuery("getAllLocation",
				implementation);
	}
	
	public List<CmpDeptLoc> getAllSharedLocation(Implementation implementation) {
		return cmpDeptLocationDAO.findByNamedQuery("getAllSharedLocation",
				implementation);
	}

	// Get leave list
	public List<Leave> getAllLeave(Implementation implementation) {
		return leaveDAO.findByNamedQuery("getAllLeave", implementation);
	}

	// Get leave list
	public List<Leave> getAllLeaveBasedOnJob(Job job) {
		return leaveDAO.findByNamedQuery("getAllLeaveBasedOnJob", job);
	}

	// Get shifts list
	public List<WorkingShift> getAllShifts(Implementation implementation) {
		return workingShiftDAO.findByNamedQuery("getAllShift", implementation);
	}

	// Get shifts list
	public List<WorkingShift> getAllShiftWithEmployee(
			Implementation implementation) {
		return workingShiftDAO.findByNamedQuery("getAllShiftWithEmployee",
				implementation);
	}

	public void saveJob(Job job) {
		jobDAO.saveOrUpdate(job);
	}

	public void saveJobAllowanceAll(List<JobPayrollElement> jobPayrollElements) {
		jobPayrollElementDAO.saveOrUpdateAll(jobPayrollElements);
	}

	public void saveJobLeaveAll(List<JobLeave> jobLeaves) {
		jobLeaveDAO.saveOrUpdateAll(jobLeaves);
	}

	public void saveJobShiftAll(List<JobShift> jobShifts) {
		jobShiftDAO.saveOrUpdateAll(jobShifts);
	}

	public void saveJobAssignmentAll(List<JobAssignment> jobAssignments) {
		jobAssignmentDAO.saveOrUpdateAll(jobAssignments);
	}

	// Get job payroll element list
	public List<JobPayrollElement> getAllJobPayrollElement(Job job) {
		return jobPayrollElementDAO.findByNamedQuery("getAllJobPayrollElement",
				job);
	}

	// Get job leave list
	public List<JobLeave> getAllJobLeave(Job job) {
		return jobLeaveDAO.findByNamedQuery("getAllJobLeave", job);
	}

	// Get job leave list
	public List<JobShift> getAllJobShift(Job job) {
		return jobShiftDAO.findByNamedQuery("getAllJobShift", job);
	}

	// Get job leave list
	public List<JobAssignment> getAllJobAssignment(Implementation implementation) {
		return jobAssignmentDAO.findByNamedQuery("getAllJobAssignment",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Get job leave list
	public List<JobAssignment> getAllActiveJobAssignment(
			Implementation implementation) {
		return jobAssignmentDAO.findByNamedQuery("getAllActiveJobAssignment",
				implementation);
	}

	// Get job leave list
	public List<JobAssignment> getJobAssignmentByPerson(Long personId) {
		return jobAssignmentDAO.findByNamedQuery("getJobAssignmentByPerson",
				personId);
	}

	// Get job leave list
	public JobAssignment getJobAssignmentInfo(Long jobAssignmentId) {
		List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();
		jobAssignments = jobAssignmentDAO.findByNamedQuery(
				"getJobAssignmentInfo", jobAssignmentId);
		if (jobAssignments != null && jobAssignments.size() > 0)
			return jobAssignments.get(0);
		else
			return null;
	}

	// Get job leave list
	public JobLeave getJobLeaveInfo(Long jobLeaveId) {
		List<JobLeave> jobAssignments = new ArrayList<JobLeave>();
		jobAssignments = jobLeaveDAO.findByNamedQuery("getJobLeaveInfo",
				jobLeaveId);
		if (jobAssignments != null && jobAssignments.size() > 0)
			return jobAssignments.get(0);
		else
			return null;
	}

	// Get job job list with date
	public List<JobShift> getAllJobShiftWithDate(Job job, Date date) {
		return jobShiftDAO.findByNamedQuery("getAllJobShiftWithDate", job,
				date, date);
	}

	public List<JobShift> getAllJobShiftWithDateByAssignment(
			JobAssignment jobAssignment, Date date) {
		return jobShiftDAO
				.findByNamedQuery("getAllJobShiftWithDateByAssignment",
						jobAssignment, date, date);
	}

	public List<JobShift> getAllJobShiftWithOutDate(Job job) {
		return jobShiftDAO.findByNamedQuery("getAllJobShiftWithOutDate", job);
	}

	public List<JobShift> getAllJobShiftWithOutDateByAssignment(
			JobAssignment job) {
		return jobShiftDAO.findByNamedQuery(
				"getAllJobShiftWithOutDateByAssignment", job);
	}

	public List<JobShift> getAllJobShiftWithLocationAndDate(Long locationId) {
		return jobShiftDAO.findByNamedQuery(
				"getAllJobShiftWithLocationAndDate", locationId);
	}

	public List<JobShift> getAllJobShiftWithLocationAndDateByAssignment(
			Long locationId) {
		return jobShiftDAO.findByNamedQuery(
				"getAllJobShiftWithLocationAndDateByAssignment", locationId);
	}

	public List<JobShift> getAllJobShiftByJobAndJobAssignment(
			Long jobAssignmentId) {
		return jobShiftDAO.findByNamedQuery(
				"getAllJobShiftByJobAndJobAssignment", jobAssignmentId);
	}

	public List<JobShift> getAllJobShiftByJobAssignment(Long jobAssignmentId) {
		return jobShiftDAO.findByNamedQuery("getAllJobShiftByJobAssignment",
				jobAssignmentId);
	}

	// Get job leave list
	public List<JobLeave> getCasualJobLeave(Job job) {
		return jobLeaveDAO.findByNamedQuery("getCasualJobLeave", job);
	}

	public void deleteJobAllowanceAll(List<JobPayrollElement> jobPayrollElements) {
		jobPayrollElementDAO.deleteAll(jobPayrollElements);
	}

	public void deleteJobLeaveAll(List<JobLeave> jobLeaves) {
		jobLeaveDAO.deleteAll(jobLeaves);
	}

	public void deleteJobAssignemntAll(List<JobAssignment> jobAssignments) {
		jobAssignmentDAO.deleteAll(jobAssignments);
	}

	public void deleteJobShiftAll(List<JobShift> jobShifts) {
		jobShiftDAO.deleteAll(jobShifts);
	}

	public void deleteJob(Job job) {
		jobDAO.delete(job);
	}

	public List<JobLeave> getAllJobLeaveByPerson(Person person) {
		return jobLeaveDAO.findByNamedQuery("getAllJobLeaveByPerson", person);
	}

	public List<Job> getShiftWithJob(Implementation implementation) {
		return jobDAO
				.findByNamedQuery("getJobWithWorkingShift", implementation);
	}

	public List<JobAssignment> getEmployeesLeaveReconciliationsByCriteria(
			Date fromDate, Date toDate, Boolean isActive,
			Implementation implementation, Long jobAssignmentId)
			throws Exception {

		Criteria jobAssignmentCriteria = jobAssignmentDAO.createCriteria();

		DetachedCriteria dc = DetachedCriteria
				.forClass(LeaveReconciliation.class);

		if (fromDate != null && toDate != null) {
			dc.add(Restrictions.gt("startDate", fromDate)).add(
					Restrictions.lt("endDate", toDate));
		}

		if (implementation != null) {
			dc.add(Restrictions.eq("implementation", implementation));
		}

		if (isActive != null) {
			dc.add(Restrictions.eq("isActive", isActive));
		}

		if (jobAssignmentId != null && jobAssignmentId > 0L) {
			dc.add(Restrictions.eq("leaveReconciliationId", jobAssignmentId));
		}

		dc.setProjection(Projections.distinct(Projections
				.property("jobAssignment.jobAssignmentId")));

		jobAssignmentCriteria.setFetchMode("job", FetchMode.EAGER);
		jobAssignmentCriteria.setFetchMode("designation", FetchMode.EAGER)
				.setFetchMode("designation.grade", FetchMode.EAGER);

		jobAssignmentCriteria.setFetchMode("cmpDeptLocation", FetchMode.EAGER)
				.setFetchMode("cmpDeptLocation.company", FetchMode.EAGER)
				.setFetchMode("cmpDeptLocation.department", FetchMode.EAGER)
				.setFetchMode("cmpDeptLocation.location", FetchMode.EAGER)
				.setFetchMode("person", FetchMode.EAGER);

		jobAssignmentCriteria.setFetchMode("cmpDeptLocation", FetchMode.EAGER)
				.setFetchMode("cmpDeptLocation.grade", FetchMode.EAGER)
				.setFetchMode("designation", FetchMode.EAGER);

		jobAssignmentCriteria.add(Property.forName("jobAssignmentId").in(dc));

		List<JobAssignment> result = jobAssignmentCriteria.list();
		for (JobAssignment ja : result) {
			for (LeaveReconciliation lrc : ja.getLeaveReconciliations()) {
				lrc.getJobLeave();
				lrc.getJobLeave().getLeave();
				lrc.getJobLeave().getLeave().getLookupDetail();
				lrc.getLeave();
				lrc.getLeave().getLookupDetail();
				lrc.getLeave().getLookupDetail().getDisplayName();
			}
		}
		return result;
	}

	public List<JobPayrollElement> getJobPayrollByJobAssignment(
			JobPayrollElementVO jobPayrollElementVo) throws Exception {
		Criteria jobPayrollCriteria = jobPayrollElementDAO.createCriteria();

		jobPayrollCriteria.createAlias("job", "jb");
		jobPayrollCriteria.createAlias("jb.jobAssignments", "jobAssignment");
		jobPayrollCriteria
				.createAlias("payrollElementByPayrollElementId", "pe");
		jobPayrollCriteria.createAlias("payrollElementByPercentageElement",
				"percent", CriteriaSpecification.LEFT_JOIN);
		if (jobPayrollElementVo.getPayPolicy() != null) {
			jobPayrollCriteria.add(Restrictions.eq("payPolicy",
					jobPayrollElementVo.getPayPolicy()));
		}

		if (jobPayrollElementVo.getPayType() != null) {
			jobPayrollCriteria.add(Restrictions.eq("payType",
					jobPayrollElementVo.getPayType()));
		}

		List<Byte> calculationTypes = new ArrayList<Byte>();
		calculationTypes.add(Constants.HR.CalculationType.Variance.getCode());
		calculationTypes.add(Constants.HR.CalculationType.Count.getCode());
		if (jobPayrollElementVo.isUseCalculationType())
			jobPayrollCriteria.add(Restrictions.in("calculationType",
					calculationTypes));

		jobPayrollCriteria.add(Restrictions.eq("jobAssignment.jobAssignmentId",
				jobPayrollElementVo.getJobAssignmentId()));
		jobPayrollCriteria.add(Restrictions.eq("isActive", true));
		return jobPayrollCriteria.list();

	}

	// Get job payroll element list
	public JobPayrollElement getJobPayrollElementById(Long jobPayrollElementId) {
		List<JobPayrollElement> jobPayrollElments = jobPayrollElementDAO
				.findByNamedQuery("getJobPayrollElementById",
						jobPayrollElementId);

		if (jobPayrollElments != null && jobPayrollElments.size() > 0)
			return jobPayrollElments.get(0);
		else
			return null;
	}

	// Getter Setter
	public AIOTechGenericDAO<CmpDeptLoc> getCmpDeptLocationDAO() {
		return cmpDeptLocationDAO;
	}

	public void setCmpDeptLocationDAO(
			AIOTechGenericDAO<CmpDeptLoc> cmpDeptLocationDAO) {
		this.cmpDeptLocationDAO = cmpDeptLocationDAO;
	}

	public AIOTechGenericDAO<Location> getLocationDAO() {
		return locationDAO;
	}

	public void setLocationDAO(AIOTechGenericDAO<Location> locationDAO) {
		this.locationDAO = locationDAO;
	}

	public AIOTechGenericDAO<Department> getDepartmentDAO() {
		return departmentDAO;
	}

	public void setDepartmentDAO(AIOTechGenericDAO<Department> departmentDAO) {
		this.departmentDAO = departmentDAO;
	}

	public AIOTechGenericDAO<Grade> getGradeDAO() {
		return gradeDAO;
	}

	public void setGradeDAO(AIOTechGenericDAO<Grade> gradeDAO) {
		this.gradeDAO = gradeDAO;
	}

	public AIOTechGenericDAO<Job> getJobDAO() {
		return jobDAO;
	}

	public void setJobDAO(AIOTechGenericDAO<Job> jobDAO) {
		this.jobDAO = jobDAO;
	}

	public AIOTechGenericDAO<Designation> getDesignationDAO() {
		return designationDAO;
	}

	public void setDesignationDAO(AIOTechGenericDAO<Designation> designationDAO) {
		this.designationDAO = designationDAO;
	}

	public AIOTechGenericDAO<WorkingShift> getWorkingShiftDAO() {
		return workingShiftDAO;
	}

	public void setWorkingShiftDAO(
			AIOTechGenericDAO<WorkingShift> workingShiftDAO) {
		this.workingShiftDAO = workingShiftDAO;
	}

	public AIOTechGenericDAO<JobShift> getJobShiftDAO() {
		return jobShiftDAO;
	}

	public void setJobShiftDAO(AIOTechGenericDAO<JobShift> jobShiftDAO) {
		this.jobShiftDAO = jobShiftDAO;
	}

	public AIOTechGenericDAO<Leave> getLeaveDAO() {
		return leaveDAO;
	}

	public void setLeaveDAO(AIOTechGenericDAO<Leave> leaveDAO) {
		this.leaveDAO = leaveDAO;
	}

	public AIOTechGenericDAO<JobLeave> getJobLeaveDAO() {
		return jobLeaveDAO;
	}

	public void setJobLeaveDAO(AIOTechGenericDAO<JobLeave> jobLeaveDAO) {
		this.jobLeaveDAO = jobLeaveDAO;
	}

	public AIOTechGenericDAO<PayrollElement> getPayrollElementDAO() {
		return payrollElementDAO;
	}

	public void setPayrollElementDAO(
			AIOTechGenericDAO<PayrollElement> payrollElementDAO) {
		this.payrollElementDAO = payrollElementDAO;
	}

	public AIOTechGenericDAO<JobPayrollElement> getJobPayrollElementDAO() {
		return jobPayrollElementDAO;
	}

	public void setJobPayrollElementDAO(
			AIOTechGenericDAO<JobPayrollElement> jobPayrollElementDAO) {
		this.jobPayrollElementDAO = jobPayrollElementDAO;
	}

	public AIOTechGenericDAO<JobAssignment> getJobAssignmentDAO() {
		return jobAssignmentDAO;
	}

	public void setJobAssignmentDAO(
			AIOTechGenericDAO<JobAssignment> jobAssignmentDAO) {
		this.jobAssignmentDAO = jobAssignmentDAO;
	}

}
