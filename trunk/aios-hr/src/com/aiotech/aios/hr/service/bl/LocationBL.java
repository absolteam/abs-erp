package com.aiotech.aios.hr.service.bl;

import org.apache.commons.beanutils.BeanUtils;

import com.aiotech.aios.hr.domain.entity.Location;
import com.aiotech.aios.hr.domain.entity.vo.LocationVO;
import com.aiotech.aios.hr.service.LocationService;

public class LocationBL {

	private LocationService locationService;
	
	public String saveLocation(Location location) throws Exception{
		String returnMsg="";
		locationService.saveLocation(location);
		return returnMsg;
	}
	
	public LocationVO getLocationFullAddress(Long locationId) throws Exception {
		LocationVO locationVO = null;
		if (locationId != null && locationId > 0) {
			Location location = locationService.getLocationById(locationId);
			locationVO = new LocationVO();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(location.getAddress()+",");
			if (location.getArea() != null
					&& !location.getArea().trim().equals(""))
				stringBuilder.append(" Area :" + location.getArea()+",");
			
			if (location.getZone() != null
					&& !location.getZone().trim().equals(""))
				stringBuilder.append(" Zone :" + location.getZone()+",");

			if (location.getCity() != null
					&& !location.getCity().getCityName().trim().equals(""))
				stringBuilder.append(" City :" + location.getCity().getCityName()+",");
			
			if (location.getState() != null
					&& !location.getState().getStateName().trim().equals(""))
				stringBuilder.append(" State/Province :" + location.getState().getStateName()+",");
			
			if (location.getCountry() != null
					&& !location.getCountry().getCountryName().trim().equals(""))
				stringBuilder.append(" Country :" + location.getCountry().getCountryName()+",");
			
			if (location.getPostalCode() != null
					&& !location.getPostalCode().trim().equals(""))
				stringBuilder.append(" P.O. BOX :" + location.getPostalCode()+",");
			
			// Copy EmployeeLoan class properties value into VO properties.
			BeanUtils.copyProperties(locationVO, location);

			locationVO.setFullAddress(stringBuilder.toString());
		}
		return locationVO;
	}
	

	public LocationService getLocationService() {
		return locationService;
	}

	public void setLocationService(LocationService locationService) {
		this.locationService = locationService;
	}
}
