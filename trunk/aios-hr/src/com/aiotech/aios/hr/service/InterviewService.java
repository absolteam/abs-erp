package com.aiotech.aios.hr.service;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.Interview;
import com.aiotech.aios.hr.domain.entity.InterviewProcess;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class InterviewService {
	public AIOTechGenericDAO<Interview> interviewDAO;
	public AIOTechGenericDAO<InterviewProcess> interviewProcessDAO;

	public List<Interview> getAllInterview(Implementation implementation){
		return interviewDAO.findByNamedQuery("getAllInterview",
				implementation);
	}
	
	
	public Interview getInterviewById(Long interviewId) {
		List<Interview> interviews = interviewDAO.findByNamedQuery(
				"getInterviewById", interviewId);
		if (interviews != null && interviews.size() > 0)
			return interviews.get(0);
		else
			return null;
	}
	
	public InterviewProcess getInterviewProcessById(Long interviewProcessId) {
		List<InterviewProcess> interviewProcesss = interviewProcessDAO.findByNamedQuery(
				"getInterviewProcessById", interviewProcessId);
		if (interviewProcesss != null && interviewProcesss.size() > 0)
			return interviewProcesss.get(0);
		else
			return null;
	}
	
	public void saveInterview(Interview interview){
		interviewDAO.saveOrUpdate(interview);
	}
	
	public void deleteInterview(Interview interview){
		interviewDAO.delete(interview);
	}
	
	public void saveInterviewProcesses(List<InterviewProcess> interviewProcesses){
		interviewProcessDAO.saveOrUpdateAll(interviewProcesses);
	}
	public void saveInterviewProcess(InterviewProcess interviewProcess){
		interviewProcessDAO.saveOrUpdate(interviewProcess);
	}
	
	public void deleteInterviewProcesses(List<InterviewProcess> interviewProcesses){
		interviewProcessDAO.deleteAll(interviewProcesses);
	}
	
	public AIOTechGenericDAO<Interview> getInterviewDAO() {
		return interviewDAO;
	}

	public void setInterviewDAO(AIOTechGenericDAO<Interview> interviewDAO) {
		this.interviewDAO = interviewDAO;
	}

	public AIOTechGenericDAO<InterviewProcess> getInterviewProcessDAO() {
		return interviewProcessDAO;
	}

	public void setInterviewProcessDAO(
			AIOTechGenericDAO<InterviewProcess> interviewProcessDAO) {
		this.interviewProcessDAO = interviewProcessDAO;
	}
}
