package com.aiotech.aios.hr.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.OpenPosition;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class JobAssignmentService {
	public AIOTechGenericDAO<JobAssignment> jobAssignmentDAO;

	public List<JobAssignment> getAllJobAssignment(Implementation implementation) {
		return jobAssignmentDAO.findByNamedQuery("getAllJobAssignment",
				implementation,
				(byte)WorkflowConstants.Status.Published.getCode());
	}

	public List<JobAssignment> getAllActiveJobAssignment(
			Implementation implementation) {
		return jobAssignmentDAO.findByNamedQuery("getAllActiveJobAssignment",
				implementation);
	}

	public JobAssignment getJobAssignmentInfo(Long jobAssignmentId) {
		List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();
		jobAssignments = jobAssignmentDAO.findByNamedQuery(
				"getJobAssignmentInfo", jobAssignmentId);
		if (jobAssignments != null && jobAssignments.size() > 0)
			return jobAssignments.get(0);
		else
			return null;
	}

	public List<JobAssignment> getAllJobAssignmentForOpenPoistion(
			OpenPosition openPosition) {
		return jobAssignmentDAO.findByNamedQuery(
				"getAllJobAssignmentForOpenPoistion",
				openPosition.getDesignation(),
				openPosition.getCmpDeptLocation());
	}
	
	public List<JobAssignment> getJobAssignmentAgainstSwipeId(
			Long swipeId,Implementation implementation) {
		return jobAssignmentDAO.findByNamedQuery(
				"getJobAssignmentAgainstSwipeId",
				swipeId,implementation);
	}

	/**
	 * fetches list of all employees if fromDate and toDate are null. If
	 * fromDate and toDate are not null then it will only fetches the list of
	 * employees who took leaves between the input date range.
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<JobAssignment> getJobAssignmentByByDifferentValues(
			Date fromDate, Date toDate, Implementation implementation,
			Boolean isActive) throws Exception {
		Criteria jobAssignmentCriteria = jobAssignmentDAO.createCriteria();

		DetachedCriteria dc = DetachedCriteria.forClass(JobAssignment.class);

		dc.setProjection(Projections.distinct(Projections.id()));

		if (fromDate != null && toDate != null) {
			dc.createAlias("leaveProcesses", "lp",
					CriteriaSpecification.INNER_JOIN);

			dc.add(Restrictions.gt("lp.fromDate", fromDate)).add(
					Restrictions.lt("lp.toDate", toDate));
		} else {
			dc.createAlias("leaveProcesses", "lp",
					CriteriaSpecification.LEFT_JOIN);
		}

		if (implementation != null) {
			dc.add(Restrictions.eq("lp.implementation", implementation));

		}

		
		
		jobAssignmentCriteria.setFetchMode("person", FetchMode.EAGER);
		jobAssignmentCriteria.setFetchMode("job", FetchMode.EAGER);
		jobAssignmentCriteria.setFetchMode("cmpDeptLocation", FetchMode.EAGER);
		jobAssignmentCriteria.setFetchMode("designation", FetchMode.EAGER)
				.setFetchMode("designation.grade", FetchMode.EAGER);

		jobAssignmentCriteria.setFetchMode("cmpDeptLocation", FetchMode.EAGER)
				.setFetchMode("cmpDeptLocation.company", FetchMode.EAGER)
				.setFetchMode("cmpDeptLocation.department", FetchMode.EAGER)
				.setFetchMode("cmpDeptLocation.location", FetchMode.EAGER);

		jobAssignmentCriteria.add(Property.forName("jobAssignmentId").in(dc));

		if (isActive != null) {
			jobAssignmentCriteria.add(Restrictions.eq("isActive", isActive));
		}

		return jobAssignmentCriteria.list();

	}
	
	

	public List<JobAssignment> getAllJobAssignmentsWhichHasAttendenceByCompanyId(
			Long companyId) {
		return jobAssignmentDAO.findByNamedQuery(
				"getAllJobAssignmentsWhichHasAttendenceByCompanyId", companyId);
	}
	
	public JobAssignment getJobAssignmentByEmployee(Long personId) {
		List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();
		jobAssignments = jobAssignmentDAO.findByNamedQuery(
				"getJobAssignmentByPerson", personId);
		if (jobAssignments != null && jobAssignments.size() > 0)
			return jobAssignments.get(0);
		else
			return null;
	}

	public void saveJobAssignment(JobAssignment jobAssignment) {
		jobAssignmentDAO.saveOrUpdate(jobAssignment);
	}
	
	public void saveJobAssignmentFromPromotion(JobAssignment jobAssignment) {
		jobAssignmentDAO.merge(jobAssignment);
	}
	
	public void saveJobAssignmentAll(List<JobAssignment> jobAssignments) {
		jobAssignmentDAO.saveOrUpdateAll(jobAssignments);
	}

	public void deleteJobAssignment(JobAssignment jobAssignment) {
		jobAssignmentDAO.delete(jobAssignment);
	}

	public AIOTechGenericDAO<JobAssignment> getJobAssignmentDAO() {
		return jobAssignmentDAO;
	}

	public void setJobAssignmentDAO(
			AIOTechGenericDAO<JobAssignment> jobAssignmentDAO) {
		this.jobAssignmentDAO = jobAssignmentDAO;
	}

}
