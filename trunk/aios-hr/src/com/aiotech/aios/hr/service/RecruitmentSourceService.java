package com.aiotech.aios.hr.service;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.RecruitmentSource;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class RecruitmentSourceService {

	public AIOTechGenericDAO<RecruitmentSource> recruitmentSourceDAO;

	public List<RecruitmentSource> getAllRecruitmentSource(
			Implementation implementation) {
		return recruitmentSourceDAO.findByNamedQuery("getAllRecruitmentSource",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public RecruitmentSource getRecruitmentSourceById(Long recruitmentSourceId) {
		List<RecruitmentSource> recruitmentSources = recruitmentSourceDAO
				.findByNamedQuery("getRecruitmentSourceById",
						recruitmentSourceId);
		if (recruitmentSources != null && recruitmentSources.size() > 0)
			return recruitmentSources.get(0);
		else
			return null;

	}

	public void saveRecruitmentSource(RecruitmentSource recruitmentSource) {
		recruitmentSourceDAO.saveOrUpdate(recruitmentSource);
	}

	public void deleteRecruitmentSource(RecruitmentSource recruitmentSource) {
		recruitmentSourceDAO.delete(recruitmentSource);
	}

	public AIOTechGenericDAO<RecruitmentSource> getRecruitmentSourceDAO() {
		return recruitmentSourceDAO;
	}

	public void setRecruitmentSourceDAO(
			AIOTechGenericDAO<RecruitmentSource> recruitmentSourceDAO) {
		this.recruitmentSourceDAO = recruitmentSourceDAO;
	}
}
