package com.aiotech.aios.hr.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.LeaveProcess;
import com.aiotech.aios.hr.domain.entity.LeaveReconciliation;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class LeaveProcessService {
	public AIOTechGenericDAO<LeaveProcess> leaveProcessDAO;
	public AIOTechGenericDAO<LeaveReconciliation> leaveReconciliationDAO;

	// Get Leave process
	public LeaveProcess getLeaveProcess(Long leaveProcessId) {
		if (leaveProcessId != null && leaveProcessId != 0)
			return leaveProcessDAO.findByNamedQuery("getLeaveProcess",
					leaveProcessId).get(0);
		else
			return null;
	}

	// Get LeaveProcess list
	public List<LeaveProcess> getAllLeaveProcessList(
			Implementation implementation) {
		return leaveProcessDAO.findByNamedQuery("getAllLeaveProcessList",
				implementation);
	}
	
	public List<LeaveProcess> getLeaveProcessByTransactionRefrence(
			String transactionReference) {
		return leaveProcessDAO.findByNamedQuery("getLeaveProcessByTransactionRefrence",
				transactionReference);
	}

	// Get LeaveProcess list
	public List<LeaveProcess> getAllLeaveProcessBasedOnPerson(Person person) {
		return leaveProcessDAO.findByNamedQuery(
				"getAllLeaveProcessBasedOnPerson", person.getPersonNumber(),
				person.getImplementation(),
				(byte)WorkflowConstants.Status.Published.getCode());
	}

	// Get LeaveProcess list
	public List<LeaveProcess> getApprovedBasedOnPersonForParticularDate(
			Person person, Date fromDate, Date toDate) {
		return leaveProcessDAO.findByNamedQuery(
				"getApprovedBasedOnPersonForParticularDate",
				person.getPersonNumber(), person.getImplementation(), fromDate,
				toDate,
				(byte)WorkflowConstants.Status.Published.getCode());
	}

	// Get LeaveProcess list
	public List<LeaveProcess> getLeavHistory(Person person, Date fromDate,
			Date toDate) {
		return leaveProcessDAO.findByNamedQuery("getLeaveHistory",
				person.getPersonNumber(), person.getImplementation(), fromDate,
				toDate);
	}

	// Get LeaveProcess list
	public List<LeaveProcess> getLossOfPays(Person person) {
		return leaveProcessDAO.findByNamedQuery("getLossOfPays",
				person.getPersonId(), person.getImplementation());
	}

	// Get LeaveProcess list
	public List<LeaveReconciliation> getLeaveReconciliation(
			JobAssignment jobAssignment) {
		return leaveReconciliationDAO.findByNamedQuery(
				"getLeaveReconciliation", jobAssignment
						.getJobAssignmentNumber(), jobAssignment.getJob()
						.getImplementation());

	}

	public List<LeaveReconciliation> getLeaveReconciliationsByDiffValuesWithAllEmpDetails(
			Date fromDate, Date toDate, Boolean isActive,
			Implementation implementation) throws Exception {

		Criteria leaveReconciliationCriteria = leaveReconciliationDAO
				.createCriteria();

		if (fromDate != null && toDate != null) {
			leaveReconciliationCriteria.add(
					Restrictions.gt("startDate", fromDate)).add(
					Restrictions.lt("endDate", toDate));

		}

		if (implementation != null) {
			leaveReconciliationCriteria.add(Restrictions.eq("implementation",
					implementation));

		}

		if (isActive != null) {
			leaveReconciliationCriteria.add(Restrictions.eq("isActive",
					isActive));
		}

		leaveReconciliationCriteria.setFetchMode("jobLeave", FetchMode.EAGER)
				.setFetchMode("jobLeave.leave", FetchMode.EAGER)
				.setFetchMode("jobLeave.leave.lookupDetail", FetchMode.EAGER);

		leaveReconciliationCriteria
				.setFetchMode("jobAssignment", FetchMode.EAGER)
				.setFetchMode("jobAssignment.cmpDeptLocation", FetchMode.EAGER)
				.setFetchMode("jobAssignment.cmpDeptLocation.company",
						FetchMode.EAGER)
				.setFetchMode("jobAssignment.cmpDeptLocation.department",
						FetchMode.EAGER)
				.setFetchMode("jobAssignment.cmpDeptLocation.location",
						FetchMode.EAGER)
				.setFetchMode("jobAssignment.person", FetchMode.EAGER);

		leaveReconciliationCriteria
				.setFetchMode("jobAssignment.cmpDeptLocation", FetchMode.EAGER)
				.setFetchMode("jobAssignment.cmpDeptLocation.grade",
						FetchMode.EAGER)
				.setFetchMode("jobAssignment.designation", FetchMode.EAGER);

		return leaveReconciliationCriteria.list();

	}

	public void saveLeaveProcess(LeaveProcess leaveProcess) {
		leaveProcessDAO.saveOrUpdate(leaveProcess);
	}

	public void saveLeaveReconciliations(
			List<LeaveReconciliation> leaveReconciliations) {
		leaveReconciliationDAO.saveOrUpdateAll(leaveReconciliations);
	}

	public void deleteLeaveProcess(LeaveProcess leaveProcess) {
		leaveProcessDAO.delete(leaveProcess);
	}
	
	public void deleteAllLeaveProcess(List<LeaveProcess> leaveProcesses) {
		leaveProcessDAO.deleteAll(leaveProcesses);
	}

	public AIOTechGenericDAO<LeaveProcess> getLeaveProcessDAO() {
		return leaveProcessDAO;
	}

	public void setLeaveProcessDAO(
			AIOTechGenericDAO<LeaveProcess> leaveProcessDAO) {
		this.leaveProcessDAO = leaveProcessDAO;
	}

	public AIOTechGenericDAO<LeaveReconciliation> getLeaveReconciliationDAO() {
		return leaveReconciliationDAO;
	}

	public void setLeaveReconciliationDAO(
			AIOTechGenericDAO<LeaveReconciliation> leaveReconciliationDAO) {
		this.leaveReconciliationDAO = leaveReconciliationDAO;
	}

}
