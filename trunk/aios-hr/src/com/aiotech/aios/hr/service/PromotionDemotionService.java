package com.aiotech.aios.hr.service;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.PromotionDemotion;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class PromotionDemotionService {
	public AIOTechGenericDAO<PromotionDemotion> promotionDemotionDAO;

	
	public List<PromotionDemotion> getAllPromotionDemotion(
			Implementation implementation) {
		return promotionDemotionDAO.findByNamedQuery("getAllPromotionDemotion",
				implementation);
	}

	public PromotionDemotion getPromotionDemotionById(Long id) {
		List<PromotionDemotion> list = promotionDemotionDAO
				.findByNamedQuery("getPromotionDemotionById", id);
		if (list != null && list.size() > 0)
			return list.get(0);
		else
			return null;
	}
	
	public List<PromotionDemotion> getAllPromotionDemotionToActivate(
			Implementation implementation) {
		return promotionDemotionDAO.findByNamedQuery("getAllPromotionDemotionToActivate",
				implementation);
	}


	public void savePromotionDemotion(PromotionDemotion promotionDemotion) {
		promotionDemotionDAO.saveOrUpdate(promotionDemotion);
	}

	public void deletePromotionDemotion(PromotionDemotion promotionDemotion) {
		promotionDemotionDAO.delete(promotionDemotion);
	}


	public AIOTechGenericDAO<PromotionDemotion> getPromotionDemotionDAO() {
		return promotionDemotionDAO;
	}

	public void setPromotionDemotionDAO(
			AIOTechGenericDAO<PromotionDemotion> promotionDemotionDAO) {
		this.promotionDemotionDAO = promotionDemotionDAO;
	}


}
