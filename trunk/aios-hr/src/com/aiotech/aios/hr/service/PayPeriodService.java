package com.aiotech.aios.hr.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.hr.domain.entity.PayPeriod;
import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;
import com.aiotech.aios.hr.domain.entity.vo.PayPeriodVO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class PayPeriodService {

	public AIOTechGenericDAO<PayPeriod> payPeriodDAO;
	public AIOTechGenericDAO<PayPeriodTransaction> payPeriodTransactionDAO;

	public List<PayPeriod> getAllPayPeriod(Implementation implementation) {
		return payPeriodDAO.findByNamedQuery("getPayPeriod", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public PayPeriod getPayPeriodById(Long payPeriodId) {
		List<PayPeriod> payPeriods = payPeriodDAO.findByNamedQuery(
				"getPayPeriodById", payPeriodId);
		if (payPeriods != null && payPeriods.size() > 0)
			return payPeriods.get(0);
		else
			return null;
	}

	public List<PayPeriodTransaction> findPayPeriod(PayPeriodVO payPeriodVO) {
		Criteria payPeriod = payPeriodTransactionDAO.createCriteria();

		payPeriod.createAlias("payPeriod", "pp");
		payPeriod.createAlias("pp.grades", "grd");
		payPeriod.createAlias("grd.designations", "des");
		payPeriod.createAlias("des.jobAssignments", "ja");
		if (payPeriodVO.getTransactionDate() != null) {
			payPeriod.add(Restrictions.conjunction().add(
					Restrictions.le("startDate",
							payPeriodVO.getTransactionDate())));
			payPeriod.add(Restrictions.conjunction()
					.add(Restrictions.ge("endDate",
							payPeriodVO.getTransactionDate())));
		}

		if (payPeriodVO.isMonthly()) {
			payPeriod.add(Restrictions.isNotNull("payMonth"));
		} else if (payPeriodVO.isWeekly()) {
			payPeriod.add(Restrictions.isNotNull("payWeek"));
		} else if (payPeriodVO.isYearly()) {
			payPeriod.add(Restrictions.eq("isYear", true));
		}

		payPeriod.add(Restrictions.or(Restrictions.isNull("isFreeze"),
				Restrictions.eq("isFreeze", false)));

		payPeriod.add(Restrictions.eq("ja.jobAssignmentId",
				payPeriodVO.getJobAssignmentId()));
		return payPeriod.list();
	}

	public void savePayPeriod(PayPeriod payPeriod) {
		payPeriodDAO.saveOrUpdate(payPeriod);
	}

	public void savePayPeriodTransaction(
			PayPeriodTransaction payPeriodTransaction) {
		payPeriodTransactionDAO.saveOrUpdate(payPeriodTransaction);
	}

	public void saveAllPayPeriodTransaciton(
			List<PayPeriodTransaction> payPeriodTrans) {
		payPeriodTransactionDAO.saveOrUpdateAll(payPeriodTrans);
	}

	public void deletePayPeriod(PayPeriod payPeriod) {
		payPeriodDAO.delete(payPeriod);
	}

	public void deleteAllPayPeriodTransaciton(
			List<PayPeriodTransaction> payPeriodTrans) {
		payPeriodTransactionDAO.deleteAll(payPeriodTrans);
	}

	public PayPeriodTransaction getPayPeriodTransactionById(
			Long payPeriodTransactionId) {
		return payPeriodTransactionDAO.findById(payPeriodTransactionId);
	}

	public AIOTechGenericDAO<PayPeriod> getPayPeriodDAO() {
		return payPeriodDAO;
	}

	public void setPayPeriodDAO(AIOTechGenericDAO<PayPeriod> payPeriodDAO) {
		this.payPeriodDAO = payPeriodDAO;
	}

	public AIOTechGenericDAO<PayPeriodTransaction> getPayPeriodTransactionDAO() {
		return payPeriodTransactionDAO;
	}

	public void setPayPeriodTransactionDAO(
			AIOTechGenericDAO<PayPeriodTransaction> payPeriodTransactionDAO) {
		this.payPeriodTransactionDAO = payPeriodTransactionDAO;
	}
}
