package com.aiotech.aios.hr.service.bl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.LoanFrequency;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.EmployeeLoan;
import com.aiotech.aios.hr.domain.entity.EmployeeLoanRepayment;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.MemoWarningPerson;
import com.aiotech.aios.hr.domain.entity.PayrollElement;
import com.aiotech.aios.hr.domain.entity.PayrollTransaction;
import com.aiotech.aios.hr.domain.entity.vo.EmployeeLoanVO;
import com.aiotech.aios.hr.domain.entity.vo.EmployeePaymentVO;
import com.aiotech.aios.hr.domain.entity.vo.PayrollVO;
import com.aiotech.aios.hr.service.EmployeeLoanService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.workflow.domain.entity.Role;
import com.aiotech.aios.workflow.domain.entity.Screen;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class EmployeeLoanBL {

	private EmployeeLoanService employeeLoanService;
	private Implementation implementation;
	private PayrollElementBL payrollElementBL;
	private CommentBL commentBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private SystemService systemService;
	private AlertBL alertBL;
	private EmployeePaymentBL employeePaymentBL;

	public List<?> convertAllEntityToVO(List<EmployeeLoan> employeeLoans)
			throws Exception {
		List<EmployeeLoanVO> employeeLoanVOs = null;
		if (employeeLoans != null && employeeLoans.size() > 0) {
			employeeLoanVOs = new ArrayList<EmployeeLoanVO>();
			for (EmployeeLoan employeeLoan : employeeLoans) {
				employeeLoanVOs.add(convertEntityIntoVO(employeeLoan));
			}
		}
		return employeeLoanVOs;
	}

	public EmployeeLoanVO convertEntityIntoVO(EmployeeLoan employeeLoan)
			throws Exception {
		EmployeeLoanVO employeeLoanVO = new EmployeeLoanVO();
		Map<Integer, String> financeTypes = this.financeTypes();
		Map<Integer, String> repaymentTypes = this.repaymentTypes();

		// Copy EmployeeLoan class properties value into VO properties.
		BeanUtils.copyProperties(employeeLoanVO, employeeLoan);

		employeeLoanVO.setRequestedDateView(DateFormat
				.convertDateToString(employeeLoan.getRequestedDate() + ""));
		employeeLoanVO.setDueStartDateView(DateFormat
				.convertDateToString(employeeLoan.getDueStartDate() + ""));
		JobAssignment jobAssigment = employeeLoan.getJobAssignment();

		employeeLoanVO.setPersonNumber(jobAssigment.getPerson()
				.getPersonNumber());
		employeeLoanVO.setPersonName(jobAssigment.getPerson().getFirstName()
				+ " " + jobAssigment.getPerson().getLastName());
		employeeLoanVO.setCompanyName(jobAssigment.getCmpDeptLocation()
				.getCompany().getCompanyName());
		employeeLoanVO.setDesignationName(jobAssigment.getDesignation()
				.getDesignationName());
		employeeLoanVO.setDepartmentName(jobAssigment.getCmpDeptLocation()
				.getDepartment().getDepartmentName());
		employeeLoanVO.setLocationName(jobAssigment.getCmpDeptLocation()
				.getLocation().getLocationName());

		employeeLoanVO.setFinanceTypeView(financeTypes.get(employeeLoan
				.getFinanceType()));
		employeeLoanVO.setRepaymentTypeView(repaymentTypes.get(employeeLoan
				.getRepaymentType()));
		List<EmployeeLoanVO> loanRepayments = new ArrayList<EmployeeLoanVO>();
		EmployeeLoanVO detailVO = null;
		for (EmployeeLoanRepayment detail : employeeLoan
				.getEmployeeLoanRepayments()) {
			detailVO = new EmployeeLoanVO();
			detailVO.setDueStartDateView(DateFormat.convertDateToString(detail
					.getDueDate() + ""));
			detailVO.setPrincipal(AIOSCommons.formatAmount(detail
					.getPrincipal()));
			loanRepayments.add(detailVO);
		}
		employeeLoanVO.setLoanRepayments(loanRepayments);
		return employeeLoanVO;
	}

	public void saveEmployeeLoan(EmployeeLoan employeeLoan,
			EmployeeLoanVO repaymentDeleteList, Long alertId) throws Exception {

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo.setRejectAlertId(alertId);
		if (employeeLoan != null && employeeLoan.getEmployeeLoanId() != null
				&& employeeLoan.getEmployeeLoanId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}

		List<EmployeeLoanVO> employeeLoanVos = generateRepaymentSchedule(employeeLoan);
		List<EmployeeLoanRepayment> employeeLoanRepayments = new ArrayList<EmployeeLoanRepayment>();
		for (EmployeeLoanVO employeeLoanVO : employeeLoanVos) {
			EmployeeLoanRepayment employeeLoanRepayment = new EmployeeLoanRepayment();
			employeeLoanRepayment.setDueDate(DateFormat
					.convertStringToDate(employeeLoanVO.getDueDate()));
			employeeLoanRepayment.setEmployeeLoan(employeeLoan);
			employeeLoanRepayment.setInstallmentAmount(Double
					.valueOf(employeeLoanVO.getInstallment()));
			employeeLoanRepayment.setIntrest(Double.valueOf(employeeLoanVO
					.getIntrestRate()));
			employeeLoanRepayment.setPrincipalBegin(Double
					.valueOf(employeeLoanVO.getPricipalBegining()));
			employeeLoanRepayment.setPrincipalEnd(Double.valueOf(employeeLoanVO
					.getPrincipalEnd()));
			employeeLoanRepayment.setPrincipal(Double.valueOf(employeeLoanVO
					.getPrincipal()));
			employeeLoanRepayments.add(employeeLoanRepayment);
		}

		if (employeeLoan.getEmployeeLoanId() != null
				&& employeeLoan.getEmployeeLoanId() > 0) {
			employeePaymentBL.getTransactionBL().deleteTransaction(
					employeeLoan.getEmployeeLoanId(),
					EmployeeLoan.class.getSimpleName());
			employeePaymentBL.getDirectPaymentBL().deleteDirectPayment(
					employeeLoan.getEmployeeLoanId(),
					EmployeeLoan.class.getSimpleName());
			employeeLoanService
					.employeeLoanRepaymentsDelete(employeeLoanRepayments);
			
		}
		employeeLoanService.employeeLoanSave(employeeLoan);
		employeeLoanService.employeeLoanRepaymentsSave(employeeLoanRepayments);

		// Workflow Integration call
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				EmployeeLoan.class.getSimpleName(),
				employeeLoan.getEmployeeLoanId(), user, workflowDetailVo);

	}

	public EmployeeLoanVO approveEmployeeLoan(EmployeeLoan employeeLoan,
			EmployeeLoanVO employeeLoanVO) {
		try {
			EmployeePaymentVO employeePaymentVO = new EmployeePaymentVO();
			employeePaymentVO = generateEmployeePaymentVO(employeePaymentVO,
					employeeLoan);
			employeePaymentVO.setEditFlag(employeeLoanVO.isEditFlag());
			if (employeeLoanVO.isEditFlag()) {
				// Reverse the accounts transaction
				employeePaymentBL.getTransactionBL().deleteTransaction(
						employeeLoan.getEmployeeLoanId(),
						EmployeeLoan.class.getSimpleName());
			}
			employeePaymentVO.setDeleteFlag(employeeLoanVO.isDeleteFlag());
			employeePaymentBL.paymentTransactionCreation(employeePaymentVO);

			if (employeeLoanVO.isDeleteFlag()) {
				employeePaymentBL.getTransactionBL().deleteTransaction(
						employeeLoan.getEmployeeLoanId(),
						EmployeeLoan.class.getSimpleName());
				employeePaymentBL.getDirectPaymentBL().deleteDirectPayment(
						employeeLoan.getEmployeeLoanId(),
						EmployeeLoan.class.getSimpleName());
				employeeLoanService
						.employeeLoanRepaymentsDelete(new ArrayList<EmployeeLoanRepayment>(
								employeeLoan.getEmployeeLoanRepayments()));
				employeeLoanService.employeeLoanDelete(employeeLoan);

			}
			return employeeLoanVO;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public EmployeePaymentVO generateEmployeePaymentVO(EmployeePaymentVO payVO,
			EmployeeLoan employeeLoan) {

		payVO.setObject(employeeLoan);
		payVO.setRecordId(employeeLoan.getEmployeeLoanId());
		payVO.setUseCase("EmployeeLoan");
		payVO.setAmount(employeeLoan.getSanctionedAmount());
		PayrollElement payrollElement = null;
		if (employeeLoan.getFinanceType() == ((int) Constants.HR.LoanFinanceType.Loan
				.getCode())) {
			payVO.setAccountCategory(Constants.HR.LoanFinanceType.Loan.name());
			payrollElement = payrollElementBL.getPayrollElementService()
					.getPayrollElementByCode(getImplementation(), "LOAN");
			payVO.setPayrollElement(payrollElement);
		} else if (employeeLoan.getFinanceType() == ((int) Constants.HR.LoanFinanceType.Advance
				.getCode())) {
			payVO.setAccountCategory(Constants.HR.LoanFinanceType.Advance
					.name());
			payrollElement = payrollElementBL.getPayrollElementService()
					.getPayrollElementByCode(getImplementation(), "ADVANCE");
			payVO.setPayrollElement(payrollElement);
		}
		return payVO;
	}

	public void deleteEmployeeLoan(EmployeeLoan employeeLoan,
			EmployeeLoanVO employeeLoanVO, Long messageId) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				EmployeeLoan.class.getSimpleName(),
				employeeLoan.getEmployeeLoanId(), user, workflowDetailVo);
	}

	public void doEmployeeLoanBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		EmployeeLoan employeeLoan = this.getEmployeeLoanService()
				.getEmployeeLoanDetail(recordId);
		List<EmployeeLoanRepayment> employeeLoanRepayments = new ArrayList<EmployeeLoanRepayment>(
				employeeLoan.getEmployeeLoanRepayments());
		if (workflowDetailVO.isDeleteFlag()) {
			employeePaymentBL.getTransactionBL().deleteTransaction(
					employeeLoan.getEmployeeLoanId(),
					EmployeeLoan.class.getSimpleName());
			employeePaymentBL.getDirectPaymentBL().deleteDirectPayment(
					employeeLoan.getEmployeeLoanId(),
					EmployeeLoan.class.getSimpleName());
			employeeLoanService
					.employeeLoanRepaymentsDelete(employeeLoanRepayments);
			employeeLoanService.employeeLoanDelete(employeeLoan);
		} else {
			EmployeePaymentVO employeePaymentVO = new EmployeePaymentVO();
			employeePaymentVO = generateEmployeePaymentVO(employeePaymentVO,
					employeeLoan);
			// Reverse the accounts transaction
			employeePaymentBL.getTransactionBL().deleteTransaction(
					employeeLoan.getEmployeeLoanId(),
					EmployeeLoan.class.getSimpleName());
			employeePaymentVO.setDeleteFlag(false);
			employeePaymentBL.paymentTransactionCreation(employeePaymentVO);
		}
	}

	@SuppressWarnings("null")
	public List<EmployeeLoanVO> generateRepaymentSchedule(
			EmployeeLoan employeeLoan) throws Exception {
		List<EmployeeLoanVO> employeeLoanVOs = null;
		/*if (employeeLoan != null
				&& (employeeLoan.getEmployeeLoanId() == null || employeeLoan
						.getEmployeeLoanId() == 0)) {*/
			employeeLoanVOs = new ArrayList<EmployeeLoanVO>();
			Double pricipalStart = employeeLoan.getSanctionedAmount();
			Date startDate = employeeLoan.getDueStartDate();
			Double installment = employeeLoan.getSanctionedAmount()
					/ employeeLoan.getNumberOfRepayment();
			Double pricipalEnd = employeeLoan.getSanctionedAmount();
			Date currentDate = null;
			for (int i = 1; i <= employeeLoan.getNumberOfRepayment(); i++) {
				EmployeeLoanVO employeeLoanVO = new EmployeeLoanVO();
				pricipalEnd -= installment;
				if (i != 1)
					currentDate = findDueDate(startDate,
							employeeLoan.getPaymentFrequency());
				else
					currentDate = startDate;
				employeeLoanVO.setDueDate(DateFormat
						.convertSystemDateToString(currentDate));
				employeeLoanVO.setDueStartDate(currentDate);
				employeeLoanVO.setInstallment(Math.round(installment) + "");
				employeeLoanVO.setPricipalBegining(Math.round(pricipalStart)
						+ "");
				employeeLoanVO.setPrincipalEnd(Math.round(pricipalEnd) + "");
				Double intrest = interestCalculation(pricipalStart,
						employeeLoan);
				employeeLoanVO.setIntrestRate(Math.round(intrest) + "");
				employeeLoanVO.setPrincipal(Math.round(installment + intrest)
						+ "");
				pricipalStart -= installment;

				startDate = currentDate;
				employeeLoanVOs.add(employeeLoanVO);
			}

		//}
			/*else {
			employeeLoanVOs = new ArrayList<EmployeeLoanVO>();
			
			 * employeeLoan = employeeLoanService
			 * .getEmployeeLoanDetail(employeeLoan.getEmployeeLoanId());
			 
			if (employeeLoan != null
					&& employeeLoan.getEmployeeLoanRepayments() != null
					&& employeeLoan.getEmployeeLoanRepayments().size() > 0)
				employeeLoanVOs
						.addAll(convertAllRepaymentLoanEntityToVO(new ArrayList<EmployeeLoanRepayment>(
								employeeLoan.getEmployeeLoanRepayments())));

		}*/

		Collections.sort(employeeLoanVOs, new Comparator<EmployeeLoanVO>() {
			public int compare(EmployeeLoanVO m1, EmployeeLoanVO m2) {
				return m1.getDueStartDate().compareTo(m2.getDueStartDate());
			}
		});
		return employeeLoanVOs;
	}

	public List<EmployeeLoanVO> convertAllRepaymentLoanEntityToVO(
			List<EmployeeLoanRepayment> employeeLoanRepayments)
			throws Exception {
		List<EmployeeLoanVO> employeeLoanVOs = null;
		if (employeeLoanRepayments != null && employeeLoanRepayments.size() > 0) {
			employeeLoanVOs = new ArrayList<EmployeeLoanVO>();
			for (EmployeeLoanRepayment employeeLoanRepayment : employeeLoanRepayments) {
				employeeLoanVOs
						.add(convertRepaymentEntityIntoVO(employeeLoanRepayment));
			}
		}
		return employeeLoanVOs;
	}

	public EmployeeLoanVO convertRepaymentEntityIntoVO(
			EmployeeLoanRepayment employeeLoanRepayment) throws Exception {
		EmployeeLoanVO employeeLoanVO = new EmployeeLoanVO();

		employeeLoanVO.setDueDate(DateFormat
				.convertDateToString(employeeLoanRepayment.getDueDate() + ""));
		employeeLoanVO.setInstallment(Math.round(employeeLoanRepayment
				.getInstallmentAmount()) + "");
		employeeLoanVO.setPricipalBegining(Math.round(employeeLoanRepayment
				.getPrincipalBegin()) + "");
		employeeLoanVO.setPrincipalEnd(Math.round(employeeLoanRepayment
				.getPrincipalEnd()) + "");
		employeeLoanVO.setIntrestRate(Math.round(employeeLoanRepayment
				.getIntrest()) + "");
		employeeLoanVO.setPrincipal(Math.round(employeeLoanRepayment
				.getPrincipal()) + "");
		employeeLoanVO.setEmployeeLoanRepaymentId(employeeLoanRepayment
				.getEmployeeLoanRepaymentId());
		employeeLoanVO.setDueStartDate(employeeLoanRepayment.getDueDate());
		return employeeLoanVO;
	}

	public List<PayrollTransaction> getRepaymentList(PayrollVO payrollVO) {
		List<PayrollTransaction> payrollTransactions = null;
		PayrollElement payrollElement = null;
		try {
			payrollTransactions = new ArrayList<PayrollTransaction>();

			payrollElement = payrollElementBL.getPayrollElementService()
					.getPayrollElementByCode(getImplementation(), "LOAN");
			payrollVO.setLoanFinanceType(1);
			if (payrollElement != null) {
				payrollTransactions = generatePayrollTransacitonList(payrollVO,
						payrollTransactions, payrollElement);
			}
			payrollElement = null;
			payrollElement = payrollElementBL.getPayrollElementService()
					.getPayrollElementByCode(getImplementation(), "ADVANCE");

			payrollVO.setLoanFinanceType(2);
			if (payrollElement != null) {
				payrollTransactions = generatePayrollTransacitonList(payrollVO,
						payrollTransactions, payrollElement);
			}

		} catch (Exception e) {
			e.printStackTrace();
			payrollTransactions = null;
		}
		return payrollTransactions;

	}

	public List<PayrollTransaction> generatePayrollTransacitonList(
			PayrollVO payrollVO, List<PayrollTransaction> payrollTransactions,
			PayrollElement payrollElement) {
		try {
			PayrollTransaction payrollTransaction = null;

			List<EmployeeLoanRepayment> repayments = employeeLoanService
					.getEmployeeLoanRepayments(payrollVO);
			if (repayments != null && repayments.size() > 0) {

				for (EmployeeLoanRepayment employeeLoanRepayment : repayments) {
					payrollTransaction = new PayrollTransaction();
					payrollTransaction.setAmount(employeeLoanRepayment
							.getInstallmentAmount());
					payrollTransaction.setCreatedDate(new Date());
					payrollTransaction.setImplementation(getImplementation());

					payrollTransaction.setPayrollElement(payrollElement
							.getPayrollElement());
					payrollTransaction
							.setStatus(Constants.HR.PayTransactionStatus.New
									.getCode());
					payrollTransaction.setPerson(payrollVO.getJobAssignment()
							.getPerson());
					payrollTransaction.setRecordId(employeeLoanRepayment
							.getEmployeeLoanRepaymentId());
					payrollTransaction.setUseCase("EmployeeLoanRepayment");
					payrollTransactions.add(payrollTransaction);

					// Interest Amount
					if (employeeLoanRepayment.getIntrest() != null
							&& employeeLoanRepayment.getIntrest() > 0.0) {
						payrollTransaction = new PayrollTransaction();
						payrollElement = payrollElementBL
								.getPayrollElementService()
								.getPayrollElementByCode(getImplementation(),
										"FINE");
						payrollTransaction.setAmount(employeeLoanRepayment
								.getIntrest());
						payrollTransaction.setCreatedDate(new Date());
						payrollTransaction
								.setImplementation(getImplementation());

						payrollTransaction.setPayrollElement(payrollElement);
						payrollTransaction
								.setStatus(Constants.HR.PayTransactionStatus.New
										.getCode());
						payrollTransaction.setPerson(payrollVO
								.getJobAssignment().getPerson());
						payrollTransaction.setRecordId(employeeLoanRepayment
								.getEmployeeLoanRepaymentId());
						payrollTransaction.setUseCase("EmployeeLoanRepayment");
						payrollTransactions.add(payrollTransaction);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			payrollTransactions = null;
		}
		return payrollTransactions;
	}

	public Date findDueDate(Date date, Character frequency) throws Exception {
		if (Constants.Accounts.LoanFrequency.get(frequency).equals(
				Constants.Accounts.LoanFrequency.Monthly)) {
			date = DateFormat.addToMonths(date, 1);
		} else if (Constants.Accounts.LoanFrequency.get(frequency).equals(
				Constants.Accounts.LoanFrequency.Anually)) {
			date = DateFormat.addToMonths(date, 12);
		} else if (Constants.Accounts.LoanFrequency.get(frequency).equals(
				Constants.Accounts.LoanFrequency.SemiAnually)) {
			date = DateFormat.addToMonths(date, 6);
		} else if (Constants.Accounts.LoanFrequency.get(frequency).equals(
				Constants.Accounts.LoanFrequency.Quaterly)) {
			date = DateFormat.addToMonths(date, 3);
		} else if (Constants.Accounts.LoanFrequency.get(frequency).equals(
				Constants.Accounts.LoanFrequency.Daily)) {
			date = DateFormat.addToDays(date, 1);
		} else if (Constants.Accounts.LoanFrequency.get(frequency).equals(
				Constants.Accounts.LoanFrequency.Weekly)) {
			date = DateFormat.addToDays(date, 7);
		}
		return date;
	}

	public Double interestCalculation(Double amount, EmployeeLoan employeeLoan)
			throws Exception {
		Double interestRate = 0.0;
		if (employeeLoan.getIntrest() == 2) {
			if (employeeLoan.getInterestPercentage() != null
					&& employeeLoan.getInterestPercentage() > 0) {
				interestRate = ((employeeLoan.getInterestPercentage() / 100) * amount)
						/ 365
						* getVarianceOfFrequecy(employeeLoan
								.getPaymentFrequency());
			} else {
				interestRate = employeeLoan.getFixedInterest();
			}
		}
		return interestRate;
	}

	public int getVarianceOfFrequecy(Character frequency) {
		int variance = 0;
		if (Constants.Accounts.LoanFrequency.get(frequency).equals(
				Constants.Accounts.LoanFrequency.Monthly)) {
			variance = 30;
		} else if (Constants.Accounts.LoanFrequency.get(frequency).equals(
				Constants.Accounts.LoanFrequency.Anually)) {
			variance = 365;
		} else if (Constants.Accounts.LoanFrequency.get(frequency).equals(
				Constants.Accounts.LoanFrequency.SemiAnually)) {
			variance = 365 / 2;
		} else if (Constants.Accounts.LoanFrequency.get(frequency).equals(
				Constants.Accounts.LoanFrequency.Quaterly)) {
			variance = 365 / 4;
		} else if (Constants.Accounts.LoanFrequency.get(frequency).equals(
				Constants.Accounts.LoanFrequency.Daily)) {
			variance = 1;
		} else if (Constants.Accounts.LoanFrequency.get(frequency).equals(
				Constants.Accounts.LoanFrequency.Weekly)) {
			variance = 7;
		}
		return variance;
	}

	public Map<Integer, String> financeTypes() {
		Map<Integer, String> types = new HashMap<Integer, String>();
		types.put((int) Constants.HR.LoanFinanceType.Loan.getCode(),
				Constants.HR.LoanFinanceType.Loan.name());
		types.put((int) Constants.HR.LoanFinanceType.Advance.getCode(),
				Constants.HR.LoanFinanceType.Advance.name());
		return types;
	}

	public Map<Integer, String> repaymentTypes() {
		Map<Integer, String> types = new HashMap<Integer, String>();
		types.put(1, "Deduction From Salary");
		types.put(2, "EOS Settlement");
		types.put(3, "Annual Leave Salary Settlment");
		types.put(4, "Cash/Other");
		return types;
	}

	public Map<Integer, String> securityDocuments() {
		Map<Integer, String> types = new HashMap<Integer, String>();
		types.put(1, "Passport");
		types.put(2, "EOS");
		types.put(3, "Car Documents");
		types.put(4, "Fixed Asset");
		types.put(5, "Other");
		return types;
	}

	public Map<Integer, String> intrestTyps() {
		Map<Integer, String> types = new HashMap<Integer, String>();
		types.put(1, "Intrest Free");
		types.put(2, "Simple");
		types.put(3, "Compound");
		types.put(4, "Annuties");
		return types;
	}

	public LoanFrequency[] frequencyTypes() {
		LoanFrequency[] types = Constants.Accounts.LoanFrequency.values();
		/*
		 * types.put("M", "Monthly"); types.put("D", "Daily"); types.put("W",
		 * "Weekly"); types.put("Q", "Quarterly"); types.put("H",
		 * "Semi Annually"); types.put("A", "Annually");
		 */
		return types;
	}

	public EmployeeLoanService getEmployeeLoanService() {
		return employeeLoanService;
	}

	public void setEmployeeLoanService(EmployeeLoanService employeeLoanService) {
		this.employeeLoanService = employeeLoanService;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public PayrollElementBL getPayrollElementBL() {
		return payrollElementBL;
	}

	public void setPayrollElementBL(PayrollElementBL payrollElementBL) {
		this.payrollElementBL = payrollElementBL;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public EmployeePaymentBL getEmployeePaymentBL() {
		return employeePaymentBL;
	}

	public void setEmployeePaymentBL(EmployeePaymentBL employeePaymentBL) {
		this.employeePaymentBL = employeePaymentBL;
	}
}
