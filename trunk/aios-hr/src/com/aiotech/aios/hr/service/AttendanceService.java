package com.aiotech.aios.hr.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.hr.domain.entity.Attendance;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.EmployeeCalendar;
import com.aiotech.aios.hr.domain.entity.EmployeeCalendarPeriod;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.SwipeInOut;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AttendanceService {
	public AIOTechGenericDAO<AttendancePolicy> attendancePolicyDAO;
	public AIOTechGenericDAO<SwipeInOut> swipeInOutDAO;
	public AIOTechGenericDAO<Attendance> attendanceDAO;
	public AIOTechGenericDAO<EmployeeCalendarPeriod> employeeCalendarPeriodDAO;
	public AIOTechGenericDAO<EmployeeCalendar> employeeCalendarDAO;

	public void saveAttendance(Attendance attendance) {
		attendanceDAO.saveOrUpdate(attendance);
	}
	
	public void saveAllAttendance(List<Attendance> attendances) {
		attendanceDAO.saveOrUpdateAll(attendances);
	}

	public void saveSwipeInOutRecords(List<SwipeInOut> swipeInOuts) {
		swipeInOutDAO.saveOrUpdateAll(swipeInOuts);
	}

	public List<SwipeInOut> getAllSwipeInOut(Long swipeId,
			Implementation implementation) {
		return swipeInOutDAO.findByNamedQuery("getAllSwipeInOut", swipeId,
				implementation);
	}

	public List<SwipeInOut> getAllSwipeInOutWithDateReader(Long swipeId,
			Implementation implementation, Date date, String type) {
		return swipeInOutDAO.findByNamedQuery("getAllSwipeInOutWithDateReader",
				swipeId, implementation, date, type);
	}

	public List<SwipeInOut> getAllSwipeInOutWithDate(Long swipeId,
			Implementation implementation, Date date) {
		return swipeInOutDAO.findByNamedQuery("getAllSwipeInOutWithDate",
				swipeId, implementation, date);
	}

	public void deleteSwipeInOutRecords(List<SwipeInOut> swipeInOuts) {
		swipeInOutDAO.deleteAll(swipeInOuts);
	}

	public AttendancePolicy getAttendancePolicyInfo(Long gradeId,
			Implementation implementation) {
		List<AttendancePolicy> attendancePolicies = attendancePolicyDAO
				.findByNamedQuery("getAttendancePolicyInfo", implementation,gradeId);
		if (attendancePolicies != null && attendancePolicies.size() > 0)
			return attendancePolicies.get(0);
		else
			return null;
	}

	public Attendance getAttendanceInfo(Attendance attendance) {
		List<Attendance> attendances = attendanceDAO.findByNamedQuery(
				"getAttendanceInfo", attendance
						.getAttendanceDate(), attendance.getPerson()
						.getPersonId());
		if (attendances != null && attendances.size() > 0)
			return attendances.get(0);
		else
			return null;
	}

	// Attendance list
	public List<Attendance> getAllAttendanceBasedOnPerson(Person person) {
		return attendanceDAO.findByNamedQuery("getAllAttendanceBasedOnPerson",
				person);
	}

	// Attendance list
	public List<Attendance> getAllAttendance(Implementation implementation) {
		return attendanceDAO.findByNamedQuery("getAllAttendance",
				implementation);
	}

	// Attendance list punch report
	public List<Attendance> getAllAttendancePunchReport(Long personId,
			Date fromDate, Date toDate) {
		return attendanceDAO.findByNamedQuery("getAllAttendancePunchReport",
				personId, fromDate, toDate);
	}

	// Attendance list punch report
	public List<Attendance> getCompanyAttendancePunchReport(Long companyId,
			Date fromDate, Date toDate) {
		return attendanceDAO.findByNamedQuery(
				"getCompanyAttendancePunchReport", companyId, fromDate, toDate);
	}

	// Attendance list punch report
	public List<Attendance> getAllAttendancePunchReportBasedPeriod(
			Implementation implementation, Date fromDate, Date toDate) {
		return attendanceDAO.findByNamedQuery(
				"getAllAttendancePunchReportBasedPeriod", implementation,
				fromDate, toDate);
	}

	public Attendance getAttendanceInfoForDevationProcess(Long attendanceId) {
		List<Attendance> attendances = attendanceDAO.findByNamedQuery(
				"getAttendanceInfoForDevationProcess", attendanceId);
		if (attendances != null && attendances.size() > 0)
			return attendances.get(0);
		else
			return null;
	}

	public Attendance getAttendanceInfoFindById(Long attendanceId) {
		return attendanceDAO.findById(attendanceId);

	}

	public void deleteAttendance(Attendance attendance) {
		attendanceDAO.delete(attendance);
	}

	// Get Employee calendar period list
	public List<EmployeeCalendarPeriod> getEmployeeCalendarPeriods(
			Date attendanceDate, Long empCalId) {
		return employeeCalendarPeriodDAO.findByNamedQuery(
				"getEmployeeCalendarPeriods", attendanceDate, attendanceDate,
				empCalId);
	}

	// Get Employee calendar period list
	public List<EmployeeCalendar> getEmployeeCalendar(
			Implementation implementation) {
		return employeeCalendarDAO.findByNamedQuery("getEmployeeCalendar",
				implementation);
	}

	// Get Employee calendar period list
	public List<Attendance> getAllAttendanceDeviationForSalary(Person person,
			Date fromDate, Date toDate) {
		return attendanceDAO.findByNamedQuery(
				"getAllAttendanceDeviationForSalary", person, fromDate, toDate);
	}

	// Get Employee calendar period list
	public List<Attendance> getAllAttendanceForCompOff(Person person,
			Date fromDate, Date toDate) {
		return attendanceDAO.findByNamedQuery("getAllAttendanceForCompOff",
				person, fromDate, toDate);
	}

	public List<SwipeInOut> getSwipeInOutsByCriteria(Date fromDate,
			Date toDate, Implementation implementation,Long swipeId) throws Exception {

		Criteria swipeInOutCriteria = swipeInOutDAO.createCriteria();

		if (fromDate != null && toDate != null) {
			swipeInOutCriteria.add(Restrictions.ge("attendanceDate", fromDate));
			swipeInOutCriteria.add(Restrictions.le("attendanceDate", toDate));
		}

		if (implementation != null) {
			swipeInOutCriteria.add(Restrictions.eq("implementation",
					implementation));
		}
		
		if (swipeId != null && swipeId>0) {
			swipeInOutCriteria.add(Restrictions.eq("swipeId",
					swipeId));
		}

		return swipeInOutCriteria.list();
	}

	public List<SwipeInOut> getSwipeInOutsByJobAssignmentIds(
			List<Long> jobAssignIds) throws Exception {

		Query query = swipeInOutDAO
				.getNamedQuery("getSwipeInOutsByJobAssignmentIds");
		query.setParameterList("jobAssignmentIds", jobAssignIds);

		return query.list();

	}
	
	public List<Attendance> getAbsenteesmByPeriod(Date fromDate,
			Date toDate, Implementation implementation) throws Exception {

		Criteria attendance = attendanceDAO.createCriteria();
		attendance.createAlias("jobAssignment", "ja",
				CriteriaSpecification.LEFT_JOIN);
		attendance.createAlias("ja.person", "person",
				CriteriaSpecification.LEFT_JOIN);
		attendance.createAlias("ja.designation", "des",
				CriteriaSpecification.LEFT_JOIN);
		attendance.createAlias("ja.cmpDeptLocation", "setup",
				CriteriaSpecification.LEFT_JOIN);
		attendance.createAlias("setup.department", "department",
				CriteriaSpecification.LEFT_JOIN);
		if (fromDate != null && toDate != null) {
			attendance.add(Restrictions.gt("attendanceDate", fromDate))
					.add(Restrictions.lt("attendanceDate", toDate));
		}

		if (implementation != null) {
			attendance.add(Restrictions.eq("implementation",
					implementation));
		}

		attendance.add(Restrictions.eq("isAbsent",
				true));
		return attendance.list();
	}

	// Getter setter
	public AIOTechGenericDAO<AttendancePolicy> getAttendancePolicyDAO() {
		return attendancePolicyDAO;
	}

	public void setAttendancePolicyDAO(
			AIOTechGenericDAO<AttendancePolicy> attendancePolicyDAO) {
		this.attendancePolicyDAO = attendancePolicyDAO;
	}

	public AIOTechGenericDAO<SwipeInOut> getSwipeInOutDAO() {
		return swipeInOutDAO;
	}

	public void setSwipeInOutDAO(AIOTechGenericDAO<SwipeInOut> swipeInOutDAO) {
		this.swipeInOutDAO = swipeInOutDAO;
	}

	public AIOTechGenericDAO<Attendance> getAttendanceDAO() {
		return attendanceDAO;
	}

	public void setAttendanceDAO(AIOTechGenericDAO<Attendance> attendanceDAO) {
		this.attendanceDAO = attendanceDAO;
	}

	public AIOTechGenericDAO<EmployeeCalendarPeriod> getEmployeeCalendarPeriodDAO() {
		return employeeCalendarPeriodDAO;
	}

	public void setEmployeeCalendarPeriodDAO(
			AIOTechGenericDAO<EmployeeCalendarPeriod> employeeCalendarPeriodDAO) {
		this.employeeCalendarPeriodDAO = employeeCalendarPeriodDAO;
	}

	public AIOTechGenericDAO<EmployeeCalendar> getEmployeeCalendarDAO() {
		return employeeCalendarDAO;
	}

	public void setEmployeeCalendarDAO(
			AIOTechGenericDAO<EmployeeCalendar> employeeCalendarDAO) {
		this.employeeCalendarDAO = employeeCalendarDAO;
	}

}
