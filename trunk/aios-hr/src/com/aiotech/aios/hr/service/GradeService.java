package com.aiotech.aios.hr.service;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.hr.domain.entity.Grade;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class GradeService {
	
	public AIOTechGenericDAO<Grade> gradeDAO;
	
	public List<Grade> getGradeList(Implementation implementation) {
		return gradeDAO.findByNamedQuery("getGradeList", implementation,
				(byte)WorkflowConstants.Status.Published.getCode());
	}
	
	public Grade getGradeInfo(Long gradeId){
		List<Grade> grades=new ArrayList<Grade>();
		grades=gradeDAO.findByNamedQuery("getGradeInfo", gradeId);
		if(grades!=null && grades.size()>0)
			return grades.get(0);
		else
			return null;
	}
	
	public Grade findGradeByJobAssignmentId(Long jobAssignmentId){
		List<Grade> grades=new ArrayList<Grade>();
		grades=gradeDAO.findByNamedQuery("findGradeByJobAssignmentId", jobAssignmentId);
		if(grades!=null && grades.size()>0)
			return grades.get(0);
		else
			return null;
	}
	
	public Grade findGradeByOpenPositionId(Long openPositionId){
		List<Grade> grades=new ArrayList<Grade>();
		grades=gradeDAO.findByNamedQuery("findGradeByOpenPositionId", openPositionId);
		if(grades!=null && grades.size()>0)
			return grades.get(0);
		else
			return null;
	}
	
	public void saveGrade(Grade grade){
		gradeDAO.saveOrUpdate(grade);
	}
	
	public void deleteGrade(Grade grade){
		gradeDAO.delete(grade);
	}

	public AIOTechGenericDAO<Grade> getGradeDAO() {
		return gradeDAO;
	}

	public void setGradeDAO(AIOTechGenericDAO<Grade> gradeDAO) {
		this.gradeDAO = gradeDAO;
	}

}
