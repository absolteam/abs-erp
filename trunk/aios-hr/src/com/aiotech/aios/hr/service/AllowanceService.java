package com.aiotech.aios.hr.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.hr.domain.entity.Allowance;
import com.aiotech.aios.hr.domain.entity.AllowanceTransaction;
import com.aiotech.aios.hr.domain.entity.vo.AllowanceVO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AllowanceService {
	public AIOTechGenericDAO<Allowance> allowanceDAO;
	public AIOTechGenericDAO<AllowanceTransaction> allowanceTransactionDAO;

	public List<Allowance> getAllAllowanceByCriteria(
AllowanceVO allowanceVO) {
		Criteria jobPayrollCriteria = allowanceDAO.createCriteria();

		jobPayrollCriteria.createAlias("jobAssignment", "ja");
		jobPayrollCriteria.createAlias("lookupDetailByAllowanceType", "type");
		jobPayrollCriteria.createAlias("lookupDetailByAllowanceSubType",
				"subType", CriteriaSpecification.LEFT_JOIN);
		jobPayrollCriteria.createAlias("jobPayrollElement", "jpa");
		jobPayrollCriteria.createAlias("jpa.payrollElementByPayrollElementId",
				"pe");
		jobPayrollCriteria.createAlias("jpa.payrollElementByPercentageElement",
				"percentage", CriteriaSpecification.LEFT_JOIN);
		if (allowanceVO.getPayPeriodTransaction() != null
				&& allowanceVO.getPayPeriodTransaction().getStartDate() != null) {
			jobPayrollCriteria.add(Restrictions.conjunction().add(
					Restrictions.ge("fromDate", allowanceVO
							.getPayPeriodTransaction().getStartDate())));
			jobPayrollCriteria.add(Restrictions.conjunction().add(
					Restrictions.le("toDate", allowanceVO
							.getPayPeriodTransaction().getEndDate())));
		}

		if (allowanceVO.getJobAssignmentId() != null) {
			jobPayrollCriteria.add(Restrictions.eq("ja.jobAssignmentId",
					allowanceVO.getJobAssignmentId()));
		}

		if (allowanceVO.getJobPayrollElementId() != null) {
			jobPayrollCriteria.add(Restrictions.eq("jpa.jobPayrollElementId",
					allowanceVO.getJobPayrollElementId()));
		}

		return jobPayrollCriteria.list();
	}

	public List<Allowance> getAllAllowance(Implementation implementation) {
		return allowanceDAO.findByNamedQuery("getAllAllowance", implementation);
	}

	public Allowance getAllowanceById(Long allowanceId) {
		List<Allowance> allowances = allowanceDAO.findByNamedQuery(
				"getAllowanceById", allowanceId);
		if (allowances != null && allowances.size() > 0)
			return allowances.get(0);
		else
			return null;
	}

	public void saveAllowance(Allowance allowance) {
		allowanceDAO.saveOrUpdate(allowance);
	}

	public void saveAllAllowanceTransaction(
			List<AllowanceTransaction> allowanceTransactions) {
		allowanceTransactionDAO.saveOrUpdateAll(allowanceTransactions);
	}

	public void deleteAllowance(Allowance allowance) {
		allowanceDAO.delete(allowance);
	}

	public void deleteAllAllowanceTransaction(
			List<AllowanceTransaction> allowanceTransactions) {
		allowanceTransactionDAO.deleteAll(allowanceTransactions);
	}

	public AIOTechGenericDAO<Allowance> getAllowanceDAO() {
		return allowanceDAO;
	}

	public void setAllowanceDAO(AIOTechGenericDAO<Allowance> allowanceDAO) {
		this.allowanceDAO = allowanceDAO;
	}

	public AIOTechGenericDAO<AllowanceTransaction> getAllowanceTransactionDAO() {
		return allowanceTransactionDAO;
	}

	public void setAllowanceTransactionDAO(
			AIOTechGenericDAO<AllowanceTransaction> allowanceTransactionDAO) {
		this.allowanceTransactionDAO = allowanceTransactionDAO;
	}

}
