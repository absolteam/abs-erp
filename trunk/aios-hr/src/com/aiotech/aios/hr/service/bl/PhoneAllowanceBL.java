package com.aiotech.aios.hr.service.bl;

import org.apache.commons.beanutils.BeanUtils;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.PhoneAllowance;
import com.aiotech.aios.hr.domain.entity.vo.PhoneAllowanceVO;
import com.aiotech.aios.hr.service.PhoneAllowanceService;
import com.aiotech.aios.system.service.bl.LookupMasterBL;

public class PhoneAllowanceBL {

	private LookupMasterBL lookupMasterBL;
	private PhoneAllowanceService phoneAllowanceService;
	private PayPeriodBL payPeriodBL;

	public PhoneAllowanceVO convertEntityToVO(PhoneAllowance phoneAllowance) {
		PhoneAllowanceVO phoneAllowanceVO = new PhoneAllowanceVO();
		try {
			// Copy Entity class properties value into VO properties.
			BeanUtils.copyProperties(phoneAllowanceVO, phoneAllowance);
			phoneAllowanceVO.setFromDateView(DateFormat
					.convertDateToString(phoneAllowance.getFromDate() + ""));
			phoneAllowanceVO.setToDateView(DateFormat
					.convertDateToString(phoneAllowance.getToDate() + ""));
			if (phoneAllowance.getIsFinanceImpact() != null
					&& phoneAllowance.getIsFinanceImpact())
				phoneAllowanceVO.setIsFinanceView("YES");
			else
				phoneAllowanceVO.setIsFinanceView("NO");

			if (phoneAllowance.getLookupDetailBySubscriptionType() != null)
				phoneAllowanceVO.setSubscriptionType(phoneAllowance
						.getLookupDetailBySubscriptionType().getDisplayName());
			if (phoneAllowance.getLookupDetailByNetworkProvider() != null)
				phoneAllowanceVO.setNetworkProvider(phoneAllowance
						.getLookupDetailByNetworkProvider().getDisplayName());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return phoneAllowanceVO;
	}

	public String savePhoneAllowance(PhoneAllowance phoneAllowance) {
		String returnStatus = "SUCCESS";
		try {

			phoneAllowanceService.savePhoneAllowance(phoneAllowance);

		} catch (Exception e) {
			returnStatus = "ERROR";
		}
		return returnStatus;
	}

	public String deletePhoneAllowance(PhoneAllowance phoneAllowance) {
		String returnStatus = "SUCCESS";
		try {

			phoneAllowanceService.deletePhoneAllowance(phoneAllowance);
		} catch (Exception e) {
			returnStatus = "ERROR";
		}
		return returnStatus;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public PhoneAllowanceService getPhoneAllowanceService() {
		return phoneAllowanceService;
	}

	public void setPhoneAllowanceService(
			PhoneAllowanceService phoneAllowanceService) {
		this.phoneAllowanceService = phoneAllowanceService;
	}

	public PayPeriodBL getPayPeriodBL() {
		return payPeriodBL;
	}

	public void setPayPeriodBL(PayPeriodBL payPeriodBL) {
		this.payPeriodBL = payPeriodBL;
	}
}
